# License

License information and attribution notices are given for every file: either in the file itself or in a file whose information applies to the whole folder and its subfolders recursively until overwritten by information in a file further down the directory tree.
