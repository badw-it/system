import argparse
import ctypes
import inspect
import os

class NoParam:
    pass

NO_PARAM = NoParam()

def get_call_args_for(function: callable) -> 'dict[str, Any]':
    '''
    From the signature of :param:`function`, generate a template and fill it
    with the parameters that are read from the command line when calling the
    file that calls this function. Return the parameters as a mapping. A key
    is a parameter name (according to the function signature), and the value
    is the parameterʼs value read from the command line.

    For the template, take only keyword or positional-and-keyword parameters
    into account, no ``*args`` and ``**kwargs``. This function will not work
    with these kind of parameters.

    For the command line, provide a `--help` option. Its content is taken from
    the functionʼs docstring, parameters, parameter defaults and parameter
    annotations.

    The values from the command line are strings. But If the annotation of a
    parameter specifies its argument as a callable, e.g. ``int`` or ``float``,
    this function is used to convert the string. If it is ``bool``, the value
    is converted according to its meaning, i.e. ``'True'`` and ``'true'`` to
    ``True``, ``'False'`` and ``'false'`` to ``False``.

    An example in a file ``'example.py'``::
        import sys_io
        def func(arg: str, kwarg: int = 3):
            print(arg * kwarg)
        if __name__ == '__main__':
            func(**sys_io.get_call_args_for(func))

    This may be called from the command line as follows::
        python example.py --arg ja --kwarg 2

    and returns::
        jaja
    '''
    def boolean(term: str) -> bool:
        term = term.lower()
        if term == 'true':
            return True
        elif term == 'false':
            return False
        else:
            raise TypeError(str(term) + '\n... could not be read as a boolean.')

    parser = argparse.ArgumentParser(
            formatter_class = argparse.RawDescriptionHelpFormatter,
            description = function.__doc__,
            )
    for param in inspect.signature(
            function, follow_wrapped = False).parameters.values():
        if param.kind not in {
                inspect.Parameter.POSITIONAL_ONLY,
                inspect.Parameter.VAR_KEYWORD,
                inspect.Parameter.VAR_POSITIONAL,
                }:
            anno = param.annotation
            doc = repr(anno)
            if not isinstance(param.default, inspect._empty):
                doc += '. Default: ' + repr(param.default)
                name = '--'
            name += param.name
            if anno == bool:
                convert = boolean
            elif anno != str and callable(anno):
                convert = anno
            else:
                convert = None
            if convert:
                parser.add_argument(
                        name, help = doc, default = NO_PARAM, type = convert)
            else:
                parser.add_argument(
                        name, help = doc, default = NO_PARAM)
    return {
            name: value for name, value in vars(parser.parse_args()).items()
            if value != NO_PARAM
            }

def pid_exists(pid: int) -> bool:
    '''
    Taken from:

    - http://stackoverflow.com/a/17645146 by ntrrgc,
    - http://stackoverflow.com/a/20186516 by J.F. Sebastian
    '''
    if os.name == 'posix':
        if pid < 0:
            return False #NOTE: pid == 0 returns True
        try:
            os.kill(pid, 0) 
        except ProcessLookupError: # errno.ESRCH
            return False # No such process
        except PermissionError: # errno.EPERM
            return True # Operation not permitted (i.e., process exists)
        else:
            return True # no error, we can send a signal to the process
    else:
        kernel32 = ctypes.windll.kernel32
        SYNCHRONIZE = 0x100000
        process = kernel32.OpenProcess(SYNCHRONIZE, 0, pid)
        if process != 0:
            kernel32.CloseHandle(process)
            return True
        else:
            return False
