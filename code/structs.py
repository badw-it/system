# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.

class DefaultkeyDict(dict):
    '''
    If a key ``k`` is looked up via ``[k]``, i.e. ``.__getitem__(k)``,
    and ``k`` is not in the dictionary, then a default key (specified on
    initialization) is looked up instead. If the default key is not in
    the dictionary, too, then a structure is returned that behaves as
    the string ``k`` but will also return ``k`` when indexed with a
    non-integer (as if it were a dictionary).

    The DefaultkeyDict dictionary is not changed by lookups.

    A use-case:

    ``glosses[keyword][language_id]`` is to return a translation of
    ``keyword`` in the language specified by ``language_id``.

    If ``glosses`` is a ``DefaultkeyDict`` of ``DefaultkeyDict``s and the
    default key specifies a default language, then:

    - If ``keyword`` is not found, ``keyword`` itself is returned.
    - If ``keyword`` is found but ``language_id`` is not, the translation
      of the default-key language is returned, if available. (If not, the
      ``language_id`` itself is returned.)
    '''
    def __init__(self, defaultkey: 'Hashable', *args, **kwargs):
        '''
        :param defaultkey: The default key, which is:

        - used to be looked up instead of any key that is looked up
          but not found in the dictionary.
        - returned, if it is not in the dictionary.
        '''
        self.update(*args, **kwargs)
        self.defaultkey = defaultkey

    def __missing__(self, key: str):
        ersatzstring = DefaultkeyString(key)
        return self.get(self.defaultkey, ersatzstring)

class DefaultkeyString(str):
    def __getitem__(self, key):
        if isinstance(key, int):
            return super().__getitem__(key)
        return self

class Empty:
    r'''
    Structure for returning an empty or null or ``False`` value.

    >>> import re
    >>> from structs import empty
    >>>
    >>> author = 'Ὅμηρος'
    >>> item = 'Year unknown.'
    >>> year = (re.search(r'(?i)year\s*(\d+)', item) or empty).group(1)
    >>> sigil = ' '.join(filter(None, (author, year)))
    >>> print(sigil)
    Ὅμηρος
    '''
    text = ''

    def __bool__(*args, **kwargs):
        return False

    def __getattr__(self, *args, **kwargs):
        return None

    def __getitem__(*args, **kwargs):
        return None

    def get(*args, **kwargs):
        return ''

    def group(*args, **kwargs):
        return ''

    def write(*args, **kwargs):
        return 0

class Void:
    '''
    Structure for returning itself except from returning
    ``False`` when tested for its boolean value.
    '''
    def __bool__(*args, **kwargs):
        return False

    def __call__(self, *args, **kwargs):
        return self

    def __getattr__(self, *args, **kwargs):
        return self

    def __getitem__(self, *args, **kwargs):
        return self

empty = Empty()
void = Void()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
