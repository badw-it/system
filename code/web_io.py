# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
Base class and basic functions for the model of a web application, including the
issue of authentication. The model NN/gehalt.py exemplifies how to use it.
'''
import copy
import hashlib
import json
import os
import regex as re # needed for finditer with timeout.
import secrets
import shutil
import ssl
import subprocess
import sys
import time
import traceback
import typing as ty
import xml.etree.ElementTree as ET
from collections import defaultdict
from collections import deque
from functools import partial
from html import unescape
from urllib.parse import parse_qsl, quote, unquote, urljoin, urlsplit, urlencode
from urllib.request import Request, urlopen

import bottle

import fs
import sql_io
import sys_io
import xmlhtml
from parse import anon_ip, sortday, sortxtext
from structs import DefaultkeyDict

HEADERS = {
        'Accept': '*/*',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en,en-US;q=0.5',
        'Connection': 'keep-alive',
        'DNT': '1',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/109.0',
        }

TAG_RE: 're.Pattern[str]' = re.compile(r'''<('[^']*'|"[^"]*"|[^>])*>''')

class IO:
    '''
    Base class for the model (gehalt) of a web application.
    '''
    def __init__(self, urdata: 'list[str]', program_path: str) -> None:
        '''
        Aggregate a configuration object from :param:`urdata`, which may contain
        one or more absolute paths to files. Each of these files has to have the
        structure of ini-files as understood by :module:`configparser`.

        From this configuration object, take the basic attributes for the object
        initialized here.

        :param program_path: absolute path to the python file that is called for
            running the website; used to resolve those relative paths – given in
            the configuration – which are meant as relative to the folder of the
            aforementioned python file.
        '''
        self.startsecond = round(time.time())

        # For the use in templates (``db.ET`` etc.) without import or parameter:
        self.ET = ET
        self.e = xmlhtml.escape
        self.q = quote
        self.re = re
        self.xmlhtml = xmlhtml

        self.urdata = urdata
        self.config = config = fs.get_config(urdata)
        self.server_name = config['connection']['server']
        self.host = config['connection']['host']
        self.port = config['connection'].getint('port')
        self.debug = config['connection'].getboolean('debug')
        self.https = config['connection'].getboolean('https')
        self.paths = fs.get_abspaths(config, {
                'paths_from_program_folder': program_path,
                'paths_from_config_folder': urdata[0],
                })
        self.paths['templates'] = (
                self.paths['templates']
                    if isinstance(self.paths['templates'], list) else
                [self.paths['templates']])
        self.content_path = self.paths['content'] + os.sep
        self.page_id = config['ids']['page']
        self.auth_in_page_id = config['ids'].get('auth_in_page', '')
        self.lang_id = config['ids']['lang']
        self.lang_ids = config['ids'].getlist('langs')
        self.glosses = read_glosses(
                [self.paths['glosses']],
                config['ids'].get('glosses_key_lang', self.lang_id),
                )
        if 'modes' in config and config['modes'].getboolean('sessioning'):
            self.auth_init()

    def auth(
            self,
            request: 'bottle.LocalRequest',
            ) -> 'tuple[None|int, None|str, set[str]]':
        '''
        For the web request :param:`request` and the belonging session, get
        the stored ID, name and the set of roles of the user. If no session
        is registered or no user ID is known, return ``None``, ``None`` and
        the empty set instead.

        This is used to decide if a request comes from a user who is logged
        in and has the required roles.
        '''
        try:
            user_id = request.environ['beaker.session']['user_id']
            return (
                    user_id,
                    self.auth_data[user_id]['name'],
                    self.auth_data[user_id]['roles'],
                    )
        except: # Sic.
            return None, None, set()

    def auth_in(self, user_name: str, password: str) -> 'None|int':
        '''
        For the combination of :param:`user_name` and :param:`password`, if
        this combination can be verified, return the belonging user ID; but
        if the combination is not known, return ``None``.

        This is used to perform the login of a user.
        '''
        time.sleep(2)
        try:
            user_id = next(
                    user_id for user_id in self.auth_data
                    if user_name == self.auth_data[user_id]['name'] )
            assert self.auth_data[user_id]['hash'] ==\
                    auth_hash(password, self.auth_data[user_id]['salt'])
            return user_id
        except: # Sic.
            return None

    def auth_init(self) -> None:
        '''
        Setup the folder for storing session information.

        If in the configuration under the section “paths_from_config_folder” (or
        under the section “paths_from_program_folder”) a key “auth_doc” could be
        found, take its value as path to a file and read the file content as the
        authentication data of the app, structured as JSON as follows::

            [
                {
                "id": 1,
                "name": "NN",
                "hash": [0, 255, 162, 54, ...],
                "salt": [1, 223, 134, 99, ...],
                "roles": ["inspector", "editor", "redactor", "administrator"]
                },
                {
                "id": 7,
                "name": "NNNN",
                "hash": [77, 231, 145, 82, ...],
                "salt": [12, 129, 167, 53, ...],
                "roles": ["inspector", "editor"]
                }
            ]

        The hash is a hash of the password obtained with :func:`.auth_hash`. The
        hash and the salt are one list of integers between 0 and 255 each. These
        lists represent bytes in an encoding-independent manner.
        '''
        self.config['session']['session.data_dir'] =\
                fs.pave_dir(self.paths['session.data_dir'])
        self.delete_sessions = subprocess.Popen([
                sys.executable,
                fs.join(__file__, '../web_io_delete_sessions.py'),
                str(os.getpid()),
                self.urdata[0],
                self.urdata[1],
                ])
        self.auth_data = {}
        if 'auth_doc' in self.paths:
            with open(self.paths['auth_doc'], 'rb') as file:
                auth_doc = json.load(file)
        else:
            print('Path of auth_doc not in config, auth_data not read from it.')
            return
        self.auth_next_id = auth_doc['next_id']
        assert isinstance(self.auth_next_id, int)
        auth_data = auth_doc['data']
        assert isinstance(auth_data, list)
        for user in auth_doc['data']:
            assert isinstance(user, dict)
            assert isinstance(user['id'], int)
            assert isinstance(user['name'], str)
            assert isinstance(user['salt'], list)
            assert isinstance(user['hash'], list)
            assert all( isinstance(num, int) for num in user['salt'] )
            assert all( isinstance(num, int) for num in user['hash'] )
            assert isinstance(user['roles'], list)
            assert all( isinstance(role, str) for role in user['roles'] )
            self.auth_data[user['id']] = {
                    'name': user['name'],
                    'salt': user['salt'],
                    'hash': user['hash'],
                    'roles': set(user['roles']),
                    }

    def auth_new(
            self,
            auth_form: 'dict[str, str]',
            request_user_id: int,
            request_roles: 'set[str]',
            lang_id: str,
            ) -> str:
        '''
        Change user names or passwords or add users according to the values from
        :param:`auth_form` – if the current userʼs ID (:param:`request_user_id`)
        or the current userʼs roles (:param:`request_roles`) permit the change.
        '''
        note = ''
        auth_tree = defaultdict(dict)
        auth_data = copy.deepcopy(self.auth_data)
        auth_next_id = self.auth_next_id
        for key in auth_form.keys():
            value = getattr(auth_form, key)
            # ``for key, value``… may yield wrong encoding; we need ``getattr``.
            user_id_str, key = key.split('-', 1)
            user_id = int(user_id_str)
            auth_tree[user_id][key] = value
        try:
            for user_id, user in sorted(auth_tree.items(), reverse = True):
                # Reverse so that the new ones (with ``user_id == 0``) are last.
                if user.get('delete', '') == 'delete':
                    if 'administrator' in request_roles and\
                            user_id != request_user_id: # Donʼt delete yourself.
                        auth_data.pop(user_id, None)
                    continue
                password = user['password']
                password_repeated = user['password_repeated']
                name = user['name']
                assert name, self.glosses['name_is_empty'][lang_id]
                assert name == name.strip(),\
                        self.glosses['name_starts_or_ends_with_space'][lang_id]
                assert name not in {
                        other_user['name'] for other_user_id, other_user
                        in auth_data.items() if other_user_id != user_id
                        }, self.glosses['name_not_unique'][lang_id]
                if user_id not in auth_data:
                    if not (user_id == 0 and 'administrator' in request_roles):
                        continue
                    # It is a new user and the permission to make one is given.
                    assert password and password_repeated,\
                            self.glosses['password_empty']
                    # Password and roles are then set below.
                    user_id = auth_next_id
                    auth_next_id += 1
                    auth_data[user_id] = {'name': name}
                elif name != auth_data[user_id]['name']:
                    # It is a new name for an existing user.
                    if not 'administrator' in request_roles:
                        continue
                    auth_data[user_id]['name'] = name
                if password and password_repeated:
                    if 'administrator' not in request_roles and (
                            user_id != request_user_id or
                            user_id not in auth_data
                            ):
                        continue
                    assert password == password_repeated,\
                            self.glosses['password_repetition_differs'][lang_id]
                    auth_data[user_id]['salt'] = auth_salt()
                    auth_data[user_id]['hash'] = auth_hash(
                            password, auth_data[user_id]['salt'])
                if 'administrator' in request_roles:
                    auth_data[user_id]['roles'] = {
                            'i': {'inspector'},
                            'ie': {'inspector', 'editor'},
                            'ier': {'inspector', 'editor', 'redactor'},
                            'iera': {'inspector', 'editor', 'redactor',
                                'administrator'},
                            }[user['roles']]
        except: # Sic.
            note = f'{user}\n' + traceback.format_exc().rsplit(
                    'AssertionError:', 1)[-1].strip()
        else:
            self.auth_next_id = auth_next_id
            self.auth_data = auth_data
            auth_doc = {
                    'next_id': auth_next_id,
                    'data': [
                            {
                                'id': user_id,
                                'name': user['name'],
                                'hash': user['hash'],
                                'salt': user['salt'],
                                'roles': tuple(user['roles']),
                            }
                            for user_id, user in auth_data.items()
                        ],
                    }
            with open(self.paths['auth_doc'], 'w', encoding = 'utf-8') as file:
                json.dump(auth_doc, file)
        return note

    def delete_row(self, table_name: str, row_id: 'int|str') -> None:
        '''
        From the database table :param:`table_name`, delete the row that has the
        ID :param:`row_id`.

        .. important::
            See the conventions told in the docstring of :meth:`describe_table`.
        '''
        table_name, cols, idcol_name = self.get_table_info(table_name)
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(rf'''
                    delete from `{table_name}` where `{idcol_name}` = %s
                    ''', (row_id,))
            con.commit()
        return None

    def describe_table(
            self,
            table_name: str,
            ) -> 'dict[str, dict[str, bool|float|int|str]]':
        '''
        Get a description of each column of the table :param:`table_name` in the
        database ``self.config['db']['name']``.

        .. important::
            There are some extra conventions for the input system:

            - One and only one column has to be an auto-incremented primary key!
              This is highly recommendable in general anyways and necessary here
              so that the table can be changed with ease and exactness.
            - Every column must be nullable or have a default value!
            - All strings must be saved as escaped for XML! That means:

              - literal “&” must be represented as “&amp;”.
              - literal “<” must be represented as “&lt;”.
              - literal “>” must be represented as “&gt;”.

              This saves the burdensome distinction between raw and escaped text
              as well as much escaping and unescaping during in- and output, but
              requires extra effort only for cases which are rarer or can run in
              the background: import, export and indexing (for text search).
            - If a column has to be uneditable – e.g. an automatically generated
              column – its name must start with a “_”, otherwise it mustnʼt!
            - In INFORMATION_SCHEMA.COLUMNS.COLUMN_COMMENT, the initial order of
              sorting may be given with “(asc)” or “(desc)”.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            return {
                    row['col_name']: {
                        'comment': row['col_comment'],
                        'date?':
                                True if '(date)' in row['col_comment']
                                else False,
                        'enum': row['col_type']
                                .removeprefix("enum('")
                                .removesuffix("')")
                                .split("','")
                            if row['data_type'] == 'enum' else False,
                        'id?':
                            True if (
                                row['col_key'].lower() == 'pri'
                                and 'auto_increment' in row['col_extra']
                                )
                            else False,
                        'name': row['col_name'],
                        'nullable?':
                                True if row['col_nullable'].lower() == 'yes'
                                else False,
                        'pos': row['col_pos'] - 1,
                        'reftable_name': row['reftable_name'] or '',
                        'refcol_name': row['refcol_name'] or '',
                        'sort':
                            'asc' if '(asc)' in row['col_comment']
                            else 'desc' if '(desc)' in row['col_comment']
                            else '',
                        'table_name': table_name,
                        'text?':
                            True if row['data_type'] in {
                                'char',
                                'longtext',
                                'mediumtext',
                                'text',
                                'tinytext',
                                'varchar',
                                }
                            else False,
                        }
                    for row in con.geteach(r"""
                            DISTINCT -- needed e.g. if columns are SERIAL and PRIMARY.
                                cols.COLUMN_KEY as col_key,
                                cols.COLUMN_NAME as col_name,
                                cols.COLUMN_COMMENT as col_comment,
                                cols.COLUMN_TYPE as col_type,
                                cols.DATA_TYPE as data_type,
                                cols.EXTRA as col_extra,
                                cols.IS_NULLABLE as col_nullable,
                                cols.ORDINAL_POSITION as col_pos,
                                key_cols.REFERENCED_TABLE_NAME as reftable_name,
                                key_cols.REFERENCED_COLUMN_NAME as refcol_name
                            from INFORMATION_SCHEMA.COLUMNS AS cols
                            left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS key_cols
                                on key_cols.TABLE_SCHEMA = %s
                                and key_cols.TABLE_NAME = %s
                                and key_cols.COLUMN_NAME = cols.COLUMN_NAME
                            where cols.TABLE_SCHEMA = %s
                            and cols.TABLE_NAME = %s
                            order by cols.ORDINAL_POSITION
                            """,
                            self.config['db']['name'], table_name,
                            self.config['db']['name'], table_name,
                            )
                    }

    def get_table_config_json(
            self,
            table_config: 'dict[str, bool|int|str|List]' = {},
            table_config_in_text: bool = True,
            text: 'bytes|str' = '',
            encoding: str = 'utf-8',
            table_config_re: 're.Pattern[str]' = re.compile(
                r"^[^<]*<[^>]*?data-table_config_json\s*=\s*'([^']*)"),
            unescape: 'None|ty.Callable[[str], str]' = unescape,
            lang_id: str = '',
            indexinfolink: str = '',
            **_: 'ty.Any',
            ) -> 'tuple[dict, str]':
        '''
        Return a datatable configuration – see http://www.datatables.net/ – both
        as a dictionary and as a JSON dump of this dictionary.

        - Start with a default config built up within this function.
        - Update this config from the dictionary :param:`table_config`.
        - Update this config further, if:

          - :param:`table_config_in_text` is ``True``.
          - :param:`text` is not empty. If given as bytes (and not as a string),
            it will be decoded with :param:`encoding` first.
          - :param:`table_config_re` finds a match in :param:`text`. The text of
            the match will be unescaped by :param:`unescape` (if given), read as
            JSON, turned into a dictionary and used to update the configuration.

        .. note::
            The entry “language” of the configuration is updated separately (not
            simply overwritten) with any entry “language” found in the mentioned
            dictionaries. So you can set certain expressions and use the default
            in all other cases.

        .. note::
            If you do not give an entry “columns” in the mentioned dictionaries,
            it is derived automatically from the HTML table that is to be made a
            datatable.

        :param lang_id: must be a language shortcut like “en” for ‘English’ that
            is used in ``self.glosses``. Here used for selecting the expressions
            in the entry “language” of the configuration.
        :param indexinfolink: if given, must be an absolute or relative URL that
            leads to a page with info about the datatable at hand.
        :param _: catches any other keyword arguments so that the function might
            be called simply with ``(**kwargs)`` if ``kwargs`` contains at least
            the necessary entries (mentioned above) – but it may contain further
            entries.

        :raises json.decoder.JSONDecodeError: if the string which was found with
            :param:`table_config_re` canʼt be read as JSON.
        :raises ValueError: if the object into which the JSON is turned canʼt be
            used as the argument for a :meth:`dict.update`.
        '''
        if isinstance(text, bytes):
            text = text.decode(encoding)
        config = { # No default for “columns”! Only then they would be deferred.
                'deferRender':          True,
                'dom':                  'plitp',
                'lengthMenu':           [100, 200, 300, 400, 500],
                'my_th_no_case':        'a=A',
                'my_th_no_case_title':  self.glosses['no_case'][lang_id],
                'my_th_no_order':       'x y=y x',
                'my_th_no_order_title': self.glosses['no_order'][lang_id],
                'my_th_regex':          '*?',
                'my_th_regex_title':    self.glosses['regex'][lang_id],
                'my_th_search':         self.glosses['search'][lang_id] + (
                    f''' <a class="key" href="{indexinfolink}">'''
                    f'''{self.glosses['user_guide'][lang_id]}</a>'''
                    if indexinfolink else ''
                    ),
                'order':                [[0, 'asc'],],
                'pageLength':           100,
                'paging':               True,
                'stateSave':            False,
                # ``False`` because it interferes with the state-saving URLs.
                # Moreover it can be difficult to return to the default sorting.
                }
        vocab = self.get_table_vocab(lang_id)
        if table_config:
            config.update(table_config)
            if 'language' in table_config:
                vocab.update(table_config['language'])
        if text and table_config_in_text:
            m = table_config_re.search(text)
            if m:
                text = m.group(1)
                if unescape:
                    text = unescape(text)
                table_config_from_text = json.loads(text)
                config.update(table_config_from_text)
                if 'language' in table_config_from_text:
                    vocab.update(table_config_from_text['language'])
        config['language'] = vocab
        return (
                config,
                json.dumps(config, cls = JSONEncoder, separators = (',', ':')),
                )

    def get_table_info(
            self,
            table_name: str,
            ) -> 'tuple[str, dict[str, dict[str, bool|float|int|str]], str]':
        '''
        Return a description of the database table :param:`table_name`:

        - :param:`table_name`, but without backslashes and backticks.
        - descriptions of the columns of the database table.
        - the name of the ID column, likewise without backslashes and backticks.

        .. important::
            This table must have exactly one column which is an auto-incremented
            primary key. Otherwise an ``AssertionError`` is thrown.
        '''
        table_name = table_name.replace('\\', '').replace('`', '')
        cols = self.describe_table(table_name)
        idcol_names = [ name for name, col in cols.items() if col['id?'] ]
        n = len(idcol_names)
        assert n == 1, f'We need one auto-incremented primary key; found {n}.'
        idcol_name = idcol_names[0].replace('\\', '').replace('`', '')
        return table_name, cols, idcol_name

    def get_table_vocab(
            self,
            lang_id: str,
            ) -> 'dict[str, str|dict[str, str]]':
        '''
        Return the vocabulary for a datatable configuration. The vocabulary will
        be put into the entry “language” of said configuration.
        '''
        vocab =  {
                "aria": {
                    "sortAscending": ": " +
                        self.glosses["aria_sortAscending"][lang_id],
                    "sortDescending": ": " +
                        self.glosses["aria_sortDescending"][lang_id],
                    },
                "emptyTable": self.glosses["emptyTable"][lang_id],
                "info": self.glosses["info"][lang_id],
                "infoEmpty": self.glosses["infoEmpty"][lang_id],
                "infoFiltered": "(" +
                    self.glosses["infoFiltered"][lang_id] + ")",
                "infoPostFix": self.glosses["infoPostFix"][lang_id],
                "lengthMenu": self.glosses["lengthMenu"][lang_id],
                "loadingRecords": self.glosses["loadingRecords"][lang_id] +
                    " …",
                "paginate": {
                    "first": self.glosses["paginate_first"][lang_id],
                    "previous": self.glosses["paginate_previous"][lang_id],
                    "next": self.glosses["paginate_next"][lang_id],
                    "last": self.glosses["paginate_last"][lang_id],
                    },
                "processing": self.glosses["processing"][lang_id] + " …",
                "search": self.glosses["search"][lang_id] + ": ",
                "thousands": "",
                "zeroRecords": self.glosses["zeroRecords"][lang_id],
                }
        return vocab

    def get_tables(self) -> 'Generator[dict[str, bytes|float|int|str|None]]':
        '''
        Yield each table from ``self.config['db']['name']``.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            for row in con.geteach(r"""
                    * from information_schema.TABLES where TABLE_SCHEMA = %s
                    order by TABLE_NAME
                    """, self.config['db']['name']):
                yield row

    def get_text_and_template_name(
            self,
            site_id: str,
            page_id: str,
            template_name: str = 'text.tpl',
            template_re: 're.Pattern[bytes]' = re.compile(
                rb'''data-template\s*=\s*["']?([^"']+)'''),
            ) -> 'tuple[bytes, str]':
        '''
        Return the bytes of the file and the name of the template which are used
        to build a certain webpage.

        The path of the file is put together concatenating ``self.content_path``
        and :param:`site_id` – which is just empty if your site does not consist
        of several subsites – and :param:`page_id`.

        The name of the template is extracted from the attribute `data-template`
        of the first element of the XML file. If such an attribute is not found,
        :param:`template_name` is assumed as default.
        '''
        page_id += '.xml'
        path = os.path.abspath(
                os.path.join(self.content_path, site_id, page_id))
        if path.startswith(self.content_path):
            try:
                with open(path, 'rb') as infile:
                    text = infile.read()
            except OSError:
                text = b''
        else: # ``path`` transgresses ``self.content_path``.
            text = b''
        if text[:3] == b'\xef\xbb\xbf':
            text = text[3:]
        match = template_re.search(text.split(b'>', 1)[0])
        return (text, match.group(1).decode() if match else template_name)

    def insert_row(
            self,
            table_name: str,
            ) -> 'dict[int|str, dict[str, bytes|float|int|str]]':
        '''
        Into the database table :param:`table_name`, insert a new row. Return it
        in a form ready to be sent to a datatable.

        .. important::
            See the conventions told in the docstring of :meth:`describe_table`.
        '''
        table_name, cols, idcol_name = self.get_table_info(table_name)
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(rf"""
                    insert into `{table_name}` (`{idcol_name}`) values (null)
                    """)
            con.commit()
            lastrowid = cur.lastrowid
            row = con.get(
                    rf"* from `{table_name}` where `{idcol_name}` = %s",
                    lastrowid)
        return self.make_tablerow(row, cols, idcol_name)

    def log_access(
            self,
            request: 'bottle.LocalRequest',
            excluded_user_agents: 'set[str]' = {},
            ) -> None:
        '''
        :param excluded_user_agents: Useful for excluding your own bots, whereas
            other bots might be named irrecognizable and must be excluded via IP
            filtering a posteriori.
        '''
        user = (request.environ.get('HTTP_USER_AGENT') or '?').split(None, 1)[0]
        if user not in excluded_user_agents:
            self.access_log.write(
                    f'{anon_ip(request.remote_addr)}█{user}█{request.path}')

    def make_tablerow(
            self,
            row: 'dict[str, bytes|float|int|str]',
            cols: 'dict[str, dict[str, bool|float|int|str]]',
            idcol_name: str,
            ) -> 'dict[int|str, dict[str, bytes|float|int|str]]':
        '''
        Turn the database record :param:`row` into a datatable row.
        '''
        tablerow = { cols[col_name]['pos']: {
                '_':
                    '' if val is None
                    else val,
                's':
                    sortday(val or '') if cols[col_name]['date?']
                    else sortxtext(val or '') if cols[col_name]['text?']
                    else val,
                }
                for col_name, val in row.items() }
        tablerow['DT_RowId'] = row[idcol_name]
        return tablerow

    def select_rows(
            self,
            table_name: str,
            where: 'tuple[str, str, bytes|float|int|str]' = (),
            ) -> 'Generator[dict[int|str, dict[str, bytes|float|int|str]]]':
        '''
        From the database table :param:`table_name`, yield every row meeting the
        condition :param:`where`, and yield every row in a form ready to be sent
        to a datatable. The condition :param:`where` may be empty. In that case,
        it will be ignored. Otherwise, it must be a tuple consisting of:

        - the name of a column of the table :param:`table_name`.
        - one of the following pivots: '=', '<>', '<', '>', '<=', '>='
        - a value – it will be sql-escaped and used as the standard to which the
          content of the given column will be compared via the given pivot.
        '''
        table_name, cols, idcol_name = self.get_table_info(table_name)
        query = rf"* from `{table_name}`"
        if where:
            wherecol, whererel, whereval = where
            wherecol = wherecol.replace('\\', '').replace('`', '')
            whererel = whererel if whererel in {
                    '=', '<>', '<', '>', '<=', '>='} else '='
            args = [rf"{query} where `{wherecol}` {whererel} %s", whereval]
        else:
            args = [query]
        with sql_io.DBCon(self.connect()) as (con, cur):
            for row in con.geteach(*args):
                yield self.make_tablerow(row, cols, idcol_name)

    def update_cell(
            self,
            table_name: str,
            col_name: str,
            row_id: 'int|str',
            val: 'None|bytes|float|int|str',
            trim_re: 're.Pattern[str]' = re.compile(r'(<br\s*/?>)+$'),
            ) -> 'dict[int|str, dict[str, bytes|float|int|str]]':
        '''
        Into the database table :param:`table_name` – column :param:`col_name` –
        row :param:`row_id`, insert :param:`val`.

        Return :param:`row_id` and the updated row. (The former is also returned
        in order to make the return-structure equal to :meth:`insert_row`.)

        .. important::
            See the conventions told in the docstring of :meth:`describe_table`.
        '''
        table_name, cols, idcol_name = self.get_table_info(table_name)
        col_name = col_name.replace('\\', '').replace('`', '')
        assert col_name != idcol_name,\
                f'“{col_name}” is the ID column and hence not updateable.'
        col = cols[col_name]
        if isinstance(val, str):
            val = trim_re.sub('', val)
        val = (
                None if val == '' and col['nullable?']
                else val
                )
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(rf"""
                    update `{table_name}`
                    set `{col_name}` = %s
                    where `{idcol_name}` = %s
                    and (
                        (`{col_name}` is not null and `{col_name}` <> %s) or
                        (`{col_name}` is not null and %s is null) or
                        (`{col_name}` is null and %s is not null)
                    )""", (val, row_id, val, val, val))
            con.commit()
            row = con.get(
                    rf"* from `{table_name}` where `{idcol_name}` = %s", row_id)
        return self.make_tablerow(row, cols, idcol_name)

class JSONEncoder(json.JSONEncoder):
    def encode(self, item):
        item = json.JSONEncoder.encode(self, item)
        if isinstance(item, str):
            item = item.replace('<', '\\u003c').replace('>', '\\u003e')
        return item

def auth_hash(password: str, salt: 'list[int]') -> 'list[int]':
    '''
    Generate a hash from :param:`password` and :param:`salt`. The latter must be
    a list of integers between 0 and 255 (inclusively), as it is produced by the
    :func:`.auth_salt`.
    '''
    return list(hashlib.scrypt(
            password.encode(encoding = 'utf-8'),
            salt = bytes(salt),
            n = 8,
            r = 8,
            p = 8,
            dklen = 128,
            ))

def auth_salt() -> 'list[int]':
    '''
    Generate salt for the password hashing as done by :func:`.auth_hash`.
    '''
    return [ secrets.randbelow(256) for i in range(128) ]

def fulltext(
        pid: int,
        connect: 'ty.Callable[[], mysql.Connection]',
        before_loop_sqls: 'tuple[str]',
        start_url: str,
        insert: 'ty.Callable[[str, str, mysql.Connection, mysql.Cursor], None]',
        headers: 'dict[str, str]' = {},
        urls_considered_for_their_text_re: 're.Pattern[str]' = re.compile(r'.'),
        urls_considered_for_further_urls_re: 're.Pattern[str]' = re.compile(
            r'.'),
        extract_url: 'ty.Callable[[str, dict[str, str]], str]' =
            lambda tag, attrs: attrs.get('href', '') if tag == 'a' else '',
        default_path: str = '',
        urls_not_considered_re: 're.Pattern[str]' = re.compile(
            r'(?i)\.(jpe?g|png|tiff?|pdf)([?#]|$)'),
        pause_between_pages_in_secs: float = 0.1,
        after_loop_sqls: 'tuple[str]' = (),
        pause_between_cycles_in_secs: float = 2,
        ) -> None:
    '''
    Make or refresh a fulltext table of the database for searching – do it in an
    endless loop as follows:

    - Start an endless loop.
    - Exit, when no process with :param`pid` is running anymore – this should be
      the process ID of the process having started this function.
    - Get a database connection and cursor with :param:`connect`.
    - Execute :param:`before_loop_sqls`, which means, as a rule, creating one or
      more tables for the fulltext collection and making indices. – Commit.
    - Iterate over webpages – starting at :param:`start_url`, which has to be an
      absolute URL. It is described in detail below which pages are included (or
      excluded).
    - For each included page: Pass its URL, its HTML content as well as both the
      connection the and cursor to :param:`insert`, which extracts rows from the
      HTML, puts them into the database and leaves a flag marking them as ‘new’.
    - Further details about the iteration over the webpages:

      - Send each request with :param:`headers` as HTTP headers.
      - Starting at :param:`start_url`, include this page and further pages only
        if the URL matches :param:`urls_considered_for_their_text_re`.
      - Collect any further URLs from a certain page only if the URL of the page
        matches :param:`urls_considered_for_further_urls_re`.
      - Extract more URLs (by passing the tag and any attributes of each element
        to :param:`extract_url`).
      - Absolutize the extracted URL with the URL of the current page, and strip
        trailing slashes.
      - If the resulting URL has no path, assume :param:`default_path`.
      - Skip any URL that has not the same domain as :param:`start_url` – “www.”
        may or may not occur, but the subdomains must also be the same.
      - Skip any URL that matches :param:`urls_not_considered_re`.
      - Sleep :param:`pause_between_pages_in_secs` seconds to reduce load.

    - Execute :param:`after_loop_sqls`: which means, as a rule, deleting any old
      record that was not updated and marking all remaining records as old. Then
      commit.
    - Sleep :param:`pause_between_cycles_in_secs` seconds to reduce load.
    - Start the loop anew.
    '''
    def adapt_even_eval_url(
            scheme: str, netloc: str, path: str, query: str, fragment: str,
            ) -> 'tuple[str, str, bool, bool]':
        '''
        To be used for :func:`web_io.iter_pages`.
        '''
        if netloc[:4] == 'www.':
            netloc = netloc[4:]
        if netloc != start_netloc:
            return '', '', False, False
        path = path.rstrip('/')
        if not path:
            path = default_path
        if query:
            query = '?' + query
        url = f'{scheme}://{netloc}{path}{query}'
        if urls_not_considered_re.search(url):
            return '', '', False, False
        return (
                url,
                path,
                bool(urls_considered_for_their_text_re.search(url)),
                bool(urls_considered_for_further_urls_re.search(url)),
                )

    default_path = '/' + default_path.strip('/')
    start_netloc = urlsplit(start_url).netloc
    if start_netloc[:4] == 'www.':
        start_netloc = start_netloc[4:]
    while True:
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        with sql_io.DBCon(connect()) as (con, cur):
            for sql in before_loop_sqls:
                cur.execute(sql)
            con.commit()
        for url, text, note in iter_pages(
                start_url,
                adapt_even_eval_url,
                request_kwargs = {'headers': headers},
                extract_url = extract_url,
                pause = pause_between_pages_in_secs,
                ):
            if url and text:
                try:
                    with sql_io.DBCon(connect()) as (con, cur):
                        insert(url, text, con, cur)
                except Exception as e:
                    if note:
                        note += '\n'
                    note += fs.string(e)
            if note:
                print(f'{note} – {url}')
                note = ''
        with sql_io.DBCon(connect()) as (con, cur):
            for sql in after_loop_sqls:
                cur.execute(sql)
            con.commit()
        time.sleep(pause_between_cycles_in_secs)

def get_filtersplits(
        term: str,
        split_re: 're.Pattern[str]',
        left_context: int = 100,
        right_context: int = 100,
        ) -> 'Generator[str]':
    '''
    Split the raw-text :param:`term` with :param:`split_re` and return the parts
    as follows:

    - Every second part, i.e. every match of :param:`split_re`, is first escaped
      and then put into a mark element in order to highlight the match.
    - Of the first part, no more than its last :param:`left_context` characters,
      of the last part, no more than its first :param:`right_context` characters
      are kept. The part is escaped.
    - Of any remaining part (i.e. context **between** matches), no more than its
      first :param:`right_context` and its last :param:`left_context` characters
      are kept. The part is escaped.

    A context part (first, last or in-between) is only cut if its length exceeds
    the allowed context, of course; and the cut is indicated with “⟦…⟧”.
    '''
    inner_context = left_context + right_context
    splits = split_re.split(term)
    splits_len = len(splits)
    for n, term in enumerate(splits):
        yield (
                f'<mark>{xmlhtml.escape(term)}</mark>'
                    if n % 2
                else xmlhtml.escape(f'⟦…⟧{term[-left_context:]}')
                    if (n == 0) and (len(term) > left_context)
                else xmlhtml.escape(f'{term[:right_context]}⟦…⟧')
                    if (n == splits_len - 1) and (len(term) > right_context)
                else xmlhtml.escape(
                        f'{term[:right_context]}⟦…⟧{term[-left_context:]}')
                    if len(term) > inner_context
                else xmlhtml.escape(term)
                )

def get_filterterms(
        term: str,
        adapt_regex: 'ty.Callable[[str], str]' = lambda term: f"(?is)({term})",
        trim_after_re: 're.Pattern[str]' = re.compile('%_+'),
        trim_before_re: 're.Pattern[str]' = re.compile('_+%'),
        trim_re: 're.Pattern[str]' = re.compile('%%+'),
        ) -> 'tuple[str, str, re.Pattern[str]]':
    '''
    From :param:`term`, get a term normalized with regard to the characters that
    are special in a SQL text search (“_” and “%”) and return:

    - the normalized term.
    - a regular-expression-pattern that is equivalent to this term. This pattern
      is then adapted with :param:`adapt_regex`. Very important: This adaptation
      must put the search term into round brackets (because it will be used with
      re.split).
    - this regular-expression-pattern compiled.
    '''
    term = trim_re.sub(
            '%', trim_before_re.sub(
                '%', trim_after_re.sub(
                    '%', term.strip('_%'))))
    term_re_str = re.escape(term).replace('_', '.').replace('%', '.*?')
    term_re_str = adapt_regex(term_re_str)
    term_re = re.compile(term_re_str)
    return term, term_re_str, term_re

def iter_pages(
        start_url: str,
        adapt_even_eval_url:
            'ty.Callable[[str, str, str, str, str], tuple[str, str, bool, bool]]',
        request_kwargs: 'dict[str, ty.Any]' = {},
        urlopen_kwargs: 'dict[str, ty.Any]' = {},
        extract_url: 'ty.Callable[[str, dict[str, str]], str]' =
            lambda tag, attrs: attrs.get('href', '') if tag == 'a' else '',
        pause: float = 0.1,
        allowed_schemes: 'set[str]' = {'http', 'https'},
        ) -> 'Generator[tuple[str, str, str]]]':
    '''
    Iter through webpages yielding for each page its URL, its content as well as
    any error message as follows:

    - Start with :param:`start_url`; this is the first URL.
    - Split the URL into scheme, netloc, path, query, fragment – this is done by
      :func:`urllib.parse.urlsplit`.
    - If the scheme is not one of the schemes in :param:`allowed_schemes`, leave
      this URL out (and terminate, if it was the last one).
    - The URL can be opened only in pure ASCII, thus:

      - IDNA-encode the netloc.
      - Unquote and quote any path segment, query item and fragment identifier.

    - Apply :param:`adapt_even_eval_url` to the URL (scheme, netloc, path, query
      and fragment identifier) in order to get:

      - the URL in the form that should be opened and yielded.
      - the URL in the evened-out form that is memorized – so that pages are not
        accessed multiple times.
      - a boolean flag telling whether the URL should be opened, and the URL and
        content should be yielded: the flag “for text”.
      - a boolean flag telling whether the URL should be opened. and the content
        scanned for further URLs to be opened: the flag “for further URLs”.

      It may be convenient to write the function :param:`adapt_even_eval_url` as
      a closure in the function calling :func:`iter_pages` – so that environment
      parameters can be used in the closure, e.g. ``start_netloc`` in this basic
      example::

          from urllib.parse import urlsplit

          def my_func(start_url: str) -> None:

              def adapt_even_eval_url(
                      scheme: str,
                      netloc: str,
                      path: str,
                      query: str,
                      fragment: str,
                      ) -> 'tuple[str, str, bool, bool]':
                  netloc = netloc.removeprefix('www.')
                  if netloc != start_netloc:
                      return '', '', False, False
                  path = path.rstrip('/')
                  return (f'{scheme}://{netloc}{path}', path, True, True)

              start_netloc = urlsplit(start_url).netloc.removeprefix('www.')
              for url, text, note in web_io.iter_pages(
                      start_url, adapt_even_eval_url):
                  print(url, text, note)

    - If the evened URL has been opened already or if both flags are false, then
      go to the next URL (or terminate, if this was the last one).
    - Open the URL and read its content with :func:`read`.
    - If an exception interrupts the execution, yield: the current URL, an empty
      string (instead of the content) and the error message.
    - If the boolean flag “for text” is true, yield: the URL and the content and
      any error message.
    - If the boolean flag “for further URLs” is true, then loop over any tag and
      apply :param:`extract_url` to its tagname and attributes, which may create
      a new URL. This URL is then appended to the pipeline of URLs to be opened.
    - Pause :param:`pause` seconds.
    - Proceed with the next URL in the pipeline, if there is any, or terminate.
    '''
    urls = deque([start_url])
    evened_urls = set()
    while urls:
        try:
            note = ''
            url = urls.popleft()
            scheme, netloc, path, query, fragment = urlsplit_norm(url)
            if scheme not in allowed_schemes:
                continue
            url, evened_url, for_text, for_further_links = adapt_even_eval_url(
                    scheme, netloc, path, query, fragment)
            if evened_url in evened_urls or not (for_text or for_further_links):
                continue
            evened_urls.add(evened_url)
            doc = read(
                    url,
                    request_kwargs = request_kwargs,
                    urlopen_kwargs = urlopen_kwargs,
                    )
            if for_text:
                yield (url, doc, note)
            if for_further_links:
                parser = xmlhtml.Itemizer()
                parser.parse(doc)
                for num, tagname, attrs, *_ in parser.items:
                    if tagname and attrs:
                        new_url = extract_url(tagname, attrs)
                        if new_url:
                            new_url = urljoin(url, new_url)
                            urls.append(new_url)
        except Exception as e:
            note = fs.string(e)
            yield url, '', note
        if pause:
            time.sleep(pause)

def mark(
        old: str,
        mark: str,
        main_re: 'None|re.Pattern[str]' =
            re.compile(r'''(?s)<main('[^']*'|"[^"]*"|[^>])*>.*?</main>'''),
        timeout: int = 3,
        mark_start: str = '<mark>',
        mark_end: str = '</mark>',
        ) -> str:
    '''
    In :param:`old`, search the regular expression :param:`mark` within the span
    covered by :param:`main_re` or, if this is ``None``, within all :param:`old`
    (though cancel the search after :param:`timeout` seconds).

    Said regular expression must contain a group. The group may encompass all of
    the regular expression.

    In any matching sequence, enclose the part that matches the first group with
    :param:`mark_start` and :param:`mark_end`. Do this fragmentized if necessary
    to avoid intersecting with existent tagging.

    For this search, treat any HTML- or XML-tag as if it didnʼt exist, and treat
    any HTML- and standard XML-entity (“&#60;”, “&#x3c;”, “&lt;”, “&nbsp;” etc.)
    as the character which it represents.

    >>> mark('<main>aaa bbb<i>ccc</i>ddd eee</main>', 'b(bcc)cd')
    '<main>aaa bb<mark>b</mark><i><mark>cc</mark>c</i>ddd eee</main>'
    '''
    try:
        mark_re = re.compile(mark)
    except: # Sic.
        return old
    new = deque()
    if main_re is None:
        rest = ''
    else:
        main_match = main_re.search(old)
        if main_match is None:
            return old
        start, end = main_match.span()
        new.extend(old[:start])
        old = main_match.group()
        rest = old[end:]
    raw = unescape(TAG_RE.sub('', old))
    old = deque(old)
    at = 0
    inner = False
    try:
        for m in mark_re.finditer(raw, timeout = timeout):
            try:
                left_at, right_at = m.span(1)
            except: # Sic.
                break
            while old:
                if at == left_at and old[0] != '<':
                    new.append(mark_start)
                    inner = True
                elif at == right_at:
                    new.append(mark_end)
                    inner = False
                    break
                c = old.popleft()
                if c == '<':
                    if inner:
                        new.append(mark_end)
                    new.append(c)
                    while old:
                        c = old.popleft()
                        new.append(c)
                        if c == '>':
                            if inner:
                                new.append(mark_start)
                            break
                else:
                    new.append(c)
                    at += 1
                    if c == '&':
                        while old:
                            c = old.popleft()
                            new.append(c)
                            if c == ';':
                                break
            if inner:
                new.append(mark_end)
    except TimeoutError:
        pass
    new.extend(old)
    new.extend(rest)
    return ''.join(new)

def read(
        url: str,
        encoding: 'None|str' = '',
        errors: str = 'replace',
        uni_newline: bool = True,
        path: str = '',
        request_kwargs: 'dict[str, ty.Any]' = {},
        urlopen_kwargs: 'dict[str, ty.Any]' = {},
        parent_re: 're.Pattern[str]' = re.compile(r'/[^/]*/\.\.(?=/)'),
        ) -> 'bytes|str':
    '''
    Open the URL :param:`url`:

      - with :obj:`urllib.request.Request` and :param:`request_kwargs`,
      - and :func:`urllib.request.urlopen` and :param:`urlopen_kwargs`.

    Get the response and:

    - if :param:`path` is not empty, see it as a path and save the content there
      and **return the path in absolutized form**. The content is read chunkwise
      and not all at once into memory.
    - if :param:`encoding` is ``None``, **return the content as bytes**.
    - if :param:`encoding` is a string, however, read the content as bytes, then
      try to decode them …

      - assume :param:`encoding` as encoding, if it is not the empty string.
      - if :param:`encoding` is the empty string, try to get the encoding: first
        with :meth:`xmlhtml.get_charset`, then from the response header; if this
        fails, assume ``'utf-8'``.

      … while using the error mode :param:`errors` (as in :meth:`bytes.decode`).
      In the end, **return the content as a string**.

    :param uni_newline: If ``True`` (the default) and a string is returned, then
        a sequence of carriage-return plus newline is replaced with one newline.
    '''
    while '/../' in url:
        url = parent_re.sub(r'', url)
    if not urlopen_kwargs.get('context'):
        urlopen_kwargs['context'] = ssl.SSLContext()
    with urlopen(Request(url, **request_kwargs), **urlopen_kwargs) as response:
        if path:
            path = os.path.abspath(path)
            with open(path, 'wb') as file:
                shutil.copyfileobj(response, file)
            return path
        else:
            data: bytes = response.read()
            if encoding is None:
                return data
            elif encoding == '':
                encoding = xmlhtml.get_charset(data)\
                        or response.headers.get_content_charset()\
                        or 'utf-8'
            data: str = data.decode(encoding, errors)
            if uni_newline:
                data = data.replace('\r\n', '\n')
            return data

def read_glosses(
        glosses_paths: 'list[str]',
        default_lang_id: str,
        ) -> 'dict[str, dict[str, str]]':
    '''
    Get a dictionary of keywords mapped to a dictionary of language codes
    mapped to translations belonging to each keyword. To be used as e.g.:

    ``translation = glosses[keyword][language_id]``

    The dictionaries are of the type DefaultkeyDict; that means using the
    given example:

    - If ``keyword`` is not found, ``keyword`` itself is returned.
    - If ``keyword`` is found but ``language_id`` is not, the translation
      of :param:`default_lang_id` is returned, if available. (If not, the
      ``language_id`` itself is returned.)
    - If ``keyword`` and ``language_id`` are both found, the accompanying
      translation is returned.

    :param glosses_paths: list of filepaths to ini-style files containing
        keywords as section names and language ids as option names (keys)
        mapped to translations.
    '''
    glosses = DefaultkeyDict(default_lang_id)
    for keyword, translations in fs.get_config(glosses_paths).items():
        glosses[keyword] = DefaultkeyDict(default_lang_id)
        for lang_id, translation in translations.items():
            glosses[keyword][lang_id] = translation
    return glosses

def urlsplit_norm(url: str) -> 'tuple[str, str, str, str, str]':
    scheme, netloc, path, query, fragment = urlsplit(url)
    netloc = netloc.encode('idna').decode('ascii')
    path = '/'.join(quote(unquote(part), safe = '') for part in path.split('/'))
    query = urlencode(parse_qsl(query))
    fragment = quote(unquote(fragment), safe = '')
    return scheme, netloc, path, query, fragment

if __name__ == '__main__':
    import doctest
    doctest.testmod()
