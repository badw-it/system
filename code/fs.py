# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
Miscellaneous functions for frequent file-input and file-output operations.
'''
import base64
import configparser
import csv
import io
import os
try: import regex as re
except ImportError: import re
import subprocess
import time
import traceback
import typing as ty
import unicodedata
from collections.abc import Iterable
from collections.abc import Mapping
from json import loads

import parse

DIFF_CHAR = '-'

class ConfigParser(configparser.ConfigParser):
    '''
    Additional converters:

    - config[...].getlist('...')
    - config[...].getlist_all('...')
    - config[...].getjson('...')
    '''
    def __init__(self, dict_type = dict):
        super().__init__(
                allow_no_value = True,
                comment_prefixes = (';',),
                default_section = '',
                delimiters = ('=',),
                dict_type = dict_type,
                empty_lines_in_values = True,
                inline_comment_prefixes = None,
                interpolation = None,
                strict = True,
                converters = { # Use with ``get``-prefix, e.g. ``getlist``.
                    'list': lambda term: list(filter(None, term.splitlines())),
                    'list_all': lambda term: term.splitlines(),
                    'json': lambda term: loads(term),
                    }
                )
        # Overwrite canonicalization of option names:
        self.optionxform = lambda option: option

class Log(io.BufferedWriter):
    def __init__(
            self,
            logged_path: str,
            log_dirpath: str = '__logs__',
            log_filename: str = '',
            use_time: bool = False,
            joint: str = '.',
            max_size: int = 500_000_000,
            tell_path: bool = False,
            ) -> str:
        '''
        Make an object for saving log items.

        :param logged_path: the path to the module to be logged. As a rule, this
            is simply “__file__” in the code calling this class. – The path will
            be mentioned in the logfile and may be used to build its name – more
            on this follows.
        :param log_dirpath: path to the folder where the log should be saved. If
            it is a relative path, then it is taken as a relative path seen from
            :param:`logged_path` as follows:

            - ``'__logs__'`` leads to a folder “__logs__” that is located in the
              same folder as :param:`logged_path`.
            - ``'../__logs__'`` leads to a folder “__logs__” which is located in
              the parent of said folder.
            - etc.

        :param log_filename: the filename which the log shall get.

            If empty and :param:`use_time` is ``False``, the filename of the log
            will be derived from :param:`logged_path` by joining the name of its
            parent folder and its basename with :param:`joint`, and removing any
            extension from the basename and adding the extension `.log`.

            If empty and :param:`use_time` is ``True``, however, the filename of
            the log will be derived from the current date and time.

        :param use_time: See above.
        :param joint: See above.
        :param max_size: If not ``0``, the maximum size of the log in bytes. The
            size must at least suffice for :param:`max_mark`, encoded in UTF-8.
        :param tell_path: If ``True``, ``print`` the path of the logfile.

        >>> import sys
        >>> import fs
        >>> log = fs.Log(__file__, '../../__logs__')
        >>> sys.stderr = log
        >>> sys.stdout = log
        '''
        logged_path = os.path.abspath(logged_path)
        dirpath, filename = os.path.split(logged_path)
        now = time.strftime('%Y-%m-%d_%H.%M.%S')
        self.path = pave(get_new_path(join(
                os.path.dirname(logged_path),
                log_dirpath,
                log_filename or (
                    f'{now}.log' if use_time
                    else os.path.basename(dirpath) + joint +
                        filename.rsplit('.', 1)[0] + '.log'
                        ))))
        self.max_size = max_size
        self.write(f'''****
* encoding: UTF-8
* program: {logged_path}
* maximum logsize in bytes: {max_size}
* logstart: {now}
****
''', timed = False)
        if tell_path:
            print(f'Log: {self.path}')

    def write(
            self,
            item: 'Any',
            timed: bool = True,
            ) -> None:
        '''
        Write :param:`item` into the logfile: each of its lines into one line of
        the logfile.

        :param timed: If True, the current time is prepended to each line.
        '''
        for line in string(item).splitlines():
            line += '\n'
            if timed:
                line = f"{time.strftime('%Y-%m-%d_%H.%M.%S')}│ {line}"
            line = line.encode()
            if self.max_size:
                size = (
                        0 if not os.path.exists(self.path) else
                        os.path.getsize(self.path)
                        )
                if size >= self.max_size:
                    return
                line, _, _ = parse.trunc_utf8(line, self.max_size - size)
            with open(self.path, 'ab') as file:
                file.write(line)

def convert(
        path: str,
        soffice_exe_path: str,
        ext: str,
        infilter: str = '',
        ) -> 'tuple[str, int]':
    '''
    Convert :param:`path` with a “SOffice” executable file at the location given
    by :param:`soffice_exe_path`, which may be something like:

    ``'/Program Files/LibreOffice/program/soffice.exe'``

    The target format is given by the extension :param:`ext`, e.g. “.html”. Take
    other extensions from the list shown under “Save as” in a graphical instance
    of :param:`soffice_exe_path`. In some cases the extension has to be extended
    with a colon plus a flag that cannot be given via :param:`infilter`, e.g.::

        path, size = fs.convert(path, soffice_exe_path, '.ods:calc8')

    … in order to convert an HTML table to an Open Document Spreadsheet.

    The conversion is saved as a file with a new path, namely :param:`path` with
    :param:`ext` as extension – but:

    .. warning::
        A file already present at this new path will be **overwritten**.

    :param infilter: specifies features of the conversion, e.g. “CSV:9,34,76,1”,
        which is useful for converting from XLSX (sic) to a TSV file in UTF-8.

        - https://unix.stackexchange.com/questions/259361/specify-encoding-with-libreoffice-convert-to-csv
        - https://bugs.documentfoundation.org/show_bug.cgi?id=36313
        - https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options

    Return the path and the size of the new file.
    '''
    path = os.path.abspath(path)
    if not os.path.isfile(path):
        raise FileNotFoundError(f'No such file: {path}')
    soffice_exe_path = os.path.abspath(soffice_exe_path)
    if not os.path.isfile(soffice_exe_path):
        raise FileNotFoundError(f'No such file: {soffice_exe_path}')
    dirpath = os.path.dirname(path)
    ext = ext.lstrip('.')
    call = rf'"{soffice_exe_path}" '\
            rf'--headless '\
            rf'--invisible '\
            rf'--convert-to "{ext}" '\
            rf'--infilter="{infilter}" '\
            rf'--outdir "{dirpath}" '\
            rf'"{path}"'
    ext = ext.split(':', 1)[0]
    subprocess.call(call, shell = True)
    path = os.path.splitext(path)[0]
    path = f'{path}.{ext}'
    return path, os.path.getsize(path)

def get_abspaths(
        config: ConfigParser,
        sections: 'dict[str, str]' = {'paths': __file__},
        ) -> 'dict[str, str|list[str]]':
    '''
    Get absolute paths from the relative paths found in :param:`config`
    in sections whose names are also keys of :param:`sections`, mapped
    to a filepath. The path of its directory is taken as the base path.
    Each value is interpreted as a **list** of paths, each of which is
    indented and put onto an own new line; indentation, trailing space
    and empty lines are skipped. The list may consist of only one item.
    In this case, this item is returned instead of the list, otherwise,
    the list itself is returned as the value.

    Return a mapping of the keys given in the section to the belonging
    path or list of paths. The order is the same as in the section.
    '''
    paths = {}
    for section_name, subbasepath in sections.items():
        basepath = os.path.dirname(subbasepath)
        section = config[section_name]
        for key in section:
            value = [
                    os.path.abspath(os.path.join(basepath, path))
                    for path in section.getlist(key) ]
            paths[key] = value[0] if len(value) == 1 else value
    return paths

def get_config(
        config_paths: 'Iterator[str]',
        encoding: str = 'utf-8',
        dict_type: type = dict,
        ) -> 'ConfigParser':
    '''
    Aggregate a configuration object from :param:`config_paths`, which
    may contain zero, one or more paths of files. These files must all
    have the encoding :param:`encoding` and the structure of ini-files
    as understood by :module:`configparser`.
    The configuration is aggregated from the files in the given order,
    with the latter ones updating the former ones.
    Options (i.e. keys) may occur without value.
    Options are treated case-sensitively.
    With ``.getlist('...')``, you can obtain a list of values from one
    value consisting of several lines; indentation, trailing space and
    empty lines are skipped. Use ``.getlist_all``, if you want to keep
    empty lines.
    '''
    config = ConfigParser(dict_type)
    for path in config_paths:
        with open(path, encoding = encoding) as file:
            config.read_file(file)
    return config

def get_keys(path: str = '', term: str = '') -> 'tuple[str, str]':
    '''
    Try to read a username and a password from the file at :param:`path` or – if
    :param:`path` is empty – from :param:`term` directly. Within the string that
    is given by the file or by the parameter, the username and the password must
    be stored in a certain form described in the docstring of :func:`set_keys`.

    On any error, print the error and return two empty strings.
    '''
    try:
        if path:
            with open(os.path.abspath(path), 'rb') as file:
                term = file.read()
        len_first, access_data =\
                base64.b16decode(term.strip()).decode().split(':', 1)
        return access_data[:int(len_first)], access_data[int(len_first):]
    except Exception as e:
        print(e)
        return '', ''

def get_new_path(
        path: str,
        ext: 'None|str' = None,
        diff_char: str = DIFF_CHAR,
        diff_min: int = 0,
        parent: str = '',
        ) -> str:
    '''
    Derive an unused, normalized and absolutized path name from :param:`path`.

    The new path name is made unique by adding :param:`diff_char` as often as it
    is necessary to get an unused path and at least :param:`diff_min` times. The
    :param:`diff_char` is added to :param:`path` …

    - either at the end (just before an extension)
    - or after :param:`parent`, if this is a parent directory of :param:`path`.

    If :param:`ext` is ``None``, it is ignored, if not, any extension of the old
    :param:`path` is replaced with it – including the leading dot! So that it is
    also possible to remove the extension.

    For safer uniqueness, the new path name must be absolute – if the old one is
    not absolute, the new one is absolutized, taking the filepath of this module
    as base.

    .. important::
        - :param:`path` must be non-empty.
        - :param:`diff_char` is treated with :func:`sanitize`.
        - :param:`diff_min` must be an integer greater than or equal to ``0``.
    '''
    assert path
    assert diff_min >= 0
    diff_char = sanitize(diff_char)
    path = os.path.abspath(path)
    path, old_ext = os.path.splitext(path)
    if ext is None:
        ext = old_ext
    base = path
    inner = ''
    if parent:
        parent = os.path.abspath(parent)
        if path.startswith(parent + os.sep):
            base = parent
            inner = path[len(parent):]
    i = diff_min
    while True:
        new_path = f'{base}{diff_char * i}{inner}{ext}'
        if not os.path.exists(new_path):
            return new_path
        i += 1

def get_paths(top_path: str) -> 'Generator[str]':
    '''
    - If :param:`top_path` is the path to a single file, yield this path.
    - If it is a folder, yield the paths to all files in this folder and also in
      its subfolders.

    The paths are normalized and absolutized, but not sorted.
    '''
    top_path = os.path.abspath(top_path)
    if os.path.isfile(top_path):
        yield top_path
    elif os.path.isdir(top_path):
        for dirpath, _, filenames in os.walk(top_path):
            for filename in filenames:
                yield os.path.join(dirpath, filename)

def join(path: str, *paths: str) -> str:
    '''
    Join :param:`path` and subsequent parameters to a path and absolutize it.

    This may be particularly useful in combination with ``__file__``, e.g.::

        fs.join(__file__, '../../../cssjs/exemplar.css')
    '''
    return os.path.abspath(os.path.join(path, *paths))

def log_ip_medium(
        log: Log,
        ip: str,
        rest: str,
        timed: bool = True,
        ) -> None:
    '''
    Write :param:`ip` plus rest in :param:`log`. Before that, anonymize the IP:

    - IPv4: only the first 3 bytes are kept.
    - IPv6: only the first 6 (3 times 2) bytes are kept. (IPv6 addresses
      contain 128 bit of information, written as 8 blocks of 2 bytes, but
      this notation can be abbreviated.)

    .. note::
        For the anonymization of IPv6, see the following description on page 3
        at http://www.ietf.org/rfc/rfc2374.txt and the explanations on the
        subsequent pages::

            The aggregatable global unicast address format is as follows:

            | 3|  13 | 8 |   24   |   16   |          64 bits               |
            +--+-----+---+--------+--------+--------------------------------+
            |FP| TLA |RES|  NLA   |  SLA   |         Interface ID           |
            |  | ID  |   |  ID    |  ID    |                                |
            +--+-----+---+--------+--------+--------------------------------+

    :param timed: The same as in :meth:`Log.write`.
    '''
    if ':' in ip:
        ip_parts = ip.split(':')
        diff = 8 - len(ip_parts)
        if diff and '' in ip_parts:
            ip_parts.insert(ip_parts.index(''), diff * '0')
        log.write(':'.join(ip_parts[:4]) + rest, timed)
    else:
        log.write(ip.rsplit('.', 1)[0] + rest, timed)

def pave(path: str) -> str:
    '''
    Normalize and absolutize :param:`path` to a ``path`` and make all the
    intermediate directories needed to contain the leaf ``path`` which do
    not already exist. Return ``path``.
    '''
    path = os.path.abspath(path)
    pave_dir(os.path.dirname(path))
    return path

def pave_dir(dirpath: str) -> str:
    '''
    Normalize and absolutize :param:`dirpath` to a ``path`` and make all the
    directories from the root to ``path`` which do not already exist. Return
    ``path``.
    '''
    dirpath = os.path.abspath(dirpath)
    try:
        os.makedirs(dirpath, mode = 0o777, exist_ok = True)
    except FileExistsError:
        os.makedirs(dirpath, mode = 0o555, exist_ok = True)
    return dirpath

def read(
        path: str,
        encoding: 'None|str' = 'utf-8',
        errors: str = 'strict',
        **kwargs: 'None|str|int',
        ) -> 'bytes|str':
    '''
    Read the file at :param:`path`:

    - If :param:`encoding` is ``None``, return the content as bytes.
    - Otherwise try to decode the content with :param:`encoding` as encoding and
      with the error mode :param:`errors` (as in :meth:`bytes.decode`).

    :param kwargs: further keyword arguments taken by the built-in :func:`open`,
        except :param:`encoding`, :param:`errors` and ``mode``.
    '''
    if encoding is None:
        with open(path, 'rb', **kwargs) as file:
            return file.read()
    else:
        with open(
                path, 'r', encoding = encoding, errors = errors, **kwargs
                ) as file:
            return file.read()

def read_csv(
        path: str,
        encoding: str = 'utf-8',
        newline: str = '',
        delimiter: str = ',',
        **kwargs: 'str|bool',
        ) -> 'Generator[list[str]]':
    '''
    Read the file at :param:`path` with the encoding :param:`encoding` as a file
    in the format CSV.

    :param encoding: the encoding of :param:`path`.
    :param newline: the same as for :func:`open`.
    :param delimiter: the same as for :func:`csv.reader`.
    :param kwargs: further keyword arguments taken by :func:`csv.reader`, except
        :param:`delimiter`.
    '''
    with open(path, encoding = encoding, newline = newline) as file:
        for row in csv.reader(file, delimiter = delimiter, **kwargs):
            yield row

def sanitize(
        name: str,
        maxlength: int = 255,
        illegal_chars: 'set[str]' =
            {'"', '*', '/', ':', '<', '>', '?', '\\', '|'},
        illegal_ending_re: 're.Pattern[str]' = re.compile(r'[.\s]+\Z'),
        illegal_names: 'set[str]' = {
            'AUX', 'COM1', 'COM2', 'COM3', 'COM4', 'COM5', 'COM6', 'COM7',
            'COM8', 'COM9', 'CON', 'LPT1', 'LPT2', 'LPT3', 'LPT4', 'LPT5',
            'LPT6', 'LPT7', 'LPT8', 'LPT9', 'NUL', 'PRN'},
        ) -> str:
    '''
    Sanitize :param:`name` so that it is a legal filename:

    - Replace with “_” any character that is given in :param:`illegal_chars` or
      has no unicode name (notably control characters).
    - Delete a match of :param:`illegal_ending_re`. The name may be empty now.
    - If the name is longer than :param:`maxlength` now, it is cut off from the
      start inwards accordingly.
    - If the name is empty or belongs to :param:`illegal_names`, it is replaced
      with “_”.
    '''
    def check(c: str) -> str:
        try:
            unicodedata.name(c)
        except:
            return '_'
        else:
            if c in illegal_chars:
                return '_'
            return c
    name = ''.join( check(c) for c in name ).rstrip('. ')
    name = illegal_ending_re.sub('', name)
    if len(name) > maxlength:
        name = name[-maxlength:]
    if name in illegal_names or not name:
        name = '_'
    return name

def set_keys(user: str = '', password: str = '') -> bytes:
    '''
    Combine :param:`user` and :param:`password` to a single string,
    prefix the string with the length of the first item plus a colon,
    return the base16-encoded representation of the whole string.
    The two original strings can be gained by :func:`get_keys`.

    .. important::
        The encoding is not meant, of course, to serve a security
        purpose but to make the storage of the data more robust.
        Strictly speaking, the access data here are only as secure
        as the place where the resulting string is stored.
    '''
    len_first = str(len(user)) + ':'
    return base64.b16encode((len_first + user + password).encode())

def string(results: 'Any') -> str:
    '''
    Try to make a string from :param:`results` (in order to write it into a file
    or so).
    '''
    def is_string_or_no_iterable(term: 'Any') -> bool:
        return isinstance(term, str) or not isinstance(term, Iterable)

    if not results:
        return ''
    if isinstance(results, str):
        return results
    if isinstance(results, Exception):
        return traceback.format_exc()
    if not isinstance(results, Iterable):
        return str(results)
    if not isinstance(results, ty.Sequence):
        if isinstance(results, Mapping):
            results = results.items()
        try:
            results = sorted(results)
        except TypeError:
            pass
    if isinstance(results[0], str):
        return ('' if results[0].endswith('\n') else '\n').join(results)
    if not isinstance(results[0], ty.Sequence):
        return '\n'.join( str(item) for item in results )
    if len(results[0]) == 2:
        # Try to join it under the assumption that it is an `Iterable` of
        # `Iterables` of the length two, namely a `label` and a `result`;
        # the `label` may be an `Iterable` itself.
        return '\n'.join(
                (
                    str(label) +
                    '\t' +
                    (
                        str(result) if is_string_or_no_iterable(result)
                        else '\n'.join( str(r) for r in result )
                    ).replace('\n', '\n\t')
                )
                if is_string_or_no_iterable(label) else
                (
                    '\t'.join( str(item) for item in label ) +
                    '\t' +
                    (
                        str(result) if is_string_or_no_iterable(result)
                        else '\n'.join( str(r) for r in result )
                    ).replace('\n', '\n' + '\t' * len(label))
                )
                for label, result in results )
    else:
        return '\n'.join(
                '\t'.join(map(
                    lambda cell: str(cell).replace('\t', '').replace('\n', ''),
                    row
                    )) for row in results )

def write(
        text: 'bytes|str',
        path: str,
        encoding: str = 'utf-8',
        ext: 'None|str' = None,
        append: bool = False,
        **kwargs: 'None|bool|int|str',
        ) -> 'tuple[str, int]':
    '''
    Write :param:`text` into a file at :param:`path` in :param:`encoding`, if it
    is a string, otherwise in bytes.

    (As a preparation for that, create the file and all the intermediate folders
    of the path using :func:`pave` if necessary.)

    Return the path of the file and its size as the number of bytes.

    :param ext: If ``None``, it is ignored, if not, it substitutes any extension
        (including its leading dot!) of the corresponding old path.
    :param append: If ``True``, append :param:`text` to the :param:`path` file –
        by using the append-mode “a” of the built-in :func:`open`.
    :param kwargs: further keyword arguments taken by the built-in :func:`open`,
        except :param:`encoding`.
    '''
    if ext is not None:
        path = os.path.splitext(path)[0] + ext
    mode = 'a' if append else 'w'
    if isinstance(text, str):
        with open(pave(path), mode, encoding = encoding, **kwargs) as file:
            return path, file.write(text)
    else:
        mode += 'b'
        with open(pave(path), mode, **kwargs) as file:
            return path, file.write(text)

def write_csv(
        table: 'ty.Sequence[ty.Sequence[str|ty.Number]]',
        path: str,
        encoding: str = 'utf-8',
        newline: str = '',
        delimiter: str = ',',
        **kwargs: 'str|bool',
        ) -> 'tuple[str, int]':
    '''
    Write :param:`table` – a table of rows of either strings or numbers – into a
    file at :param:`path` as a CSV file, which can be opened with an application
    for spreadsheets.

    Return the path of the file and its size as the number of bytes.

    :param encoding: the encoding of :param:`path`; for Excel still “utf-16”.
    :param newline: the same as for :func:`open`.
    :param delimiter: the same as for :func:`csv.reader`.
    :param kwargs: further keyword arguments taken by :func:`csv.writer`, except
        :param:`delimiter`.
    '''
    with open(path, 'w', encoding = encoding, newline = newline) as file:
        writer = csv.writer(file, delimiter = delimiter, **kwargs)
        writer.writerows(table)
    return path, os.path.getsize(path)

def write_new(
        text: 'bytes|str',
        path: str,
        encoding: str = 'utf-8',
        ext: 'None|str' = None,
        append: bool = False,
        diff_char: str = DIFF_CHAR,
        diff_min: int = 0,
        parent: str = '',
        **kwargs: 'None|bool|int|str',
        ) -> 'tuple[str, int]':
    '''
    Write :param:`text` into a file at an unused path derived from :param:`path`
    in :param:`encoding`, if it is a string, otherwise in bytes.

    (As a preparation for that, create the file and all the intermediate folders
    of the path using :func:`pave` if necessary.)

    For the other parameters, see :func:`get_new_path`.

    Return the derived path and the file size as number of bytes.
    '''
    return write(
            text,
            get_new_path(path, ext, diff_char, diff_min, parent),
            encoding,
            **kwargs,
            )

if __name__ == '__main__':
    import doctest
    doctest.testmod()
