import nimpy
import std/editDistance
import std/unicode

proc minDist(xs: seq[string], ys: seq[string]): float {.exportpy.} =
  var dist: float = Inf
  var newDist: float
  var a: seq[Rune]
  var b: seq[Rune]
  for x in xs:
    for y in ys:
      if x.runeLen < y.runeLen:
        a = x.toRunes
        b = y.toRunes
      else:
        a = y.toRunes
        b = x.toRunes
      if a.len == 0:
        continue
      newDist = editDistance($a, $b) / b.len
      if dist > newDist:
        dist = newDist
      for i in 0 .. b.len - a.len:
        newDist = editDistance($a, $b[i .. i + a.len - 1]) / a.len
        if dist > newDist:
          dist = newDist
  return dist
