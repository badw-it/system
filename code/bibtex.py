# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
from collections import deque
from collections.abc import Sequence
from itertools import zip_longest

class Data:
    '''
    Model of bibtex data.
    For format-specification details, see e.g.:
    http://artis.imag.fr/~Xavier.Decoret/resources/xdkbibtex/bibtex_summary.html

    >>> import bibtex
    >>>
    >>> text = """
    ...     comment outside the items
    ...     @preamble {{command}}
    ...     @string {const = {resolved string}}
    ...     @kind {name,
    ...       key1 = 0123,
    ...       key2 = {value {@2}},
    ...       key3 = "{"}value{"}",
    ...       key4 = "value {@3",
    ...       key5 = const,
    ...       key6 = const # {value {@2}},
    ...       key7 = "value {@3}" # const,
    ...     }
    ...     @comment {equally treated as a comment outside the items
    ...         @kind {this_item_is_NOT_skipped,
    ...           key8 = 4567
    ...         }
    ...     } further comment outside item"""
    >>>
    >>> data = bibtex.Data(text = text)
    >>> for piece in data.outside_items:
    ...     print(piece)
    comment outside the items
    <BLANKLINE>
    <BLANKLINE>
    @comment {equally treated as a comment outside the items
    <BLANKLINE>
        } further comment outside item
    >>> for item in data.items:
    ...     print('-----------')
    ...     print(item.kind, item.name)
    ...     for key, val in item.fields.items():
    ...         print(key, val)
    -----------
    preamble 
     {command}
    -----------
    string 
    const {resolved string}
    -----------
    kind name
    key1 0123
    key2 {value {@2}}
    key3 "{"}value{"}"
    key4 "value {@3"
    key5 const
    key6 const # {value {@2}}
    key7 "value {@3}" # const
    -----------
    kind this_item_is_NOT_skipped
    key8 4567
    '''
    def __init__(
            self,
            text: str = '',
            outside_items: 'deque[str]' = deque(['']),
            items: 'deque[None|Item]' = deque(),
            keys_to_lower: bool = False,
            ) -> None:
        '''
        Instantiate a data structure containing:

        - :attr:`outside_items`: a sequence of the text chunks before,
          after and between the items, including ``'@comment'`` strings.
          These text chunks are stripped, i.e. they do not contain any
          leading or trailing whitespace. If there are no data yet, this
          sequence contains a single empty string.
        - :attr:`items`: a sequence of zero or more :class:`Item` data
          structures. See :meth:`Item.__init__`.

        .. important::
            If :param:`text` is given, it is taken as bibtex formatted text
            and read into the data structure :class:`Data`. Doing so, the
            keys are lowered if :param:`keys_to_lower` is ``True`` and the key
            is not a key in the item type “String” or “string”.
            Else, :param:`outside_items` is taken for :attr:`outside_items`
            and :param:`items` is taken for :attr:`items`, if they have
            fitting data types; if not, an error is thrown.
        '''
        if text:
            self.outside_items = deque([''])
            self.items = deque()
            self.read(deque(text), keys_to_lower)
        else:
            assert isinstance(items, Sequence)
            assert (not items or
                    all( isinstance(item, Item) for item in items ))
            self.items = deque(items)
            if isinstance(outside_items, str):
                self.outside_items = deque([outside_items])
            else:
                assert isinstance(outside_items, Sequence)
                self.outside_items = deque(outside_items)

    def read(self, chars: 'deque[str]', keys_to_lower: bool = False) -> None:
        '''
        Read :param:`chars`, a deque of characters representing a
        bibtex-formatted text. Read it into :attr:`outside_items` and
        :attr:`items` (both described in :meth:`__init__`).
        Thereby, the following rules are observed:

        - The order of all items, of the parts between the items and
          of the fields within the items is preserved.
        - The orthography of all items is preserved (also of keys, constants
          and abbreviations). But if :param:`keys_to_lower` is ``True``,
          the keys are lowered if they are not keys in the item type “String”
          or “string”.
        - Within field values, constants are preserved. Constants are written
          without surrounding delimiters, either standalone or beside another,
          delimited part of the field value. (As a rule, constants are
          abbreviations resolved in an item of the kind ``'@string'``.)
        - The delimiters of a field value (qutotation marks, i.e. `"`, or
          curly braces) are preserved due to constant values, which may be
          part of the value but are not included in the delimiters.
        - Within field values delimited by quotation marks, a quotation mark
          within curly braces is understood as an escaped quotation mark.
          Other curly braces may occur, and they may occur in properly
          nested pairs or not. An at-sign may occur.
        - Within field values delimited by curly braces, other braces may
          occur but only in properly nested pairs. According to the rules
          of bibtex, an at-sign must not occur at all. But this function
          does parse it correctly.
        '''
        inside = False
        in_kind = False
        in_name = False
        in_body = False
        in_key = False
        in_constant_value = False
        in_integer_value = False
        in_quoted_value = False
        in_bracketed_value = False
        after_nonconstant_value = False
        after_value = False
        braces_level = 0
        outside_part = ''
        kind = ''
        name = ''
        key = ''
        value = ''
        value_parts = []
        while chars:
            c = chars.popleft()
            if inside:
                if in_kind:
                    if c == '{':
                        kind = kind.strip()
                        self.items[-1].kind = kind
                        in_kind = False
                        if kind.lower() == 'preamble':
                            in_body = True
                            key = ''
                            in_constant_value = True
                        elif kind.lower() == 'string':
                            in_body = True
                            in_key = True
                            key = ''
                        else:
                            in_name = True
                            name = ''
                    else:
                        kind += c
                elif in_name:
                    if c == ',':
                        self.items[-1].name = name.strip()
                        name = ''
                        in_name = False
                        in_body = True
                        in_key = True
                        key = ''
                    else:
                        name += c
                elif in_body:
                    if in_key:
                        if c in ',':
                            continue
                        elif c == '=':
                            key = key.strip()
                            in_key = False
                            in_constant_value = True
                        elif c == '}':
                            in_key = False
                            in_body = False
                            inside = False
                            self.outside_items.append('')
                            outside_part = ''
                        else:
                            key += c
                    elif in_constant_value:
                        if c in '{"0123456789':
                            value_parts.append(value.strip())
                            value = c
                            in_constant_value = False
                            if c == '{':
                                braces_level += 1
                                in_bracketed_value = True
                            elif c == '"':
                                in_quoted_value = True
                            elif c in '0123456789':
                                in_integer_value = True
                        elif c in ',}':
                            value_parts.append(value.strip())
                            if keys_to_lower and kind.lower() != 'string':
                                key = key.lower()
                            self.items[-1].fields[key] = \
                                    ' '.join(filter(None, value_parts))
                            value = ''
                            value_parts = []
                            in_constant_value = False
                            in_key = True
                            key = ''
                            if c == '}':
                                chars.appendleft('}')
                        else:
                            value += c
                    elif in_integer_value:
                        if c in '0123456789':
                            value += c
                        else:
                            chars.appendleft(c)
                            after_nonconstant_value = True
                    elif in_quoted_value:
                        if c == '"' and not (value and (
                                (value[-1] == '{' and chars and chars[0] == '}')
                                )):
                            after_nonconstant_value = True
                        value += c
                    elif in_bracketed_value:
                        value += c
                        if c == '{':
                            braces_level += 1
                        elif c == '}':
                            braces_level -= 1
                            if braces_level == 0:
                                after_nonconstant_value = True
                    if after_nonconstant_value:
                        value_parts.append(value.strip())
                        value = ''
                        after_nonconstant_value = in_integer_value = \
                                in_quoted_value = in_bracketed_value = False
                        in_constant_value = True
            else:
                if c == '@' and not (
                        len(chars) > 6 and
                        ''.join( chars[n] for n in range(7) ).lower()
                            == 'comment'
                        ):
                    self.outside_items[-1] = outside_part.strip()
                    inside = True
                    in_kind = True
                    self.items.append(Item())
                    kind = ''
                else:
                    outside_part += c
        self.outside_items[-1] = outside_part

    def write(
            self,
            item_outside_joiner: str = '\n',
            item_outside_block_joiner: str = '\n',
            ) -> str:
        '''
        >>> import bibtex
        >>>
        >>> text = """
        ...         comment outside the items
        ...         @preamble {{command}}
        ...         @string {const = {resolved string}}
        ...         @kind {name,
        ...           key1 = 0123,
        ...           key2 = {value {@2}},
        ...           key3 = "value {@3",
        ...           key4_old = const,
        ...           key5 = const # {value {@2}},
        ...           key6 = "value {@3}" # const,
        ...         }
        ...         @comment {equally treated as a comment outside the items
        ...             @kind {this_item_is_NOT_skipped,
        ...               key_new = 4567
        ...             }
        ...         } further comment outside item"""
        >>>
        >>> data = bibtex.Data(text = text)
        >>> data.outside_items = deque(['% Encoding: UTF8\\n'])
        >>> print(data.write())
        % Encoding: UTF8
        <BLANKLINE>
        @preamble{{command}}
        <BLANKLINE>
        @string{const = {resolved string}}
        <BLANKLINE>
        @kind{name,
          key1     = 0123,
          key2     = {value {@2}},
          key3     = "value {@3",
          key4_old = const,
          key5     = const # {value {@2}},
          key6     = "value {@3}" # const
        }
        <BLANKLINE>
        @kind{this_item_is_NOT_skipped,
          key_new  = 4567
        }
        '''
        max_keywidth = max( len(key)
                for item in self.items
                for key in item.fields.keys() )
        items = ( item.write(max_keywidth = max_keywidth)
                for item in self.items )
        return item_outside_block_joiner.join(
                outside + item_outside_joiner + item
                for outside, item in
                zip_longest(self.outside_items, items, fillvalue = '')
                )

class Item:
    def __init__(
            self,
            kind: str = '',
            name: str = '',
            fields: 'dict[str, str]' = {},
            ) -> None:
        '''
        Instantiate a data structure containing:

        - :attr:`kind`: a string to distinguish between the following
          kind of items:

          - ``'preamble'`` item: The sigil should be the empty string.
            There should be only one mapping. The key should be the empty
            string. The value may represent a Tex command.
          - ``'string'`` item: The sigil should be the empty string.
            There should be only one mapping. The key may be an abbreviation
            (used in the bibtex data themselves), the value may be the
            resolution of this abbreviation.
          - actual bibliographical items, whose key-value pairs describe
            a publication; and different kinds of items are distinguished
            by the very attribute :attr:`kind`.

        - :attr:`name`: may be the empty string or a sigil for the entity
          described by item.
        - :attr: `fields`: a key-value mapping preserving insertion order.
          There may be zero or more mappings. The key must be the empty or
          another string. The value must be a string representing an integer
          value or a constant (i.e. usually an abbreviation resolved by a
          ``'string'`` item) or a literal or a combination of constants and
          literals (usually combined with ``'#'``). Literals are any strings
          and must be delimited, i.e. quoted.

        .. important::
            The quoting is not done automatically, but one can use
            :func:`quote`. Cf.:

        >>> import bibtex
        >>>
        >>> item = bibtex.Item(
        ...         'article',
        ...         'Xyz2007',
        ...         {'author': bibtex.quote('Ab@c "the hand" Xyz'),
        ...          'year': '2007'})
        >>> print(item.fields['author'])
        "Ab@c {"}the hand{"} Xyz"
        >>> print(item.write())
        @article{Xyz2007,
          author = "Ab@c {"}the hand{"} Xyz",
          year   = 2007
        }
        '''
        self.kind = kind
        self.name = name
        if fields:
            assert isinstance(fields, dict)
            self.fields = fields
        else:
            # Do not use the default value, because then Python would use only
            # a new reference to the same data object, not a new data object.
            self.fields = {}

    def write(self, indent: str = '  ', max_keywidth: int = -1) -> str:
        '''
        Convert :param:`self` to a bibtex string representation.

        :param indent: indent to be written in front of the key.
        :param max_keywidth: the length of the longest key, used for padding
            behind the key in order to align the values.

            - If the value is negative, the alignment is done automatically
              according to the longest key within :param:`item`. This is the
              default.
            - If the value is ``0`` (or lesser than the length of the shortest
              key), no alignment is done at all.
            - One can specify another value in order to ensure an alignment
              that is the same e.g. for all items in a whole document.
        '''
        if max_keywidth < 0:
            max_keywidth = max( len(key) for key in self.fields.keys() )
        return '@{kind}{{{name}{fields}}}'.format(
                kind = self.kind,
                name = self.name + (',\n' if self.name else ''),
                fields = ',\n'.join(
                    '{key}{value}'.format(
                        key = '{indent}{key:<{max_keywidth}} = '.format(
                            indent = indent if self.name else '',
                            key = key,
                            max_keywidth = max_keywidth if self.name else 0
                            ) if key else '',
                        value = value)
                    for key, value in self.fields.items() )
                    + ('\n' if self.name else '')
                )

def quote(value: str, braces: bool = False) -> str:
    '''
    Quote :param:`value`.

    If :param:`braces` is ``False``, use quotation marks as delimiters,
    else use curly braces.

    - In the case of quotation marks, already occurring quotation marks
      within the value are escaped by enclosing braces: `{"}`.
    - In the case of curly braces, according to the bibtex rules, no
      escaping is done. Already occurring curly braces must be properly
      nested. An at-sign must not occur at all.
    '''
    if braces:
        return '{' + value + '}'
    else:
        return '"{}"'.format(value.replace('"', '{"}'))

def unquote(value: str) -> str:
    '''
    - Embracing quotation marks and curly braces are removed.
    - In the case of embracing quotation marks, instances of
      `{"}` are replaced by `"`.
    - If a constant value is mixed in, the function may produce
      a broken value.
    '''
    if len(value) > 1:
        if value[0] == '"' and value[-1] == '"':
            return value[1:-1].replace('{"}', '"')
        if value[0] == '{' and value[-1] == '}':
            return value[1:-1]
    return value

if __name__ == '__main__':
    import doctest
    doctest.testmod()
