import re
import __init__
import fs

def segment(text):
    wackernagel = '(же|(?:оу|ѹ҆)?бо)'
    text = text.strip()
    for old, new in (
            # Precorrection:
            (
                r'჻',
                r' ჻ '
            ),
            (
                r'\s+',
                r' '
            ),
            # Segmentation:
            (
                rf'(?i)(\S+ {wackernagel}( |$))',
                r'\n\g<1>'
            ),
            (
                r'჻',
                r'჻\n'
            ),
            (
                r'((^| )(А|Но|[Тт][еѣ]мъ?же)( |$))',
                r'\n\g<1>'
            ),
            # Postcorrection:
            (
                r'\s*\n\s*',
                r'\n\n'
            ),
            (
                rf'(?i){wackernagel}\n\n((\S+ ){{,3}}{wackernagel})',
                    # “{{,3}}” with double curlies as long as it is an f-string!
                r'\g<1> \g<2>'
            ),
            (
                r'(?i)\s+(а|не|но|[ѡи]\u0486?)\n\n',
                r'\n\n\g<1> '
            )
            ):
        text = re.sub(old, new, text)
    return text

if __name__ == '__main__':
    # Substitute the path that is true for your environment here:
    path = r'C:\Users\M\A\ablage\exempla_py\de_lepra.txt'
    text = fs.read(path)
    print(segment(text))
