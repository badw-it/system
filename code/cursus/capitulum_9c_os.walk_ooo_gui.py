# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 9c. Ordnerbäume durchlaufen. Objekte und Klassen. GUIs
# ================================================================

import os
import re

import utilia

# Zugriff auf eine Reihe von Dateien, auch in Unterordnern
# --------------------------------------------------------

# Als Beispiel dient ein Ordner “corpus”, den man in folgender zip-Datei findet:
# https://dienst.badw.de/varia/exempla.zip

# Man lege ihn irgendwo auf den eigenen Rechner und mache eine Kopie – etwa über
# Markieren, Strg + C und Strg + V, was z. B. in Windows eine Kopie erzeugt, die
# das Namensanhängsel “ - Kopie” hat.

# Wir arbeiten mit einer Kopie, weil wir Unterordner und Dateien überschreiben –
# zur Umbenennung oder für Änderungen am Inhalt. Wenn man in einem ganzen großen
# Baum von Ordnern und Dateien überschreibt, dann ist es sicher und bequem, wenn
# man vorher eine Kopie anfertigt und die Kopie abändert. Bei Nichtgefallen kann
# man einfach die Kopie löschen, eine neue anfertigen, das Programm anpassen und
# wieder anwenden – und das immer wieder, ohne im Programm jemals die Pfadangabe
# ändern zu müssen.

# Jetzt zu Beginn ist die folgende Pfadangabe natürlich einmal den eigenen Namen
# von Ordnern anzupassen:

path = r'C:\Users\M\A\ablage\exempla_py\corpus - Kopie' + os.sep

# Wir fügen ``os.sep`` (auf Windows der Rückschlag, auf Linux der Schlag) hinzu,
# damit wir es unten einfacher haben, Pfade zusammenzusetzen: Es genügt dann ein
# bloßes ``path + name``.

# Die folgende Funktion werden wir für die Änderung der Dateinamen verwenden:

def make_date_sortable(match):
    # Wir holen die Untergruppe “day” (Name frei vergebbar) aus dem Match-Objekt
    # und füllen nach links so mit Nullen auf, dass sich mindestens zwei Zeichen
    # ergeben.
    day = match.group('day').zfill(2)
    # Entsprechendes mit der Monats- und Jahr-Angabe:
    month = match.group('month').zfill(2)
    year = match.group('year').zfill(4)
    return f'{year}-{month}-{day}'

# Wir schauen uns einmal die Liste der Ordner und Dateien unterhalb von “corpus”
# an, und zwar sortiert:

for name in sorted(os.listdir(path)):
    print(name)

# Die Namen enthalten Tagesdaten. Wir wollen diese Daten gemäß ISO 8601 formen –
# dann ist die alphanumerische Sortierung zugleich chronologisch:

for old in os.listdir(path):
    # ``re.sub`` kann auch eine Funktion als Ersetzen-Ausdruck nehmen! Sie nimmt
    # als Argument das Match-Objekt und gibt die Zeichenkette zurück, die an die
    # Stelle, wo das Match-Objekt gefunden wurde, einzusetzen ist. Auf diese Art
    # kann man sehr verwickelte Ersetzungen durchführen.
    new = re.sub(
            r'(?<=_)(?P<day>\d+)\.(?P<month>\d+)\.(?P<year>\d+)',
            make_date_sortable,
            old)
    print(path + old)
    print(path + new)
    print('------------')
    # ACHTUNG: Wenn man die folgende Zeile einkommentiert, wird umbenannt; daher
    # ist es sehr wichtig, sich vorher zu versichern, ob wirklich ``path + old``
    # das ist, was umbenannt werden soll, und ``path + new`` das gewollte Neue –
    # denn ein Umbenennen kann auch ein Verschieben sein, und arges Ungemach mag
    # hier geschehen!
    ##os.replace(path + old, path + new)

# Wenn man umbenannt hat, ergibt die Sortierung eine sinnvollere Reihe.

for name in sorted(os.listdir(path)):
    print(name)

# Nun wollen wir auf alle Dateien zugreifen, die irgendwo in einem Ordner liegen
# – also vielleicht in Unter- oder Unterunterordnern usw.:

for dirpath, dirnames, filenames in sorted(os.walk(path)):
    dirpath = dirpath + os.sep
    for filename in sorted(filenames):
        filepath = dirpath + filename
        print(filepath)

# Ein Beispiel, bei dem Text der Dateien geändert wird:

for dirpath, dirnames, filenames in os.walk(path):
    dirpath += os.sep
    for filename in filenames:
        filepath = dirpath + filename
        text = utilia.read(filepath)
        # Eine Kette von Ersetzungen kann man auch als ``for``-Schleife, die ein
        # Tupel von Vorher-Nachher-Paaren durchläuft, schreiben. Das ist möglich
        # mit normalen Zeichenketten und ``replace`` oder auch mit r-Strings und
        # ``re.sub``, also mit regulären Ausdrücken wie im Folgenden:
        for old, new in (
                # Die Ersetzungen stellen eine – unvollständige – Überführung in
                # das Internationale Phonetische Alphabet dar.
                (r'ā', 'a:'),
                (r'ē', 'e:'),
                (r'ī', 'i:'),
                (r'ō', 'o:'),
                (r'ū', 'u:'),
                # Hier ein Beispiel für eine umrahmte Ersetzung; die umrahmenden
                # Ersetzungen verhindern die Ersetzung “sch” zu “ʃ” dort, wo das
                # “sch” gar nicht zusammengehört:
                (r'ssch', 'ss-ch'),
                (r'sch', 'ʃ'),
                (r'ss-ch', 'ssch'),
                # (Das würde man wohl mit regulären Ausdrücken machen, wenn man,
                # wie hier, ohnehin schon reguläre Ausdrücke verwendet. Das Paar
                # wäre ``(r'(?<!s)sch', 'ʃ'),``.)

                # Hier ein Beispiel, wo ein regulärer Ausdruck zupass kommt:

                # Nach hinteren Vokalen wird “ch” durch “x” ersetzt – aber nicht
                # nach dem Diphthong “iu”:
                (r'(([ao]|(?<!i)u):?)ch', '\g<1>x'),
                # Sonst wird “ch” durch “ç” ersetzt:
                (r'ch', 'ç'),
                ):
            text = re.sub(old, new, text)
        # ACHTUNG: Wenn man die folgenden Zeilen einkommentiert, wird die ältere
        # Datei ``filepath`` überschrieben!
        with open(filepath, mode = 'w', encoding = 'utf-8') as file:
            print(filepath)
            file.write(text)

# Objekte und Klassen
# -------------------

# Ein Objekt ist eine Art von dictionary. Zunächst zum Vergleich ein dictionary:

test_dictionary = {'key1': 'value', 'key2': 'value'}

print()
print(test_dictionary)

# Schlüssel müssen einmalig sein, Werte nicht. Und hier noch der Zugriff auf das
# dictionary:

print(test_dictionary['key1'])

# Objekte werden anders geschrieben, mit einem ``class``-Block:

# (``class``-Blöcke werden üblicherweise noch vor ``def``-Blöcke geschrieben!)

class TestObject:
    key1 = 'value'
    key2 = 'value'

test_object = TestObject()

print()
print(test_object)

# Auch der Zugriff auf Werte (man spricht hier von Attributen) des Objektes wird
# anders geschrieben:

print(test_object.key1)

# Ein Objekt aber schreibt man weniger um solcher gewöhnlichen Werte willen, die
# man ebenso einfach in einem dictionary speichern könnte. Sondern um bestimmter
# Attribute willen: nämlich der Methoden, die ja – wie gelegentlich angedeutet –
# eine Art von Funktionen sind. Das Besondere an Methoden ist, dass sie immerdar
# als erstes Argument *das Objekt selbst* nehmen. Und darin liegt ein Hauptzweck
# von Objekten, denn so kann man in jeder Methode einen Wert als Attribut an das
# Objekt hängen, ihn abrufen und ändern, und die Änderung kommt in jeder Methode
# und überhaupt jedem Attributaufruf an.

class TestObject:
    # ``__init__`` ist eine besondere Methode, die ausgeführt wird, wenn man ein
    # Objekt erstellt, also einen ``class``-Block aufruft.
    def __init__(self, minimum):
        # Das Objekt selbst wird herkömmlicherweise ``self`` genannt.
        self.minimum = minimum
        self.minimum_was_used = False

    def diff(self, a, b):
        delta = a - b
        if delta < 0:
            delta = b - a
        if delta < self.minimum:
            self.minimum_was_used = True
            return self.minimum
        else:
            return delta

test_object = TestObject(1)

print()
print(f'{test_object.minimum = }')
# … ein Kürzel für ``print('test_object.minimum =', test_object.minimum)``.

print(f'{test_object.minimum_was_used = }')

for a, b in (
        (4, 6),
        (6, 4),
        (4, 4),
        (4, 5),
        ):
    delta = test_object.diff(a, b)
    print(f'{a}-{b}-{delta = } with {test_object.minimum = }')
    print(f'{test_object.minimum_was_used = }')

print()

test_object = TestObject(0)
for a, b in (
        (4, 6),
        (6, 4),
        (4, 4),
        (4, 5),
        ):
    delta = test_object.diff(a, b)
    print(f'{a}-{b}-{delta = } with {test_object.minimum = }')
    print(f'{test_object.minimum_was_used = }')

# Ein anderer Hauptzweck von Objekten ist Vererbung: Man kann ein Objekt auf ein
# anderes gründen so, dass dessen Attribute und Methoden übernommen werden, ohne
# dass man sie noch einmal schriebe. Ein Attribut oder eine Methode, die man mit
# gleichem Namen wie eine übernommene schreibt, überschreibt die übernommene.

class GrandTestObject(TestObject):
    def __init__(self, minimum, maximum):
        # ``__init__`` von ``TestObject`` wird überschrieben.
        self.minimum = minimum
        self.maximum = maximum
        self.minimum_was_used = False
        self.maximum_was_used = False

    # ``diff`` wird geerbt.

    def add(self, a, b):
        # … wird hinzugefügt.
        total = a + b
        if total < self.minimum:
            self.minimum_was_used = True
            return self.minimum
        elif total > self.maximum:
            self.maximum_was_used = True
            return self.maximum
        else:
            return total

print()

test_object = GrandTestObject(0, 9)
for a, b in (
        (4, 6),
        (6, 4),
        (4, 4),
        (4, 5),
        ):
    delta = test_object.diff(a, b)
    total = test_object.add(a, b)
    print(f'{a}-{b}-{delta = } with {test_object.minimum = }')
    print(f'{test_object.minimum_was_used = }')
    print(f'{a}-{b}-{total = } with {test_object.maximum = }')
    print(f'{test_object.maximum_was_used = }')

# Programmierung von “Graphical User Interfaces” = “GUIs”
# -------------------------------------------------------

# GUIs sind Programme, in denen eine Benutzeroberfläche programmiert ist, die so
# etwas wie Schalt- und Anzeigeflächen in der Form von Knöpfen, Fenstern, Listen
# usw. enthält. Kurzum, normale Programme wie Word oder Firefox.

# Es gibt verschiedene Gerüste, mit denen man in Python GUIs programmieren kann:

# - Tkinter: ist als einziges in der Pythoninstallation bereits enthalten – vgl.
#   https://docs.python.org/3/library/tkinter.html –, ist Schnittstelle zu einem
#   Gerüst namens Tk der Skriptsprache Tcl, recht übersichtlich, etwas veraltet.

# - PySide2: ist zu installieren – vgl. https://pypi.org/project/PySide2/ –, ist
#   Schnittstelle zum sehr umfangreichen und fortschrittlichen Gerüst namens Qt,
#   das in der sehr schnellen Sprache C++ geschrieben ist. Etwas schwierig.

# - PyQt6: ist zu installieren – vgl. https://pypi.org/project/PyQt6/ – und eine
#   andere Schnittstelle zum genannten Qt. Man muss eine Lizenz kaufen, wenn man
#   nicht den eigenen Code seinerseits zur Verfügung stellt. Wird besser auf dem
#   neuesten Stand gehalten als PySide2.

# - PyGObject: ist zu installieren – vgl. https://pypi.org/project/PyGObject/ –,
#   eine Schnittstelle zum umfangreichen, fortschrittlichen und stabilen Gerüste
#   namens GTK+, das in der sehr schnellen Sprache C geschrieben ist. Auch etwas
#   schwierig. Wird nicht so häufig verwendet wie die vorgenannten.

# - Einige weitere sind in Kürze:

#   - wxPython: https://pypi.org/project/wxPython/ – früher zumindest der größte
#     Konkurrent von Tkinter selbst, inzwischen wohl nicht mehr.

#   - Kivy: https://pypi.org/project/Kivy/ – das wohl pythonischste Gerüst, aber
#     noch nicht auf dem Stand der Großen; die Geschwindigkeit wird es wohl auch
#     nicht leicht werden.

#   - pygame: https://pypi.org/project/pygame/ – zur Spieleentwicklung.
