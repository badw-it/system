'''
Make a collection of forms that are tagged as names in HTML fragments structured
in JSON, which is an export from a database of notes by Jean Paul.

The collection has every unique name form and the number of its occurrences.
'''
import json
import os
import xml.etree.ElementTree as ET
import collections

def collect_names(path, sort_function, encoding = 'utf-8'):
    '''
    In the JSON file :param:`path` (exported via phpmyadmin),
    extract all annotated name tokens from the HTML fields,
    collect all name types and the number of tokens of each type
    and return a list of name-number tuples sorted by number and
    then by the name normalized with :func:`normalize`.
    '''
    with open(path, encoding = encoding) as file:
        text = file.read()

    doc = json.loads(text)

    names = {}
    for table in doc:
        # Not every row in ``doc`` is a table, actually. So:
        if table.get('type') != 'table':
            continue
        for element in table['data']:
            cell = element['eintragstext']
            cell = '<root>' + cell + '</root>'
            root = ET.XML(cell)
            for span in root.findall('.//span[@class="name"]'):
                name = ''.join(span.itertext())
                # ``span.text`` gets only the first uninterrupted text segment.
                if name in names:
                    names[name] += 1 # i.e. ``names[name] = names[name] + 1``
                else:
                    names[name] = 1

    return sorted(names.items(), key = sort_function)

def normalize(term):
    term = term.lower()
    for old, new in (
            ('ä', 'a'),
            ('ö', 'o'),
            ('ü', 'u'),
            ('ß', 's'),
            ):
        term = term.replace(old, new)
    return term

def output(names, path):
    '''
    Enrich :param:`names`, convert it into JSON and write the json string into a
    file, whose path is derived from :param:`path` by appending “.json” until it
    is unique.

    For the sake of readability and editability by humans, prettify the JSON (by
    using an indent) and use “ü” etc. instead of pure ASCII escape sequences.
    '''
    names = [
            {
                'ID': ID, # a counter, provided by ``enumerate`` below.
                'name': element[0],
                'GND': '',
                'count': element[1],
                'variants': '',
            }
            for (ID, element) in enumerate(names, start = 1)
            ]

    doc = json.dumps(names, indent = 1, ensure_ascii = False)

    while os.path.exists(path):
        path += '.json'

    with open(path, 'w', encoding = 'utf-8') as file:
        size = file.write(doc)

    return (path, size)

def sort_by_name(number_name):
    return normalize(number_name[0])

def sort_by_number_and_name(number_name):
    return (-number_name[1], normalize(number_name[0]))

if __name__ == '__main__':
    # Substitute the path that is true for your environment here:
    path = r"C:\Users\M\A\ablage\exempla_py\jean-paul-db.json"

    names = collect_names(path, sort_by_number_and_name)

##    # Check the result:
##    for (name, number) in names:
##        print(number, name)

    print(output(names, path))
