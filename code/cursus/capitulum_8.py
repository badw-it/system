# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 8
# ===========

# Hauptteil
# ---------

# Im Folgenden sind einige Teile der Datei vom letzten Kurs wiederholt – aber um
# die dort zu findenden Erläuterungen gekürzt. Gegenüber dem letzten Mal ergänzt
# ist das, was dort hinter ``####`` als offenes Anliegen festgehalten stand: die
# Aufgliederung der Stellenangabe.

import html
import json
import re

import fitz

import utilia

def normalize_whitespace(term):
    return re.sub(r'\s+', ' ', term).strip()

# Zum try-except-Block in der folgenden Funktion siehe die Dokumentation unter:
# https://docs.python.org/3/tutorial/errors.html#handling-exceptions

def register(quotations, term):
    '''
    Register the location information :param:`term` in the latest element
    of :param:`quotations`. Note:

    - If a unit of location seems to be missing, assume “cap.”.
    - Unify the units “lib.” and “liber” to “lib.”.
    '''
    for item in term.split(','):
        item = item.strip()
        try:
            unit, value = item.split(':', 1)
        except ValueError: # The split-up attempt has thrown an error, so:
            unit = 'cap.'
            value = item
        unit = unit.strip()
        if unit == 'liber':
            unit = 'lib.'
        quotations[-1][unit] = value.strip()

def remove_tag(term):
    return re.sub(r'<[^>]*>', '', term)

def replace_linebreaks(match):
    werkID = normalize_whitespace(remove_tag(match.group('werkID')))
    speicher = normalize_whitespace(remove_tag(match.group('speicher')))
    return f'█{werkID}█{speicher}</p>'

def untag(term):
    return normalize_whitespace(html.unescape(remove_tag(term)))

path = r'C:\Users\M\A\ablage\exempla_py\reservare.pdf'

doc = fitz.open(path)

temp = []
for page in doc:
    text = page.getText('html')
    temp.append(text)

text = '\n'.join(temp)
text = re.split(r'(?im)^(?:<.*?>)*Total\s*:\s*\d.*\n', text, maxsplit = 1)[1]
text = re.sub(r'''(?x)
        \( # The last round-bracket expression before the database info.
        (?P<werkID>
        [^)]*
        )
        \)
        (\s|<[^>]*>)*?-(\s|<[^>]*>)*? # A hyphen surrounded by space or tags.
        (?P<speicher> # The database info, e.g. “LLT-A” or “LLT-B”.
        LLT(\s|<[^>]*>)*?-(\s|<[^>]*>)*?A
        |
        LLT(\s|<[^>]*>)*?-(\s|<[^>]*>)*?B
        )
        (\s|<.*?>)*? # Space or tags again.
        </p> # The closing paragraph tag.
        ''', replace_linebreaks, text)

quotations = []
immediately_after_first_line = False
for para_match in re.finditer(r'(?s)<p\b.*?</p>', text):
    para_text = para_match.group()
    if '█' in para_text: # The title info is recognized by a “█”.
        para_text, werkID, speicher = para_text.split('█', 2)
        para_text = untag(para_text)
        # The remaining paragraph text should contain the author and the title –
        # separated by a hyphen – but the author info may be missing.
        para_text_items = para_text.split('-', 1)
        if len(para_text_items) == 2:
            verfasser, werk = para_text_items
        else: # There is no author info.
            verfasser = ''
            werk = para_text_items[0]
        quotations.append({
                'speicher': untag(speicher),
                'werkID': untag(werkID),
                'verfasser': verfasser.strip(),
                'werk': werk.strip(),
                'beleg': '', # used as a target for appending (see below).
                })
        immediately_after_first_line = True
    elif immediately_after_first_line: # The passage info is recognized by flag.
        immediately_after_first_line = False
        stelle = untag(para_text)
        # Wir zergliedern die Stellenangabe noch in die Einheiten und Werte, die
        # sie enthält (wie “cap. : 12” u.a.).
        if '(' in stelle: # There may be an additional info in round brackets.
            hauptstelle, nebenstelle = stelle.split('(', 1)
            nebenstelle = nebenstelle.strip().strip(')')
        else:
            hauptstelle = stelle
            nebenstelle = ''
        register(quotations, hauptstelle)
        register(quotations, nebenstelle)
    else: # Any other paragraph should belong to the quotation itself.
        # Introduce the markers for the matching form:
        para_text = para_text.replace('<b>', '#').replace('</b>', '€')
        para_text = untag(para_text)
        # Improve the position of the markers:
        para_text = re.sub(r'#(\s+)', '\g<1>#', para_text)
        para_text = re.sub(r'(\s+|,|\.|;|:|!|\?)€', '€\g<1>', para_text)
        if quotations[-1]['beleg']:
            quotations[-1]['beleg'] += ' ' # Append a space first.
        quotations[-1]['beleg'] += para_text

# Die nun aufgegliedert unter ``quotations`` gespeicherten Angaben sollen nun in
# eine Datei gespeichert werden – aber so, dass sie daraus jederzeit wieder ohne
# erneuten Zergliederungsaufwand in die gleiche Python-Datenstruktur (eine Liste
# von Schlüssel-Wert-Zuordnungen) eingelesen werden können. Dafür bietet es sich
# an, ``quotations`` zu einem String nach JSON-Regeln zu machen:
#
# - Wie gehabt kann ein String in Bytes umgewandelt werden, Bytes können in eine
#   Datei geschrieben werden.
# - Das Schreiben und Lesen von JSON kann mit Code erledigt werden, der schon in
#   der Python-Standardinstallation enthalten ist.
# - Alle von uns für ``quotations`` verwendeten Python-Datentypen haben eine und
#   nur eine genau passende JSON-Schreibweise, die sich zudem weitgehend mit der
#   in Python üblichen deckt, noch mehr mit der in Javascript üblichen.
#
#   Folgendes ist wie in Python:
#
#   - Listen werden von eckigen Klammern umfasst.
#   - dictionaries (“Objekte” genannt) werden von Schweifklammern umfasst.
#   - Schlüssel und Wert werden von einem Doppelpunkt getrennt.
#   - Glieder in Listen und dictionaries werden von Kommata getrennt.
#   - Zeichenketten stehen in doppelten Anführungszeichen.
#   - Zahlen werden ohne Grenzzeichen aufgeschrieben; bei Bruchzahlen steht kein
#     Komma, sondern ein Punkt (wie im Englischen usw. üblich).
#   - Die Umgehung eines doppelten Anführungszeichens in einer Zeichenkette wird
#     mit Rückschlag vorgenommen: ``"Er sagte \"Ja\" und verstummte."``
#   - Außerdem können beliebige Zeichen umschrieben werden mit einer Folge “\u”,
#     der eine genau vierstellige (also bei Bedarf links mit Nullen zu füllende)
#     hexadezimale Zahl folgt, die einfach die Unicode-Nummer des Zeichens ist –
#     vgl. https://unicode-table.com zum Finden solcher Nummern. Das Zeichen “ξ”
#     etwa kann durch “\u03BE” umschrieben werden.
#
#   Folgendes ist anders als in Python:
#
#   - ``True``, ``False``, ``None`` heißen ``true``, ``false``, ``null``.
#   - Andere als die schon erwähnten Datentypen gibt es nicht.
#   - Zeichenketten müssen mit doppelten Anführungsstrichen umfasst werden; kein
#     anderer Typ von Anführungsstrichen ist erlaubt.
#   - Überschüssige Kommata am Ende von Listen oder dictionaries sind verboten –
#     ``["a", "b",]`` ist in Python erlaubt, in JSON nur ``["a", "b"]``.

#   Vgl. des weiteren: https://www.json.org

# Zum Folgenden siehe: https://docs.python.org/3/library/json.html#json.dumps

text = json.dumps(quotations, indent = '\t', ensure_ascii = False)
path, size = utilia.write(text, path, ext = '.json')

print(path, size)

# Wir lesen die so erzeugte Datei zur Probe gleich wieder ein:

text = utilia.read(path)

# Zum Folgenden siehe: https://docs.python.org/3/library/json.html#json.loads

quotations2 = json.loads(text)

# Die Probe aufs Exempel:

print('``quotations == quotations2``:', quotations == quotations2)

# Anhang
# ------

# Im Anschluss an die oben erwähnte, in Python und JSON vorgesehene Umschreibung
# von Zeichen mit “\uXXXX” sei hier auf das Modul “unicodedata” hingewiesen, das
# in der Python-Standardinstallation enthalten ist:

import unicodedata

print('Name of \\u03BE:', unicodedata.name('\u03BE'))

# Ich benutze es vor allem zur Vereinheitlichung von Diakritika:

term = 'babhūva'

print('Length of term with separate macron:', len(term))

term2 = unicodedata.normalize('NFC', term) # NFC = Normal Form “Composition”.

print('Length of term with inherent macron:', len(term2))

term3 = unicodedata.normalize('NFD', term2) # NFD = Normal Form “Decomposition”.

print('``term`` and ``term3`` are equal:', term == term3)

# Man kann also Zeichenketten in die Richtung der einen oder in die Richtung der
# anderen Normalform vereinheitlichen. Ein weiterer Schritt zur Verringerung von
# Varianz kann sein, jedes Zeichen, das ein anderes Zeichen als eine festgelegte
# Entsprechung hat, durch dieses andere Zeichen zu ersetzen, zum Beispiel “Ⅰ” –
# was das Zeichen für ‘ROMAN NUMERAL ONE’ ist – durch das stattdessen wohl öfter
# verwendete große “I”.

print('“Ⅰ” and “I” are unequal:', 'Ⅰ' != 'I')

print(
        'Canonicalized “Ⅰ” and “I” are equal:',
        unicodedata.normalize('NFKD', 'Ⅰ') == 'I' # K = Kanonisch
        )
