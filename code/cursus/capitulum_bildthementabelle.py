import itertools
import re
import xml.etree.ElementTree as ET
import __init__
import fs
import xmlhtml

def get_absolute_distance(value_pairs):
    '''
    Measure the distance between the group of first values and the one of second
    values of :param:`value_pairs`. The measure is::

        distance = 0
        for a, b in value_pairs:
            distance += abs(a - b)
        return distance
    '''
    return sum( abs(a - b) for a, b in value_pairs )

def get_euclidean_distance(value_pairs):
    '''
    Measure the distance between the group of first values and the one of second
    values of :param:`value_pairs`. The measure is the known Euclidean distance.
    '''
    return (sum( (a - b)**2 for a, b in value_pairs ))**0.5

def get_jaccardlike_distance(value_pairs):
    '''
    Measure the distance between the group of first values and the one of second
    values of :param:`value_pairs`. The measure is the known Jaccard distance in
    modification as follows:

    - A value may be between 0 and 1. Hence ``1 - abs(a - b)``.
    - A case when both values are 0 should be skipped. Hence ``if (a or b)``.
    '''
    # value_pairs is an exhaustable iterator but has to be used repeatedly, so:
    value_pairs = tuple(value_pairs)
    union_len = sum( 1 for a, b in value_pairs if (a or b))
    intersect_len = sum( 1 - abs(a - b) for a, b in value_pairs if (a or b) )
    return (union_len - intersect_len) / (union_len or 0.1)

def get_scored_distance(value_pairs):
    '''
    Measure the distance between the group of first values and the one of second
    values of :param:`value_pairs`. The measure is taken with scores assigned to
    the different cases of accordance or deviation.
    '''
    return sum( # TODO: Tune the scores.
            -1 if a == 1 and b == 1
            else 1 if (a == 1 and b == 0) or (a == 0 and b == 1)
            else -0.5 if 0 < a < 1 and 0 < b < 1
            else -0.2 if (0 < a < 1 and b == 1) or (a == 1 and 0 < b < 1)
            else 0.2 if (0 < a < 1 and b == 0) or (a == 0 and 0 < b < 1)
            else 0 # i.e. if ``a == 0 and b == 0``.
            for a, b in value_pairs
            )

def get_weighted_distance(value_pairs):
    '''
    Measure the distance between the group of first values and the one of second
    values of :param:`value_pairs`. – The measure is similar to the measure that
    is calculated with :func:`get_scored_distance`, but the assignment of scores
    is replaced with a calculation.
    '''
    return sum( (abs(a - b) - min(a, b)) for a, b in value_pairs )

def get_adjusted_distance(value_pairs):
    '''
    Measure the distance between the group of first values and the one of second
    values of :param:`value_pairs`. – The measure is similar to the measure that
    is calculated with :func:`get_weighted_distance`, but instead of an absolute
    value the square is taken, hence ``min(a, b)`` is replaced with ``a * b``.
    '''
    return sum( ((a - b)**2 - a * b) for a, b in value_pairs )

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    """
    Consider the element :param:`tag` with :param:`attrs`; return:

    - either ``None``: Then nothing is changed.
    - or a string, possibly the empty string: Then the whole element
        will be replaced with this string **unescaped**.

    :param startend: ``True``, if the element is self-closing.
    """
    if tag == 'head':
        return ''
    else:
        return None

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    """
    Consider the element :param:`tag` with :param:`attrs`; return:

    1. a string: The tag name will be this string. But if the string
        is the empty string, the whole tag will be deleted.
    2. a dictionary: The attributes will be taken from it. And if it
        is empty, no attributes will be set.
    3. a boolean: If ``True``, the element is made self-closing.

    :param startend: ``True``, if the element is self-closing.
    """
    if tag in {'table', 'td', 'tr'}:
        if 'rowspan' in attrs:
            attrs = {'rowspan': attrs['rowspan']}
        else:
            attrs = {}
    else:
        tag = ''
    return tag, attrs, startend

def main(
        path,
        soffice_exe_path,
        number_of_leading_columns_to_be_skipped = 0,
        number_of_trailing_rows_to_be_skipped = 0,
        compare = get_absolute_distance,
        ):
    path, size = fs.convert(path, soffice_exe_path, '.html')
    doc = fs.read(path)
    doc = xmlhtml.replace(
            doc,
            replace_element = replace_element,
            replace_tag = replace_tag,
            )
    doc = re.sub(r'\n(\s*\n)+', '\n', doc).strip()

    # Convert htmlxml table to list of rows:
    rows = []
    table = ET.XML(doc)
    for row in table.iterfind('./tr'):
        rows.append([])
        for cell in row.iterfind('./td'):
            text = (cell.text or '').strip()
            span = int(cell.get('rowspan', '1'))
            rows[-1].append((text, span))
    rows = rows[:len(rows) - number_of_trailing_rows_to_be_skipped]

    # Insert merge placeholders:
    for row_num, row in enumerate(rows):
        for col_num, (text, span) in enumerate(row):
            for next_row_num in range(row_num + 1, row_num + span):
                rows[next_row_num].insert(col_num, (None, 1))

    # Columnize and quantify:
    witnesses = []
    cols = []
    for col_num, col in enumerate(zip(*rows), start = 1):
        if col_num > number_of_leading_columns_to_be_skipped:
            witness = col[0][0]
            witnesses.append(witness)
            cols.append([])
            row_num = 1
            while row_num < len(col):
                term, span = col[row_num]
                if term is not None:
                    value = (1 if term else 0) / span
                cols[-1].append(value)
                row_num += 1

    # Compare each column with every other column using :param:`get_deviation`:
    comparisons = []
    for a_num, b_num in itertools.combinations(range(len(witnesses)), 2):
        comparisons.append((
                compare(zip(cols[a_num], cols[b_num])),
                witnesses[a_num],
                witnesses[b_num],
                ))
    return comparisons

if __name__ == '__main__':
    # Substitute the path that is true for your environment here:
    path = r'C:\Users\M\A\ablage\exempla_py\Bildthementabelle.xlsx'
    # Substitute the path that is true for your environment here:
    soffice_exe_path = r'C:\Program Files\LibreOffice\program\soffice.exe'
    # Substitute the number that is true for your xlsx-file here:
    number_of_leading_columns_to_be_skipped = 2
    # Substitute the number that is true for your xlsx-file here:
    number_of_trailing_rows_to_be_skipped = 1
    # Specify the function to be used for measuring the distance:
    compare = get_adjusted_distance

    comparisons = main(
            path,
            soffice_exe_path,
            number_of_leading_columns_to_be_skipped,
            number_of_trailing_rows_to_be_skipped,
            compare = compare,
            )

    for comparison in sorted(comparisons):
        print(comparison)
    print(f'\n{len(comparisons)=}')
