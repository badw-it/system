import os
import re
import time
from urllib.request import Request, urlopen

def get_html(url, encoding = 'utf-8'):
    return urlopen(Request(url)).read().decode(encoding = encoding)

def main(url, dirpath):
    dirpath = os.path.abspath(dirpath) + os.sep
    text = get_html(url)
    for match in re.finditer(
            r'''https://www.deutschestextarchiv.de/book/show/([^"']*)''', text):
        time.sleep(0.5)
        siglum = match.group(1)
        url = f'https://www.deutschestextarchiv.de/book/download_html/{siglum}'
        text = get_html(url)
        filepath = f'{dirpath}{siglum}.html'
        with open(filepath, 'w', encoding = 'utf-8') as file:
            print(filepath, file.write(text))

if __name__ == '__main__':
    main(
            'https://www.deutschestextarchiv.de/list',
            r'C:\Users\M\A\ablage\exempla_py\corpus_dta',
            )
