# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 9
# ===========

# Das Abrufen und Verarbeiten von Netzseiten 1: Handwerkliches
# ------------------------------------------------------------

import collections
import os
import re
import time
import urllib.request

dirpath = r'C:\Users\M\A\ablage\exempla_py\lta'
# Wie üblich anzupassen: ein Pfad zu einem Ordner, darin die Dateien gespeichert
# werden können, die wir im Folgenden herunterladen.

# Zur Gewinnung von Inhalten aus dem Netz gibt es eine kleine Rezeptsammlung auf
# der Python-Dokumentationseite (https://docs.python.org/3/howto/urllib2.html).

# Netzseiten kann man ähnlich aufrufen wie Dateien öffnen:

with urllib.request.urlopen('https://lta.bbaw.de/list/by/author') as response:
    # Python sendet hier eine Anfrage an die Adresse ``url``; die Antwort stellt
    # eine eigene Art von Wert dar, hier ``response`` genannt.

    # Unter Umständen gibt Python die Fehlermeldung aus, dass das SSL-Zertifikat
    # der Seite nicht verifiziert werden konnte. Dazu und zu einer Lösung siehe:
    # https://stackoverflow.com/questions/27835619/urllib-and-ssl-certificate-verify-failed-error#42334357

    text = response.read().decode(encoding = 'utf-8')
    # Die Methode ``read`` des ``response``-Objektes gibt Bytes zurück. Um diese
    # Bytes in einen Text umwandeln zu können, muss man den Zeichenschlüssel der
    # Seite in Erfahrung bringen: Er sollte innert der ersten 1024 Bytes stehen;
    # man findet ihn in der Regel am einfachsten, indem man in einem Browser auf
    # die Seite geht und sich den Quelltext anzeigen lässt (mit “Strg u” o.ä.) –
    # im Fall der vorliegenden Seite etwa liest man da “<meta charset="utf-8">”.
    # Nun kann man die von ``read`` zurückgegebenen Bytes mit ``decode`` und der
    # Zeichenschlüsselangabe in einen Strang von Buchstaben umwandeln. Nebenbei:
    # Netzseiten sollten als Zeichenschlüssel am besten UTF-8 verwenden; so auch
    # ist der Grundwert des ``encoding``-Argumentes “utf-8”. Man kann also statt
    # ``decode(encoding = 'utf-8')`` auch ``decode()`` schreiben.

# Aus dem so erhaltenen Text wollen wir alle URLs zu den Werkseiten fischen. Die
# URLs scheinen sich dadurch auszuzeichnen, dass sie alle einen ähnlichen Anfang
# haben.

pattern = r'''<a\s+href\s*=\s*["'](https://lta\.bbaw\.de/text/show/[^'"]+)'''

urls_by_author = []
for match in re.finditer(pattern, text):
    urls_by_author.append(match.group(1))

print('Number of URLs found on list/by/author:', len(urls_by_author))

# Vergleichen wir es mit der Linkmenge, die eine anders gruppierte Seite bietet:

with urllib.request.urlopen('https://lta.bbaw.de/list/by/texttype') as response:
    text = response.read().decode(encoding = 'utf-8')

urls_by_texttype = []
for match in re.finditer(pattern, text):
    urls_by_texttype.append(match.group(1))

print('Number of URLs found on list/by/texttype:', len(urls_by_author))

# Allerdings könnte die Seite eine URL mehrmals aufgeführt haben – vielleicht im
# Fall unklarer Autorschaft oder unklaren Texttyps – und diese Dublette würde in
# unserer Liste dann ebenfalls enthalten sein. Daher formen wir die Listen um in
# eine andere Art von Wert: eine Menge (``set``). Mengen speichern ihre Elemente
# (anders als Listen) ohne Dubletten und ohne bestimmte Reihenfolge ab.

distinct_urls_by_author = set(urls_by_author)
distinct_urls_by_texttype = set(urls_by_texttype)

print(
        'Number of distinct URLs found on list/by/author:',
        len(distinct_urls_by_author))
print(
        'Number of distinct URLs found on list/by/texttype:',
        len(distinct_urls_by_texttype))

# Bei mir geben die ersteren beiden ``print``s “13307” aus, die zweite ``12807``
# und die dritte ``12809``! Es sind also tatsächlich Dubletten vorhanden – zudem
# verschieden viele je nach befragter Seite. Welche URLs nur auf der einen Seite
# auftauchen und nicht auf der anderen, kann man mit Mengen und den Anweisungen,
# die Mengen als Argumente nehmen, leicht herausfinden: mit Differenzbildung.

print(
        'URLs on list/by/author and not on list/by/texttype:',
        distinct_urls_by_author - distinct_urls_by_texttype)

print(
        'URLs on list/by/texttype and not on list/by/author:',
        distinct_urls_by_texttype - distinct_urls_by_author)

# https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset bietet
# weitere Auskunft zu Mengen und ihnen zugeordneten Anweisungen.

# Wer mag, kann übungshalber noch herauszufinden suchen, welche der URLs mehrere
# Male vorkommen (dafür braucht man wiederum die Listen, nicht die Mengen), etwa
# mit einem ``defaultdict`` wie beim Nibelungenbeispiel.

# Die Betrachtung einiger Beispiele für Mehrfachvorkommen zeigt, ein Grund dafür
# ist eine Mehrfachzuordnung: Eine URL etwa, der sieben Autoren zugeordnet sind,
# kommt siebenmal in der nach Autor gruppierten Liste vor. Das ist an sich nicht
# überraschend, wohl aber, dass sieben Autoren zugeordnet sind. Kenner im Kurs –
# namentlich Isabelle – haben bei der Gelegenheit entdeckt, dass wohl regelmäßig
# Personen als Autor in den Metadaten auftauchen, die gar keine Verfasser waren,
# sondern beschlossen oder beurkundet haben. Mit solchen Mängeln digitaler Daten
# muss man immer rechnen, da es Digitalisierern oft an Fachkenntnissen mangelt –
# oder eine Berücksichtigung mancher Feinheiten in Programmen zu aufwändig wäre.

# Zum Schluss gehen wir nun die größere Menge der URLs durch, rufen zu jeder die
# Netzseite auf und entnehmen ihr die URL zum XML-Text des Werkes. Vor jeglicher
# Seite halten wir mit ``time.sleep`` kurz inne – um die angefragte Stelle nicht
# übermäßig in Anspruch zu nehmen. Außerdem sammeln wir die URLs diesmal in eine
# Menge ein, um von vornherein Dubletten auszuschließen. Statt ``append`` müssen
# wir also die entsprechende Anweisung für Mengen nehmen: ``add``.

distinct_urls_of_docs = set()
max_num_of_texts = 5 # or ``None`` if you want to collect *all* ca. 13000 texts.
pattern = r'''<a\s+href\s*=\s*["'](https://lta.bbaw.de/text/fullxml/[^'"]+)'''
for url in tuple(distinct_urls_by_texttype)[:max_num_of_texts]:
    time.sleep(0.2)
    with urllib.request.urlopen(url) as response:
        text = response.read().decode()
    match = re.search(pattern, text)
    if match:
        doc_url = match.group(1)
        if doc_url not in distinct_urls_of_docs:
            distinct_urls_of_docs.add(doc_url)
            with urllib.request.urlopen(doc_url) as response:
                doc_bytes = response.read()
            # We write the bytes just as they are in a file, hence no decoding.
            filename = doc_url.rstrip('/').rsplit('/', 1)[-1] + '.xml'
            filepath = os.path.join(dirpath, filename)
            with open(filepath, 'wb') as file: # ``'wb'``: ‘write bytes’
                size = file.write(doc_bytes)
            print(filepath, size)

# Das Abrufen und Verarbeiten von Netzseiten 1: Urheberrechtliches
# ----------------------------------------------------------------

# Das `Urheberrechtsgesetz <https://www.gesetze-im-internet.de/urhg/>`_ hat wohl
# unter anderem folgende Stellen dazu – Auswahl, Verlinkungen sowie zwischen “⟦”
# und “⟧” Gestelltes sind von mir –:
#
# `§ 15 Allgemeines <https://www.gesetze-im-internet.de/urhg/__15.html>`_:
#
# “(1) Der Urheber hat das ausschließliche Recht, sein Werk in körperlicher Form
# zu verwerten; das Recht umfaßt insbesondere 1. das Vervielfältigungsrecht
# (§ 16), 2. das Verbreitungsrecht (§ 17), 3. das Ausstellungsrecht (§ 18).”
#
# `§ 16 Vervielfältigungsrecht <https://www.gesetze-im-internet.de/urhg/__16.html>`_:
#
# “(1) Das Vervielfältigungsrecht ist das Recht, Vervielfältigungsstücke des
# Werkes herzustellen, gleichviel ob vorübergehend oder dauerhaft, in welchem
# Verfahren und in welcher Zahl.”
#
# `§ 23 Bearbeitungen und Umgestaltungen <https://www.gesetze-im-internet.de/urhg/__23.html>`_:
#
# “(1) Bearbeitungen oder andere Umgestaltungen eines Werkes, insbesondere auch
# einer Melodie, dürfen nur mit Zustimmung des Urhebers veröffentlicht oder
# verwertet werden. Wahrt das neu geschaffene Werk einen hinreichenden Abstand
# zum benutzten Werk, so liegt keine Bearbeitung oder Umgestaltung im Sinne des
# Satzes 1 vor. (2) Handelt es sich um ⟦…⟧ 4. die Bearbeitung oder Umgestaltung
# eines Datenbankwerkes, so bedarf bereits das Herstellen der Bearbeitung oder
# Umgestaltung der Zustimmung des Urhebers. (3) Auf ausschließlich technisch
# bedingte Änderungen eines Werkes bei Nutzungen nach § 44b Absatz 2 ⟦s.u.⟧,
# § 60d Absatz 1 ⟦s.u.⟧, § 60e Absatz 1 sowie § 60f Absatz 2 sind die Absätze 1
# und 2 nicht anzuwenden.”
#
# `§ 44a Vorübergehende Vervielfältigungshandlungen <https://www.gesetze-im-internet.de/urhg/__44a.html>`_:
#
# “Zulässig sind vorübergehende Vervielfältigungshandlungen, die flüchtig oder
# begleitend sind und einen integralen und wesentlichen Teil eines technischen
# Verfahrens darstellen und deren alleiniger Zweck es ist, 1. eine Übertragung
# in einem Netz zwischen Dritten durch einen Vermittler oder 2. eine rechtmäßige
# Nutzung eines Werkes oder sonstigen Schutzgegenstands zu ermöglichen, und die
# keine eigenständige wirtschaftliche Bedeutung haben.”
#
# `§ 44b Text und Data Mining <https://www.gesetze-im-internet.de/urhg/__44b.html>`_:
#
# “(1) Text und Data Mining ist die automatisierte Analyse von einzelnen oder
# mehreren digitalen oder digitalisierten Werken, um daraus Informationen
# insbesondere über Muster, Trends und Korrelationen zu gewinnen. (2) Zulässig
# sind Vervielfältigungen von rechtmäßig zugänglichen Werken für das Text und
# Data Mining. Die Vervielfältigungen sind zu löschen, wenn sie für das Text und
# Data Mining nicht mehr erforderlich sind.”
#
# `§ 51 Zitate <https://www.gesetze-im-internet.de/urhg/__51.html>`_:
#
# “Zulässig ist die Vervielfältigung, Verbreitung und öffentliche Wiedergabe
# eines veröffentlichten Werkes zum Zweck des Zitats, sofern die Nutzung in
# ihrem Umfang durch den besonderen Zweck gerechtfertigt ist. Zulässig ist dies
# insbesondere, wenn 1. einzelne Werke nach der Veröffentlichung in ein
# selbständiges wissenschaftliches Werk zur Erläuterung des Inhalts aufgenommen
# werden, 2. Stellen eines Werkes nach der Veröffentlichung in einem
# selbständigen Sprachwerk angeführt werden, 3. einzelne Stellen eines
# erschienenen Werkes der Musik in einem selbständigen Werk der Musik angeführt
# werden. Von der Zitierbefugnis gemäß den Sätzen 1 und 2 umfasst ist die
# Nutzung einer Abbildung oder sonstigen Vervielfältigung des zitierten Werkes,
# auch wenn diese selbst durch ein Urheberrecht oder ein verwandtes Schutzrecht
# geschützt ist.”
#
# `§ 53 Vervielfältigungen zum privaten und sonstigen eigenen Gebrauch <https://www.gesetze-im-internet.de/urhg/__53.html>`_:
#
# “(1) Zulässig sind einzelne Vervielfältigungen eines Werkes durch eine
# natürliche Person zum privaten Gebrauch auf beliebigen Trägern, sofern sie
# weder unmittelbar noch mittelbar Erwerbszwecken dienen, soweit nicht zur
# Vervielfältigung eine offensichtlich rechtswidrig hergestellte oder öffentlich
# zugänglich gemachte Vorlage verwendet wird. Der zur Vervielfältigung Befugte
# darf die Vervielfältigungsstücke auch durch einen anderen herstellen lassen,
# sofern dies unentgeltlich geschieht oder es sich um Vervielfältigungen auf
# Papier oder einem ähnlichen Träger mittels beliebiger photomechanischer
# Verfahren oder anderer Verfahren mit ähnlicher Wirkung handelt.
#
# (2) Zulässig ist, einzelne Vervielfältigungsstücke eines Werkes herzustellen
# oder herstellen zu lassen ⟦…⟧
#
# (3) (weggefallen)
#
# (4) Die Vervielfältigung a) graphischer Aufzeichnungen von Werken der Musik,
# b) eines Buches oder einer Zeitschrift, wenn es sich um eine im wesentlichen
# vollständige Vervielfältigung handelt, ist, soweit sie nicht durch Abschreiben
# vorgenommen wird, stets nur mit Einwilligung des Berechtigten zulässig oder
# unter den Voraussetzungen des Absatzes 2 Satz 1 Nr. 2 oder zum eigenen
# Gebrauch, wenn es sich um ein seit mindestens zwei Jahren vergriffenes Werk
# handelt.
#
# (5) Die Absätze 1 und 2 Satz 1 Nr. 2 bis 4 finden keine Anwendung auf
# Datenbankwerke, deren Elemente einzeln mit Hilfe elektronischer Mittel
# zugänglich sind.”
#
# `§ 55a Benutzung eines Datenbankwerkes <https://www.gesetze-im-internet.de/urhg/__55a.html>`_:
#
# “Zulässig ist die Bearbeitung sowie die Vervielfältigung eines Datenbankwerkes
# durch den Eigentümer eines mit Zustimmung des Urhebers durch Veräußerung in
# Verkehr gebrachten Vervielfältigungsstücks des Datenbankwerkes, den in
# sonstiger Weise zu dessen Gebrauch Berechtigten oder denjenigen, dem ein
# Datenbankwerk aufgrund eines mit dem Urheber oder eines mit dessen Zustimmung
# mit einem Dritten geschlossenen Vertrags zugänglich gemacht wird, wenn und
# soweit die Bearbeitung oder Vervielfältigung für den Zugang zu den Elementen
# des Datenbankwerkes und für dessen übliche Benutzung erforderlich ist. Wird
# aufgrund eines Vertrags nach Satz 1 nur ein Teil des Datenbankwerkes
# zugänglich gemacht, so ist nur die Bearbeitung sowie die Vervielfältigung
# dieses Teils zulässig. Entgegenstehende vertragliche Vereinbarungen sind
# nichtig.”
#
# `§ 60a Unterricht und Lehre <https://www.gesetze-im-internet.de/urhg/__60a.html>`_:
#
# “(1) Zur Veranschaulichung des Unterrichts und der Lehre an
# Bildungseinrichtungen dürfen zu nicht kommerziellen Zwecken bis zu 15 Prozent
# eines veröffentlichten Werkes vervielfältigt, verbreitet, öffentlich
# zugänglich gemacht und in sonstiger Weise öffentlich wiedergegeben werden
#
# 1. für Lehrende und Teilnehmer der jeweiligen Veranstaltung,
#
# 2. für Lehrende und Prüfer an derselben Bildungseinrichtung sowie
#
# 3. für Dritte, soweit dies der Präsentation des Unterrichts, von Unterrichts-
# oder Lernergebnissen an der Bildungseinrichtung dient.
#
# (2) Abbildungen, einzelne Beiträge aus derselben Fachzeitschrift oder
# wissenschaftlichen Zeitschrift, sonstige Werke geringen Umfangs und
# vergriffene Werke dürfen abweichend von Absatz 1 vollständig genutzt werden.
#
# (3) Nicht nach den Absätzen 1 und 2 erlaubt sind folgende Nutzungen:
#
# 1. Vervielfältigung durch Aufnahme auf Bild- oder Tonträger und öffentliche
# Wiedergabe eines Werkes, während es öffentlich vorgetragen, aufgeführt oder
# vorgeführt wird,    
#
# 2. Vervielfältigung, Verbreitung und öffentliche Wiedergabe eines Werkes, das
# ausschließlich für den Unterricht an Schulen geeignet, bestimmt und
# entsprechend gekennzeichnet ist, an Schulen sowie
#
# 3. Vervielfältigung von grafischen Aufzeichnungen von Werken der Musik, soweit
# sie nicht für die öffentliche Zugänglichmachung nach den Absätzen 1 oder 2
# erforderlich ist. 
#
# Satz 1 ist nur anzuwenden, wenn Lizenzen für diese Nutzungen leicht verfügbar
# und auffindbar sind, den Bedürfnissen und Besonderheiten von
# Bildungseinrichtungen entsprechen und Nutzungen nach Satz 1 Nummer 1 bis 3
# erlauben.
#
# (3a) Werden Werke in gesicherten elektronischen Umgebungen für die in Absatz 1
# Nummer 1 und 2 sowie Absatz 2 genannten Zwecke in Mitgliedstaaten der
# Europäischen Union und Vertragsstaaten des Abkommens über den Europäischen
# Wirtschaftsraum genutzt, so gilt diese Nutzung nur als in dem Mitgliedstaat
# oder Vertragsstaat erfolgt, in dem die Bildungseinrichtung ihren Sitz hat.
#
# (4) Bildungseinrichtungen sind frühkindliche Bildungseinrichtungen, Schulen,
# Hochschulen sowie Einrichtungen der Berufsbildung oder der sonstigen Aus- und
# Weiterbildung.”
#
# `§ 60c Wissenschaftliche Forschung <https://www.gesetze-im-internet.de/urhg/__60c.html>`_:
#
# “(1) Zum Zweck der nicht kommerziellen wissenschaftlichen Forschung dürfen bis
# zu 15 Prozent eines Werkes vervielfältigt, verbreitet und öffentlich
# zugänglich gemacht werden 1. für einen bestimmt abgegrenzten Kreis von
# Personen für deren eigene wissenschaftliche Forschung sowie 2. für einzelne
# Dritte, soweit dies der Überprüfung der Qualität wissenschaftlicher Forschung
# dient. (2) Für die eigene wissenschaftliche Forschung dürfen bis zu 75 Prozent
# eines Werkes vervielfältigt werden. (3) Abbildungen, einzelne Beiträge aus
# derselben Fachzeitschrift oder wissenschaftlichen Zeitschrift, sonstige Werke
# geringen Umfangs und vergriffene Werke dürfen abweichend von den Absätzen 1
# und 2 vollständig genutzt werden. (4) Nicht nach den Absätzen 1 bis 3 erlaubt
# ist es, während öffentlicher Vorträge, Aufführungen oder Vorführungen eines
# Werkes diese auf Bild- oder Tonträger aufzunehmen und später öffentlich
# zugänglich zu machen.”
#
# `§ 60d Text und Data Mining für Zwecke der wissenschaftlichen Forschung <https://www.gesetze-im-internet.de/urhg/__60d.html>`_:
#
# “(1) Vervielfältigungen für Text und Data Mining (`§ 44b Absatz 1 und 2
# Satz 1 ⟦s.o.⟧) sind für Zwecke der wissenschaftlichen Forschung nach Maßgabe
# der nachfolgenden Bestimmungen zulässig.
#
# (2) Zu Vervielfältigungen berechtigt sind Forschungsorganisationen.
# Forschungsorganisationen sind Hochschulen, Forschungsinstitute oder sonstige
# Einrichtungen, die wissenschaftliche Forschung betreiben, sofern sie
#
# 1. nicht kommerzielle Zwecke verfolgen,
#
# 2. sämtliche Gewinne in die wissenschaftliche Forschung reinvestieren oder
#
# 3. im Rahmen eines staatlich anerkannten Auftrags im öffentlichen Interesse
# tätig sind.
#
# Nicht nach Satz 1 berechtigt sind Forschungsorganisationen, die mit einem
# privaten Unternehmen zusammenarbeiten, das einen bestimmenden Einfluss auf die
# Forschungsorganisation und einen bevorzugten Zugang zu den Ergebnissen der
# wissenschaftlichen Forschung hat.
#
# (3) Zu Vervielfältigungen berechtigt sind ferner
#
# 1. Bibliotheken und Museen, sofern sie öffentlich zugänglich sind, sowie
# Archive und Einrichtungen im Bereich des Film- oder Tonerbes
# (Kulturerbe-Einrichtungen),
#
# 2. einzelne Forscher, sofern sie nicht kommerzielle Zwecke verfolgen.
#
# (4) Berechtigte nach den Absätzen 2 und 3, die nicht kommerzielle Zwecke
# verfolgen, dürfen Vervielfältigungen nach Absatz 1 folgenden Personen
# öffentlich zugänglich machen:
#
# 1. einem bestimmt abgegrenzten Kreis von Personen für deren gemeinsame
# wissenschaftliche Forschung sowie
#
# 2. einzelnen Dritten zur Überprüfung der Qualität wissenschaftlicher
# Forschung.
#
# Sobald die gemeinsame wissenschaftliche Forschung oder die Überprüfung der
# Qualität wissenschaftlicher Forschung abgeschlossen ist, ist die öffentliche
# Zugänglichmachung zu beenden.
#
# (5) Berechtigte nach den Absätzen 2 und 3 Nummer 1 dürfen Vervielfältigungen
# nach Absatz 1 mit angemessenen Sicherheitsvorkehrungen gegen unbefugte
# Benutzung aufbewahren, solange sie für Zwecke der wissenschaftlichen
# Forschung oder zur Überprüfung wissenschaftlicher Erkenntnisse erforderlich
# sind.
#
# (6) Rechtsinhaber sind befugt, erforderliche Maßnahmen zu ergreifen, um zu
# verhindern, dass die Sicherheit und Integrität ihrer Netze und Datenbanken
# durch Vervielfältigungen nach Absatz 1 gefährdet werden.”
#
# `§ 60h Angemessene Vergütung der gesetzlich erlaubten Nutzungen <https://www.gesetze-im-internet.de/urhg/__60h.html>`_:
#
# “(2) Folgende Nutzungen sind abweichend von Absatz 1 vergütungsfrei:
#
# ⟦…⟧
#
# 3. Vervielfältigungen im Rahmen des Text und Data Mining für Zwecke der
# wissenschaftlichen Forschung nach § 60d Absatz 1.”
#
# `§ 87c Schranken des Rechts des Datenbankherstellers <https://www.gesetze-im-internet.de/urhg/__87c.html>`_:
#
# “(1) Die Vervielfältigung eines nach Art oder Umfang wesentlichen Teils einer
# Datenbank ist zulässig
#
# 1. zum privaten Gebrauch; dies gilt nicht für eine Datenbank, deren Elemente
# einzeln mit Hilfe elektronischer Mittel zugänglich sind,
#
# 2. zu Zwecken der wissenschaftlichen Forschung gemäß § 60c,
#
# 3. zu Zwecken der Veranschaulichung des Unterrichts und der Lehre gemäß den
# §§ 60a und 60b,
#
# 4. zu Zwecken des Text und Data Mining gemäß § 44b,
#
# 5. zu Zwecken des Text und Data Mining für Zwecke der wissenschaftlichen
# Forschung gemäß § 60d,
#
# 6. zu Zwecken der Erhaltung einer Datenbank gemäß § 60e Absatz 1 und 6 und
# § 60f Absatz 1 und 3.
#
# (2) Die Vervielfältigung, Verbreitung und öffentliche Wiedergabe eines nach
# Art oder Umfang wesentlichen Teils einer Datenbank ist zulässig zur Verwendung
# in Verfahren vor einem Gericht, einem Schiedsgericht oder einer Behörde sowie
# für Zwecke der öffentlichen Sicherheit.
#
# (3) Die §§ 45b bis 45d sowie 61d bis 61g gelten entsprechend.
#
# (4) Die digitale Verbreitung und digitale öffentliche Wiedergabe eines nach
# Art oder Umfang wesentlichen Teils einer Datenbank ist zulässig für Zwecke der
# Veranschaulichung des Unterrichts und der Lehre gemäß § 60a.
#
# (5) Für die Quellenangabe ist § 63 entsprechend anzuwenden.
#
# (6) In den Fällen des Absatzes 1 Nummer 2, 3, 5 und 6 sowie des Absatzes 4 ist
# § 60g Absatz 1 entsprechend anzuwenden.”
