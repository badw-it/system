import contextlib
import re
import sqlite3
import unicodedata

def prepare_bib_items(doc):
    '''
    From :param:`doc`, extract the bibliographical items and preprocess them for
    being ingested into a database:

    - Split at double newlines (with or without other whitespace around them).
    - Apply the Unicode canonicalization.
    - Replace or remove some characters or strings.

    Return the split and preprocessed items.
    '''
    doc = doc.strip()
    items = []
    for line in re.split(r'\s*\n\s*\n\s*', doc):
        line = unicodedata.normalize('NFKD', line)
        for old, new in (
                ('<', '['),
                ('>', ']'),
                (r'\s+', ' '),
                (r'¬', ''),
                (r'\.\s*→', ' →'),
                (r'(\df?f) →', '\g<1>. →'),
                (r'[\s.]+$', ''),
                (r'→(.*)', '(MWG\g<1>)'),
                (r'(Aufl|\df?f)\)', '\g<1>.)'),
                (r'(\d)-(\d)', '\g<1>–\g<2>'),
                (r'([a-zß])-([a-zß])', '\g<1>\g<2>'),
                (r'''(?x)
                    ^
                    A?\s*
                    \d\d\d\d
                    (
                        (
                            [-\s]+
                            [aAIVXLCDM]+
                        )?
                        [-\s/]+
                        (\d+|Ex)
                    )?
                    \s+
                    ''', ''),
                (r'^[\s\d]+\[', '['),
                (r'(?<!\.)$', '.'),
                ):
            line = re.sub(old, new, line)
        line = line.strip()
        line = unicodedata.normalize('NFC', line)
        items.append(line)
    return items

def insert_into_db(bib_items, dbpath):
    '''
    Insert all :param:`bib_items` into a database at :param:`dbpath`.

    If a database is not already there, make one, and create a table in it which
    adheres to the requirements specified by the project: https://mwg.badw.de
    '''
    with contextlib.closing(sqlite3.connect(dbpath)) as con:
        cur = con.cursor()
        cur.execute("""
                CREATE TABLE if not exists `mastertable_literaturverzeichnis` (
                    'worktitle.id' TEXT,
                    'shorttitle.prefLabel' TEXT,
                    'shorttitle.altLabel' TEXT,
                    'shorttitle.hiddenLabel' TEXT,
                    'worktitle.creator' TEXT,
                    'worktitle.creator.id' TEXT,
                    'worktitle.prefLabel' TEXT,
                    'worktitle.altLabel' TEXT,
                    'worktitle.hiddenLabel' TEXT,
                    'worktitle.scopeNote' TEXT,
                    'Form_der_Publikation' TEXT,
                    'Textkategorie' TEXT,
                    'Temporalitaet' TEXT,
                    'Editorische_Referenzierung_von_Webers_Literaturangaben' TEXT,
                    'Sprache' TEXT,
                    'worktitle.editorMention' TEXT,
                    'worktitle.weberMention' TEXT,
                    'inQuellreg' TEXT,
                    'Autorenschaft' TEXT,
                    'Autorisierung' TEXT,
                    'Dokumentenart' TEXT,
                    'Editionsbereich' TEXT,
                    'Textfassung' TEXT,
                    'Editionstext_in_MWG' TEXT,
                    'EdBericht_in_MWG' TEXT,
                    'Normdaten' TEXT,
                    'hasTranslation' TEXT,
                    'hasScan' TEXT,
                    'inSammeledition' TEXT,
                    'worktitle.keywords' TEXT,
                    'note' TEXT
                    )""")
        for bib_item in bib_items:
            cur.execute("""
                    insert into mastertable_literaturverzeichnis
                        (`worktitle.prefLabel`) values
                        (?)
                    """, (bib_item,))
        con.commit()

def main(filepath, dbpath):
    with open(filepath, encoding = 'utf-8') as file:
        doc = file.read()
    bib_items = prepare_bib_items(doc)
    insert_into_db(bib_items, dbpath)

if __name__ == '__main__':
    filepath = r"C:\Users\M\A\ablage\exempla_py\mwg-literatur.txt"
    # this file can be obtained at https://dienst.badw.de/varia/exempla.zip
    dbpath = r"C:\Users\M\A\ablage\exempla_py\mwg-literatur.sqlite"
    main(filepath, dbpath)
