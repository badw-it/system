'''
Download of an article of http://gorazd.org/gulliver
'''
import json
import os
import re
import urllib.request

import __init__
import xmlhtml

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    """
    Consider the element :param:`tag` with :param:`attrs`; return:

    1. a string: The tag name will be this string. But if the string
       is the empty string, the whole tag will be deleted.
    2. a dictionary: The attributes will be taken from it. And if it
       is empty, no attributes will be set.
    3. a boolean: If ``True``, the element is made self-closing.

    :param startend: ``True``, if the element is self-closing.
    """
    if 'italics' in attrs.get('aip-style', ''):
        tag = 'i'
        attrs = {}
    elif tag == 'p':
        attrs = {}
    elif tag == 'sup':
        attrs = {}
    else:
        tag = ''
    return tag, attrs, startend

def insert_spaces(doc):
    '''
    Replace ``'</span>'`` with a space, delete all other tags.
    '''
    doc = re.sub(r'</span\s*>', '</span> ', doc)
    return doc

def show_keys(data, indent = 0):
    for key in data:
        print(indent * '\t', key)
        if isinstance(data[key], dict):
            show_keys(data[key], indent = indent + 1)

if __name__ == '__main__':
    # Substitute the path that is true for your environment here:
    outpath_template = '/Users/M/A/ablage/aksl_lem_{}.html'

    article_url = input(
            'Lemma-URL (wie http://gorazd.org/gulliver/?recordId=4772) nennen: '
            )
    json_url_template = \
            'http://castor.gorazd.org:8080/gorazd/show_record_id?value={}'
    article_id = re.search(r'(?i)record_?id=(\d+)', article_url).group(1)

    outpath = outpath_template.format(article_id)
    json_url = json_url_template.format(article_id)

    with urllib.request.urlopen(json_url) as response:
        data = json.loads(response.read().decode())

##    show_keys(data)

    doc = data['response']['result']['SourceD']

    doc = insert_spaces(doc)
    doc = xmlhtml.replace(doc, replace_tag = replace_tag)
    doc = f'''<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<title>Lemma</title>
</head>
<body>
{doc}
</body>
</html>'''

    with open(outpath, 'w', encoding = 'utf-8') as file:
        size = file.write(doc)

    print(outpath, size)
