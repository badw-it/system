# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by … in 20… ff.
'''
Short description of this module.
'''

import os # example for a built-in module.

import regex # example for a third-party module.

import utilia # example for an own module.

def do_something_general(arg, kwarg = 'default'):
    '''
    Short description of the functionʼs purpose and of its arguments.
    '''
    ...
    return ...

def main(arg, kwarg = 'default'):
    '''
    Short description of the functionʼs purpose and of its arguments.
    '''
    ...
    return ...

if __name__ == '__main__':
    # This condition is met only if this module is not imported from another.
    # Accordingly, we strive for separating more and less general code parts:
    # The most general parts are most reusable and put into functions above.
    # The use of these functions within this module is put into :func:`main`.
    # The least general, most case-dependent parts are put into this block:
    # e.g. setting basic values like paths, calling :func:`main`, print output.
    path = ...
    data = main(path, ...)
    print(data)
