# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 5
# ===========

# Wie am Ende des letzten Kurses erwähnt, werden Funktionsdefinitionen, also die
# ``def``-Blöcke, vor anderen Code geschrieben, der nicht in einem ``def``-Block
# steht.

# Wenn es wahrscheinlich ist, dass man eine Funktion in mehr als einer Datei gut
# verwenden könnte, kann man Folgendes tun:

# 1. Man schreibt die Funktion in eine andere Python-Datei. Als Beispiel gibt es
# in demselben Ordner, aus dem man die vorliegende Zusammenfassung herunterladen
# konnte, eine Datei “utilia.py”. Dieser Name ist freilich eher ein Platzhalter,
# an dessen Stelle man im Echteinsatz einen wählen würde, der die Aufgabe der im
# Modul enthaltenen Funktionen klarer andeutet. Doch muss der Namensteil vor dem
# “.py” jenen Regeln folgen, die im Python-Code für Variablennamen gelten. Grund
# dafür ist, dass der Name in der Tat als Variablenname verwendet werden wird.

# 2. Man legt diese andere Datei neben Python-Dateien, in denen man die Funktion
# nachnutzen will.

# 3. Man schreibt in einer Datei, in der man die Funktion nachnutzen will, einen
# ``import``-Befehl; im Fall der Datei “utilia.py” wäre das:

import utilia

# Wichtig: Diese ``import``-Anweisungen stehen in einer Datei oben, noch vor den
# ``def``-Blöcken, und führen das hinter ``import`` genannte Modul aus, also den
# in ihm enthaltenen Code, hier jenen, der in “utilia.py” steht.

# Daher schreibt man in solche zu importierenden Dateien zumeist eben nichts als
# Funktionsdefinitionen: Sie auszuführen bedeutet zunächst bloß, dass fortan der
# in ihnen enthaltene Code unter dem Namen der Funktion verfügbar ist.

# Dabei zu beachten ist: Nach einem ``import utilia`` wird eine Funktion, die in
# “utilia.py” definiert ist, nach dem sogenannten Namensraum “utilia” verwendbar
# sein: so etwa eine Funktion ``read`` als ``utilia.read``.

# Dadurch weiß man, woher die Funktion kommt und wo man sie nachlesen kann; auch
# ist es keine Schwierigkeit, wenn Funktionen einmal den gleichen Namen erhalten
# sollten, solange sie nur in verschiedenen zu importierenden Dateien stehen und
# daher durch den Namensraum der Dateien unterschieden werden.

# Für das Folgende öffnet man neben dieser Zusammenfassung die Datei “utilia.py”
# und betrachte dort die Funktionen, die in der vorliegenden Datei verwendet und
# mit “utilia.” präfigiert werden.

path = r'C:\Users\M\A\bestand\Mhd\PP\P\III-0-v4.Nib\Nib.txt' # An assignment.

text = utilia.read(path, 'cp437')

items = text.split('\n@H', 1) # A list (a sequence of items in some order).
text = items[1] # Indexing. Slicing is shown below: ``lemmata_numbers[:80]``.
lines = text.strip().split('\n')
lemmata = {} # A dictionary (for key-value-pairs; the keys are unique).

for line in lines: # A ``for``-statement (for repetition).
    lemma = line.split('\t')[0]
    if lemma not in lemmata: # An ``if``-statement (for conditional execution).
        lemmata[lemma] = 1 # Indexing dictionaries is done by keyword.
    else:
        lemmata[lemma] = lemmata[lemma] + 1

# Die schon bekannte Sortierung nach Häufigkeit, nun mit importierter Funktion:

lemmata_numbers = sorted(
        lemmata.items(),
        key = utilia.get_key_for_sorting_by_number,
        reverse = True, # I want to see the most frequent lemma first, not last.
        )

# Die ``key``-Funktion wird beim Ausführen von ``sorted`` mit jedem Wert aus der
# Folge ``lemmata.items()`` aufgerufen und gibt dann einen Wert zurück, nach dem
# im Hintergrund sortiert wird, während die Folge selbst nur ihre ursprünglichen
# Werte weiter enthält. Als Sortierwert gibt man gerne ein Tupel zurück, wenn es
# gilt, eine Vermischung von Sortierebenen zu vermeiden – vgl. die verschiedenen
# Ausgaben der folgenden Sortierungen:

print(sorted(['a9', 'a10']))
print(sorted([('a', 9), ('a', 10)]))

print(sorted(['abc', 'abbe']))
print(sorted([('a', 'bc'), ('ab', 'be')]))

# Nun die vorgeschlagene Sortierung nach der Länge des Lemmas:

lemmata_numbers = sorted(
        lemmata.items(),
        key = utilia.get_key_for_sorting_by_length,
        reverse = True, # I want to see the longest lemma first, not last.
        )

# Dies ist auch ein gutes Beispiel für den möglichen Nutzen darin, eine Funktion
# in eine Datei auszulagern, um sie importieren und dann an vielen verschiedenen
# Stellen nutzen zu können: Denn man muss sie nur an einer Stelle lesen (um ihre
# Wirkung zu verstehen) und verbessern (wenn man hier zum Beispiel genauer weiß,
# wie die Lemmalänge wohl am besten zu messen sei).

# Das Ergebnis wollen wir diesmal übersichtlicher in eine Datei schreiben.

temp = []
for lemma, number in lemmata_numbers:
    # ``lemma, number`` ist “tuple-unpacking”: Ein ``for tu in lemmata_numbers``
    # wiese der Variablen ``tu`` vor jedem Schleifendurchlauf einfach ein ganzes
    # Tupel zu, während ``lemma, number`` die beiden Werte des jeweiligen Tupels
    # gleich auf die beiden Variablen ``lemma`` und ``number`` verteilt. Das ist
    # auch bei normalen Zuweisungen möglich, etwa ``lemma, number = tu``.

    # Aus der Zahl müssen wir eine Zeichenkette machen, um sie mit einer anderen
    # Zeichenkette verketten zu können:
    number = str(number)
    # Die Umkehrfunktion von ``split`` ist ``join``. Aus internen Gründen ist es
    # eine Methode des Strings, der als Bindeglied dient; und als Argument nimmt
    # sie eine Folge von Zeichenketten, die dann von ihr verbunden werden:
    term = ' - '.join((lemma, number))
    # Die so gewonnene Verbundfolge hängen wir unserer neuen Liste an:
    temp.append(term)

# Die Zeichenketten in der neuen Liste verbinden wir nun auch noch:
text = '\n'.join(temp)

# Das Erzeugen eines neuen Pfades für die Ausgabedatei und das Schreiben mit dem
# ``with``-Block sind schon bekannt. Der ``with``-Block ist vorsichtshalber noch
# auskommentiert; man muss die ``##`` entfernen, um das Schreiben auszulösen.

new_path = path + '.txt'
print('Neuer Pfad:', new_path)

##with open(new_path, mode = 'w', encoding = 'utf-8') as file:
##    size = file.write(text)
##    print(size)

# Beim nächsten Mal werden wir in “utilia.py” eine Funktion auch fürʼs Schreiben
# definieren, in der auch gleich dafür gesorgt werden soll, einen auf jeden Fall
# neuen Pfad zu erzeugen.
