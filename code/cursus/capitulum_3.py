# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 3
# ===========

# Wir lesen zunächst wieder unsere Datei so wie beim letzten Mal ein:

path = r'C:\Users\M\A\bestand\Mhd\PP\P\III-0-v4.Nib\Nib.txt'

with open(path, encoding = 'cp437') as file:
    text = file.read()

print(text[:5000])

# Unser Ziel ist, alle Lemmata in diesem Text zu sammeln und dabei die Anzahl zu
# erheben, mit der sie im Text vorkommen.

# Vor dem eigentlichen Text steht ein “Header” mit Metadaten. Wie werden wir ihn
# los? Er endet mit einem “@H” am Anfang einer Zeile – also spalten wir den Text
# am ersten “\n@H” genau einmal auf:

temp = text.split('\n@H', 1)

# ``temp`` ist jetzt eine Liste mit zwei Elementen:
# 1. Eine Zeichenkette mit allem Text bis vor dem ersten “\n@H”.
# 2. Eine Zeichenkette mit allem Text nach dem ersten “\n@H”.
# Das “\n@H” selbst ist in keiner der Zeichenketten mehr enthalten.

# Listen sind ein weiterer Datentyp Pythons, der oft nützlich ist. Wenn man eine
# Liste schreibt oder Python sie auf der Shell anzeigt, dann ist sie umschlossen
# von eckigen Klammern; ihre Elemente sind mit Komma voneinander getrennt:

print(['Eine', 'Liste', 'mit', 'fünf', 'Elementen'])
print('Eine leere Liste:', [])

# Eingeschoben sei hier ein Beispiel mit einer kürzeren Zeichenkette, die besser
# auf der Shell ausgegeben werden kann als die sehr lange unseres Textes:

example = 'Das ist ein Beispiel \n@H mit mehreren “\n@H” experimenti causa'

print(example.split('\n@H', 1))
print(example.split('\n@H'))

# Zurück zu unserem Fall. Von der Liste ``temp`` möchten wir das zweite Element.
# Wir greifen darauf wieder mittels unserer Slicing-Methode mit eckigen Klammern
# zu. Obacht, Python fängt bei ``0`` an zu zählen:

text = temp[1]

print(text[:500])

# So weit, so gut. Am Anfang steht noch eine Leerzeile, die wir löschen wollen –
# jede Zeile soll eine Zeile sein, die ein Lemma enthält. Dafür verwende ich die
# eingebaute Zeichenketten-Methode ``strip``, s. in der Python-Dokumentation den
# Eintrag im “Index” unter “strip() (str Method)”.

text = text.strip()

print(text[:500])

# Nun möchten wir eine Liste der Zeilen haben, spalten also am Zeilenende auf:

lines = text.split('\n')

print(lines[:5])

# Namen für Werte können wir frei vergeben. Es gibt gewisse syntaktische Regeln:
# Namen dürfen nicht mit einer Zahl anfangen und nur Zeichen enthalten, die laut
# Unicode-Standard innerhalb von Wörtern vorkommen können. Mehr oder minder sind
# das der Unterstrich sowie alle Zeichen, die keine Interpunktionszeichen sind –
# was auch die Leerzeichen verbietet. Man muss die genaue Gruppe nicht auswendig
# kennen, sondern verwendet einfach ein Zeichen, das man braucht, und schaut, ob
# der Python-Interpreter es ankreidet (oder eher: anrötelt).

# Um ein Programm übersichtlich und lesbar zu halten, empfiehlt sich, Listen und
# andere Behälter von Werten mit Namen im Plural wie ``lines`` zu benennen.

# Außerdem ist üblich, Leerzeichen nicht einfach fortzulassen, sondern mit einem
# Unterstrich zu ersetzen, vgl. ``extracted_lines``.

# Für unsere Lemmasammlung mit Zählung nutzen wir nun einen andren Datentyp, ein
# sogenanntes “dictionary”. Der Unterschied zur Liste ist: Jedes Element besteht
# aus zwei Gliedern, einem Schlüssel (Key) und einem Wert (Value). Der Schlüssel
# ist einmalig und dient dazu, auf den Wert zuzugreifen (ihn nachzuschlagen) so,
# wie bei Listen die Position. Verwendet wird ebenfalls die Slicing-Syntax:

example = {'Schlüssel 1': 'Wert', 'Schlüssel 2': 'Wert', 'Schlüssel 3': 'Wort'}
print(example)
print(example['Schlüssel 1'])
print(example['Schlüssel 2'])
print(example['Schlüssel 3'])

# Für unsere Sammlung erzeugen wir zunächst ein leeres dictionary:

lemmata = {}

# Dann gehen wir mit einer ``for``-Schleife jede Zeile durch. Diese Schleife ist
# auch eine Art von Block. Also wird wieder eingerückt, um anzuzeigen, wo Anfang
# und Ende des Blockes liegen.

for line in lines:
    # ``line`` ist wieder ein frei zu vergebender Name für einen Wert – wiederum
    # empfiehlt sich ein sprechender Name, etwa der Singular zum Plural.
    # Der Wert, den dieser Name nach dem ``for`` bezeichnet (hier ``line``), ist
    # im ersten Schleifendurchlauf die erste Zeile, im zweiten die zweite usw.
    # Nun spalten wir die jeweilige Zeile an Tabulatoren auf (denn sie sind hier
    # die Feldtrenner, in anderen Dateien mag es anders sein) und nehmen aus der
    # so erzeugten Liste das erste Element und nennen es ``lemma`` (denn das ist
    # in dieser Datei in der Tat das Lemma, in anderen mag es anders sein):
    lemma = line.split('\t')[0]
    # Nun treffen wir eine Fallunterscheidung mit einem ``if``-Block:
    # Wenn das Lemma noch nicht vorkam …
    if lemma not in lemmata:
        # … schreiben wir es als Schlüssel in unser dictionary und weisen ihm zu
        # den Wert ``1`` (für ‘kam bisher einmal vor’):
        lemmata[lemma] = 1
    # Ansonsten (also wenn es schon vorkam) …
    else:
        # … schlagen wir in ``lemmata`` jenen Wert nach, der unter dem Schlüssel
        # verbucht ist, den ``lemma`` jeweils bezeichnet, addieren ``1``, da das
        # Lemma ein weiteres Mal vorkam, und verbuchen den um ``1`` höheren Wert
        # unter demselben Schlüssel:
        lemmata[lemma] = lemmata[lemma] + 1
        # Für diese letztere Anweisung gibt es übrigens eine Kurzschreibweise:
        # ``lemmata[lemma] += 1``

# Hier, nach dem Verlassen der ``for``-Schleife, sind alle Zeilen durchlaufen.

# Wir möchten uns nun die gesammelten Lemmata und ihre Häufigkeiten ausgeben.

# Zunächst wandeln wir das dictionary in eine Reihe von Lemma-Anzahl-Paaren um:

lemmata_numbers = lemmata.items()

# Diese Reihe wandeln wir in eine sortierte Liste um …

lemmata_numbers = sorted(lemmata_numbers)

# … und geben uns die 80 ersten Elemente dieser Liste aus:

for lemma_number in lemmata_numbers[:80]:
    print(lemma_number)
    # Die Elemente dieser Liste, also die Paare aus Lemma und Anzahl, sind keine
    # Listen, sondern sogenannte Tupel – ein weiterer Datentyp Pythons. Sie sind
    # im Unterschied zu Listen von Rundklammern umschlossen – wie die Ausgabe ja
    # zeigt. Inhaltlich aber unterscheiden sie sich von Listen nur dadurch, dass
    # sie unveränderliche Werte sind, Listen veränderliche. Was das heißt, sehen
    # wir später einmal.

# Die Sortierung von Zeichenketten erfolgt schlichtweg nach der Unicode-Zahl der
# Zeichen, daher kommen zunächst alle Großbuchstaben, dann alle Kleinbuchstaben;
# noch später kommen alle Umlaute oder “ß”. Man kann aber – mit mehr oder minder
# großem Aufwand – für eine Sortierung nach anderen, erwünschtern Regeln sorgen;
# das schauen wir uns noch an.

# Weiters liegt nahe, neben der Sortierung nach den Lemmata auch eine Sortierung
# nach den Anzahlen oder der Länge der Lemmata vorzunehmen. Damit fahren wir das
# nächste Mal fort.

# Mehr zu Listen, dictionaries und anderen in Python fest eingebauten Datentypen
# findet man in der Python-Dokumenation im folgenden Abschnitt:
# https://docs.python.org/3/library/stdtypes.html
# Das Inhaltsverzeichnis auf der linken Seite bietet einen Überblick mit Links –
# der zum dictionary-Abschnitt findet sich unter “Mapping Types — dict”.
