# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 6
# ===========

# Gliederung von Python-Dateien
# -----------------------------

# Importe sind, wie gesagt, die ersten Befehle in einer Datei. Daher schon hier,
# was erst unten erläutert und verwendet wird:

import html
import re

# Die Datei “exemplum.py” (neben der vorliegenden zu finden) veranschaulicht den
# mehr oder minder so empfohlenen Grundaufbau von Python-Dateien und eignet sich
# vielleicht als Vorlage für eigene Module.

# Funktion zum Schreiben von Dateien. Docstrings. Import von Modulen anderer
# --------------------------------------------------------------------------

# Das Ziel ist, eine immer wieder verwendbare Funktion zu haben, mit der man auf
# einfache Weise Dateien fürs Schreiben öffnen, beschreiben und schließen kann –
# wobei auch dafür gesorgt sein soll, dass man nicht eine schon vorhandene Datei
# überschreibe.

# Da sie ja immer wieder verwendbar sein soll, schreiben wir die Funktion in die
# Datei (m.a.W.: das Modul) “utilia.py” und importieren es hier:

import utilia

# Siehe die Funktion ``write`` in “utilia.py” – mit einem sogenannten Docstring,
# das ist eine gleich nach der ``def``-Anweisung stehende Zeichenkette. Weil sie
# meist mehrere Zeilen enthält, nimmt man meist die dreifachen Anführungsstriche
# zur Abgrenzung dieser Zeichenkette. In ihr beschreibt man, was die Aufgabe der
# Funktion ist und was ihre Argumente bedeuten. Das ist nützlich, ja nötig, wenn
# die Funktion jemand anders oder auch man selbst nach einiger Zeit noch nutzen,
# berichtigen oder erweitern will.

# Man kann solche Docstrings nach gewissen Regeln schreiben und dann automatisch
# eine Dokumentation erzeugen, siehe https://www.sphinx-doc.org/en/master/ dazu.

# Im Docstring stehen keine Einzelheiten der Umsetzung, dafür sind die mit ``#``
# eingeleiteten Kommentare zu bestimmten Code-Abschnitten besser geeignet.

# In der ``write``-Funktion von “utilia.py” haben wir Befehle, die wir aus einer
# anderen Datei (m.a.W.: einem anderen Modul) her importieren – ähnlich, wie wir
# in dieser vorliegenden Datei ja “utilia” importieren – nur dass wir dort keine
# selbstgeschriebene Datei importieren, sondern ein Modul, das beim Installieren
# von Python gleich mitinstalliert wird: ``import os``, worin wiederum ein Modul
# unter dem Namen ``path`` importiert wird, das die für unsere Zwecke nützlichen
# Befehle enthält – sie stehen uns nun also mit dem Namensraumpräfix ``os.path``
# zur Verfügung. Die Module os und os.path sind in der Python-Dokumentation ganz
# ausführlich beschrieben:

# - https://docs.python.org/3/library/os.html
# - https://docs.python.org/3/library/os.path.html

# In der ``write``-Funktion von “utilia.py” ist nun die zweite Art von Schleifen
# eingeführt, die es in Python gibt: ``while``-Schleifen. Im Unterschied zur uns
# schon bekannten ``for``-Schleife läuft eine ``while``-Schleife nicht über eine
# Reihe von Elementen (etwa in einer Liste), sondern eben so lange, wie die nach
# dem ``while`` angegebene Bedingung ``True`` ist.

# Es folgt ein Test der ``write``-Funktion. Für den ``path`` habe ich einen Pfad
# genommen, den es auf meinem Rechner gibt; diese Angabe ist also – wie üblich –
# anzupassen; die Datei, die im Folgenden als Beispiel verwendet wird, ist neben
# anderen, später zu verwendenden Beispieldateien in folgendem Ordner enthalten:
# https://dienst.badw.de/varia/exempla.zip

path = r'C:\Users\M\A\ablage\exempla_py\reservare.html'

text = 'Test.'
newpath, size = utilia.write(text, path)
print(newpath, size)

# Verarbeiten von docx- oder odt-Dateien. Installation von Modulen anderer
# ------------------------------------------------------------------------

# Auch Dateien, die man mit MS Word, LibreOffice oder ähnlichen Editoren für das
# Bearbeiten von Text, dem Formatierung hinterlegt ist, erstellt, enthalten bloß
# Bytes – ebenso wie die Datei mit dem Nibelungentext, der bisher unser Beispiel
# war. Und um diesen Bytestrom mit Python zu verarbeiten, mussten wir auch schon
# einige Regeln berücksichtigen, nach denen Inhalte in ihm bezeichnet sind: Dazu
# gehören der Zeichenschlüssel (das Encoding) vor allem, dann aber auch, dass es
# einen Kopfteil gibt, der mit “\n@H\n” endet, oder dass jede Form des Textes in
# einer eigenen Zeile steht und jede Zeile durch das Trennzeichen “\t” in Felder
# aufgeteilt ist, von denen das erste das Lemma enthält, und es gäbe noch einige
# mehr, die wir nicht brauchten.

# Aber das Regelwerk von einem Format wie docx ist erheblich viel umfangreicher:
# nämlich mehrere tausend Seiten stark.

# Es gibt mehrere denkbare Ansätze, damit umzugehen:

# 1. Man versucht trotzdem, die Datei mit selbstgeschriebenen Python-Befehlen zu
# verarbeiten. Oft braucht man dafür nicht das ganze Regelwerk. Im Fall von docx
# und odt kann man die Datei entzippen (sie sind eine Art von zip-Datei) und hat
# dann einen Ordner mit xml-Dateien, worin man vielleicht mit nicht allzu großem
# Aufwand findet, was man wie zu verarbeiten habe.

# 2. Vielleicht kann man sich die Datei vom Editor in ein einfacheres Format als
# docx oder odt umwandeln lassen und dann diese Ausgabe verarbeiten – vielleicht
# ist die Umwandlung nicht verlustfrei, aber ausreichend für die eigenen Zwecke.
# Für unser nächstes Beispiel werden wir diesen Weg gehen – und zwar mittels der
# html-Ausgabe von LibreOffice, denn sie ist vergleichsweise übersichtlich, auch
# können sich alle, die mögen, LibreOffice installieren, um sich Ausgaben in dem
# Format zu erzeugen, das wir hier einmal kennen lernen.

# 3. Vielleicht gibt es ein Modul anderer, das Befehle enthält, um docx oder odt
# verarbeiten zu können. Mit diesem Ansatz geht man meist etwa so vor:

# - Im Netz nach Hinweisen auf ein geeignetes Modul suchen.
# - Ein gefundenes geeignetes Modul im “Python Package Index” aufsuchen, so etwa
#   https://pypi.org/project/odfpy/ – hier findet man auch den Befehl, um dieses
#   Modul zu installieren, nämlich “pip install odfpy”, und meist einen Link auf
#   eine Dokumentation, hier https://github.com/eea/odfpy/wiki (was weiter weist
#   auf ein Dokument in einem zu installierenden Ordner).
# - Achtung!! Ein Modul mag auf PyPI liegen und dennoch schädlich, ja gefährlich
#   sein. Es ist kaum möglich, sich dagegen ganz abzusichern. Unwahrscheinlicher
#   ist die Gefahr bei altbekannten und von vielen verwendeten Modulen.
# - Den pip-Befehl in einer Eingabeaufforderung (m.a.W.: einer Shell) ausführen:
#   Unter Windows etwa öffnet man eine Shell, indem man das Startmenü öffnet und
#   “cmd” tipp und die Enter-Taste drückt; nun kann man den “pip install”-Befehl
#   eingeben und wieder Enter drücken. Unter Linux muss man “pip3 install”, also
#   “pip3” statt “pip”, tippen, da “pip” für eine ältere Python-Version da ist.
# - Im eigenen Programm das nun installierte Modul importieren, im Beispiel etwa
#   ``import odf``.
# - Anhand der Dokumentation die geeigneten Befehle finden und einbauen.

# In einer Python-Datei schreibt man erst die Importe jener Module, die schon in
# der Python-Installation enthalten sind, dann die der nachinstallierten Module,
# dann die der selbstgeschriebenen Module. (Innerhalb dieser drei Gruppen ordnet
# man die Importe am besten alphabetisch.)

# Wohldokumentierte Module mit reichem Befehlssatz für schwierige Aufgaben, frei
# verfügbar zudem, sind natürlich ein Glücksfall.

# Für unser Beispiel wählen wir den Weg 2, zumal wir von aller Auszeichnung, die
# dem Text hinterlegt ist, nur die Fettung behalten wollen.

# Die mit LibreOffice erzeugte und vor der Veranstaltung rundgesandte html-Datei
# öffnet man am besten einmal in einem Texteditor wie Visual Studio Code, Emacs,
# Textmate oder Notepad++.

# Man sieht dann im HTML den Zeichenschlüssel: “windows-1252” ziemlich am Anfang
# der Datei. Bei Python heißt dieser Zeichenschlüssel “cp1252”.

text = utilia.read(path, encoding = 'cp1252')

# Für das Folgende brauchen wir Befehle mit besonders reichen Platzhalterzeichen
# (sogenannten Regulären Ausdrücken). Es gibt sie im Modul “re”, das wie “os” in
# Python mitinstalliert wird, aber anders als eingebaute Befehle (wie ``print``)
# importiert werden muss. Eine verbesserte, beschleunigte und um einige wichtige
# Möglichkeiten bereicherte Version ist das Modul “regex”, das erst so, wie oben
# anhand eines anderen Moduls beschrieben, installiert werden muss und dann auch
# importiert werden kann: “pip install regex” (“pip3 install regex” auf Linux) –
# in der Datei dann “import regex”.

# Die Dokumentation von “re” und darin auch aller der Platzhalterzeichen wie “.”
# und “*” und “?” findet sich hier: https://docs.python.org/3/library/re.html

# “regex” ist mit “re” weitgehend kompatibel; die Unterschiede und Erweiterungen
# finden sich hier: https://pypi.org/project/regex/

# Unser Ziel ist, den Text ohne das Kopfteil (das “head”-Element) sowie ohne die
# meisten Auszeichnungen (die Tags) zu erhalten; beibehalten wollen wir aber die
# “b”-Auszeichnung (für Fettung) und die Absätze.

# Das “head”-Element löschen:
text = re.sub(r'(?s)<head>.*?</head>', '', text)

# Schließende Tags außer “b” (wohl aber “body”) und “p” (für Absätze) löschen:
text = re.sub(r'(?s)</(?!b\b|p\b).*?>', '', text)

# Öffnende Tags außer “b” und den “DOCTYPE” löschen:
text = re.sub(r'(?s)<(?!/|b\b).*?>', '', text)

# Alle Whitespace-Zeichen zu Leerzeichen vereinheitlichen; es geht uns dabei vor
# allem um die Newline-Zeichen, die jetzt noch ganz sinnarm im Text stehen:
text = re.sub(r'\s+', ' ', text)

# Die verbliebenen schließenden “p”-Tags zu Newline-Zeichen machen:
text = re.sub(r'</p>', '\n', text)

# Ein Leerzeichen am Ende der Fettung gehört dem Sinn nach nicht mehr dazu:
text = text.replace(' </b>', '</b> ')

# Die letzten verbliebenen Tags durch sonst nicht vorkommende Zeichen ersetzen:
text = text.replace('<b>', '#').replace('</b>', '€')

# Prüfen, ob keine Spitzklammern übrig geblieben sind:
if '<' in text or '>' in text:
    # Mit einer Fehlermeldung abbrechen:
    raise Exception('Error! Es gibt noch Spitzklammern.')
else:
    print('Alle Spitzklammern erfolgreich beseitigt.')

# Da es keine Spitzklammern mehr gibt, kann alles Escaping aufgehoben werden, so
# dass “<” statt der Umschreibung “&lt;” erscheint, “ö” statt “&ouml;” usw.:
text = html.unescape(text)

# Überschüssige Weißzeichen am Anfang und Ende löschen:
text = text.strip()

# Überschüssige Weißzeichen am Anfang und Ende von Zeilen löschen:
text = re.sub(r'\n\s+', '\n', text)
text = re.sub(r'\s+\n', '\n', text)

# Abspeichern:
print(utilia.write(text, path, ext = '.txt'))

# Solch Datenaufbereitung ist übrigens bedeutender, als es scheinen mag: Überall
# erfordert sie den größten Aufwand, von den lichtumflossnen Almen der digitalen
# Geisteswissenschaft bis hinunter ins Bergwerk goldschürfender “data science”.
