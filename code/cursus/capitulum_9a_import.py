# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 9a. Wiederholung und Vertiefung: ``import`` und Verwandtes
# ====================================================================

# Ohne ``import`` kann man diejenigen Anweisungen verwenden, die als “eingebaut”
# (“built-in”) bezeichnet werden wie ``print``. Eine Übersicht aller eingebauten
# Anweisungen findet man hier: https://docs.python.org/3/library/functions.html

# Mit ``import NAME`` liest man ein Modul (also eine Python-Datei) ein – so dass
# eine darin definierte Anweisung ``func`` mit ``NAME.func()`` aufgerufen werden
# kann. Vgl. die folgenden Beispiele:

import os
import re

# Ab hier steht nun z. B. das uns bekannte ``re.search(...)`` zur Verfügung.

# Für Werte (statt Anweisungen) gilt Entsprechendes. So steht ab hier …

import sys

# … nun die Liste ``sys.path`` zur Verfügung:

print(sys.path)

# Die Glieder dieser Liste sind übrigens genau die Ordner, in die Python auf ein
# ``import NAME`` hin nacheinander schauet, ob da eine Python-Datei “NAME.py” zu
# finden sei; sobald sie gefunden ist, wird sie eingelesen; wenn sie nirgends zu
# finden ist, wird ein ``ImportError`` ausgegeben.

# Liegt das gewünschte Modul (also die Python-Datei) nicht sogleich in einem der
# ``sys.path``-Ordner, sondern in einem Unterordner, gibt man den Pfad an, etwa:

import xml.etree.ElementTree

# Das Modul “ElementTree” liegt nämlich in einem Ordner “etree”, dieser wiederum
# in einem Ordner “xml” und dieser endlich in einem der ``sys.path``-Ordner, und
# zwar im Ordner “Lib”.

# Man muss sich solche Pfade nicht anhand der Ordner zusammensuchen, sondern man
# übernimmt sie einfach aus der Dokumentation oder aus Hilfeseiten im Netz.

# Dieses Beispiel eignet sich auch gleich, um eine erste von zwei Spielarten der
# ``import``-Anweisung einzuführen: In ``import NAME`` ist NAME ja zugleich, wie
# oben angedeutet, das Präfix (= der Namensraum) bei der Verwendung, so im Falle
# von ``import re`` und dann ``re.search(...)``. Man müsste also bei ElementTree
# immer ``xml.etree.ElementTree`` vor die Anweisung schreiben. Die einzuführende
# Spielart aber ermöglicht, einen Alias-Namensraum festzulegen – im vorliegenden
# Fall ist “ET” üblich:

import xml.etree.ElementTree as ET

# Zum Beispiel heißt es nun statt ``xml.etreeElementTree.tostring(...)`` einfach
# ``ET.tostring(...)``.

# Die zweite Spielart ermöglichtʼs, unmittelbar eine Anweisung (oder einen Wert)
# aus einem Modul zu importieren:

from urllib.request import urlopen

# Das so importierte ``urlopen`` wird dann ohne Namensraumpräfix verwendet.

# Unter den ``sys.path``-Ordnern befindet sich immer auch der Ordner, in dem die
# Datei liegt, worin die ``import``-Anweisung steht. Daher gelingt die folgende,
# uns schon bekannte Anweisung:

import utilia

# Überraschend schwierig ist es, statt aus diesem Ordner aus seinem Elternordner
# zu importieren. Man kann ihn aber in ``sys.path`` einfügen:

import __init__

# “__file__” ist der jeweilige Pfad zur Datei, in der “__file__” steht; und “..”
# bedeutet ‘eine Datei- oder Ordnerebene nach oben’. Dieser so gegebene relative
# Pfad wird mit “os.path.abspath” absolut gemacht, was auf Windows weniger, aber
# etwa auf Linux nötig ist.

# Nun kann man die Datei “xmlhtml.py” importieren, wenn man sie zur Vorbereitung
# in den Elternordner des die vorliegende Datei enthaltenden Ordners gelegt hat. 
# So ist auch die Anordnung in dem Netzordner, aus dem man “xmlhtml.py” bekommt:
# https://gitlab.lrz.de/badw-it/system/-/tree/master/intern

import xmlhtml
