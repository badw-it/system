# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 4
# ===========

# Eine kleine Wiederholung (mit englischen Fachtermini in Kommentaren):

path = r'C:\Users\M\A\bestand\Mhd\PP\P\III-0-v4.Nib\Nib.txt' # An assignment.

# A ``with``-block:
with open(path, encoding = 'cp437') as file: # open(arg1, ...): A function call.
    text = file.read() # A method call.

items = text.split('\n@H', 1) # A list (a sequence of items in some order).

text = items[1] # Indexing. Slicing is shown below: ``lemmata_numbers[:80]``.

lines = text.strip().split('\n')

lemmata = {} # A dictionary (for key-value-pairs; the keys are unique).

for line in lines: # A ``for``-statement (for repetition).
    lemma = line.split('\t')[0]
    if lemma not in lemmata: # An ``if``-statement (for conditional execution).
        lemmata[lemma] = 1 # Indexing dictionaries is done by keyword.
    else:
        lemmata[lemma] += 1

lemmata_numbers = sorted(lemmata.items()) # A list of tuples (immutable lists).

for lemma_number in lemmata_numbers[:80]: # Slicing.
    print(lemma_number)

# Listen und Tupel
# ----------------

# Beispiel für eine Liste:

li = ['a', 'x', 'e']

# Beispiel für ein Tupel:

tu = ('a', 'x', 'e')

# Die Liste kann verändert werden:

print(li, '– die Liste in ihrer Urgestalt.')
li.sort()
print(li, '– dieselbe Liste sortiert.')

# ``li`` verweist hier immer auf denselben (nicht nur: den gleichen) Wert; hier:
# auf dieselbe Liste namens ``li``.

# Das Tuple kann nicht verändert werden. Aber es kann dem Namen, der es benennt,
# ein neuer Wert und mithin ein neues Tupel zugewiesen werden mit ``=``:

print(tu, '– das Tupel in seiner Urgestalt.')
tu = tuple(sorted(tu)) # ``sorted`` returns a list.
print(tu, '– ein anderes, gleichnamiges Tupel, sortiert.')

# Das ``tu`` verweist nun, nach der Neuzuweisung, nicht mehr auf denselben Wert;
# genauer: nicht mehr auf dasselbe Tupel. (Auch nicht auf das gleiche, wegen der
# Umsortierung in diesem Beispiel.)

# Die Veränderlichkeit von Listen zeigt sich auch darin, dass sie durch Indexing
# veränderlich sind:

li[1] = 'b'
print(li, '– dieselbe Liste, durch Indexierung verändert.')

# Tuple sind auch durch Indexing nicht veränderlich. Die folgende Zeile führt zu
# einer Fehlermeldung, wenn man die ``#``-Kommentarzeichen entfernt:

##tu[1] = 'b'

# Eigene Funktionen schreiben
# ---------------------------

# Man kann eigene Funktionen schreiben und dann genau so wie die in Python schon
# eingebauten verwenden. In diesen Funktionen steht letztlich Python-Code ebenso
# wie außerhalb der Funktionen. Wozu dienen Funktionen dann? Zusammengefasst:

# 1. braucht man sie für Funktionen “höherer Ordnung”: Denn eine solche Funktion
# nimmt eine oder mehrere Funktionen als Argumente – das heißt, der Argumentwert
# ist eine Funktion statt beispielsweise einer Zahl oder Zeichenkette – so nimmt
# unsere ``sorted``-Funktion ebenso wie unsere ``sort``-Methode von oben eine im
# Sortiervorgang auf jedes zu sortierende Element anzuwendende Funktion als Wert
# des ``key``-Schlüsselwortelementes. Übergeben wird der Name einer Funktion.

# 2. braucht man sie, um das Wiederholen gleichen Python-Codes zu vermeiden: Oft
# verwendete Codeabschnitte lagert man in Funktionen aus, die man dann an vielen
# verschiedenen anderen Stellen in seinem Code aufrufend verwenden kann. Das ist
# vor allem von Vorteil dafür, den Code übersichtlicher und wartbarer zu machen;
# denn der so ausgelagerte Code muss nur noch an einer Stelle verstanden und bei
# Bedarf berichtigt oder ergänzt werden.

# 3. sind sie nützlich, um den Code in Sinnabschnitte zu unterteilen und dadurch
# übersichtlicher zu machen.

# Man schreibt eigene Funktionen in einem ``def``-Block.

# ACHTUNG: In dieser Zusammenfassung zwar folgt der ``def``-Block erst hier – in
# normalen Python-Dateien aber schreibt man etwaige ``def``-Blöcke immer **vor**
# dem Code, der außerhalb von ``def``-Blöcken auf Modulebene steht!

def get_key_for_sorting_by_number(lemma_number):
    return (lemma_number[1], lemma_number[0])

# - ``get_key_for_sorting_by_number`` ist der frei wählbare Funktionsname.
# - In einer Rundklammer folgen die Argumente, ebenfalls frei wählbar benannt.
# - Die Syntax gleicht der Syntax beim Aufruf von Funktionen.
# - Der Doppelpunkt zeigt den Block an.
# - In der Funktion steht normaler Python-Code, in dem man Argumentvariablen wie
#   normale Variablen benutzen kann.
# - Nach ``return`` steht ein Wert, den die Funktion zurückgibt – dabei verlässt
#   der Programmablauf die Funktion und setzt dort fort, wo sie aufgerufen ward.

# In diesem Beispiel wird ein Tupel (von Rundklammern umschlossen) zurückgegeben
# und darin die ersten beiden Glieder von ``lemma_number`` – und zwar zuerst das
# zweite und dann das erste.

# Diese Funktion dient nun dazu, unsere Sammlung ``lemmata_numbers`` nach den je
# zweiten Gliedern zu sortieren, also nach der Anzahl statt nach dem Lemma:

lemmata_numbers = sorted(
        lemmata.items(),
        key = get_key_for_sorting_by_number,
        reverse = True, # I want to see the most frequent lemma first, not last.
        )

for lemma_number in lemmata_numbers[:80]:
    print(lemma_number)

# In eine Datei schreiben
# -----------------------

# Beim Schreiben in eine Datei ist Vorsicht geboten: Während alle bisher getanen
# Schritte keinen dauerhaften Schaden anrichten können, mag es beim Schreiben in
# Dateien vorkommen, dass man eine Datei überschreibt, also ihren Inhalt löscht!
# Zwar könnte das gewollt sein, etwa um einen Inhalt durch veränderten Inhalt zu
# ersetzen. Aber wenn man einen Programmierfehler gemacht hat (und das ist allzu
# wahrscheinlich bei umfangreicheren Änderungen und beim ersten Ausführen), dann
# führt auch das an sich beabsichtigte Überschreiben zu einem Malheur.

# Deswegen ist in der Regel geboten, erst einmal sicherzustellen, dass man nicht
# eine alte Datei überschreiben wird, sondern eine neue anlegt, das heißt, einen
# neuen Pfad einführt, etwa, indem man an den alten eine neue Erweiterung hängt:

new_path = path + '.txt'
print('Neuer Pfad:', new_path)

# … und nachschaut, ob es unter diesem Pfad schon eine Datei gibt. Wenn ja, aber
# nur eine von diesem Programm automatisch erzeugte, kann man ausnahmsweise auch
# überschreiben.

# In eine Datei können nur Bytes geschrieben werden – keine python-eigenen Werte
# wie Zahlen, Zeichenketten, Listen oder Tupel. Wir erzeugen Bytes, indem wir …

# 1. unsere Werte in eine Zeichenkette umwandeln:

text = str(lemmata_numbers)

# 2. und diese Zeichenkette dann in Bytes. Dafür müssen wir angeben, mit welchem
# Zeichenschlüssel das geschehen soll. Wann immer irgend möglich, sollte man den
# Schlüssel “utf-8” benutzen.

# Diesen zweiten Schritt können wir beim Schreiben in die Datei erledigen lassen
# und geben dafür den Zeichenschlüssel schon beim Öffnen der Datei an.

# Das Öffnen einer Datei zum Schreiben ähnelt dem Öffnen zum Lesen. ACHTUNG:
# - Ist unter dem angegebenen Pfad noch keine Datei gespeichert, wird eine neue,
#   leere Datei an diese Stelle gesetzt und geöffnet.
# - Ist schon eine vorhanden, so wird sie mit einer neuen, leeren Datei ersetzt,
#   geht also verloren!

# Wenn man sichergestellt hat, dass das Öffnen der Datei zum Schreiben nicht ein
# Unheil anrichtet, dann kann man die folgenden Zeilen einkommentieren:

##with open(new_path, mode = 'w', encoding = 'utf-8') as file:
##    size = file.write(text)
##    print(size)

# Die Methode ``write`` von ``file``-Objekten gibt die Größe (in Bytes) der just
# von dieser Methode beschriebenen Datei zurück.

# Ein Blick in die Datei lehrt, dass unser ``text = str(lemmata_numbers`` keinen
# gut lesbaren Inhalt erzeugt hat, weniger lesbar als unsere zeilenweise Ausgabe
# weiter oben mit ``print``.

# In der kommenden Veranstaltung werden wir eine neue Funktion schreiben, um ein
# Sortieren nach Lemma-Länge durchzuführen, und werden das Ergebnis in eine bass
# lesbare Zeichenkette verwandeln.
