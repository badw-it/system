# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 7
# ===========

import html
import re

# Für alle PDF-Verarbeitung installiere ich mir gerne ein Zusatzmodul “PyMuPDF”:
# “pip install PyMuPDF” oder “pip3 install PyMuPDF” (was die Zusammenfassung vom
# letzten Mal erläutert). – Das dadurch verfügbare Modul wird im Code allerdings
# ``fitz`` genannt, aus historischen Gründen.

import fitz

import utilia

# Stellt man fest, dass man im Programm einen Teil mehrmals geschrieben hat oder
# zu schreiben sich anschickt, erwäge man, den Teil in eine Funktion auszulagern
# und anstelle der Wiederholung jedesmal die Funktion aufzurufen. Das bietet die
# Vorteile, dass man Verbesserungen am bewussten Teil nicht mehrmals auszuführen
# hat und dass das Programm, zumal bei sprechend gewähltem Funktionsnamen, etwas
# übersichtlicher wird.

def normalize_whitespace(term):
    return re.sub(r'\s+', ' ', term).strip()

# Die folgende Funktion ist ein Beispiel für den Vorteil, Verbesserungen bloß an
# einer einzigen Stelle erledigen zu müssen: Der reguläre Ausdruck hier war erst
# ``r'(?s)<.*?>'``, was sich aber in einem anderen Ausdruck als langsam erwiesen
# hat und daher auch hier ersetzt wurde.

def remove_tag(term):
    return re.sub(r'<[^>]*>', '', term)

def replace_linebreaks(match):
    werkID = normalize_whitespace(remove_tag(match.group('werkID')))
    speicher = normalize_whitespace(remove_tag(match.group('speicher')))
    return f'█{werkID}█{speicher}</p>'

def untag(term):
    return normalize_whitespace(html.unescape(remove_tag(term)))

path = r'C:\Users\M\A\ablage\exempla_py\reservare.pdf'

# Um mit dem Modul etwas anfangen zu können, suche ich seine Dokumentation auf –
# mit entsprechenden Stichwörtern über eine Netzsuche oder aber über seine Seite
# im “Python Package Index” – https://pypi.org/project/PyMuPDF/ –, wo in der Tat
# ein Link zu https://pymupdf.readthedocs.io/en/latest/ versteckt ist, unter der
# Wendung “Read The Docs”.

# https://pymupdf.readthedocs.io/en/latest/tutorial.html#opening-a-document sagt
# mir, wie ich eine PDF-Datei einlese:

doc = fitz.open(path)

# Die PDF-Datei ist ein kaum kommensurabler Salat von Bytes, geschrieben, wie es
# das elefantendicke PDF-Regelwerk verlangt; das Ergebnis des Einlesens dagegen,
# unser ``doc`` oben, ist ein Python-Objekt mit Attributen und Methoden, wodurch
# wir viel bequemer auf das PDF zugreifen und es verändern können.

# Attribute und Methoden werden in der ja schon bekannten Schreibweise mit einem
# Punkt aufgerufen; vgl. ``doc.page_count`` – ein Attribut, das die Seitenanzahl
# enthält – oder ``doc.get_toc()`` (wie üblich mit Rundklammern zum Aufrufen von
# Funktionen und Methoden) – eine Methode, die ein Inhaltsverzeichnis aus der im
# PDF enthaltenen Gliederung ableitet und zurückgibt.

# Welche Attribute und Methoden es gibt und wie man sie wofür verwendet, ist der
# Dokumentation zu entnehmen.

# Zur Weiterverarbeitung erzeugen wir uns zwei Ausgaben des PDF-Inhaltes, einmal
# baren Text, einmal ein HTML-Dokument.

for option, ext in (
        ('text', '.txt'),
        ('xhtml', '.xhtml'),
        ('html', '.html'), # must be the last pair: the HTML is used after that.
        ):
    # Anmerkung: ``for a, b in a_sequence_of_pairs`` nennt sich tuple unpacking;
    # entsprechend geht ``for a, b, c in a_sequence_of_triples`` usw.
    temp = []
    for page in doc:
        # Dass man so über die Seiten im PDF laufen kann, muss der Dokumentation
        # entnommen werden. – Auch ``page`` ist ein Python-Objekt mit Attributen
        # und Methoden.
        text = page.getText(option)
        temp.append(text)
    text = '\n'.join(temp)
    # Die folgende Zeile kann man (mit “##”) auskommentieren, hat man die beiden
    # Dateien schon einmal erzeugt. – Wir brauchen sie nur als Zwischenergebnis,
    # um uns ansehen zu können, was wir im Folgenden weiterverarbeiten wollen.
    print(utilia.write(text, path, ext = ext))

# Nach dem letzten Durchlauf der obigen ``for``-Schleife verweist ``text`` jetzt
# auf die HTML-Form des PDF-Inhaltes.

# Als erstes entfernen wir wieder (wie bei den Nibelungen) den Vorspann. Bei dem
# folgenden umfänglichen Regulären Ausdruck ist wichtig:

# - Der Flaggenausdruck ``(?im)`` sorgt dafür, dass 1. die Groß-Klein-Schreibung
#   keinen Unterschied macht (“i” ‘ignore case’), 2. die Zeichen “^” und “$” für
#   Anfang und Ende jeder Zeile (nicht nur des Textes insgesamt) stehen (“m” ist
#   Kürzel für “multiline”).
# - Das “?:” am Anfang der folgenden geklammerten Gruppe sorgt dafür, dass diese
#   Gruppe keine “capturing group” ist, denn bei den Funktionen ``re.split`` und
#   ``re.findall`` führen “capturing groups” oft zu unerwünschtem Verhalten. Das
#   wird in der Dokumentation des ``re``-Moduls beschrieben.
# - Es ist nicht von vornherein klar, wie eng der Ausdruck sein soll: Wenn er zu
#   eng ist, dann findet er vielleicht eine abweichend geformte letzte Zeile des
#   Vorspanns nicht; wenn zu weit, dann findet er vielleicht einmal eine ähnlich
#   geformte Zeile, die noch mitten im Vorspann steht. Der folgende Ausdruck ist
#   eher eng, indem er auch noch fordert, dass dem “Total” ein Doppelpunkt sowie
#   eine Zahl folgen.

text = re.split(r'(?im)^(?:<.*?>)*Total\s*:\s*\d.*\n', text, maxsplit = 1)[1]

# Nun geht es darum, die Folge der übrigen Absätze zu gliedern. Dafür müssen wir
# erkennen, wo jeweils ein neues Trefferblöckchen beginnt und welche Absätze des
# jeweiligen Blöckchens zur Werkangabe, zur Stellenangabe und zum Beleg gehören.
# Das ist schwierig, weil die im PDF sichtbare Auszeichnung der Blöckchen – eine
# graue Rahmung – im HTML nicht mehr fassbar ist. Am besten scheint es zu gehen,
# indem man die Werkangabe und damit den Anfang jedes Blöckchens am schließenden
# Datenbankkürzel (wie “- LLT-A”), die Stellenangabe als die unmittelbar nächste
# Zeile und den Belegtext als alle restlichen Zeilen erkennt.

# Die folgende Ersetzung sorgt dafür, dass die Werkangabe mindestens am Ende, wo
# das rundgeklammerte Werk- und dann das Datenbankkürzel stehen, nicht von einem
# Zeilenumbruch unterbrochen wird. Außerdem wird ein Zeichen vor diese genannten
# Kürzel gesetzt, um sie und die erste Zeile eines Blockes leichter erkennbar zu
# machen. – Der reguläre Ausdruck ist durch die Flagge “x” als im ‘verbose’-Stil
# geschrieben gekennzeichnet – https://docs.python.org/3/library/re.html#re.X –,
# eine Schreibweise, bei der Kommentare und Leerraumzeichen frei verwendbar sind
# und die Unübersichtlichkeit des Ausdruckes mildern können.

text = re.sub(r'''(?x)
        \( # The last round-bracket expression before the database info.
        (?P<werkID>
        [^)]*
        )
        \)
        (\s|<[^>]*>)*?-(\s|<[^>]*>)*? # A hyphen surrounded by space or tags.
        (?P<speicher> # The database info, e.g. “LLT-A” or “LLT-B”.
        LLT(\s|<[^>]*>)*?-(\s|<[^>]*>)*?A
        |
        LLT(\s|<[^>]*>)*?-(\s|<[^>]*>)*?B
        )
        (\s|<.*?>)*? # Space or tags again.
        </p> # The closing paragraph tag.
        ''', replace_linebreaks, text)

# Nun sammeln wir die Belegblöckchen in eine Liste ein, hier zunächst die leere:

quotations = []

# Wir setzen eine Flagge, die anzeigt, ob wir uns unmittelbar nach der je ersten
# Zeile eines Blöckchens befinden, also die Stellenangabe vor uns haben.

immediately_after_first_line = False

# Jetzt gehen wir den Text Absatz für Absatz durch:

# - Kommt im Absatz ein “█” vor, ist es die Werkangabe. Wir extrahieren zunächst
#   Werk- und Datenbankkürzel, dann Titel und, soweit vorhanden, Verfasser.
# - Ein Absatz unmittelbar nach der Werkangabe ist die Stellenangabe, die weiter
#   in Einzelangaben – “liber” oder “lib.”, “cap.”, “pag.”, “par.”, “linea” – zu
#   zergliedern ist.
# - Alle sonstigen Absätze werden als Bestandteil des gerade vorliegenden Belegs
#   betrachtet.

# Die extrahierten Angaben eines jeden Beleges schreiben wir in ein dictionary –
# das erlaubt eine leichtere Änderung und Erweiterung der Art und Anzahl von den
# Angaben, als wenn wir ein Tupel oder eine Liste nähmen und der Zugriff auf die
# Angaben dann nicht mit Schlüsselwörtern, sondern Indexzahlen erfolgen müsste.

for para_match in re.finditer(r'(?s)<p\b.*?</p>', text):
    para_text = para_match.group()
    if '█' in para_text: # The title info is recognized by a “█”.
        para_text, werkID, speicher = para_text.split('█', 2)
        para_text = untag(para_text)
        # The remaining paragraph text should contain the author and the title –
        # separated by a hyphen – but the author info may be missing.
        para_text_items = para_text.split('-', 1)
        if len(para_text_items) == 2:
            verfasser, werk = para_text_items
        else: # There is no author info.
            verfasser = ''
            werk = para_text_items[0]
        quotations.append({
                'speicher': untag(speicher),
                'werkID': untag(werkID),
                'verfasser': verfasser.strip(),
                'werk': werk.strip(),
                'beleg': '', # used as a target for appending (see below).
                })
        immediately_after_first_line = True
    elif immediately_after_first_line: # The passage info is recognized by flag.
        immediately_after_first_line = False
        quotations[-1]['stelle'] = untag(para_text)
        #### Anstelle der vorhergehenden Zeile, die die Angabe “stelle” zuweist:
        #### para_text aufspalten und die Einzelangaben “liber” / “lib.”, “cap.”
        #### usw. einzeln zuweisen, so dass man mit ``quotations[0]['lib.']`` an
        #### die Buchangabe (usw.) kommt.
    else: # Any other paragraph should belong to the quotation itself.
        # Introduce the markers for the matching form:
        para_text = para_text.replace('<b>', '#').replace('</b>', '€')
        para_text = untag(para_text)
        # Improve the position of the markers:
        para_text = re.sub(r'#(\s+)', '\g<1>#', para_text)
        para_text = re.sub(r'(\s+|,|\.|;|:|!|\?)€', '€\g<1>', para_text)
        if quotations[-1]['beleg']:
            quotations[-1]['beleg'] += ' ' # Append a space first.
        quotations[-1]['beleg'] += para_text

for quot in quotations:
    print('speicher:', quot['speicher'])
    print('werkID:', quot['werkID'])
    print('verfasser:', quot['verfasser'])
    print('werk:', quot['werk'])
    print('stelle:', quot['stelle'])
    print('beleg:', quot['beleg'])
    print('--------------------')

# Das nächste Mal widmen wir uns dem oben nach ``####`` geschriebenen Anliegen –
# wer möchte, kann sich bis dahin schon selbst an einer Lösung versuchen.
