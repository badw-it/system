# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 1
# ===========

# Vorbemerkung: Das ist eine Python-Datei. Zwischen dem Zeichen “#” und dem Ende
# der Zeile kann stehn, was mag; es wird als Kommentar angesehen und übergangen,
# wenn die Datei ausgeführt wird. Andere Zeilen aber müssen gemäß Pythons Regeln
# verfasst sein, damit nicht beim Ausführen ein Fehler gemeldet wird. – Im Sinne
# der Lesbarkeit wird empfohlen, Zeilen in Python-Dateien nicht länger zu machen
# als 80 Zeichen (anderwärts empfohlene Obergrenzen sind 88 und 100).

# Der erste Schritt: Python installieren
# --------------------------------------

# Um ein in Python geschriebenes Programm von meinem Rechner ausführen lassen zu
# können, muss ich Python zunächst installieren:

# Weg 1: Ich gehe auf https://python.org und lade die zu meinem Rechner passende
# Installationsdatei der gerade neuesten Python-Version herunter.

# (Die Datei mag dann etwa “python-3.12.1-amd64.exe” heißen.)

# Ich führe die Datei aus. Im Fenster des Installationsvorganges setze ich unten
# die beiden Checkboxen so, dass Python 1. ohne Adminprivilegien installiert, 2.
# in der sogenannten “PATH”-Angabe ergänzt werde, und klicke dann auf den großen
# “Install”-Knopf oben.

# Weg 2: Ich installiere die neueste verfügbare Python-Version mittels einer dem
# Betriebssystem eigenen Paketverwaltung (so etwa unter Linux).

# So habe ich nun insbesondere installiert …

# 1. den Python-Interpreter: ein Programm, das ein in Python verfasstes Programm
#    in die Maschinensprache meines Rechners übersetzen und damit zur Ausführung
#    gelangen lassen kann. Wie man das auslöst, wird gleich beschrieben.
# 2. einen Editor, der für das Verfassen und Starten von Python-Programmen recht
#    wohl geeignet ist. Sein Name ist “IDLE”. Man findet ihn unter Windows etwa,
#    indem man das Startmenü öffnet und “Python” tippt. Das findet daneben auch:
# 3. eine umfassende Beschreibung der Regeln, wie ich Python-Programme verfassen
#    und ihre Ausführung einzuleiten habe. Diese Beschreibung aber umfasst sogar
#    eine Art Einführungskurs (ein “Tutorial”). Die Beschreibung ist übrigens im
#    Netz – unter https://docs.python.org – ebenfalls verfügbar.

# … und befinde mich damit auf festem Grund: Wann immer einmal ich Python wieder
# verwenden möchte und was immer mir dann von Gelerntem noch in Erinnerung sei –
# mit diesem ersten Schritt hin zu Python-Übersetzer, Editor und Regelwerk (samt
# Einführung) stehe ich an einer Stelle, von der aus ich alles Weitere wieder in
# Erfahrung bringen kann, was ich eben für mein Anliegen benötige.

# Überhaupt lernt man Rechnersprachen wohl in der Regel nicht so wie die ja auch
# viel feineren und geheimeren Regeln folgenden natürlichen Sprachen: weniger im
# Vorhinein mit Vokabellisten und Grammatiken, sondern, wenn man erst einmal die
# Grundlagen der Sprache und allgemeine Grundsätze für das Verfassen anpassbarer
# Programme gelernt hat: indem man seinem Anliegen entsprechende Schlagwörter im
# Netz sucht und Einzelheiten in der Dokumentation nachschlägt.

# Das erste Programm
# ------------------

# Ich lege eine bloße Textdatei an. Etwa indem ich einen Ordner öffne, die weiße
# Leerfläche im Ordner mit Rechts anklicke, über “Neu” gehend auf “Textdokument”
# klicke und die erzeugte Datei benenne. Für die Benennung ist wichtig, dass ich
# die Dateierweiterung von “.txt” in “.py” ändere.

# Anmerkung: Wenn die Dateierweiterung “.txt” nicht sichtbar ist und daher nicht
# geändert werden kann, zeigt das Betriebssystem leider nicht alle Dateinamen so
# an, wie sie wirklich lauten, sondern lässt manchmal die Erweiterungen weg. Das
# kann man etwa unter Windows wie folgt ändern: Oben auf “Ansicht”, “Optionen” –
# “Ordner- und Suchoptionen ändern” – klicken, dann auf “Ansicht” und dann unter
# “Erweiterte Einstellungen” das Merkmal “Erweiterungen bei bekannten Dateitypen
# ausblenden” abwählen.

# Ich nenne meine Datei “capitulum_1.py” und möchte nun etwas Python-Code in sie
# hineinschreiben:

# Weg 1: Ich nutze den erwähnten Editor IDLE.

# Eine Anmerkung dazu: Ich kann versuchen, mir einzurichten, dass Python-Dateien
# von nun an immer mit IDLE geöffnet werden. Etwa unter Windows so, dass ich mit
# Rechts auf die Datei, dann auf “Öffnen mit” und “Andere App auswählen” klicke,
# unten “Immer diese App zum Öffnen von .py-Dateien” (oder ähnlich) anwähle, nun
# auf “Weitere Apps” klicke, dann nach unten spule und “Andere App auf diesem PC
# suchen” auswähle, im darauf sich öffnenden Fenster in das Feld oben klicke (wo
# der Pfad des gezeigten Ordners steht), aber nicht auf den Pfad, sondern rechts
# daneben, so dass ich den Pfad ändern kann: Ich lösche den Pfad, damit das Feld
# leer ist, gebe “%LocalAppData%” ein und bestätige mit Enter, doppelklicke dann
# im Fenster, das sich öffnet, auf “Programs”, “Python”, auf den Ordner der eben
# installierten Python-Version (etwa “Python39”), auf “Lib”, “idlelib” und darin
# endlich auf “idle.bat”, so dass sie ausgewählt wird, und bestätige mit “OK”.

# Weg 2: Ich benutze einen anderen Texteditor als IDLE, etwa Emacs oder VS Code.
# Oft bieten diese Editoren eine schon eingebaute grundlegende Unterstützung für
# das Verfassen von Python-Programmen und darüber hinaus installierbare Zusätze,
# die eine noch weitergehende – manchmal über IDLE hinausgehende – Unterstützung
# geben. Besonders weit geht darin ein Editor wie PyCharm.

# Ich schreibe in meine Datei …

print('Salve', 'orbis!')

# … und führe die Datei dann aus:

# Weg 1: im Editor selbst – in IDLE etwa mit F5 oder “Run”, “Run Module”. Darauf
# öffnet sich die IDLE-Shell, auf der auch etwaige Ausgaben erscheinen werden.

# Weg 2: außerhalb des Editors: indem ich eine Eingabeaufforderung (auch “Shell”
# oder “Terminal” genannt) öffne und dort etwas eingebe und mit Enter bestätige:
# - den Pfad zum installierten Python; vielleicht ist auch bloßes “python3” oder
#   “python” ausreichend.
# - den Pfad zu meinem Programm. Wenn die Eingabeaufforderung gerade in eben dem
#   Ordner geöffnet ist, in dem auch mein Programm liegt, genügt der bloße Name.

# Das Programm wird (über einen Zwischenschritt, sogenannten Python-Bytecode) in
# Maschinensprache übersetzt und vom Rechner ausgeführt. Das Ergebnis dessen ist
# die Ausgabe der an “print” übergebenen Zeichenfolgen: auf der IDLE-Shell, wenn
# das Programm aus IDLE heraus gestartet wurde, oder auf der Eingabeaufforderung
# (Shell), wenn es dort gestartet wurde.

# Die wesentlichen Grundlagen Pythons und des Programmierens scheinen hier auf:

# Alles, was einem in Programmiersprachen begegnet, ist …

# 1. entweder eine Art von Wert = Datenstruktur.
# 2. oder eine Anweisung.

# Im Vorfeld seiner Erfindung des digitalen Rechners war sich Konrad Zuse dessen
# bewusst geworden, dass alle automatische Datenverarbeitung gefasst werden kann
# als Zusammenspiel von Werten und Anweisungen, darin Anweisungen Funktionen und
# Werte Eingabewerte (Argumente) oder Rückgabewerte dieser Funktionen sind.

# In Python wird das im Allgemeinen so geschrieben, wie das obige Beispiel zeigt
# mit “print” (dem Namen einer in Python eingebauten Funktion), Klammern, welche
# erstens sagen, dass die Funktion hier ausgeführt werden soll (sonst würde hier
# nur ihr Name genannt sein), und zweitens die zu übergebenden Argumente rahmen,
# sowie schließlich mit den durch Komma getrennten Argumenten.

# Das Grundmuster ist also:

# Funktionsname(Argument1, Argument2, Argument3, ...)

# Im “print”-Beispiel stehn die Argumente noch jeweils in Anführungsstrichen, da
# der Python-Interpreter sie als Zeichenketten verstehen soll.

# Von diesem Grundmuster wird in Python allerdings öfters abgewichen, um kürzere
# oder gewohntere Schreibungen stattdessen vorzusehen, so die aus der Mathematik
# gewohnten Schreibungen, etwa “3 + 5” statt “add(3, 5)”:

##print(3 + 5)

# Um die vorstehende Zeile zu testen, muss man das sie einleitende “#” löschen.

# Eine besondere Art von Argumenten in Python sind die Schlüsselwort-Argumente –
# sie werden mit einem Schlüsselwort und Gleichheitszeichen vor dem eigentlichen
# Wert geschrieben:

##print('Salve', 'orbis!', sep = ', ')

# Nicht-Schlüsselwort-Argumente heißen “Positionsargumente”.

# Im Unterschied zu Positionsargumenten ist bei Schlüsselwort-Argumenten erstens
# die Reihenfolge gleichgültig, in der ich sie angebe, zweitens kann ich sie gar
# weglassen, es wird dann ein Rückfallwert übergeben.

# Um zu erfahrn, was eine Funktion bewirkt, welche Positionsargumente und welche
# Schlüsselwortargumente mit welchen Rückfallwerten sie nimmt, schlage ich meist
# in der Dokumentation nach (in IDLE mit F1 oder über “Help”, “Python Docs”) und
# benutze dort meist die Suche im “Index” (im Zweifelsfall sind die Einträge mit
# mehr Rundklammern übrigens die hilfreicheren).

# In der Dokumentation zu “print” findet sich eine seltene Besonderheit, die wir
# uns noch nicht merken müssen: Das “*” nach dem ersten Positionsargument zeigt,
# dass hier noch beliebig viele weitere Positonsargumente folgen könnten.

# Beim nächsten Mal beschäftigen wir uns mehr mit Zeichenketten, einigen anderen
# Datentypen (Arten von Werten) sowie mit Funktionen, um Dateien zu lesen und zu
# schreiben.
