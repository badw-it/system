# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2023,
# #### TODO: append your by-line, e.g. “extended by Abc Xyz in 2023.”
'''
#### TODO: docstring describing the general purpose of this module.
'''
import unicodedata
import xml.etree.ElementTree as ET
import xml.parsers.expat

from collections import Counter, deque

import utilia

# Include the parent folder of this module into look-for-import path:
import __init__

# When https://gitlab.lrz.de/badw-it/system/-/blob/master/code/xmlhtml.py is put
# into the parent folder (seen from the file at hand), you can do now:
import xmlhtml

def add_to_chars(
        term: str,
        chars: 'Counter',
        with_normalization: bool,
        ) -> None:
    '''
    #### TODO: docstring.
    '''
    if with_normalization:
        term = normalize(term)
    for i, char in enumerate(term):
        if i > 0 and 'COMBINING' in unicodedata.name(char, ''):
            char = f'{term[i - 1]}{char}'
        chars[char] += 1

def inventarize_chars(doc: str, with_normalization: bool = False) -> str:
    '''
    #### TODO: docstring.
    '''
    root = ET.XML(doc)
    chars = Counter()
    for elem in root.iter():
        for key, value in elem.attrib.items():
            add_to_chars(value, chars, with_normalization)
        text = elem.text
        if text:
            add_to_chars(text, chars, with_normalization)
        tail = elem.tail
        if tail:
            add_to_chars(tail, chars, with_normalization)
    items = []
    for char, num in sorted(chars.items()):
        name = unicodedata.name(char[-1], hex(ord(char[-1])))
        items.append(f'{char}\t{num}\t{name}')
    return '\n'.join(items)

def normalize(term: str) -> str:
    '''
    Normalize :param:`term`:

    - Apply the unicode normal form “NFC”, not “NFKC”, which changes variants to
      the canonical (“K”) variant, because it changes e.g. “…” to “...”.
    - #### TODO: More normalization?
    '''
    term = unicodedata.normalize('NFC', term)
    return term

def normalize_chars(doc: str, report: bool = False) -> str:
    '''
    #### TODO: docstring.
    '''
    items = deque()
    for num, start, attrs, end, text, com, pi, decl, undecl \
            in xmlhtml.itemize(doc, True):
        attrs_new = { key: normalize(value) for key, value in attrs.items() }
        if report and attrs != attrs_new:
            print(attrs)
            print(attrs_new)
            print('─────────────────────')
        text_new = normalize(text)
        if report and text != text_new:
            print(text)
            print(text_new)
            print('─────────────────────')
        items.append(
                [num, start, attrs_new, end, text_new, com, pi, decl, undecl])
    return xmlhtml.serialize(items)

if __name__ == '__main__':
    # Insert the path to your XML file here:
    path = r"C:\Users\M\A\ablage\exempla_py\bwb.xml"
    doc = utilia.read(path)

    print(utilia.write(
            inventarize_chars(doc, with_normalization = True),
            path,
            ext = '.txt'))

    print(utilia.write(
            normalize_chars(doc, report = True),
            path,
            ext = '.xml'))
