# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 2
# ===========

# Vorspiel: Werte und Namen
# -------------------------

# Werten kann ein Name zugewiesen werden:

text = 'der Worte sind genug gewechselt'

# Von hier an kann man den Namen setzen statt den Wert selbst zu wiederholen:

print(text)

# Anmerkung: Damit die Python-Datei nur bis zu jener Zeile hin ausgeführt werde,
# mit der man sich gerade beschäftiget, kann man gleich hinter dieser Zeile noch
# eine Funktion aufrufen, die den Programmlauf abbricht:

##exit()

# (Dafür muss man diesen Aufruf einkommentieren.) – Zurück zu den Zuweisungen.

# Man kann einem Wert mehrere Namen zuweisen:

doc = text
is_equal = doc == text
is_equal = (doc == text)
print('``text`` und ``doc`` verweisen auf den gleichen Wert:', is_equal)

# Während die Zuweisung mit ``=`` geschieht, geschieht der Vergleich mit ``==``.
# ``=`` heißt ‘verweise auf’, ‘sei gleich’; ``==`` heißt ‘ist gleich’.

# Die Negation von ``==`` ist ``!=`` ‘ist ungleich’:

print('``text`` verweist nicht auf “Hallo”:', text != 'Hallo')

# Prolog: Der Datentyp Zeichenkette
# ---------------------------------

# Zeichenketten kann man in einfache oder doppelte Anführungsstriche setzen:

text = 'Von Zeit zu Zeit seh ich den Alten gern'
text = "Von Zeit zu Zeit seh ich den Alten gern"

# Man kann auch dreimal einfache oder doppelte Anführungsstriche verwenden:

text = '''Von Zeit zu Zeit seh ich den Alten gern'''
text = """Von Zeit zu Zeit seh ich den Alten gern"""

# Diese Wahlmöglichkeit mag einmal nützlich sein, wenn sonst der Begrenzer nicht
# eindeutig wäre – weil er in der Zeichenkette selbst vorkommt:

print("Von Zeit zu Zeit seh' ich den Alten gern")
print('"Von Zeit zu Zeit seh ich den Alten gern"')
print('''"Von Zeit zu Zeit seh' ich den Alten gern"''')

# Die dreimal einfachen oder doppelten Anführungsstriche erlauben außerdem, dass
# man Zeilenwechsel (Enterzeichen) einfach so in die Zeichenkette setzt:

print('''
Von Zeit zu Zeit sehʼ ich den Alten gern,
Und hüte mich mit ihm zu brechen.
Es ist gar hübsch von einem großen Herrn
So menschlich mit dem Teufel selbst zu sprechen.
''')

# Für alle Fälle aber gibt es die Umschreibung mit Rückschlag:

print('Es ist nicht Goethe, aber vielleicht Dada:', '""" \'\'\' """')

# Da nun ebenfalls ein Sonderzeichen, muss auch der Rückschlag eine Umschreibung
# haben:

print('Ein einfacher Rückschlag:', '\\')

# Eine Rückschlagschreibung gibt es auch für Zeilenwechsel und Tabulatoren:

print('Dada mit Zeilenwechseln (Newline):', '\n"""\n \'\'\'\n"""')
print('Dada mit Zeilenwechseln und Tabulatoren:', '\n\t"""\n\t\t\'\'\'\n\t"""')

# Es gibt einige wenige solcher Umschreibungen mehr; man verwendet sie aber noch
# seltener.

# Eine andere Art von Rückschlagumschreibungen ist dafür da, ein jedes beliebige
# Unicode-Zeichen mittels seiner hexadezimalen Unicode-Zahl einzugeben – die man
# in Listen findet wie https://unicode-table.com/en/ oder in Wikipedia, vgl. die
# Seite https://en.wikipedia.org/wiki/Whitespace_character. Leerzeichen sind ein
# Fall, in dem sich die Nutzung dieser Umschreibung lohnen mag, da sie kaum oder
# gar nicht unterschieden werden können, wenn man sie als sie selbst eingibt:

print('Sind gewöhnliches und geschütztes Leerzeichen gleich?', ' ' == '\xa0')
print('Zwei geschützte Leerzeichen sind gleich:', ' ' == '\xa0')

# Nach ``\x`` müssen genau zwei Ziffern folgen; man füllt mit ``0`` auf:

print('Tabulator:', '\t' == '\x09')

# Für höhere Unicode-Zahlen gibt es ``\u``:

print('Tabulator:', '\t' == '\x09' == '\u0009')
print('Narrow no-break space:', 'z.\u202fB.')

# Für noch höhere Unicode-Zahlen gibt es ``\U``:

print('Tabulator:', '\t' == '\x09' == '\u0009' == '\U00000009')
print('Unter Tränen lächelnd, Emoji-Style:', '\U0001f602')

# In der Regel aber lassen sich Zeichen unmittelbar in den Code eingeben (ebenso
# hineinkopieren):

print('Unter Tränen lächelnd, homerisch:', 'δακρυόεν γελάσασα')
print('U\\u0366 == Uͦ:', 'U\u0366' == 'Uͦ')

# Ob und wie gut ausgefallene Zeichen erscheinen, hängt von der Anzeigefähigkeit
# des Programms ab und davon, ob und wie sie in die Schriftart eingebaut sind.

# Eine wichtige Art von Zeichenketten sind die Raw Strings: In Zeichenketten von
# dieser Art hat der Rückschlag keine besondere Bedeutung. Das verunmöglicht das
# eben dargestellte Umschreiben, ist aber nützlich, sofern in einer Zeichenkette
# keine Umschreibungen nötig sind und viele Rückschläge vorkommen, die man sonst
# alle umschreiben müsste.

print('Raw Strings werden mit vorgesetztem “r” geschrieben:', r'\n')
print('Raw Strings werden mit vorgesetztem “r” geschrieben:', r"\n")
print('Raw Strings werden mit vorgesetztem “r” geschrieben:', r'''\n''')
print('Raw Strings werden mit vorgesetztem “r” geschrieben:', r"""\n""")

# Es gibt viele für die Verarbeitung von Zeichenketten nützliche Funktionen, die
# uns nach und nach begegnen werden. Hier auf Nachfrage einige Beispiele vorab:

part = 'Zeit'
print(part)
print(part in text)

length = len(part)
print(length)

# Funktionen, die besonders an eine bestimmte Art von Werten (so zum Beispiel an
# Zeichenketten, wie hier) gebunden sind, heißen “Methoden” dieses Objektes.

# Methoden werden anders als Funktionen aufgerufen, vgl.::
#
#     funktion(objekt, arg2, arg3, ...)
#     objekt.methode(arg2, arg3, ...)

# Ein Beispiel:

startpos = text.index(part)
print(startpos)

# Für das Ausschneiden von Abschnitten gibt es eine besondere Schreibweise:

print(text[4:8])
print(text[4 : 6 + 2])
print(text[startpos : startpos + length])

# Namentlich die ``len``-Funktion und das Ausschneiden (Slicing) mit den eckigen
# Klammern ist sehr häufig nützlich, überdies auch bei anderen Arten von Folgen,
# so Listen und Tupeln. Dazu später mehr.

# Erste Szene: Pfade. Bytes. Dateien öffnen, einlesen und schließen
# -----------------------------------------------------------------

# https://daten.badw.de/mhd-korpus/-/blob/arbeitsfassung/P/III-0-v4.Nib/Nib.txt
# ist eine Datei, die man sich wie diese Python-Datei herunterladen und irgendwo
# auf dem eigenen Rechner in einen Ordner legen kann.

# Man kopiere den Ordnerpfad aus der Adressleiste der Ordneranzeige und füge den
# Dateinamen hinzu. Unter Windows haben die Pfade den Rückschlag als Trenner, so
# nutze man hier einen Raw String – das Ergebnis sähe etwa so aus:

path = r'C:\Users\M\A\bestand\Mhd\PP\P\III-0-v4.Nib\Nib.txt'

# Auf Linux etwa so:

path = '/home/M/A/bestand/Mhd/PP/P/III-0-v4.Nib/Nib.txt'

# “/” ist das üblichere Trennzeichen und wird von Python auch in Windows richtig
# verarbeitet; ich könnte also auch schreiben:

path = 'C:/Users/M/A/bestand/Mhd/PP/P/III-0-v4.Nib/Nib.txt'

# Ein Pfad, der nicht nur in einem kurzlebigen Skript, sondern einem auf längere
# Nutzungsdauer vielleicht auf verschiedenen Rechnern angelegten Skript verwandt
# werden soll, wird am besten mit “/” und nicht als absoluter, sondern relativer
# Pfad geschrieben – vgl. https://de.wikipedia.org/wiki/Pfadname –:

path = '../../../bestand/Mhd/PP/P/III-0-v4.Nib/Nib.txt'

# In der folgenden Zeile, bitte, den auf Ihrem Rechner richtigen Pfad eintragen,
# damit die ``open``-Funktion die Datei auf Ihrem Rechner findet:

path = r'C:\Users\M\A\bestand\Mhd\PP\P\III-0-v4.Nib\Nib.txt'

# Die Datei wird erst geöffnet. Wie alle Dateien enthält sie nichts als Bytes.

# Anmerkung (die Beschwörung des Erdgeistes – oder der Auftritt Wagners):

# Die Phoneme in der Rechnersprache sind Bits. Leibniz und wohl vor ihm Gelehrte
# in Asien hatten erkannt, dass eine einzige Unterscheidung genügt, alle anderen
# Unterscheidungen auszudrücken, man also nur zwei unterschiedliche Einheiten in
# einer universalen Sprache braucht: “Omnibus ex nihilo ducendis sufficit unum”.
# Zuse hat diese Erkenntnis im digitalen Rechner umgesetzt, dem ersten danach ja
# “digital” genannten Rechner. – Allophone sind: die Zeichen “0” und “1”, ferner
# die An- und die Abwesenheit eines Loches in einer Karte oder eines Flusses von
# Strom in einem Schaltkreis usw.

# Die Morpheme in der Rechnersprache sind Bytes. Damit auch die Maschine schnell
# und sicher feststellen könne, wo ein Byte aufhört und ein neues beginnt, setzt
# man einfach die Anzahl von Bits fest, aus denen jedes Byte bestehen muss. Zwar
# hat es hier in der Geschichte verschiedene Festsetzungen gegeben, mittlerweile
# aber gilt allgemein die Zahl 8: Ein Byte ist also eine Einheit aus acht Bits.

# Ebenso gab und gibt es verschiedene Festsetzungen, mittels welcher Bytesfolgen
# welche Zeichen gespeichert werden. (Solche Festsetzungen nennt man “Encoding”,
# “Kodierung” oder “Zeichenschlüssel”.) Zum Beispiel wird “ü” nach dem Schlüssel
# UTF-8 durch die Bytesfolge “11000011 10111100” gespeichert, nach dem Schlüssel
# UTF-16 Big Endian hingegen durch die Bytesfolge “00000000 11111100”.

# Unter den Zeichenschlüsseln hat sich keiner so klar durchgesetzt wie unter den
# Arten von Bytes das mit 8 Bits. Aber während man immer noch anderen Schlüsseln
# begegnen kann, sollte man selbst, wann immer möglich, UTF-8 verwenden. Weitere
# Auskunft gibt https://de.wikipedia.org/wiki/UTF-8 und dort Verlinktes.

# Damit nun also die Bytesfolge der Datei in die richtige Zeichenfolge übersetzt
# wird, muss man den Zeichenschlüssel (das Encoding) angeben, nach dem zuvor die
# Zeichen in Bytes übersetzt worden waren:

file = open(path, encoding = 'cp437')

# Nun wird die Datei eingelesen:

text = file.read()

# Die runden Klammern stehen auch hier, obwohl keine Argumente abgegrenzt werden
# müssen. Denn sie haben immer auch noch einen anderen Sinn: zu bezeichnen, dass
# die Funktion hier nicht nur genannt, erwähnt, sondern wirklich gestartet wird.
# (Es gibt in der Tat den Fall, dass man eine Funktion nur nennen will: wenn man
# sie nur als Argument behandeln, an eine andere Funktion übergeben will.)

# Nun wird die Datei geschlossen (damit man sie nachher wieder umändern, löschen
# oder verschieben kann, ohne erst den Rechner neu starten zu müssen):

file.close()

# Wir lassen uns die ersten 1000 Zeichen ausgeben, auch, um zu sehen, ob wir den
# richtigen Zeichenschlüssel erwischt haben – ein hinreichender Anhaltspunkt ist
# dafür meist ein außerhalb der schlichten ASCII-Zeichen liegendes Zeichen, etwa
# ein Umlaut-“ü” –:

print(text[:100])

# Wir schreiben das Öffnen und Schließen von Dateien aber künftig nicht mehr so,
# wie eben angegeben, sondern mit einem ``with``-Block. Das heißt, aus …

file = open(path, encoding = 'cp437')
text = file.read()
file.close()

# … werden die folgenden zwei Zeilen:

with open(path, encoding = 'cp437') as file:
    text = file.read()

print(text[:100])

# Was zum Block gehört, wird mit Einrückung angezeigt: ``text = file.read()`` im
# ``with``-Block ist um vier Leerzeichen eingerückt, während das danach folgende
# ``print(text[:500])`` wieder um vier Leerzeichen ausgerückt ist.

# Um diese vier Leerzeichen zu erzeugen, genügt in IDLE die Tabulatortaste, denn
# dank der Voreinstellung erzeugt sie statt eines Tabulators vier Leerzeichen. –
# In anderen Editoren kann man das ebenfalls einstellen.

# Der Vorteil des ``with``-Blocks ist, dass das ``file.close()`` ganz von selbst
# ausgeführt wird, sobald der Programmablauf den ``with``-Block verlässt. Und so
# muss man es nicht mehr hinschreiben, ja mehr noch, die Ausführung ist wirklich
# sichergestellt: Wenn beim Einlesen ein Fehler auftritt, vielleicht wegen eines
# irrig angegebenen Zeichenschlüssels, und das Programm abstürzen lässt, wird ja
# das ``file.close()`` gar nicht mehr erreicht und mithin nicht ausgeführt, aber
# der ``with``-Block wird beim Absturz noch verlassen und die Datei damit sicher
# geschlossen.
