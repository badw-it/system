import os

def change_text(topdirpath):
    for dirpath, dirnames, filenames in os.walk(topdirpath):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            with open(filepath, encoding = 'utf-8') as file:
                doc = file.read()
            doc = doc.replace('ē', 'ê').replace('ī', 'î').replace('ū', 'û')
            with open(filepath, 'w', encoding = 'utf-8') as file:
                print(filepath, file.write(doc))

def rename_textfiles(topdirpath):
    num = 0
    for dirpath, dirnames, filenames in os.walk(topdirpath):
        for filename in filenames:
            if not os.path.splitext(filename)[0] == 'text':
                continue
            num += 1
            filepath = os.path.join(dirpath, filename)
            filepath_new = os.path.join(dirpath, f'doc{num}.txt')
            print(filepath)
            print(filepath_new)
            print()
            os.replace(filepath, filepath_new)

if __name__ == '__main__':
    # It is strongly recommended to apply any changes to a **copy**.
    topdirpath = r'C:\Users\M\A\ablage\exempla_py\corpus - Kopie'
    print('Running change_text:')
    change_text(topdirpath)
    print('·······················')
    print('Running rename_textfiles:')
    rename_textfiles(topdirpath)
