import os

def get_key_for_sorting_by_length(lemma_number):
    lemma = lemma_number[0] # yields e.g. '`dër @art'
    lemma = lemma.split('@', 1)[0].strip()[1:] # yields e.g. 'dër'
    lemma = lemma.replace('(', '').replace(')', '').replace('-', '')
    length = len(lemma)
    return (length, lemma_number[0], lemma_number[1])

def get_key_for_sorting_by_number(lemma_number):
    return (lemma_number[1], lemma_number[0])

def read(path, encoding = 'utf-8'):
    with open(path, 'r', encoding = encoding) as file:
        return file.read()

def write(text, path, ext = '', encoding = 'utf-8'):
    '''
    Write :param:`text` with :param:`encoding` into a file
    whose name is based on :param:`path` und :param:`ext`:

    - If ``path`` is a relative path, make it absolute.
    - If an ``ext`` is given, append it to the path.
    - If ``path`` is already in use, append ``ext`` to the
      path until a new, unused path is reached. If ``ext``
      was not given, take the extension from :param:`path`
      (from its absolute version). If it has no extension,
      take the string “._” as extension.

    So this function does not overwrite an existing file.

    Return the possibly new path and the size of the file.
    '''
    path = os.path.abspath(path)
    if ext:
        path += ext
    else:
        basepath, ext = os.path.splitext(path)
        if not ext:
            ext = '._'
    while os.path.exists(path):
        path += ext
    with open(path, mode = 'w', encoding = encoding) as file:
        size = file.write(text)
    return path, size
