import collections
import json
import unicodedata

from pprint import pp

def switch(pair):
    return (pair[1], pair[0])

path = r"C:\Users\M\A\ablage\exempla_py\schmeller.json"

with open(path, encoding = 'utf-8') as file:
    lines = json.load(file)

chars = collections.defaultdict(int)
hapax = {}

for line in lines:
    word = line[0]
    word = unicodedata.normalize('NFD', word)
    for pos, char in enumerate(word):
        if 'COMBINING' in unicodedata.name(char, 'n.n.'):
            if pos == 0:
                char_prev = ''
            else:
                char_prev = word[pos - 1]
            combo = char_prev + char
            chars[combo] += 1
            if chars[combo] == 1:
                hapax[combo] = word
            elif chars[combo] > 1:
                hapax.pop(combo, None)

for combo, num in sorted(chars.items()):#, key = switch):
    print(
            combo,
            num,
            hex(ord(combo[-1])),
            unicodedata.name(combo[-1], 'n.n.'),
            hapax.get(combo, ''),
            sep = '\t',
            )
