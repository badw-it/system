# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2021 ff. (© http://badw.de)

# Capitulum 9b. XML und HTML einlesen, durchsuchen, ändern, schreiben
# ===================================================================

import re
import xml.etree.ElementTree as ET
from urllib.request import urlopen

# Zum Einlesen und Schreiben von Dateien wieder nützlich:
import utilia

# Wie das letzte Mal:
import __init__
import xmlhtml

# XML einlesen und durchsuchen
# ----------------------------

# Mit normalen regulären Ausdrücken kann man Verschachtelungen nicht verarbeiten
# – vgl. die folgenden verschachtelten “div”-Elemente:

text = r'''
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="utf-8"/>
        <title>Exemplum</title>
    </head>
    <body>
        <div>
            Wir sind im äußeren “div”.
            <div>
                Wir sind im inneren “div”.
            </div>
            Wir sind wieder im äußeren “div”.
        </div>
        <div>
            Hier sind wir in einem neuen, im ersten nicht enthaltenen “div”.
        </div>
    </body>
</html>
'''

# Es ist mit regulären Ausdrücken nicht möglich, genau das erste äußere “div” zu
# erfassen:

# Folgender Ausdruck erfasst zu viel:

print('Too greedy:')
print(re.search(r'(?s)<div>.*</div>', text).group())

# Folgender Ausdruck – die non-greedy-Variante mit “?” – erfasst zu wenig:

print('Too non-greedy:')
print(re.search(r'(?s)<div>.*?</div>', text).group())

# Mit einem XML-Parser ist es gut möglich, Verschachtelungen zu verarbeiten. Wir
# verwenden den auf https://docs.python.org/3/library/xml.etree.elementtree.html
# beschriebenen.

# Zuerst liest man den Text ein; dafür nutzen wir eine Anweisung, die einen Wert
# zurückgibt, der das oberste Element (= das Wurzelelement) darstellt. In diesem
# obersten Element sind alle anderen Elemente enthalten.

html = ET.XML(text)

print(html, html.tag, html.attrib)

# Auf diesen Wert kann man seine Methode “find” anwenden, die außer diesem Werte
# als ein weiteres Argument einen sogenannten XPath-Ausdruck nimmt. Durch solche
# X-Path-Ausdrücke kann man zu findende Elemente beschreiben – ihr Zweck ist dem
# Zweck regulärer Ausdrücke ähnlich. Die in ET eingebauten XPath-Ausdrücke sind:
# https://docs.python.org/3/library/xml.etree.elementtree.html#xpath-support

div1 = html.find('./body/div[1]')
print(div1, div1.tag, div1.attrib)

# Um den “div1”-Wert wiederum in eine Zeichenkette umzuwandeln, benutzen wir das
# xmlhtml-Modul:

print('The correct match:')
print(xmlhtml.serialize_elem(div1))

# Das HTML mancher Netzseiten ist kein wohlgeformtes XML; denn HTML erlaubt mehr
# Freiheiten, und Browser bemühen sich sowieso, jede Seite irgendwie anzuzeigen.
# Der XML-Parser aber bricht das Einlesen bei jedem Fehler ab; die Fehlermeldung
# enthält einen Hinweis darauf, was nicht wohlgeformt ist.

text = urlopen('https://badw.de').read().decode()
try:
    html = ET.XML(text)
except Exception as errortext:
    print('Parsing error:', errortext)

# Mit einer Anweisung aus dem xmlhtml-Modul kann man den Text so umwandeln, dass
# er im XML-Sinne wohlgeformt ist:

text = xmlhtml.replace(text)

# (Zu weiteren Möglichkeiten vgl. den Docstring dieser Funktion ``normalize``.)

# Nun können wir den XML-Parser verwenden:

html = ET.XML(text)
div_news = html.find('.//div[@class="c-card-neuigkeit__body"]')
print('The news element:')
print(xmlhtml.serialize_elem(div_news))

# Wenden wir uns echten Daten zu. Der folgende Pfad ist, wie üblich, anzupassen:

path = r'C:\Users\M\A\ablage\exempla_py\alfred-einstein-ausgabe.xml'

text = utilia.read(path)

# ``text`` ist nun eine Zeichenkette, die das XML-Dokument darstellt.

# Das Beispiel-XML bietet Gelegenheit zu zeigen, wie man einigermaßen schmerzlos
# mit Namensräumen im XML umgehen kann. Zunächst lege man ein Verzeichnis der im
# XML gesetzten Namensräume an:

nss = {
        '': 'http://www.tei-c.org/ns/1.0',
        'xml': 'http://www.w3.org/XML/1998/namespace',
        }

# Dieses Verzeichnis ergibt sich aus dem xmlns-Attribut im Wurzelelement “TEI” –
# “xmlns="http://www.tei-c.org/ns/1.0"”. Das heißt: Der implizite Namensraum ist
# ab da: “http://www.tei-c.org/ns/1.0”. Implizit ist der Namensraum in dem Sinn,
# dass er für jeden Element- und Attributnamen gilt, der *kein* Namensraumpräfix
# hat. So heißt das Element “TEI” also eigentlich nicht einfach “TEI”, sondern –
# mit der üblichen Schweifklammerschreibung für voll ausgeschriebene Namensräume
# – “{http://www.tei-c.org/ns/1.0}TEI”. So erklärt sich der erste Eintrag in dem
# Verzeichnis: Der Schlüssel ist das Präfix und mithin – da implizit – die leere
# Zeichenkette; der Wert ist der ausgeschriebene Namensraum. Der zweite Eintrag,
# der als Präfix “xml” setzt, erklärt sich aber so: Ohne das xmlns-Attribut wäre
# der implizite Namensraum der von XML selbst; wenn nun der implizite Namensraum
# anders gesetzt ist, muss der XML-Namensraum vor den Element- und Attributnamen
# aus diesem Namensraum explizit gesetzt werden, und zwar mit dem Präfix “xml” –
# das sieht man im Wurzelelement selbst beim Attribut “xml:id="text"”.

# Was in diesem XML-Dokument nicht vorkommt, ist ein weiterer Namensraum, der ja
# dann zur Unterscheidung vom TEI-Namensraum ebenso wie der XML-Namensraum durch
# explizites Präfix anzugeben wäre. Die Setzung eines solchen Namensraumes sieht
# so aus: “xmlns:PRÄFIX="URI"”; vor seinen Element- und Attributnamen steht dann
# entsprechend “PRÄFIX:” wie “xml:” in “xml:id”.

# Wie wir dieses Verzeichnis überhaupt verwenden, wird im Folgenden gezeigt – es
# handelt sich um zwei Zwecke, die damit erreicht werden:

# Zunächst “registrieren” wir die Namensräume, damit später eine Rückverwandlung
# in eine Zeichenkette nicht überall Namensraumpräfixe wie “ns01” oder Ähnliches
# setzt – das Registrieren muss geschehen, bevor man die XML-Zeichenkette in ein
# XML-Element-Objekt umwandelt:

for key, val in nss.items():
    ET.register_namespace(key, val)

# Jetzt kann man die Zeichenkette in ein XML-Element-Objekt wandeln, welches das
# Wurzelelement darstellt, das alle anderen Elemente enthält:

root = ET.XML(text)

# Der zweite Verwendungszweck des Namensraumverzeichnisses ist, es bei Suchen im
# XML mit XPath-Ausdrücken anzugeben, damit man im XPath-Ausdruck schlicht jenen
# Element- oder Attributnamen verwenden kann, der im XML steht, z. B.:

author = root.find('.//author', nss)

# https://docs.python.org/3/library/xml.etree.elementtree.html#xpath-support

print(author)

# Bloßes ``root.find('.//author')`` dagegen fände nichts, gäbe ``None`` zurück.

# In einem Element sind seine Unterelemente gespeichert, auf die man, wie soeben
# gezeigt, mit ``find`` zugreifen kann (oder mit ``findall`` oder ``iterfind``).
# Außerdem sind in einem Element noch die folgenden vier Angaben gespeichert:

print('The element name:', author.tag)
print('The attributes of the element, as a dictionary:', author.attrib)
print('The element text:', author.text)
print('The element tail:', author.tail)

# Auf ein bestimmtes Attribut kann man wie bei dictionaries üblich zugreifen:

print()
print(author.attrib['{http://www.w3.org/XML/1998/namespace}id'])

# Oder mit der Methode ``get`` eines Elementes:

print(author.get('{http://www.w3.org/XML/1998/namespace}id'))

# Hier kann man einen Wert angeben, der, wenn das Attribut nicht vorliegt, statt
# seines Wertes zurückgegeben wird:

print(author.get('id', 'XML-Namensräume sind weit mehr Bürde als Vorteil.'))

# Was aber ``text`` und ``tail`` sind, soll im Folgenden klarer werden:

p = root.find('.//text//p', nss)

print()
print(f'{p.tag=}')
print(f'{p.attrib=}')
print(f'{p.text=}')
print(f'{p.tail=}')

# ``text`` ist also etwaiger Text im Element nur bis zur nächsten Spitzklammer –
# auch wenn diese Spitzklammer nicht die schließende des Elementes ist.

add = p.find('./add[@source="#B2"]', nss)

print()
print(f'{add.tag=}')
print(f'{add.attrib=}')
print(f'{add.text=}')
print(f'{add.tail=}')

# ``tail`` ist etwaiger Text *nach* dem Element bis zur nächsten Spitzklammer.

# Außer ``find`` verwendet man zum Durchsuchen oft noch ``findall``:

print()
for elem in root.findall('.//add', nss):
    print(elem.get('source', ''), elem.text, sep = ': ')

# Ein besonderer Fallstrick sei hier noch erwähnt: Ein Element verhält sich etwa
# wie eine Liste seiner unmittelbaren Kindelemente, vgl.:

print()
for child in root:
    print(child)

# Das heißt aber auch: Ein Element ohne Kindelement ist ``False``, sogar wenn es
# vorhanden ist, Attribute oder gar Text (und Zagel) hat:

print()
if author:
    print('The element author evaluates to ``False``, so this is not printed.')

if author is not None:
    print('In order to test for existence, use ``is not None``.')

# XML ändern
# ----------

# ``text`` und ``tail`` können einfach neu zugewiesen werden:

author.text = 'Einstein, Alfred ' # The trailing blank is written on purpose.
author.tail = '\n' + author.tail

# Das ``attrib``-dictionary kann wie andere dictionaries verändert werden:

author.attrib['{http://www.w3.org/XML/1998/namespace}id'] = '945686'

# Oder mit der Methode ``set``:

author.set('gnd', 'http://d-nb.info/gnd/116425725')

# Ein Kindelement kann wie folgt hinzugefügt werden:

degree = ET.SubElement(author, 'degree')
degree.text = 'Dr. phil.'

# Das Ergebnis sollte gleich in der Ausgabedatei zu sehen sein.

# Erstellen würde ich ein XML-Dokument auf diese Weise übrigens eher nicht, also
# nicht ein Wurzelelement mit ``ET.Element``, Kindelemente mit ``ET.SubElement``
# erzeugen. In der Regel jedenfalls ist es für mich einfacher, Pythons f-Strings
# zu nutzen oder von https://bottlepy.org/docs/dev/stpl.html gebotene Templates.

# XML wieder in eine Zeichenkette verwandeln und in eine Datei schreiben
# ----------------------------------------------------------------------

text = ET.tostring(root, encoding = 'unicode')
print()
print(utilia.write(text, path))
