# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
Extract RDF triples from RDFa annotations in a given XML or HTML document,
write the triples in a format like N-Triples. For details and an example,
see :class:`RDFaParser`.
'''
import html
import html.parser
try: import regex as re
except ImportError: import re
import urllib.parse
import urllib.request
from collections import deque
from collections.abc import Iterable

import xmlhtml

class RDFaParser(xmlhtml.Parser):
    '''
    Parser to extract triples from an XML or HTML document.

    The document is given to the parser with its method `parse`. Then,
    the triples are stored in :attr:`RDFaParser.triples`, which is a deque of
    3-tuples, each containing a string as subject, a string as predicate and
    a dictionary as object. Each dictionary contains ...

    - `value`: a string: the actual value of the object.
    - `is_iri`: a boolean value: if True, `value` is an IRI, else a literal.
    - `lang`: a string: the language of `value`. If `lang` is empty, the
      language is undefined. If `value` is an IRI, `lang` has no meaning.
    - `type`: a string: the datatype of `value`. If `type` is empty, the
      datatype is undefined. If `value` is an IRI, `type` has no meaning.

    While initializing, one can specify a `base` value for resolving relative
    IRIs, an initial language value for the `lang` information, an initial
    `vocab` value for resolving terms and, finally, a prefix mapping for
    resolving CURIEs. The default prefixes are the same as in
    http://www.w3.org/2011/rdfa-context/rdfa-1.1.

    >>> import rdf
    >>>
    >>> doc = """
    ...         <p about="http://example.org/edition">
    ...           <span property="schema:datePublished" datatype="schema:Date">2014</span>
    ...         </p>"""
    >>>
    >>> parser = rdf.RDFaParser(base = rdf.get_base(doc))
    >>> parser.parse(doc)
    >>> print(rdf.convert_triples_to_nt(parser.triples))
    <http://example.org/edition> <http://schema.org/datePublished> "2014"^^http://schema.org/Date .

    .. important::
        This parser supports only a strict subset of RDFaCore_. This subset is
        suitable for expressing any triple and it is much easier to describe,
        to learn and to use than RDFaCore_. It is also easier to understand and
        to process than RDFaLite_ (another subset of RDFaCore_) and has some
        features of RDFaCore_ which RDFaLite_ lacks. In the following, the
        subset supported by this parser is described in detail; after that, the
        differences from RDFaCore_ are mentioned.

    An XML or HTML document may contain RDFa annotation. From this annotation,
    RDF triples can be automatically extracted. RDF triples are statements
    consisting of **subject**, **predicate** and **object**. The subject is the
    topic of the statement, i.e. the individual thing the statement is about
    (`thing` meant in a wide sense). The predicate and the object together are
    that what is said about the subject. E.g. `(subject) <hasName> <Jean Paul>`.
    The predicate can be seen as the relation that connects subject and object.
    The object can be seen as a data node, which is tied by the predicate to
    the subject data node. Thus, a set of statements can be seen as a graph of
    nodes and arcs.

    The **subject** is initially the value of `base` given to :meth:`__init__`.
    Whenever an element has an `about` attribute and the value of it is a valid
    subject (as specified in the next paragraph), this value is the new subject
    for the current element and its descendant elements, until it is superseded
    by another `about` attribute in a descendant element.

    A valid `about`-attribute value is either ...

    - a normal IRI (internationalized URI),
    - a CURIE, i.e. a prefix-abbreviated IRI like `prefix:rest_of_the_IRI`,
      e.g. `dc:creator` for `http://purl.org/dc/terms/creator`,
    - a relative IRI, i.e. an implicitly abbreviated IRI,
      e.g. `#8` for `http://example.org/this_page#8`.

    The parser must resolve the two abbreviated types:

    - Prefixes are replaced by their resolution (but blank nodes are left
      unchanged). An initial mapping of prefixes and resolutions can be given
      by `prefixes` to :meth:`__init__`. A resolution must be (the start of)
      an absolute IRI. If a valid `prefix` attribute occurs, its value updates
      the dictionary of prefixes and resolutions. A valid `prefix` attribute
      is a white space separated list of pairs, containing a `prefix:` (note
      the colon) and `resolution`, both seperated by white space from each
      other. This list is taken as a mapping to update the current mapping.
      But this update is valid only for the element in which the `prefix`
      attribute is found, and for the descendants of this element. After
      that, the dictionary as given before the update is used. This complies
      with RDFaCore_, but should not be abused: Prefixes should not change
      their meaning within one document.
    - Relative IRIs are appended to `base`, a string given to :meth:`__init__`.
      Thereby, a `/` is inserted between `base` and the path, if necessary.
      `base` is used unchanged in the whole document. `base` is mostly the path
      to the page which contains the relative IRI, but not necessarily.

    A **predicate** (or several predicates at once) are given by a valid
    `property` attribute. A valid `property`-attribute value is either ...

    - a normal IRI,
    - a CURIE,
    - a term, i.e. an implicitly abbreviated IRI just like a relative IRI,
      e.g. `creator` for `http://purl.org/dc/terms/creator`. A term, however,
      will not be resolved by appending it to :param:`base`, but by appending
      it to the current value of `vocab`, e.g. to `http://purl.org/dc/terms/`.
      `vocab` works quite similar to `prefixes`: An initial value of `vocab`
      can be given to :meth:`__init__`. If a `vocab` attribute occurs, its value
      is the new `vocab` value. But this update is valid only for the element
      in which the `vocab` attribute is found, and for the descendants of this
      element. After that, the `vocab` value that has been given before the
      update is used. This complies with RDFaCore_, but should not be used:
      `vocab` values should not change within one document.
    - or a white space separated list of items, which are normal IRIs or CURIEs
      or terms (the type need not to be one and the same for each).

    In the last case, each item is one predicate, in the other cases, the whole
    value is one predicate.

    And now, whenever one or more predicates are given in a tag, an **object**
    is taken for each predicate, namely:

    1. from a valid `content` attribute or, if this is not present in the tag,
    2. from a valid `href` attribute or, if this is not present in the tag,
    3. from a valid `src` attribute or, if this is not present in the tag,
    4. from the inner content of the element started by the tag.

    But 2. and 3. are skipped, if a `datatype` attribute is present in the tag.

    Valid values are the following:

    - A valid `content` attribute is a literal (a CDATA string). It may be an
      empty string.
    - A valid `href` or `src` attribute is a normal or relative IRI.
    - The inner content of the element is a literal, e.g. `inner content of an
      element` in the case of `<tag>inner <subtag>content</subtag> of an
      element</tag>`. It may be an empty string. All whitespace is preserved.
      Tags are omitted normally, but if a `datatype` attribute of the element
      is given and its value is `rdf:XMLLiteral` (or a synonym), then nested
      tags are preserved, leading to e.g. `inner <subtag>content</subtag> of
      an element`.

    If the object is taken from a `content` attribute or from the inner content
    of the element, this object is stored alongside with ...

    - a datatype information, if any has been given by a valid `datatype`
      attribute in the current tag. Valid means: a normal IRI, a CURIE or a
      term.
    - the language value, if no datatype information is given and if a language
      value is given in the following manner: Initially, the language value is
      the value given by :param:`lang`. As in the determination of the subject,
      this value is updated:  Whenever an element has a `lang` or `xml:lang`
      attribute, this value is the new value for the current element and its
      descendant elements, until it is superseded by another `lang` or
      `xml:lang` attribute in a descendant element. `lang` wins over `xml:lang`,
      if both are present in the same tag.

    .. note:: These rules are, as mentioned, a subset of RDFaCore_ and differ
        in the following four simplifications:

        - This parser does not support safe CURIEs (i.e. normal CURIEs enclosed
          by square brackets), because they are considered obsolete by the W3C,
          they are a disproportionate complication of IRI resolving and, above
          all, of RDFa rules - e.g. sometimes only normal CURIEs are allowed,
          sometimes also safe CURIEs (cp. RDFaCoreCURIE_, RDFaCoreSyntax_).
        - This parser does not support the attribute `inlist`, because it is a
          significant complication and the meaning, which it is supposed to
          convey syntactically, is more explicitly expressed lexically, i.e.
          with the help of an appropriate vocabulary (e.g. `first author` by
          `http://d-nb.info/standards/elementset/gnd#firstAuthor`).
        - This parser does not support the attributes `rel`, `rev`, `resource`
          and `typeof`, because they are never inevitably necessary to express
          triples and they can cause chaining. *Chaining is avoided completely*,
          because its rules are a very disproportionate complication of RDFa;
          they can, e.g., easily lead to triples not intended by the annotator
          (cp. RDFaCoreChaining_, RDFaCoreChaining2_).
        - This parser does not use the value of a `vocab` attribute to generate
          a triple like `<(base)> <http://www.w3.org/ns/rdfa#usesVocabulary>
          <(value from @vocab)> .` (Cf. RDFaCoreVocab_) For this is a hardly
          intended, hardly useful triple, as its information is already given by
          the IRIs (because they must be resolved) and it is moreover misleading
          in so far, as - according to the RDFa rules - a vocabulary given in
          the initial context does not yield an analogous triple.

    .. _RDFaCore: http://www.w3.org/TR/2013/REC-rdfa-core-20130822
    .. _RDFaCoreChaining: http://www.w3.org/TR/2013/REC-rdfa-core-20130822/#s_chaining
    .. _RDFaCoreChaining2: http://www.w3.org/TR/2013/REC-rdfa-core-20130822/#chaining-with-property-and-typeof
    .. _RDFaCoreCURIE: http://www.w3.org/TR/2013/REC-rdfa-core-20130822/#s_curieprocessing
    .. _RDFaCoreSyntax: http://www.w3.org/TR/2013/REC-rdfa-core-20130822/#s_syntax
    .. _RDFaCoreVocab: http://www.w3.org/TR/2013/REC-rdfa-core-20130822/#PS-default-vocabulary
    .. _RDFaLite: http://www.w3.org/TR/2012/REC-rdfa-lite-20120607/
    '''

    def __init__(self, base: str = '', lang: str = '', vocab: str = '',
            prefixes: 'dict[str, str]' = {
            'cc':      'http://creativecommons.org/ns#',
            'ctag':    'http://commontag.org/ns#',
            'dc':      'http://purl.org/dc/terms/',
            'dcterms': 'http://purl.org/dc/terms/',
            'dc11':    'http://purl.org/dc/elements/1.1/', # old version of `dc`
            'describedby': 'http://www.w3.org/2007/05/powder-s#describedby',
            'foaf':    'http://xmlns.com/foaf/0.1/',
            'gr':      'http://purl.org/goodrelations/v1#',
            'grddl':   'http://www.w3.org/2003/g/data-view#',
            'ical':    'http://www.w3.org/2002/12/cal/icaltzd#',
            'license': 'http://www.w3.org/1999/xhtml/vocab#license',
            'ma':      'http://www.w3.org/ns/ma-ont#',
            'og':      'http://ogp.me/ns#',
            'owl':     'http://www.w3.org/2002/07/owl#',
            'prov':    'http://www.w3.org/ns/prov#',
            'rdf':     'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
            'rdfa':    'http://www.w3.org/ns/rdfa#',
            'rdfs':    'http://www.w3.org/2000/01/rdf-schema#',
            'rev':     'http://purl.org/stuff/rev#',
            'rif':     'http://www.w3.org/2007/rif#',
            'role':    'http://www.w3.org/1999/xhtml/vocab#role',
            'rr':      'http://www.w3.org/ns/r2rml#',
            'schema':  'http://schema.org/',
            'sd':      'http://www.w3.org/ns/sparql-service-description#',
            'sioc':    'http://rdfs.org/sioc/ns#',
            'skos':    'http://www.w3.org/2004/02/skos/core#',
            'skosxl':  'http://www.w3.org/2008/05/skos-xl#',
            'v':       'http://rdf.data-vocabulary.org/#', # deprecated
            'vcard':   'http://www.w3.org/2006/vcard/ns#',
            'void':    'http://rdfs.org/ns/void#',
            'wdr':     'http://www.w3.org/2007/05/powder#',
            'wdrs':    'http://www.w3.org/2007/05/powder-s#',
            'xhv':     'http://www.w3.org/1999/xhtml/vocab#',
            'xml':     'http://www.w3.org/XML/1998/namespace',
            'xsd':     'http://www.w3.org/2001/XMLSchema#',
            }):
        '''
        The docstring of the class explains the following parameters within
        the context of RDFa parsing in the whole.

        :param base: a string, serving as default subject and for resolving
            relative IRIs. Normally the IRI of the page containing the RDFa
            to be extracted and taken from a `<base>`-tag of the page
            (e.g. by :func:`.get_base`).
        :param lang: the initial language value to be stored as information
            about any literal object, if no datatype is specified for that
            object, and as long as this initial language value is not
            overwritten (in the manner described in the docstring of this
            class).
        :param vocab: the initial `vocab` value for resolving terms.
        :param prefixes: maps prefixes to their resolution, used to resolve
            prefixes. Should contain at least the default prefixes given in
            http://www.w3.org/2011/rdfa-context/rdfa-1.1; see further the
            docstring of this class about prefix resolution.
        '''
        super().__init__(convert_charrefs = True)
        self.base            = base
        self.types           = deque([''])
        self.langs           = deque([lang])
        self.vocabs          = deque([vocab])
        self.prefixes        = deque([prefixes])
        self.subjects        = deque([base])
        self.predicates      = deque([None])
        self.obj_lits        = deque([None])
        self.obj_lits_nested = deque([[False, None]])
        self.obj_iris        = deque([None])
        self.triples         = deque()

    def handle_data(self, data: str):
        '''Append :param:`data` to any open inner-content literal.'''
        for is_xml_lit, lit in self.obj_lits_nested:
            if lit != None:
                lit.append(data)

    def handle_endtag(self, tag: str):
        '''
        Generate a triple for each pending predicate.
        Forget the vocab, prefix, lang, datatype and subject values which have
        been valid for this tag.
        Append the tagname to any open inner-content literal, if its datatype
        is `XMLLiteral`.
        '''
        if self.predicates[-1]:
            is_xml_lit, obj_lits_nested = self.obj_lits_nested[-1]
            objs = (
                    (self.obj_lits[-1], False),
                    (''.join(obj_lits_nested) if obj_lits_nested != None
                     else None, False),
                    (self.obj_iris[-1], True),
                    )
            obj, is_iri = [ (obj, is_iri)
                    for obj, is_iri in objs if obj != None ][0]
            for predicate in self.predicates[-1]:
                self.triples.append((
                        self.subjects[-1],
                        predicate,
                        {
                        'value': obj,
                        'lang'  : '' if is_iri else self.langs[-1],
                        'type'  : '' if is_iri else self.types[-1],
                        'is_iri': is_iri,
                        }))
        self.vocabs.pop()
        self.prefixes.pop()
        self.langs.pop()
        self.types.pop()
        self.subjects.pop()
        self.predicates.pop()
        self.obj_lits.pop()
        self.obj_lits_nested.pop()
        self.obj_iris.pop()
        for is_xml_lit, lit in self.obj_lits_nested:
            if is_xml_lit and lit != None:
                lit.append(f'</{tag}>')

    def handle_startendtag(self, tag: str, attrs: 'list[tuple[str, None|str]]'):
        self.handle_starttag(tag, attrs)
        self.handle_endtag(tag)

    def handle_starttag(self, tag: str, attrs: 'list[tuple[str, None|str]]'):
        '''
        Append the tag to any open inner-content literal, if its datatype is
        `XMLLiteral`.
        Update the vocab, prefix, lang, datatype and subject values.
        If a `property` attribute is given in this tag, take its values as new
        predicate(s) and take an object value from a `content`, `href` or `src`
        attribute or open a container to collect the object value from the
        following inner content of the element.
        '''
        attrs = dict(attrs)
        keys = attrs.keys()
        for is_xml_lit, lit in self.obj_lits_nested:
            if is_xml_lit and lit != None:
                lit.append('<{}{}>'.format(
                        tag,
                        ''.join( ' {}="{}"'.format(
                            key,
                            html.escape(val))
                            for key, val in attrs.items() )))
        self.vocabs.append(attrs.get('vocab', self.vocabs[-1]))
        self.prefixes.append(self.prefixes[-1])
        if 'prefix' in keys:
            i = iter(attrs['prefix'].split())
            self.prefixes[-1].update(
                    { prefix.rstrip(':'): iri for prefix, iri in zip(i, i) })
        self.langs.append(
                attrs.get('lang', attrs.get('xml:lang', self.langs[-1])))
        self.types.append(
                self.resolve(attrs['datatype']) if 'datatype' in keys else '')
        self.subjects.append(self.resolve(attrs['about'])
                if 'about' in keys else self.subjects[-1])
        if 'property' in keys:
            self.predicates.append([ self.resolve(predicate, self.vocabs[-1])
                    for predicate in attrs['property'].split() ])
            if 'content' in keys:
                self.obj_lits.append(attrs.get('content'))
                self.obj_lits_nested.append([False, None])
                self.obj_iris.append(None)
            elif ('href' in keys or 'src' in keys) and not ('datatype' in keys):
                self.obj_lits.append(None)
                self.obj_lits_nested.append([False, None])
                self.obj_iris.append(self.resolve(attrs.get
                                                  ('href', attrs.get('src'))))
            else:
                self.obj_lits.append(None)
                self.obj_lits_nested.append((
                        True if re.search(r'[/#:]XMLLiteral$', self.types[-1])
                        else False,
                        deque(),
                        ))
                self.obj_iris.append(None)
        else:
            self.predicates.append(None)
            self.obj_lits.append(None)
            self.obj_lits_nested.append([False, None])
            self.obj_iris.append(None)

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

    def resolve(self, iri: str, isterm: bool = False) -> str:
        '''
        Resolve :param:`iri`:

        - Replace any prefix with the mapping given in the current (i.e. last)
          item of `self.prefixes`.
        - After that, resolve a relative path by appending it to a base. If
          :param:`isterm` is ``True``, the base is the current (i.e. last)
          item of `self.vocabs`, else, it is `self.base`.
        - Leave absolute IRIs, unknown prefixes and blank nodes unchanged.
        '''
        iri_stripped = iri.strip()
        iri = iri_stripped if iri_stripped else iri
        return iri if iri.startswith('_:') else resolve(iri,
                self.vocabs[-1] if isterm else self.base, self.prefixes[-1])

def convert_triples_to_nt(triples: 'Iterable[tuple[str, str, dict[str, str]]]'
        ) -> str:
    '''Convert the given RDF :param:`triples` to N-Triples.'''
    return '\n'.join( ' '.join((
            '<{}>'.format(s),
            '<{}>'.format(p),
            '<{}>'.format(o['value']) if o['is_iri']
                else '"{}"{}'.format(
                    escape_nt_literal(o['value']),
                    '^^' + o['type'] if o['type'] else
                    '@' + o['lang'] if o['lang'] else
                    ''),
            '.')) for s, p, o in triples )

def escape_nt_literal(literal: str) -> str:
    '''
    Escape an N-Triples literal. Note, that N-Triples strings are UTF-8
    encoded and may contain any Unicode character except for the four
    syntactically used characters backslash, newline, carriage return and
    quote.
    '''
    for old, new in (
            ('\\', '\\\\'),
            ('\n', '\\n'),
            ('\r', '\\r'),
            ('"' , '\\"'),
            ):
        literal = literal.replace(old, new)
    return literal

def get_base(doc: str) -> str:
    '''
    Return the IRI given in the `href` of the first `base`-element of
    :param:`doc`. If this is not available, return the empty string.
    Note, that the return value - even if not empty - may be a relative IRI,
    which would have to be resolved relative to the document's address in
    order to provide an absolute IRI as `base` value for :class:`RDFaParser`.
    '''
    base = re.search(
            r'''<\s*base\s(?:[^>]*?\s)?href\s*=\s*'''
            r'''(?:"?([^"\s>]*)|'([^'\s>]*))''', doc)
    return base.group(1) if base else ''

def resolve(iri: str, base: str = '', prefixes: 'dict[str, str]' = {}) -> str:
    '''
    Resolve :param:`iri`:

    - Replace any prefix with the mapping given in :param:`prefixes`.
    - After that, resolve a relative path by appending it to :param:`base`.
    - Leave absolute IRIs and unknown prefixes unchanged.
   '''
    if re.match('[^/?#.]+?:(?!/)', iri):
        # `iri` might be prefixed.
        prefix, rest = iri.split(':', 1)
        if prefix in prefixes:
            iri = prefixes[prefix] + rest
        else:
            return iri
    return urllib.parse.urljoin(base, iri)

if __name__ == '__main__':
    import doctest
    import web_io
    doctest.testmod()

    # Enquire parameters and start the process.
    while inpath := input('''
Please give the path to the file or to the webpage to be parsed, e.g. …
    C:\MyFolder\MyFile
… or …
    https://example.org/page
The encoding of this file or page must be UTF-8.
If you want to quit, write nothing.
Finally, press the enter key.
'''):
        if re.match('https?://', inpath):
            doc = web_io.read(inpath)
        else:
            with open(inpath, 'r', encoding = 'utf-8') as infile:
                doc = infile.read()
        parser = RDFaParser(base = get_base(doc))
        parser.parse(doc)
        print()
        print('Output (written in N-Triples):')
        print('────────────────')
        print(convert_triples_to_nt(parser.triples))
        print('━━━━━━━━━━━━━━━━')
