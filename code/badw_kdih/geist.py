# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import json
import os
try: import regex as re
except ImportError: import re
import stat
import sys
import time
from collections import deque

from beaker.middleware import SessionMiddleware

import gehalt
import __init__
import bottle
import fs
import web_io
from bottle import HTTPError, redirect, request, response, static_file

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        if not db.debug:
            server.quiet = True
        self.kwargs = {
                'app': self if SessionMiddleware is None else
                    SessionMiddleware(self, db.config['session']),
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.

        The following has some quirks due to the fact that the public site is
        behind another webserver which must receive URLs with a path starting
        with “/datenbank”, whereas it passes them on to this system sans this
        “/datenbank” (and also with “http” instead of “https”).
        '''
        @self.hook('before_request')
        def status():
            request.user_id, request.user_name, request.roles = db.auth(request)

        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/kdih/<path:path>')
        def redirect_legacy(path):
            redirect('/datenbank/' + path)

        @self.route('/datenbank')
        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which specify no page.
            Assume the default page of the site.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/datenbank/' + db.page_id)

        @self.route('/datenbank/ablage')
        @self.route(          '/ablage')
        def ablage(note = '', url = ''):
            if not request.roles:
                redirect('/datenbank/' + db.page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'ablage.tpl',
                        'note': note,
                        'repros': db.list_repros(),
                        })

        @self.route('/datenbank/ablage', method = 'POST')
        @self.route(          '/ablage', method = 'POST')
        def ablage_post():
            if not request.roles:
                redirect('/datenbank/' + db.page_id)
            note = ''
            try:
                if request.forms.redaction == 'delete':
                    name = os.path.basename(request.forms.filename)
                    assert name
                    path = os.path.join(db.repros_path, name)
                    os.chmod(path, stat.S_IWRITE)
                    os.remove(path)
                else:
                    for file in request.files.getall('file'):
                        assert file
                        name = fs.sanitize(file.raw_filename)
                        assert name
                        path = os.path.join(db.repros_path, name)
                        file.save(path, overwrite = True, max_size = 1024 ** 3)
            except Exception as e:
                note = str(e)
            return ablage(note = note)

        @self.route('/datenbank/api/hsc/urls.json')
        @self.route(          '/api/hsc/urls.json')
        def return_hsc():
            '''
            Return JSON telling all URLs of all pages linking to the
            handschriftencensus and the belonging handschriftencensus-URLs.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {
                        'name': db.config['ids']['name'],
                        'siglum': db.config['ids']['siglum'],
                        'url': db.config['ids']['url'],
                        'email': db.config['ids']['email'],
                        'data_info': db.config['api_hsc']['data_info'],
                        'data_url': db.config['api_hsc']['data_url'],
                        'data': tuple(db.get_hsc_urls()),
                    },
                    separators = (',', ':'),
                    )

        @self.route('/datenbank/api/<kind>/register.json')
        @self.route(          '/api/<kind>/register.json')
        def return_register(kind):
            '''
            Return JSON containing the register enriched with data from
            other tables.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    tuple(db.get_reg_items(kind, request)),
                    separators = (',', ':'),
                    )

        @self.route('/datenbank/auth_end')
        @self.route(          '/auth_end')
        def auth_end():
            session = request.environ.get('beaker.session', None)
            if session is not None:
                session.delete()
            redirect(request.query.url or '/datenbank/' + db.page_id)

        @self.route('/datenbank/' + db.auth_in_page_id)
        @self.route(          '/' + db.auth_in_page_id)
        def auth_in():
            return template('base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_in.tpl'})

        @self.route('/datenbank/' + db.auth_in_page_id, method = 'POST')
        @self.route(          '/' + db.auth_in_page_id, method = 'POST')
        def auth_in_post():
            user_name = request.forms.user_name
            password = request.forms.password
            user_id = db.auth_in(user_name, password)
            if user_id is None:
                query = request.forms.query
                if query:
                    query = '?' + query
                redirect(request.fullpath + query)
            else:
                session = request.environ['beaker.session']
                session['user_id'] = user_id
                session.save()
                redirect(request.forms.url or '/datenbank/' + db.page_id)

        @self.route('/datenbank/cssjs/<path:path>')
        @self.route(          '/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/datenbank/cssjs/jquery/jquery.min.js`
            - `/datenbank/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>')
        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>')
        @self.route('/datenbank/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>/<ein>')
        @self.route(          '/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>')
        @self.route(          '/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>')
        @self.route(          '/<kind:re:(druck|handschrift|stoffgruppe|untergruppe)>/<stoff>/<unter>/<ein>')
        def return_item(kind, stoff, unter = None, ein = None):
            item = db.get_item(kind, stoff, unter, ein, request)
            if item:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl':
                                'eintrag.tpl'
                                    if kind in {'druck', 'handschrift'} else
                                kind + '.tpl',
                            'page_id': '/'.join(
                                filter(None, (kind, stoff, unter, ein))),
                            'item': item,
                            'kind': kind,
                            })
            else: redirect('/datenbank/404')

        @self.route('/datenbank/edit/reg/<item_id:int>', method = ['GET', 'POST'])
        @self.route(          '/edit/reg/<item_id:int>', method = ['GET', 'POST'])
        def edit_reg_item(item_id):
            note = ''
            new = True if item_id == 0 else False
            item = { # Not simply ``dict(request.forms)``: wrong decoding.
                    key: getattr(request.forms, key, '')
                    for key in request.forms.keys()
                    } if request.method == 'POST' else {}
            try:
                roles = {'Admin', 'Hauptredakteur', 'Mitarbeiter'}
                assert request.roles & roles, f'Nicht angemeldet als: {roles}'
                if request.method == 'GET':
                    if not new:
                        item = db.get_reg_item(item_id, for_edit = True)
                else:
                    item_id, item = db.upsert_reg_item(item_id, item)
            except Exception as e:
                note = f'Abbruch: {e}'
            if request.method == 'POST' and new:
                redirect(f'/datenbank/edit/reg/{item_id}')
            if request.method == 'GET' and note:
                return note
            else:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'edit_reg_item.tpl',
                            'item_id': item_id,
                            'item': item,
                            'page_id': f'edit/reg/{item_id}',
                            'note': note,
                            })

        @self.route('/datenbank/ersatzindex/<page_id>')
        @self.route(          '/ersatzindex/<page_id>')
        def return_ersatzindex(page_id):
            '''
            Return an ersatz for the datatable that would otherwise be shown
            on the page :param:`page_id`.
            '''
            table = deque('<table class="index">')
            exists = False
            for row in db.get_index(page_id, request):
                exists = True
                table.append('<tr><td>' + '</td> <td>'.join(
                        val['_'] if isinstance(val, dict) else str(val)
                        for _, val in sorted(row.items())
                        ) + '</td></tr>')
            if not exists:
                redirect('/datenbank/404')
            table.append('</table>')
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'text.tpl', 'text': ''.join(table)})

        @self.route('/datenbank/filter')
        @self.route(          '/filter')
        def return_filter():
            '''
            Return a page of results filtered according to the filters sent with
            ``request.query.text``.

            Or, if no ``request.query.text`` is sent, return a form for extended
            filtering.
            '''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'filter.tpl',
                        'starttag': '',
                        'endtag': '',
                        'table_config': {
                            'ajax': f'/datenbank/filter/json?{request.query_string}',
                            'columns': [
                                {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                                None,
                                None,
                                ],
                            },
                        })

        @self.route('/datenbank/filter/json')
        @self.route(          '/filter/json')
        def return_filter_json():
            '''
            Return JSON for a datatable of filtered results.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.filter(request))},
                    separators = (',', ':'))

        @self.route('/datenbank/indices/<kind>/data')
        @self.route(          '/indices/<kind>/data')
        def return_index(kind):
            '''
            Return JSON for a datatable of e.g. MSS (or other entities,
            specified by :param:`kind`).
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.get_index(kind, request))},
                    separators = (',', ':'))

        @self.route('/datenbank/literatur/<bibl_id:int>')
        @self.route(          '/literatur/<bibl_id:int>')
        def return_item_bibl(bibl_id):
            '''
            Return a page about the bibliographical item :param:`bibl_id`.
            Such pages are the target of bibliographical links, given in
            the description of Stoffgruppen, Untergruppen, Einträgen.
            '''
            item = db.get_item_bibl(bibl_id)
            if item:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'bibl.tpl',
                            'page_id': f'literatur/{bibl_id}',
                            'item': item,
                            })
            else: redirect('/datenbank/404')

        @self.route('/datenbank/literatur_sg/<stoff_nr>')
        @self.route(          '/literatur_sg/<stoff_nr>')
        def return_index_of_literature_of_stoffgruppe(stoff_nr):
            text = template(
                    'literatur_sg.tpl', db = db, request = request,
                    kwargs = {'stoff_nr': stoff_nr}
                    ).encode()
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'index.tpl',
                        'page_id': 'literatur_sg',
                        'text': text,
                        'indexinfolink': 'hinweise#indexfilter',
                        })

        @self.route('/datenbank/redact/<table>/<item_id:int>', method = 'POST')
        @self.route(          '/redact/<table>/<item_id:int>', method = 'POST')
        def redact(table, item_id):
            '''
            Redact the item :param:`item_id` of the table :param:`table`.
            '''
            if not request.roles & {'Admin', 'Hauptredakteur', 'Mitarbeiter'}:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            if request.forms.redaction == 'delete':
                db.delete(table, item_id)
                redirect(f'/datenbank/{table}')

        @self.route('/datenbank/reg/<item_id:int>')
        @self.route(          '/reg/<item_id:int>')
        def return_reg_item(item_id):
            item = db.get_reg_item(item_id)
            if item:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'reg_item.tpl',
                            'page_id': f'reg/{item_id}',
                            'item_id': item_id,
                            'item': item,
                            'starttag': '',
                            'endtag': '',
                            'table_config': {
                                'ajax': f'/datenbank/filter/json?reg_id={item_id}',
                                'columns': [
                                    {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                                    None,
                                    None,
                                    ],
                                },
                            })
            else: redirect('/datenbank/404')

        @self.route('/datenbank/repro/<path:path>')
        @self.route(          '/repro/<path:path>')
        def return_repro(path):
            '''
            Return a file from the folder of repros.
            '''
            return static_file(
                    path,
                    db.repros_path,
                    download = ('export' in request.query),
                    )

        @self.route('/datenbank/<page_id>')
        @self.route(          '/<page_id>')
        def return_page(page_id):
            '''
            Return the page :param:`page_id`.

            If the given page is not found, show a 404-page.
            '''
            text, tpl = db.get_text_and_template_name('', page_id)
            if text:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'text': text,
                            'ersatzlink': ('/datenbank/ersatzindex/' + page_id)
                                if tpl == 'index.tpl' else '',
                            'indexinfolink': 'hinweise#indexfilter',
                            })
            else: redirect('/datenbank/404')

        @self.route('/datenbank/-/<path:path>')
        @self.route(          '/-/<path:path>')
        def return_static_content(path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/-/icons/favicon.ico`
            - `/-/acta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(1)

def template(
        *args,
        alternatives_re = re.compile(r'\{\{([^}]*)\}\{([^}]*)\}\}'),
        **kwargs,
        ) -> str:
    doc = bottle.template(*args, **kwargs)
    if '{{' in doc:
        docparts = doc.split('</head>', 1)
        if len(docparts) == 2:
            docparts[1] = alternatives_re.sub('\g<1>', docparts[1])
            doc = f'{docparts[0]}</head>{docparts[1]}'
    if 'request' in kwargs:
        mark = request.query.mark
        if mark:
            doc = web_io.mark(doc, mark)
        css = request.query.css
        if css:
            doc = doc.replace('</head>', f'\t<style>{css}</style>\n</head>')
    return doc

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
