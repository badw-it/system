% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying an index of bibliographical items for a certain stoffgruppe.
% lang_id = kwargs['lang_id'] = db.lang_id
% stoff_nr = kwargs.get('stoff_nr', '')
<article lang="{{lang_id}}" class="index" data-template="index.tpl"
	data-table_config_json='{
		"ajax": "/datenbank/indices/literatur/data?stoff_nr={{stoff_nr}}",
		"columns": [
			{
				"data": "0",
				"render": {
					"_": "_",
					"sort": "s"
				}
			},
			{
				"data": "1",
				"render": {
					"_": "_",
					"sort": "s"
				}
			},
			{
				"data": "2",
				"render": {
					"_": "_",
					"sort": "s"
				}
			},
			{
				"data": "3",
				"render": {
					"_": "_",
					"sort": "s"
				}
			},
			null
		]
	}'
style="min-width: 60%">
<h1>Literatur zur Stoffgruppe {{stoff_nr}}</h1>
<table id="index">
<thead>
	<tr>
		<th>Kurztitel</th>
		<th>Volltitel</th>
		<th>Bände</th>
		<th>Stoffgruppen</th>
		<th>Standardwerk</th>
	</tr>
</thead>
</table>
</article>