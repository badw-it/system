% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% sg_title = item['f_stoffgr_ueberschrift'].replace('<p>', '').replace('</p>', '').strip()
% ug_title = title = item['f_untergr_ueberschrift'].replace('<p>', '').replace('</p>', '').strip()
% if len(title) > 60:
%     title = title.replace(', ›', ', <br/>›')
% end
% volume = item['f_band_nr'] or ''

<main>
<article class="sheet wide with_kolumnentitel">

<nav class="kolumnentitel">
	<a class="key" href="/datenbank/stoffgruppe/{{item['f_stoffgr_nr']}}">↑ {{item['f_stoffgr_nr']}}.</a>&#8192;{{!sg_title}}
</nav>

<div class="head">
<h2>{{item['f_stoffgr_nr']}}.{{item['f_untergr_nr']}}.&#8192;{{!title}}</h2>
<p>{{item['authority']}}</p>
% if volume[:1] == '0':
<p class="volume">Ergänzung zum gedruckten KdiH</p>
% else:
<p class="volume">KdiH-Band {{volume}}</p>
% end
</div>

<div id="ug-einleitung">
{{!item['f_untergr_beschreibung']}}
</div>

% term = item['f_untergr_edition']
% if term:
<div class="inlining petit" style="margin-top: 1.5em">
<strong>Editionen:</strong> {{!term}}
</div>
% end

% term = item['f_untergr_lit']
% if term:
<div class="inlining petit">
<strong>Literatur zu den Illustrationen:</strong> {{!term}}
</div>
% end

% term = item['f_untergr_siehe']
% if term:
<div class="inlining petit">
<strong>Siehe auch:</strong>
{{!term}}
</div>
% end

% term = item['f_untergr_notiz']
% if term:
<div class="inlining petit">
{{!term}}
</div>
% end

<aside class="citebox">
	<p>Empfohlene Zitierweise</p>
	<p>{{item['authors'] + ': ' if item['authors'] else ''}}{{!sg_title}}. {{!ug_title}} (Nr. {{item['f_stoffgr_nr']}}.{{item['f_untergr_nr']}}.). In: {{item['title']}} \\
  % if volume[:1] == '0':
	{{'Ergänzung zum gedruckten KdiH'}}. \\
  % else:
	Band {{volume}}. {{item['place']}} {{item['year']}}. \\
  % end
	http://kdih.badw.de/datenbank/untergruppe/{{item['f_stoffgr_nr']}}/{{item['f_untergr_nr']}}; zuletzt geändert am {{item['modified_on'].strftime('%d.%m.%Y')}}.</p>
</aside>

</article>
</main>