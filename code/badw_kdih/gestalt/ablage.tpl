% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
<main>
<article class="just sheet wide">
	<h1>Bildablage</h1>
	<h2>Datei hochladen</h2>
	<form enctype="multipart/form-data" method="post">
		<ol class="list">
			<li>Schritt: <input class="key" multiple="" type="file" name="file"/></li>
			<li>Schritt: <button type="submit" formaction="/datenbank/ablage">Auswahl hochladen.</button></li>
		</ol>
	</form>
	<h2>Derzeit hochgeladene Dateien</h2>
  % if kwargs['repros']:
	<ol class="list">
	% for name in kwargs['repros']:
		<li>
			<div class="flexrow">
				<span>{{name}}</span>
				<button onclick="window.open('/datenbank/repro/{{db.q(name)}}', '_blank', 'popup')" type="button">👁</button>
				<a class="key" download="{{name}}" href="/datenbank/repro/{{db.q(name)}}?export=1"> ⭳ </a>
				<form action="/datenbank/ablage" method="post">
					<input type="hidden" name="filename" value="{{name}}"/>
					<button class="lid" type="button">✕</button><button name="redaction" value="delete">✕</button>
				</form>
			</div>
		</li>
	% end
	</ol>
  % else:
	<p>(keine)</p>
  % end
</article>
</main>
