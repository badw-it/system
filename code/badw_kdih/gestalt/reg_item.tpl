% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% item = kwargs['item']
<main>
<article class="just sheet wide">
	<p><strong>{{item['reg_art']}}</strong></p>
	<h1>{{!item['bezeichnung']}}</h1>
  % if item['reg_nebenbezeichnung']:
	<p>{{item['reg_nebenbezeichnung']}}</p>
  % end
  % if item['reg_url']:
	<p><a href="{{item['reg_url']}}">{{item['reg_url']}}</a></p>
  % end
  % if item['reg_url_gnd']:
	<p><a href="{{item['reg_url_gnd']}}">{{item['reg_url_gnd']}}</a></p>
  % end
	<h2>Verzeichnungen:</h2>
	<% kwargs['text'] = '''
	<div class="index">
		<table id="index">
			<thead>
				<tr>
					<th>Nr. &amp; Link</th>
					<th data-select="">Bereich</th>
					<th>Fund</th>
				</tr>
			</thead>
		</table>
	</div>
	'''
	%>
% include('index.tpl', db = db, request = request, kwargs = kwargs)
</article>
</main>