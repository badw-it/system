% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="sheet wide">
<dl>
<dt>{{!item['f_kurztitel']}}</dt>
<dd>
{{!item['f_volltitel']}}
</dd>
</dl>
</article>
</main>