% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% sg_title = title = item['f_stoffgr_ueberschrift'].replace('<p>', '').replace('</p>', '').strip()
% if len(title) > 60:
%     title = title.replace(', ›', ', <br/>›')
% end
% volume = item['f_band_nr'] or ''

<main class="stoffgruppe">
<article class="sheet wide">

<div class="head">
<h1>{{item['f_stoffgr_nr']}}.&#8192;{{!title}}</h1>
<p>{{item['authority']}}</p>
% if volume[:1] == '0':
<p class="volume">Ergänzung zum gedruckten KdiH</p>
% else:
<p class="volume">KdiH-Band {{volume}}</p>
% end
</div>

<div id="sg-einleitung">
{{!item['f_stoffgr_beschreibung']}}
</div>

% term = item['f_stoffgr_edition']
% if term:
<div class="inlining petit" style="margin-top: 1.5em">
<strong>Editionen:</strong> {{!term}}
</div>
% end

% term = item['f_stoffgr_lit']
% if term:
<div class="inlining petit">
<strong>Literatur zu den Illustrationen:</strong> {{!term}}
</div>
% end

% term = item['f_stoffgr_siehe']
% if term:
<div class="petit">
<strong>Siehe auch:</strong>
{{!term}}
</div>
% end

% term = item['f_stoffgr_notiz']
% if term:
<div class="inlining petit" style="margin-top: 1.5em">
{{!term}}
</div>
% end

% if item['f_stoffgr_nr'] != '43':
<aside class="citebox">
	<p>Empfohlene Zitierweise</p>
	<p>{{item['authors']}}: {{!sg_title}} (Nr. {{item['f_stoffgr_nr']}}.). In: {{item['title']}} \\
  % if volume[:1] == '0':
	Ergänzung zum gedruckten KdiH. \\
  % else:
	Band {{volume}}. {{item['place']}} {{item['year']}}. \\
  % end
	http://kdih.badw.de/datenbank/stoffgruppe/{{item['f_stoffgr_nr']}}; zuletzt geändert am {{item['modified_on'].strftime('%d.%m.%Y')}}.</p>
</aside>
% end

</article>
</main>