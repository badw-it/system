% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id'] = db.lang_id
% page_id = kwargs.setdefault('page_id', request.path.rsplit('/', 1)[-1])
% kwargs['prepath'] = '/datenbank'
% item = kwargs.get('item', {})
% ugs = item.get('ugs', [])
% mss = item.get('mss', [])
% prints = item.get('prints', [])
% stoff_nr = item.get('f_stoffgr_nr', '')
% unter_nr = item.get('f_untergr_nr', '')
% nr = item.get('f_hs_position_nr', '')
% note = kwargs.get('note', '')
<!DOCTYPE html>
<html id="top" class="desk txt" lang="{{lang_id}}">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="{{request.path}}" rel="canonical"/>
	<link href="/datenbank/-/icons/favicon.ico" rel="icon"/>
	<link href="/datenbank/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/datenbank/cssjs/badw_kdih.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/datenbank/cssjs/jquery/jquery.min.js"></script>
	<script src="/datenbank/cssjs/all.js?v={{db.startsecond}}"></script>
	<script src="/datenbank/cssjs/badw_kdih.js?v={{db.startsecond}}"></script>
	<title>KdiH: {{page_id[0].upper() + page_id[1:]}}</title>
</head>
<body>
% if note:
<input class="flip" id="flip1" type="checkbox"/><label class="key notice top" for="flip1"></label><article class="card">{{note}}</article>
% end
<header>
	<a class="img" href="/datenbank"><img class="shadow" src="/datenbank/-/icons/main.png" alt="KdiH"/></a>
	<h1><a href="/datenbank">KdiH</a></h1>
	<form action="/datenbank/filter" method="get">
		<input type="search" name="text" value="{{request.query.text}}" aria-label="{{db.glosses['search_term'][lang_id]}}" placeholder="Suche" id="main_input"/>
		<div class="droparea shadow">
			<table>
				<tr><th><kbd>_</kbd></th> <td> (der Unterstrich) ist Platzhalter für genau ein Zeichen.</td></tr>
				<tr><th><kbd>%</kbd></th> <td> (das Prozentzeichen) ist Platzhalter für kein, ein oder mehr als ein Zeichen.</td></tr>
			</table>
			<p class="petit">Ganz am Anfang und ganz am Ende der Sucheingabe sind die Platzhalterzeichen überflüssig.</p>
			<a class="key" title="latin small letter sharp s" onclick="insertAtCursor('main_input', '&#223;');">&#223;</a> 
			<a class="key" title="middle dot" onclick="insertAtCursor('main_input', '&#183;');">&#183;</a> 
			<a class="key" title="copyright sign" onclick="insertAtCursor('main_input', '&#169;');">&#169;</a> 
			<a class="key" title="horizontal ellipsis" onclick="insertAtCursor('main_input', '&#8230;');">&#8230;</a> 
			<a class="key" title="en dash" onclick="insertAtCursor('main_input', '&#8211;');">&#8211;</a> 
			<a class="key" title="dagger" onclick="insertAtCursor('main_input', '&#8224;');">&#8224;</a> 
			<a class="key" title="feminine ordinal indicator" onclick="insertAtCursor('main_input', '&#170;');">&#170;</a> 
			<a class="key" title="masculine ordinal indicator" onclick="insertAtCursor('main_input', '&#186;');">&#186;</a> 
			<a class="key" title="apostrophe / right single quotation mark" onclick="insertAtCursor('main_input', '&#8217;');">&#8217;</a> 
			<a class="key" title="single left-pointing angle quotation mark" onclick="insertAtCursor('main_input', '&#8249;');">&#8249;</a> 
			<a class="key" title="single right-pointing angle quotation mark" onclick="insertAtCursor('main_input', '&#8250;');">&#8250;</a> 
			<a class="key" title="left-pointing double angle quotation mark" onclick="insertAtCursor('main_input', '&#171;');">&#171;</a> 
			<a class="key" title="right-pointing double angle quotation mark" onclick="insertAtCursor('main_input', '&#187;');">&#187;</a> 
			<a class="key" title="multiplication sign" onclick="insertAtCursor('main_input', '&#215;');">&#215;</a> 
			<a class="key" title="full block" onclick="insertAtCursor('main_input', '&#9608;');">&#9608;</a> 
			<a class="key" title="latin small letter ae" onclick="insertAtCursor('main_input', '&#230;');">&#230;</a> 
			<a class="key" title="latin small ligature oe" onclick="insertAtCursor('main_input', '&#339;');">&#339;</a> 
			<a class="key" title="latin capital letter c with cedilla" onclick="insertAtCursor('main_input', '&#199;');">&#199;</a> 
			<a class="key" title="latin small letter c with cedilla" onclick="insertAtCursor('main_input', '&#231;');">&#231;</a> 
			<a class="key" title="latin small letter c with caron" onclick="insertAtCursor('main_input', '&#269;');">&#269;</a> 
			<a class="key" title="latin small letter s with caron" onclick="insertAtCursor('main_input', '&#353;');">&#353;</a> 
			<a class="key" title="latin capital letter l with stroke" onclick="insertAtCursor('main_input', '&#321;');">&#321;</a> 
			<a class="key" title="latin small letter l with stroke" onclick="insertAtCursor('main_input', '&#322;');">&#322;</a> 
			<a class="key" title="combining acute accent" onclick="insertAtCursor('main_input', '&#769;');"> &#769;</a> 
			<a class="key" title="combining grave accent" onclick="insertAtCursor('main_input', '&#768;');"> &#768;</a> 
			<a class="key" title="combining tilde" onclick="insertAtCursor('main_input', '&#771;');"> &#771;</a> 
			<a class="key" title="combining diaeresis" onclick="insertAtCursor('main_input', '&#776;');"> &#776;</a> 
			<a class="key" title="combining macron" onclick="insertAtCursor('main_input', '&#772;');"> &#772;</a> 
			<a class="key" title="combining ring above" onclick="insertAtCursor('main_input', '&#778;');"> &#778;</a> 
			<a class="key" title="combining dot above" onclick="insertAtCursor('main_input', '&#775;');"> &#775;</a> 
			<a class="key" title="combining double acute accent" onclick="insertAtCursor('main_input', '&#779;');"> &#779;</a> 
			<a class="key" title="combining latin small letter a" onclick="insertAtCursor('main_input', '&#867;');"> &#867;</a> 
			<a class="key" title="combining latin small letter e" onclick="insertAtCursor('main_input', '&#868;');"> &#868;</a> 
			<a class="key" title="combining latin small letter i" onclick="insertAtCursor('main_input', '&#869;');"> &#869;</a> 
			<a class="key" title="combining latin small letter o" onclick="insertAtCursor('main_input', '&#870;');"> &#870;</a> 
			<a class="key" title="combining latin small letter u" onclick="insertAtCursor('main_input', '&#871;');"> &#871;</a> 
			<a class="key" title="combining latin small letter v" onclick="insertAtCursor('main_input', '&#878;');"> &#878;</a> 
			<a class="key" title="greek capital letter alpha" onclick="insertAtCursor('main_input', '&#913;');">&#913;</a> 
			<a class="key" title="greek capital letter beta" onclick="insertAtCursor('main_input', '&#914;');">&#914;</a> 
			<a class="key" title="greek capital letter gamma" onclick="insertAtCursor('main_input', '&#915;');">&#915;</a> 
			<a class="key" title="greek capital letter delta" onclick="insertAtCursor('main_input', '&#916;');">&#916;</a> 
			<a class="key" title="greek capital letter epsilon" onclick="insertAtCursor('main_input', '&#917;');">&#917;</a> 
			<a class="key" title="greek capital letter zeta" onclick="insertAtCursor('main_input', '&#918;');">&#918;</a> 
			<a class="key" title="greek capital letter eta" onclick="insertAtCursor('main_input', '&#919;');">&#919;</a> 
			<a class="key" title="greek capital letter theta" onclick="insertAtCursor('main_input', '&#920;');">&#920;</a> 
			<a class="key" title="greek capital letter iota" onclick="insertAtCursor('main_input', '&#921;');">&#921;</a> 
			<a class="key" title="greek capital letter kappa" onclick="insertAtCursor('main_input', '&#922;');">&#922;</a> 
			<a class="key" title="greek capital letter lamda" onclick="insertAtCursor('main_input', '&#923;');">&#923;</a> 
			<a class="key" title="greek capital letter mu" onclick="insertAtCursor('main_input', '&#924;');">&#924;</a> 
			<a class="key" title="greek capital letter nu" onclick="insertAtCursor('main_input', '&#925;');">&#925;</a> 
			<a class="key" title="greek capital letter xi" onclick="insertAtCursor('main_input', '&#926;');">&#926;</a> 
			<a class="key" title="greek capital letter omicron" onclick="insertAtCursor('main_input', '&#927;');">&#927;</a> 
			<a class="key" title="greek capital letter pi" onclick="insertAtCursor('main_input', '&#928;');">&#928;</a> 
			<a class="key" title="greek capital letter rho" onclick="insertAtCursor('main_input', '&#929;');">&#929;</a> 
			<a class="key" title="greek capital letter sigma" onclick="insertAtCursor('main_input', '&#931;');">&#931;</a> 
			<a class="key" title="greek capital letter tau" onclick="insertAtCursor('main_input', '&#932;');">&#932;</a> 
			<a class="key" title="greek capital letter upsilon" onclick="insertAtCursor('main_input', '&#933;');">&#933;</a> 
			<a class="key" title="greek capital letter phi" onclick="insertAtCursor('main_input', '&#934;');">&#934;</a> 
			<a class="key" title="greek capital letter chi" onclick="insertAtCursor('main_input', '&#935;');">&#935;</a> 
			<a class="key" title="greek capital letter psi" onclick="insertAtCursor('main_input', '&#936;');">&#936;</a> 
			<a class="key" title="greek capital letter omega" onclick="insertAtCursor('main_input', '&#937;');">&#937;</a> 
			<a class="key" title="greek small letter alpha" onclick="insertAtCursor('main_input', '&#945;');">&#945;</a> 
			<a class="key" title="greek small letter beta" onclick="insertAtCursor('main_input', '&#946;');">&#946;</a> 
			<a class="key" title="greek small letter gamma" onclick="insertAtCursor('main_input', '&#947;');">&#947;</a> 
			<a class="key" title="greek small letter delta" onclick="insertAtCursor('main_input', '&#948;');">&#948;</a> 
			<a class="key" title="greek small letter epsilon" onclick="insertAtCursor('main_input', '&#949;');">&#949;</a> 
			<a class="key" title="greek small letter zeta" onclick="insertAtCursor('main_input', '&#950;');">&#950;</a> 
			<a class="key" title="greek small letter eta" onclick="insertAtCursor('main_input', '&#951;');">&#951;</a> 
			<a class="key" title="greek small letter theta" onclick="insertAtCursor('main_input', '&#952;');">&#952;</a> 
			<a class="key" title="greek small letter iota" onclick="insertAtCursor('main_input', '&#953;');">&#953;</a> 
			<a class="key" title="greek small letter kappa" onclick="insertAtCursor('main_input', '&#954;');">&#954;</a> 
			<a class="key" title="greek small letter lamda" onclick="insertAtCursor('main_input', '&#955;');">&#955;</a> 
			<a class="key" title="greek small letter mu" onclick="insertAtCursor('main_input', '&#956;');">&#956;</a> 
			<a class="key" title="greek small letter nu" onclick="insertAtCursor('main_input', '&#957;');">&#957;</a> 
			<a class="key" title="greek small letter xi" onclick="insertAtCursor('main_input', '&#958;');">&#958;</a> 
			<a class="key" title="greek small letter omicron" onclick="insertAtCursor('main_input', '&#959;');">&#959;</a> 
			<a class="key" title="greek small letter pi" onclick="insertAtCursor('main_input', '&#960;');">&#960;</a> 
			<a class="key" title="greek small letter rho" onclick="insertAtCursor('main_input', '&#961;');">&#961;</a> 
			<a class="key" title="greek small letter sigma" onclick="insertAtCursor('main_input', '&#963;');">&#963;</a> 
			<a class="key" title="greek small letter final sigma" onclick="insertAtCursor('main_input', '&#962;');">&#962;</a> 
			<a class="key" title="greek small letter tau" onclick="insertAtCursor('main_input', '&#964;');">&#964;</a> 
			<a class="key" title="greek small letter upsilon" onclick="insertAtCursor('main_input', '&#965;');">&#965;</a> 
			<a class="key" title="greek small letter phi" onclick="insertAtCursor('main_input', '&#966;');">&#966;</a> 
			<a class="key" title="greek small letter chi" onclick="insertAtCursor('main_input', '&#967;');">&#967;</a> 
			<a class="key" title="greek small letter psi" onclick="insertAtCursor('main_input', '&#968;');">&#968;</a> 
			<a class="key" title="greek small letter omega" onclick="insertAtCursor('main_input', '&#969;');">&#969;</a> 
			<a class="key" title="combining greek ypogegrammeni" onclick="insertAtCursor('main_input', '&#837;');"> &#837;</a> 
			<a class="key" title="combining comma above right" onclick="insertAtCursor('main_input', '&#789;');"> &#789;</a> 
			<a class="key" title="combining reversed comma above" onclick="insertAtCursor('main_input', '&#788;');"> &#788;</a>
		</div><button type="submit">&#128270;&#65038;</button>
	</form>
	<nav>
		<ul>
			<li>{{!'<b>Erweiterte Suche</b>' if page_id == '/datenbank/filter' and not request.query.text else f'<a href="/datenbank/filter">Erweiterte Suche</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Stoffgruppen</b>' if page_id == 'stoffgruppen' else '<a href="/datenbank/stoffgruppen">Stoffgruppen</a>'}}</li>
			<li>{{!'<b>Untergruppen</b>' if page_id == 'untergruppen' else '<a href="/datenbank/untergruppen">Untergruppen</a>'}}</li>
		  % if ugs:
			<li>
				<ul class="p petit">
				  % for _, _stoff_nr, _unter_nr in ugs:
				   % if (_stoff_nr, _unter_nr) == (stoff_nr, unter_nr):
					<li>{{_stoff_nr}}.{{_unter_nr}}.</li>
				   % else:
					<li><a href="/datenbank/untergruppe/{{_stoff_nr}}/{{_unter_nr}}">{{_stoff_nr}}.{{_unter_nr}}.</a></li>
				   % end
				  % end
				</ul>
			</li>
		  % end
			<li>{{!'<b>Handschriften</b>' if page_id == 'handschriften' else '<a href="/datenbank/handschriften">Handschriften</a>'}}</li>
		  % if mss:
			<li>
				<ul class="p petit">
				  % for _, _stoff_nr, _unter_nr, _nr in mss:
				   % if (_stoff_nr, _unter_nr, _nr) == (stoff_nr, unter_nr, nr):
					<li>{{_stoff_nr}}.{{_unter_nr}}.{{_nr}}.</li>
				   % else:
					<li><a href="/datenbank/handschrift/{{_stoff_nr}}/{{_unter_nr}}/{{_nr}}">{{_stoff_nr}}.{{_unter_nr}}.{{_nr}}.</a></li>
				   % end
				  % end
				</ul>
			</li>
		  % end
			<li>{{!'<b>Drucke</b>' if page_id == 'drucke' else '<a href="/datenbank/drucke">Drucke</a>'}}</li>
		  % if prints:
			<li>
				<ul class="p petit">
				  % for _, _stoff_nr, _unter_nr, _nr in prints:
				   % if (_stoff_nr, _unter_nr, _nr) == (stoff_nr, unter_nr, nr):
					<li>{{_stoff_nr}}.{{_unter_nr}}.{{_nr}}.</li>
				   % else:
					<li><a href="/datenbank/druck/{{_stoff_nr}}/{{_unter_nr}}/{{_nr}}">{{_stoff_nr}}.{{_unter_nr}}.{{_nr}}.</a></li>
				   % end
				  % end
				</ul>
			</li>
		  % end
		  % if kwargs.get('kind', '') == 'stoffgruppe' or page_id == 'literatur_sg':
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Literatur zur Stoffgruppe</b>' if page_id == 'literatur_sg' else '<a href="/datenbank/literatur_sg/' + stoff_nr + '">Literatur zur Stoffgruppe</a>'}}</li>
		  % end
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Literatur</b>' if page_id == 'literatur' else '<a href="/datenbank/literatur">Literatur</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li><a href="/">Das Projekt</a></li>
		</ul>
	</nav>
	<nav class="extra">
		<a href="/kontakt">Kontakt</a>&#160;·
		<a href="http://badw.de/data/footer-navigation/impressum.html">Impressum</a>&#160;·
		<a href="http://badw.de/data/footer-navigation/datenschutz.html">Datenschutz</a>
	  % if request.roles:
		<br/><a href="/datenbank/reg">Register</a>
		<br/><a href="/datenbank/ablage">Bildablage</a>
		<br/><strong><a href="/datenbank/auth_end">{{request.user_name}}: {{db.glosses['logout'][lang_id]}}</a></strong>
	  % end
		<p><a class="key" href="#top">Zum Seitenanfang&#x202f;↑</a></p>
	</nav>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
<footer>
	<p>
		<a class="img" href="http://badw.de"><img src="/datenbank/-/icons/badw_marke_name.svg" height="64" alt="BAdW"/></a>
	</p>
</footer>
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>
