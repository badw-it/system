% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying search results.
<main>
<article class="just wide" role="search">
  % if not request.query.text: # Otherwise it is a simple search with the search text in: request.query.text
	<h1>Erweiterte Suche</h1>
	<form method="get">
		<input type="hidden" name="filter" value="form"/>
		<section class="card flextable wide">
			<label class="card">
				<div style="width: 6.5rem">SG bzw. UG:</div>
				<input type="search" name="nr" value="{{request.query.nr}}" autofocus=""/>
			</label>
			<p class="p petit">Hier, wenn erwünscht, Stoff- oder auch Untergruppen angeben, auf die die Suche beschränkt werden soll. Beispiele: <kbd>3.</kbd> (‘nur in Stoffgruppe 3. samt der zugehörigen Einträge’), <kbd>3.1.</kbd> (‘nur in Untergruppe 3.1.’), <kbd>3.1., 3.2.</kbd> (‘nur in den Untergruppen 3.1. und 3.2.’), <kbd>3.1.-3.4.</kbd> (‘nur in den Untergruppen 3.1. bis 3.4.’), <kbd>3.1.-3.3., 19.</kbd> (‘nur in den Untergruppen 3.1. bis 3.3. und in der Stoffgruppe 19. samt der zugehörigen Einträge’). – Der Punkt (<kbd>.</kbd>) ist am Ende von Nummern stets fakultativ.</p>
		</section>
		<section class="card wide">
		  % n = -1
		  % while True:
			% n += 1
			% if n and not request.query.getunicode(f'text_{n}'):
			%     break
			% end
			<section class="card flextable wide">
				<label class="card">
					<div style="width: 6.5rem">Suchtext:</div>
					<input type="search" name="text_{{n}}" value="{{request.query.getunicode(f'text_{n}', '')}}" aria-label="{{db.glosses['search_term'][lang_id]}}"/>
				</label>
			  % if n == 0:
				<p class="p petit">Der Unterstrich (<kbd>_</kbd>) ist Platzhalter für genau ein Zeichen. Das Prozentzeichen (<kbd>%</kbd>) ist Platzhalter für kein, ein oder mehr als ein Zeichen. Ganz am Anfang und ganz am Ende der Sucheingabe sind die Platzhalterzeichen überflüssig. – Der Längsstrich (<kbd>|</kbd>) trennt Suchalternativen: <kbd>rot|grün</kbd> sucht nach “rot” und “grün” gleichermaßen.</p>
				<p class="p petit">Unter den folgenden Seitenbereichen, wenn erwünscht, einen oder mehrere anwählen, auf die die Suche beschränkt werden soll:</p>
			  % end
				<label-group class="card">
					<div style="width: 6.5rem">Stoffgruppen:</div>
					<div>
						<label class="key"><input type="checkbox" name="sg-einleitung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'sg-einleitung_{n}') == '1' else ''}}/> Einleitung</label>
					</div>
				</label-group>
				<label-group class="card">
					<div style="width: 6.5rem">Untergruppen:</div>
					<div>
						<label class="key"><input type="checkbox" name="ug-einleitung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'ug-einleitung_{n}') == '1' else ''}}/> Einleitung</label>
					</div>
				</label-group>
				<label-group class="card">
					<div style="width: 6.5rem">Handschriften:</div>
					<div>
						<label class="key"><input type="checkbox" name="hs-datierung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-datierung_{n}') == '1' else ''}}/> Datierung</label
						><label class="key"><input type="checkbox" name="hs-lokalisierung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-lokalisierung_{n}') == '1' else ''}}/> Lokalisierung</label
						><label class="key"><input type="checkbox" name="hs-besitzgeschichte_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-besitzgeschichte_{n}') == '1' else ''}}/> Besitzgeschichte</label
						><label class="key"><input type="checkbox" name="hs-inhaltsbestandteile_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-inhaltsbestandteile_{n}') == '1' else ''}}/> Inhaltsbestandteile</label
						><label class="key"><input type="checkbox" name="hs-beschreibung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-beschreibung_{n}') == '1' else ''}}/> Kodikologische Beschreibung</label
						><label class="key"><input type="checkbox" name="hs-schreibsprache_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-schreibsprache_{n}') == '1' else ''}}/> Schreibsprache</label
						><label class="key"><input type="checkbox" name="hs-bildausstattung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-bildausstattung_{n}') == '1' else ''}}/> Zusammenfassung Bildausstattung</label
						><label class="key"><input type="checkbox" name="hs-format_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-format_{n}') == '1' else ''}}/> Format und Anordnung</label
						><label class="key"><input type="checkbox" name="hs-bildaufbau_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-bildaufbau_{n}') == '1' else ''}}/> Bildaufbau und ‑ausführung</label
						><label class="key"><input type="checkbox" name="hs-bildthemen_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-bildthemen_{n}') == '1' else ''}}/> Bildthemen</label
						><label class="key"><input type="checkbox" name="hs-farben_{{n}}" value="1"{{' checked' if request.query.getunicode(f'hs-farben_{n}') == '1' else ''}}/> Farben</label>
					</div>
				</label-group>
				<label-group class="card">
					<div style="width: 6.5rem">Drucke:</div>
					<div>
						<label class="key"><input type="checkbox" name="dr-ort_{{n}}" value="1"{{' checked' if request.query.getunicode(f'dr-ort_{n}') == '1' else ''}}/> Ort</label
						><label class="key"><input type="checkbox" name="dr-drucker_{{n}}" value="1"{{' checked' if request.query.getunicode(f'dr-drucker_{n}') == '1' else ''}}/> Drucker</label
						><label class="key"><input type="checkbox" name="dr-jahr_{{n}}" value="1"{{' checked' if request.query.getunicode(f'dr-jahr_{n}') == '1' else ''}}/> Jahr</label
						><label class="key"><input type="checkbox" name="dr-inhaltsbestandteile_{{n}}" value="1"{{' checked' if request.query.getunicode(f'dr-inhaltsbestandteile_{n}') == '1' else ''}}/> Inhaltsbestandteile</label
						><label class="key"><input type="checkbox" name="dr-beschreibung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'dr-beschreibung_{n}') == '1' else ''}}/> Kurzbeschreibung</label
						><label class="key"><input type="checkbox" name="dr-bildausstattung_{{n}}" value="1"{{' checked' if request.query.getunicode(f'dr-bildausstattung_{n}') == '1' else ''}}/> Zusammenfassung Bildausstattung</label>
					</div>
				</label-group>
			</section>
		  % end
			<button type="button" onclick="addBlock(this)">… <b>und</b> im selben Eintrag … (Hier klicken, um einen weiteren Angabenblock hinzuzufügen)</button>
		</section>
		<section id="start" class="card">
			<button type="submit" formaction="filter#start">Suchen</button>
			<a class="key" href="filter">Zurücksetzen</a>
		</section>
	</form>
	<h2>Treffer:</h2>
  % end
	<% kwargs['text'] = '''
	<div class="index">
		<table id="index">
			<thead>
				<tr>
					<th style="width: 6em">Nr. &amp; Link</th>
					<th style="width: 6em" data-select="">Bereich</th>
					<th>Fund</th>
				</tr>
			</thead>
		</table>
	</div>
	'''
	%>
% include('index.tpl', db = db, request = request, kwargs = kwargs)
</article>
</main>