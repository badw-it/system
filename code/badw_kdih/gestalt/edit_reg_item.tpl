% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% item_id = kwargs['item_id']
% item = kwargs.get('item', {})
% art = item.get('reg_art')
<main>
<article>
	<h1>Registereingabe</h1>
  % if item_id:
	<h2>Eingabe für den Registereintrag {{item_id}}</h2>
  % else:
	<h2>Eingabe für einen neuen Registereintrag</h2>
  % end
	<form action="/datenbank/edit/reg/{{item_id}}" method="POST">
		<div class="flextable">
			<div class="card flexrow">
				<div style="width: 17rem">Art:</div>
				<div>
					<label class="key">
						<input id="druck" name="reg_art" type="radio" value="Druck"{{' checked=""' if art == 'Druck' else ''}} required=""/>
						Druck
					</label>
					<label class="key">
						<input id="werk" name="reg_art" type="radio" value="Werk"{{' checked=""' if art == 'Werk' else ''}}/>
						Werk
					</label>
					<label class="key">
						<input id="person" name="reg_art" type="radio" value="Person"{{' checked=""' if art == 'Person' else ''}}/>
						Person
					</label>
					<label class="key">
						<input id="kloster" name="reg_art" type="radio" value="Kloster"{{' checked=""' if art == 'Kloster' else ''}}/>
						Kloster
					</label>
					<label class="key">
						<input id="ikon" name="reg_art" type="radio" value="Ikon."{{' checked=""' if art == 'Ikon.' else ''}}/>
						Ikon.
					</label>
				</div>
			</div>
			<label class="card">
				<div style="width: 17rem">Bezeichnung:</div>
				<input id="bez" name="reg_bezeichnung" onkeyup="fill_button()" size="64" type="text" value="{{item.get('reg_bezeichnung', '')}}" required=""/>
			</label>
			<label class="card">
				<div style="width: 17rem">Anmerkung:</div>
				<input id="nebenbez" name="reg_nebenbezeichnung" size="64" type="text" value="{{item.get('reg_nebenbezeichnung', '')}}"/>
			</label>
			<label class="card">
				<div style="width: 17rem">URL:</div>
				<input id="url" name="reg_url" size="64" type="url" value="{{item.get('reg_url', '')}}"/>
			</label>
			<label class="card">
				<div style="width: 17rem">GND-URL:</div>
				<input id="url_gnd" name="reg_url_gnd" size="64" type="url" value="{{item.get('reg_url_gnd', '')}}"/>
			</label>
			<label class="card">
				<div style="width: 17rem">Für Werke: Untergruppe-Datenbank-ID:</div>
				<input id="ug_id" name="reg_ug_id" size="64" type="text" value="{{item.get('reg_ug_id', '') or ''}}"/>
			</label>
			<strong>oder</strong>
			<label class="card">
				<div style="width: 17rem">Für Werke: Stoffgruppe-Datenbank-ID:</div>
				<input id="sg_id" name="reg_sg_id" size="64" type="text" value="{{item.get('reg_sg_id', '') or ''}}"/>
			</label>
		</div>
		<p><button size="64" type="submit">Abspeichern</button><a class="key" href="0">Weiteren Eintrag anlegen</a><a class="key" href="../../reg">Zurück zur Liste</a></p>
	</form>
	<form action="/datenbank/redact/reg/{{item_id}}" method="POST">
		<input class="flip" id="flip3" type="checkbox"/><label class="key lid" data-checked=" abbrechen" data-unchecked="?!" for="flip3">Löschen</label><button name="redaction" value="delete">Löschen!</button>
	</form>
	<p><strong>Erst abspeichern, dann, wenn erwünscht:</strong> Zur Anzeichnung merken: <a class="key" id="cc" onclick="clipcopy(event)">⚕</a></p>
	<section class="sheet wide">
		<h2>Unbedingt einzuhaltende Muster für das Feld <strong>Bezeichnung</strong></h2>
		<ul>
			<li>Bei Drucken: <kbd>DRUCKORT: DRUCKER, DRUCKDATUM</kbd></li>
			<li>Bei Werken: <kbd>AUTOR, ›TITEL‹</kbd> (wie <kbd>Hugo von Trimberg, ›Der Renner‹</kbd> oder <kbd>Mandeville, Jean de, ›Reisen‹</kbd>) oder nur <kbd>›TITEL‹</kbd></li>
			<li>Bei Personen: <kbd>NAME, VORNAME</kbd> oder <kbd>NAME</kbd> (wie <kbd>Gottfried von Straßburg</kbd>) oder <kbd>NAME, TITEL</kbd> (wie <kbd>Maria, Erzherzogin von Österreich</kbd> oder <kbd>Rappoltstein, Herren von</kbd>).</li>
			<li>Bei Klöstern: <kbd>ORT, KLOSTER</kbd> (z. B. <kbd>Steinfeld, Prämonstratenserkloster</kbd>).</li>
			<li>Bei Ikon.: <kbd>BEZEICHNUNG</kbd> oder <kbd>OBERBEZEICHNUNG|UNTERBEZEICHNUNG</kbd>; die Bezeichnungen alle in Sortierform; wenn nötig, mit Komma invertiert (<kbd>Barbara, hl.</kbd>), sonst kein Komma und keine <kbd>|</kbd> darin verwenden.</li>
		</ul>
		<h2>Regeln für das Feld <strong>Anmerkung</strong></h2>
		<ul>
			<li>Synonymiefall: Bei Hauptbezeichnung <i>Minne</i> mag eine Anmerkung sein: <kbd>s. Liebe</kbd>. <strong>Ein solcher Synonymeneintrag – hier wäre das der Eintrag <kbd>Minne</kbd> – soll nicht verlinkt werden.</strong> Nach einem <kbd>#</kbd> kann eingetragen werden, in welchem Band so ein Synonymeneintrag ins gedruckte Register aufgenommen werden soll; z. B.: <kbd>s. Liebe #8, 9</kbd>.</li>
			<li>Erläuterungsfall: Bei Hauptbezeichnung <i>Fisch</i> mag eine Anmerkung sein: <kbd>s. auch Tierkreiszeichen</kbd>.</li>
		</ul>
		<h2>Regel für die Felder <strong>Untergruppe-Datenbank-ID</strong> und <strong>Stoffgruppe-Datenbank-ID</strong></h2>
		<ul>
			<li>Das ist <strong>nicht</strong> die KdiH-Nummer (wie <kbd>2a.1.</kbd> oder <kbd>3.</kbd>), sondern die Datenbank-ID (wie <kbd>8154</kbd>). Denn eine KdiH-Nummer kann sich – wenn auch sehr selten – ändern.</li>
		</ul>
	</section>
</article>
<script>
function fill_button() {
	var bezeichnung = document.getElementById('bez').value;
	var elem = document.getElementById('cc');
	var text = bezeichnung + '⚕' + {{item_id}}
	elem.innerHTML = text;
};
fill_button();
</script>
</main>