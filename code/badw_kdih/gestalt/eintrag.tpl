% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% kind = kwargs['kind']
% sg_title = item['f_stoffgr_ueberschrift'].replace('<p>', '').replace('</p>', '').strip()
% ug_title = item['f_untergr_ueberschrift'].replace('<p>', '').replace('</p>', '').strip()
% volume = item['f_band_nr'] or ''
<main>
<article class="sheet wide with_kolumnentitel">

<nav class="kolumnentitel">
	<a class="key" href="/datenbank/stoffgruppe/{{item['f_stoffgr_nr']}}">↑ {{item['f_stoffgr_nr']}}.</a>&#8192;{{!sg_title}} <br/>
  % if ug_title:
	<a class="key" href="/datenbank/untergruppe/{{item['f_stoffgr_nr']}}/{{item['f_untergr_nr']}}">↑ {{item['f_stoffgr_nr']}}.{{item['f_untergr_nr']}}.</a>&#8192;{{!ug_title}}
  % end
</nav>

<div class="head_in_item">
<h1>
	<span>{{item['f_stoffgr_nr']}}.{{item['f_untergr_nr']}}.{{item['f_hs_position_nr']}}.</span>
  % if kind == 'handschrift':
	<span>{{!item['f_string']}}</span>
  % else:
	<span><span id="dr-ort">{{!item['druckort']}}</span>: <span id="dr-drucker">{{!item['drucker']}}</span>, <span id="dr-jahr">{{!item['druckjahr']}}</span></span>
  % end
</h1>
<p><span></span><span>{{item['authority']}}</span></p>
% if volume[:1] == '0':
<p class="volume"><span></span><span>Ergänzung zum gedruckten KdiH</span></p>
% else:
<p class="volume"><span></span><span>KdiH-Band {{volume}}</span></p>
% end
</div>

% term = item['f_bestand_bemerkung']
% if term:
<div id="{{'hs' if kind == 'handschrift' else 'dr'}}-bestandsbemerkung">
{{!term}}
</div>
% end

% term = item['f_datum']
% if term:
<div id="hs-datierung" class="inlining">
<strong>Datierung:</strong>
{{!term}}
</div>
% end

% term = item['f_herkunftsort']
% if term:
<div id="hs-lokalisierung" class="inlining">
<strong>Lokalisierung:</strong>
{{!term}}
</div>
% end

% term = item['f_herkunft_erweiterung']
% if term:
<div id="hs-besitzgeschichte" class="inlining">
<strong>Besitzgeschichte:</strong>
{{!term}}
</div>
% end

{{!item['f_fruehere_beschreibung']}}

% term = item['f_inhalt_seiten']
% if term:
<div id="{{'hs-inhaltsbestandteile' if kind == 'handschrift' else 'dr-inhaltsbestandteile'}}" class="contenttable">
<strong>Inhalt:</strong> {{!item['f_inhalt_bmkg'].replace('<p>', '').replace('</p>', '')}}
{{!term}}
</div>
% end

% term = item['f_kodikologie']
% if term:
<div id="{{'hs-beschreibung' if kind == 'handschrift' else 'dr-beschreibung'}}" class="inlining" style="margin-top: 1.5em">
<strong>{{'I. Kodikologische Beschreibung' if kind == 'handschrift' else 'Beschreibung'}}:</strong>
{{!term}}
</div>
% end

% term = item['f_sprache']
% if term:
<div id="hs-schreibsprache" class="inlining">
<strong>Schreibsprache:</strong>
{{!term}}
</div>
% end

% term = item['f_bildausstattung']
% if term:
<div id="{{'hs-bildausstattung' if kind == 'handschrift' else 'dr-bildausstattung'}}" class="inlining" style="margin-top: 1.5em">
<strong>{{'II. Bildausstattung' if kind == 'handschrift' else 'Bildausstattung'}}:</strong>
{{!term}}
</div>
% end

% term = item['f_format']
% if term:
<div id="hs-format" class="inlining_indented">
<strong>{{item['f_format_selector']}}:</strong>
{{!term}}
</div>
% end

% term = item['f_bildaufbau']
% if term:
<div id="hs-bildaufbau" class="inlining_indented">
<strong>{{item['f_bildaufbau_selector']}}:</strong>
{{!term}}
</div>
% end

% term = item['f_bildthemen']
% if term:
<div id="hs-bildthemen" class="inlining_indented">
<strong>{{item['f_bildthemen_selector']}}:</strong>
{{!term}}
</div>
% end

% term = item['f_farben']
% if term:
<div id="hs-farben" class="inlining_indented">
<strong>Farben:</strong>
{{!term}}
</div>
% end

% dterm = item['f_digitalisate']
% fterm = item['f_faksimile']
% lterm = item['f_literatur']

% if dterm:
<div class="inlining petit" style="margin-top: 1.5em">
<strong>Digitalisat:</strong>
{{!dterm}}
</div>
% end

% if fterm:
<div class="inlining petit"{{!'' if dterm else ' style="margin-top: 1.5em"'}}>
<strong>Faksimile:</strong>
{{!fterm}}
</div>
% end

% if lterm:
<div class="inlining petit"{{!'' if (dterm or fterm) else ' style="margin-top: 1.5em"'}}>
<strong>Literatur:</strong>
{{!lterm}}
</div>
% end

% term = item['f_anmerkungen']
% if term:
<div class="inlining petit">
<strong>Anmerkungen:</strong>
{{!term}}
</div>
% end

% term = item['f_externe_daten']
% if term:
<div class="inlining petit">
<strong>Weitere Materialien im Internet:</strong>
{{!term}}
</div>
% end

% tterm = item['f_tafeln']
% aterm = item['f_abbildungen']
% if tterm:
<div class="petit" style="margin-top: 1.5em">{{!tterm}}</div>
% end

% if aterm:
<div class="petit"{{!'' if tterm else ' style="margin-top: 1.5em"'}}>{{!aterm}}</div>
% end

% for name in item['abb']:
<figure>
	<img alt="{{name}}" class="shadow" src="/datenbank/repro/{{name}}"/>
	<figcaption>{{name.split('_', 1)[-1].rsplit('.', 1)[0].replace('_', ' ').strip()}}.</figcaption>
</figure>
% end

<aside class="citebox">
	<p>Empfohlene Zitierweise</p>
  % if item['f_stoffgr_nr'] == '43':
	<p>(Siehe die <a href="/datenbank/stoffgruppe/43">Einleitung zur Stoffgruppe</a>.)</p>
  % else:
	<p>{{item['authors']}}: {{!sg_title}}. {{!ug_title + '. ' if ug_title else ''}}{{'Druck' if kind == 'druck' else 'Handschrift'}} Nr. {{item['f_stoffgr_nr']}}.{{item['f_untergr_nr']}}.{{item['f_hs_position_nr']}}. In: {{item['title']}} \\
	% if volume[:1] == '0':
		{{'Ergänzung zum gedruckten KdiH'}}. \\
	% else:
		Band {{volume}}. {{item['place']}} {{item['year']}}. \\
	% end
	http://kdih.badw.de/datenbank/{{kind}}/{{item['f_stoffgr_nr']}}/{{item['f_untergr_nr']}}/{{item['f_hs_position_nr']}}; zuletzt geändert am {{item['modified_on'].strftime('%d.%m.%Y')}}.</p>
  % end
</aside>

</article>
</main>