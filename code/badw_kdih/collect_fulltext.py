# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
import sys
import urllib.parse
import xml.etree.ElementTree as ET
from functools import partial
from html import unescape

import __init__
import fs
import sql_io
import web_io
import xmlhtml

def insert(
        url: str,
        text: str,
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.Cursor',
        ft_table: str = '',
        tag_re: 're.Pattern[str]' = re.compile(r'<[^>]*>'),
        space_re: 're.Pattern[str]' = re.compile(r'\s+'),
        reg_tag_re: 're.Pattern[str]' = re.compile(
            r'(?s)<a class="reg"[^>]*?href="[^"]*?/reg/(\d+)"[^>]*?>(.*?)</a>'),
        non_mark_re: 're.Pattern[str]' = re.compile(r'<(?!/?mark)[^>]*>'),
        after_mark_re: 're.Pattern[str]' = re.compile(r'(</mark>)([^<]*)(<|$)'),
        ) -> None:
    '''
    Extract rows from data given as :param:`url` (which is the URL of a webpage)
    and :param:`text` (which is the HTML of said webpage). Insert said rows into
    a database with the cursor :param:`cur` of the connection :param:`con`.

    (For more information, look at the calling :func:`web_io.fulltext`.)
    '''
    def inter_sub(m: 're.Match[str]') -> str:
        prae, inter, post = m.group(1, 2, 3)
        if post:
            if len(inter) > 200:
                inter = f'{inter[:100]}⟦…⟧{inter[-100:]}'
        else:
            if len(inter) > 100:
                inter = inter[:100] + '⟦…⟧'
        return f'{prae}{inter}{post}'
    def reg_sub(m: 're.Match[str]') -> str:
        reg_id = m.group(1)
        term = m.group(2)
        reg_ids.add(reg_id)
        return f'<mark data-id="{reg_id}">{term}</mark>'
    scheme, netloc, path, *_ = urllib.parse.urlsplit(url)
    area = path.split('/datenbank/', 1)[-1].split('/', 1)[-1].replace('/', '.')
    if area.count('.') > 1:
        area = area.rsplit('.', 1)[0]
    area += '.'
    text = xmlhtml.replace(text, replace_element = replace_element)
    # We will need attributes for the extraction of parts.
    raw_text = unescape(space_re.sub(' ', tag_re.sub('', text)).strip())
    cur.execute(rf"""
            insert into {ft_table} values (1, %s, %s, %s, %s)
            on duplicate key update
            ft_new = 1, ft_text = %s, ft_area = %s
            """,
            (raw_text, path, '', area, raw_text, area))
    con.commit()
    reg_ids = set()
    reg_text = non_mark_re.sub('', reg_tag_re.sub(reg_sub, text))
    for reg_id in reg_ids:
        id_reg_text = re.sub(
                rf'<mark data-id="(?!{reg_id}")[^"]*">(.*?)</mark>',
                r'\g<1>', reg_text)
        id_reg_text = after_mark_re.sub(inter_sub, id_reg_text)
        start = max(0, id_reg_text.find('<') - 100)
        id_reg_text = id_reg_text[start:]
        if start > 0:
            id_reg_text = '⟦…⟧' + id_reg_text
        cur.execute(rf"""
                insert into {ft_table} values (1, %s, %s, %s, %s)
                on duplicate key update
                ft_new = 1, ft_text = %s, ft_area = %s
                """,
                (id_reg_text, path, reg_id, 'reg', id_reg_text, 'reg'))
        con.commit()
    try:
        root = ET.XML(text)
    except ET.ParseError:
        pass
    else:
        for elem in root.iterfind('.//*[@id]'):
            if elem.tag == 'body':
                continue
            part = elem.attrib['id']
            raw_text = unescape(
                    space_re.sub(
                        ' ',
                        tag_re.sub(
                            '', xmlhtml.serialize_elem(elem, 'text'))).strip())
            cur.execute(rf"""
                    insert into {ft_table} values (1, %s, %s, %s, %s)
                    on duplicate key update
                    ft_new = 1, ft_text = %s, ft_area = %s
                    """,
                    (raw_text, path, part, area, raw_text, area))
        con.commit()

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    """
    Process :param:`tag` and :param:`attrs`.
    A return value of ``None`` does not change the element.
    If a string is returned, the element is replaced by it
    without escaping; if the empty string is returned, the
    element is deleted.
    If the element is void, :param:`startend` is ``True``.
    """
    if (
            (tag in {
                'aside',
                'details',
                'footer',
                'head',
                'header',
                'nav',
                'script',
                'style',
                })
            or ('data-not-for-ft' in attrs)
            ):
        return ''
    elif tag == 'br':
        return ' '
    return None

if __name__ == '__main__':
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    if len(sys.argv) < 5:
        auth_num = None
        ft_table = 'ft'
        urdata = sys.argv[2:]
    else:
        auth_num = sys.argv[2]
        ft_table = 'ft_all'
        urdata = sys.argv[3:]
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    headers = dict(config['bot_headers'])
    if auth_num is not None:
        headers['auth-num'] = auth_num # For HTTP, it *must* be a hyphen here.
    web_io.fulltext(
            int(sys.argv[1]),
            sql_io.get_connect(*fs.get_keys(paths['db_access']), config),
            (
                rf"""
                    create table if not exists {ft_table}
                    (
                    ft_new TINYINT UNSIGNED NOT NULL DEFAULT 1, -- Whether the record is newly inserted.
                    ft_text LONGTEXT NOT NULL COLLATE utf8mb4_german2_ci, -- Content.
                    ft_path VARCHAR(8000) NOT NULL COLLATE utf8mb4_bin, -- URL of a page without domain, but with a leading slash.
                    ft_part VARCHAR(190) NOT NULL COLLATE utf8mb4_bin, -- Fragment ID of the part of the page, with leading hashtag. Empty, if it is the whole page.
                    ft_area VARCHAR(190) NOT NULL COLLATE utf8mb4_bin, -- Area to which the page belongs within the website, inferred e.g. from the path.
                    INDEX (ft_part),
                    INDEX (ft_area),
                    UNIQUE (ft_path, ft_part)
                    )
                """,
            ),
            config['ids']['start_url'],
            partial(insert, ft_table = ft_table),
            headers = headers,
            urls_considered_for_their_text_re = re.compile(
                config['ids']['urls_considered_for_their_text_re']),
            urls_considered_for_further_urls_re = re.compile(
                config['ids']['urls_considered_for_further_urls_re']),
            default_path = '/' + config['ids']['page'],
            pause_between_pages_in_secs = 0.5,
            after_loop_sqls = (
                rf"delete from {ft_table} where ft_new = 0",
                rf"update {ft_table} set ft_new = 0",
                ),
            )
