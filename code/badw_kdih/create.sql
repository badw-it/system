-- Licensed under http://www.apache.org/licenses/LICENSE-2.0
-- Attribution notice: by Tim Price, Oliver von Criegern, Stefan Müller in 2013 ff. (© http://badw.de)

-- create database kdih character set utf8mb4;
-- grant all privileges on kdih.* to 'test'@'localhost' identified by 'test';

USE kdih;

SET GLOBAL innodb_file_format = "Barracuda";
SET GLOBAL innodb_file_format_max = "Barracuda";
SET GLOBAL innodb_file_per_table = "ON";
SET GLOBAL innodb_strict_mode = "ON";
-- Note: Additionally, in the config file (e.g. /etc/my.cnf) you might increase:
-- innodb_log_file_size=256M

START TRANSACTION;

SET FOREIGN_KEY_CHECKS=0;

drop table if exists t_untergruppen_archive;
create table if not exists t_untergruppen_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	f_band_nr VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_nr VARCHAR ( 512 ) NOT NULL,
	f_untergr_nr VARCHAR ( 512 ) NOT NULL,
	f_untergr_ueberschrift VARCHAR ( 512 ) DEFAULT '',
	f_untergr_beschreibung LONGTEXT,
	f_untergr_edition LONGTEXT,
	f_untergr_lit LONGTEXT,
	f_untergr_siehe LONGTEXT,
	f_untergr_notiz LONGTEXT,
	f_ug_index VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_1 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_2 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_3 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_4 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_5 VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by VARCHAR ( 512 ) DEFAULT '',
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (current_record) REFERENCES t_untergruppen(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_untergruppen;
create table if not exists t_untergruppen (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_band_nr VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_nr VARCHAR ( 512 ) NOT NULL,
	f_untergr_nr VARCHAR ( 512 ) NOT NULL,
	f_untergr_ueberschrift LONGTEXT,
	f_untergr_beschreibung LONGTEXT,
	f_untergr_edition LONGTEXT,
	f_untergr_lit LONGTEXT,
	f_untergr_siehe LONGTEXT,
	f_untergr_notiz LONGTEXT,
	f_ug_index VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_1 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_2 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_3 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_4 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_5 VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by VARCHAR ( 512 ) DEFAULT '',
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_stoffgruppen_archive;
create table if not exists t_stoffgruppen_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	f_band_nr VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_nr VARCHAR ( 512 ) NOT NULL,
	f_stoffgr_ueberschrift VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_beschreibung LONGTEXT,
	f_stoffgr_edition LONGTEXT,
	f_stoffgr_lit LONGTEXT,
	f_stoffgr_siehe LONGTEXT,
	f_stoffgr_notiz LONGTEXT,
	f_zustaendig_1 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_2 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_3 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_4 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_5 VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by INTEGER,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by INTEGER,
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (current_record) REFERENCES t_stoffgruppen(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_stoffgruppen;
create table if not exists t_stoffgruppen (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_band_nr VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_nr VARCHAR ( 191 ) NOT NULL,
	f_stoffgr_ueberschrift LONGTEXT,
	f_stoffgr_beschreibung LONGTEXT,
	f_stoffgr_edition LONGTEXT,
	f_stoffgr_lit LONGTEXT,
	f_stoffgr_siehe LONGTEXT,
	f_stoffgr_notiz LONGTEXT,
	f_zustaendig_1 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_2 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_3 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_4 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_5 VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by INTEGER,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by INTEGER,
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (created_by) REFERENCES auth_user(id) ON DELETE RESTRICT,
	FOREIGN KEY (modified_by) REFERENCES auth_user(id) ON DELETE RESTRICT
	)
	ROW_FORMAT = COMPRESSED
	;
create index i_f_stoffgr_nr_stoff on t_stoffgruppen (f_stoffgr_nr);
drop table if exists t_standorte;
create table if not exists t_standorte (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_name VARCHAR ( 512 )
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_signaturen;
create table if not exists t_signaturen (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_institution_id INTEGER,
	f_objekt_id INTEGER,
	f_signatur VARCHAR ( 512 ) DEFAULT '',
	f_status VARCHAR ( 512 ) DEFAULT '',
	f_string LONGTEXT,
	FOREIGN KEY (f_institution_id) REFERENCES t_institutionen(id) ON DELETE CASCADE,
	FOREIGN KEY (f_objekt_id) REFERENCES t_objekte(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_personen_archive;
create table if not exists t_personen_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	f_name VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by VARCHAR ( 512 ) DEFAULT '',
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (current_record) REFERENCES t_personen(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_personen;
create table if not exists t_personen (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_name VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by VARCHAR ( 512 ) DEFAULT '',
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_objekte;
create table if not exists t_objekte (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_signatur_id INTEGER,
	f_handschrift_id INTEGER,
	f_teil_nr INTEGER,
	f_anmerkung LONGTEXT,
	f_string LONGTEXT,
	FOREIGN KEY (f_handschrift_id) REFERENCES t_handschriften(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_lit_archive;
create table if not exists t_lit_archive (
	current_record INTEGER,
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_band VARCHAR ( 512 ) DEFAULT '',
	f_kurztitel LONGTEXT,
	f_volltitel LONGTEXT,
	f_stoffgruppe VARCHAR ( 512 ) DEFAULT '',
	f_standardwerk VARCHAR ( 1 ),
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by INTEGER,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by INTEGER,
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (current_record) REFERENCES t_lit(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_lit;
create table if not exists t_lit (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_band VARCHAR ( 512 ) DEFAULT '',
	f_kurztitel LONGTEXT,
	f_volltitel LONGTEXT,
	f_stoffgruppe VARCHAR ( 512 ) DEFAULT '',
	f_standardwerk VARCHAR ( 1 ),
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by INTEGER,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by INTEGER,
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (modified_by) REFERENCES auth_user(id) ON DELETE RESTRICT,
	FOREIGN KEY (created_by) REFERENCES auth_user(id) ON DELETE RESTRICT
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_kommentare_archive;
create table if not exists t_kommentare_archive (
	current_record INTEGER,
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_eintrag_id LONGTEXT,
	f_nachricht LONGTEXT,
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by INTEGER,
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (current_record) REFERENCES t_kommentare(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_kommentare;
create table if not exists t_kommentare (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_eintrag_id INTEGER,
	f_nachricht LONGTEXT,
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by INTEGER,
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (f_eintrag_id) REFERENCES t_eintraege(id) ON DELETE CASCADE,
	FOREIGN KEY (modified_by) REFERENCES auth_user(id) ON DELETE RESTRICT
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_institutionen;
create table if not exists t_institutionen (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_standort_id INTEGER,
	f_name VARCHAR ( 512 ) DEFAULT '',
	f_string LONGTEXT,
	FOREIGN KEY (f_standort_id) REFERENCES t_standorte(id) ON DELETE RESTRICT
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_handschriften;
create table if not exists t_handschriften (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_bestand_typ VARCHAR ( 512 ) DEFAULT '',
	f_druck_ort VARCHAR ( 512 ) DEFAULT '',
	f_druck_ort_ergaenzt VARCHAR ( 1 ),
	f_druck_datum VARCHAR ( 512 ) DEFAULT '',
	f_druck_datum_ergaenzt VARCHAR ( 1 ),
	f_drucker VARCHAR ( 512 ) DEFAULT '',
	f_drucker_ergaenzt VARCHAR ( 1 ),
	f_verleger VARCHAR ( 512 ) DEFAULT '',
	f_verleger_ergaenzt VARCHAR ( 1 ),
	f_string LONGTEXT
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_eintraege_archive;
create table if not exists t_eintraege_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	f_band_nr VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_nr VARCHAR ( 512 ) DEFAULT '',
	f_untergr_nr VARCHAR ( 512 ) DEFAULT '',
	f_hs_position_nr VARCHAR ( 512 ) DEFAULT '',
	f_bestand_typ VARCHAR ( 512 ) DEFAULT '',
	f_signatur LONGTEXT,
	f_handschrift_id INTEGER,
	f_arbeitsgrundlage LONGTEXT,
	f_bestand_bemerkung LONGTEXT,
	f_datum LONGTEXT,
	f_herkunftsort LONGTEXT,
	f_herkunft_erweiterung LONGTEXT,
	f_inhalt_bmkg LONGTEXT,
	f_inhalt_seiten LONGTEXT,
	f_kodikologie LONGTEXT,
	f_sprache LONGTEXT,
	f_bildausstattung LONGTEXT,
	f_format LONGTEXT,
	f_format_selector LONGTEXT,
	f_bildaufbau LONGTEXT,
	f_bildaufbau_selector LONGTEXT,
	f_bildthemen LONGTEXT,
	f_bildthemen_selector LONGTEXT,
	f_farben LONGTEXT,
	f_digitalisate LONGTEXT,
	f_faksimile LONGTEXT,
	f_literatur LONGTEXT,
	f_allgemein VARCHAR ( 512 ) DEFAULT '',
	f_abbildungen LONGTEXT,
	f_tafeln VARCHAR ( 512 ) DEFAULT '',
	f_externe_daten LONGTEXT,
	f_anmerkungen VARCHAR ( 512 ) DEFAULT '',
	f_hs_index VARCHAR ( 512 ) DEFAULT '',
	f_string LONGTEXT,
	f_zustaendig_1 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_2 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_3 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_4 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_5 VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by VARCHAR ( 512 ) DEFAULT '',
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (current_record) REFERENCES t_eintraege(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_eintraege;
create table if not exists t_eintraege (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_band_nr VARCHAR ( 512 ) DEFAULT '',
	f_stoffgr_nr VARCHAR ( 128 ) DEFAULT '',
	f_untergr_nr VARCHAR ( 512 ) DEFAULT '',
	f_hs_position_nr VARCHAR ( 512 ) DEFAULT '',
	f_bestand_typ VARCHAR ( 512 ) DEFAULT '',
	f_signatur LONGTEXT,
	f_handschrift_id INTEGER,
	f_arbeitsgrundlage LONGTEXT,
	f_bestand_bemerkung LONGTEXT,
	f_datum LONGTEXT,
	f_herkunftsort LONGTEXT,
	f_fruehere_beschreibung LONGTEXT NOT NULL;
	f_herkunft_erweiterung LONGTEXT,
	f_inhalt_bmkg LONGTEXT,
	f_inhalt_seiten LONGTEXT,
	f_kodikologie LONGTEXT,
	f_sprache LONGTEXT,
	f_bildausstattung LONGTEXT,
	f_format LONGTEXT,
	f_format_selector VARCHAR ( 512 ) DEFAULT '',
	f_bildaufbau LONGTEXT,
	f_bildaufbau_selector VARCHAR ( 512 ) DEFAULT '',
	f_bildthemen LONGTEXT,
	f_bildthemen_selector VARCHAR ( 512 ) DEFAULT '',
	f_farben LONGTEXT,
	f_digitalisate LONGTEXT,
	f_faksimile LONGTEXT,
	f_literatur LONGTEXT,
	f_allgemein LONGTEXT,
	f_abbildungen LONGTEXT,
	f_tafeln LONGTEXT,
	f_externe_daten LONGTEXT,
	f_anmerkungen LONGTEXT,
	f_hs_index VARCHAR ( 512 ) DEFAULT '',
	f_string LONGTEXT,
	f_zustaendig_1 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_2 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_3 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_4 VARCHAR ( 512 ) DEFAULT '',
	f_zustaendig_5 VARCHAR ( 512 ) DEFAULT '',
	is_active VARCHAR ( 1 ) DEFAULT 'T',
	created_by VARCHAR ( 512 ) DEFAULT '',
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	modified_by VARCHAR ( 512 ) DEFAULT '',
	modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (f_handschrift_id) REFERENCES t_handschriften(id) ON DELETE RESTRICT
	)
	ROW_FORMAT = COMPRESSED
	;
create index i_f_stoffgr_nr on t_eintraege (f_stoffgr_nr);
drop table if exists auth_user_archive;
create table if not exists auth_user_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	first_name VARCHAR ( 128 ),
	last_name VARCHAR ( 128 ),
	email VARCHAR ( 512 ) DEFAULT '',
	password VARCHAR ( 512 ) DEFAULT '',
	registration_key VARCHAR ( 512 ) DEFAULT '',
	reset_password_key VARCHAR ( 512 ) DEFAULT '',
	registration_id VARCHAR ( 512 ) DEFAULT '',
	FOREIGN KEY (current_record) REFERENCES auth_user(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_user;
create table if not exists auth_user (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	first_name VARCHAR ( 128 ),
	last_name VARCHAR ( 128 ),
	email VARCHAR ( 512 ) DEFAULT '',
	password VARCHAR ( 512 ) DEFAULT '',
	registration_key VARCHAR ( 512 ) DEFAULT '',
	reset_password_key VARCHAR ( 512 ) DEFAULT '',
	registration_id VARCHAR ( 512 )
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_permission;
create table if not exists auth_permission (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	group_id INTEGER,
	name VARCHAR ( 191 ) DEFAULT '',
	table_name VARCHAR ( 191 ) DEFAULT '',
	record_id INTEGER,
	FOREIGN KEY (group_id) REFERENCES auth_group(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
create index group_id on auth_permission (group_id);
create index table_name on auth_permission (table_name);
create index record_id on auth_permission (record_id);
drop table if exists auth_membership_archive;
create table if not exists auth_membership_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	user_id INTEGER,
	group_id INTEGER,
	FOREIGN KEY (current_record) REFERENCES auth_membership(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_membership;
create table if not exists auth_membership (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user_id INTEGER,
	group_id INTEGER,
	FOREIGN KEY (user_id) REFERENCES auth_user(id) ON DELETE CASCADE,
	FOREIGN KEY (group_id) REFERENCES auth_group(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_group_archive;
create table if not exists auth_group_archive (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	current_record INTEGER,
	role VARCHAR ( 512 ) DEFAULT '',
	description LONGTEXT,
	FOREIGN KEY (current_record) REFERENCES auth_group(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_group;
create table if not exists auth_group (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	role VARCHAR ( 512 ) DEFAULT '',
	description LONGTEXT
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_event;
create table if not exists auth_event (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	time_stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	client_ip VARCHAR ( 512 ) DEFAULT '',
	user_id INTEGER,
	origin VARCHAR ( 512 ) DEFAULT '',
	description LONGTEXT,
	FOREIGN KEY (user_id) REFERENCES auth_user(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists auth_cas;
create table if not exists auth_cas (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user_id INTEGER,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	service VARCHAR ( 512 ) DEFAULT '',
	ticket VARCHAR ( 512 ) DEFAULT '',
	renew VARCHAR ( 1 ),
	FOREIGN KEY (user_id) REFERENCES auth_user(id) ON DELETE CASCADE
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists t_baende;
create table if not exists t_baende (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	f_band_nr VARCHAR ( 512 ) NOT NULL DEFAULT '',
	f_titel VARCHAR ( 8192 ) NOT NULL DEFAULT '',
	f_ort VARCHAR ( 2048 ) NOT NULL DEFAULT '',
	f_jahr VARCHAR ( 2048 ) NOT NULL DEFAULT ''
	)
	ROW_FORMAT = COMPRESSED
	;
drop table if exists reg;
create table if not exists reg (
	reg_id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
	reg_von INT NOT NULL DEFAULT 0,
	reg_art VARCHAR(768) NOT NULL DEFAULT '' CHECK(reg_art in
		(''
		, 'Druck'
		, 'Hs.'
		, 'Ikon.'
		, 'Kloster'
		, 'Person'
		, 'Werk'
		)),
	reg_bezeichnung VARCHAR(768) NOT NULL DEFAULT '', -- Bei Drucken, Hss., Werken: aus IDs erzeugen, wenn angegeben.
	reg_nebenbezeichnung VARCHAR(768) NOT NULL DEFAULT '', -- Nur für Ikon.
	reg_url VARCHAR(768) NOT NULL DEFAULT '', -- Bei Hss.: der Handschriftencensus.
	reg_url_gnd VARCHAR(768) NOT NULL DEFAULT '',
	reg_ei_id INTEGER, -- Für Drucke und für Hss. (Wenn für eine Hs., heißt das, dass dieser Eintrag als Ganzes jener Hs. gewidmet ist.)
    reg_hs_id INTEGER, -- Für Hss.
	reg_ug_id INTEGER, -- Für Werke.
	reg_sg_id INTEGER, -- Für Werke.
	FOREIGN KEY (reg_ei_id) REFERENCES t_eintraege(id) ON DELETE RESTRICT,
	FOREIGN KEY (reg_hs_id) REFERENCES t_handschriften(id) ON DELETE RESTRICT,
	FOREIGN KEY (reg_ug_id) REFERENCES t_untergruppen(id) ON DELETE RESTRICT,
	FOREIGN KEY (reg_sg_id) REFERENCES t_stoffgruppen(id) ON DELETE RESTRICT,
	UNIQUE KEY (reg_ei_id),
	UNIQUE KEY (reg_hs_id),
	UNIQUE KEY (reg_ug_id),
	UNIQUE KEY (reg_sg_id),
	PRIMARY KEY (reg_id)
	)
	ROW_FORMAT = COMPRESSED
	;

SET FOREIGN_KEY_CHECKS=1;

COMMIT;
