# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the KdiH project of the Bavarian Academy of Sciences.
'''
import os
try: import regex as re
except ImportError: import re
import secrets
import subprocess
import sys
import unicodedata
from binascii import hexlify
from collections import defaultdict, deque
from functools import partial
from hashlib import pbkdf2_hmac # for Web2py passwords
from urllib.parse import unquote

try:
    import pymysql as mysql
except ImportError:
    pass

import __init__
import bottle
import fs
import parse
import sql_io
import web_io
import xmlhtml
from structs import empty

class DB(web_io.IO):

    year_re: 're.Pattern[str]' = re.compile(
            r'(?P<place_printer>^[^:]*:[^,]*,).*?(?P<year>\d\d\d\d).*')

    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.
        '''
        super().__init__(urdata, __file__)

        self.repros_path = self.paths['repros'] + os.sep
        self.connect = sql_io.get_connect(
                *fs.get_keys(self.paths['db_access']),
                self.config,
                )
        self.auth_num = secrets.randbelow(1_000_000_000_000_000_000_000)

        if self.config['modes'].getboolean('refreshing'):
            self.process_fulltext = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../collect_fulltext.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_fulltext_all = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../collect_fulltext.py'),
                    str(os.getpid()),
                    str(self.auth_num),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_db = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../process_db.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])

    def auth(self, request) -> 'tuple[None|int, None|str, set[str]]':
        '''
        For the web request :param:`request` and the belonging session, get
        the stored ID, name and the set of roles of the user. If no session
        is registered or no user ID is known, return ``None``, ``None`` and
        the empty set instead.

        This is used to decide if a request comes from a user who is logged
        in and has the required roles.
        '''
        try:
            auth_num = request.environ.get('HTTP_AUTH_NUM', '')
            if auth_num and int(auth_num) == self.auth_num:
                return 0, '', {'inspector'}
            user_id = request.environ['beaker.session']['user_id']
            assert user_id not in (None, '')
            with sql_io.DBCon(self.connect()) as (con, cur):
                cur.execute(r'''
                        select
                            auth_user.email as name,
                            auth_group.role as role
                        from auth_user
                        join auth_membership on
                            auth_membership.user_id = auth_user.id
                        join auth_group on
                            auth_group.id = auth_membership.group_id
                        where auth_user.id = %s
                        ''', (user_id,))
                results = cur.fetchall()
            name = results[0]['name']
            return user_id, name, { result['role'] for result in results }
        except: # Sic.
            return None, None, set()

    def auth_in(self, user_name: str, password: str) -> 'None|int':
        '''
        For the combination of :param:`user_name` and :param:`password`, if
        this combination can be verified, return the belonging user ID; but
        if the combination is not known, return ``None``.

        This is used to perform the login of a user.
        '''
        try:
            user_name = user_name.strip()
            # The user_name is the email; if it is a number, it is invalid.
            assert '@' in user_name
            with sql_io.DBCon(self.connect()) as (con, cur):
                cur.execute(r'''
                        select id, password from auth_user where email = %s
                        ''', (user_name,))
                results = cur.fetchall()
            assert len(results) == 1
            user_id = results[0]['id']
            _, salt, password_hash = results[0]['password'].split('$')
            salt = salt.encode()
            password_hash = password_hash.encode()
            password = password.encode()
            assert password_hash == hexlify(pbkdf2_hmac(
                    'sha512', password, salt, iterations = 1000, dklen = 20
                    ))
            return user_id
        except Exception as e: # Sic.
            return None

    def delete(self, table: str, item_id: int) -> None:
        '''
        Delete the item :param:`item_id` of the table :param:`table`.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            table = table.replace('\\', '').replace('`', '').replace("'", '')
            if table == 'reg':
                cur.execute(r"""
                        update reg set reg_von = -1 where reg_id = %s
                        """, (item_id,))
                con.commit()

    def filter(
            self,
            request: 'bottle.LocalRequest',
            nr_norm1_re: 're.Pattern[str]' = re.compile(r'(^\D+|[^-.,\dABab])'),
            nr_norm2_re: 're.Pattern[str]' = re.compile(r'[-.,](?!\d)'),
            nr_norm3_re: 're.Pattern[str]' = re.compile(
                r'(\d[ABab]?\.\d+[ABab]?)\.\d*[A-Za-z]?'),
            text_norm_re: 're.Pattern[str]' = re.compile(r'\|[\s_%]*\|+'),
            ) -> 'Generator':

        def get_data_row(
                row: 'dict[str, Any]',
                term_re_str: str,
                term_re: 're.Pattern[str]',
                ) -> 'dict[str, tuple[dict[str, str], str, str]]':
            path = row['ft_path']
            part = row['ft_part']
            term_re_str = self.q(term_re_str)
            _, _, area, *nums = path.split('/')
            num = 'Lit. ' + nums[0] if area == 'literatur'\
                    else '.'.join(nums) + '.'
            if part:
                part = '#' + part
            return { str(rank): item for rank, item in enumerate((
                    {
                        '_': f'<a class="dark key" href="{path}?mark={term_re_str}{part}">{num}</a>',
                        's': parse.sortnumtext(num),
                    },
                    area.title(),
                    ''.join(web_io.get_filtersplits(row['ft_text'], term_re)),
                    )) }

        table = 'ft_all' if request.roles else 'ft'
        with sql_io.DBCon(self.connect()) as (con, cur):
            text = request.query.text
            if request.query.getunicode('filter') == 'form':
                sql = f"* from {table} where ft_area <> 'reg'"
                nr = nr_norm2_re.sub(
                        '', nr_norm1_re.sub(
                            '', request.query.getunicode('nr', '')))
                nrs = []
                if nr:
                    nr = nr_norm3_re.sub(r'\g<1>', nr) # Extend search to ug.
                    if '-' in nr:
                        areas = sorted((
                                row['ft_area'] for row in con.geteach(rf"""
                                    distinct ft_area from {table}
                                    where ft_area <> 'reg'
                                    """)
                                ), key = parse.sortnumtext)
                    for nr in nr.split(','):
                        if '-' in nr:
                            first, last = nr.split('-', 1)
                            first += '.'
                            last += '.'
                            try:
                                start = areas.index(first)
                                end = areas.index(last, start)
                                for area in areas[start:end + 1]:
                                    nrs.append(area)
                            except (IndexError, ValueError):
                                pass
                        else:
                            nrs.append(nr + '.')
                if nrs:
                    nr = "%' or ft_area like '".join(nrs)
                    sql += f"\nand (ft_area like '{nr}%')"
                text_parts = defaultdict(dict)
                for key in request.query:
                    val = request.query.getunicode(key, '').strip()
                    if val and '_' in key:
                        name, num = key.split('_', 1)
                        if (name == 'text') or (
                                val == '1' and
                                key.split('-', 1)[0] in {'dr', 'hs', 'sg', 'ug'}
                                ):
                            text_parts[num][name] = val
                results = defaultdict(list)
                # maps the webpage paths to the result rows so that we can check
                # for the condition that one result set has to be drawn from one
                # and the same webpage.
                for text_part in text_parts.values():
                    if 'text' in text_part:
                        term = text_norm_re.sub('|', text_part.pop('text'))
                        term, term_re_str, term_re = web_io.get_filterterms(
                                term,
                                adapt_regex = lambda term:
                                    "(?is)({})".format(term.replace('\\|', '|'))
                                )
                        if term:
                            term = "%' or ft_text like '%".join(
                                    map(con.escape_string, term.split('|')))
                            term = f"\nand (ft_text like '%{term}%')"
                            # The following works for :sql:`ft_part = ''`, too.
                            part = "' or ft_part = '".join(
                                    map(con.escape_string, text_part.keys()))
                            part = f"\nand (ft_part = '{part}')"
                            rows = con.geteach(sql + term + part)
                            if results: # It is not the first subresult.
                                results_new = defaultdict(list)
                                for row in rows:
                                    path = row['ft_path']
                                    if path in results:
                                        results_new[path].extend(results[path])
                                        results_new[path].append(
                                                (row, term_re_str, term_re))
                                results = results_new
                            else:
                                for row in rows:
                                    results[row['ft_path']].append(
                                            (row, term_re_str, term_re))
                            if not results:
                                # If ``results`` is or gets empty after at least
                                # the first subresult was seen, then there is at
                                # least one subresult empty. And that means that
                                # the AND condition of the whole query is false.
                                break
                for result in results.values():
                    for row, term_re_str, term_re in result:
                        yield get_data_row(row, term_re_str, term_re)
            elif text:
                term, term_re_str, term_re = web_io.get_filterterms(text)
                if not term:
                    return
                for row in con.geteach(rf"""
                        * from {table}
                        where ft_text like %s COLLATE utf8mb4_german2_ci
                        and ft_part = ''
                        """, '%' + term + '%'):
                    yield get_data_row(row, term_re_str, term_re)
            else:
                try:
                    reg_id = int(request.query.reg_id)
                except: # Sic.
                    return
                css = self.q(
                        f'a[href$="/reg/{reg_id}"] {{background: #ffffa0}}'
                        )
                for row in con.geteach(rf"""
                        *
                        from {table}
                        where ft_part = %s
                        and ft_area = 'reg'
                        """, reg_id):
                    path = row['ft_path']
                    _, _, area, *nums = path.split('/')
                    num = '.'.join(nums) + '.'
                    yield { str(rank): item for rank, item in enumerate((
                            {
                            '_': f'<a class="dark key" '\
                                f'href="{path}?css={css}">'\
                                f'{num}</a>',
                            's': parse.sortnumtext(num),
                            },
                            area.title(),
                            row['ft_text'], # No escaping: is tagged text.
                            )) }

    def get_hsc_urls(
            self,
            link_re: 're.Pattern[str]' = re.compile(
                r'''\shref\s*=\s*['"](https?://(www\.)?handschriftencensus\.de/.*?)['"]'''),
            ) -> 'Generator[tuple[str, str]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            for item in con.geteach("""
                    f_stoffgr_nr,
                    f_untergr_nr,
                    f_hs_position_nr,
                    f_bestand_typ,
                    f_externe_daten
                    from t_eintraege
                    join auth_permission
                        on auth_permission.table_name = 't_eintraege'
                        and auth_permission.record_id = t_eintraege.id
                        and auth_permission.group_id = 41
                    where f_externe_daten like '%handschriftencensus%'
                    """):
                kind = item['f_bestand_typ'].lower() or 'handschrift'
                stoff = item['f_stoffgr_nr']
                unter = item['f_untergr_nr']
                ein = item['f_hs_position_nr']
                url = f"{self.config['ids']['url']}/{kind}/{stoff}/{unter}/{ein}"
                for match in link_re.finditer(item['f_externe_daten']):
                    yield (url, match.group(1))

    def get_index(
            self,
            kind: str,
            request: 'bottle.LocalRequest',
            p_tags_re: 're.Pattern[str]' = re.compile(
                r'''(?s)</?p\b(?:[^"'>]*?|".*?"|'.*?')*?>'''),
            blank_re: 're.Pattern[str]' = re.compile(r'\s+'),
            lit_erase_re: 're.Pattern[str]' = re.compile(
                r'(^2[^\d\s]|\bvon\s+)'),
            dat_re: 're.Pattern[str]' = re.compile(
                r'(?<!\d)(1\d\d\d|[7-9]\d\d|1\d\.($|<|\s*(Jh\.|Jahrh))|[7-9]\.($|<|\s*(Jh\.|Jahrh)))(?!\d)'),
            gw_re: 're.Pattern[str]' = re.compile(
                r'(?i)https?://(?:www\.)?gesamtkatalogderwiegendrucke.de/docs/(?:GW)?([^"]+)'),
            vd_re: 're.Pattern[str]' = re.compile(
                r'(?i)https?://(?:www\.)?gateway-bayern\.de/(VD16[^"]+)'),
            ) -> 'Generator[dict[str, str]]':
        '''
        Yield all database entities of a certain :param:`kind`.
        The format is apt for a datatable.
        '''
        can_edit = bool(request.roles &
                {'Admin', 'Hauptredakteur', 'Mitarbeiter'})
        with sql_io.DBCon(self.connect()) as (con, cur):
            if kind == 'reg' and can_edit:
                cur.execute(r"select * from reg where reg_von <> -1")
                for row in cur.fetchall():
                    reg_id = row['reg_id']
                    id1 = id2 = id3 = id4 = ''
                    bezeichnung = trim(row['reg_bezeichnung'])
                    if row['reg_art'] == 'Druck':
                        if row['reg_ei_id']:
                            item = con.get(r"""
                                    f_signatur, f_stoffgr_nr, f_untergr_nr,
                                    f_hs_position_nr, f_externe_daten
                                    from t_eintraege where id = %s
                                    """, row['reg_ei_id'])
                            sg = item['f_stoffgr_nr']
                            ug = item['f_untergr_nr']
                            ei = item['f_hs_position_nr']
                            bezeichnung = trim(item['f_signatur'])
                            bezeichnung = self.year_re.sub(
                                    r'\g<place_printer> \g<year>', bezeichnung)
                            m = gw_re.search(item['f_externe_daten'])
                            if m:
                                gw = m.group(1).split('.')[0]
                                bezeichnung += f', GW {gw}'
                                if not row['reg_url']:
                                    row['reg_url'] = f'{m.group()}'
                            else:
                                m = vd_re.search(item['f_externe_daten'])
                                if m:
                                    vd = m.group(1).replace('+', ' ').upper()
                                    bezeichnung += f', {vd}'
                                    if not row['reg_url']:
                                        row['reg_url'] = f'{m.group()}'
                            id1 = f'<a href="/datenbank/druck/{sg}/{ug}/{ei}">{row["reg_ei_id"]}</a>'\
                                    if sg and ug and ei else row['reg_ei_id']
                    elif row['reg_hs_id']:
                        item = con.get(r"""
                                f_string from t_handschriften where id = %s
                                """, row['reg_hs_id'])
                        bezeichnung = trim(item.get('f_string', ''))
                        id2 = row['reg_hs_id']
                    elif row['reg_ug_id']:
                        item = con.get(r"""
                                f_stoffgr_ueberschrift,
                                f_untergr_ueberschrift,
                                t_untergruppen.f_stoffgr_nr as sg,
                                t_untergruppen.f_untergr_nr as ug
                                from t_untergruppen join t_stoffgruppen
                                on t_stoffgruppen.f_stoffgr_nr = t_untergruppen.f_stoffgr_nr
                                where t_untergruppen.id = %s
                                """, row['reg_ug_id'])
                        sg = item.get('sg', '')
                        ug = item.get('ug', '')
                        id3 = f'<a href="/datenbank/untergruppe/{sg}/{ug}">{row["reg_ug_id"]}</a>'\
                                if sg and ug else row['reg_ug_id']
                    elif row['reg_sg_id']:
                        item = con.get(r"""
                                f_stoffgr_ueberschrift,
                                f_stoffgr_nr as sg
                                from t_stoffgruppen
                                where id = %s
                                """, row['reg_sg_id'])
                        sg = item.get('sg', '')
                        id4 = f'<a href="/datenbank/stoffgruppe/{sg}">{row["reg_sg_id"]}</a>'\
                                if sg else row['reg_sg_id']
                    link = f'<a class="key" href="/datenbank/reg/{reg_id}">↗</a>'
                    if can_edit:
                        # Any item with `(row['reg_ei_id'] or row['reg_hs_id'])`
                        # must be deletable. (It does not need to be editable.)
                        link += f'<a class="key" href="/datenbank/edit/reg/{reg_id}">✎</a>'
                    yield { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'.{reg_id}.',
                                's': reg_id,
                            },
                            row['reg_art'],
                            {
                                '_': f'<a class="key" onclick="clipcopy(event)">'\
                                    f'{bezeichnung}⚕{reg_id}</a>',
                                's': parse.sortnumtext(bezeichnung),
                            },
                            row['reg_nebenbezeichnung'],
                            f'<a href="{row["reg_url"]}">{row["reg_url"]}</a>'
                                if row["reg_url"] else '',
                            f'<a href="{row["reg_url_gnd"]}">{row["reg_url_gnd"]}</a>'
                                if row["reg_url_gnd"] else '',
                            id1,
                            id2,
                            id3,
                            id4,
                            link,
                            )) }
            elif kind == 'stoffgruppen':
                exclusion = '' if request.roles else r"""
                        join auth_permission
                            on auth_permission.table_name = 't_stoffgruppen'
                            and auth_permission.record_id = t_stoffgruppen.id
                            and auth_permission.group_id = 41
                            """
                cur.execute(rf"""
                        select
                            f_stoffgr_nr as sn,
                            f_stoffgr_ueberschrift as su,
                            f_band_nr as bn
                        from t_stoffgruppen
                        {exclusion}
                        where t_stoffgruppen.is_active = 'T'
                        """)
                for row in cur.fetchall():
                    bn = row['bn'] or ''
                    sn = row['sn'] or ''
                    su = row['su'] or ''
                    su = blank_re.sub(' ', p_tags_re.sub(' ', su)).strip()
                    if sn:
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': sn + '.',
                                    's': parse.sortnumtext(sn)
                                },
                                {
                                    '_': '<strong><a href="/datenbank/stoffgruppe/{sn}">{su}</a></strong>'\
                                        .format(sn = sn, su = su),
                                    's': parse.sortxtext(su)
                                },
                                {
                                    '_': bn,
                                    's': parse.sortnumtext(bn)
                                },
                                )) }
            elif kind == 'untergruppen':
                exclusion = '' if request.roles else r"""
                        join auth_permission
                            on auth_permission.table_name = 't_untergruppen'
                            and auth_permission.record_id = t_untergruppen.id
                            and auth_permission.group_id = 41
                            """
                cur.execute(rf"""
                        select
                            t_stoffgruppen.id as sg_id,
                            t_stoffgruppen.f_stoffgr_nr as sn,
                            t_stoffgruppen.f_stoffgr_ueberschrift as su,
                            t_untergruppen.id as ug_id,
                            t_untergruppen.f_untergr_nr as un,
                            t_untergruppen.f_untergr_ueberschrift as uu,
                            t_stoffgruppen.f_band_nr as bn
                        from t_stoffgruppen
                        join t_untergruppen
                            on t_untergruppen.f_stoffgr_nr = t_stoffgruppen.f_stoffgr_nr
                        {exclusion}
                        where t_stoffgruppen.is_active = 'T'
                        and t_untergruppen.is_active = 'T'
                        """)
                for row in cur.fetchall():
                    if can_edit:
                        sg_id = row['sg_id']
                        ug_id = row['ug_id']
                        sg_id = f' (ID {sg_id})'
                        ug_id = f' (ID {ug_id})'
                    else:
                        sg_id = ug_id = ''
                    bn = row['bn'] or ''
                    sn = row['sn'] or ''
                    su = row['su'] or ''
                    su = blank_re.sub(' ', p_tags_re.sub(' ', su)).strip()
                    un = row['un'] or ''
                    uu = row['uu'] or ''
                    uu = blank_re.sub(' ', p_tags_re.sub(' ', uu)).strip()
                    if sn and un:
                        nr = sn + '.' + un + '.'
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': nr,
                                    's': parse.sortnumtext(nr)
                                },
                                {
                                    '_': f'<strong><a href="/datenbank/untergruppe/{sn}/{un}">{uu}</a></strong>{ug_id}'
                                        if uu else '–',
                                    's': parse.sortxtext(uu)
                                },
                                {
                                    '_': f'<a href="/datenbank/stoffgruppe/{sn}">{su}</a>{sg_id}',
                                    's': parse.sortxtext(su)
                                },
                                {
                                    '_': bn,
                                    's': parse.sortnumtext(bn)
                                },
                                )) }
            elif kind == 'handschriften' or kind == 'drucke':
                exclusion = '' if request.roles else r"""
                        join auth_permission
                            on auth_permission.table_name = 't_eintraege'
                            and auth_permission.record_id = t_eintraege.id
                            and auth_permission.group_id = 41
                            """
                if kind == 'handschriften':
                    col = 'f_string'
                    name = 'Handschrift'
                    link = 'handschrift'
                else:
                    col = 'f_signatur'
                    name = 'Druck'
                    link = 'druck'
                cur.execute(rf"""
                        select
                            t_stoffgruppen.f_stoffgr_nr as sn,
                            t_stoffgruppen.f_stoffgr_ueberschrift as su,
                            t_untergruppen.f_untergr_nr as un,
                            t_untergruppen.f_untergr_ueberschrift as uu,
                            t_eintraege.f_hs_position_nr as en,
                            t_eintraege.{col} as eu,
                            t_eintraege.f_datum as edat,
                            t_eintraege.f_band_nr as bn
                        from t_stoffgruppen
                        join t_untergruppen
                            on t_untergruppen.f_stoffgr_nr = t_stoffgruppen.f_stoffgr_nr
                        join t_eintraege
                            on t_eintraege.f_stoffgr_nr = t_stoffgruppen.f_stoffgr_nr
                            and t_eintraege.f_untergr_nr = t_untergruppen.f_untergr_nr
                        {exclusion}
                        where t_stoffgruppen.is_active = 'T'
                        and t_untergruppen.is_active = 'T'
                        and t_eintraege.is_active = 'T'
                        and t_eintraege.f_bestand_typ = '{name}'
                        """)
                for row in cur.fetchall():
                    bn = row['bn'] or ''
                    sn = row['sn'] or ''
                    su = row['su'] or ''
                    su = blank_re.sub(' ', p_tags_re.sub(' ', su)).strip()
                    un = row['un'] or ''
                    uu = row['uu'] or ''
                    uu = blank_re.sub(' ', p_tags_re.sub(' ', uu)).strip()
                    en = row['en'] or ''
                    eu = row['eu'] or ''
                    eu = blank_re.sub(' ', p_tags_re.sub(' ', eu)).strip()
                    edat = row['edat'] or ''
                    if sn and un and en:
                        nr = sn + '.' + un + '.' + en + '.'
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': nr,
                                    's': parse.sortnumtext(nr)
                                },
                                {
                                    '_': '<strong><a href="/datenbank/{link}/{sn}/{un}/{en}">{eu}</a></strong>'\
                                        .format(link = link, sn = sn, un = un, en = en, eu = eu),
                                    's': parse.sortxtext(eu)
                                },
                                {
                                    '_': edat,
                                    's': (dat_re.search(edat) or empty).group(),
                                },
                                {
                                    '_': '<a href="/datenbank/untergruppe/{sn}/{un}">{uu}</a>'\
                                        .format(sn = sn, un = un, uu = uu) if uu else '',
                                    's': parse.sortxtext(uu)
                                },
                                {
                                    '_': '<a href="/datenbank/stoffgruppe/{sn}">{su}</a>'\
                                        .format(sn = sn, su = su),
                                    's': parse.sortxtext(su)
                                },
                                {
                                    '_': bn,
                                    's': parse.sortnumtext(bn)
                                },
                                )) }
            elif kind == 'literatur':
                stoff_nr = request.query.stoff_nr\
                        .replace('\\', '').replace('`', '').replace("'", '')
                sql = r"""
                        select
                            id,
                            f_kurztitel as lk,
                            f_volltitel as lv,
                            f_band as lb,
                            f_stoffgruppe as lsg,
                            f_standardwerk as stw
                        from t_lit
                        where is_active = 'T'
                        """
                if stoff_nr:
                    sql += r"""
                            and f_stoffgruppe rlike '\\b{}\\b'
                            """.format(stoff_nr)
                cur.execute(sql)
                for row in cur.fetchall():
                    lk = row['lk'] or ''
                    lv = row['lv'] or ''
                    lb = row['lb'] or ''
                    lsg = row['lsg'] or ''
                    lk = blank_re.sub(' ', p_tags_re.sub(' ', lk)).strip()
                    lv = blank_re.sub(' ', p_tags_re.sub(' ', lv)).strip()
                    lk = ' <a class="key" href="/datenbank/literatur/{}">{}</a>'\
                            .format(row['id'], lk)
                    if lk:
                        yield { str(rank): item for rank, item in enumerate((
                                {
                                    '_': lk,
                                    's': parse.sortxtext(
                                        lk,
                                        acts = (
                                            partial(lit_erase_re.sub, ''),
                                            parse.sortnumtext,
                                            ))
                                },
                                {
                                    '_': lv,
                                    's': parse.sortxtext(lv)
                                },
                                {
                                    '_': lb,
                                    's': parse.sortnumtext(lb)
                                },
                                {
                                    '_': lsg,
                                    's': parse.sortnumtext(lsg)
                                },
                                'ja' if row['stw'] == 'T' else '—',
                                )) }

    def get_item(
            self,
            kind: str,
            stoff: str,
            unter: 'None|str',
            ein: 'None|str',
            request: 'bottle.LocalRequest',
            print_re: 're.Pattern[str]' = re.compile(
                r'^(?P<place>[^:]*):\s*(?P<printer>[^,]*),\s*(?P<year>.*)'),
            ) -> 'dict[str, str]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            if kind in {'druck', 'handschrift'}:
                exclusion = '' if request.roles else r"""
                        join auth_permission
                            on auth_permission.table_name = 't_eintraege'
                            and auth_permission.record_id = t_eintraege.id
                            and auth_permission.group_id = 41
                            """
                sql = rf"""
                        select
                            t_eintraege.*,
                            REGEXP_REPLACE(ifnull(t_stoffgruppen.f_stoffgr_ueberschrift, ''), '<.*?>', '') as f_stoffgr_ueberschrift,
                            REGEXP_REPLACE(ifnull(t_untergruppen.f_untergr_ueberschrift, ''), '<.*?>', '') as f_untergr_ueberschrift,
                            t_baende.f_titel as title,
                            t_baende.f_ort   as place,
                            t_baende.f_jahr  as year
                        from t_eintraege
                        left join t_baende
                            on t_baende.f_band_nr = t_eintraege.f_band_nr
                        left join t_stoffgruppen
                            on t_stoffgruppen.f_stoffgr_nr = t_eintraege.f_stoffgr_nr
                        left join t_untergruppen
                            on t_untergruppen.f_stoffgr_nr = t_eintraege.f_stoffgr_nr
                            and t_untergruppen.f_untergr_nr = t_eintraege.f_untergr_nr
                        {exclusion}
                        where t_eintraege.is_active = 'T'
                        and t_eintraege.f_stoffgr_nr = %s
                        and t_eintraege.f_untergr_nr = %s
                        and t_eintraege.f_hs_position_nr = %s
                        limit 1
                        """
                cur.execute(sql, (stoff, unter, ein))
                item = cur.fetchone()
                if not item:
                    return {}
                if kind == 'druck':
                    (item['druckort'], item['drucker'], item['druckjahr']) = (
                            m.group('place', 'printer', 'year')
                            if (m := print_re.search(item['f_string']))
                            else ('', '', '')
                            )
                add_mss_and_prints(cur, item, stoff, unter)
            elif kind == 'untergruppe':
                exclusion = '' if request.roles else r"""
                        join auth_permission
                            on auth_permission.table_name = 't_untergruppen'
                            and auth_permission.record_id = t_untergruppen.id
                            and auth_permission.group_id = 41
                            """
                cur.execute(rf"""
                        select
                            t_untergruppen.*,
                            REGEXP_REPLACE(ifnull(t_stoffgruppen.f_stoffgr_ueberschrift, ''), '<.*?>', '') as f_stoffgr_ueberschrift,
                            t_baende.f_titel as title,
                            t_baende.f_ort   as place,
                            t_baende.f_jahr  as year
                        from t_untergruppen
                        left join t_baende
                            on t_baende.f_band_nr = t_untergruppen.f_band_nr
                        left join t_stoffgruppen
                            on t_stoffgruppen.f_stoffgr_nr = t_untergruppen.f_stoffgr_nr
                        {exclusion}
                        where t_untergruppen.is_active = 'T'
                        and t_untergruppen.f_stoffgr_nr = %s
                        and t_untergruppen.f_untergr_nr = %s
                        and t_untergruppen.f_untergr_nr <> '0'
                        and (t_untergruppen.f_stoffgr_nr <> '43' or t_untergruppen.f_untergr_nr <> '1')
                        limit 1
                        """, (stoff, unter))
                item = cur.fetchone()
                if not item:
                    return {}
                add_mss_and_prints(cur, item, stoff, unter)
                add_ugs(cur, item, stoff)
            elif kind == 'stoffgruppe':
                exclusion = '' if request.roles else r"""
                        join auth_permission
                            on auth_permission.table_name = 't_stoffgruppen'
                            and auth_permission.record_id = t_stoffgruppen.id
                            and auth_permission.group_id = 41
                            """
                sql = rf"""
                        select
                            t_stoffgruppen.*,
                            t_baende.f_titel as title,
                            t_baende.f_ort   as place,
                            t_baende.f_jahr  as year
                        from t_stoffgruppen
                        left join t_baende
                            on t_baende.f_band_nr = t_stoffgruppen.f_band_nr
                        {exclusion}
                        where is_active = 'T'
                        and f_stoffgr_nr = %s
                        limit 1
                        """
                cur.execute(sql, (stoff,))
                item = cur.fetchone()
                if not item:
                    return {}
                add_mss_and_prints(cur, item, stoff, None)
                add_ugs(cur, item, stoff)
            else:
                return {}
        authors = tuple(filter(None, (
                item['f_zustaendig_1'],
                item['f_zustaendig_2'],
                item['f_zustaendig_3'],
                item['f_zustaendig_4'],
                item['f_zustaendig_5'],
                )))
        num = len(authors)
        if num == 0:
            authority = ''
        elif num == 1:
            authority = authors[0]
        else:
            authority = f"{', '.join(authors[:-1])} und {authors[-1]}"
        item['authors'] = '\xa0/ '.join(authors) if authors else ''
        pos = item.get('f_hs_position_nr', '')
        if not authority:
            item['authority'] = ''
        elif (
                item.get('f_stoffgr_nr', '') != '43' or (
                    item.get('f_untergr_nr', '') == '1' and
                    int(re.sub(r'\D', '', pos or '0')) <= 65 and (
                        pos[-1:] != 'a' or
                        pos == '54a'
                        )
                    )
                ):
            item['authority'] = 'Bearbeitet von ' + authority
        else:
            if authority == 'Regina Cermann':
                item['authority'] = 'Bearbeitet von ' + authority
            else:
                item['authority'] = 'Begonnen von Regina Cermann, fortgeführt von ' + authority
        if kind in {'druck', 'handschrift'}:
            try:
                names = os.listdir(self.repros_path)
            except FileNotFoundError:
                names = ()
            item['abb'] = sorted((
                    name for name in names
                    if name.startswith('.'.join((
                        item['f_stoffgr_nr'],
                        item['f_untergr_nr'],
                        pos,
                        )) + '._') ), key = sort_abb)
            item['f_inhalt_seiten'] = contenttablulate(item['f_inhalt_seiten'])
        return item

    def get_item_bibl(self, bibl_id: int) -> 'dict[str, str]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(r"""
                    select * from t_lit where is_active = 'T' and id = %s
                    limit 1
                    """, (bibl_id,))
            item = cur.fetchone() or {}
        return item

    def get_reg_item(
            self,
            item_id: int,
            for_edit: bool = False,
            ) -> 'dict[str, None|int|str]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            if for_edit:
                item = con.get(r"* from reg where reg_id = %s", item_id)
                assert item, 'Kein änderbarer Eintrag.'
                return item
            row = con.get(
                    r"* from reg where reg_id = %s and reg_von <> -1", item_id)
            if not row:
                return {}
            bezeichnung = self.e(
                    row['reg_bezeichnung']
                    .split('#', 1)[0]
                    .replace('|', ' '))
            if row['reg_ei_id']:
                item = con.get(r"""
                        f_signatur, f_stoffgr_nr, f_untergr_nr, f_hs_position_nr
                        from t_eintraege where id = %s
                        """, row['reg_ei_id'])
                bezeichnung = self.e(
                        item.get('f_signatur', '')
                        .replace('\n', ' ')
                        .replace('<p>', '')
                        .replace('</p>', '')
                        .strip())
                sg = item.get('f_stoffgr_nr', '')
                ug = item.get('f_untergr_nr', '')
                ei = item.get('f_hs_position_nr', '')
                bezeichnung = '<a href="/datenbank/druck/{}/{}/{}">{}</a>'\
                        .format(
                            sg, ug, ei, bezeichnung
                        ) if sg and ug and ei else bezeichnung
            elif row['reg_hs_id']:
                item = con.get(r"""
                        f_string from t_handschriften where id = %s
                        """, row['reg_hs_id'])
                bezeichnung = self.e(
                        item.get('f_string', '')
                        .replace('\n', ' ')
                        .replace('<p>', '')
                        .replace('</p>', '')
                        .strip())
            elif row['reg_ug_id']:
                item = con.get(r"""
                        f_stoffgr_nr as sg,
                        f_untergr_nr as ug
                        from t_untergruppen
                        where id = %s
                        """, row['reg_ug_id'])
                sg = item.get('sg', '')
                ug = item.get('ug', '')
                if sg and ug:
                    bezeichnung = f'<a href="/datenbank/untergruppe/{sg}/{ug}">{bezeichnung}</a>'
            elif row['reg_sg_id']:
                item = con.get(r"""
                        f_stoffgr_nr as sg
                        from t_stoffgruppen
                        where id = %s
                        """, row['reg_sg_id'])
                sg = item.get('sg', '')
                if sg:
                    bezeichnung = f'<a href="/datenbank/stoffgruppe/{sg}">{bezeichnung}</a>'
        row['bezeichnung'] = bezeichnung
        return row

    def get_reg_items(
            self,
            kind: str,
            request: 'bottle.LocalRequest',
            url_re: 're.Pattern[str]' = re.compile(r'href="[^"]*'),
            print_id_re: 're.Pattern[str]' = re.compile(r'(?:GW)?0*(?P<id>.*)'),
            image_id_re: 're.Pattern[str]' = re.compile(r'\bAbb\.?\s*(\d+)'),
            ) -> 'Generator[dict[str, int|str]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            for item in con.getall(r"* from reg where reg_von <> -1"):
                bezeichnung = trim(item['reg_bezeichnung'])
                reg_ei_ids = []
                image_ids = []
                kdih_ids = []
                print_ids = []
                if item['reg_ei_id']:
                    reg_ei_ids = [item['reg_ei_id']]
                elif item['reg_hs_id']:
                    bezeichnung = trim(con.get(r"""
                            IFNULL(f_string, '') as f_string
                            from t_handschriften where id = %s
                            """, item['reg_hs_id'])['f_string'])
                    for ei in con.geteach(r"""
                            id from t_eintraege where f_handschrift_id = %s
                            """, item['reg_hs_id']):
                        reg_ei_ids.append(ei['id'])
                for reg_ei_id in reg_ei_ids:
                    ei = con.get(r"""
                            IFNULL(f_stoffgr_nr, '') as sg,
                            IFNULL(f_untergr_nr, '') as ug,
                            IFNULL(f_hs_position_nr, '') as ei,
                            IFNULL(f_signatur, '') as f_signatur,
                            IFNULL(f_abbildungen, '') as f_abbildungen,
                            IFNULL(f_externe_daten, '') as f_externe_daten
                            from t_eintraege where id = %s
                            """, reg_ei_id)
                    if ei:
                        if item['reg_ei_id'] and not bezeichnung:
                            bezeichnung = trim(ei['f_signatur'])
                            bezeichnung = self.year_re.sub(
                                    r'\g<place_printer> \g<year>', bezeichnung)
                        for m in image_id_re.finditer(ei['f_abbildungen']):
                            image_ids.append(m.group(1))
                        kdih_ids.append(
                                f"{ei['sg']}.{ei['ug']}.{ei['ei']}"
                                .rstrip('.') + '.')
                        if item['reg_art'] == 'Druck':
                            term = ei['f_externe_daten']
                            term = (
                                    (url_re.search(term) or empty).group() or ''
                                    ).rstrip('/')
                            term, print_id = (
                                    term.rsplit('/', 1) if '/' in term else
                                    ('', ''))
                            print_id = print_id.rsplit('.', 1)[0] # cut “.htm”.
                            if print_id:
                                print_id = unquote(print_id).replace('+', ' ')
                                print_id = print_id_re.search(
                                        print_id).group('id')
                                if print_id and\
                                        'gesamtkatalogderwiegendrucke' in term:
                                    print_id = f'GW {print_id}'
                                print_ids.append(print_id)
                image_id = ', '.join(sorted(image_ids, key = parse.sortnumtext))
                yield {
                        'ID': item['reg_id'],
                        'Begriff': bezeichnung,
                        'Verweis': item['reg_nebenbezeichnung'],
                        'Zuordnung': item['reg_art'],
                        'Druck-Nr.': ', '.join(
                            sorted(print_ids, key = parse.sortnumtext)),
                        'KdiH-Nr.': ', '.join(
                            sorted(kdih_ids, key = parse.sortnumtext)),
                        'Abb.-Nr.': f'Abb. {image_id}' if image_id else '',
                        }

    def list_repros(self) -> 'list[tuple[str, str]]':
        try:
            return sorted(os.listdir(self.repros_path), key = parse.sortnumtext)
        except FileNotFoundError:
            return []

    def upsert_reg_item(
            self,
            item_id: int,
            item: 'dict[str, Any]',
            ) -> 'tuple[int, dict[str, str]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            if item_id == 0:
                cur.execute(r"""
                        insert into reg (
                            reg_art,
                            reg_bezeichnung,
                            reg_nebenbezeichnung,
                            reg_url,
                            reg_url_gnd,
                            reg_ug_id,
                            reg_sg_id
                            ) values (%s, %s, %s, %s, %s, %s, %s)
                        """, (
                            item['reg_art'],
                            item['reg_bezeichnung'],
                            item['reg_nebenbezeichnung'],
                            item['reg_url'],
                            item['reg_url_gnd'],
                            item['reg_ug_id'] or None,
                            item['reg_sg_id'] or None,
                            ))
                con.commit()
                item_id = cur.lastrowid
            else:
                cur.execute(r"""
                        update reg set
                            reg_art = %s,
                            reg_bezeichnung = %s,
                            reg_nebenbezeichnung = %s,
                            reg_url = %s,
                            reg_url_gnd = %s,
                            reg_ug_id = %s,
                            reg_sg_id = %s
                        where reg_id = %s
                        """, (
                            item['reg_art'],
                            item['reg_bezeichnung'],
                            item['reg_nebenbezeichnung'],
                            item['reg_url'],
                            item['reg_url_gnd'],
                            item['reg_ug_id'] or None,
                            item['reg_sg_id'] or None,
                            item_id
                            ))
                con.commit()
            item = con.get(r"* from reg where reg_id = %s", item_id)
            return item_id, item

def add_mss_and_prints(
        cur: 'mysql.cursors.DictCursor',
        item: 'dict[str, str]',
        stoff_nr: str,
        unter_nr: 'None|str',
        ) -> 'list[tuple[str, str, str, str]]':
    sql = r"""
            select
                f_untergr_nr as unter_nr,
                f_hs_position_nr as nr
            from t_eintraege
            join auth_permission
                on auth_permission.table_name = 't_eintraege'
                and auth_permission.record_id = t_eintraege.id
                and auth_permission.group_id = 41
            where t_eintraege.is_active = 'T'
            and f_stoffgr_nr = %s
            and f_bestand_typ = %s
            """
    for results_name, type_name in (
            ('mss', 'Handschrift'),
            ('prints', 'Druck'),
            ):
        if unter_nr is None:
            cur.execute(sql, (stoff_nr, type_name))
        else:
            cur.execute(sql + r"""
                    and f_untergr_nr = %s
                    """, (stoff_nr, type_name, unter_nr))
        item[results_name] = sorted(
                (
                    parse.sortnumtext(
                        '.'.join((stoff_nr, row['unter_nr'], row['nr']))),
                    stoff_nr,
                    row['unter_nr'],
                    row['nr'],
                ) for row in cur.fetchall() )

def add_ugs(
        cur: 'mysql.cursors.DictCursor',
        item: 'dict[str, str]',
        stoff_nr: str,
        ) -> 'list[tuple[str, str, str, str]]':
    cur.execute(r"""
            select
                f_untergr_nr as unter_nr
            from t_untergruppen
            join auth_permission
                on auth_permission.table_name = 't_untergruppen'
                and auth_permission.record_id = t_untergruppen.id
                and auth_permission.group_id = 41
            where t_untergruppen.is_active = 'T'
            and f_stoffgr_nr = %s
            and f_untergr_nr <> '0'
            and (t_untergruppen.f_stoffgr_nr <> '43' or t_untergruppen.f_untergr_nr <> '1')
            """, (stoff_nr,))
    item['ugs'] = sorted(
            (
                parse.sortnumtext('.'.join((stoff_nr, row['unter_nr']))),
                stoff_nr,
                row['unter_nr'],
            ) for row in cur.fetchall() )

def contenttablulate(
        term: str,
        fourth_cell_re: 're.Pattern[str]' = re.compile(r'''(?sx)
                <tr(?P<a1>[^>]*)>\s*
                    <td(?P<a2>[^>]*)>(?P<i2>(?:(?!</td>).)*?)</td>\s*
                    <td(?P<a3>[^>]*)>(?P<i3>(?:(?!</td>).)*?)</td>\s*
                    <td(?P<a4>[^>]*)>(?P<i4>(?:(?!</td>).)*?)</td>\s*
                    <td(?P<a5>[^>]*)>(?P<i5>(?:(?!</td>).)*?)</td>\s*
                </tr>''')
        ) -> str:
    return fourth_cell_re.sub(r'''<tr\g<a1>>
<td\g<a2>>\g<i2></td>
<td\g<a3>>\g<i3></td>
<td\g<a4>>
\g<i4>
<div class="description">\g<i5></div>
</td>
</tr>''', term)

def correct_html(term: str, col: str, public: bool) -> str:
    '''
    Correct :param:`term` coming from the cell :param:`col`.

    - Return the empty string, if :param:`term` – after being stripped from tags
      and from any leading or trailing whitespace and being lowered – is nothing
      more than the empty string or the string ``'none'``. This second condition
      is due to one of the many wrong turns of Web2Py.
    - Convert:

        - `<em>` → `<i>`
        - `<span style="font-style: italic; text-decoration: line-through">` →
          `<i><s>…</s></i>`
        - `<span style="font-style: italic; text-decoration: underline">` →
          `<i><u>…</u></i>`
        - `<span style="font-style: italic">` → `<i>`
        - `<span style="text-decoration: line-through">` → `<s>`
        - `<span style="text-decoration: underline">` → `<u>`
        - `<span style="font-variant: small-caps">` → `<author-name>`
        - `<span class="small-caps">` → `<author-name>`

    - After that, keep only the following tag-attrs combinations:

        - `<a class="reg" href title>` – means ‘Registerverweis’
        - `<a href rel="noopener noreferrer" target="_blank">` –
                               means ‘Verweis, erzwingt Öffnen von neuem Reiter’
        - `<a href>` – means ‘Verweis’ –
                              is enforced if the link is a literature reference.
        - In href attributes: “http://kdih.badw.de/” → “https://kdih.badw.de/”
        - `<author-name>` – means ‘Autorname’
        - `<b>` – means ‘Hervorhebung’
        - `<br/>` – means ‘Zeilenwechsel’
        - `<i>` – means ‘Objektsprache (und Hervorhebung)’
        - `<li>` – means ‘Listenglied’
        - `<not-in-print>` – means ‘Nach Drucklegung ergänzt’
        - `<ol style="list-style-type: lower-alpha">` –
                                                means ‘Liste, durchbuchstabiert’
        - `<ol>` – means ‘Liste, durchnummeriert’
        - `<p>` – means ‘Absatz’
        - `<s>` – means ‘Durchstreichung’
        - `<small>` – means ‘petit-Satz’
        - `<span style="color: …">` – if :param:`public` is not ``True`` –
                                                                 means ‘Färbung’
        - `<sub>` – means ‘Tiefstellung’
        - `<sup>` – means ‘Hochstellung’
        - `<table>` – means ‘Tabelle’
        - `<td colspan>` – means ‘Tabellenzelle über mehrere Spalten hinweg’
        - `<td colspan style>` – if :param:`col` is ``'f_untergr_siehe'`` –
            means ‘Tabellenzelle über mehrere Spalten hinweg und mit Gestaltung’
        - `<td style>` – if in f_untergr_siehe –
                                               means ‘Tabellenzelle mit Gestalt’
        - `<td>` – means ‘Tabellenzelle’
        - `<tr>` – means ‘Tabellenzeile’
        - `<u>` – means ‘Unterstreichung’
        - `<ul>` – means ‘Liste mit nicht-ordnenden Aufzählungszeichen’

        - `<wbr/>` – means ‘word break’ (used for long links or so)

    - In the text parts between tags:

      - change straight apostrophe to curly apostrophe.
      - change “°” ‘DEGREE SIGN’ into “º” ‘MASCULINE ORDINAL INDICATOR’.
      - ``unicodedata.normalize('NFC', text)``.
      - turn round into square brackets according to the following rules:

        - Round and square brackets are properly nested.
        - Turn ``'( ). ( ( ) ). ( [ ] ). [ ( ) ]. ( [ ] ). ( ( ( ) ) ). ( [ ( ) ] ). ( ( [ ] ) ). [ ( ( ) ) ]'``
          into ``'( ). ( [ ] ). ( [ ] ). [ ( ) ]. ( [ ] ). ( [ ( ) ] ). ( [ ( ) ] ). ( [ [ ] ] ). [ ( ( ) ) ]'``

    - Remove tagging nested into tagging of the same tag name except the tagging
      of lists and tables.
    - Turn newlines, tabs and four-per-em spaces (U+2005) into blanks.
    - Put whitespace found after a starttag before it.
    - Put whitespace found before an endtag after it.
    - Merge certain elements, when they are equal and adjacent to each other.
    - Normalize whitespace around block elements.
    - Remove empty elements except br and td.
    - Of a sequence of two or more whitespaces, keep only the first.
    - r'<author-name><a href="…(1)…">…</a></author-name> <a href="…(1)…">…</a>'→
      r'<a href="…(1)…"><author-name>…</author-name> …</a>', i.e. defragmentize.
    '''

    def replace_element(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'xmlhtml.Replacer',
            ) -> 'None|str':
        if tag in {'head', 'script', 'style', 'xml'}:
            return ''
        return None

    def replace_tag(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'xmlhtml.Replacer',
            ) -> 'tuple[str, dict[str, None|str], bool]':
        startend = False
        styles = xmlhtml.get_styles(attrs.get('style', ''))
        italic = styles.get('font-style') == 'italic'
        crossed = styles.get('text-decoration') == 'line-through'
        underlined = styles.get('text-decoration') == 'underline'
        # Convert:
        if italic and crossed:
            tag = 'i-s'
        elif italic and underlined:
            tag = 'i-u'
        elif italic or tag == 'em':
            tag = 'i'
        elif crossed:
            tag = 's'
        elif underlined:
            tag = 'u'
        elif (
                styles.get('font-variant') == 'small-caps'
                or attrs.get('class') == 'small-caps'
                ):
            tag = 'author-name'
        # Filter:
        if tag == 'a' and attrs.get('href'):
            if attrs['href'].startswith('http://kdih.badw.de/'):
                attrs['href'] = attrs['href'].replace('http', 'https', 1)
            if attrs['href'].startswith(
                    'https://kdih.badw.de/datenbank/literatur/'):
                attrs = {'href': attrs['href']}
            else:
                attrs = {
                        k: v for k, v in attrs.items()
                        if (k == 'href')
                        or (k == 'title' and attrs.get('class') == 'reg')
                        or (k == 'class' and v == 'reg')
                        or (k == 'target' and v == '_blank')
                        }
                if attrs.get('target') == '_blank':
                    attrs['rel'] = 'noopener noreferrer'
        elif tag in {
                'author-name',
                'b',
                'i',
                'i-s',
                'i-u',
                'li',
                'not-in-print',
                'p',
                's',
                'small',
                'sub',
                'sup',
                'table',
                'tr',
                'u',
                'ul',
                }:
            attrs = {}
        elif tag in {'br', 'wbr'}:
            attrs = {}
            startend = True
        elif tag == 'ol':
            attrs = {'style': 'list-style-type: lower-alpha'}\
                    if styles.get('list-style-type') == 'lower-alpha'\
                    else {}
        elif tag == 'span' and 'color' in styles and not parser.info['public']:
            attrs = {'style': f'color: {styles["color"]}'}
        elif tag == 'td':
            attrs = {
                    k: v for k, v in attrs.items() if
                    (
                        k == 'colspan'
                        and v.isdigit()
                        and v.lstrip('0') not in {'', '1'}
                    ) or (
                        k == 'style'
                        and parser.info['col'] == 'f_untergr_siehe'
                    )
                    }
        else:
            tag = ''
        return tag, attrs, startend

    def replace_text(
            text: str,
            parser: 'xmlhtml.Replacer',
            ) -> str:
        text = text.replace('°', 'º')
        text = text.replace("'", '’')
        text = unicodedata.normalize('NFC', text)
        if parser.info['rebracketing']:
            temp = deque()
            for c in text:
                if c == '[':
                    parser.info['rebrackets'].append(False)
                elif c == ']':
                    parser.info['rebrackets'].pop()
                elif c == '(':
                    if len(parser.info['rebrackets']) == 1:
                        parser.info['rebrackets'].append(True)
                    elif (len(parser.info['rebrackets']) == 2
                            and parser.info['rebrackets'][-1]):
                        parser.info['rebrackets'].append(True)
                        c = '['
                    else:
                        parser.info['rebrackets'].append(False)
                elif c == ')':
                    if (len(parser.info['rebrackets']) == 3
                            and parser.info['rebrackets'][-1]):
                        c = ']'
                    parser.info['rebrackets'].pop()
                temp.append(c)
            text = ''.join(temp)
        return text

    temp = re.sub(r'<[^>]*>', '', term).strip()
    if not temp or temp.lower() == 'none':
        return ''
    brackets = deque()
    rebracketing = False
    for c in temp:
        if c == '(':
            brackets.append('(')
        elif c == ')':
            if not brackets or brackets[-1] != '(':
                break
            brackets.pop()
        elif c == '[':
            brackets.append('[')
        elif c == ']':
            if not brackets or brackets[-1] != '[':
                break
            brackets.pop()
    else:
        if not brackets:
            rebracketing = True
    term = term.replace('<td width:=', '<td width=')

    term = xmlhtml.replace(
            term,
            replace_element = replace_element,
            replace_tag = replace_tag,
            replace_text = replace_text,
            info = {
                'col': col,
                'public': public,
                'rebracketing': rebracketing,
                'rebrackets': deque([False]),
                },
            )
    term = term.replace('<i-s>', '<i><s>').replace('</i-s>', '</s></i>')
    term = term.replace('<i-u>', '<i><u>').replace('</i-u>', '</s></u>')
    term = xmlhtml.unnest(
            term,
            test = lambda tag, attrs, startend, info:
                tag in info['tags'] and
                tag not in {'li', 'ol', 'table', 'td', 'th', 'tr', 'ul'},
            )
    term = re.sub(r'[\n\r\t\u2005]', ' ', term)
    n = 1
    while n:
        term, n = re.subn(r'(<(?!/)[^>]*>)(\s+)', r'\g<2>\g<1>', term)
    n = 1
    while n:
        term, n = re.subn(r'(\s+)(</[^>]*>)', r'\g<2>\g<1>', term)
    block = '(br/|li|ol|p|table|td|th|tr|ul)'
    term = re.sub(rf'\s*<{block}>\s*', r'\n<\g<1>>', term)
    term = re.sub(rf'\s*</{block}>\s*', r'</\g<1>>\n', term)
    n = 1
    while n:
        term, n = re.subn(
                r'<(?!/|td)(?P<name>[^/>]*)></(?P=name)>', '', term)
    n = 1
    while n:
        term, n = re.subn(
                r'</(?P<x>b|i|s|author-name|sub|sup|u)>(?P<in>\s*|-)<(?P<x>)>',
                r'\g<in>', term)
    term = re.sub(rf'\s*<{block}>\s*', r'\n<\g<1>>', term)
    term = re.sub(rf'\s*</{block}>\s*', r'</\g<1>>\n', term)
    term = re.sub(r'(\s)\s+', r'\g<1>', term)
    term = re.sub(
            r'<author-name>(?P<a><a href="(?P<url>[^"]*)">)(?P<text>[^<>]*)</a>'
            r'</author-name>(?P<space>\s*)<a href="(?P=url)">',
            r'\g<a><author-name>\g<text></author-name>\g<space>',
            term)
    term = re.sub(
            r'(?P<a><a [^>]*>)(?P<inner>(?:(?!</a>).)*)</a>(?P=a)',
            r'\g<a>\g<inner>',
            term)
    return term.strip()

def sort_abb(
        abb: str,
        strip_re: 're.Pattern[str]' = re.compile(r'(?i)(^\W+|\W+$)'),
        split_re: 're.Pattern[str]' = re.compile(r'[\W_]+'),
        roman_re: 're.Pattern[str]' = re.compile(r'^[IVXLCDM]+([a-z])?$'),
        arabic_re: 're.Pattern[str]' = re.compile(r'^\d+$'),
        letters: 'set[str]' = frozenset('abcdefghijklmnopqrstuvwxyz'),
        ) -> 'list[str]':
    if '_' not in abb:
        return ['']
    abb = abb.split('_', 1)[1].rsplit('.', 1)[0].replace('Abb', 'Zbb')
    items = []
    for item in split_re.split(strip_re.sub('', abb)):
        if arabic_re.search(item):
            items.append(item.zfill(4))
        elif roman_re.search(item):
            letter = item[-1]
            if letter in letters:
                items.append(str(parse.roman_to_arabic(item[:-1])).zfill(4))
                items.append(letter)
            else:
                items.append(str(parse.roman_to_arabic(item)).zfill(4))
        else:
            items.append(item)
    return items

def trim(
        term: str,
        space_re: 're.Pattern[str]' = re.compile(r'(?: |\n|<p>|</p>)+'),
        ) -> str:
    return space_re.sub(' ', term).strip()
