# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
import sys
import time
import traceback
from collections import deque

import gehalt
import __init__
import fs
import sql_io
import sys_io

def trace(e_old: str, table: str, row_id: str) -> str:
    e = traceback.format_exc()
    if e != e_old:
        print(f'In table {table}, row {row_id}:')
        print(e, end = '\n\n')
        e_old = e
    time.sleep(2)

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Perform an ongoing background process:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Supply entries for the register.
    - Apply :func:`gehalt.correct_html`.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    connect = sql_io.get_connect(*fs.get_keys(paths['db_access']), config)
    e_old = ''
    while True:
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        time.sleep(1)
        items = deque()
        table = None
        row_id = None
        try:
            with sql_io.DBCon(connect()) as (con, cur):
                for item in con.geteach(r"""
                        id from t_eintraege
                        left join reg on reg_ei_id = id
                        where reg_ei_id is NULL and id is not NULL
                        and f_bestand_typ = 'Druck'
                        """):
                    items.append(
                            ('Druck', '', '', '', item['id'], None, None, None))
                for item in con.geteach(r"""
                        id from t_handschriften
                        left join reg on reg_hs_id = id
                        where reg_hs_id is NULL and id is not NULL
                        and f_string <> '' and f_string is not NULL
                        """):
                    items.append(
                            ('Hs.', '', '', '', None, item['id'], None, None))
                for item in items:
                    cur.execute(r"""
                            insert into reg (
                                reg_art, reg_bezeichnung, reg_url, reg_url_gnd,
                                reg_ei_id, reg_hs_id, reg_ug_id, reg_sg_id
                            ) values (%s, %s, %s, %s, %s, %s, %s, %s)
                            """, item)
                con.commit()
                results = con.getall(r"""
                        table_name from information_schema.tables
                        where table_schema = %s
                        """, config['db']['name'])
            for result in results:
                time.sleep(1)
                table = result['table_name']
                if table not in {
                        't_eintraege',
                        't_lit',
                        't_stoffgruppen',
                        't_untergruppen',
                        }:
                    continue
                with sql_io.DBCon(connect()) as (con, cur):
                    cur.execute("describe " + table)
                    cols = cur.fetchall()
                if 'id' not in { col['Field'] for col in cols }:
                    continue
                cols = tuple(
                        col['Field'] for col in cols
                        if 'text' in col['Type'].lower()
                        and col['Field'].startswith('f_')
                        )
                if not cols:
                    continue
                colterm = ', '.join(cols)
                colsets = ' = %s, '.join(cols) + ' = %s'
                with sql_io.DBCon(connect()) as (con, cur):
                    rows = deque(con.geteach("id from " + table))
                for row in rows:
                    time.sleep(0.1)
                    row_id = row['id']
                    while True:
                        try:
                            with sql_io.DBCon(connect()) as (con, cur):
                                public = True if con.get(r"""
                                        id from auth_permission
                                        where group_id = 41 -- Allgemeinheit
                                        and table_name = %s
                                        and record_id = %s
                                        """, table, row_id) else False
                                con.begin()
                                # LOCK IN SHARE MODE:
                                # Prevent writing until we have written, so that
                                # rather we get overwritten in a race condition.
                                # Do not prevent reading (would be `FOR UPDATE`)
                                # because this could hamper editors too much.
                                cur.execute(f"""
                                        select {colterm}
                                        from `{table}` where id = %s
                                        LOCK IN SHARE MODE
                                        """, row_id)
                                result = cur.fetchone()
                                if result: # The row may have been removed.
                                    olds = []
                                    news = []
                                    for col in cols:
                                        old = result[col]
                                        olds.append(old)
                                        new = old or '' # Type “text” ensured.
                                        new = gehalt.correct_html(
                                                new, col, public)
                                        news.append(new)
                                    if news != olds:
                                        news.append(row_id)
                                        cur.execute(f"""
                                                update `{table}` set {colsets}
                                                where id = %s
                                                """, news)
                                con.commit()
                            break
                        except: # Sic.
                            e_old = trace(e_old, table, row_id)
                row_id = None
            table = None
        except: # Sic.
            e_old = trace(e_old, table, row_id)

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
