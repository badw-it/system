# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
'''
Infinite loop saving files at certain time intervals and
thinning them out after certain time intervals.
'''
import os
import subprocess
import sys
import time
from collections import deque

import fs

def run_backup(
        command_sans_filename: 'Iterable[str]',
        backupdirpath: str,
        seconds_between_backups: int = 3600, # 1 hour
        thin_out_seconds: 'dict[int, int]' = {
            86400: 21600,
            1036800: 345600,
            2073600: 1036800,
            3110400: 2764800,
            4147200: 3110400,
            5184000: 5184000,
            10368000: 10368000,
            31104000: 31104000,
            },
        dateformat = '%Y-%m-%d_%H.%M.%S',
        ) -> None:
    '''
    Do a backup with :param:`command_sans_filename`. This is a list of arguments
    which are executable by :func:``subprocess.call`` and produce a backup, e.g.
    an SQL dump, and store it in a file in :param:`backupdirpath`.

    The basename of the file is a time stamp produced while the backup is taken.
    In this regard, it is important to note:

    .. important::
        At least one argument of :param:`command_sans_filename` must contain the
        string “{}” as a placeholder for the **basename of the file** – any “{}”
        in any argument will be replaced by a time stamp for naming each backup.

        Do not use “{}” except for this purpose.

        Do not use “{}” more than once in each argument.

        :param:`dateformat` specifies the format of the time stamp in the manner
        required by :func:`time.strftime`. For a time-zone aware output, include
        “%z” in :param:`dateformat`.

    An example for :param:`command_sans_filename`:

    ``['mysqldump', 'DB', '--user=YYY', '--password=ZZZ', '--result-file={}']``

    Do the backup every :param:`seconds_between_backups` in an infinite loop.

    Thin out older backups according to :param:`thin_out_seconds` – its keys and
    values are durations in seconds and can be explained best by elaborating the
    meaning of the given default as follows:

    - After 1 day, keep only backups with a distance of at least 6 hours.
    - After 12 days, keep only backups with a distance of at least 4 days.
    - After 24 days, keep only backups with a distance of at least 12 days.
    - After 36 days, keep only backups with a distance of at least 24 days.
    - After 48 days, keep only backups with a distance of at least 36 days.
    - After 60 days, keep only backups with a distance of at least 60 days.
    - After 120 days, keep only backups with a distance of at least 120 days.
    - After 360 days, keep only backups with a distance of at least 360 days.

    After each thin-out, yield the names of the backup files still saved.
    '''
    while True:
        current_time = time.time()
        backupfilepath = fs.pave(fs.get_new_path(os.path.join(
                backupdirpath,
                time.strftime(dateformat, time.localtime(current_time)),
                )))
        backupdirpath, backupfilename = os.path.split(backupfilepath)
        os.chdir(backupdirpath)
        subprocess.run(
                [ arg.format(backupfilename) for arg in command_sans_filename ])
        for path in thin_out_files(
                backupdirpath,
                dateformat,
                thin_out_seconds,
                ):
            try:
                os.remove(path)
            except: # Sic.
                pass
        while True:
            time.sleep(1)
            if (time.time() - current_time) >= seconds_between_backups:
                break

def thin_out_files(
        dirpath: str,
        dateformat: str,
        thin_out_seconds: 'dict[int, int]',
        ) -> 'deque[str]':
    '''
    In :param:`dirpath`, parse the basename of every file as a date given in the
    format :param:`dateformat`. (If this fails, skip the file.)

    Return all filepaths that must be removed as per :param:`thin_out_seconds` –
    for this parameter, see :func:`run_backup`.

    The oldest and the newest backups are never named for deletion. If there are
    fewer than two files, consequently, an empty sequence is returned.
    '''
    names = os.listdir(dirpath)
    for i in range(len(names) - 1, -1, -1):
        basename = os.path.splitext(names[i])[0]
        try:
            date = time.mktime(time.strptime(basename, dateformat))
        except:
            del names[i]
        else:
            names[i] = (date, names[i])
    if len(names) < 2:
        return []
    names.sort(reverse = True)
    # The oldest backup is never returned.
    # Its date is the starting point to measure the distances of backups.
    older = names.pop()[0]
    # The date of the newest backup is the fixed point to measure the ages.
    newest = names[0][0]
    to_be_removed_paths = deque()
    while names:
        date, name = names.pop()
        current_age = newest - date
        current_distance = date - older
        if all(
                (current_age <= age or current_distance >= distance)
                for age, distance in thin_out_seconds.items() ):
            older = date
        else:
            to_be_removed_paths.append(os.path.join(dirpath, name))
    return to_be_removed_paths
