# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)

import __init__
import web_io

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.
        '''
        super().__init__(urdata, __file__)
