# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import sys
import time
try:
    from beaker.middleware import SessionMiddleware
except:
    SessionMiddleware = None

import gehalt
import __init__
import bottle
from bottle import HTTPError, redirect, request, response, static_file, template

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        if not db.debug:
            server.quiet = True
        self.kwargs = {
                'app': self if SessionMiddleware is None else
                    SessionMiddleware(self, db.config['session']),
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.hook('before_request')
        def status():
            request.user_id, request.user_name, request.roles = db.auth(request)

        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which do not specify the page to the default page.
            '''
            redirect(f'/{db.lang_id}/{db.page_id}')

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/<lang_id>/auth_admin')
        def auth_admin(lang_id):
            if not request.roles:
                redirect('/' + db.page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_admin.tpl', 'lang_id': lang_id})

        @self.route('/<lang_id>/auth_admin', method = 'POST')
        def auth_admin_post(lang_id):
            note = db.auth_new(
                    request.forms,
                    request.user_id,
                    request.roles,
                    lang_id)
            redirect('/auth_admin?note=' + db.q(note))

        @self.route('/<lang_id>/auth_end')
        def auth_end(lang_id):
            session = request.environ.get('beaker.session', None)
            if session is not None:
                session.delete()
            redirect(request.query.url or f'/{lang_id}/{db.page_id}')

        @self.route('/<lang_id>/' + db.auth_in_page_id)
        def auth_in(lang_id):
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_in.tpl', 'lang_id': lang_id})

        @self.route('/<lang_id>/' + db.auth_in_page_id, method = 'POST')
        def auth_in_post(lang_id):
            user_name = request.forms.user_name
            password = request.forms.password
            user_id = db.auth_in(user_name, password)
            if user_id is None:
                query = request.forms.query
                if query:
                    query = '?' + query
                redirect(request.fullpath + query)
            else:
                session = request.environ['beaker.session']
                session['user_id'] = user_id
                session.save()
                redirect(request.forms.url or f'/{lang_id}/{db.page_id}')

        @self.route('/<lang_id>/<page_id>')
        def return_page(lang_id, page_id):
            '''
            Return the page :param:`page_id`.

            If the given page is not found, raise the HTTP error 404.
            '''
            text, tpl = db.get_text_and_template_name('', page_id)
            if text:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'lang_id': lang_id,
                            'text': text,
                            })
            else:
                raise HTTPError(404, db.glosses['httperror404'][lang_id])

        @self.route('/<lang_id>/<page_id>/json')
        def return_data(lang_id, page_id):
            '''
            Return JSON data for a datatable at :param:`page_id`.
            '''
            return static_file(
                    f'{page_id}.json',
                    db.content_path,
                    mimetype = 'application/json',
                    )

        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/icons/favicon.ico`
            - `/de/acta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `download` and a
            non-empty value, the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(2)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
