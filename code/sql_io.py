# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
import sqlite3
import sys
import typing as ty
from functools import partial

try:
    import pymysql as mysql
except ImportError:
    pass # PyMySQL may be not necessary (e.g. for working just with sqlite).

class DBCon():
    '''
    Context for a database connection and a database cursor. E.g.::

        connect = sql_io.get_connect(
                input('Username'),
                input('Password'),
                {'db': {
                    'host': 'localhost',
                    'port': '3306',
                    'name': 'testdb',
                    'charset': 'utf8mb4',
                    }},
                )
        with sql_io.DBCon(connect()) as (con, cur):
            for row in con.geteach("* from testtable"):
                print(row)

    E.g. with sqlite::

        with sql_io.DBCon(sqlite3.connect(path)) as (con, cur):
            cur.execute("select * from testtable")
            for row in cur.fetchall():
                print(row)

    On exit: If an exception has occurred, a rollback is done. In any case, the
    connection is closed. (The rollback is done explicitly as it is not implied
    in the case that connection pooling is used.) A commit is **not** done. For
    this to happen, use ``con.commit()``.
    '''
    def __init__(self, con):
        self.con = con
        self.cur = self.con.cursor()
        try:
            self.cur.execute("SET autocommit=0;")
        except: # Sic.
            pass
        try:
            self.con.get = self.get
            self.con.getall = self.getall
            self.con.geteach = self.geteach
        except AttributeError:
            # Not possible e.g. for SQLite.
            pass

    def __enter__(self):
        return (self.con, self.cur)

    def __exit__(self, exception, value, traceback):
        if exception:
            self.con.rollback()
        self.cur.close()
        self.con.close()

    def get(self, sql, *params) -> 'dict':
        '''
        Get one record from the query :param:`sql`, parameterized with
        :param:`params`, and return it as a dictionary. If there is no
        matching record, return an empty dictionary.

        .. important::
            - The leading ``'SELECT '`` of the query is prepended here
              and **must not** be already given in :param:`sql`.
            - Similarly, ``'\nLIMIT 1'`` will be appended to the query
              and **must not** be already given in :param:`sql`.
            - Due to the previous point, :param:`sql` **must not** end
              with a semicolon and a comment.

        This method is bound to ``self.con`` in :meth:`__init__`, i.e.
        you can do e.g.:

        ``name = con.get(
                "name from names where term = %s and num = %s",
                'halb', 12
                ).get('name', '')``.
        '''
        if params:
            self.cur.execute('SELECT ' + sql.rstrip(';') + '\nLIMIT 1', params)
        else:
            self.cur.execute('SELECT ' + sql.rstrip(';') + '\nLIMIT 1')
        return self.cur.fetchone() or {}

    def getall(self, sql, *params) -> 'list[dict]':
        '''
        Get all records from the query :param:`sql`, parameterized with
        :param:`params`, and return them as a list of dictionaries or –
        if there is no matching record – as an empty list.

        .. important::
            - The leading ``'SELECT '`` of the query is prepended here
              and **must not** be already given in :param:`sql`.

        This method is bound to ``self.con`` in :meth:`__init__`, hence
        you can do e.g.:

        ``for name in con.getall(
                "name from names where term = %s and num = %s",
                'halb', 12
                ):``.
        '''
        if params:
            self.cur.execute('SELECT ' + sql, params)
        else:
            self.cur.execute('SELECT ' + sql)
        return self.cur.fetchall()

    def geteach(self, sql, *params) -> 'ty.Iterator[dict]':
        '''
        Get each record from the query :param:`sql`, parameterized with
        :param:`params`, and return them as an iterator of dictionaries
        or – if there is no matching record – as an empty iterator.

        .. important::
            - The leading ``'SELECT '`` of the query is prepended here
              and **must not** be already given in :param:`sql`.

        This method is bound to ``self.con`` in :meth:`__init__`, hence
        you can do e.g.:

        ``for name in con.geteach(
                "name from names where term = %s and num = %s",
                'halb', 12
                ):``.
        '''
        if params:
            self.cur.execute('SELECT ' + sql, params)
        else:
            self.cur.execute('SELECT ' + sql)
        return iter(self.cur.fetchone, None)

def cols(
        cur: 'mysql.cursors.DictCursor|sqlite3.Cursor',
        schema: 'None|str',
        table: str,
        ) -> 'dict[str, dict[str, str|int]|tuple[str|int, ...]]':
    '''
    For the schema :param:`schema` and one of its tables, :param:`table`, return
    a dictionary whose keys are the names of the columns of this table and whose
    values are descriptions of each column:

    - as given in the table “COLUMNS” in the schema “information_schema”.
    - or, if :param:`schema` is ``None`` and sqlite is assumed, as given via the
      pragma “table_info”.
    '''
    if schema is None:
        table = table.replace('\\', '').replace('`', '')
        cur.execute(rf"pragma table_info(`{table}`)")
        info = { row[1]: row for row in cur.fetchall() }
    else:
        cur.execute(r"""
                select * from information_schema.COLUMNS
                where TABLE_SCHEMA = %s
                and TABLE_NAME = %s
                """, (schema, table))
        info = { row['COLUMN_NAME']: row for row in cur.fetchall() }
    return info

def get_connect(
        db_user: str,
        db_password: str,
        config: 'dict[str, dict[str, ty.Any]]',
        key: str = 'db',
        db_host_key: str = 'host',
        db_port_key: str = 'port',
        db_name_key: str = 'name',
        db_charset_key: str = 'charset',
        db_unix_socket_key: str = 'unix_socket',
        db_cursor_key: str = 'cursor',
        ) -> 'ty.Callable[[], mysql.Connection]':
    '''
    Get a partial object that returns a database connection when called. E.g.::

        connect = sql_io.get_connect(
                input('Username'),
                input('Password'),
                {'db': {
                    'host': 'localhost',
                    'port': '3306',
                    'name': 'testdb',
                    'charset': 'utf8mb4',
                    }},
                )
        with sql_io.DBCon(connect()) as (con, cur):
            for row in con.geteach("* from testtable"):
                print(row)

    :param db_user: the user name used for the connection.
    :param db_password: the userʼs password.
    :param config: has at least the key :param:`key` whose value is a mapping of
        keys to values for establishing a database connection. The keys and some
        example values are:
    :param db_host_key: In the configuration, the value given at this key may be
        ``'localhost'``.
    :param db_port_key: In the configuration, the value given at this key may be
        ``3306``.
    :param db_name_key: The name of the database in the DBMS, may be ``'test'``.
    :param db_charset_key: In the configuration, the value given at this key may
        be ``'utf8mb4'``.
    :param db_unix_socket_key: Only for Linux and if the host is “localhost”! In
        the configuration, the value specified at this key is the path to a file
        (which may not yet exist) like ``/run/mysqld/mysqld.sock``.
    :param db_cursor_key: Optional. If the belonging value is one of …

        - ``'DictCursor'``
        - ``'SSCursor'``
        - ``'SSDictCursor'``

        … the corresponding mysql.cursors cursor is used. (See the documentation
          https://pymysql.readthedocs.io/en/latest/modules/cursors.html on these
          cursor types.)

        Otherwise, the DictCursor is used.
    '''
    host = config[key][db_host_key]
    return partial(
            mysql.connect,
            autocommit  = False,
            host        = host,
            port        = int(config[key][db_port_key]),
            user        = db_user,
            passwd      = db_password,
            db          = config[key][db_name_key],
            charset     = config[key][db_charset_key],
            unix_socket =
                config[key][db_unix_socket_key]
                    if (host == 'localhost' and sys.platform == 'linux')
                else None,
            cursorclass = {
                'DictCursor': mysql.cursors.DictCursor,
                'SSCursor': mysql.cursors.SSCursor,
                'SSDictCursor': mysql.cursors.SSDictCursor,
                }[config[key].get(db_cursor_key, 'DictCursor')]
            )

def rows(
        cur: 'mysql.cursors.DictCursor|sqlite3.cursor',
        schema: 'None|str',
        table: str,
        ) -> 'ty.Generator[dict[str, ty.Any]]':
    '''
    Get all rows of table :param:`table` of the database :param:`schema`; but if
    this argument is ``None``, assume a sqlite database with only one schema.

    Yield a generator of rows. Yield each row as a dictionary whose keys are the
    names of the columns and whose values are the values of the cells.
    '''
    table = table.replace('\\', '').replace('`', '')
    if schema is None:
        names = cols(cur, schema, table).keys()
        cur.execute(rf"select * from `{table}`")
        for row in iter(cur.fetchone, None):
            yield dict(zip(names, row))
    else:
        schema = schema.replace('\\', '').replace('`', '')
        cur.execute(rf"select * from `{schema}`.`{table}`")
        for row in iter(cur.fetchone, None):
            yield row

def tables(
        cur: 'mysql.cursors.DictCursor|sqlite3.cursor',
        schema: 'None|str',
        ) -> 'ty.Generator[str]':
    '''
    Get all table names of the database :param:`schema`; but if this argument is
    ``None``, assume a sqlite database with only one schema.
    '''
    if schema is None:
        cur.execute("select name from sqlite_master where type = 'table'")
        for row in iter(cur.fetchone, None):
            yield row[0]
    else:
        cur.execute(r"""
                select TABLE_NAME from information_schema.TABLES
                where TABLE_SCHEMA = %s
                """, (schema,))
        for row in iter(cur.fetchone, None):
            yield row['TABLE_NAME']
