# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import os
import statistics
from collections import defaultdict
from collections import deque
from decimal import Decimal as D
from functools import reduce
from itertools import combinations
from operator import itemgetter

try:
    import numpy as np
except ImportError:
    print('numpy could not be imported.')

import bottle

def format_cell(
        ratio: 'None|float',
        mad: float,
        total: float,
        tablemad: float,
        convert: 'ty.Callable[[float], str]' =
                lambda ratio: f'{ratio:.0%}'[:-1],
        cell_pattern: str       =
            '{spread_marker}{ratio_converted}{few_marker}{hint_at_total}',
        hint_at_total: str      = '<sub><i>{total}</i></sub>',
        few_limit: int          = 15,
        few_marker: str         = '<sub>?</sub>',
        too_few_limit: int      = 5,
        too_few_marker: str     = '?',
        too_few_background: str = '#ffffff',
        too_few_color: str      = '#000000',
        spread_marker: str      = '<sub>≈</sub>',
        text_bright: str        = '#ffffff',
        text_dark: str          = '#000000',
        ) -> 'tuple[str, str, str]':
    '''
    Get a tuple consisting of:

    - a background color determined by :param:`ratio`: a grey tone
      corresponding with the ratio value, and white for a ratio of 0,
      black for a ratio of 1.
    - a text color: dark text for grey brighter than fifty percent, else
      bright text.
    - a value, converted from :param:`ratio` and formatted according to
      :param:`mad` and :param:`total`, compared with :param:`tablemad`,
      :param:`few_limit` and :param:`too_few_limit`. If :param:`total`
      is less than :param:`too_few_limit`, then:

      - the value is set to :param:`too_few_marker`,
      - the background is set to :param:`too_few_background`,
      - the text color is set to :param:`too_few_color`.

    If :param:`ratio` is ``None``, :param:`total` is expected to be ``0``.
    This case is handled like all cases in which :param:`total` is less
    than :param:`too_few_limit`, regardless of the actual value of this
    parameter.

    :param cell_pattern: becomes converted to the cell value with
        :meth:`str.format`, which takes the following keyword arguments:

        - ratio_converted, i.e :param:`ratio` after having been processed as
          argument of :param:`convert`.
        - the following three keyword arguments of this function itself:

    :param spread_marker: the empty string or :param:`spread_marker`, if the
        spread in the cell is greater than the spread in the whole table.
    :param few_marker: the empty string or :param:`few_marker`, if the
        total value is less than :param:`few_limit` and greater than
        :param:`too_few_limit`. This is skipped if the following parameter
        is not the empty string:
    :param hint_at_total: becomes converted to a hint at the total value of
        the cell with :meth:`str.format`, which takes the total value as
        keyword argument.
    '''
    if total < too_few_limit or ratio == None:
        ratio_converted = too_few_marker
        background      = too_few_background
        color           = too_few_color
        few_marker      = ''
        spread_marker   = ''
        hint_at_total   = hint_at_total.format(total = total) \
                          if hint_at_total else ''
    else:
        # For the grey scale, map the ratio to a corresponding
        # value between 10 and 245 (not 0 and 255 to avoid total
        # black and white).
        grey            = hex(int(10 + 235 - 235 * ratio))
        background      = '#{0:0>2}{0:0>2}{0:0>2}'.format(grey[2:])
        color           = text_dark if ratio < 0.5 else text_bright
        spread_marker   = spread_marker if mad > tablemad else ''
        ratio_converted = convert(ratio)
        if hint_at_total:
            hint_at_total = hint_at_total.format(total = total)
            few_marker    = ''
        else:
            few_marker    = few_marker if total < few_limit else ''
            hint_at_total = ''
    return background, color, cell_pattern.format(
            spread_marker   = spread_marker,
            ratio_converted = ratio_converted,
            hint_at_total   = hint_at_total,
            few_marker      = few_marker,
            )

def heatmaps(
        template_path: str,
        matchtable: 'Iterable[Iterable[Hashable]]',
        results: 'Iterable[Hashable]',
        casegetter: itemgetter = itemgetter(0),
        matchgetter: itemgetter = itemgetter(1, 2),
        variantgetter: itemgetter = itemgetter(3),
        default_factory: 'ty.Callable[[], float]' = lambda: 0,
        total: float = 0,
        format_config: 'dict[str, str|int|ty.Callable[[float], str]' = {},
        ) -> str:
    '''
    Get HTML charts. Each cell in each chart is greyed proportionally to its own
    value divided by the sum of all the corresponding cells in each chart – e.g.
    the value of the upper left cell divided by the sum of all upper left cells.

    The HTML string which contains the charts is built using the bottle template
    at :param:`template_path`.

    The charts are structured according to :param:`matchtable` after being drawn
    from the items of :param:`results` by :param:`casegetter` – for more details
    and for the following parameters, see :func:`.tabulate`.

    The items of :param:`format_config`, that lets you customize the formatting,
    are given as keyword arguments to :func:`.format_cell`. For further details,
    see this function.
    '''
    data = defaultdict(int)
    for key in results:
        data[key] += 1
    tables, total = tabulate(data, matchtable, casegetter, matchgetter,
            variantgetter, default_factory, total)
    totals_table = table_of_totals(tables)
    ratio_tables = tables_of_case_ratios(tables)
    stats_tables = { case:
            table_of_derived_cell_values(ratio_tables[case])
            for case in tables }
    total_tables = { case:
            table_of_derived_cell_values(
                tables[case], derive = lambda values: {'total': sum(values)})
            for case in tables }
    ratio_total_tables = tables_of_case_ratios(total_tables)
    tablestats = { case:
            mad_mean(deque( value
                for row in tables[case]
                for cell in row
                for value in (cell.values() or (0,)) ))
            for case in tables }
    charts = { case:
            ( format_cell(
                    ratiocell['total'],
                    madcell.get('mad', 0),
                    totalcell['total'],
                    tablestats[case].get('mad', 0),
                    **format_config)
                for ratiosrow, madsrow, totalsrow in zip(
                    ratio_total_tables[case],
                    stats_tables[case],
                    totals_table)
                for ratiocell, madcell, totalcell in zip(
                    ratiosrow, madsrow, totalsrow) )
            for case in tables }
    template_dirpath, template_name = os.path.split(template_path)
    bottle.TEMPLATE_PATH = [template_dirpath]
    return bottle.template(template_name, charts = charts, total = total)

def integer_range(
        integer: int,
        integers_in_range: int,
        integers_in_superrange: int,
        ) -> 'tuple[int, int]':
    '''
    Get a range that contains :param:`integer`, e.g. a range of years like
    1250 to 1275 for the year 1254, when a century is 100 years long.

    >>> import calc
    >>> print(calc.integer_range(1254, 25, 100))
    (1250, 1275)
    '''
    begin = integer - integer % integers_in_superrange
    while True:
        if begin <= integer < begin + integers_in_range:
            break
        begin += integers_in_range
    return (begin, begin + integers_in_range)

def mad_mean(
        values: 'Sequence[float|D]',
        ) -> 'dict[str, float|D]':
    '''
    Get the mean and the mean absolute deviation of :param:`values`.
    '''
    if values:
        mean = statistics.mean(values)
        mad = statistics.mean( abs(value - mean) for value in values )
        return {'mad': mad, 'mean': mean}
    else:
        return {}

def mad_median(
        values: 'Sequence[float|D]',
        ) -> 'dict[str, float|D]':
    '''
    Get the median and the median absolute deviation of :param:`values`.
    '''
    if values:
        median = statistics.median(values)
        mad = statistics.median( abs(value - median) for value in values )
        return {'mad': mad, 'median': median}
    else:
        return {}

def matrix(
        data: 'dict[Frozenset[str], int]',
        ) -> 'tuple[list[str], np.array, np.array]':
    '''
    Process :param:`data`. Its keys are cases and its values are the numbers how
    often these cases were counted. A case is a set of features. Each feature is
    represented as a name denoting the feature.

    From these data, get:

    - an alphabetically sorted list of all feature names.
    - a table-like matrix: Each column belongs to a feature; each row represents
      a case. (A case is a unique set of features.) Each field of a row contains
      a boolean value – the value tells if the feature represented by the column
      of this field occurred in that case or not.
    - an array of values, each of which corresponds with a row of the matrix and
      tells how often the case represented by this row occurred.

    >>> import calc
    >>> data = {frozenset(('red', 'green')): 343,
    ...         frozenset(('green', 'blue', 'mauve')): 22}
    >>> cols, matrix, rowcounts = calc.matrix(data)
    >>> cols
    ['blue', 'green', 'mauve', 'red']
    >>> matrix
    array([[False,  True, False,  True],
           [ True,  True,  True, False]])
    >>> rowcounts
    array([343,  22], dtype=int64)
    '''
    cols = sorted(reduce(lambda x, y: x | y, data.keys()))
    rowcounts = np.fromiter(data.values(), dtype = np.int64)
    matrix = np.array(
            [ [ True if name in key else False for name in cols ]
                for key in data.keys() ],
            dtype = bool)
    return cols, matrix, rowcounts

def per_ratio(
        A_B: int,
        nonA_B: int,
        A_nonB: int,
        nonA_nonB: int,
        infinity: float = float('infinity'),
        ) -> 'float|int':
    '''(Anteilsverhältnis)

    Get the percentage ratio: i.e. a number telling how many times more often an
    occurrence A occurred when also a condition B occurred, than A occurred when
    B did not occur. Imagine a cross tabulation with four cells:

    :param A_B: the number of cases where A and B occurred: A ∩ B
    :param nonA_B: the number of cases where A did not and B did occur: ¬A ∩ B
    :param A_nonB: the number of cases where A did and B did not occur: A ∩ ¬B
    :param nonA_nonB: the number of cases where neither occurred: ¬A ∩ ¬B

    Thus the resulting number is a measure to estimate how much more likely A is
    under the condition B than under the condition non-B: P(A|B) / P(A|¬B)

    This number is in principle calculated as follows:
    ``(A_B / (A_B + nonA_B)) / (A_nonB / (A_nonB + nonA_nonB))``

    Hence:
    ``(A_B * (A_nonB + nonA_nonB)) / (A_nonB * (A_B + nonA_B))``

    If the values for B and non-B are switched – i.e. ``A_B`` becomes ``A_nonB``
    and vice versa, while ``nonA_B`` becomes ``nonA_nonB`` and vice versa –, the
    resulting number then becomes the multiplicative inverse and is a measure to
    estimate how much more likely A is under the condition non-B than under B.

    Both a result of ``0`` and a result of :param:`infinity` indicates a lack of
    information about how much more often A occurs with B than without B. Note:

    1. The result ``0`` given as integer ``0`` indicates that both the numerator
       ``A_B * (A_nonB + nonA_nonB)`` (in the transformed equation) and also the
       denominator ``A_nonB * (A_B + nonA_B)`` equal ``0``. – To return ``None``
       here would be more appropriate but would impede sorting unnecessarily.
    2. The result ``0`` given as float ``0.0`` indicates that only the numerator
       equals ``0``, whereas the denominator does not.
    3. The default of :param:`infinity` was chosen as the multiplicative inverse
       of case 2, indicating that only the denominator equals ``0``, whereas the
       numerator does not.

    .. important::
        The resulting number is not to be seen as an absolute measure how much B
        influences A (see Simpsonʼs paradox).

        But when you calculate the percentage ratio for each possible pairing of
        features from the raw data, you can rank the resulting percentage ratios
        for each pairing and obtain how likely influences between which features
        appear comparatively.

        This can be done with :func:`per_ratios`.

    .. note::
        The percentage ratio is often called “relative risk”.

    .. note::
        A similar measure is the “odds ratio”. It is, as a rule, calculated as:
        ``(A_B / nonA_B) / (A_nonB / nonA_nonB)``

        Hence:
        ``(A_B * nonA_nonB) / (nonA_B * A_nonB)``

        It has not a clear meaning in itself like the percentage ratio.

        It fails to deliver an estimation more often than the percentage ratio –
        e.g. ``A_B = 3``, ``nonA_B = 0``, ``A_nonB = 2``, ``nonA_nonB = 4`` lead
        to the percentage ratio ``3 * (2 + 4) / 2 * (3 + 0) == 3`` – whereas the
        odds ratio ``(3 * 4) / (0 * 2)`` does not yield a finite number.

        It yields the same value when you switch A and B.

        It yields the same value when ``A_B + nonA_B`` or ``A_nonB + nonA_nonB``
        does not correspond to 100 percent, but the numbers are *proportionally*
        smaller. Therefore the odds ratio is recommended for cases where you are
        dealing with mere samples in the sense of partial data.
    '''
    numerator = A_B * (A_nonB + nonA_nonB)
    denominator = A_nonB * (A_B + nonA_B)
    return (
            0 if (0 == numerator == denominator)
            else infinity if (denominator == 0)
            else numerator / denominator
            )

def per_ratios(
        cols: 'list[str]',
        matrix: 'np.array',
        rowcounts: 'np.array',
        infinity: float = float('infinity'),
        ) -> '''Generator[tuple[float|int, list[str], list[str]]]''':
    '''
    Get all percentage ratios (see :func:`per_ratio`!) for the given data:

    - :param:`cols` are names (in alphabetical order) for observed features.
    - Each column of :param:`matrix` represents a feature. Each row represents a
      case. (A case is a unique set of features.) Each field of a row contains a
      boolean value. The value tells if the feature represented by the column of
      this field occurred in that case or not.
    - Each value in :param:`rowcounts` corresponds with a row of :param:`matrix`
      and tells how often the case represented by this row occurred.

    First, get all possible pairs of all possible subsets of the given features.
    E.g. for four features numbered from 0 to 3, get the following pairs::

        {0}, {1}
        {0}, {2}
        {0}, {3}
        {1}, {2}
        {1}, {3}
        {2}, {3}
        {0}, {1, 2}
        {0, 1}, {2}
        {0}, {1, 3}
        {0, 1}, {3}
        {0}, {2, 3}
        {0, 2}, {3}
        {1}, {2, 3}
        {1, 2}, {3}
        {0}, {1, 2, 3}
        {0, 1}, {2, 3}
        {0, 1, 2}, {3}

    … and a respective reversed version of each (having the first and the second
    subset switched), i.e. …::

        {1}, {0}
        {2}, {0}
        {3}, {0}
        … and so on until
        {3}, {0, 1, 2}

    For each pair, return the following tuple:

    1. the percentage ratio or – if this is less than ``1`` – its multiplicative
       inverse:

       - The original ratio tells ‘how much more often A occurred when B **did**
         occur’, i.e. P(A|B).
       - The multiplicative inverse tells ‘how much more often A occurred when B
         did **not** occur’, i.e. P(A|¬B).

       Thus all results are floats greater than or equal to ``1`` or the integer
       ``0``. – About the latter case see the docstring of :func:`per_ratio` and
       case 1.

    2. the names (from :param:`cols`) of the features representing A.
    3. the names (from :param:`cols`) of the features representing B.
    4. whether the returned ratio is the multiplicative inverse.
    '''
    colslen = len(cols)
    colnums = list(range(colslen))
    for i in range(2, colslen + 1):
        for combination in combinations(colnums, i):
            combination = list(combination)
            for ii in range(1, len(combination)):
                a = combination[:ii]
                b = combination[ii:]
                ab = a + b
                A_B = np.sum(rowcounts, where = [
                        True if row[ab].all()
                        else False for row in matrix ])
                A_nonB = np.sum(rowcounts, where = [
                        True if row[a].all() and not row[b].any()
                        else False for row in matrix ])
                nonA_B = np.sum(rowcounts, where = [
                        True if not row[a].any() and row[b].all()
                        else False for row in matrix ])
                nonA_nonB = np.sum(rowcounts, where = [
                        True if not row[ab].any()
                        else False for row in matrix ])
                for (
                        first_comb,
                        second_comb,
                        numerator,
                        denominator,
                        ) in (
                            (
                                a,
                                b,
                                A_B * (A_nonB + nonA_nonB),
                                A_nonB * (A_B + nonA_B),
                            ),
                            (
                                b,
                                a,
                                A_B * (nonA_B + nonA_nonB),
                                nonA_B * (A_B + A_nonB),
                            )):
                    inversed = numerator < denominator
                    yield (
                            0 if (0 == numerator == denominator)
                            else infinity if
                                [denominator == 0, numerator == 0][inversed]
                            else denominator / numerator if inversed
                            else numerator / denominator,
                            [ cols[i] for i in first_comb ],
                            [ cols[i] for i in second_comb ],
                            inversed,
                            )

def ratios(
        cells: 'dict[Hashable, dict[Hashable, float]]',
        cases: 'Sequence[Hashable]',
        default: int = 0
        ) -> 'dict[Hashable, dict[Hashable, None|float]]':
    '''
    For the values in :param:`cells`, get ratios.
    Each divisor is the total sum over :param:`cases` (see the testcode below).
    If such a total sum is ``0``, the ratio is ``None``.

    >>> import calc
    >>> cells = {
    ...     'case a': {'txt1': 50, 'txt2': 100},
    ...     'case b': {'txt1': 150, 'txt2': 400},
    ...     'case c': {}}
    >>> cases = deque(['case a', 'case b', 'case c'])
    >>> ratios = calc.ratios(cells, cases)
    >>> for case in sorted(ratios.keys()):
    ...     print(case + ':')
    ...     print(sorted(ratios[case].items()))
    case a:
    [('txt1', 0.25), ('txt2', 0.2)]
    case b:
    [('txt1', 0.75), ('txt2', 0.8)]
    case c:
    [('txt1', 0.0), ('txt2', 0.0)]
    '''
    variants = set( var for cell in cells.values() for var in cell.keys() )
    totals = { var: sum( abs(cells[case].get(var, default)) for case in cases )
            for var in variants }
    return { case:
            { var: (cells[case].get(var, default) / totals[var])
                if totals[var] else None for var in variants }
            for case in cases }

def table_of_derived_cell_values(
        table: 'deque[deque[dict]]',
        derive: 'ty.Callable[[dict], dict]' = mad_mean,
        ) -> 'deque[deque[dict]]':
    return deque( deque( derive(cell.values())
            for cell in row ) for row in table )

def table_of_totals(
        tables: 'dict[Hashable, deque[deque[dict]]]'
        ) -> 'deque[deque[dict]]':
    '''
    Get a table which is structured like one in :param:`tables` and
    contains as cell values a dictionary like ``{'total': x}`` with
    x being the total sum of the corresponding cells over all cases.

    :param tables: to be built by :func:`.tabulate`.
    '''
    return deque( deque( {'total': sum( value
            for cell in cells for value in cell.values() ) }
                for cells in zip(*rows) )
                    for rows in zip(*tables.values()) )

def tables_of_case_ratios(
        tables:
            'dict[Hashable, deque[deque[dict[Hashable, float]]]]'
        ) -> 'dict[Hashable, deque[deque[dict[Hashable, None|float]]]]':
    '''
    Take a table like the ones built by :func:`.tabulate` and derive
    a table that contains ratios instead of the absolute cell values.
    Each ratio is the ratio of the absolute value of one case to the
    sum of the corresponding absolute values of all cases.
    If this sum is ``0``, the ratio is ``None``.

    >>> import calc
    >>> data = {
    ...         ('case a', '1200', 'bair.', 'txt1', 'ycx'): 50,
    ...         ('case a', '1200', 'bair.', 'txt2', 'xxc'): 25,
    ...         ('case a', '1200', 'bair.', 'txt2', 'xxy'): 25,
    ...         ('case a', '1200', 'alem.', 'txt3', 'xxx'): 10,
    ...         ('case a', '1200', 'alem.', 'txt4', 'xcc'): 20,
    ...         ('case a', '1250', 'bair.', 'txt5', 'xyc'): 100,
    ...         ('case a', '1250', 'alem.', 'txt6', 'cxc'): 10,
    ...         ('case b', '1200', 'bair.', 'txt1', 'yyy'): 150,
    ...         ('case b', '1200', 'bair.', 'txt2', 'xxc'): 75,
    ...         ('case b', '1200', 'bair.', 'txt2', 'xxy'): 75,
    ...         ('case b', '1200', 'alem.', 'txt3', 'ycy'): 30,
    ...         ('case b', '1200', 'alem.', 'txt4', 'ycc'): 60,
    ...         ('case b', '1250', 'bair.', 'txt5', 'yyx'): 300,
    ...         ('case b', '1250', 'alem.', 'txt6', 'ccy'): 40,
    ...         ('case c', '1250', 'bair.', 'txt5', 'yyx'): 5,
    ...         }
    >>> matchtable = (
    ...         (('1200', 'bair.'), ('1200', 'alem.')),
    ...         (('1250', 'bair.'), ('1250', 'alem.')),
    ...         )
    >>> tables, total = calc.tabulate(data, matchtable)
    >>> print(total)
    975
    >>> ratio_tables = tables_of_case_ratios(tables)
    >>> for case in sorted(ratio_tables.keys()):
    ...     print(case + ':')
    ...     for row in ratio_tables[case]:
    ...         for cell in row:
    ...             print(sorted(cell.items()))
    case a:
    [('txt1', 0.25), ('txt2', 0.25)]
    [('txt3', 0.25), ('txt4', 0.25)]
    [('txt5', 0.24691358024691357)]
    [('txt6', 0.2)]
    case b:
    [('txt1', 0.75), ('txt2', 0.75)]
    [('txt3', 0.75), ('txt4', 0.75)]
    [('txt5', 0.7407407407407407)]
    [('txt6', 0.8)]
    case c:
    [('txt1', 0.0), ('txt2', 0.0)]
    [('txt3', 0.0), ('txt4', 0.0)]
    [('txt5', 0.012345679012345678)]
    [('txt6', 0.0)]
    '''
    cases = deque(tables.keys())
    ratio_tables = { case: deque() for case in cases }
    for r, row in enumerate(tables[cases[0]]):
        for case in cases:
            ratio_tables[case].append(deque())
        for c in range(len(row)):
            case_ratios = ratios(
                    { case: tables[case][r][c] for case in cases },
                    cases)
            for case in cases:
                ratio_tables[case][-1].append(case_ratios[case])
    return ratio_tables

def tabulate(
        data: 'dict[Sequence[Hashable], float]',
        matchtable: 'Iterable[Iterable[Hashable]]',
        casegetter: itemgetter = itemgetter(0),
        matchgetter: itemgetter = itemgetter(1, 2),
        variantgetter: itemgetter = itemgetter(3),
        default_factory: 'ty.Callable[[], float]' = lambda: 0,
        total: float = 0,
        ) -> '''tuple[
                    dict[
                        Hashable,               # a case
                            deque[              # a table
                                deque[          # a row
                                    Defaultdict # a cell
                                    ]]],
                    int]''':
    '''
    Get a dictionary of tables and a total sum from :param:`data`.

    The dictionary consists of keys and values; the tables are the values,
    the so-called cases are the keys of the dictionary. The cases are drawn
    from the keys of :param:`data` by :param:`casegetter`. The shape of each
    table (i.e. the number of rows and the number of cells of each row) is
    determined by :param:`matchtable`. And the cells are filled as follows:
    For each item in :param:`data`, the key tells:

    - which table is concerned: ``casegetter(key)``,
    - which cell of :param:`matchtable` corresponds: ``matchgetter(key)``,
    - which subcell of the cell is concerned: ``variantgetter(key)``.

    All values are added to :param:`total`, and the result is returned.

    All values for which the table, the cell and the subcell are equal, are
    **added up**, and the result of this addition becomes the value of the
    subcell.

    The key of the subcell is the outcome of ``variantgetter(key)``.

    The cell is a `collections.defaultdict` of such subcells. **Thus, beware
    of unintentionally adding keys afterward.**

    The row is a `collections.deque` of such cells.
    The table is a deque of such rows.

    :param default_factory: returns a starting value for each subcell.

    >>> import calc
    >>> data = {
    ...         ('case a', '1200', 'bair.', 'txt1', 'ycx'): 50,
    ...         ('case a', '1200', 'bair.', 'txt2', 'xxc'): 25,
    ...         ('case a', '1200', 'bair.', 'txt2', 'xxy'): 25,
    ...         ('case a', '1200', 'alem.', 'txt3', 'xxx'): 10,
    ...         ('case a', '1200', 'alem.', 'txt4', 'xcc'): 20,
    ...         ('case a', '1250', 'bair.', 'txt5', 'xyc'): 100,
    ...         ('case a', '1250', 'alem.', 'txt6', 'cxc'): 10,
    ...
    ...         ('case b', '1200', 'bair.', 'txt1', 'yyy'): 150,
    ...         ('case b', '1200', 'bair.', 'txt2', 'xxc'): 75,
    ...         ('case b', '1200', 'bair.', 'txt2', 'xxy'): 75,
    ...         ('case b', '1200', 'alem.', 'txt3', 'ycy'): 30,
    ...         ('case b', '1200', 'alem.', 'txt4', 'ycc'): 60,
    ...         ('case b', '1250', 'bair.', 'txt5', 'yyx'): 300,
    ...         ('case b', '1250', 'alem.', 'txt6', 'ccy'): 40,
    ...
    ...         ('case c', '1250', 'bair.', 'txt5', 'yyx'): 5,
    ...         }
    >>> matchtable = (
    ...         (('1200', 'bair.'), ('1200', 'alem.')),
    ...         (('1250', 'bair.'), ('1250', 'alem.')),
    ...         )
    >>> tables, total = calc.tabulate(data, matchtable)
    >>> print(total)
    975
    >>> for case in sorted(tables.keys()):
    ...     print(case + ':')
    ...     for row in tables[case]:
    ...         for cell in row:
    ...             print(sorted(cell.items()))
    case a:
    [('txt1', 50), ('txt2', 50)]
    [('txt3', 10), ('txt4', 20)]
    [('txt5', 100)]
    [('txt6', 10)]
    case b:
    [('txt1', 150), ('txt2', 150)]
    [('txt3', 30), ('txt4', 60)]
    [('txt5', 300)]
    [('txt6', 40)]
    case c:
    []
    []
    [('txt5', 5)]
    []
    '''
    data = data.items()
    # Provide an empty deque as the table for each case
    tables = { casegetter(key): deque() for key, _ in data }
    for matchrow in matchtable:
        for case in tables:
            # Provide an empty deque as the next row
            tables[case].append(deque())
        for matchcell in matchrow:
            for case in tables:
                # Provide an empty defaultdict as the next cell
                tables[case][-1].append(defaultdict(default_factory))
            for key, value in data:
                # Sum up all values of matching keys, but separated by variant
                if matchgetter(key) == matchcell:
                    tables[casegetter(key)][-1][-1][variantgetter(key)] += value
                    total += value
    return tables, total

if __name__ == '__main__':
    import doctest
    doctest.testmod()
