import os
import sys
topdirpath = os.path.abspath(f'{__file__}/../..')
if topdirpath not in sys.path[1:2]:
    sys.path.insert(1, topdirpath)
