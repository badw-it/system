# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
Small editor for the annotation of plain text. The annotation is done via a
hovering window, showing one or more lists, whose items are the allowed
annotation values. The lists are made and curated independently.
'''
#### TODO: Ensure that (for guess_item) maxlen > 0 and
####       0.0 <= cutoff <= 1.0
#### TODO: special chars: Veronika-table: clickable chars in tk.Text.
#### TODO: for HTML: paragraph-tagging: insert widget in the beginning
####       of every paragraph to click tag and class-attribute?
#### TODO: for HTML / else: inline-tagging: with Serpentina, but other
####       TAG_PREFIXes? And different formatting (e.g. background)?
#### TODO: during long-term processing (guess_item), give
####       a hint at the ongoing processing, so that the user does not assume
####       a crash of the program.
#### TODO: acceleration possible?:
####           from concurrent import futures
####           with futures.ProcessPoolExecutor() as executor:
####               tuple(executor.map(function, args))
####           # instead of:
####           tuple(map(function, args))
import difflib
import heapq
import html
import json
import os
try: import regex as re
except ImportError: import re
import subprocess
import sys
import tkinter as tk
import tkinter.filedialog as tkfiledialog
import tkinter.font as tkfont
import tkinter.messagebox as tkmessagebox
import tkinter.simpledialog as tksimpledialog
import tkinter.ttk as ttk
import urllib.parse
import webbrowser
import xml.etree.ElementTree as ET
from _tkinter import TclError
from collections import defaultdict
from collections import deque
from collections.abc import Iterable
from decimal import Decimal as Dec
from functools import partial
from operator import itemgetter

import __init__
import fs
import parse
import web_io
import xmlhtml
from gui import lay
from gui import lay_scrollbars
from gui import get_tlabel_font_text_width
from gui import get_toplevel_decoration_height
from gui import get_toplevel_decoration_width
from structs import DefaultkeyDict

TAG_PREFIX = '#'

class Editor(tk.Tk):

    maxlen = 5
    cutoff = 0.3

    def __init__(self):
        self.site_id = 'anselmus'
        self.dirpath = os.path.dirname(os.path.abspath(__file__)) + os.sep
        self.last_dirpath = os.path.abspath(self.dirpath + '../../../app/data')

        config_path          = self.dirpath + 'config/config.ini'
        url_templates_path   = self.dirpath + 'config/websearch_templates'
        self.bindings_path   = self.dirpath + 'config/bindings.json'
        self.tk_styles_path  = self.dirpath + 'config/tk_styles.json'
        self.ttk_styles_path = self.dirpath + 'config/ttk_styles.json'
        icon_path            = self.dirpath + 'icons/favicon.ico'
        glosses_path         = self.last_dirpath + '/glosses.ini'

        config = fs.get_config([config_path])
        self.lang_ids = config['ids'].getlist('langs')
        self.lang_id = config['ids']['lang']
        if self.lang_id not in self.lang_ids:
            self.lang_ids.append(self.lang_id)
        self.glosses = web_io.read_glosses([glosses_path], self.lang_id)

        self.url_template  = ''
        self.url_templates = tuple(xmlhtml.elem_to_tree(
                ET.parse(url_templates_path)).items())

        self.archivarius  = None
        self.search_term  = ''
        self.replace_term = ''

        # Container for current tagging, with items like ``lemma: is_selected``:
        self.tagging          = {}
        self.muse             = None
        self.annotation_frame = None
        self.register_frame   = None

        super().__init__()
        self.protocol('WM_DELETE_WINDOW', self.end)
        self.update_idletasks()
        self.iconbitmap(default = icon_path)
        self.title('')
        self.bind()
        self.style()

        width = (self.winfo_screenwidth() // 2 -
                get_toplevel_decoration_width(self))
        height = (self.winfo_screenheight() // 2 -
                get_toplevel_decoration_height(self))
        self.geometry('{}x{}+{}+{}'.format(width, height, 0, height - 40))
        self.update_idletasks()

        self.path = os.path.normcase(sys.argv[1]) if (
                len(sys.argv) > 1 and isinstance(sys.argv[1], str)
                ) else ''
        self.lay_text()
        self.lay_menu()

    def bind(self):
        with open(self.bindings_path, 'r', encoding = 'utf-8') as infile:
            bindings = json.loads(infile.read())
        self.bindings = {}
        for key, val in bindings.items():
            self.bindings[key] = []
            for trigger, callback, kwargs, flag in val:
                if flag == '-':
                    if key == '':
                        self.unbind_all(trigger)
                    else:
                        self.unbind_class(key, trigger)
                else:
                    callback = getattr(self, callback)
                    self.bindings[key].append((trigger, callback, kwargs, flag))
                    if key == '':
                        self.bind_all(
                                trigger, partial(callback, **kwargs), flag)
                    else:
                        self.bind_class(key,
                                trigger, partial(callback, **kwargs), flag)

    def change_annotation_on_button_1(self, lemma, status):
        if self.textselection_exists():
            self.deannotate()
            self.tagging[lemma] = status.get()
            self.set_tag(get_tag_id(self.tagging), 'sel.first', 'sel.last')

    def change_annotation_on_button_3(self, event):
        if self.textselection_exists():
            self.deannotate()
            self.tagging = { tag_id: False for tag_id in self.tagging }
            self.tagging[event.widget.cget('text')] = True
            self.set_tag(get_tag_id(self.tagging), 'sel.first', 'sel.last')
            self.lay_annotation_frame()

    def close_register(self, subframe):
        subframe.destroy()

    def deannotate(self):
        sel_range = self.text.tag_ranges('sel')
        if sel_range:
            tag_id = get_tag_id(self.tagging)
            self.text.tag_remove(tag_id, 'sel.first', 'sel.last')
            if not self.text.tag_ranges(tag_id):
                self.text.tag_delete(tag_id)
            self.text.edit_modified(1)

    def delete_lemma(self, lemma, status):
        if self.textselection_exists():
            if not status.get():
                self.deannotate()
                del self.tagging[lemma]
                if self.tagging:
                    self.set_tag(get_tag_id(self.tagging), 'sel.first',
                            'sel.last')
                self.lay_muse()

    def end(self):
        if self.text.edit_modified():
            answer = tkmessagebox.Message(
                    title = '“[…] ist Ihre Tusche auch nicht haltbar”',
                    message = self.glosses['save?'][self.lang_id],
                    icon = 'question', type = 'yesnocancel').show()
            if answer == 'cancel':
                return
            elif answer == 'yes':
                self.save()
        self.destroy()

    def end_archivarius(self):
        self.archivarius.destroy()
        self.archivarius = None

    def end_muse(self):
        self.muse.destroy()
        self.muse = None

    def export(self):
        path = tkfiledialog.asksaveasfilename(
                defaultextension = '.xml',
                initialdir = self.last_dirpath,
                title = '“meine schöne englische Cursivschrift”',
                )
        if path:
            path = os.path.normcase(path)
            self.last_dirpath = os.path.dirname(path) if path else \
                    self.last_dirpath
            with open(path, 'w', encoding = 'utf-8') as file:
                file.write('<?xml version="1.0" encoding="UTF-8"?>\n<text>')
                for kind, value, index in self.text.dump('1.0', 'end',
                        tag = True, text = True):
                    value = html.escape(value, quote = False)
                    if kind == 'text':
                        file.write(value)
                    else:
                        if value.startswith(TAG_PREFIX):
                            if kind == 'tagon':
                                file.write('<start>{}</start>'.format(value))
                            elif kind == 'tagoff':
                                file.write('<end>{}</end>'.format(value))
                file.write('</text>')

    def generate_variant_register(self):
        register = self.get_register('select_variant_register_optional', 4)
        if not register:
            register = self.get_register('select_norm_register', 2)
            register = { item_id: {1: {item_id: 1}}
                    for item_id, _ in register.items() }
        if not register:
            return
        dirpath = os.path.normcase(tkfiledialog.askdirectory(
                initialdir = self.last_dirpath,
                title = self.glosses['select_corpus'][self.lang_id],
                mustexist = 1))
        if not dirpath:
            return
        self.last_dirpath = os.path.abspath(dirpath + '/..')
        new_variant_register = {}
        inpaths = fs.get_paths(dirpath)
        for inpath in inpaths:
            try:
                with open(inpath, 'r', encoding = 'utf-8') as infile:
                    text = infile.read()
            except UnicodeDecodeError:
                reaction = say('{}, {}:\n\t{}'.format(inpath, encoding,
                            self.glosses['bad_coding'][self.lang_id]),
                        title = '“schnöde Hahnenfüße”',
                        icon = 'error',
                        type = 'okcancel')
                if reaction == 'cancel':
                    return
            else:
                for term, (_, _), (_, _) in iterterms(text):
                    try:
                        new_variant_register[term] += 1
                    except KeyError:
                        new_variant_register[term] = 1
        total = Dec(sum(new_variant_register.values()))
        for term in new_variant_register:
            pivots_priors = guess_item(term.split()[0],
                    register = register,
                    maxlen = self.maxlen,
                    cutoff = self.cutoff)
            new_variant_register[term] = {
                    Dec(new_variant_register[term]) / total:
                    dict(pivots_priors)}
        path = tkfiledialog.asksaveasfilename(
                defaultextension = '',
                initialdir = self.last_dirpath,
                title = self.glosses['save_as'][self.lang_id])
        if path:
            self.last_dirpath = os.path.dirname(path)
            table = xmlhtml.table_to_tags(
                    xmlhtml.tree_to_table(new_variant_register),
                    table_attrs = {'class': 'txt'})
            with open(path, 'w', encoding = 'utf-8') as file:
                try:
                    file.write(next(table))
                    file.write('\n<meta charset="utf-8"/>')
                except StopIteration:
                    file.write('')
                for item in table:
                    file.write(item)
        say(self.glosses['finished'][self.lang_id] + '\n' + path, title =
                self.glosses['generate_variant_register'][self.lang_id],
                icon = 'info')

    def get_chars_and_tags(self):
        if self.path:
            encoding = 'utf-8-sig'
            while True:
                try:
                    with open(self.path, encoding = encoding) as file:
                        text = file.read()
                    break
                except (UnicodeDecodeError, LookupError):
                    tkmessagebox.Message(
                            title = '“schnöde Hahnenfüße”',
                            message = self.glosses['bad_coding'][self.lang_id],
                            icon = 'info', type = 'ok').show()
                    encoding = tksimpledialog.askstring(
                            '“mit geübter Hand schwarz überziehen”',
                            self.glosses['enter_coding'][self.lang_id],
                            initialvalue = encoding)
                    if not encoding:
                        chars = ''
                        tags = ()
                        break
            chars, tags = open_as_json(text, partial(say,
                    self.glosses['unreadable'][self.lang_id],
                    title = '“sonderbare krause Züge und Schnörkel”'))
        else:
            chars = ''
            tags = ()
        return chars, tags

    def get_register(self, label, colnum):
        path = os.path.normcase(tkfiledialog.askopenfilename(
                initialdir = self.last_dirpath,
                title = self.glosses[label][self.lang_id]))
        self.last_dirpath = os.path.dirname(path) if path else self.last_dirpath
        try:
            register = xmlhtml.table_to_tree(crop_table(extract_table(path), colnum))
        except Exception:
            say(self.glosses['unreadable'][self.lang_id],
                    title = '“sonderbare krause Züge und Schnörkel”')
            register = {}
        return register

    def inform(self):
        self.information = tk.Toplevel()
        self.information.attributes('-alpha',0.9)
        self.information.title('“Famulus”')
        self.information.frame = lay(self.information, ttk.Frame, 0, 0, 1, 1,
                {'style': 'TFrame'}, xweight = 1, yweight = 1)
        self.information.text = lay(self.information.frame, tk.Text, 0, 0, 1, 1,
                self.tk_styles['information'], xweight = 1, yweight = 1)
        lay_scrollbars(self.information.frame, self.information.text)
        information = ( (
                key,
                re.sub('(Key-|<|>)', '', trigger),
                re.sub('_on_.+$', '', callback.__name__),
                kwargs.get('val', ''),
                ) for key in self.bindings.keys()
                for (trigger, callback, kwargs, _) in self.bindings[key] )
        information = [ (
                key if key else '(passim)',
                trigger,
                (self.glosses[callback][self.lang_id]
                    if callback in self.glosses else callback)
                + (': ' + val if val else '')
                ) for key, trigger, callback, val in information ]
        information = [[
                self.glosses['domain'][self.lang_id],
                self.glosses['shortcut'][self.lang_id],
                self.glosses['function'][self.lang_id]]] + information
        tabwidth = str(max(( tkfont.Font(font = self.information.text['font']
                ).measure(shortcut) for _, shortcut, _ in information )) + 10)
        information = '\n'.join( '\t'.join(record) for record in information )
        self.information.text.insert('1.0', information)
        self.information.text.tag_add('title', '1.0', '2.0')
        self.information.text.tag_configure('title', spacing3 = 10,
                underline = 1)
        self.information.text.configure(state = 'disabled', tabs = tabwidth)

    def insert_on_event(self, event, val = ''):
        widget = event.widget
        if widget.winfo_class() == 'Text':
            if widget.tag_ranges('sel'):
                widget.delete('sel.first', 'sel.last')
        try:
            widget.insert('insert', val)
        except AttributeError:
            pass

    def lay_annotation_frame(self):
        width = self.muse.winfo_width()
        height = self.muse.winfo_height()
        if self.annotation_frame:
            self.annotation_frame.destroy()
        self.annotation_frame = lay(self.muse, ttk.Labelframe, 1, 0, 1, 1,
                {'text': self.glosses['annotation'][self.lang_id]},
                xweight = 1, yweight = 0)
        for row, (lemma, is_selected) in enumerate(self.tagging.items()):
            status = tk.IntVar()
            status.set(int(is_selected))
            lay(self.annotation_frame, ttk.Checkbutton, row, 0, 1, 1,
                    {'command': partial(self.change_annotation_on_button_1,
                                        lemma, status),
                     'style': 'TCheckbutton', 'text': lemma,
                     'variable': status}, xweight = 1, yweight = 1, padx = 4
                    ).bindtags(('TCheckbutton', 'MuseCheckbutton', '.', 'all'))
            lay(self.annotation_frame, ttk.Button, row, 1, 1, 1,
                    {'command': partial(self.delete_lemma, lemma, status),
                     'style': 'Button.TLabel',
                     'text': self.glosses['delete'][self.lang_id]})
        # So that the muse window is resized according to its new content:
        self.muse.wm_geometry('')
        self.muse.update_idletasks()
        if self.muse.winfo_width() < width or self.muse.winfo_height() < height:
            self.muse.wm_geometry(str(width) + 'x' + str(height))

    def lay_archivarius(self):
        if not self.archivarius:
            self.archivarius = tk.Toplevel()
            self.archivarius.protocol('WM_DELETE_WINDOW', self.end_archivarius)
            self.archivarius.title('Archivarius')
            self.archivarius.wm_attributes('-alpha', 0.95)
            search_frame = lay(self.archivarius, ttk.Frame, 0, 0, 1, 1)
            options_frame = lay(self.archivarius, ttk.Frame, 1, 0, 1, 1)
            websearch_frame = lay(self.archivarius, ttk.Frame, 2, 0, 1, 1)
            replace_frame = lay(self.archivarius, ttk.Frame, 3, 0, 1, 1)
            search_item = lay(search_frame, tk.Entry, 0, 0, 1, 1,
                    self.tk_styles['item'], xweight = 1, padx = 4, pady = 4)
            options = {
                    'wrap_around': True,
                    'backwards': False,
                    'no_case': True,
                    'regex': False,
                    }
            for column, (name, is_selected) in enumerate(options.items()):
                options[name] = lay(options_frame, ttk.Checkbutton,
                        0, column, 1, 1,
                        {'style': 'TCheckbutton',
                        'text': self.glosses[name][self.lang_id]},
                        xweight = 1, yweight = 1, padx = 4)
                options[name].status = tk.IntVar()
                options[name].status.set(int(is_selected))
                options[name].configure(variable = options[name].status)
            self.lay_archivarius_button(search_frame, 0, 1,
                    partial(self.search_in_text, search_item, **options),
                    self.glosses['search'][self.lang_id])
            self.lay_archivarius_button(search_frame, 0, 2,
                    partial(self.search_in_text, search_item,
                    in_tags = True, **options),
                    self.glosses['search_annotation'][self.lang_id])
            search_item.delete('0', 'end')
            search_item.insert('0', self.search_term)
            search_item.focus()
            for column, (label, url_template) in enumerate(self.url_templates):
                self.lay_archivarius_button(websearch_frame, 0, column,
                        partial(self.search_in_web, search_item, url_template),
                        label)
            replace_item = lay(replace_frame, tk.Entry, 0, 0, 1, 1,
                    self.tk_styles['item'], xweight = 1, padx = 4, pady = 4)
            self.lay_archivarius_button(replace_frame, 0, 1,
                    partial(self.replace_in_text, replace_item),
                    self.glosses['replace'][self.lang_id])
            replace_item.delete('0', 'end')
            replace_item.insert('0', self.replace_term)
        self.archivarius.deiconify()
        self.archivarius.lift()

    def lay_archivarius_button(self, parent, row, column, command, text):
        lay(parent, ttk.Button, row, column, 1, 1,
                {'command': command, 'style': 'Button.TLabel', 'text': text},
                padx = 4, pady = 4, sticky = 'nw')

    def lay_archivarius_on_event(self, event):
        term = get_term(event)
        if term:
            self.search_term = term
        self.lay_archivarius()

    def lay_menu(self):
        menu = tk.Menu(self)
        menu.add_command(command = self.end,
                label = self.glosses['end'][self.lang_id])
        menu.add_command(command = new,
                label = self.glosses['new'][self.lang_id])

        menu.add_command(command = self.open,
                label = self.glosses['open'][self.lang_id])
        menu.add_command(command = self.save,
                label = self.glosses['save'][self.lang_id])
        menu.add_command(command = self.save_as,
                label = self.glosses['save_as'][self.lang_id])
        menu.add_command(command = self.export,
                label = self.glosses['export'][self.lang_id])
        menu.add_command(command = partial(self.undo, None),
                label = self.glosses['undo'][self.lang_id])
        menu.add_command(command = partial(self.redo, None),
                label = self.glosses['redo'][self.lang_id])
        menu.add_command(command = self.lay_archivarius,
                label = self.glosses['search'][self.lang_id])

        menu_annotation = tk.Menu(self)
        menu.add_cascade(menu = menu_annotation,
                label = self.glosses['annotation'][self.lang_id])
        menu_annotation.add_command(command = self.preannotate,
                label = self.glosses['automatic'][self.lang_id])
        menu_annotation.add_command(command = self.lay_muse,
                label = self.glosses['manual'][self.lang_id])
        menu_annotation.add_command(command = self.generate_variant_register,
                label = self.glosses['generate_variant_register'][self.lang_id])

        menu_settings = tk.Menu(self)
        menu.add_cascade(menu = menu_settings,
                label = self.glosses['settings'][self.lang_id])

        menu_settings_lang = tk.Menu(self)
        menu_settings.add_cascade(menu = menu_settings_lang,
                label = self.glosses['language'][self.lang_id])
        for lang_id in self.lang_ids:
            menu_settings_lang.add_radiobutton(
                    command = partial(self.set_lang, lang_id),
                    label = self.glosses['lang_name'][lang_id])

        menu.add_command(command = self.inform,
                label = self.glosses['user_guide'][self.lang_id])

        menu_license = tk.Menu(self)
        menu.add_cascade(menu = menu_license,
                label = self.glosses['license'][self.lang_id])
        url = 'http://www.apache.org/licenses/LICENSE-2.0'
        menu_license.add_command(command = partial(
                webbrowser.open, url, new = 0, autoraise = True),
                label = 'Attribution notice: by Stefan Müller in 2013 ff. '\
                        'Licensed under ' + url)
        self.config(menu = menu)

    def lay_muse(self, tag_range = ()):
        if not self.muse:
            self.muse = tk.Toplevel()
            self.muse.protocol('WM_DELETE_WINDOW', self.end_muse)
            self.muse.title('Serpentina')
            self.muse.attributes('-alpha',0.99)
            self.register_frame = lay(self.muse, ttk.Labelframe, 0, 0, 1, 1,
                    {'text': self.glosses['annotation_lexicon'][self.lang_id]},
                    xweight = 1, yweight = 1)
            self.lay_register_button(self.register_frame, 0)
        self.lay_annotation_frame()
        self.muse.deiconify()
        self.muse.lift()
        if tag_range:
            if self.text.tag_ranges('sel'):
                self.text.tag_remove('sel', 'sel.first', 'sel.last')
            self.text.tag_add('sel', *tag_range)
            self.text.tag_raise('sel')
            self.text.mark_set('insert', tag_range[0])
        visible_cursor = self.text.bbox('insert')
        if visible_cursor:
            curs_x, curs_y, _, curs_height = visible_cursor
            new_x_abs = self.text.winfo_rootx() + curs_x
            # Laid out above the cursor:
            new_y_abs = self.text.winfo_rooty() + curs_y - (
                    # raw height of the toplevel window:
                    self.muse.winfo_height() +
                    # height of the decoration of the toplevel window:
                    get_toplevel_decoration_height(self.muse) +
                    # a margin:
                    6)
            self.muse.geometry('+{}+{}'.format(new_x_abs, new_y_abs))
            self.muse.update_idletasks()

    def lay_muse_on_enter(self, tag_id, event):
        self.tagging = get_tagging(tag_id)
        tag_range = self.text.tag_prevrange(tag_id, 'current+1c')
        if tag_range:
            self.lay_muse(tag_range = tag_range)

    def lay_muse_on_press(self, event):
        self.tagging = {}
        if self.text.tag_ranges('sel'):
            for tag_id in self.text.tag_names('sel.first'):
                ranges = iter(self.text.tag_ranges(tag_id))
                if ('sel.first', 'sel.last') in zip(ranges):
                    self.tagging = get_tagging(tag_id)
                    break
            self.lay_muse()

    def lay_register(self, column, old_open_button):
        path = os.path.normcase(tkfiledialog.askopenfilename(
                initialdir = self.last_dirpath,
                title = '“wo Bhogovotgitas Meister unsrer warten”'))
        self.last_dirpath = os.path.dirname(path) if path else self.last_dirpath
        try:
            register = tuple( lemma.replace('\x1e', '').replace('\x1f', '')
                    for lemma in xmlhtml.table_to_tree(
                        crop_table(extract_table(path), 2)).values()
                    if lemma )
        except Exception:
            say(self.glosses['unreadable'][self.lang_id],
                    title = '“sonderbare krause Züge und Schnörkel”')
        else:
            old_open_button.destroy()
            subframe = lay(self.register_frame, ttk.Frame,
                    0, column, 1, 1, padx = (6, 12), xweight = 1, yweight = 1)
            text = self.glosses['close_register'][self.lang_id]
            lay(subframe, ttk.Button, 0, 0, 1, 2,
                    {'style': 'Button.TLabel',
                     'command': partial(self.close_register, subframe),
                     'text': text}, xweight = 1, pady = 4,
                    xminsize = get_tlabel_font_text_width(text) + 20)
            listbox = lay(subframe, tk.Listbox, 1, 0, 1, 1,
                    self.tk_styles['register'], xweight = 1, yweight = 1)
            listbox.bindtags(('Listbox', 'MuseListbox', '.', 'all'))
            lay_scrollbars(subframe, listbox, x_offset = 1)
            lay(subframe, tk.Entry, 3, 0, 1, 2,
                    self.tk_styles['item_not_found'], xweight = 1, pady = 4
                    ).bindtags(('Entry', 'MuseItem', '.', 'all'))
            self.lay_register_button(self.register_frame, column + 1)
            for lemma in register:
                listbox.insert('end', lemma)
            # So that the muse window is resized according to its new content:
            self.muse.wm_geometry('')
            self.lay_muse()

    def lay_register_button(self, parent, column):
        button = lay(parent, ttk.Button, 0, column, 1, 2,
                {'style': 'Button.TLabel',
                 'text': self.glosses['open_register'][self.lang_id]},
                padx = 4, pady = 4, sticky = 'n')
        button.configure(command = partial(self.lay_register, column, button))

    def lay_text(self):
        chars, tags = self.get_chars_and_tags()
        self.frame = lay(self, ttk.Frame, 0, 0, 1, 1,
                {'style': 'TFrame'}, xweight = 1, yweight = 1)
        self.text = lay(self.frame, tk.Text, 0, 0, 1, 1,
                self.tk_styles['text'], xweight = 1, yweight = 1)
        self.text.bindtags(('Text', 'MainText', '.', 'all'))
        lay_scrollbars(self.frame, self.text)
        self.text.insert('end', chars)
        for tag_id, start_index, end_index in tags:
            self.set_tag(tag_id, start_index, end_index)
        self.text.edit_modified(0)
        self.title(os.path.basename(self.path) + ' · ' + self.path)

    def open(self):
        path = os.path.normcase(tkfiledialog.askopenfilename(
                initialdir = self.last_dirpath,
                title = '“in einem hohen, rings mit Bücherschränken ' \
                'umstellten Zimmer”'))
        if path:
            self.last_dirpath = os.path.dirname(path)
            if self.path or self.text.edit_modified():
                new(path)
            else:
                self.path = path
                self.lay_text()

    def preannotate(self):
        variant_register = self.get_register('select_variant_register_optional',
                4)
        norm_register = self.get_register('select_norm_register', 2)
        if norm_register:
            answer = say(self.glosses['delete_old_annotation?'][self.lang_id],
                    title = '“ins Kristall bald Dein Fall – ins Kristall!”',
                    type = 'yesnocancel')
            if answer == 'cancel':
                return
            elif answer == 'yes':
                for tag_id in self.text.tag_names():
                    if tag_id.startswith(TAG_PREFIX):
                        self.text.tag_delete(tag_id)
            text = self.text.get('1.0', 'end')
            for term, (startline, startchar), (endline, endchar) \
                    in iterterms(text):
                startindex = '{}.{}'.format(startline + 1, startchar)
                if not [ tag_id for tag_id in self.text.tag_names(startindex) if
                        tag_id.startswith(TAG_PREFIX) ]:
                    self.text.see('{}.{}'.format(startline, startchar))
                    self.update_idletasks()
                    lemmata_priors = [ (norm_register.get(pivot, ''), prior)
                            for pivot, prior in guess_item(
                                term.split()[0],
                                register = variant_register,
                                maxlen = self.maxlen,
                                cutoff = self.cutoff) ]
                    tagging = {}
                    for lemma, _ in lemmata_priors:
                        if lemma:
                            tagging[lemma] = False
                    if tagging:
                        tagging[tuple(tagging.keys())[0]] = True
                        self.set_tag(get_tag_id(tagging), startindex,
                                '{}.{}'.format(endline + 1, endchar))

    def redo(self, _):
        try:
            self.text.edit_redo()
        except TclError:
            pass

    def replace_in_text(self, replace_item):
        if replace_item:
            self.replace_term = replace_item.get()
        if self.textselection_exists():
            self.text.replace('sel.first', 'sel.last', self.replace_term)

    def save(self):
        if self.text.edit_modified():
            if self.path and os.path.exists(self.path):
                with open(self.path, 'w', encoding = 'utf-8') as file:
                    file.write(json.dumps(self.text.dump('1.0', 'end',
                            tag = True, text = True), ensure_ascii = False,
                            separators = (',', ':')))
                self.text.edit_modified(0)
            else:
                self.save_as()

    def save_as(self):
        path = tkfiledialog.asksaveasfilename(
                defaultextension = '.json',
                initialdir = self.last_dirpath,
                title = '“die geheimnisvolle Tinte floß rabenschwarz ' \
                        'und gefügig”',
                )
        if path:
            self.last_dirpath = os.path.dirname(path)
            self.path = os.path.normcase(path)
            self.text.edit_modified(1)
            self.title(os.path.basename(self.path) + ' · ' + self.path)
            # Make the file here; test in :meth:`save`, if it exists.
            try:
                os.makedirs(os.path.dirname(self.path), exist_ok = True)
            except OSError:
                pass
            with open(self.path, 'w', encoding = 'utf-8') as file:
                file.write('')
            self.save()

    def save_on_event(self, event):
        self.save()

    def search_in_text(self, search_item, wrap_around = True,
            backwards = False, no_case = True, regex = False,
            in_tags = False):
        wrap_around = get_option(wrap_around)
        backwards = get_option(backwards)
        no_case = get_option(no_case)
        regex = get_option(regex)
        if search_item:
            self.search_term = search_item.get()
        if in_tags:
            if regex:
                try:
                    compiled_term = re.compile(self.search_term)
                except Exception:
                    compiled_term = re.compile(r'\Z\A')
                tag_ids = deque( tag_id for tag_id in self.text.tag_names() if
                        compiled_term.search(tag_id) )
            elif no_case:
                tag_ids = deque( tag_id for tag_id in self.text.tag_names() if
                        self.search_term.lower() in tag_id.lower() )
            else:
                tag_ids = deque( tag_id for tag_id in self.text.tag_names() if
                        self.search_term in tag_id )
            if tag_ids:
                if backwards:
                    tag_get_range = self.text.tag_prevrange
                    off = ''
                    comparer = '>'
                    nth = 1
                else:
                    tag_get_range = self.text.tag_nextrange
                    off = '+1c'
                    comparer = '<'
                    nth = 0
                near_tag_id = ''
                for startindex in ('insert', 'end' if backwards else '1.0'):
                    near_tag_range = (startindex, startindex)
                    for tag_id in tag_ids:
                        tag_range = tag_get_range(tag_id, startindex + off) or \
                                near_tag_range
                        if self.text.compare(
                                near_tag_range[nth], comparer, tag_range[nth]):
                            near_tag_id = tag_id
                            near_tag_range = tag_range
                    if near_tag_id or not wrap_around:
                        break
                if near_tag_id:
                    self.lay_muse(tag_range = near_tag_range)
        else:
            length = tk.IntVar()
            start = self.text.search(self.search_term, 'insert',
                    stopindex = None if wrap_around else
                        ('1.0' if backwards else 'end+1c'),
                    backwards = backwards or None,
                    nocase = no_case or None,
                    regexp = regex or None,
                    count = length)
            if start:
                end = start + '+{}c'.format(length.get())
                if self.text.tag_ranges('sel'):
                    self.text.tag_remove('sel', 'sel.first', 'sel.last')
                self.text.tag_add('sel', start, end)
                self.text.tag_raise('sel')
                self.text.mark_set('insert', start if backwards else end)
                self.text.see(start)

    def search_in_web(self, search_item, url_template):
        self.url_template = url_template
        search_in_web(search_item.get(), url_template)

    def search_in_web_on_event(self, event):
        term = get_term(event)
        if term and self.url_template:
            search_in_web(term, self.url_template)
        else:
            self.lay_archivarius_on_event(event)

    def select_all_on_event(self, event):
        if self.text.tag_ranges('sel'):
            self.text.tag_remove('sel', 'sel.first', 'sel.last')
        self.text.tag_add('sel', '1.0', 'end')
        self.text.tag_raise('sel')

    def set_checkbutton(self, lemma):
        if self.textselection_exists():
            self.deannotate()
            self.tagging[lemma] = True
            self.set_tag(get_tag_id(self.tagging), 'sel.first', 'sel.last')
            self.lay_muse()

    def set_checkbutton_on_item_return(self, event):
        entry = event.widget
        subframe = entry.grid_info()['in']
        listbox = next( child for child in subframe.winfo_children()
                if child.winfo_class() == 'Listbox' )
        lemma = entry.get()
        if lemma in listbox.get('0', 'end'):
            self.set_checkbutton(lemma)

    def set_checkbutton_on_listbox_select(self, event):
        listbox = event.widget
        self.set_checkbutton(listbox.get(listbox.curselection()[0]))

    def set_item_on_listbox_select(self, event):
        listbox = event.widget
        subframe = listbox.grid_info()['in']
        item = next( child for child in subframe.winfo_children()
                if child.winfo_class() == 'Entry' )
        lemma = listbox.get(listbox.curselection()[0])
        item.delete('0', 'end')
        item.insert('0', lemma)
        item.config(**self.tk_styles['item_found'])

    def set_lang(self, lang_id):
        self.lang_id = lang_id
        self.lay_menu()

    def set_listbox_on_item_input(self, event):
        entry = event.widget
        subframe = entry.grid_info()['in']
        listbox = next( child for child in subframe.winfo_children()
                if child.winfo_class() == 'Listbox' )
        entry.config(**self.tk_styles['item_not_found'])
        item = entry.get()
        for index, lemma in enumerate(listbox.get('0', 'end')):
            if lemma.startswith(item):
                entry.config(**self.tk_styles['item_found'])
                listbox.selection_clear('0', 'end')
                listbox.selection_set(index)
                listbox.see(index)
                break

    def set_sel_ever_visible_tag_on_event(self, event):
        tag_id = 'sel_ever_visible'
        if tag_id in self.text.tag_names():
            if self.text.tag_ranges(tag_id):
                self.text.tag_remove(tag_id,
                        tag_id + '.first', tag_id + '.last')
        if self.text.tag_ranges('sel'):
            self.text.tag_add(tag_id, 'sel.first', 'sel.last')
            self.text.tag_configure(tag_id, background =
                    self.text.tag_cget('sel', 'background'))
            self.text.tag_raise(tag_id)

    def set_tag(self, tag_id, start_index, end_index):
        self.text.tag_add(tag_id, start_index, end_index)
        self.text.tag_configure(tag_id, background = '#fcfccc')
        self.text.tag_bind(tag_id, '<Enter>',
                partial(self.lay_muse_on_enter, tag_id))
        self.text.edit_modified(1)

    def style(self):
        with open(self.tk_styles_path, 'r', encoding = 'utf-8') as infile:
            self.tk_styles = json.loads(infile.read())
        with open(self.ttk_styles_path, 'r', encoding = 'utf-8') as infile:
            self.ttk_styles = json.loads(infile.read())
        ttk_style = ttk.Style()
        for theme in ('xpnative', 'aqua', 'winnative', 'clam',):
            try:
                ttk_style.theme_use(theme)
                break
            except tk.TclError:
                pass
        for widget, design in self.ttk_styles:
            ttk_style.map(widget, **design)

    def textselection_exists(self):
        if self.text.tag_ranges('sel'):
            return True
        else:
            say(self.glosses['no_text_selected'][self.lang_id])
            return False

    def track_modification_on_event(self, event):
        if event.widget == self.text:
            circumfix = '*' if self.text.edit_modified() else ''
            self.title(circumfix + os.path.basename(self.path) + circumfix
                    + ' · ' + self.path)

    def undo(self, _):
        try:
            self.text.edit_undo()
        except TclError:
            pass

class TableFilter(html.parser.HTMLParser):
    '''
    Parser to extract the first table from an (X)HTML string, leaving
    out all tags and attributes except the ones specified as args of
    :meth:`__init__`.

    .. warning::
        If a closing tag is omitted in the input string, it is *not* supplied.

    The document is given to the parser with its method `parse`. Then, the
    extracted parts are stored in :attr:`valid_parts`, which is a deque of
    strings.

    .. testcode::
        doc = '\
            <html xmlns:v="urn:schemas-microsoft-com:vml">\
            <head>\
            <meta http-equiv=Content-Type content="text/html; charset=utf-8">\
            <style>\
            <!--table @page {margin:.98in .79in .98in .79in;} -->\
            </style>\
            <![if !supportTabStrip]><script language="JavaScript">\
            <!--\
            if (window.name!="frSheet")\
             window.location.replace("wblux_variant_register");\
            //-->\
            </script>\
            <![endif]>\
            </head>\
            <body link=blue vlink=purple>\
            <table border=0 cellpadding=0 cellspacing=0 width=472 style="border-collapse:\
             collapse;table-layout:fixed;width:355pt">\
             <col width=79>\
             <col width=122>\
             <col width=134>\
             <col width=137>\
             <tr height=22 style="height:16.5pt">\
              <td height=22 class=xl65 width=79 style="height:16.5pt;width:59pt">geoffenbaret</td>\
              <td class=xl65 align=right width=122 style="width:92pt">0</td>\
              <td class=xl65 width=134 style="width:101pt">`ge-offen-bâren @swv</td>\
              <td class=xl65 width=137 style="width:103pt">0.42857142857142855</td>\
             </tr>\
             <tr height=22 style="height:16.5pt">\
              <td height=22 class=xl65 width=79 style="height:16.5pt;width:59pt">geoffenbaret</td>\
              <td class=xl65 align=right width=122 style="width:92pt">0</td>\
              <td class=xl65 width=134 style="width:101pt">`offen-bæren @swv</td>\
              <td class=xl65 width=137 style="width:103pt">0.5714285714285714</td>\
             </tr>\
             <![if supportMisalignedColumns]>\
             <tr height=0 style="display:none">\
              <td width=79 style="width:59pt"></td>\
              <td width=122 style="width:92pt"></td>\
              <td width=134 style="width:101pt"></td>\
              <td width=137 style="width:103pt"></td>\
             </tr>\
             <![endif]>\
            </table>\
            </body>\
            </html>'
        parser = TableFilter()
        parser.parse(doc)
        print(''.join(parser.valid_parts).replace(
                '<tr>', '\n  <tr>').replace(
                    '</table>', '\n</table>'))

    .. testoutput::
        <table>
          <tr><td>geoffenbaret</td> <td>0</td> <td>`ge-offen-bâren @swv</td> <td>0.42857142857142855</td></tr>
          <tr><td>geoffenbaret</td> <td>0</td> <td>`offen-bæren @swv</td> <td>0.5714285714285714</td></tr>
          <tr><td></td> <td></td> <td></td> <td></td></tr>
        </table>
    '''

    def __init__(
            self,
            table_tag = 'table',
            row_tag = 'tr',
            valid_text_tags = ('td', 'th'),
            valid_tags = ('table', 'thead', 'tbody', 'tfoot', 'tr', 'th', 'td'),
            valid_attrs = ()
            ):
        super().__init__(convert_charrefs = True)
        self.table_tag       = table_tag
        self.row_tag         = row_tag
        self.valid_text_tags = valid_text_tags
        self.valid_tags      = valid_tags
        self.valid_attrs     = valid_attrs
        self.valid_parts     = deque()
        self.in_table        = False
        self.in_valid_text   = False

    def handle_data(self, data):
        if self.in_table and self.in_valid_text:
            self.valid_parts.append(html.escape(data))

    def handle_endtag(self, tag):
        if self.in_table:
            if tag in self.valid_tags:
                self.valid_parts.append(xmlhtml.serialize_tag(tag, end = True))
            if tag in self.valid_text_tags:
                self.in_valid_text = False
        if tag == self.table_tag:
            self.in_table = False

    def handle_starttag(self, tag, attrs):
        if tag == self.table_tag:
            self.in_table = True
        if self.in_table:
            if tag in self.valid_text_tags:
                self.in_valid_text = True
            if tag in self.valid_tags:
                attrs = [ (key, value) for key, value in attrs
                        if key in self.valid_attrs ]
                self.valid_parts.append(xmlhtml.serialize_tag(tag, attrs))

    def parse(self, doc):
        self.feed(doc)
        self.close()

def crop_table(table, row_length):
    new_table = deque()
    while table:
        row = table.popleft()
        if any(row):
            new_table.append(deque())
            for i in range(row_length):
                new_table[-1].append(row.popleft())
    return new_table

def extract_table(
        path,
        encoding = 'utf-8',
        convert = xmlhtml.elem_to_table,
        ):
    '''
    Extract a table from :param:`path`. If it is not well-formed XML,
    try to extract a table with :class:`TableFilter`.
    Convert it by the function :param:`convert`.
    '''
    try:
        root = ET.parse(path).getroot()
    except ET.ParseError:
        parser = TableFilter()
        with open(path, 'r', encoding = encoding) as file:
            parser.parse(file.read())
        root = ET.XML(''.join(parser.valid_parts))
    xmlhtml_table = root if root.tag == 'table' else root.find('.//table')
    return convert(xmlhtml_table,
            row_xpath = 'tbody/tr' if xmlhtml_table.find('tbody') else './/tr')

def get_option(option):
    return option if isinstance(option, bool) else option.status.get()

def get_tag_id(tagging, record_delimiter = '\x1e', term_delimiter = '\x1f'):
    # The tag_id is prefixed to avoid any confusion with tk’s `sel` tag.
    return TAG_PREFIX + record_delimiter.join( item + term_delimiter +
            ('1' if is_selected else '0')
            for item, is_selected in tagging.items() )

def get_tagging(tag_id, record_delimiter = '\x1e', term_delimiter = '\x1f'):
    # The tag_id is prefixed to avoid any confusion with tk’s `sel` tag.
    tagging = ( record.split(term_delimiter, 1) for record in
            re.sub('^' + TAG_PREFIX, '', tag_id).split(record_delimiter)
            if record )
    return { item: (True if is_selected == '1' else False)
            for item, is_selected in tagging }

def get_term(event):
    widget = event.widget
    if widget.winfo_class() == 'Entry':
        return (widget.get()[widget.index('sel.first'):widget.index('sel.last')]
                if widget.select_present() else widget.get())
    elif widget.winfo_class() == 'Listbox':
        return widget.get(widget.curselection()[0])
    elif widget.winfo_class() == 'Text':
        if widget.tag_ranges('sel'):
            return widget.get('sel.first', 'sel.last')
    return ''

def guess_item(
        term: str,
        register: 'dict[str, dict[float, dict[str, float]]]',
        isjunk: 'None|ty.Callable[[str], bool]' = None,
        modify_term: 'ty.Callable[[str], str]' = parse.even,
        modify_variant: 'ty.Callable[[str], str]' = parse.even,
        maxlen: int = 1,
        cutoff: float = 0.4,
        ) -> 'List[Tuple[Dec, str]]':
    '''
    Guess, which item_id(s) in :param:`register` belong(s) to :param:`term`.
    Return at most :param:`maxlen` suggestions.

    :param:`register` is a dictionary like::
        {variant1: {prior_of_variant1: {item_id1: prior_of_item1,
                                        item_id2: prior_of_item2,
                                        item_id3: prior_of_item3}},
         variant2: {prior_of_variant2: {item_id1: prior_of_item1,
                                        item_id4: prior_of_item4,
                                        item_id5: prior_of_item5}},
        }

    .. note::
        The `prior_of_variant...` is not used at the time being and may be any
        value.

    The suggestions are a list of tuples, each containing a probability and an
    item_id. The probability is calculated (1) from the similarity ratio
    between :param:`term` and the most similar variant that is in
    param:`register` associated with the item_id, and (2) from a prior
    probability (`prior_of_item...` in the example above). This prior is, as a
    rule, the ratio ``variant_x_occurs / variant_x_is_annotated_with_item_y``.

    :param isjunk: The same as ``isjunk`` in :class:`difflib.SequenceMatcher`.
    :param modify_term: A function to modify the term, before it is compared
        with a variant.
    :param modify_variant: A function to modify the variant, before it is
        compared with a term.
    :param maxlen: The maximum length of the suggestions to be returned.
    :param cutoff: A threshold between 0 and 1: a similarity score a candidate
        must surpass; if the candidate does not surpass, it is discarded.

    .. note::
        The calculation of the similarity is expensive, but is avoided, if
        the :param:`register` contains at least :param:`maxlen` keys, for
        which the following applies: The key, modified by
        :param:`modify_variant`, is equal to the term, modified by
        :param:`modify_term`. Thus, one can accelerate the run by tweaking
        the :param:`register`.

    .. note::
        The probability of the suggested item_id is calculated by updating the
        associated prior of the item. The calculation here, alas, is not the
        quite right thing. The right thing is to multiply the prior with the
        probability `P(term|item)`: How probable is the term, if the item is
        meant? Here, this probability is only roughly approximated by the
        similarity ratio between term and item.
    '''
    def consider(
            variant: str,
            suggestions: 'List[Tuple[Dec, str]]',
            register: 'dict[str, Tuple[str, Dec]]',
            maxlen: int,
            ratio: Dec
            ) -> 'List[Tuple[Dec, str]]':
        for item_id, prior in register[variant].items():
            prob = Dec(prior) * ratio ** 3
            if len(suggestions) < maxlen:
                heapq.heappush(suggestions, (prob, item_id))
            else:
                heapq.heappushpop(suggestions, (prob, item_id))
        return suggestions

    register = { variant: tuple(prior_item_id_prior.values())[0]
            for variant, prior_item_id_prior in register.items() }
    variants = register.keys()
    term_mod = modify_term()
    suggestions = []
    for variant in variants:
        if modify_variant(variant) == term_mod:
            suggestions = consider(variant, suggestions, register, maxlen, 1)
            if len(suggestions) == maxlen:
                break
    if len(suggestions) < maxlen:
        matcher = difflib.SequenceMatcher(isjunk = isjunk, autojunk = False)
        matcher.set_seq2(term_mod)
        for variant in variants:
            variant_mod = modify_variant(variant)
            if not variant_mod == term_mod:
                matcher.set_seq1(variant_mod)
                if matcher.real_quick_ratio() > cutoff and \
                        matcher.quick_ratio() > cutoff:
                    ratio = Dec(matcher.ratio())
                    if ratio > cutoff:
                        suggestions = consider(
                                variant, suggestions, register, maxlen, ratio)
    total = sum( prob for prob, _ in suggestions )
    suggestions = [ (item_id, Dec(prob) / total)
            for prob, item_id in suggestions ]
    suggestions.sort(key = itemgetter(1, 0), reverse = True)
    return suggestions

def iterterms(
        text: str,
        startline: int = 0,
        startchar: int = 0,
        term_ends_with_termchar_re: str = r'[^\s|]\Z',
        term_ends_with_hyphenchar_re: str = r'-(<[^>]*?>)*\Z',
        tagstarter: str = '<',
        tagcloser: str = '>',
        source_start_tags: 'Sequence[str]' = ('<tx>',),
        source_close_tags: 'Sequence[str]' = ('</tx>',),
        text_start_tags: 'Sequence[str]' = ('</S>', '</v>', '</tit>'),
        text_close_tags: 'Sequence[str]' = ('<S>', '<v>', '<tit>'),
        ) -> 'Generator[tuple[str, tuple[int, int], tuple[int, int]]]':
    '''
    Yield (iteratively, incrementally) every item of :param:`text` in a tuple
    like ``('item', (1, 2), (3, 4))``, which would mean: In :param:`text`,
    the item :str:`item` starts in line 1 at character 2 and ends in
    line 3 at character 4.

    .. note::
        This given range may in :param:`text` include more than just
        :str:`item`, e.g. it may also include a tagging which has been
        deliberately excluded from :str:`item`.

    This iterator is intended for the WBLux-project format, developed by
    Britta Weimann. With other values for the keyword arguments, it may be
    used for other formats, too.
    '''
    endline = startline
    endchar = startchar
    pretag_endline = endline
    pretag_endchar = endchar
    term_ends_with_termchar_comp = re.compile(term_ends_with_termchar_re)
    term_ends_with_hyphenchar_comp = re.compile(term_ends_with_hyphenchar_re)
    in_source = False
    in_text = False
    in_term = False
    in_tag = False
    term = ''
    tag = ''
    tag_in_term = ''
    for char in text:
        if char == '\r':
            continue
        elif char == tagstarter:
            in_tag = True
        if in_tag:
            tag += char
            if in_term:
                tag_in_term += char
        elif in_text and (term_ends_with_termchar_comp.search(term + char) or
                (char == '\n' and term_ends_with_hyphenchar_comp.search(term))):
            if tag_in_term:
                term += tag_in_term
                tag_in_term = ''
            term += char
            if not in_term:
                in_term = True
                startline = endline
                startchar = endchar
        elif in_term:
            yield (term, (startline, startchar),
                    (pretag_endline, pretag_endchar))
            term = ''
            tag_in_term = ''
            in_term = False
        if char == '\n':
            endline += 1
            endchar = 0
        else:
            endchar += 1
        if not in_tag:
            pretag_endline = endline
            pretag_endchar = endchar
        if char == tagcloser:
            tag = re.sub(r'\s', '', tag)
            if tag in source_start_tags:
                in_source = True
                in_text = True
            elif tag in source_close_tags:
                in_source = False
                in_text = False
            elif in_source and tag in text_start_tags:
                in_text = True
            elif in_source and tag in text_close_tags:
                in_text = False
            tag = ''
            in_tag = False

def new(path = ''):
    subprocess.Popen([sys.executable, __file__, path])

def open_as_json(text, tell_unreadable):
    chars_and_tags = {'text': [], 'tagon': [], 'tagoff': []}
    try:
        for kind, value, index in json.loads(text):
            chars_and_tags[kind].append(
                    (value, tuple(map(int, index.split('.')))))
    except Exception:
        tell_unreadable()
        return text, ()
    chars = ''.join(map(itemgetter(0), chars_and_tags['text']))
    tags = ( (value, '.'.join(map(str, start)), '.'.join(map(str, end)))
            for (value, start), (value, end) in zip(
            sorted(chars_and_tags['tagon']),
            sorted(chars_and_tags['tagoff']))
            if value.startswith(TAG_PREFIX) )
    return chars, tags

def say(message,
        title = '“ein falscher Strich […] stürzt Sie ins Unglück”',
        icon = 'warning',
        type = 'ok'
        ):
    return tk.messagebox.Message(title = title, message = message,
            icon = icon, type = type).show()

def search_in_web(term, url_template):
    url = url_template.format(
            urllib.parse.quote_plus(term),
            parse.even(term.strip().split()[0]))
    if url:
        try:
            webbrowser.open(url, new = 0, autoraise = True)
        except Exception:
            pass

def main():
    editor = Editor()
    editor.mainloop()

if __name__ == '__main__':
    main()
