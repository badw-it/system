% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs.setdefault('lang_id', db.lang_id)
% relpath = request.path.split('/', 2)[-1] # without lang_id
<!DOCTYPE html>
<html id="top" class="desk txt" lang="{{lang_id}}">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design of the foundation by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="{{request.path}}" rel="canonical"/>
	<link href="/-/icons/favicon.ico" rel="icon"/>
	<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/cssjs/badw_sjd.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/cssjs/jquery/jquery.min.js"></script>
	<script src="/cssjs/all.js?v={{db.startsecond}}"></script>
	<title>NN · {{kwargs.get('title', db.glosses[relpath][lang_id].title())}}</title>
</head>
<body>
<header>
	<a class="img" href="https://NN"><img src="/-/icons/jvd_portrait.jpg" justify="right" alt="Bildnis des Johannes von Damaskus"/></a>
	<h1>NN</h1>
	<nav>
		<ul>
		  <%
		  for path, menu_name in (
				('start', db.glosses['start'][lang_id]),
				('introduction', db.glosses['introduction'][lang_id]),
				('bildungsroman', 'Bildungsromane'),
				):
		  %>
			<li>\\
			  % if path == relpath:
				<b>{{!menu_name}}</b>\\
			  % else:
				<a href="/{{!lang_id}}/{{!path}}">{{!menu_name}}</a>\\
			  % end
			</li>
		  % end
		</ul>
	</nav>
	<nav class="extra">
		<ul>
		  <% for path, menu_name in (
				('/privacy', db.glosses['datenschutz'][lang_id]),
				('/impressum', 'Impressum'),
				):
		  %>
			% if path == relpath:
			<li><b>{{!menu_name}}</b></li>
			% else:
			<li><a href="/{{!lang_id}}/{{!path}}">{{!menu_name}}</a></li>
			% end
		  % end
		</ul>
	  % if request.roles:
		<p><strong><a href="/{{!lang_id}}/auth_end">{{request.user_name}}: {{db.glosses['logout'][lang_id]}}</a></strong> · <a href="/{{!lang_id}}/auth_admin">{{db.glosses['administer'][lang_id]}}</a></p>
	  % end
	</nav>
	<nav class="extra lang">
	% for target_lang_id in db.lang_ids:
	  % if target_lang_id == lang_id:
		<b>{{db.glosses['lang_name'][lang_id]}}</b>
	  % else:
		<a href="{{request.url.replace(f'/{lang_id}/', f'/{target_lang_id}/')}}" hreflang="{{target_lang_id}}" lang="{{target_lang_id}}" rel="alternate">{{db.glosses['lang_name'][target_lang_id]}}</a>
	  % end
	% end
	</nav>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
</body>
</html>
