-- Licensed under http://www.apache.org/licenses/LICENSE-2.0
-- Attribution notice: by Stefan Müller in 2023 ff. (© http://badw.de)

-- Any column whose name starts with “_” exists for display purposes only and will be filled automatically.
-- All string columns are written with XML escaping and may contain tags.

CREATE TABLE `bestimmung` (
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
-- TODO:
	`art` ENUM('', 'begriff', 'institution', 'land', 'netzseite', 'ort', 'sprache', 'werk') NOT NULL DEFAULT '',
 	`bezeichnung` VARCHAR(768) NOT NULL DEFAULT '',
 	`erläuterung` LONGTEXT NOT NULL DEFAULT '',
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	PRIMARY KEY (ID)
);
CREATE TABLE `bestimmung_bestimmung` (
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`bestimmung_ID_1` BIGINT UNSIGNED NOT NULL,
	`art` ENUM('', 'liegt in', 'Unterbegriff von', 'verwandt mit', 'verwechselbar mit') NOT NULL DEFAULT '', -- TODO -- nicht-zu-verwechseln-Feld höchstens bei Geographika!
	`bestimmung_ID_2` BIGINT UNSIGNED NOT NULL,
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	PRIMARY KEY (ID)
);
CREATE TABLE `bestimmungsalias` (
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`bestimmung_ID` BIGINT UNSIGNED DEFAULT NULL,
	`bezeichnung` VARCHAR(768) NOT NULL DEFAULT '',
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	PRIMARY KEY (ID)
);
CREATE TABLE `dokument` (
	`ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- Alte IDs hier aufheben, also beim Einspeisen “00000103” → `103` usw.
	`signatur` TEXT NOT NULL DEFAULT '',
	`blattzählung` TEXT NOT NULL DEFAULT '',
	`objektart` ENUM, -- TODO
	`editionshinweis` LONGTEXT NOT NULL DEFAULT '',
	`literaturhinweis` LONGTEXT NOT NULL DEFAULT '',
	`gattung_1` ENUM, -- TODO
	`gattung_2` ENUM, -- TODO
	`absenderart` ENUM, -- TODO
	`empfängerart` ENUM, -- TODO
	`entstehung_von` VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`geburtsdatum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31. -- ([0-9]+|\?|o\.\s*D\.) -- Händisch werden noch überführt: unterspezifizierte, mit “?” versehene, als “o. D.” gebuchte Briefe
	`entstehung_von_anm` TEXT NOT NULL DEFAULT '',
	`entstehung_bis` VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`geburtsdatum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31. -- nur, wenn Zeitraum, sonst genügt bis-Angabe.
	`entstehung_bis_anm` TEXT NOT NULL DEFAULT '', -- nur, wenn Zeitraum, sonst genügt bis-Angabe.
	`umfang_Beilagen` TEXT NOT NULL DEFAULT '',
	`ausreifungsgrad` TEXT NOT NULL DEFAULT '',
	`erhaltungszustand` TEXT NOT NULL DEFAULT '',
	`inhalt` LONGTEXT NOT NULL DEFAULT '',
 	`bemerkung` LONGTEXT NOT NULL DEFAULT '',
	`letztbearbeiter` VARCHAR(768) NOT NULL DEFAULT '',
	`letztänderungsdatum` INT UNSIGNED, -- raafzeit
	`stand` TINYINT,
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	INDEX (stand),
	PRIMARY KEY (ID)
);
CREATE TABLE `dokument_bestimmung` (
-- TODO:
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`dokument_ID` BIGINT UNSIGNED DEFAULT NULL,
	`art` ENUM('') NOT NULL DEFAULT '',
	`person_ID` BIGINT UNSIGNED DEFAULT NULL,
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	PRIMARY KEY (ID)
);
CREATE TABLE `dokument_dokument` (
-- TODO:
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`dokument_ID_1` BIGINT UNSIGNED DEFAULT NULL,
	`_datum_1` VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser_1` VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger_1` VARCHAR(768) NOT NULL DEFAULT '',
	`_titel_1` TEXT NOT NULL DEFAULT '',
	`art` ENUM('', 'Antwort auf', 'Abschrift von', 'Begleitbrief von', 'Beilage zu', 'erschlossen aus', 'erwähnt in', 'notiert auf', 'Konzept von') NOT NULL DEFAULT '',
	`dokument_ID_2` BIGINT UNSIGNED DEFAULT NULL,
	`_datum_2` VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser_2` VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger_2` VARCHAR(768) NOT NULL DEFAULT '',
	`_titel_2` TEXT NOT NULL DEFAULT '',
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	UNIQUE (art, dokument_ID_1, dokument_ID_2),
	FOREIGN KEY (dokument_ID_1) REFERENCES dokument(ID),
	FOREIGN KEY (dokument_ID_2) REFERENCES dokument(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `person` (
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT, -- Alte IDs hier aufheben, also beim Einspeisen “00000103” → `103` usw.
-- TODO:
	`art` ENUM('', 'biblisch', 'literarisch', 'mythisch', 'real') NOT NULL DEFAULT 'real',
	`name` VARCHAR(768) NOT NULL DEFAULT '', -- “Ehename|Mädchenname, Vorname”
	`geburtsdatum` VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`geburtsdatum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31.
	`todesdatum` VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`todesdatum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31.
	`geburtsort_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_geburtsort` VARCHAR(768) NOT NULL DEFAULT '',
	`todesort_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_todesort` VARCHAR(768) NOT NULL DEFAULT '',
	`leben` TEXT NOT NULL DEFAULT '',
	`gnd` VARCHAR(768) NOT NULL DEFAULT '',
	`zitiert_in_AA` TEXT NOT NULL DEFAULT '',
	`bemerkung` LONGTEXT NOT NULL DEFAULT '',
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	INDEX (art),
	UNIQUE (name),
	FOREIGN KEY (geburtsort_ID) REFERENCES ort(ID),
	FOREIGN KEY (todesort_ID) REFERENCES ort(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `person_bestimmung` (
-- TODO:
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	PRIMARY KEY (ID)
);
CREATE TABLE `person_dokument` ( -- auch: körperschaft zu person.
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`person_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_person` VARCHAR(768) NOT NULL DEFAULT '',
-- TODO:
	`art` ENUM('Absender von', 'Empfänger von', 'erwähnt in', 'Patient in'),
	`dokument_ID` BIGINT UNSIGNED DEFAULT NULL,
	-- `_datum` VARCHAR(768) NOT NULL DEFAULT '',
	-- `_verfasser` VARCHAR(768) NOT NULL DEFAULT '',
	-- `_empfänger` VARCHAR(768) NOT NULL DEFAULT '',
	-- `_titel` TEXT NOT NULL DEFAULT '',
	`verlässlichkeit` ENUM('', 'gesichert') NOT NULL DEFAULT 'gesichert', -- “gesichert” in Ausgabe nicht anzeigen.
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	UNIQUE (art, dokument_ID, person_ID),
	FOREIGN KEY (person_ID) REFERENCES person(ID),
	FOREIGN KEY (ort_ID) REFERENCES ort(ID),
	FOREIGN KEY (dokument_ID) REFERENCES dokument(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `personenalias` (
	ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`person_ID` BIGINT UNSIGNED DEFAULT NULL,
	`bezeichnung` VARCHAR(768) NOT NULL DEFAULT '',
 	`anm` TEXT NOT NULL DEFAULT '', -- nicht-öffentlich
	PRIMARY KEY (ID)
);
