# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import json
try: import regex as re
except ImportError: import re
import sys
import time
from collections import deque
from itertools import groupby
try:
    from beaker.middleware import SessionMiddleware
except:
    SessionMiddleware = None

import gehalt
import __init__
import bottle
from bottle import HTTPError, redirect, request, response, static_file, template

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        if not db.debug:
            server.quiet = True
        self.kwargs = {
                'app': self if SessionMiddleware is None else
                    SessionMiddleware(self, db.config['session']),
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.hook('before_request')
        def status():
            if request.query.s in db.lang_ids:
                request.lang_id = request.query.s
                response.set_cookie('lang_id', request.lang_id)
            else:
                request.lang_id = request.get_cookie('lang_id') or db.lang_id
            request.user_id, request.user_name, request.roles = db.auth(request)

        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('<path:path>.html')
        def redirect_endformat(path):
            '''
            Redirect requests whose URL ends with `.html`: Ignore the trailing
            `.html`.
            '''
            redirect(path)

        @self.route('/<page_id:re:(autoren|datenschutz|heilige|impressum|index|kloester(_al|_ord)?|mss(_autogr|_codexUnicus|_inedunkrit)?|orte(_source)?|personen|projekt|repOpus_.*|repPers_.*|schlagw|werke)>')
        def redirect_legacy(page_id):
            '''
            Redirect requests with a legacy URL.
            '''
            if page_id.startswith('repPers'):
                item_id = db.resolve_alt_id(page_id)
                if item_id:
                    redirect(f'/autor/{item_id}')
                else:
                    redirect('/autor')
            elif page_id.startswith('repOpus'):
                item_id = db.resolve_alt_id(page_id)
                if item_id:
                    redirect(f'/werk/{item_id}')
                else:
                    redirect('/werk')
            elif page_id.startswith('autoren'):
                redirect('/autor')
            elif page_id.startswith('datenschutz'):
                redirect('http://badw.de/data/footer-navigation/datenschutz.html')
            elif page_id.startswith('heilige'):
                redirect('/heilig')
            elif page_id.startswith('impressum'):
                redirect('http://badw.de/data/footer-navigation/impressum.html')
            elif page_id.startswith('index'):
                redirect('/start')
            elif page_id.startswith('kloester'):
                redirect('/kloster')
            elif page_id.startswith('mss_autogra'):
                redirect('/autograph')
            elif page_id.startswith('mss_codexUnicus'):
                redirect('/unikat')
            elif page_id.startswith('mss_inedunkrit'):
                redirect('/unediert')
            elif page_id.startswith('mss'):
                redirect('/hs')
            elif page_id.startswith('orte_source'):
                redirect('/quellenort')
            elif page_id.startswith('orte'):
                redirect('/ort')
            elif page_id.startswith('personen'):
                redirect('/person')
            elif page_id.startswith('projekt'):
                redirect('http://geschichtsquellen.badw.de')
            elif page_id.startswith('schlagw'):
                redirect('/schlagwort')
            elif page_id.startswith('werke'):
                redirect('/werk')

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which do not specify the page to the default page.
            '''
            redirect('/' + db.page_id)

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/api/gnd/person')
        def return_gnd_person_map():
            '''
            Return a machine-readable mapping from GNDs to persons registered in
            the Geschichtsquellen. (Example usage: Corpus Corporum.)
            '''
            response.content_type = 'application/json'
            return json.dumps(db.get_gnd_person_map())

        @self.route('/api/hsc/urls.json')
        def return_hsc():
            '''
            Return JSON telling all URLs of all works linking to the
            handschriftencensus and the belonging handschriftencensus-URLs.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {
                        'name': db.config['ids']['name'],
                        'siglum': db.config['ids']['siglum'],
                        'url': db.config['ids']['url'],
                        'email': db.config['ids']['email'],
                        'data_info': db.config['api_hsc']['data_info'],
                        'data_url': db.config['api_hsc']['data_url'],
                        'data': tuple(db.get_hsc_urls()),
                    },
                    separators = (',', ':'),
                    )

        @self.route('/auth_admin')
        def auth_admin():
            if not request.roles:
                redirect('/' + db.page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_admin.tpl'})

        @self.route('/auth_admin', method = 'POST')
        def auth_admin_post():
            note = db.auth_new(
                    request.forms,
                    request.user_id,
                    request.roles,
                    request.lang_id)
            redirect('/auth_admin?note=' + db.q(note))

        @self.route('/auth_end')
        def auth_end():
            session = request.environ.get('beaker.session', None)
            if session is not None:
                session.delete()
            redirect(request.query.url or '/' + db.page_id)

        @self.route('/' + db.auth_in_page_id)
        def auth_in():
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_in.tpl'})

        @self.route('/' + db.auth_in_page_id, method = 'POST')
        def auth_in_post():
            user_name = request.forms.user_name
            password = request.forms.password
            user_id = db.auth_in(user_name, password)
            if user_id is None:
                query = request.forms.query
                if query:
                    query = '?' + query
                redirect(request.fullpath + query)
            else:
                session = request.environ['beaker.session']
                session['user_id'] = user_id
                session.save()
                redirect(request.forms.url or '/' + db.page_id)

        @self.route('/eingabe')
        def return_input_menu():
            '''
            Return a page to access – via datatables – all items
            which can be inserted or edited.
            '''
            if 'editor' not in request.roles:
                redirect(f'/{db.auth_in_page_id}?url={db.q(request.fullpath)}')
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'eingabe.tpl'})

        @self.route('/eingabe/index/<kind>/data')
        def return_index_json(kind):
            '''
            Return JSON for a datatable of items of the kind :param:`kind`.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.get_index(kind, request.roles))},
                    separators = (',', ':'))

        @self.route('/eingabe/index/<kind>')
        def return_index(kind):
            '''
            Return a datatable of items of the kind :param:`kind` for input.
            '''
            if 'editor' not in request.roles:
                redirect(f'/{db.auth_in_page_id}?url={db.q(request.fullpath)}')
            table_config = {
                    'ajax': f'/eingabe/index/{kind}/data',
                    'columns':
                        [{'data': '0', 'render': {'_': '_', 'sort': 's'}}, {'searchable': False}, None]
                            if kind in {'bibliothek', 'gattung', 'hs', 'ink', 'region', 'schlagwort', 'sprache', 'zsaufsatz'}
                        else [{'data': '0', 'render': {'_': '_', 'sort': 's'}}, None, {'searchable': False}, {'searchable': False}, None]
                            if kind in {'autor', 'ort', 'person'}
                        else [{'data': '0', 'render': {'_': '_', 'sort': 's'}}, None, {'searchable': False}, None]
                            if kind == 'werk'
                        else [None, None, {'searchable': False}, None]
                        ,
                    'dom': 'fplitp',
                    'order': [[0, 'asc'], [1, 'asc']],
                    }
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'index.tpl',
                        'table_config': table_config,
                        'with_filtercard': False,
                        'text': template(
                            'index_input.tpl', db = db, request = request,
                            kwargs = {'kind': kind}),
                        })

        @self.route('/eingabe/lit/<item_id:int>')
        def redirect_lit(item_id):
            '''
            Redirect to a URL having “lit” replaced with the kind of
            literature (e.g. “zs”) that belongs to ``item_id``.
            '''
            kind = db.get_lit_kind(item_id, version = 0)
            if kind:
                redirect(f'/eingabe/{kind}/{item_id}')
            else:
                raise HTTPError(404, 'The page has not been found.')

        @self.route('/eingabe/<kind>')
        @self.route('/eingabe/<kind>/<item_id:int>')
        def return_form_item(kind, item_id = None):
            '''
            Get the data about the item :param:`item_id` of the kind
            :param:`kind` from the database and return these data as
            a form for reviewing and editing them.

            If :param:`item_id` is ``None``, insert a new entry, get
            its ID and redirect to this same function, now having an
            :param:`item_id`.
            '''
            if 'editor' not in request.roles:
                redirect(f'/{db.auth_in_page_id}?url={db.q(request.fullpath)}')
            item, item_id = db.get_item(kind, item_id, request.roles)
            if item is None:
                redirect(f'/eingabe/{kind}/{item_id}')
            if not item:
                raise HTTPError(404, 'The page has not been found.')
            if kind == 'person' and item['person_art'] == 'autor':
                kind = 'autor'
            elif kind == 'lit':
                kind = item['lit_art']
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': f'eingabe_{kind}.tpl',
                        'item_id': item_id,
                        'item': item,
                        })

        @self.route('/ersatzindex/<page_id>')
        def return_ersatzindex(page_id):
            '''
            Return an ersatz for the datatable that would otherwise be shown
            on the page :param:`page_id`.
            '''
            table = deque(['<table class="index">'])
            exists = False
            for row in db.get_index(
                        page_id,
                        {},
                        int(request.query.item_id or 0),
                        ):
                exists = True
                table.append('<tr><td>' + '</td> <td>'.join(
                        val['_'] if isinstance(val, dict) else str(val)
                        for _, val in sorted(row.items())
                        ) + '</td></tr>')
            if not exists:
                raise HTTPError(404, 'The page has not been found.')
            table.append('</table>')
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'text.tpl', 'text': ''.join(table)})

        @self.route('/filter')
        def return_filter():
            '''
            Return a page of results filtered according to the filters sent with
            ``request.query.text``.

            Or, if no ``request.query.text`` is sent, return a form for extended
            filtering.
            '''
            table_config = {
                    'ajax': f'/filter/json?{request.query_string}',
                    'columns': [
                        None,
                        {'data': '1', 'render': {'_': '_', 'sort': 's'}},
                        None,
                        ],
                    'dom': 'fplitp',
                    'order': [[0, 'asc'], [1, 'asc']],
                    }
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'index.tpl',
                        'table_config': table_config,
                        'with_filtercard': False,
                        'text': template(
                            'filter.tpl', db = db, request = request,
                            kwargs = {
                                'lang_id': request.lang_id,
                                }),
                        } if request.query.text else {'tpl': 'filter.tpl'} )

        @self.route('/filter', method = 'POST')
        def return_filter_post():
            '''
            Return a page showing a form to filter the content. The options that
            can be selected are taken from the database and are then prefiltered
            according to previously chosen options of another category.

            Or, if the request does not require further filters, just return the
            filter results according to all the selected options.
            '''
            def split_select(term: str) -> 'tuple[str, frozenset[int]]':
                kind, keys = term.split(':', 1)
                return (kind, frozenset(map(int, keys.split(','))))
            selects = dict(map(split_select, request.forms.selects.split(';'))
                    ) if request.forms.selects else {}
            if request.query.filter:
                items = db.filter_db(
                        request.query.filter,
                        request.forms.text,
                        1 if request.forms.desc == '1' else 0,
                        request.forms.bericht_von_bis,
                        request.forms.entstehung_von_bis,
                        selects,
                        )
                return template(
                        'filterform.tpl', db = db, request = request,
                        kwargs = {'selects': selects, 'items': items})
            else:
                if request.query.alt:
                    ids = request.forms.getall('id')
                    if ids:
                        selects[request.query.alt] = frozenset(map(int, ids))
                    else:
                        selects.pop(request.query.alt, None)
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'filter.tpl',
                            'items': db.filter_db(
                                'werk',
                                request.forms.text,
                                1 if request.forms.desc == '1' else 0,
                                request.forms.bericht_von_bis,
                                request.forms.entstehung_von_bis,
                                selects,
                                ),
                            'selects': {
                                kind:
                                    '; '.join(db.get_item_names(kind, item_ids))
                                for kind, item_ids in selects.items()
                                },
                            'selectstring': ';'.join(
                                k + ':' + ','.join(map(str, v))
                                for k, v in selects.items() ),
                            'geo': True,
                            })

        @self.route('/filter/json')
        def return_filter_json():
            '''
            Return JSON for a datatable of filtered results.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.filter_text(
                        request.query.text, request.query.desc))},
                    separators = (',', ':'))

        @self.route('/geo', method = 'POST')
        def geo():
            '''
            Return geodata and other metadata of places indicated in the request
            either directly or via items (e.g. works) associated with places.
            '''
            note = ''
            geodata = ()
            try:
                geodata = db.geo(
                        kind = request.forms.kind,
                        place_ID_string = request.forms.place_ID_string,
                        person_ID_string = request.forms.person_ID_string,
                        work_ID_string = request.forms.work_ID_string,
                        )
            except Exception as e:
                note = f'Error: {e}'
            return {'geodata': geodata, 'note': note}

        @self.route('/json/<act>')
        def return_json(act):
            '''
            Return in JSON all available results for the specified :param:`act`.
            '''
            response.content_type = 'application/json'
            return json.dumps(tuple(db.yield_for(act)), separators = (',', ':'))

        @self.route('/redact', method = 'POST')
        @self.route('/redact/<table>/<item_id:int>', method = 'POST')
        def redact(table = None, item_id = None):
            '''
            Redact the item :param:`item_id` of the table :param:`table`.
            The kind of the redaction, given as a POST parameter, may be:

            - the release of the item.
            - the deletion, i.e. a marking as ‘deleted’.
            '''
            if 'redactor' not in request.roles:
                redirect(f'/{db.auth_in_page_id}?url={db.q(request.fullpath)}')
            if request.forms.redaction == 'delete':
                db.delete(table, item_id)
                redirect('/eingabe')
            elif request.forms.redaction == 'release':
                num = db.release(table, item_id)
                if table is None or item_id is None:
                    redirect(f'/eingabe?num={num}')
                else:
                    redirect(f'/eingabe/{table}/{item_id}')

        @self.route('/upsert', method = 'POST')
        def upsert():
            '''
            Ingest content of a field sent via AJAX into the database.
            '''
            if 'editor' not in request.roles:
                raise HTTPError(401, 'Unauthorized request.')
            col = request.forms.col
            doc = request.forms.doc
            item_id = request.forms.item_id
            kind = request.forms.kind
            rank = request.forms.rank
            parent_col = request.forms.parent_col
            parent_id = request.forms.parent_id
            grandparent_col = request.forms.grandparent_col
            grandparent_id = request.forms.grandparent_id
            note = ''
            try:
                item_id = int(item_id or 0)
                rank = int(rank or 0)
                parent_id = int(parent_id or 0)
                grandparent_id = int(grandparent_id or 0)
                item_id, doc = db.upsert(
                        col,
                        item_id,
                        doc,
                        kind,
                        rank,
                        parent_col,
                        parent_id,
                        grandparent_col,
                        grandparent_id,
                        )
            except Exception as e:
                note = f'Error: {e}'
            return {'item_id': item_id, 'doc': doc, 'note': note}

        @self.route('/<kind:re:(autor|heilig|hs|ort|person|quellenort|schlagwort|werk)>/<item_id:int>')
        @self.route('/<kind:re:(autor|heilig|hs|ort|person|quellenort|schlagwort|werk|werk_neu|autograph|incipit|kloster|unediert|unikat)>')
        def return_info(kind, item_id = 0):
            '''
            For public output, return information:

            - about all items of :param:`kind` if item_id is ``0``.
            - otherwise about the item with :param:`item_id`. If this happens to
              be one of the two main kinds (author and work), return the article
              about the item. Otherwise, return a table of the authors and works
              whose description references the item :param:`item_id`.

            .. note::
                ‘heilig’ must be distinguished from ‘person’, since a person may
                be ‘heilig’ with regard to certain articles but just a person as
                far as others are concerned. Cf. /heilig/20918 – /person/20918.

                Similarly ‘ort’ and ‘quellenort’ must be distinguished.
            '''
            version = request.query.version.strip()
            if version:
                try:
                    version = int(version)
                    assert version >= 10101 # i.e. year 1, Jan. 1st in Raafzeit.
                except: # Sic.
                    redirect(f'/{kind}/{item_id}')
            else:
                version = gehalt.ENDZEIT # i.e. the currently published version.
            lang_id = request.lang_id
            table_config = {
                    'ajax': f'/{kind}/data?item_id={item_id}',
                    'columns':
                        [{'data': '0', 'render': {'_': '_', 'sort': 's'}}]
                            if item_id and kind == 'schlagwort'
                        else [
                            None,
                            {'data': '1', 'render': {'_': '_', 'sort': 's'}},
                            ] if item_id
                        else [{'data': '0', 'render': {'_': '_', 'sort': 's'}}]
                            if kind in {'autograph', 'unediert', 'unikat'}
                        else [
                            {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                            None,
                            ] if kind == 'schlagwort'
                        else [
                            {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                            {'data': '1', 'render': {'_': '_', 'sort': 's'}},
                            ] if kind == 'incipit'
                        else [
                            {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                            None,
                            None,
                            ] if kind == 'hs'
                        else [
                            {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                            {'data': '1', 'render': {'_': '_', 'sort': 's'}},
                            None,
                            ] if kind == 'autor'
                        else [
                            {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                            None,
                            None,
                            None,
                            ] if kind in {'heilig', 'person'}
                        else [
                            {'data': '0', 'render': {'_': '_', 'sort': 's'}},
                            {'data': '1', 'render': {'_': '_', 'sort': 's'}},
                            None,
                            None,
                            ] if kind in {'kloster', 'ort', 'quellenort'}
                        else [ # i.e. kind in {'werk', 'werk_neu'}
                            {'width': '30%', 'data': '0', 'render': {'_': '_', 'sort': 's'}, 'type': 'html-num'},
                            {'width': '25%', 'data': '1', 'render': {'_': '_', 'sort': 's'}},
                            {'width': '30%', 'data': '2', 'render': {'_': '_', 'sort': 's'}},
                            {'width': '15%', 'data': '3', 'render': {'_': '_', 'sort': 's'}},
                            ]
                        ,
                    'dom': 'fplitp',
                    'order':
                        [[0, 'asc'], [1, 'asc']]
                            if item_id and kind != 'schlagwort'
                        else [[1, 'asc'], [0, 'asc']]
                            if kind == 'kloster'
                        else [[0, 'asc']]
                        ,
                    }
            with_geo = True if kind in {
                    'kloster', 'quellenort', 'ort'} else False
            if not item_id:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'index.tpl',
                            'table_config': table_config,
                            'with_filtercard': False,
                            'text': template(
                                'index_output.tpl', db = db, request = request,
                                kwargs = {
                                    'kind': kind,
                                    'item_id': item_id,
                                    'lang_id': lang_id,
                                    }),
                            'geo': with_geo,
                            'ersatzlink': '/ersatzindex/' + kind,
                            })
            elif kind in {'autor', 'werk'}:
                tpl_name = 'autor.tpl' if kind == 'autor' else 'werk.tpl'
                item, item_id = db.get_item(kind, item_id, {}, version)
                if not item:
                    raise HTTPError(404, 'The page has not been found.')
                return db.reannotate(
                        template(
                            'base.tpl', db = db, request = request,
                            kwargs = {
                                'tpl': tpl_name,
                                'kind': kind,
                                'item_id': item_id,
                                'item': item,
                                'groupby': groupby,
                                }),
                        request.query.mark)
            else:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'index.tpl',
                            'table_config': table_config,
                            'with_filtercard': False,
                            'text': template(
                                'index_output.tpl', db = db, request = request,
                                kwargs = {
                                    'kind': kind,
                                    'item_id': item_id,
                                    'item_name': db.e(next(db.get_item_names(
                                        kind, [item_id], version), '')),
                                    'lang_id': lang_id,
                                    }),
                            'geo': with_geo,
                            })

        @self.route('/<kind:re:(autograph|autor|heilig|hs|incipit|kloster|ort|person|quellenort|schlagwort|unediert|unikat|werk|werk_neu)>/data')
        def return_index_json_ausgabe(kind):
            '''
            Return JSON for a datatable of items of the kind :param:`kind`
            for output.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.get_index(
                        kind,
                        {},
                        int(request.query.item_id or 0),
                        ))},
                    separators = (',', ':'))

        @self.route('/<page_id>')
        def return_page(page_id):
            '''
            Get the text specified by :param:`page_id` from the data base,
            fill it into the default template, build and return the page.
            '''
            text, tpl = db.get_text_and_template_name('', page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': tpl, 'text': text})

        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/icons/favicon.ico`
            - `/de/acta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(2)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
