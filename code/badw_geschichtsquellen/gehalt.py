# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import subprocess
import sys
import traceback
from collections import defaultdict
from functools import partial
from operator import itemgetter
from urllib.parse import urlencode

try:
    import pymysql as mysql
    from pymysql import IntegrityError
except ImportError:
    pass

import __init__
import fs
import sql_io
import web_io
import xmlhtml
from parse import even, sortnum, sortxtext, XTAG_PRE

ENDZEIT: int = 2111222333

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.

        Information about the database
        ==============================

        The Database Structure …
        -------------------------

        … is given in “../../../sql/badw_geschichtsquellen.sql”.

        Escaping status of all strings
        ------------------------------

        In all fields of a string datatype (i.e. VARCHAR, TEXT and LONGTEXT),
        the text is minimally escaped according to the rules of XML and HTML:
        `&` → `&amp;`
        `<` → `&lt;`
        `>` → `&gt;`
        `"` → `&quot;`

        Release, versioning and deletion
        --------------------------------

        Fields ending with “_id”, “_von” and “_bis”  work as follows:

        1. “_von” is a start date, “_bis” is an end date. The combination of
        at least …

        - “_id” and “_von”
        - or “_id” and “_bis”

        … is unique for every row.

        2. The meaning of the “_von” and “_bis” values are as follows:

        2.1. The unpublished version of a row has:

        - “_von” = “0”
        - “_bis” = “0” if there is nothing to do, “1” if it has been changed
          and “2” if it is to be published.

        2.2. A deleted row has “_von” = “-1” and any value of “_bis”.

        2.3. A published version of a row has integers in “_von” and “_bis”.
        These integers represent a UTC-date in *tagesgequantelter Raafzeit*,
        e.g. “17490828” for ‘1749-08-28’ and cannot be e.g. “0”, “1”, “2” or
        “-1”.

        - The “_von” value represents the day when the row was published.
        - The “_bis” value …

          - of the currently public row is the defined maximum number of the
            “_bis” column: ``ENDZEIT``.
          - of any outdated row represents the very last day before this row
            was superseded because a newer version with the same “_id” value
            was published.

        3. These “_von” and “_bis” values are computed as follows:

        3.1 In the beginning, “_von” and “_bis” are “0”, meaning ‘no time’.

        3.2 There are other fields of the same row, henceforth called OF.

        3.3 When an editor puts new content in the OF, “_bis” is set to “1”.

        3.4 When an editor publishes an entry affecting the row, then “_bis”
        is set to “2”.

        3.5 When an editor deletes an entry affecting the row, “_von” is set
        to “-1” and “_bis” is set to “2”, too.

        3.6. In an infinite loop, a background program makes an integer R in
        tagesgequantelter Raafzeit and processes each row where “_bis” has a
        value of “2” as follows:

        3.7. If in the same table there is another row having the same “_id”
        and a “_von” value of R, then do nothing, which means effectively to
        wait a day before publishing, so that the row that is already public
        stays public for one day.

        3.8. If the row with “_bis” = “2” has “_von” = “-1”, then its “_bis”
        value is set back to “0”, and another row with the same “_id” value,
        but another “_von” value is searched and, if found, its “_bis” value
        is set to R minus 1. This is a form of versioned deletion.

        3.8. If the row with “_bis” = “2” has “_von” = “0”, its “_bis” value
        is set back to “0” too, but then:

        - The whole row is copied.
        - The “_von” value of the copy is set to R.
        - The “_bis” value of the copy is set to ``ENDZEIT``.
        - If there is not another row with the same “_id”, this copy is just
          inserted.
        - If there *is* another row with the same “_id”, but the OF have the
          same values, the copy is just discarded.
        - If there *is* another row with the same “_id”, but the OF have not
          the same values, the copy is inserted, and the “_bis” value of the
          row that was published before is set to R minus 1.
        '''
        super().__init__(urdata, __file__)

        self.connect = sql_io.get_connect(
                *fs.get_keys(self.paths['db_access']),
                self.config,
                )

        if self.config['modes'].getboolean('refreshing'):
            self.process_fulltext = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../collect_fulltext.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_db = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../process_db.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])

    def delete(self, table: str, item_id: int) -> None:
        '''
        Mark the item :param:`item_id` of the table :param:`table`
        as ‘deleted’; i.e. the “_von” field is set to “-1”.

        Mark the item :param:`item_id` of the table :param:`table`
        as ‘to be published’; i.e. the “_bis” field is set to “2”.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            table = table.replace('\\', '').replace('`', '').replace("'", '')
            cur.execute(rf"""
                    update `{table}` set `{table}_von` = -1, `{table}_bis` = 2
                    where `{table}_von` = 0 and `{table}_id` = %s
                    """, item_id)
            con.commit()

    def filter_db(
            self,
            kind: str,
            term: str,
            desc: int,
            bericht_von_bis: str,
            entstehung_von_bis: str,
            selects: 'dict[str, set[int]]',
            version: int = ENDZEIT,
            ) -> 'Generator[dict[str, str|int]]':
        version = int(version)
        term = term.replace('\\', '').replace("'", '')
        term, term_re_str, term_re = web_io.get_filterterms(term)
        bericht_von, bericht_bis = string_to_raafzeiten(bericht_von_bis)
        entstehung_von, entstehung_bis = string_to_raafzeiten(
                entstehung_von_bis)
        sql = 'DISTINCT'
        where = ''
        if kind in {'gattung', 'region', 'schlagwort', 'sprache'}:
            if kind == 'gattung':
                sql += rf"""
                        gattung_id, gattung_bezeichnung
                        """
                where += r"""
                        order by gattung_bezeichnung
                        """
            elif kind == 'region':
                sql += rf"""
                        region_id,
                        concat(IF(region_spät, '(SpätMA) ', '(FrühMA) '), region_bezeichnung) as region_bezeichnung,
                        region_spät
                        """
                where += r"""
                        order by region_spät, region_bezeichnung
                        """
            elif kind == 'schlagwort':
                sql += rf"""
                        schlagwort_id, schlagwort_bezeichnung
                        """
                where = r"""
                        order by schlagwort_bezeichnung
                        """
            elif kind == 'sprache':
                sql += rf"""
                        sprache_id, sprache_bezeichnung
                        """
                where += r"""
                        order by sprache_bezeichnung
                        """
            sql += rf"""
                    from {kind}
                    join werk2{kind}
                        on {version} between werk2{kind}_von and werk2{kind}_bis
                        and werk2{kind}_{kind}_id = {kind}_id
                    join werk
                        on {version} between werk_von and werk_bis
                        and werk_id = werk2{kind}_werk_id
                    """
            where = rf"""
                    where {version} between {kind}_von and {kind}_bis
                    """ + where
        elif kind in {'ort', 'person'}:
            if kind == 'ort':
                sql += r"""
                        ort_id, ort_name, ort_orden, ort_gattung
                        """
                where += r"""
                        and _quellenort = 1
                        order by ort_name, ort_orden, ort_gattung
                        """
            elif kind == 'person':
                sql += r"""
                        person_id, person_art, person_hauptname, person_nebenname
                        """
                where += r"""
                        order by person_hauptname
                        """
            sql += rf"""
                    from {kind}
                    join _index_{kind}
                        on _index_{kind}._eigen_id = {kind}_id
                        and _index_{kind}._werk_id <> 0
                    join werk
                        on {version} between werk_von and werk_bis
                        and werk_id = _werk_id
                    """
            where = rf"""
                    where {version} between {kind}_von and {kind}_bis
                    """ + where
        elif kind == 'werk':
            sql += rf"""
                    werk_id,
                    werk_haupttitel,
                    werk_nebentitel,
                    IFNULL(person_id, '') as person_id,
                    IFNULL(person_hauptname, '') as person_hauptname
                    from {kind}
                    left join werk2person
                        on {version} between werk2person_von and werk2person_bis
                        and werk2person_werk_id = werk_id
                    left join person
                        on {version} between person_von and person_bis
                        and person_id = werk2person_person_id
                    """
            where += rf"""
                    where {version} between {kind}_von and {kind}_bis
                    and werk_id <> 1
                    order by werk_haupttitel, person_hauptname
                    """
        else:
            return
        if term:
            sql += rf"""
                    join _index_ft
                        on ft_text like '%{term}%' COLLATE utf8mb4_german2_ci
                        and ft_desc = {desc}
                        and ft_art = 0 -- werk
                        and ft_id = werk_id
                    """
        if bericht_von != 0:
            sql += rf"""
                    join zeit as bzeit
                        on {version} between bzeit.zeit_von and bzeit.zeit_bis
                        and bzeit.zeit_werk_id = werk_id
                        and bzeit.zeit_art = 'bericht'
                        and bzeit.zeit_ende <> 0
                        and ({bericht_von} <= bzeit.zeit_ende and bzeit.zeit_beginn <= {bericht_bis})
                    """
        if entstehung_von != 0:
            sql += rf"""
                    join zeit as ezeit
                        on {version} between ezeit.zeit_von and ezeit.zeit_bis
                        and ezeit.zeit_werk_id = werk_id
                        and ezeit.zeit_art = 'entstehung'
                        and ezeit.zeit_ende <> 0
                        and ({entstehung_von} <= ezeit.zeit_ende and ezeit.zeit_beginn <= {entstehung_bis})
                    """
        for ander in ('gattung', 'region', 'schlagwort', 'sprache'):
            if ander == kind: continue
            if ander not in selects: continue
            if not selects[ander]: continue
            ids = ', '.join(map(str, selects[ander]))
            sql += rf"""
                    join werk2{ander}
                        on {version} between werk2{ander}_von and werk2{ander}_bis
                        and werk2{ander}_werk_id = werk_id
                    join {ander}
                        on {version} between {ander}_von and {ander}_bis
                        and {ander}_id = werk2{ander}_{ander}_id
                        and {ander}_id in ({ids})
                    """
        for ander in ('ort', 'person', 'werk'):
            if ander == kind: continue
            if ander not in selects: continue
            if not selects[ander]: continue
            ids = ', '.join(map(str, selects[ander]))
            sql += rf"""
                    join _index_{ander}
                        on _index_{ander}._werk_id = werk_id
                        and _index_{ander}._eigen_id in ({ids})
                    """
            if ander == 'ort':
                sql += r"""
                        and _quellenort = 1
                        """
        sql += where
        with sql_io.DBCon(self.connect()) as (con, cur):
            for row in con.geteach(sql):
                yield row

    def filter_text(self, term: str, desc: str) -> 'Generator':
        term, term_re_str, term_re = web_io.get_filterterms(term)
        if not term:
            return
        desc = 1 if desc == '1' else 0
        with sql_io.DBCon(self.connect()) as (con, cur):
            for row in con.geteach(rf"""
                    * from _index_ft
                    where ft_text like %s COLLATE utf8mb4_german2_ci
                    and ft_desc = {desc}
                    """, '%' + term + '%'):
                art = 'autor' if row['ft_art'] else 'werk'
                path = f"/{art}/{row['ft_id']}"
                name = row['ft_name']
                splits = web_io.get_filtersplits(row['ft_text'], term_re)
                yield { str(rank): item for rank, item in enumerate((
                        art.title(),
                        {
                            '_': f'<a class="key" href="{path}?mark={self.q(term_re_str)}">{name}</a>',
                            's': sortxtext(name),
                        },
                        ''.join(splits),
                        )) }

    def geo(
            self,
            place_ID_string: str = '',
            person_ID_string: str = '',
            work_ID_string: str = '',
            kind: str = '',
            version: int = ENDZEIT,
            ) -> '''tuple[
                dict[
                    # the place
                    str,
                    int|float|str|list[
                        # the list of persons or the list of works
                        dict[
                            # a person or a work
                            str,
                            int|str]]]]''':
        '''
        Get all places from the database specified directly or indirectly:

        - directly with :param:`place_ID_string`, i.e. a comma-separated list of
        IDs of places. If empty, skipt it. If ``'0'``, exclude no place.
        - indirectly with :param:`person_ID_string`, i.e. a comma-separated list
        of IDs of persons who may be linked to a place. If empty, skip it.
        - indirectly with :param:`work_ID_string`, id est a comma-separated list
        of IDs of works that may be linked to a place. If empty, skip it.

        Return each place as a dictionary that is the record of the place within
        the database minus some irrelevant columns and plus …

        - a key “persons” whose value is a list of persons linked to the place –
          each person is given as a dictionary with the keys “ID” and “name”.
        - a key “works” whose value is a list of works linked to the place – and
          each work is given as a dictionary with the keys “ID” and “name”.
        '''
        places = {}
        base_sql = """
                DISTINCT
                    ort_id,
                    ort_breite,
                    ort_länge,
                    ort_name,
                    ort_normverweis,
                    ort_orden,
                    ort_gattung,
                """
        with sql_io.DBCon(self.connect()) as (con, cur):
            num_re = re.compile(r'\A\d+(,\d+)*\Z')
            if num_re.match(place_ID_string):
                sql = base_sql + """
                            person_id,
                            person_hauptname
                        from ort
                        join _index_ort
                            on ort.ort_id = _index_ort._eigen_id
                        """
                if kind == 'quellenort':
                    sql += """
                            and _index_ort._quellenort = 1
                            """
                sql += rf"""
                        join person
                            on {version} between person_von and person_bis
                            and _index_ort._person_id = person.person_id
                        where {version} between ort_von and ort_bis
                        and ort_breite <> 0
                        """
                if kind == 'kloster':
                    sql += """
                            and ort_gattung = 'Kloster'
                            """
                if place_ID_string != '0':
                    sql += rf"""
                            and ort_id in ({place_ID_string})
                            """
                sql += """
                        order by person_hauptname
                        """
                for row in con.geteach(sql):
                    if row['ort_id'] not in places:
                        places[row['ort_id']] = row
                        places[row['ort_id']]['persons'] = []
                        places[row['ort_id']]['works'] = []
                    places[row['ort_id']]['persons'].append({
                            'ID': row['person_id'],
                            'name': row['person_hauptname'],
                            })
                sql = base_sql + """
                            werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from ort
                        join _index_ort
                            on ort.ort_id = _index_ort._eigen_id
                        """
                if kind == 'quellenort':
                    sql += """
                            and _index_ort._quellenort = 1
                            """
                sql += rf"""
                        join werk
                            on {version} between werk_von and werk_bis
                            and _index_ort._werk_id = werk.werk_id
                        left join werk2person
                            on {version} between
                                werk2person_von and werk2person_bis
                            and werk_id = werk2person_werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and werk2person_person_id = person_id
                        where {version} between ort_von and ort_bis
                        and ort_breite <> 0
                        """
                if kind == 'kloster':
                    sql += """
                            and ort_gattung = 'Kloster'
                            """
                if place_ID_string != '0':
                    sql += rf"""
                            and ort_id in ({place_ID_string})
                            """
                sql += """
                        order by werk_haupttitel, person_hauptname
                        """
                for row in con.geteach(sql):
                    if row['ort_id'] not in places:
                        places[row['ort_id']] = row
                        places[row['ort_id']]['persons'] = []
                        places[row['ort_id']]['works'] = []
                    places[row['ort_id']]['works'].append({
                            'ID': row['werk_id'],
                            'name': row['werk_haupttitel'],
                            'person': row['person_hauptname'],
                            })
            elif num_re.match(person_ID_string):
                for row in con.geteach(base_sql + rf"""
                            person_id,
                            person_hauptname
                        from ort
                        join _index_ort on ort.ort_id = _index_ort._eigen_id
                        join person
                            on {version} between person_von and person_bis
                            and _index_ort._person_id = person.person_id
                            and person.person_id in ({person_ID_string})
                        where {version} between ort_von and ort_bis
                        and ort_breite <> 0
                        """):
                    if row['ort_id'] not in places:
                        places[row['ort_id']] = row
                        places[row['ort_id']]['persons'] = []
                        places[row['ort_id']]['works'] = []
                    places[row['ort_id']]['persons'].append({
                            'ID': row['person_id'],
                            'name': row['person_hauptname'],
                            })
            elif num_re.match(work_ID_string):
                for row in con.geteach(base_sql + rf"""
                            werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from ort
                        join _index_ort
                            on ort.ort_id = _index_ort._eigen_id
                            and _index_ort._quellenort = 1
                        join werk
                            on {version} between werk_von and werk_bis
                            and _index_ort._werk_id = werk.werk_id
                            and werk.werk_id in ({work_ID_string})
                        left join werk2person
                            on {version} between werk2person_von and werk2person_bis
                            and werk_id = werk2person_werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and werk2person_person_id = person_id
                        where {version} between ort_von and ort_bis
                        and ort_breite <> 0
                        order by werk_haupttitel, person_hauptname
                        """):
                    if row['ort_id'] not in places:
                        places[row['ort_id']] = row
                        places[row['ort_id']]['persons'] = []
                        places[row['ort_id']]['works'] = []
                    places[row['ort_id']]['works'].append({
                            'ID': row['werk_id'],
                            'name': row['werk_haupttitel'],
                            'person': row['person_hauptname'],
                            })
        return tuple(places.values())

    def get_gnd_person_map(
            self,
            version: int = ENDZEIT,
            ) -> 'dict[str, list[str]]':
        version = int(version)
        base_url = self.config['ids']['url']
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(rf"""
                    (
                        SELECT person_id, person_art, person_gnd
                        from person
                        where {version} between person_von and person_bis
                        and person_gnd <> ''
                        and person_art = 'autor'
                    )
                    UNION
                    (
                        SELECT person_id, person_art, person_gnd
                        from person
                        join _index_person on person_id = _eigen_id
                        where {version} between person_von and person_bis
                        and person_gnd <> ''
                        and person_art <> 'autor'
                        and (
                            _person_id <> 0
                            or _werk_id <> 0
                        )
                    )
                    """)
            return {
                    row['person_gnd']:
                        f"{base_url}/autor/{row['person_id']}"
                        if row['person_art'] == 'autor' else
                        f"{base_url}/person/{row['person_id']}"
                    for row in iter(cur.fetchone, None)
                    }

    def get_hsc_urls(
            self,
            version: int = ENDZEIT,
            ) -> 'Generator[tuple[str, str]]':
        version = int(version)
        work_ids = []
        with sql_io.DBCon(self.connect()) as (con, cur):
            for item in con.geteach(rf"""
                        lit_verweis,
                        werk2lit_werk_id
                    from lit
                    join werk2lit
                        on {version} between werk2lit_von and werk2lit_bis
                        and werk2lit_lit_id = lit_id
                    where
                        {version} between lit_von and lit_bis
                        and lit_verweis like '%handschriftencensus.de/werke/%'
                    """):
                yield (
                        f"{self.config['ids']['url']}/werk/{item['werk2lit_werk_id']}",
                        item['lit_verweis'],
                        )

    def get_index(
            self,
            kind: str,
            roles: 'set[str]',
            item_id: int = 0,
            version: int = ENDZEIT,
            welfenfond_re = re.compile(
                r'(?<=August Bibliothek, Cod. Guelf.)\s*(\S+(?: \(\S+\))?)\s*(.*)'),
            ) -> 'Generator[dict[str, str | dict[str, str | int]]]':
        '''
        Yield database items of a certain :param:`kind`.
        Which of the items are given and how depends on :param:`roles`.
        If :param:`item_id` is not ``0``, this information is used to restrict
        the returned rows to the rows related to the item :param:`item_id`.
        The return format is apt for converting into datatable-compatible JSON.
        '''
        version = int(version)
        with sql_io.DBCon(self.connect()) as (con, cur):
            if not roles and item_id and kind in {
                    'heilig', 'hs', 'ort', 'person', 'quellenort'}:
                table = {
                        'heilig': '_index_person',
                        'hs': '_index_zeuge',
                        'ort': '_index_ort',
                        'person': '_index_person',
                        'quellenort': '_index_ort',
                        }[kind]
                sql = rf"""
                        DISTINCT
                        person_id,
                        person_hauptname
                        from {table}
                        join person
                            on {version} between person_von and person_bis
                            and person_id = _person_id
                        where _eigen_id = %s and _person_id <> 0
                        """
                if kind == 'heilig':
                    sql += """
                            and _heilig = 1
                            """
                elif kind == 'quellenort':
                    sql += """
                            and _quellenort = 1
                            """
                sql += """
                        order by person_hauptname
                        """
                for row in con.geteach(sql, item_id):
                    eigen_id = row[f'person_id']
                    term = row['person_hauptname']
                    if not term:
                        continue
                    row = { str(rank): item for rank, item in enumerate((
                            'Autor',
                            {
                                '_': f'''<a href="/autor/{eigen_id}">{term}</a>''',
                                's': sortxtext(term),
                            },
                            )) }
                    yield row
                sql = rf"""
                            DISTINCT
                            werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                            from {table}
                            join werk on {version} between werk_von and werk_bis
                                and werk_id = _werk_id
                            left join werk2person
                                on {version} between werk2person_von and werk2person_bis
                                and werk2person_werk_id = werk_id
                            left join person
                                on {version} between person_von and person_bis
                                and person_id = werk2person_person_id
                            where _eigen_id = %s and _werk_id <> 0
                            """
                if kind == 'heilig':
                    sql += """
                            and _heilig = 1
                            """
                elif kind == 'quellenort':
                    sql += """
                            and _quellenort = 1
                            """
                sql += """
                        order by werk_haupttitel, person_hauptname
                        """
                for row in con.geteach(sql, item_id):
                    eigen_id = row[f'werk_id']
                    term = row['werk_haupttitel']
                    if not term:
                        continue
                    term = f'<cite>{term}</cite>'
                    if row['person_hauptname']:
                        term += f''' ({row['person_hauptname']})'''
                    row = { str(rank): item for rank, item in enumerate((
                            'Werk',
                            {
                                '_': f'''<a href="/werk/{eigen_id}">{term}</a>''',
                                's': sortxtext(term),
                            },
                            )) }
                    yield row
            elif not roles and item_id and kind == 'schlagwort':
                for row in con.geteach(rf"""
                        DISTINCT
                        werk_id,
                        werk_haupttitel,
                        IFNULL(person_hauptname, '') as person_hauptname
                        from werk2schlagwort
                        join werk on {version} between werk_von and werk_bis
                            and werk_id = werk2schlagwort_werk_id
                        left join werk2person
                            on {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        where {version} between werk2schlagwort_von and werk2schlagwort_bis
                        and werk2schlagwort_schlagwort_id = %s
                        order by werk_haupttitel, person_hauptname
                        """, item_id):
                    eigen_id = row[f'werk_id']
                    term = row['werk_haupttitel']
                    if not term:
                        continue
                    term = f'<cite>{term}</cite>'
                    if row['person_hauptname']:
                        term += f''' ({row['person_hauptname']})'''
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'''<a href="/werk/{eigen_id}">{term}</a>''',
                                's': sortxtext(term),
                            },
                            )) }
                    yield row
            elif not roles and kind in {'autograph', 'unediert', 'unikat'}:
                for row in con.geteach(rf"""
                        DISTINCT
                            _werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from _index_werkbezug
                        join werk on {version} between werk_von and werk_bis
                            and werk_id = _werk_id
                        left join werk2person
                            on {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        where _art = %s
                        order by werk_haupttitel, person_hauptname
                        """, kind):
                    eigen_id = row['_werk_id']
                    term = row['werk_haupttitel']
                    term = f'<cite>{term}</cite>'
                    if row['person_hauptname']:
                        term += f''' ({row['person_hauptname']})'''
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a href="/werk/{eigen_id}">{term}</a>',
                                's': sortxtext(term),
                            },
                            )) }
                    yield row
            elif not roles and kind == 'autor':
                for row in con.geteach(rf"""
                        person_id, person_hauptname, person_nebenname, person_gnd
                        from person where {version} between person_von and person_bis
                        and person_art = 'autor'
                        """):
                    eigen_id = row['person_id']
                    hauptname = row['person_hauptname']
                    if not hauptname:
                        continue
                    nebenname = row['person_nebenname']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a href="/autor/{eigen_id}">{hauptname}</a>',
                                's': sortxtext(hauptname),
                            },
                            {
                                '_': nebenname,
                                's': sortxtext(nebenname),
                            },
                            row['person_gnd'],
                            )) }
                    yield row
            elif not roles and kind in {'heilig', 'person'}:
                sql = rf"""
                        DISTINCT
                            _eigen_id,
                            person_art,
                            person_gnd,
                            person_hauptname,
                            COUNT(DISTINCT IF(_person_id = 0, NULL, _person_id)) as num_person,
                            COUNT(DISTINCT IF(_werk_id = 0, NULL, _werk_id)) as num_werk
                        from _index_person
                        join person on {version} between person_von and person_bis
                            and person_id = _eigen_id
                            and person_hauptname <> ''
                        """
                if kind == 'heilig':
                    sql += r" where _heilig = 1 "
                sql += " group by _eigen_id "
                for row in con.geteach(sql):
                    eigen_id = row['_eigen_id']
                    art = 'emph' if row['person_art'] == 'autor' else ''
                    name = row['person_hauptname']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a class="{art}" href="/{kind}/{eigen_id}">{name}</a>',
                                's': sortxtext(name),
                            },
                            row['person_gnd'],
                            row['num_person'],
                            row['num_werk'],
                            )) }
                    yield row
            elif not roles and kind == 'hs':
                for row in con.geteach(r"""
                        DISTINCT
                            _eigen_id,
                            _term,
                            COUNT(DISTINCT IF(_person_id = 0, NULL, _person_id)) as num_person,
                            COUNT(DISTINCT IF(_werk_id = 0, NULL, _werk_id)) as num_werk
                        from _index_zeuge where _hs = 1
                        group by _eigen_id
                        """):
                    eigen_id = row['_eigen_id']
                    term = row['_term']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a href="/hs/{eigen_id}">{term}</a>{for_filter(term)}',
                                's': sortxtext(
                                    welfenfond_re.sub(r' \g<2> \g<1>', term)),
                            },
                            row['num_person'],
                            row['num_werk'],
                            )) }
                    yield row
            elif not roles and kind == 'incipit':
                for row in con.geteach(rf"""
                        DISTINCT
                            _term,
                            _werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from _index_werkbezug
                        join werk on {version} between werk_von and werk_bis
                            and werk_id = _werk_id
                        left join werk2person
                            on {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        where _art = 'incipit'
                        order by _term ASC, werk_haupttitel ASC
                        """):
                    werk_id = row['_werk_id']
                    term = row['_term']
                    titel = row['werk_haupttitel']
                    titel = f'<cite>{titel}</cite>'
                    if row['person_hauptname']:
                        titel += f''' ({row['person_hauptname']})'''
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': term,
                                's': sortxtext(term),
                            },
                            {
                                '_': f'<a href="/werk/{werk_id}">{titel}</a>',
                                's': sortxtext(titel),
                            },
                            )) }
                    yield row
            elif not roles and kind in {'kloster', 'ort', 'quellenort'}:
                sql = rf"""
                        DISTINCT
                            ort_id,
                            ort_name,
                            ort_orden,
                            ort_gattung,
                            COUNT(DISTINCT IF(_person_id = 0, NULL, _person_id)) as num_person,
                            COUNT(DISTINCT IF(_werk_id = 0, NULL, _werk_id)) as num_werk
                        from _index_ort
                        join ort on {version} between ort_von and ort_bis
                            and ort_id = _eigen_id
                        """
                if kind == 'kloster':
                    sql += r"""
                            and ort_gattung = 'Kloster'
                            """
                elif kind == 'quellenort':
                    sql += r"""
                            where _quellenort = 1
                            """
                sql += r"""
                        group by _eigen_id
                        """
                # ‘ort’ and ‘kloster’ are ‘ort’, but ‘quellenort’ remains:
                case = 'quellenort' if kind == 'quellenort' else 'ort'
                for row in con.geteach(sql):
                    eigen_id = row['ort_id']
                    term = row['ort_name']
                    byterm = row['ort_orden']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a href="/{case}/{eigen_id}">{term}</a>{for_filter(term)}',
                                's': sortxtext(term),
                            },
                            {
                                '_': byterm,
                                's': sortxtext(byterm),
                            },
                            row['num_person'],
                            row['num_werk'],
                            )) }
                    yield row
            elif not roles and kind == 'schlagwort':
                for row in con.geteach(rf"""
                        DISTINCT
                            schlagwort_id,
                            schlagwort_bezeichnung,
                            COUNT(DISTINCT IF(werk2schlagwort_werk_id = 0, NULL, werk2schlagwort_werk_id)) as num_werk
                        from schlagwort
                        join werk2schlagwort on {version} between werk2schlagwort_von and werk2schlagwort_bis
                            and werk2schlagwort_schlagwort_id = schlagwort_id
                        where {version} between schlagwort_von and schlagwort_bis
                        group by werk2schlagwort_schlagwort_id
                        """):
                    eigen_id = row['schlagwort_id']
                    term = row['schlagwort_bezeichnung']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a href="/schlagwort/{eigen_id}">{term}</a>',
                                's': sortxtext(term),
                            },
                            row['num_werk'],
                            )) }
                    yield row
            elif not roles and kind in {'werk', 'werk_neu'}:
                sql = rf"""
                            werk_id,
                            werk_haupttitel,
                            werk_nebentitel,
                            IFNULL(person_id, '') as person_id,
                            IFNULL(person_hauptname, '') as person_hauptname,
                            IFNULL(group_concat(DISTINCT _term order by _term SEPARATOR '; '), '') as werk_andertitel
                        from werk
                        left join werk2person
                            on {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        left join _index_werk
                            on _term <> '' and _eigen_id = werk_id
                        where {version} between werk_von and werk_bis
                        """
                if kind == 'werk_neu':
                    sql += """
                        group by werk_id, werk_haupttitel
                        order by werk_id DESC
                        LIMIT 100
                        """
                else:
                    sql += """
                        group by werk_haupttitel, werk_id
                        order by werk_haupttitel, person_hauptname
                        """
                for num, row in enumerate(con.getall(sql), start = 1):
                    haupttitel = row['werk_haupttitel']
                    eigen_id = row['werk_id']
                    if not haupttitel:
                        continue
                    nebentitel = row['werk_nebentitel']
                    andertitel = row['werk_andertitel']
                    autor_id = row['person_id']
                    autor_name = row['person_hauptname']
                    prefix = f'{num}. ' if kind == 'werk_neu' else ''
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'{prefix}<a href="/werk/{eigen_id}"><cite>{haupttitel}</cite></a>',
                                's': num if kind == 'werk_neu'
                                    else sortxtext(haupttitel),
                            },
                            {
                                '_': nebentitel,
                                's': sortxtext(nebentitel),
                            },
                            {
                                '_': andertitel,
                                's': sortxtext(andertitel),
                            },
                            {
                                '_': f'<a href="/autor/{autor_id}">{autor_name}</a>'
                                    if autor_name else '',
                                's': sortxtext(autor_name)
                                    if autor_name else '',
                            },
                            )) }
                    yield row
            elif 'editor' in roles and kind == 'autor':
                for row in con.geteach(r"""
                            person_id,
                            person_gnd,
                            person_hauptname,
                            person_nebenname
                        from person
                        where person_von = 0
                        and person_art = 'autor'
                        order by person_hauptname
                        """):
                    person_id = row['person_id']
                    gnd = row['person_gnd']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': row['person_hauptname'],
                                's': sortxtext(row['person_hauptname']),
                            },
                            row['person_nebenname'] + (
                                f' (<a href="http://d-nb.info/gnd/{gnd}">{gnd}</a>)'
                                if gnd else ''),
                            f'<form><input type="radio" name="{person_id}" value="Hl"/> / <input type="radio" name="{person_id}" value="" checked=""/></form>',
                            f'<button onclick="s(this, {person_id})">📋</button>',
                            f'<a class="key" href="/eingabe/autor/{person_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = person_id
                    yield row
            elif 'editor' in roles and kind == 'bibliothek':
                for row in con.geteach(r"""
                            bibliothek_id,
                            bibliothek_ort,
                            bibliothek_institution,
                            bibliothek_isil
                        from bibliothek
                        where bibliothek_von = 0
                        order by bibliothek_ort, bibliothek_institution
                        """):
                    bibliothek_id = row['bibliothek_id']
                    text = f"{row['bibliothek_ort']}, {row['bibliothek_institution']}"
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': text,
                                's': sortxtext(text),
                            },
                            f'<button onclick="s(this, {bibliothek_id})">📋</button>',
                            f'<a class="key" href="/eingabe/bibliothek/{bibliothek_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = bibliothek_id
                    yield row
            elif 'editor' in roles and kind == 'gattung':
                for row in con.geteach(r"""
                            gattung_id,
                            gattung_bezeichnung
                        from gattung
                        where gattung_von = 0
                        order by gattung_bezeichnung
                        """):
                    gattung_id = row['gattung_id']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': row['gattung_bezeichnung'],
                                's': sortxtext(row['gattung_bezeichnung']),
                            },
                            f'<button onclick="s(this, {gattung_id})">📋</button>',
                            f'<a class="key" href="/eingabe/gattung/{gattung_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = gattung_id
                    yield row
            elif 'editor' in roles and kind == 'hs':
                for row in con.geteach(r"""
                            lit_id,
                            lit_signatur,
                            IFNULL(bibliothek_ort, '') as bibliothek_ort,
                            IFNULL(bibliothek_institution, '') as bibliothek_institution
                        from lit
                        left join bibliothek
                            on bibliothek_von = 0
                            and bibliothek_id = lit_bibliothek_id
                        where lit_von = 0
                        and lit_art = 'hs'
                        order by bibliothek_ort, bibliothek_institution, lit_signatur
                        """):
                    lit_id = row['lit_id']
                    ort = f"{row['bibliothek_ort']}, {row['bibliothek_institution']}, {row['lit_signatur']}"
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': ort,
                                's': sortxtext(
                                    welfenfond_re.sub(r' \g<2> \g<1>', ort)),
                            },
                            f'<button onclick="s(this, {lit_id})">📋</button>',
                            f'<a class="key" href="/eingabe/hs/{lit_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = lit_id
                    yield row
            elif 'editor' in roles and kind == 'ink':
                for row in con.geteach(r"""
                            lit.lit_id as lit_id,
                            lit.lit_ort as lit_ort,
                            lit.lit_jahr as lit_jahr,
                            IFNULL(litrhema.lit_verweis, '') as lit_verweis
                        from lit
                        left join lit2lit
                            on lit2lit_von = 0
                            and lit2lit_thema_lit_id = lit.lit_id
                            and lit2lit_art = 'istc'
                        left join lit as litrhema
                            on litrhema.lit_von = 0
                            and litrhema.lit_id = lit2lit_rhema_lit_id
                        where lit.lit_von = 0
                        and lit.lit_art = 'ink'
                        order by lit_verweis
                        """):
                    lit_id = row['lit_id']
                    text = f"{row['lit_verweis']}, {row['lit_ort']}, {row['lit_jahr']}"
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': text,
                                's': sortxtext(text),
                            },
                            f'<button onclick="s(this, {lit_id})">📋</button>',
                            f'<a class="key" href="/eingabe/ink/{lit_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = lit_id
                    yield row
            elif 'editor' in roles and kind == 'mono':
                for row in con.geteach(r"""
                            lit_id,
                            lit_abkürzung,
                            lit_haupttitel,
                            lit_untertitel,
                            lit_teilnr,
                            lit_reihentitel,
                            lit_reihennr,
                            lit_jahr,
                            IFNULL((
                                select group_concat(person_hauptname order by person2lit_rang SEPARATOR ' – ')
                            	from person2lit
                            	left join person
                                    on person_von = 0
                                    and person_id = person2lit_person_id
                                where person2lit_von = 0
                                and person2lit_lit_id = lit_id
                                and person2lit_art = 'autor'
                                group by person2lit_lit_id
                                ), '') as autoren
                        from lit
                        where lit_von = 0
                        and lit_art = 'mono'
                        order by autoren
                        """):
                    lit_id = row['lit_id']
                    untertitel = row['lit_untertitel']
                    titel = self.get_titel(row)
                    row = { str(rank): item for rank, item in enumerate((
                            f"{row['autoren']} ({row['lit_jahr']})"
                                if row['autoren'] else
                            row['lit_abkürzung'] or
                            f"{row['lit_haupttitel']} ({row['lit_jahr']})",
                            ' · '.join(filter(None, (
                                titel,
                                row['lit_reihentitel'],
                                row['lit_reihennr'],
                                ))),
                            f'<button onclick="s(this, {lit_id})">📋</button>',
                            f'<a class="key" href="/eingabe/mono/{lit_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = lit_id
                    yield row
            elif 'editor' in roles and kind == 'ort':
                for row in con.geteach(r"""
                            ort_id,
                            ort_normverweis,
                            ort_name,
                            ort_orden
                        from ort
                        where ort_von = 0
                        order by ort_name
                        """):
                    ort_id = row['ort_id']
                    normverweis = row['ort_normverweis']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': row['ort_name'],
                                's': sortxtext(row['ort_name']),
                            },
                            ' · '.join(filter(None, (
                                row['ort_orden'],
                                normverweis or '',
                                ))),
                            f'<form><input type="radio" name="{ort_id}" value="Q"/> / <input type="radio" name="{ort_id}" value="" checked=""/></form>',
                            f'<button onclick="s(this, {ort_id})">📋</button>',
                            f'<a class="key" href="/eingabe/ort/{ort_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = ort_id
                    yield row
            elif 'editor' in roles and kind == 'person':
                for row in con.geteach(r"""
                            person_id,
                            person_gnd,
                            person_hauptname,
                            person_nebenname
                        from person
                        where person_von = 0
                        and person_art = 'erwähnt'
                        order by person_hauptname
                        """):
                    person_id = row['person_id']
                    gnd = row['person_gnd']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': row['person_hauptname'],
                                's': sortxtext(row['person_hauptname']),
                            },
                            row['person_nebenname'] + (
                                f' (<a href="http://d-nb.info/gnd/{gnd}">{gnd}</a>)'
                                if gnd else ''),
                            f'<form><input type="radio" name="{person_id}" value="Hl"/> / <input type="radio" name="{person_id}" value="" checked=""/></form>',
                            f'<button onclick="s(this, {person_id})">📋</button>',
                            f'<a class="key" href="/eingabe/person/{person_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = person_id
                    yield row
            elif 'editor' in roles and kind == 'region':
                for row in con.geteach(r"""
                            region_id,
                            region_spät,
                            region_bezeichnung
                        from region
                        where region_von = 0
                        order by region_bezeichnung
                        """):
                    region_id = row['region_id']
                    text = row['region_bezeichnung'] + (
                            ' (SpätMA)' if row['region_spät'] else ' (FrühMA)')
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': text,
                                's': sortxtext(text),
                            },
                            f'<button onclick="s(this, {region_id})">📋</button>',
                            f'<a class="key" href="/eingabe/region/{region_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = region_id
                    yield row
            elif 'editor' in roles and kind in {'sammelband', 'zs'}:
                for row in con.geteach(r"""
                            lit_id,
                            lit_abkürzung,
                            lit_haupttitel,
                            lit_untertitel,
                            lit_teilnr,
                            lit_reihentitel,
                            lit_reihennr,
                            lit_jahr
                        from lit
                        where lit_von = 0
                        and lit_art = %s
                        order by lit_abkürzung, lit_haupttitel
                        """, kind):
                    lit_id = row['lit_id']
                    titel = self.get_titel(row)
                    if kind == 'sammelband':
                        row = { str(rank): item for rank, item in enumerate((
                                titel,
                                ' · '.join(filter(None, (
                                    row['lit_jahr'],
                                    row['lit_reihentitel'],
                                    row['lit_reihennr'],
                                    ))),
                                f'<button onclick="s(this, {lit_id})">📋</button>',
                                f'<a class="key" href="/eingabe/sammelband/{lit_id}">✎</a>',
                                )) }
                    else:
                        row = { str(rank): item for rank, item in enumerate((
                                titel,
                                row['lit_haupttitel'],
                                f'<button onclick="s(this, {lit_id})">📋</button>',
                                f'<a class="key" href="/eingabe/zs/{lit_id}">✎</a>',
                                )) }
                    row['DT_RowId'] = lit_id
                    yield row
            elif 'editor' in roles and kind in {'sammelbandaufsatz', 'zsaufsatz'}:
                for row in con.geteach(r"""
                            lit.lit_id as lit_id,
                            lit.lit_abkürzung as lit_abkürzung,
                            lit.lit_haupttitel as lit_haupttitel,
                            lit.lit_untertitel as lit_untertitel,
                            lit.lit_reihennr as lit_reihennr,
                            lit.lit_stelle as lit_stelle,
                            lit.lit_jahr as lit_jahr,
                            litrhema.lit_abkürzung as bandabkürzung,
                            litrhema.lit_haupttitel as bandhaupttitel,
                            litrhema.lit_untertitel as banduntertitel,
                            litrhema.lit_teilnr as lit_teilnr,
                            litrhema.lit_reihentitel as lit_reihentitel,
                            litrhema.lit_reihennr as bandreihennr,
                            litrhema.lit_jahr as bandjahr,
                            IFNULL((
                                select group_concat(person_hauptname order by person2lit_rang SEPARATOR ' – ')
                            	from person2lit
                            	left join person
                                    on person_von = 0
                                    and person_id = person2lit_person_id
                                where person2lit_von = 0
                                and person2lit_lit_id = lit.lit_id
                                and person2lit_art = 'autor'
                                group by person2lit_lit_id
                                ), '') as autoren
                        from lit
                        join lit2lit
                            on lit2lit_von = 0
                            and lit2lit_thema_lit_id = lit.lit_id
                            and lit2lit_art = 'in'
                        join lit as litrhema
                            on litrhema.lit_von = 0
                            and litrhema.lit_id = lit2lit_rhema_lit_id
                        where lit.lit_von = 0
                        and lit.lit_art = %s
                        order by lit.lit_haupttitel, bandhaupttitel
                        """, kind):
                    lit_id = row['lit_id']
                    autoren = row['autoren']
                    titel = self.get_titel(row)
                    bandtitel = row['bandabkürzung']
                    if not bandtitel:
                        bandtitel = '. '.join(filter(None, (
                                row['bandhaupttitel'], row['banduntertitel'])))
                    if kind == 'sammelbandaufsatz':
                        row = { str(rank): item for rank, item in enumerate((
                                f"{row['autoren']}, {titel}, {bandtitel}, {row['lit_stelle']}",
                                ' · '.join(filter(None, (
                                    row['bandjahr'],
                                    row['lit_reihentitel'],
                                    row['bandreihennr'],
                                    ))),
                                f'<button onclick="s(this, {lit_id})">📋</button>',
                                f'<a class="key" href="/eingabe/sammelbandaufsatz/{lit_id}">✎</a>',
                                )) }
                    else:
                        text = f"{row['autoren']}, {titel}, {bandtitel}, {row['lit_reihennr']}, {row['lit_jahr']}, {row['lit_stelle']}"
                        row = { str(rank): item for rank, item in enumerate((
                                {
                                    '_': text,
                                    's': sortxtext(text),
                                },
                                f'<button onclick="s(this, {lit_id})">📋</button>',
                                f'<a class="key" href="/eingabe/zsaufsatz/{lit_id}">✎</a>',
                                )) }
                    row['DT_RowId'] = lit_id
                    yield row
            elif 'editor' in roles and kind == 'schlagwort':
                for row in con.geteach(r"""
                            schlagwort_id,
                            schlagwort_bezeichnung
                        from schlagwort
                        where schlagwort_von = 0
                        order by schlagwort_bezeichnung
                        """):
                    schlagwort_id = row['schlagwort_id']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': row['schlagwort_bezeichnung'],
                                's': sortxtext(row['schlagwort_bezeichnung']),
                            },
                            f'<button onclick="s(this, {schlagwort_id})">📋</button>',
                            f'<a class="key" href="/eingabe/schlagwort/{schlagwort_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = schlagwort_id
                    yield row
            elif 'editor' in roles and kind == 'sprache':
                for row in con.geteach(r"""
                            sprache_id,
                            sprache_bezeichnung
                        from sprache
                        where sprache_von = 0
                        order by sprache_bezeichnung
                        """):
                    sprache_id = row['sprache_id']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': row['sprache_bezeichnung'],
                                's': sortxtext(row['sprache_bezeichnung']),
                            },
                            f'<button onclick="s(this, {sprache_id})">📋</button>',
                            f'<a class="key" href="/eingabe/sprache/{sprache_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = sprache_id
                    yield row
            elif 'editor' in roles and kind == 'werk':
                for row in con.geteach(r"""
                            werk_id,
                            werk_haupttitel,
                            werk_nebentitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from werk
                        left join werk2person
                            on werk2person_von = 0
                            and werk2person_werk_id = werk_id
                            and werk2person_art = 'autor'
                        left join person
                            on person_von = 0
                            and person_id = werk2person_person_id
                        where werk_von = 0
                        order by werk_haupttitel, person_hauptname
                        """):
                    werk_id = row['werk_id']
                    row = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f"<cite>{row['werk_haupttitel']}</cite>",
                                's': sortxtext(row['werk_haupttitel']),
                            },
                            f"{row['werk_nebentitel']} · {row['person_hauptname']}",
                            f'<button onclick="s(this, {werk_id})">📋</button>',
                            f'<a class="key" href="/eingabe/werk/{werk_id}">✎</a>',
                            )) }
                    row['DT_RowId'] = werk_id
                    yield row

    def get_item(
            self,
            kind: str,
            item_id: 'None|int',
            roles: 'set[str]',
            version: int = ENDZEIT,
            ) -> 'tuple[None|dict[str, Any], None|int]':
        '''
        Get the database item of :param:`item_id` of a certain :param:`kind`.
        Which of the items are given and how depends on :param:`roles`.
        The return format is apt for the use in the belonging form template.
        '''
        version = int(version)
        lit_cols = """
                lit_id,
                lit_von,
                lit_bis,
                lit_verweis,
                lit_art,
                lit_gnd,
                lit_bibliothek_id,
                lit_signatur,
                lit_abkürzung,
                lit_haupttitel,
                lit_untertitel,
                lit_teilnr,
                lit_reihentitel,
                lit_reihennr,
                lit_stelle,
                lit_auflage,
                lit_ort,
                lit_jahr,
                lit_neuauflage,
                lit_nachdruck
                """
        with sql_io.DBCon(self.connect()) as (con, cur):
            versions = set()
            if not roles and kind == 'autor':
                item = con.get(rf"""
                            *
                        from person
                        where {version} between person_von and person_bis
                            and person_id = %s
                        """, item_id)
                if not item:
                    return None, item_id
                versions.add(item['person_von'])
                item['person_werke'] = []
                for row in con.geteach(rf"""
                            werk2person_von,
                            werk_id,
                            werk_von,
                            werk_haupttitel
                        from werk2person
                        join werk on {version} between werk_von and werk_bis
                            and werk_id = werk2person_werk_id
                        where {version} between werk2person_von and werk2person_bis
                            and werk2person_person_id = %s
                        order by werk_haupttitel COLLATE utf8mb4_german2_ci
                        """, item_id):
                    item['person_werke'].append(row)
                    versions.add(row['werk2person_von'])
                    versions.add(row['werk_von'])
                item['person_lit'] = []
                for lit in con.geteach(rf"""
                            {lit_cols},
                            person2lit_von,
                            person2lit_anmerkung,
                            person2lit_stelle as lit_bezugsstelle,
                            '' as lit_sprache
                        from person2lit
                        join lit on {version} between lit_von and lit_bis
                            and lit_id = person2lit_lit_id
                        where {version} between person2lit_von and person2lit_bis
                        and person2lit_art = 'bezug'
                        and person2lit_person_id = %s
                        """, item_id):
                    item['person_lit'].append(self.get_lit(lit))
                    versions.add(lit['lit_von'])
                    versions.add(lit['person2lit_von'])
                item['person_lit'].sort(key = itemgetter(0))
                item['person_erwähnung_in_person'] = con.getall(rf"""
                        DISTINCT
                            person_id,
                            person_hauptname
                        from _index_person
                        join person on {version} between person_von and person_bis
                            and person_id = _person_id
                        where _eigen_id = %s
                        and _person_id <> %s
                        order by person_hauptname
                        """, item_id, item_id)
                item['person_erwähnung_in_werk'] = con.getall(rf"""
                        DISTINCT
                            werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from _index_person
                        join werk on {version} between werk_von and werk_bis
                            and werk_id = _werk_id
                        left join werk2person on {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = werk_id
                        left join person on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        where _eigen_id = %s
                            and not (_eigen_id = person_id and person_id is not NULL)
                        order by werk_haupttitel, person_hauptname
                        """, item_id)
                item['latest_version'] = max(versions)
                return item, item_id
            elif not roles and kind == 'werk':
                item = con.get(rf"""
                            werk_id,
                            werk_von,
                            werk_bis,
                            werk_rep,
                            werk_haupttitel,
                            werk_nebentitel,
                            werk_beschreibung,
                            werk_stand,
                            werk.alt_id as alt_id
                        from werk
                        where {version} between werk_von and werk_bis
                            and werk_id = %s
                        """, item_id)
                if not item:
                    return None, item_id
                versions.add(item['werk_von'])
                item['autoren'] = []
                for subitem in con.getall(rf"""
                            werk2person_von,
                            person_id,
                            person_von,
                            person_hauptname
                        from werk2person
                        join person on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        where {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = %s
                            and werk2person_art = 'autor'
                        order by werk2person_rang
                        """, item_id):
                    subitem['has_person_lit'] = bool(
                            con.get(rf"""
                                1
                            from person2lit
                            join lit on {version} between lit_von and lit_bis
                                and lit_id = person2lit_lit_id
                            where {version} between person2lit_von and person2lit_bis
                            and person2lit_art = 'bezug'
                            and person2lit_person_id = %s
                            """, subitem['person_id']))
                    item['autoren'].append(subitem)
                    versions.add(subitem['werk2person_von'])
                    versions.add(subitem['person_von'])
                for case in ('bericht', 'entstehung'):
                    data = []
                    for row in con.geteach(rf"""
                            DISTINCT
                                zeit_von,
                                zeit_bezeichnung
                            from zeit
                            where {version} between zeit_von and zeit_bis
                            and zeit_art = %s
                            and zeit_werk_id = %s
                            """, case, item_id):
                        data.append(row['zeit_bezeichnung'])
                        versions.add(row['zeit_von'])
                    item[f'werk_{case}szeit'] = '; '.join(data)
                for case in ('gattung', 'region', 'schlagwort', 'sprache'):
                    data = []
                    for row in con.geteach(rf"""
                            DISTINCT
                                {case}_von,
                                {case}_bezeichnung,
                                werk2{case}_von
                            from {case}
                            join werk2{case}
                                on {version} between werk2{case}_von and werk2{case}_bis
                                and werk2{case}_{case}_id = {case}_id
                                and werk2{case}_werk_id = %s
                            where {version} between {case}_von and {case}_bis
                            """, item_id):
                        data.append(row[f'{case}_bezeichnung'])
                        versions.add(row[f'{case}_von'])
                        versions.add(row[f'werk2{case}_von'])
                    item[f'werk_{case}'] = '; '.join(data)
                item['werk_zeuge'] = []
                for subitem in con.getall(rf"""
                            {lit_cols},
                            werk2lit_von,
                            werk2lit_stelle,
                            werk2lit_anmerkung,
                            IFNULL(bibliothek_von, 0) as bibliothek_von,
                            IFNULL(bibliothek_ort, '') as bibliothek_ort,
                            IFNULL(bibliothek_institution, '') as bibliothek_institution
                        from werk2lit
                        join lit on {version} between lit_von and lit_bis
                            and lit_id = werk2lit_lit_id
                        left join bibliothek
                            on {version} between bibliothek_von and bibliothek_bis
                            and bibliothek_id = lit_bibliothek_id
                        where {version} between werk2lit_von and werk2lit_bis
                        and werk2lit_art = 'zeuge'
                        and werk2lit_werk_id = %s
                        order by bibliothek_ort, bibliothek_institution, lit_signatur
                        """, item_id):
                    versions.add(subitem['lit_von'])
                    versions.add(subitem['werk2lit_von'])
                    versions.add(subitem['bibliothek_von'])
                    subitem['lit'] = []
                    for lit in con.geteach(rf"""
                                {lit_cols},
                                lit2lit_von,
                                lit2lit_art,
                                lit2lit_anmerkung,
                                lit2lit_stelle as lit_bezugsstelle
                            from lit2lit
                            join lit on {version} between lit_von and lit_bis
                                and lit_id = lit2lit_thema_lit_id
                            where {version} between lit2lit_von and lit2lit_bis
                            and lit2lit_werk_id = %s
                            and lit2lit_rhema_lit_id = %s
                            """, item_id, subitem['lit_id']):
                        subitem['lit'].append(self.get_lit(lit))
                        versions.add(lit['lit_von'])
                        versions.add(lit['lit2lit_von'])
                    subitem['lit'].sort(key = itemgetter(0))
                    item['werk_zeuge'].append(subitem)
                item['werk_zeuge'].sort(key = lambda x: (
                        x['bibliothek_ort'],
                        x['bibliothek_institution'],
                        sortxtext(x['lit_signatur']),
                        ))
                item['werk_lit'] = {}
                for lit_art in (
                        'zeugenlit',
                        'faksimile',
                        'altübersetzung',
                        'ausgabe',
                        'übersetzung',
                        'zumwerk',
                        ):
                    item['werk_lit'][lit_art] = []
                    for lit in con.geteach(rf"""
                                {lit_cols},
                                werk2lit_von,
                                werk2lit_art,
                                werk2lit_anmerkung,
                                werk2lit_rubrik,
                                werk2lit_stelle as lit_bezugsstelle,
                                IFNULL(sprache_von, 0) as lit_sprache_von,
                                IFNULL(sprache_bezeichnung, '') as lit_sprache
                            from werk2lit
                            join lit on {version} between lit_von and lit_bis
                                and lit_id = werk2lit_lit_id
                            left join sprache on {version} between sprache_von and sprache_bis
                                and sprache_id = werk2lit_sprache_id
                            where {version} between werk2lit_von and werk2lit_bis
                            and werk2lit_art = %s
                            and werk2lit_werk_id = %s
                            """, lit_art, item_id):
                        item['werk_lit'][lit_art].append(self.get_lit(lit))
                        versions.add(lit['lit_von'])
                        versions.add(lit['werk2lit_von'])
                        versions.add(lit['lit_sprache_von'])
                    item['werk_lit'][lit_art].sort(key = itemgetter(0))
                item['werk_erwähnung_in_person'] = con.getall(rf"""
                        DISTINCT
                            person_id,
                            person_hauptname
                        from _index_werk
                        join person on {version} between person_von and person_bis
                            and person_id = _person_id
                        where _eigen_id = %s
                        order by person_hauptname
                        """, item_id)
                item['werk_erwähnung_in_werk'] = con.getall(rf"""
                        DISTINCT
                            werk_id,
                            werk_haupttitel,
                            IFNULL(person_hauptname, '') as person_hauptname
                        from _index_werk
                        join werk on {version} between werk_von and werk_bis
                            and werk_id = _werk_id
                        left join werk2person
                            on {version} between werk2person_von and werk2person_bis
                            and werk2person_werk_id = werk_id
                        left join person
                            on {version} between person_von and person_bis
                            and person_id = werk2person_person_id
                        where _eigen_id = %s
                        and _werk_id <> %s
                        order by werk_haupttitel, person_hauptname
                        """, item_id, item_id)
                item['latest_version'] = max(versions)
                return item, item_id
            elif 'editor' in roles and kind in {'autor', 'person'}:
                if item_id is None:
                    cur.execute(r"""
                            insert into person (person_art) values (%s)
                            """, 'autor' if kind == 'autor' else 'erwähnt')
                    con.commit()
                    return None, cur.lastrowid
                item = con.get(r"""
                        * from person where person_von = 0 and person_id = %s
                        """, item_id)
                if item['person_art'] == 'autor':
                    item['person2lit'] = sorted(
                            con.getall(rf"""
                                *,
                                person2lit_stelle as lit_bezugsstelle
                            from person2lit
                            join lit on lit_von = 0
                                and lit_id = person2lit_lit_id
                            where person2lit_von = 0
                            and person2lit_art = 'bezug'
                            and person2lit_person_id = %s
                            -- order by person2lit_rang
                            """, item_id), key = lambda x: self.get_lit(x)[0])
                    item['werk2person'] = sorted(
                            (
                            self.get_linktext('werk', row['werk2person_werk_id']),
                            row['werk2person_werk_id'],
                            )
                            for row in con.getall(r"""
                                * from werk2person
                                where werk2person_von = 0
                                and werk2person_art = 'autor'
                                and werk2person_person_id = %s
                                """, item_id) )
                return item, item_id
            elif 'editor' in roles and kind == 'bibliothek':
                if item_id is None:
                    cur.execute(r"""
                            insert into bibliothek (bibliothek_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                return con.get(r"""
                        * from bibliothek
                        where bibliothek_von = 0
                        and bibliothek_id = %s
                        """, item_id), item_id
            elif 'editor' in roles and kind == 'gattung':
                if item_id is None:
                    cur.execute(r"""
                            insert into gattung (gattung_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                return con.get(r"""
                        * from gattung
                        where gattung_von = 0
                        and gattung_id = %s
                        """, item_id), item_id
            elif 'editor' in roles and kind in {
                    'hs',
                    'ink',
                    'mono',
                    'sammelband',
                    'sammelbandaufsatz',
                    'zs',
                    'zsaufsatz',
                    }:
                if item_id is None:
                    cur.execute(r"""
                            insert into lit (lit_art) values (%s)
                            """, kind)
                    con.commit()
                    return None, cur.lastrowid
                item = con.get(r"""
                        * from lit where lit_von = 0 and lit_id = %s
                        """, item_id)
                item['person2lit'] = con.getall(r"""
                        * from person2lit
                        where person2lit_von = 0
                        and person2lit_lit_id = %s
                        order by person2lit_rang
                        """, item_id)
                item['lit2lit'] = con.getall(r"""
                        * from lit2lit
                        join lit
                            on lit_von = 0
                            and lit_id = lit2lit_rhema_lit_id
                        where lit2lit_von = 0
                        and lit2lit_thema_lit_id = %s
                        order by lit2lit_rang
                        """, item_id)
                return item, item_id
            elif 'editor' in roles and kind == 'ort':
                if item_id is None:
                    cur.execute(r"""
                            insert into ort (ort_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                return con.get(r"""
                        * from ort where ort_von = 0 and ort_id = %s
                        """, item_id), item_id
            elif 'editor' in roles and kind == 'region':
                if item_id is None:
                    cur.execute(r"""
                            insert into region (region_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                return con.get(r"""
                        * from region
                        where region_von = 0
                        and region_id = %s
                        """, item_id), item_id
            elif 'editor' in roles and kind == 'schlagwort':
                if item_id is None:
                    cur.execute(r"""
                            insert into schlagwort (schlagwort_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                return con.get(r"""
                        * from schlagwort
                        where schlagwort_von = 0
                        and schlagwort_id = %s
                        """, item_id), item_id
            elif 'editor' in roles and kind == 'sprache':
                if item_id is None:
                    cur.execute(r"""
                            insert into sprache (sprache_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                return con.get(r"""
                        * from sprache
                        where sprache_von = 0
                        and sprache_id = %s
                        """, item_id), item_id
            elif 'editor' in roles and kind == 'werk':
                if item_id is None:
                    cur.execute(r"""
                            insert into werk (werk_id) values (null)
                            """)
                    con.commit()
                    return None, cur.lastrowid
                item = con.get(r"""
                        * from werk where werk_von = 0 and werk_id = %s
                        """, item_id)
                item['zeit'] = con.getall(r"""
                        * from zeit where zeit_von = 0 and zeit_werk_id = %s
                        order by case when zeit_beginn = 0
                            then NULL
                            else zeit_beginn
                        end
                        """, item_id)
                for table in (
                        'gattung',
                        'person',
                        'region',
                        'schlagwort',
                        'sprache',
                        ):
                    sql = rf"""
                            * from werk2{table}
                            where werk2{table}_von = 0
                            and werk2{table}_werk_id = %s
                            """
                    if table == 'lit':
                        sql += "order by werk2lit_rang"
                    item[f'werk2{table}'] = con.getall(sql, item_id)
                for case in (
                        'altübersetzung',
                        'ausgabe',
                        'faksimile',
                        'zeuge',
                        'zeugenlit',
                        'übersetzung',
                        'zumwerk',
                        ):
                    item[case] = sorted(
                            con.getall(r"""
                                *,
                                werk2lit_stelle as lit_bezugsstelle,
                                IFNULL(sprache_bezeichnung, '') as lit_sprache
                            from werk2lit
                            join lit on lit_von = 0 and lit_id = werk2lit_lit_id
                            left join sprache on sprache_von = 0
                                and sprache_id = werk2lit_sprache_id
                            where werk2lit_von = 0
                            and werk2lit_werk_id = %s
                            and werk2lit_art = %s
                            """, item_id, case),
                            key = lambda x: self.get_lit(x, version = 0)[0])
                for i, zeuge in enumerate(item['zeuge']):
                    item['zeuge'][i]['zu'] = con.getall(r"""
                            * from lit2lit
                            where lit2lit_von = 0
                            and lit2lit_werk_id = %s
                            and lit2lit_rhema_lit_id = %s
                            and lit2lit_art = 'zu'
                            order by lit2lit_rang
                            """, item_id, zeuge['werk2lit_lit_id'])
                return item, item_id
            else:
                return {}, None

    def get_item_names(
            self,
            kind: str,
            item_ids: 'Sequence[int]',
            version: int = ENDZEIT,
            ) -> 'Generator[str]':
        '''
        For the item :param:`item_id` of kind :param:`kind`,
        get a human-readable designation (name or title).
        '''
        version = int(version)
        if item_ids and all( isinstance(item_id, int) for item_id in item_ids ):
            item_ids = ', '.join(map(str, item_ids))
            with sql_io.DBCon(self.connect()) as (con, cur):
                if kind in {'heilig', 'person'}:
                    for row in con.geteach(rf"""
                                person_hauptname
                            from person
                            where {version} between person_von and person_bis
                            and person_id in ({item_ids})
                            order by person_hauptname
                            """):
                        yield row['person_hauptname']
                elif kind == 'hs':
                    for row in con.geteach(rf"""
                                lit_signatur,
                                IFNULL(bibliothek_ort, '') as bibliothek_ort,
                                IFNULL(bibliothek_institution, '') as bibliothek_institution
                            from lit
                            left join bibliothek
                                on bibliothek_von = 0
                                and bibliothek_id = lit_bibliothek_id
                            where {version} between lit_von and lit_bis
                            and lit_id in ({item_ids})
                            order by bibliothek_ort, bibliothek_institution, lit_signatur
                            """):
                        yield f"{row['bibliothek_ort']}, {row['bibliothek_institution']}, {row['lit_signatur']}"
                elif kind in {'ort', 'quellenort'}:
                    for row in con.geteach(rf"""
                                ort_name, ort_orden
                            from ort
                            where {version} between ort_von and ort_bis
                            and ort_id in ({item_ids})
                            order by ort_name
                            """):
                        orden = row['ort_orden']
                        if orden:
                            orden = f', {orden}'
                        yield f"{row['ort_name']}{orden}"
                elif kind in {'gattung', 'region', 'schlagwort', 'sprache'}:
                    for row in con.geteach(rf"""
                                {kind}_bezeichnung
                            from {kind}
                            where {version} between {kind}_von and {kind}_bis
                            and {kind}_id in ({item_ids})
                            order by {kind}_bezeichnung
                            """):
                        yield row[f'{kind}_bezeichnung']

    def get_linktext(self, table: str, item_id: int) -> str:
        '''
        Get from :param:`table` data of :param:`item_id` and
        put them together to a string that can be used as an
        informative, human-readable linktext of a link. This
        link contains the machine-readable information which
        item is meant.
        '''
        if item_id is None or table not in {
                'bibliothek',
                'gattung',
                'lit',
                'person',
                'region',
                'schlagwort',
                'sprache',
                'werk',
                }:
            return ''
        with sql_io.DBCon(self.connect()) as (con, cur):
            row = con.get(rf"""
                    * from {table} where {table}_von = 0 and {table}_id = %s
                    """, item_id)
            if not row:
                return ''
            if table == 'bibliothek':
                return f"{row['bibliothek_ort']}, {row['bibliothek_institution']}"
            elif table == 'lit':
                art = row['lit_art']
                if art == 'sonst':
                    return row['lit_abkürzung']
                elif art == 'verweis':
                    return row['lit_verweis']
                elif art == 'hs':
                    bibrow = con.get(r"""
                                bibliothek_ort,
                                bibliothek_institution
                            from bibliothek
                            where bibliothek_von = 0
                            and bibliothek_id = %s
                            """, row['lit_bibliothek_id'])
                    return f"{bibrow.get('bibliothek_ort', '')}, "\
                           f"{bibrow.get('bibliothek_institution', '')}, "\
                           f"{row['lit_signatur']}"
                elif art == 'ink':
                    istc = con.get(r"""
                                lit_verweis
                            from lit
                            join lit2lit
                                on lit2lit_von = 0
                                and lit2lit_art = 'istc'
                                and lit2lit_thema_lit_id = %s
                                and lit2lit_rhema_lit_id = lit_id
                            where lit_von = 0
                            """, item_id).get('lit_verweis', '')
                    return f"{istc}, {row['lit_ort']}, {row['lit_jahr']}"
                titel = self.get_titel(row)
                autoren = ' – '.join(
                        filter(None, (
                            r['person_hauptname'] for r in con.getall(r"""
                                    person_hauptname
                                from person2lit
                                left join person
                                    on person_von = 0
                                    and person_id = person2lit_person_id
                                where person2lit_von = 0
                                and person2lit_lit_id = %s
                                and person2lit_art = 'autor'
                                order by person2lit_rang
                                """, item_id) )))
                if art == 'mono':
                    if autoren:
                        return f"{autoren} ({row['lit_jahr']})"
                    else:
                        return row['lit_abkürzung'] or\
                                f"{row['lit_haupttitel']} ({row['lit_jahr']})"
                elif art in {'sammelband', 'zs'}:
                    return titel
                elif art in {'sammelbandaufsatz', 'zsaufsatz'}:
                    bandrow = con.get(r"""
                            * from lit
                            join lit2lit
                                on lit2lit_von = 0
                                and lit2lit_thema_lit_id = %s
                                and lit2lit_rhema_lit_id = lit_id
                                and lit2lit_art = 'in'
                            where lit_von = 0
                            """, item_id)
                    bandtitel = self.get_titel(bandrow) if bandrow else '[NN]'
                    return f"{autoren}, {titel}, {bandtitel}, {row['lit_stelle']}"
            elif table == 'person':
                return row['person_hauptname']
            elif table == 'werk':
                return row['werk_haupttitel']
            elif table in {'gattung', 'region', 'schlagwort', 'sprache'}:
                return row[f'{table}_bezeichnung']

    def get_lit(
            self,
            item: 'dict[str, str|int]',
            version: int = ENDZEIT,
            praeponenda_re: 're.Pattern[str]' =
                re.compile(r'v\. (Edd\.|Comm\.)\b'),
            ) -> '''tuple[
                tuple[str, str, str, str, str, str],
                str,
                dict[str, str|int]]''':
        '''
        Get a sortsequence and an outputstring, showing a bibliographical
        entry, from :param:`item` and return these two and :param:`item`.
        '''
        def call(term: str) -> str:
            if ', ' in term:
                last, first = term.split(', ', 1)
                return f'{first} <a class="last_name">{last}</a>'
            else:
                return f'<a class="last_name">{term}</a>'

        def get_linkstring(lit_id: int) -> str:
            terms = []
            for row in con.getall(rf"""
                    litrhema.lit_verweis as lit_verweis,
                    lit2lit_art
                    from lit
                    join lit2lit
                        on {version} between lit2lit_von and lit2lit_bis
                        and lit2lit_thema_lit_id = lit.lit_id
                    join lit as litrhema
                        on {version} between litrhema.lit_von and litrhema.lit_bis
                        and litrhema.lit_id = lit2lit_rhema_lit_id
                        and litrhema.lit_art = 'verweis'
                    where {version} between lit.lit_von and lit.lit_bis
                    and lit.lit_id = %s
                    and lit2lit_art <> 'zu'
                    """, lit_id):
                verweis = row['lit_verweis']
                verweis_art = row['lit2lit_art']
                if verweis[:7] in {'http://', 'https:/'}:
                    if verweis_art:
                        sorter = 'b' + verweis_art
                        verweis_art = verweis_art.upper()
                        verweis_art = verweis_art.replace('DIGITAL', 'digital')
                    else:
                        sorter = 'c'
                        verweis_art = '/'.join(verweis.split('/', 3)[:3])
                    terms.append((
                            sorter,
                            f'<a href="{verweis}">{verweis_art}</a>'))
                else:
                    if verweis_art in {'copinger', 'hain'}:
                        verweis_art = verweis_art.title()
                        sorter = 'a'
                    else:
                        sorter = 'c'
                    terms.append((
                            sorter + verweis_art,
                            f'{verweis_art} {verweis}'))
            return ' – '.join( term for _, term in sorted(terms) )

        def get_sorter(
                item:'dict[str, str|int]',
                lit_art: str = '',
                werk2lit_art: str = '',
                lit_haupttitel: str = '',
                autor_sort: str = '',
                ) -> 'tuple[str, str, str, str, str, str]':
            return (
                    item['lit_sprache'] if werk2lit_art in {'altübersetzung', 'übersetzung'}
                    else item['werk2lit_rubrik'] if werk2lit_art == 'zumwerk'
                    else '',
                    'a' if lit_art == 'hs'
                    else 'b' if praeponenda_re.match(lit_haupttitel)
                    else 'd' if lit_art == 'verweis'
                    else 'c',
                    sortnum(item['lit_jahr']),
                    sortxtext(item['lit_ort']) if lit_art == 'ink'
                    else sortxtext(autor_sort),
                    sortxtext(lit_haupttitel),
                    sortxtext(item['lit_signatur']) if lit_art == 'hs'
                    else '',
                    )

        version = int(version)
        with sql_io.DBCon(self.connect()) as (con, cur):
            lit_id = item['lit_id']
            lit_verweis = item['lit_verweis']
            lit_art = item['lit_art']
            werk2lit_art = item.get('werk2lit_art', '')
            if lit_art == 'hs':
                subitem = con.get(rf"""
                        bibliothek_ort,
                        bibliothek_institution
                        from bibliothek
                        where {version} between bibliothek_von and bibliothek_bis
                        and bibliothek_id = %s
                        """, item['lit_bibliothek_id'])
                term = f'''<a href="/hs/{lit_id}">{subitem['bibliothek_ort']}, {subitem['bibliothek_institution']}, {item['lit_signatur']}</a>'''
                if item['lit_bezugsstelle']:
                    term += ', ' + item['lit_bezugsstelle']
                sorter = get_sorter(item, lit_art, werk2lit_art, term)
                item['bibliothek'] = subitem
                return sorter, term, item
            lit_haupttitel = item['lit_haupttitel'] or item['lit_abkürzung']
            for case in ('autor', 'drucker', 'hg'):
                terms = [ row['person_hauptname'] for row in con.getall(rf"""
                        person_hauptname
                        from person2lit
                        join person on {version} between person_von and person_bis
                            and person_id = person2lit_person_id
                        where {version} between person2lit_von and person2lit_bis
                            and person2lit_art = '{case}'
                            and person2lit_lit_id = %s
                        order by person2lit_rang
                        """, lit_id) ]
                if case == 'autor':
                    autor_sort = ' '.join(terms)
                    autor = ' – '.join( call(term) for term in terms )
                elif case == 'drucker':
                    drucker = ' – '.join(terms)
                else:
                    editor = ' – '.join( call(term) for term in terms )
            band = {}
            bandtitel = ''
            bandeditor = ''
            links = ''
            if lit_art in {'sammelbandaufsatz', 'zsaufsatz'}:
                band = con.get(rf"""
                            litrhema.lit_id as lit_id,
                            litrhema.lit_abkürzung as lit_abkürzung,
                            litrhema.lit_haupttitel as lit_haupttitel,
                            litrhema.lit_untertitel as lit_untertitel,
                            litrhema.lit_teilnr as lit_teilnr,
                            litrhema.lit_reihentitel as lit_reihentitel,
                            litrhema.lit_reihennr as lit_reihennr,
                            litrhema.lit_auflage as lit_auflage,
                            litrhema.lit_ort as lit_ort,
                            litrhema.lit_jahr as lit_jahr,
                            litrhema.lit_neuauflage as lit_neuauflage,
                            litrhema.lit_nachdruck as lit_nachdruck
                        from lit
                        join lit2lit
                            on {version} between lit2lit_von and lit2lit_bis
                            and lit2lit_thema_lit_id = lit.lit_id
                            and lit2lit_art = 'in'
                        join lit as litrhema
                            on {version} between litrhema.lit_von and litrhema.lit_bis
                            and litrhema.lit_id = lit2lit_rhema_lit_id
                        where {version} between lit.lit_von and lit.lit_bis
                        and lit.lit_id = %s
                        """, lit_id)
                if band:
                    for case in (
                            'lit_auflage', 'lit_ort', 'lit_jahr',
                            'lit_neuauflage', 'lit_nachdruck'):
                        if not item[case]:
                            item[case] = band[case]
                    bandtitel = band['lit_haupttitel'] or band['lit_abkürzung']
                    if band['lit_untertitel']:
                        if bandtitel and bandtitel[-1] not in {
                                '.', ':', ',', ';', '!', '?'}:
                            bandtitel += '.'
                        bandtitel += ' ' + band['lit_untertitel']
                    if band['lit_teilnr']:
                        bandtitel += ', ' + band['lit_teilnr']
                    if band['lit_reihentitel']:
                        bandtitel += ' (' + band['lit_reihentitel']
                        if band['lit_reihennr']:
                            bandtitel += ', ' + band['lit_reihennr']
                        bandtitel += ')'
                    terms = [ row['person_hauptname'] for row in con.getall(rf"""
                            person_hauptname
                            from person2lit
                            join person on {version} between person_von and person_bis
                                and person_id = person2lit_person_id
                            where {version} between person2lit_von and person2lit_bis
                                and person2lit_art = 'hg'
                                and person2lit_lit_id = %s
                            order by person2lit_rang
                            """, band['lit_id']) ]
                    bandeditor = ' – '.join( call(term) for term in terms )
                    links = get_linkstring(band['lit_id'])
            elif lit_art != 'verweis':
                links = get_linkstring(lit_id)
        sorter = get_sorter(item, lit_art, werk2lit_art, lit_haupttitel, autor_sort)
        if lit_art == 'verweis':
            term = f'''<a href="{lit_verweis}">{lit_verweis}</a>'''
        elif lit_art == 'ink':
            term = f'''{item['lit_ort']} {item['lit_jahr']}'''
            if drucker:
                term += f' bei {drucker}'
            for new, pre in (
                    ('lit_neuauflage', ''), ('lit_nachdruck', 'Nachdruck ')
                    ):
                if item[new]:
                    term += f' ({pre}{item[new]})'
        else:
            term = ''
            if autor:
                term += autor
            if lit_haupttitel:
                term += (', ' if term  else '') + lit_haupttitel
            if item['lit_untertitel']:
                if term and term[-1] not in {'.', ':', ',', ';', '!', '?'}:
                    term += '. '
                term += ' ' + item['lit_untertitel']
            if lit_art in {'mono', 'sammelband'}:
                if item['lit_teilnr']:
                    term += ', ' + item['lit_teilnr']
                if editor:
                    term += ', hg. von ' + editor
                if item['lit_reihentitel']:
                    term += ' (' + item['lit_reihentitel']
                    if item['lit_reihennr']:
                        term += ', ' + item['lit_reihennr']
                    term += ')'
            elif lit_art == 'sammelbandaufsatz':
                term += ', in: ' + bandtitel
                if bandeditor:
                    term += ', hg. von ' + bandeditor
            elif lit_art == 'zsaufsatz':
                term += ', ' + bandtitel
                if item['lit_reihennr']:
                    term += ' ' + item['lit_reihennr']
                if item['lit_jahr']:
                    term += f''' ({item['lit_jahr']})'''
                for new, pre in (
                        ('lit_neuauflage', ''), ('lit_nachdruck', 'Nachdruck ')
                        ):
                    if item[new]:
                        term += f' ({pre}{item[new]})'
                if item['lit_stelle']:
                    term += ' ' + item['lit_stelle']
            if lit_art != 'zsaufsatz':
                if item['lit_ort']:
                    term += ', ' + item['lit_ort']
                if item['lit_jahr']:
                    term += (' ' if item['lit_ort'] else ', ')
                    if item['lit_auflage']:
                        term += '<sup>' + item['lit_auflage'] + '</sup>'
                    term += item['lit_jahr']
                for new, pre in (
                        ('lit_neuauflage', ''), ('lit_nachdruck', 'Nachdruck ')
                        ):
                    if item[new]:
                        term += f' ({pre}{item[new]})'
                if lit_art == 'sammelbandaufsatz':
                    if item['lit_stelle']:
                        term += ', ' + item['lit_stelle']
                else:
                    if item['lit_bezugsstelle']:
                        term += ', ' + item['lit_bezugsstelle']
        if links and not lit_art == 'verweis':
            term += ' (' + links + ')'
        return sorter, f'<lit->{term}</lit->', item

    def get_lit_kind(self, item_id: int, version: int = ENDZEIT) -> str:
        version = int(version)
        with sql_io.DBCon(self.connect()) as (con, cur):
            return con.get(rf"""
                    lit_art from lit
                    where {version} between lit_von and lit_bis
                    and lit_id = %s
                    """, item_id).get(f'lit_art', '')

    def get_titel(self, row: 'dict[str, Any]') -> str:
        titel = row['lit_abkürzung']
        if not titel:
            titel = '. '.join(filter(None, (
                    row['lit_haupttitel'], row['lit_untertitel'])))
        teilnr = row['lit_teilnr']
        if teilnr:
            titel += f' ({teilnr})'
        return titel

    def reannotate(
            self,
            doc: str,
            mark: str,
            version: int = ENDZEIT,
            title_re: 're.Pattern[str]' =
                re.compile(r'<a href="/werk/1">(.*?)</a>'),
            lit_re: 're.Pattern[str]' =
                re.compile(r'<a href="/lit/([^"]*)">(.*?)</a>'),
            before_pagenum_re: 're.Pattern[str]' =
                re.compile(r'</lit->\s*(?=\d)'),
            ) -> str:

        def restate_lit(
                con: 'mysql.Connection',
                match: 're.Match[str]',
                ) -> str:
            item = con.get(rf"""
                    *, '' as lit_bezugsstelle from lit
                    where {version} between lit_von and lit_bis
                    and lit_id = %s
                    """, match.group(1))
            if item:
                return self.get_lit(item)[1]
            else:
                return f'<lit->{match.group(2)}</lit->'

        version = int(version)
        doc = doc.replace('<a href="/a/', '<a class="')
        doc = title_re.sub(r'<cite>\g<1></cite>', doc)
        with sql_io.DBCon(self.connect()) as (con, cur):
            doc = lit_re.sub(partial(restate_lit, con), doc)
        doc = doc.replace(
                '<a href="http',
                '<a rel="noopener noreferrer" target="_blank" href="http')
        doc = before_pagenum_re.sub('</lit->, ', doc)
        if mark:
            doc = web_io.mark(doc, mark)
        return doc

    def release(
            self,
            table: 'None|str' = None,
            item_id: 'None|int' = None,
            ) -> int:
        '''
        Mark the item :param:`item_id` of the table :param:`table`
        as ‘to be published’; i.e. set the “_bis” field to ``2``.

        If :param:`table` or :param:`item_id` is ``None`` or both,
        mark any item (in any table) as ‘to be published’ when the
        “_bis” field of the item equals ``1`` (i.e. ‘is changed’).

        Return the number of updated items.
        '''
        num = 0
        with sql_io.DBCon(self.connect()) as (con, cur):
            con.begin()
            if table is None or item_id is None:
                schema = getattr(con, 'db').decode()
                for table in tuple(sql_io.tables(cur, schema)):
                    if table.startswith('_'):
                        continue
                    num += cur.execute(rf"""
                            update {table} set {table}_bis = 2
                            where {table}_von = 0 and {table}_bis = 1
                            """)
            else:
                table = table.replace('\\', '').replace('`', '').replace("'", '')
                num += cur.execute(rf"""
                        update `{table}` set `{table}_bis` = 2
                        where `{table}_von` = 0 and `{table}_bis` = 1
                        and `{table}_id` = %s
                        """, item_id)
                for t1, t2 in [
                        ('person', 'lit'),
                        ('werk', 'gattung'),
                        ('werk', 'lit'),
                        ('werk', 'person'),
                        ('werk', 'region'),
                        ('werk', 'schlagwort'),
                        ('werk', 'sprache'),
                        ]:
                    if table == t1 or table == t2:
                        num += cur.execute(rf"""
                                update {t1}2{t2} set {t1}2{t2}_bis = 2
                                where {t1}2{t2}_von = 0 and {t1}2{t2}_bis = 1
                                and {t1}2{t2}_{table}_id = %s
                                """, item_id)
                if table == 'werk':
                    num += cur.execute(rf"""
                            update zeit set zeit_bis = 2
                            where zeit_von = 0 and zeit_bis = 1
                            and zeit_werk_id = %s
                            """, item_id)
                if table == 'lit':
                    num += cur.execute(rf"""
                            update lit2lit, lit
                            set lit2lit_bis = 2, lit_bis = 2
                            where lit2lit_von = 0 and lit2lit_bis > 0
                            and lit_von = 0 and lit_bis > 0
                            and lit2lit_rhema_lit_id = lit_id
                            and lit2lit_thema_lit_id = %s
                            """, item_id)
            con.commit()
        return num

    def resolve_alt_id(
            self,
            page_id: str,
            version: int = ENDZEIT,
            ) -> 'None|int':
        version = int(version)
        with sql_io.DBCon(self.connect()) as (con, cur):
            table = 'person' if page_id.startswith('repPers') else 'werk'
            return con.get(rf"""
                    {table}_id from {table}
                    where {version} between {table}_von and {table}_bis
                    and alt_id = %s
                    """, page_id).get(f'{table}_id', None)

    def upsert(
            self,
            col: str,
            item_id: int,
            doc: str,
            kind: str,
            rank: int,
            parent_col: str,
            parent_id: int,
            grandparent_col: str,
            grandparent_id: int,
            whitespace_re: 're.Pattern[str]' = re.compile(r'[ \n\r\t]+'),
            delete_re: 're.Pattern[str]' = re.compile(r'[\x00-\x08\x0b-\x1f]'),
            href_re: 're.Pattern[str]' = re.compile(
                r'''\bhref\s*=\s*(?P<quot>["'])(?P<verweis>/[^?]*?)(?P<modus>(\?.*?)?)(?P=quot)'''
                ),
            tag_re: 're.Pattern[str]' = re.compile(r'<[^>]*>'),
            urlstart_re: 're.Pattern[str]' = re.compile(r'https?://'),
            ) -> 'tuple[int, str]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            con.begin()
            col = col.replace('\\', '').replace('`', '').replace("'", '')
            parent_col = parent_col\
                    .replace('\\', '').replace('`', '').replace("'", '')
            grandparent_col = grandparent_col\
                    .replace('\\', '').replace('`', '').replace("'", '')
            table = col.split('_', 1)[0]
            assert all(
                    col != table + '_' + blocked
                    for blocked in {'id', 'von', 'bis', 'verweis', 'art'}
                    ), f'Feld “{col}” ist gesperrt.'
            doc = xmlhtml.replace(doc)
            doc = whitespace_re.sub(
                    ' ',
                    whitespace_re.sub(' ', doc).strip()\
                        .replace('<p> ', '<p>')\
                        .replace(' </p>', '</p>')\
                        .replace('</p> </p>', '</p><p>')\
                        .replace('</p><p>', '</p>\n<p>')\
                        .replace('<a></a>', '')\
                        .strip()
                    )
            doc = delete_re.sub('', doc)
            term = doc
            if not col.endswith('_beschreibung'):
                term = term.replace('<p>', '').replace('</p>', '  ').strip()
                # The double space allows for reconstructing the paragraphs.
            if table == 'region' and col == 'region_spät':
                term = 0 if 'f' in term.lower() else 1 # “f” ‘früh’, “s” ‘spät’.
            _bis = 1 if table in {'person', 'werk'} else 2
            # 1 ‘changed, not yet to be published’; 2 ‘to be published’.
            reftable = ''
            if col.endswith('_id'):
                reftable = col.split('_')[-2]
                match = href_re.search(term)
                if match:
                    modus = match.group('modus').strip()
                    _, verweistable, term_id =\
                            match.group('verweis').strip().rsplit('/', 2)
                    term_id = int(term_id)
                    assert verweistable == reftable,\
                            f'Verweistafel “{verweistable}” ungleich Referenztafel “{reftable}”.'
                    linktext = self.get_linktext(reftable, term_id)
                    doc = f'<p><a href="/{reftable}/{term_id}{modus}">{linktext}</a></p>'
                    term = term_id # for ``vals = [term]`` below
                else:
                    verweis = tag_re.sub('', term).strip()
                    if verweis:
                        if reftable == 'lit':
                            if (
                                    urlstart_re.search(verweis) or
                                    kind in {'copinger', 'hain'}
                                    ):
                                lit_col = 'lit_verweis'
                                lit_kind = 'verweis'
                            else:
                                lit_col = 'lit_abkürzung'
                                lit_kind = 'sonst'
                            term_id = con.get(rf"""
                                    lit_id from lit
                                    where lit_von = 0
                                    and `{lit_col}` = %s COLLATE utf8mb4_bin
                                    """, verweis).get('lit_id', 0)
                            if not term_id:
                                cur.execute(rf"""
                                        insert into lit (lit_bis, lit_art, `{lit_col}`)
                                        values (2, '{lit_kind}', %s)
                                        """, verweis)
                                con.commit()
                                term_id = cur.lastrowid
                            linktext = self.get_linktext(reftable, term_id)
                            if kind in {'copinger', 'hain'}:
                                doc = f'<p>{linktext}</p>'
                            else:
                                doc = f'<p><a href="/{reftable}/{term_id}">{linktext}</a></p>'
                        elif reftable == 'person':
                            term_id = con.get(r"""
                                    person_id from person
                                    where person_von = 0
                                    and person_art = 'sekundär'
                                    and person_hauptname = %s COLLATE utf8mb4_bin
                                    """, verweis).get('person_id', 0)
                            if not term_id:
                                cur.execute(r"""
                                    insert into person (person_bis, person_art, person_hauptname)
                                    values (2, 'sekundär', %s)
                                    """, verweis)
                                con.commit()
                                term_id = cur.lastrowid
                            doc = f'<p>{verweis}</p>' if verweis else ''
                        term = term_id # for ``vals = [term]`` below
                    else:
                        term = 0
                        doc = ''
            if not item_id:
                if reftable == 'lit' and col == 'lit2lit_thema_lit_id':
                    # ``parent_col`` is “lit2lit_rhema_lit_id”.
                    # ``parent_id``, however, is still “werk2lit_id”,
                    # because that is what the webform can tell us.
                    # So derive “lit2lit_rhema_lit_id” from “werk2lit_lit_id”:
                    parent_id = con.get(r"""
                            werk2lit_lit_id from werk2lit
                            where werk2lit_von = 0
                            and werk2lit_id = %s
                            and werk2lit_art = 'zeuge'
                            """, parent_id).get('werk2lit_lit_id', 0)
                cols = [f'`{table}_bis`']
                vals = [_bis]
                cols.append(f'`{col}`')
                vals.append(term)
                if kind:
                    cols.append(f'`{table}_art`')
                    vals.append(kind)
                if rank:
                    cols.append(f'`{table}_rang`')
                    vals.append(rank)
                if parent_col:
                    assert parent_id, f'Der übergeordnete Block ({parent_col}) ist noch unverbucht.'
                    cols.append(f'`{parent_col}`')
                    vals.append(parent_id)
                if grandparent_col:
                    assert grandparent_id, f'Der überübergeordnete Block ({grandparent_col}) ist noch unverbucht.'
                    cols.append(f'`{grandparent_col}`')
                    vals.append(grandparent_id)
                placeholders = ', '.join(['%s'] * len(vals))
                cols = ', '.join(cols)
                cur.execute(rf"""
                        insert into `{table}` ({cols}) values ({placeholders})
                        """, vals)
                con.commit()
                item_id = cur.lastrowid
            else:
                cur.execute(rf"""
                        update `{table}`
                        set `{table}_bis` = {_bis}, `{col}` = %s
                        where `{table}_von` = 0
                        and `{table}_id` = %s
                        """, (term, item_id))
                con.commit()
            if col == 'zeit_bezeichnung': # Rework the Raafzeiten or delete it.
                term = term.replace('Weltchronik', f'-{ENDZEIT}')
                zeit_beginn, zeit_ende = string_to_raafzeiten(term)
                cur.execute(r"""
                        update zeit
                        set zeit_beginn = %s, zeit_ende = %s
                        where zeit_von = 0
                        and zeit_id = %s
                        """, (zeit_beginn, zeit_ende, item_id))
                con.commit()
        assert item_id, f'Das Feld hatte keine ID, war also nicht zu speichern.'
        return item_id, doc

    def yield_for(self, act: str) -> 'Generator':
        '''
        Yield all available results for the specified :param:`act`.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            if act == 'autorengnds':
                for row in con.geteach(r'''
                        person_gnd from person
                        where person_art = 'autor' and person_gnd <> ''
                        '''):
                    yield row['person_gnd']
            else:
                return None

def for_filter(
        term: str,
        pre: 'tuple[tuple[re.Pattern[str], str], ...]' = (
            ('ł', 'l'),
            ),
        ) -> str:
    even_term = even(term, pre = (
            ('ä', 'ä'), # Undo decomposition
            ('ö', 'ö'), # Undo decomposition
            ('ü', 'ü'), # Undo decomposition
            ('ł', 'l'),
            ) + XTAG_PRE)
    return f'<div hidden="">█{even_term}</div>'

def string_to_raafzeiten(
        term: str,
        endzeit: int = ENDZEIT,
        digit_re: 're.Pattern[str]' = re.compile(r'\d'),
        non_dashdotdigit_re: 're.Pattern[str]' = re.compile(r'[^-.\d]+'),
        ) -> 'tuple[int, int]':
    '''
    >>> string_to_raafzeiten('21.6.1203-21.12.1248', 2111222333)
    (12030621, 12481221)
    >>> string_to_raafzeiten('1203', 2111222333)
    (12030000, 12039999)
    >>> string_to_raafzeiten('6.1203-12.1248', 2111222333)
    (12030600, 12481299)
    >>> string_to_raafzeiten('1203-', 2111222333)
    (12030000, 2111222333)
    >>> string_to_raafzeiten('-1203', 2111222333)
    (-2111222333, 12039999)
    '''
    if not digit_re.search(term):
        return (0, 0)
    parts = [
            (['', ''] + term.split('.')[:3])
            for term in non_dashdotdigit_re.sub('', term).split('-')[:2]
            ]
    if len(parts) == 2:
        von, bis = parts
    else:
        von = bis = parts[0]
    return (
            int(von[-1] + von[-2][:2].zfill(2) + von[-3][:2].zfill(2))
                if von[-1] else -endzeit,
            int(
                bis[-1] +
                (bis[-2][:2] or '99').zfill(2) +
                (bis[-3][:2] or '99').zfill(2)
                )
                if bis[-1] else endzeit
            )
