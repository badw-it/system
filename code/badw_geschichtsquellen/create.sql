-- Licensed under http://www.apache.org/licenses/LICENSE-2.0
-- Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)

-- All string columns are written with XML escaping and may contain tags.

drop table if exists bibliothek;
create table bibliothek
(
bibliothek_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
bibliothek_von INT NOT NULL DEFAULT 0,
bibliothek_bis INT NOT NULL DEFAULT 1,
bibliothek_ort VARCHAR(768) NOT NULL DEFAULT '', -- Soll nicht mit Tafel “ort” verknüpft werden: ist nur Bibliographicum, kein Geographicum.
bibliothek_institution VARCHAR(768) NOT NULL DEFAULT '',
bibliothek_isil VARCHAR(768) NOT NULL DEFAULT '',
bibliothek_anmerkung LONGTEXT NOT NULL DEFAULT '',
alt_id VARCHAR(768) NOT NULL DEFAULT '',
INDEX (bibliothek_von, bibliothek_bis),
PRIMARY KEY (bibliothek_id, bibliothek_von, bibliothek_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists gattung;
create table gattung -- Für Werke.
(
gattung_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
gattung_von INT NOT NULL DEFAULT 0,
gattung_bis INT NOT NULL DEFAULT 1,
gattung_bezeichnung VARCHAR(768) NOT NULL DEFAULT '',
INDEX (gattung_von, gattung_bis),
PRIMARY KEY (gattung_id, gattung_von, gattung_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists lit;
create table lit
(
lit_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
lit_von INT NOT NULL DEFAULT 0,
lit_bis INT NOT NULL DEFAULT 1,
lit_verweis VARCHAR(768) NOT NULL DEFAULT '', -- Für jeden Verweis in Gestalt einer auf ein Werk bezüglichen Kennung oder in Gestalt einer Netzseite.
lit_art VARCHAR(24) NOT NULL DEFAULT 'verweis' CHECK(lit_art in (
	'hs',
	'ink',
	'mono',
	'sonst', -- lit_abkürzung stand unmittelbar in <bibl>...</bibl>.
	'sammelband',
	'sammelbandaufsatz',
	'verweis',
	'zs',
	'zsaufsatz'
	)),
lit_gnd VARCHAR(768) NOT NULL DEFAULT '',
lit_bibliothek_id INT UNSIGNED NOT NULL DEFAULT 0, -- Für Hss.
lit_signatur VARCHAR(768) NOT NULL DEFAULT '', -- Für Hss.
lit_abkürzung VARCHAR(768) NOT NULL DEFAULT '', -- Für Zeitschriften und Standardwerke.
lit_haupttitel VARCHAR(768) NOT NULL DEFAULT '',
lit_untertitel VARCHAR(768) NOT NULL DEFAULT '',
lit_teilnr VARCHAR(768) NOT NULL DEFAULT '', -- Für Veröffentlichungen, die in Teilen erscheinen.
lit_reihentitel VARCHAR(768) NOT NULL DEFAULT '',
lit_reihennr VARCHAR(768) NOT NULL DEFAULT '',
lit_stelle VARCHAR(768) NOT NULL DEFAULT '', -- Eingenommene Stelle in Hs., Sammelband, Zeitschriftenband, Netzgesamtseite. Blätter (“f.”), Seiten (“p.”) oder (für Netzseiten) IDs, die auf der Netzseite nachzuschlagen sind.
lit_auflage VARCHAR(768) NOT NULL DEFAULT '',
lit_ort VARCHAR(768) NOT NULL DEFAULT '',
lit_jahr VARCHAR(768) NOT NULL DEFAULT '',
lit_nachdruck VARCHAR(768) NOT NULL DEFAULT '',
lit_sprache_id INT UNSIGNED NOT NULL DEFAULT 0,
lit_anmerkung LONGTEXT NOT NULL DEFAULT '',
alt_id VARCHAR(768) NOT NULL DEFAULT '',
INDEX (lit_von, lit_bis),
INDEX (lit_art(24), lit_id, lit_von, lit_bis),
PRIMARY KEY (lit_id, lit_von, lit_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists lit2lit;
create table lit2lit
(
lit2lit_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
lit2lit_von INT NOT NULL DEFAULT 0,
lit2lit_bis INT NOT NULL DEFAULT 1,
lit2lit_art VARCHAR(24) NOT NULL DEFAULT '' CHECK(lit2lit_art in (
	'',
	-- Die Arten sind Beziehungen zwischen _thema_lit_id und _rhema_lit_id und werden im Folgenden näher erläutert.
	'in', -- Thema ist Aufsatz in Rhema (immer ein Band oder eine Zs.). Nicht für Bände in Reihen: die Reihe wird unmittelbar in lit_reihentitel (dazu lit_reihennr) eingetragen.
	'zu', -- Thema ist Literatur (Netzseiten eingeschlossen) zu Rhema (Hs.) in Bezug auf die Überlieferung von Werk lit2lit_werk_id.
	-- Die Folgenden nach dem Muster: Thema hat als ...-Kennung Rhema (meist eine URL).
	'bsbink', -- (Thema ist Inkunabel.)
	'bv',
	'copinger', -- (Thema ist Inkunabel.)
	'gw', -- (Thema ist Inkunabel.)
	'hain', -- (Thema ist Inkunabel.)
	'issn',
	'istc', -- (Thema ist Inkunabel.)
	'urn',
	'vd16',
	'vd17',
	'vd18',
	'zdb',
	'zdbdigital',
	'zsdigital'
	)),
lit2lit_thema_lit_id INT UNSIGNED NOT NULL DEFAULT 0,
lit2lit_rhema_lit_id INT UNSIGNED NOT NULL DEFAULT 0,
lit2lit_werk_id INT UNSIGNED NOT NULL DEFAULT 0, -- Werk, in bezug worauf Thema Literatur zu Rhema ist (lit2lit_art = 'zu').
lit2lit_stelle VARCHAR(768) NOT NULL DEFAULT '',
lit2lit_anmerkung LONGTEXT NOT NULL DEFAULT '',
lit2lit_rang INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (lit2lit_von, lit2lit_bis),
INDEX (lit2lit_art(24), lit2lit_von, lit2lit_bis),
INDEX (lit2lit_thema_lit_id, lit2lit_rhema_lit_id, lit2lit_von, lit2lit_bis),
INDEX (lit2lit_rhema_lit_id, lit2lit_von, lit2lit_bis),
INDEX (lit2lit_werk_id, lit2lit_von, lit2lit_bis),
PRIMARY KEY (lit2lit_id, lit2lit_von, lit2lit_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists ort;
create table ort
(
ort_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
ort_von INT NOT NULL DEFAULT 0,
ort_bis INT NOT NULL DEFAULT 1,
ort_normverweis VARCHAR(768) NOT NULL DEFAULT '',
ort_name VARCHAR(768) NOT NULL DEFAULT '',
ort_breite DOUBLE NOT NULL DEFAULT 0,
ort_länge DOUBLE NOT NULL DEFAULT 0,
ort_orden VARCHAR(192) NOT NULL DEFAULT '',
ort_anmerkung LONGTEXT NOT NULL DEFAULT '',
INDEX (ort_orden(192), ort_von, ort_bis),
INDEX (ort_von, ort_bis),
PRIMARY KEY (ort_id, ort_von, ort_bis)
)
ROW_FORMAT = DYNAMIC;
insert into ort (ort_von) values (0); -- Platzhaltereintrag

drop table if exists person;
create table person
(
person_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
person_von INT NOT NULL DEFAULT 0,
person_bis INT NOT NULL DEFAULT 1,
person_art VARCHAR(24) NOT NULL DEFAULT '' CHECK(person_art in (
	'', -- für Platzhaltereintrag.
	'autor', -- ma. Quellenautor.
	'erwähnt',  -- nur erwähnte ma. Person., darunter die Drucker von Inkunabeln.
	'sekundär' -- Autor oder Hg. von Sekundärliteratur.
	)),
person_gnd VARCHAR(768) NOT NULL DEFAULT '',
person_hauptname VARCHAR(768) NOT NULL DEFAULT '',
person_nebenname VARCHAR(768) NOT NULL DEFAULT '',
person_beschreibung LONGTEXT NOT NULL DEFAULT '',
person_wikiverweis VARCHAR(768) NOT NULL DEFAULT '',
person_stand VARCHAR(768) NOT NULL DEFAULT '',
person_anmerkung LONGTEXT NOT NULL DEFAULT '',
alt_id VARCHAR(768) NOT NULL DEFAULT '',
INDEX (person_von, person_bis),
INDEX (person_art(24), person_von, person_bis),
PRIMARY KEY (person_id, person_von, person_bis)
)
ROW_FORMAT = DYNAMIC;
insert into person (person_von) values (0); -- Platzhaltereintrag

drop table if exists person2lit;
create table person2lit
(
person2lit_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
person2lit_von INT NOT NULL DEFAULT 0,
person2lit_bis INT NOT NULL DEFAULT 1,
person2lit_art VARCHAR(24) NOT NULL DEFAULT 'autor' CHECK(person2lit_art in (
	'autor',
	'hg', -- Bei Sammelbänden
	'drucker', -- Bei Inkunabeln
	'bezug' -- Bei Literatur _über_ die verknüpfte Person.
	)),
person2lit_lit_id INT UNSIGNED NOT NULL DEFAULT 0,
person2lit_person_id INT UNSIGNED NOT NULL DEFAULT 0,
person2lit_stelle VARCHAR(768) NOT NULL DEFAULT '',
person2lit_anmerkung LONGTEXT NOT NULL DEFAULT '',
person2lit_rang INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (person2lit_von, person2lit_bis),
INDEX (person2lit_art(24), person2lit_von, person2lit_bis),
INDEX (person2lit_lit_id, person2lit_art(24), person2lit_von, person2lit_bis),
INDEX (person2lit_person_id, person2lit_art(24), person2lit_von, person2lit_bis),
PRIMARY KEY (person2lit_id, person2lit_von, person2lit_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists region;
create table region -- Für Werke.
(
region_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
region_von INT NOT NULL DEFAULT 0,
region_bis INT NOT NULL DEFAULT 1,
region_spät TINYINT(1) UNSIGNED NOT NULL DEFAULT 1, -- früh: 750–1200; spät: 1200–1500.
region_bezeichnung VARCHAR(768) NOT NULL DEFAULT '',
INDEX (region_von, region_bis),
PRIMARY KEY (region_id, region_von, region_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists schlagwort;
create table schlagwort -- Für Werke.
(
schlagwort_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
schlagwort_von INT NOT NULL DEFAULT 0,
schlagwort_bis INT NOT NULL DEFAULT 1,
schlagwort_bezeichnung VARCHAR(768) NOT NULL DEFAULT '',
INDEX (schlagwort_von, schlagwort_bis),
PRIMARY KEY (schlagwort_id, schlagwort_von, schlagwort_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists sprache;
create table sprache
(
sprache_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
sprache_von INT NOT NULL DEFAULT 0,
sprache_bis INT NOT NULL DEFAULT 1,
sprache_bezeichnung VARCHAR(768) NOT NULL DEFAULT '',
INDEX (sprache_von, sprache_bis),
PRIMARY KEY (sprache_id, sprache_von, sprache_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists werk;
create table werk
(
werk_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk_von INT NOT NULL DEFAULT 0,
werk_bis INT NOT NULL DEFAULT 1,
werk_rep VARCHAR(768) NOT NULL DEFAULT '', -- Verweis auf das gedruckte Repositorium.
werk_haupttitel VARCHAR(768) NOT NULL DEFAULT '',
werk_nebentitel VARCHAR(768) NOT NULL DEFAULT '',
werk_beschreibung LONGTEXT NOT NULL DEFAULT '',
werk_stand VARCHAR(768) NOT NULL DEFAULT '',
alt_id VARCHAR(768) NOT NULL DEFAULT '',
INDEX (werk_von, werk_bis),
PRIMARY KEY (werk_id, werk_von, werk_bis)
)
ROW_FORMAT = DYNAMIC;
insert into werk (werk_von) values (0); -- Platzhaltereintrag

drop table if exists werk2gattung;
create table werk2gattung
(
werk2gattung_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk2gattung_von INT NOT NULL DEFAULT 0,
werk2gattung_bis INT NOT NULL DEFAULT 1,
werk2gattung_gattung_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2gattung_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (werk2gattung_von, werk2gattung_bis),
INDEX (werk2gattung_werk_id, werk2gattung_gattung_id, werk2gattung_von, werk2gattung_bis),
PRIMARY KEY (werk2gattung_id, werk2gattung_von, werk2gattung_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists werk2lit;
create table werk2lit
(
werk2lit_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk2lit_von INT NOT NULL DEFAULT 0,
werk2lit_bis INT NOT NULL DEFAULT 1,
werk2lit_art VARCHAR(24) NOT NULL DEFAULT 'zumwerk' CHECK(werk2lit_art in (
	'altübersetzung',
	'ausgabe',
	'faksimile',
	'zeuge',
	'zeugenlit',
	'übersetzung',
	'zumwerk'
	)),
werk2lit_lit_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2lit_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2lit_stelle VARCHAR(768) NOT NULL DEFAULT '',
werk2lit_sprache_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2lit_rang INT UNSIGNED NOT NULL DEFAULT 0,
werk2lit_rubrik VARCHAR(768) NOT NULL DEFAULT '',
werk2lit_anmerkung LONGTEXT NOT NULL DEFAULT '',
INDEX (werk2lit_von, werk2lit_bis),
INDEX (werk2lit_werk_id, werk2lit_art(24), werk2lit_von, werk2lit_bis),
INDEX (werk2lit_lit_id, werk2lit_von, werk2lit_bis),
PRIMARY KEY (werk2lit_id, werk2lit_von, werk2lit_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists werk2person;
create table werk2person
(
werk2person_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk2person_von INT NOT NULL DEFAULT 0,
werk2person_bis INT NOT NULL DEFAULT 1,
werk2person_art VARCHAR(24) NOT NULL DEFAULT '' CHECK(werk2person_art in (
	'',
	'autor'
	)), -- Art der Beziehung.
werk2person_person_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2person_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2person_rang INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (werk2person_von, werk2person_bis),
INDEX (werk2person_person_id, werk2person_art(24), werk2person_von, werk2person_bis),
INDEX (werk2person_werk_id, werk2person_person_id, werk2person_art(24), werk2person_von, werk2person_bis),
PRIMARY KEY (werk2person_id, werk2person_von, werk2person_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists werk2region;
create table werk2region
(
werk2region_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk2region_von INT NOT NULL DEFAULT 0,
werk2region_bis INT NOT NULL DEFAULT 1,
werk2region_region_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2region_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (werk2region_von, werk2region_bis),
INDEX (werk2region_werk_id, werk2region_region_id, werk2region_von, werk2region_bis),
PRIMARY KEY (werk2region_id, werk2region_von, werk2region_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists werk2schlagwort;
create table werk2schlagwort
(
werk2schlagwort_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk2schlagwort_von INT NOT NULL DEFAULT 0,
werk2schlagwort_bis INT NOT NULL DEFAULT 1,
werk2schlagwort_schlagwort_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2schlagwort_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (werk2schlagwort_von, werk2schlagwort_bis),
INDEX (werk2schlagwort_werk_id, werk2schlagwort_schlagwort_id, werk2schlagwort_von, werk2schlagwort_bis),
PRIMARY KEY (werk2schlagwort_id, werk2schlagwort_von, werk2schlagwort_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists werk2sprache;
create table werk2sprache
(
werk2sprache_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
werk2sprache_von INT NOT NULL DEFAULT 0,
werk2sprache_bis INT NOT NULL DEFAULT 1,
werk2sprache_sprache_id INT UNSIGNED NOT NULL DEFAULT 0,
werk2sprache_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
INDEX (werk2sprache_von, werk2sprache_bis),
INDEX (werk2sprache_werk_id, werk2sprache_sprache_id, werk2sprache_von, werk2sprache_bis),
PRIMARY KEY (werk2sprache_id, werk2sprache_von, werk2sprache_bis)
)
ROW_FORMAT = DYNAMIC;

drop table if exists zeit;
create table zeit
(
zeit_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
zeit_von INT NOT NULL DEFAULT 0,
zeit_bis INT NOT NULL DEFAULT 1,
zeit_werk_id INT UNSIGNED NOT NULL DEFAULT 0,
zeit_art VARCHAR(24) NOT NULL DEFAULT 'entstehung' CHECK(zeit_art in (
	'bericht',
	'entstehung'
	)),
zeit_bezeichnung VARCHAR(768) NOT NULL DEFAULT '',
zeit_beginn INT NOT NULL DEFAULT 0,
zeit_ende INT NOT NULL DEFAULT 0,
INDEX (zeit_von, zeit_bis),
INDEX (zeit_werk_id, zeit_art, zeit_von, zeit_bis),
INDEX (zeit_beginn, zeit_von, zeit_bis),
INDEX (zeit_ende, zeit_von, zeit_bis),
PRIMARY KEY (zeit_id, zeit_von, zeit_bis)
)
ROW_FORMAT = DYNAMIC;

CREATE INDEX person_alt_id ON person (alt_id);
CREATE INDEX werk_alt_id ON werk (alt_id);
