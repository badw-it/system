# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Infinite loop backing up a database at certain time intervals.
See :func:`backup.run_backup`.

This script has to be called with one or more arguments representing
absolute paths to configuration files in the ini-format.
'''
import sys

import __init__
import backup
import fs

urdata = sys.argv[1:]
config = fs.get_config(urdata)
paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
db_user, db_password = fs.get_keys(paths['db_access'])
backup.run_backup(
        [
            'mysqldump',
            'geschichtsquellen',
            '--default-character-set=' + config['db']['charset'],
            '--user=' + db_user,
            '--password=' + db_password,
            '--result-file={}.sql',
            '--ignore-table=geschichtsquellen._index_ft',
            '--ignore-table=geschichtsquellen._index_ort',
            '--ignore-table=geschichtsquellen._index_person',
            '--ignore-table=geschichtsquellen._index_werk',
            '--ignore-table=geschichtsquellen._index_werkbezug',
            '--ignore-table=geschichtsquellen._index_zeuge',
            '--ignore-table=geschichtsquellen._latest_refresh',
        ],
        paths['backup_db'],
        seconds_between_backups = 7200,
        )
