# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
import json
try: import regex as re
except ImportError: import re
import sys
import time
import traceback
from collections import deque
from urllib.request import urlopen

import gehalt
import __init__
import fs
import sys_io
import sql_io

ENDZEIT = gehalt.ENDZEIT

ART_WERKBEZUG = {
        'incipit',
        'autograph',
        'unikat',
        'unediert',
        }

INDEX_TABLES = {
        '_index_ort',
        '_index_person',
        '_index_werk',
        '_index_werkbezug',
        '_index_zeuge',
        }

def check_dtbio(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        config: 'fs.ConfigParser',
        ) -> None:
    dtbio_items = json.loads(
            urlopen(config['ids']['dtbio_url']).read()
            )['response']['docs']
    common_gnds = {
                item.get('defgnd', None) for item in dtbio_items
            } & {
                r['person_gnd'] for r in con.geteach(r'person_gnd from person')
            }
    common_gnds_sql = "', '".join(
            gnd.replace('\\', '').replace("'", '') for gnd in common_gnds )
    cur.execute(rf'''
            update person set person_dtbioverweis = 0
            where person_bis = {ENDZEIT}
            ''')
    cur.execute(rf'''
            update person set person_dtbioverweis = 1
            where person_bis = {ENDZEIT}
            and person_gnd in ('{common_gnds_sql}')
            ''')

def create_tables(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        ) -> None:
    cur.execute(r"""
            create table if not exists _latest_refresh
            (
            latest_refresh BIGINT NOT NULL
            )
            ROW_FORMAT = DYNAMIC""")
    cur.execute(r"""
            create table if not exists _index_ort -- Mentionings of locations in articles on a person or on a work.
            (
            _eigen_id   INT UNSIGNED NOT NULL, -- Database ID of the item itself.
            _person_id  INT UNSIGNED NOT NULL, -- Database ID of the person in whose description the _eigen_id was found.
            _werk_id    INT UNSIGNED NOT NULL, -- Database ID of the werk in whose description the _eigen_id was found.
            _quellenort TINYINT UNSIGNED NOT NULL, -- Whether it is a quellenort or not.
            INDEX (_eigen_id, _quellenort)
            )
            ROW_FORMAT = DYNAMIC""")
    cur.execute(r"""
            create table if not exists _index_person -- Mentionings of persons in articles on a person or on a work.
            (
            _eigen_id  INT UNSIGNED NOT NULL,
            _person_id INT UNSIGNED NOT NULL,
            _werk_id   INT UNSIGNED NOT NULL,
            _heilig    TINYINT UNSIGNED NOT NULL -- Whether the person is – in the context of the passage – recognized as a saint.
            )
            ROW_FORMAT = DYNAMIC""")
    cur.execute(r"""
            create table if not exists _index_werk -- Mentionings of works in articles on a person or on a work.
            (
            _eigen_id  INT UNSIGNED NOT NULL,
            _person_id INT UNSIGNED NOT NULL,
            _werk_id   INT UNSIGNED NOT NULL,
            _term      VARCHAR(768) NOT NULL, -- Designation of the work, if not equal to werk_haupttitel or werk_nebentitel, and empty else.
            INDEX (_eigen_id, _term(4)),
            INDEX (_eigen_id, _werk_id, _person_id)
            )
            ROW_FORMAT = DYNAMIC""")
    cur.execute(rf"""
            create table if not exists _index_werkbezug
            (
            _art     VARCHAR(20) NOT NULL CHECK(_art in ('{"', '".join(ART_WERKBEZUG)}')),
            _lit_id  INT UNSIGNED NOT NULL, -- For cases from werk2lit_anmerkung.
            _werk_id INT UNSIGNED NOT NULL,
            _term    VARCHAR(768) NOT NULL, -- The incipit, if _art = 'incipit', otherwise the empty string.
            INDEX (_werk_id, _art)
            )
            ROW_FORMAT = DYNAMIC""")
    cur.execute(r"""
            create table if not exists _index_zeuge
            (
            _eigen_id  INT UNSIGNED NOT NULL,
            _person_id INT UNSIGNED NOT NULL,
            _werk_id   INT UNSIGNED NOT NULL,
            _hs        TINYINT UNSIGNED NOT NULL, -- Flag to distinguish between mss and incunabula.
            _term      VARCHAR(768) NOT NULL, -- Standard designation of the witness, pre-computed here for fast access later.
            INDEX (_eigen_id, _werk_id),
            INDEX (_eigen_id, _person_id, _werk_id)
            )
            ROW_FORMAT = DYNAMIC""")

def delete_dangling_or_empty_items(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        ) -> None:
    cur.execute(r"""
            delete from lit2lit
            where (lit2lit_thema_lit_id = 0 or lit2lit_rhema_lit_id = 0)
            and lit2lit_bis not in (1, 2)
            and lit2lit_stelle = ''
            and lit2lit_anmerkung = ''
            """)
    cur.execute(r"""
            delete from person2lit
            where (person2lit_lit_id = 0 or person2lit_person_id = 0)
            and person2lit_bis not in (1, 2)
            and person2lit_stelle = ''
            and person2lit_anmerkung = ''
            """)
    cur.execute(r"""
            delete from werk2lit
            where (werk2lit_lit_id = 0 or werk2lit_werk_id = 0)
            and werk2lit_bis not in (1, 2)
            and werk2lit_stelle = ''
            and werk2lit_anmerkung = ''
            """)
    con.commit()
    con.begin()
    cur.execute(r"""
            delete from lit
            where lit_art = 'verweis'
            and lit_id not in (select lit2lit_thema_lit_id from lit2lit)
            and lit_id not in (select lit2lit_rhema_lit_id from lit2lit)
            and lit_id not in (select person2lit_lit_id from person2lit)
            and lit_id not in (select werk2lit_lit_id from werk2lit)
            """) # lit_anmerkung should be empty and never cause to keep it.

def index(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        table: str,
        row: 'dict[str, int|str]',
        ) -> None:
    doc = ''
    werk2lit_art = ''
    lit_id = 0
    person_id = 0
    werk_id = 0
    if table == 'lit2lit':
        doc = row['lit2lit_anmerkung']
        werk_id = row['lit2lit_werk_id']
        if not werk_id:
            return
    elif table == 'person':
        doc = row['person_beschreibung']
        person_id = row['person_id']
        if not person_id:
            return
    elif table == 'person2lit':
        doc = row['person2lit_anmerkung']
        person2lit_art = row['person2lit_art']
        lit_id = row['person2lit_lit_id']
        person_id = row['person2lit_person_id']
        if not (lit_id and person_id):
            return
    elif table == 'werk':
        doc = row['werk_beschreibung']
        werk_id = row['werk_id']
        if not werk_id:
            return
    elif table == 'werk2lit':
        doc = row['werk2lit_anmerkung']
        werk2lit_art = row['werk2lit_art']
        lit_id = row['werk2lit_lit_id']
        werk_id = row['werk2lit_werk_id']
        if not (lit_id and werk_id):
            return
    if not doc:
        return
    # Index of: ART_WERKBEZUG.
    if table == 'werk' or (table == 'werk2lit' and werk2lit_art == 'zeuge'):
        for art in ART_WERKBEZUG:
            for match in re.finditer(rf'(?s)<a href="/a/{art}">(.*?)</a>', doc):
                term = match.group(1).strip() if art == 'incipit' else ''
                cur.execute(r"""
                    insert into _index_werkbezug
                    (_art, _lit_id, _werk_id, _term)
                    values (%s, %s, %s, %s)
                    """, (art, lit_id, werk_id, term))
    # Index of: hs, ort, person, werk.
    if table in {'lit2lit', 'person', 'person2lit', 'werk', 'werk2lit'}:
        for match in re.finditer(
                r'<a href="/(?P<art>lit|ort|person|werk)/(?P<eigen_id>\d*)'\
                r'(?P<modus>\?modus=[^"]*)?">(?P<term>.*?)</a>',
                doc):
            art = match.group('art')
            eigen_id = int(match.group('eigen_id'))
            if art == 'lit': # For hs and ink.
                lit = con.get(rf"""
                        lit_art,
                        lit_signatur,
                        IFNULL(bibliothek_ort, '') as bibliothek_ort,
                        IFNULL(bibliothek_institution, '') as bibliothek_institution
                        from lit
                        left join bibliothek
                            on {ENDZEIT} between bibliothek_von and bibliothek_bis
                            and bibliothek_id = lit_bibliothek_id
                        where {ENDZEIT} between lit_von and lit_bis
                        and lit_id = %s
                        """, eigen_id)
                if not (lit and lit['lit_art'] in {'hs', 'ink'}):
                    continue
                typus = 1 if lit['lit_art'] == 'hs' else 0
                term = f"{lit['bibliothek_ort']}, {lit['bibliothek_institution']}, {lit['lit_signatur']}"
                # Avoid insertion of redundant item.
                if con.get(r"""
                        1 from _index_zeuge
                        where _eigen_id = %s
                        and _person_id = %s
                        and _werk_id = %s
                        """, eigen_id, person_id, werk_id):
                    continue
                cur.execute(r"""
                        insert into _index_zeuge
                        (_eigen_id, _person_id, _werk_id, _hs, _term)
                        values (%s, %s, %s, %s, %s)
                        """, (eigen_id, person_id, werk_id, typus, term))
            elif art == 'ort':
                modus = 1 if '=Q' in match.group('modus') else 0
                cur.execute(r"""
                        insert into _index_ort
                        (_eigen_id, _person_id, _werk_id, _quellenort)
                        values (%s, %s, %s, %s)
                        """, (eigen_id, person_id, werk_id, modus))
            elif art == 'person':
                modus = 1 if '=Hl' in match.group('modus') else 0
                # Exclude persons being only booked as secondary authors.
                if not con.get(rf"""
                        1 from person
                        where {ENDZEIT} between person_von and person_bis
                        and person_id = %s
                        and person_art in ('autor', 'erwähnt')
                        """, eigen_id):
                    continue
                cur.execute(r"""
                        insert into _index_person
                        (_eigen_id, _person_id, _werk_id, _heilig)
                        values (%s, %s, %s, %s)
                        """, (eigen_id, person_id, werk_id, modus))
            elif art == 'werk':
                term = match.group('term').strip()
                # Insert a nebentitel only from an article about the very werk.
                if eigen_id != werk_id:
                    term = ''
                # Insert a nebentitel only if not known as haupt- or nebentitel.
                elif con.get(rf"""
                        1 from werk where {ENDZEIT} between werk_von and werk_bis
                        and werk_id = %s
                        and (
                            werk_haupttitel = %s or
                            werk_nebentitel = %s
                            )
                        """, eigen_id, term, term):
                    term = ''
                # Avoid insertion of a redundant item.
                if con.get(r"""
                        1 from _index_werk
                        where _eigen_id = %s
                        and _person_id = %s
                        and _werk_id = %s
                        and _term = %s
                        """, eigen_id, person_id, werk_id, term):
                    continue
                cur.execute(r"""
                        insert into _index_werk
                        (_eigen_id, _person_id, _werk_id, _term)
                        values (%s, %s, %s, %s)
                        """, (eigen_id, person_id, werk_id, term))
            else:
                continue

def index_lit_zeuge_hs(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        ) -> None:
    for row in deque(con.geteach(rf"""
            lit_id,
            lit_signatur,
            IFNULL(bibliothek_ort, '') as bibliothek_ort,
            IFNULL(bibliothek_institution, '') as bibliothek_institution
            from lit
            left join bibliothek
                on {ENDZEIT} between bibliothek_von and bibliothek_bis
                and bibliothek_id = lit_bibliothek_id
            where {ENDZEIT} between lit_von and lit_bis
            and lit_art = 'hs'
            """)):
        for subrow in deque(con.geteach(rf"""
                werk2lit_werk_id from werk2lit
                where {ENDZEIT} between werk2lit_von and werk2lit_bis
                and werk2lit_lit_id = %s
                """, row['lit_id'])):
            cur.execute(r"""
                    insert into _index_zeuge
                    (_eigen_id, _person_id, _werk_id, _hs, _term)
                    values (%s, %s, %s, %s, %s)
                    """, (
                        row['lit_id'],
                        0,
                        subrow['werk2lit_werk_id'],
                        1,
                        f"{row['bibliothek_ort']}, {row['bibliothek_institution']}, {row['lit_signatur']}",
                        ))

def index_lit_zeuge_ink(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        ) -> None:
    for row in deque(con.geteach(rf"""
            lit_id,
            lit_ort,
            lit_jahr
            from lit
            where {ENDZEIT} between lit_von and lit_bis
            and lit_art = 'ink'
            """)):
        for subrow in deque(con.geteach(rf"""
                werk2lit_werk_id from werk2lit
                where {ENDZEIT} between werk2lit_von and werk2lit_bis
                and werk2lit_lit_id = %s
                """, row['lit_id'])):
            cur.execute(r"""
                    insert into _index_zeuge
                    (_eigen_id, _person_id, _werk_id, _hs, _term)
                    values (%s, %s, %s, %s, %s)
                    """, (
                        row['lit_id'],
                        0,
                        subrow['werk2lit_werk_id'],
                        0,
                        f"{row['lit_ort']}, {row['lit_jahr']}",
                        ))

def index_person_werk_autor(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        ) -> None:
    for row in deque(con.geteach(rf"""
            person_id from person
            where {ENDZEIT} between person_von and person_bis
            and person_art = 'autor'
            """)):
        eigen_id = person_id = row['person_id']
        werk_id = 0
        # Avoid insertion of redundant item.
        if con.get(r"""
                1 from _index_person
                where _eigen_id = %s
                and _person_id = %s
                and _werk_id = %s
                """, eigen_id, person_id, werk_id):
            continue
        cur.execute(r"""
                insert into _index_person
                values (%s, %s, %s, 0)
                """, (eigen_id, person_id, werk_id))
        person_id = 0
        for subrow in deque(con.geteach(rf"""
                werk2person_werk_id from werk2person
                where {ENDZEIT} between werk2person_von and werk2person_bis
                and werk2person_person_id = %s
                and werk2person_art = 'autor'
                """, eigen_id)):
            werk_id = subrow['werk2person_werk_id']
            # Avoid insertion of redundant item.
            if con.get(r"""
                    1 from _index_person
                    where _eigen_id = %s
                    and _person_id = %s
                    and _werk_id = %s
                    """, eigen_id, person_id, werk_id):
                continue
            cur.execute(r"""
                    insert into _index_person
                    values (%s, %s, %s, 0)
                    """, (eigen_id, person_id, werk_id))
    for row in deque(con.geteach(rf"""
            person2lit_person_id as person_id, werk2lit_werk_id as werk_id
            from person2lit
            join werk2lit
                on {ENDZEIT} between werk2lit_von and werk2lit_bis
                and werk2lit_lit_id = person2lit_lit_id
            where {ENDZEIT} between person2lit_von and person2lit_bis
            and person2lit_art = 'drucker'
            """)):
        eigen_id = row['person_id']
        werk_id = row['werk_id']
        # Avoid insertion of redundant item.
        if con.get(r"""
                1 from _index_person
                where _eigen_id = %s
                and _werk_id = %s
                """, eigen_id, werk_id):
            continue
        cur.execute(r"""
                insert into _index_person
                values (%s, 0, %s, 0)
                """, (eigen_id, werk_id))

def release(
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.DictCursor',
        table: str,
        today: int,
        ) -> None:
    for candidate in deque(con.geteach(rf"""
                {table}_id, {table}_von from {table}
                where {table}_bis = 2
                """)):
        row_id = candidate[f'{table}_id']
        # Skip, if there is a release of today already.
        if con.get(rf"""
                1 from {table}
                where {table}_id = %s and {table}_von = %s
                """, row_id, today):
            continue
        # In the candidate, set _bis back to 0.
        cur.execute(rf"""
                update {table} set {table}_bis = 0
                where {table}_id = %s and {table}_von = 0
                """, row_id)
        # If it is a candidate for deletion.
        if candidate[f'{table}_von'] == -1:
            # In a current row of the same ID, just set the _bis to today - 1.
            cur.execute(rf"""
                    update {table} set {table}_bis = %s
                    where {table}_von <> 0 and {table}_bis = {ENDZEIT}
                        and {table}_id = %s
                    """, (today - 1, row_id))
            continue
        # If it is a candidate for inserting or updating.
        # Make a copy of the candidate.
        new = con.get(rf"""
                * from {table}
                where {table}_von = 0 and {table}_id = %s
                """, row_id)
        # Set the _von and _bis values of the copy.
        new[f'{table}_von'] = today
        new[f'{table}_bis'] = ENDZEIT
        # Get the most recently released row of the same ID.
        old = con.get(rf"""
                * from {table}
                where {table}_von <> 0 and {table}_bis = {ENDZEIT}
                    and {table}_id = %s
                """, row_id)
        # If this older release has equal values, we are done.
        if old and all(
                old[key] == new[key] for key in old if key not in
                {f'{table}_id', f'{table}_von', f'{table}_bis'}
                ):
            continue
        # If not, release the candidate.
        cols, vals = zip(*new.items())
        cols = f"`{'`, `'.join(cols)}`"
        params = ', '.join( '%s' for val in vals )
        cur.execute(rf"""
                insert into {table} ({cols}) values ({params})
                """, vals)
        # Set the ‘until’ value of the older release to yesterday.
        if old:
            old_von = old[f'{table}_von']
            cur.execute(rf"""
                    update {table} set {table}_bis = {today - 1}
                    where {table}_id = %s and {table}_von = %s
                    """, (row_id, old_von))

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Refresh or clean up data in an ongoing background process.

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Refresh the indices, release the records which are to be published.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    connect = sql_io.get_connect(*fs.get_keys(paths['db_access']), config)
    exception_old = ''
    while True:
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        time.sleep(10)
        try:
            with sql_io.DBCon(connect()) as (con, cur):
                con.begin()
                create_tables(con, cur)
                con.commit()
                latest_refresh = con.get(r"""
                        latest_refresh from _latest_refresh
                        """).get('latest_refresh', 0)
                today = int(time.strftime('%Y%m%d'))
                if today <= latest_refresh:
                    continue
                con.begin()
                for table in tuple(sql_io.tables(cur, config['db']['name'])):
                    time.sleep(1)
                    if table.startswith('_'):
                        continue
                    release(con, cur, table, today)
                # Index the current public content in the following steps.
                for table in INDEX_TABLES:
                    cur.execute(rf"delete from {table}")
                time.sleep(1)
                index_lit_zeuge_hs(con, cur)
                time.sleep(1)
                index_lit_zeuge_ink(con, cur)
                time.sleep(1)
                for table in tuple(sql_io.tables(cur, config['db']['name'])):
                    time.sleep(0.5)
                    if table.startswith('_') or table == 'lit':
                        continue
                    for row in deque(con.geteach(rf"""
                            * from {table}
                            where {table}_von > 0 and {table}_bis = {ENDZEIT}
                            """)):
                        index(con, cur, table, row)
                time.sleep(1)
                index_person_werk_autor(con, cur)
                delete_dangling_or_empty_items(con, cur)
                if latest_refresh:
                    cur.execute(
                            r"update _latest_refresh set latest_refresh = %s",
                            today)
                else:
                    cur.execute(
                            r"insert into _latest_refresh values (%s)", today)
                con.commit()
                check_dtbio(con, cur, config)
                con.commit()
        except: # Sic.
            e = traceback.format_exc()
            if e != exception_old:
                print()
                print(e)
                exception_old = e
            for _ in range(60):
                time.sleep(2)

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
