% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Werk (Lemma)</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'werk')
</div>
<table data-id="{{!kwargs['item_id']}}">
	<tr>
		<th scope="row">Vw. auf gedrucktes Rep.</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk_rep">{{!item.get('werk_rep', '')}}</div></td>
	</tr>
  % for n, subitem in enumerate([ subitem for subitem in item.get('werk2person', []) if subitem.get('werk2person_art', '') == 'autor' ] or [{}]):
	<tr data-id="{{!subitem.get('werk2person_id', 0)}}" data-kind="autor" data-rank="{{!subitem.get('werk2person_rang', '0')}}" data-parent_col="werk2person_werk_id">
		<th scope="row">Autor 📋
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2person_person_id">\\
		  % ident = subitem.get('werk2person_person_id', 0)
		  % if ident:
<a href="/person/{{!ident}}">{{!db.get_linktext('person', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Haupttitel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk_haupttitel">{{!item.get('werk_haupttitel', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Nebentitel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk_nebentitel">{{!item.get('werk_nebentitel', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Beschreibung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk_beschreibung">{{!item.get('werk_beschreibung', '')}}</div></td>
	</tr>
  % for n, subitem in enumerate([ subitem for subitem in item.get('zeit', []) if subitem.get('zeit_art', '') == 'entstehung' ] or [{}]):
	<tr data-id="{{!subitem.get('zeit_id', 0)}}" data-kind="entstehung" data-parent_col="zeit_werk_id">
		<th scope="row">Entstehungszeit
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="zeit_bezeichnung">{{!subitem.get('zeit_bezeichnung', '')}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate([ subitem for subitem in item.get('zeit', []) if subitem.get('zeit_art', '') == 'bericht' ] or [{}]):
	<tr data-id="{{!subitem.get('zeit_id', 0)}}" data-kind="bericht" data-parent_col="zeit_werk_id">
		<th scope="row">Berichtszeit
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="zeit_bezeichnung">{{!subitem.get('zeit_bezeichnung', '')}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('werk2region', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2region_id', 0)}}" data-parent_col="werk2region_werk_id">
		<th scope="row">Region 📋
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2region_region_id">\\
		  % ident = subitem.get('werk2region_region_id', 0)
		  % if ident:
<a href="/region/{{!ident}}">{{!db.get_linktext('region', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('werk2gattung', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2gattung_id', 0)}}" data-parent_col="werk2gattung_werk_id">
		<th scope="row">Gattung 📋
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2gattung_gattung_id">\\
		  % ident = subitem.get('werk2gattung_gattung_id', 0)
		  % if ident:
<a href="/gattung/{{!ident}}">{{!db.get_linktext('gattung', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('werk2schlagwort', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2schlagwort_id', 0)}}" data-parent_col="werk2schlagwort_werk_id">
		<th scope="row">Schlagwort 📋
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2schlagwort_schlagwort_id">\\
		  % ident = subitem.get('werk2schlagwort_schlagwort_id', 0)
		  % if ident:
<a href="/schlagwort/{{!ident}}">{{!db.get_linktext('schlagwort', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('werk2sprache', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2sprache_id', 0)}}" data-parent_col="werk2sprache_werk_id">
		<th scope="row">Sprache 📋
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2sprache_sprache_id">\\
		  % ident = subitem.get('werk2sprache_sprache_id', 0)
		  % if ident:
<a href="/sprache/{{!ident}}">{{!db.get_linktext('sprache', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('zeuge', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="zeuge" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Hs.
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Handschrift 📋</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
			  % for nn, subsubitem in enumerate(subitem.get('zu', []) or [{}]):
				<tr data-id="{{!subsubitem.get('lit2lit_id', 0)}}" data-kind="zu" data-rank="{{!subsubitem.get('lit2lit_rang', '0')}}" data-parent_col="lit2lit_rhema_lit_id" data-grandparent_col="lit2lit_werk_id">
					<th scope="row">Literatur zur Hs.
					  % if nn:
						<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
					  % end
					</th>
					<td>
						<table>
							<tr>
								<th scope="row">Publikation 📋🔗</th>
								<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_thema_lit_id">\\
								  % ident = subsubitem.get('lit2lit_thema_lit_id', 0)
								  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
								  % end
</div></td>
							</tr>
							<tr>
								<th scope="row">Seite / ID</th>
								<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_stelle">{{!subsubitem.get('lit2lit_stelle', '')}}</div></td>
							</tr>
							<tr>
								<th scope="row">Anmerkung</th>
								<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_anmerkung">{{!subsubitem.get('lit2lit_anmerkung', '')}}</div></td>
							</tr>
						</table>
					</td>
				</tr>
			  % end
				<tr>
					<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('zeugenlit', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="zeugenlit" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Literatur zu Hss. gesamt
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('faksimile', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="faksimile" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Faksimile
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('altübersetzung', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="altübersetzung" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Alte Übersetzung
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Sprache 📋</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_sprache_id">\\
					  % ident = subitem.get('werk2lit_sprache_id', 0)
					  % if ident:
<a href="/sprache/{{!ident}}">{{!db.get_linktext('sprache', ident)}}</a>\\
					  % end
</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('ausgabe', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="ausgabe" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Ausgabe
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('übersetzung', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="übersetzung" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Übersetzung
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Sprache 📋</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_sprache_id">\\
					  % ident = subitem.get('werk2lit_sprache_id', 0)
					  % if ident:
<a href="/sprache/{{!ident}}">{{!db.get_linktext('sprache', ident)}}</a>\\
					  % end
</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate(item.get('zumwerk', []) or [{}]):
	<tr data-id="{{!subitem.get('werk2lit_id', 0)}}" data-kind="zumwerk" data-rank="{{!subitem.get('werk2lit_rang', '0')}}" data-parent_col="werk2lit_werk_id">
		<th scope="row">Literatur zum Werk
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_lit_id">\\
					  % ident = subitem.get('werk2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_stelle">{{!subitem.get('werk2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_anmerkung">{{!subitem.get('werk2lit_anmerkung', '')}}</div></td>
				</tr>
				<tr class="marginal">
					<th>Rubrik</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk2lit_rubrik">{{!subitem.get('werk2lit_rubrik', '')}}</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Bearbeitungsstand</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="werk_stand">{{!item.get('werk_stand', '')}}</div></td>
	</tr>
</table>
</article>
</main>
