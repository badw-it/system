% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Erwähnte Person</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'person')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="erwähnt">
	<tr>
		<th scope="row">GND</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_gnd">{{!item.get('person_gnd', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Name (DNB-Form)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_hauptname">{{!item.get('person_hauptname', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_anmerkung">{{!item.get('person_anmerkung', '')}}</div></td>
	</tr>
</table>
</article>
</main>