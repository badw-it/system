% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
% # Template for displaying a datatable.
% kind = kwargs['kind']
<article class="index" data-dbtable="{{db.config['kind_table_map'].get(kind, kind)}}">
	<a class="key" href="/eingabe/{{kind}}">Neuen Eintrag anlegen</a>
	<table id="index">
		<thead>
			<tr>
				<th data-sortable="">Verweistext (Hauptname)</th>
			  % if kind not in {'bibliothek', 'gattung', 'hs', 'ink', 'region', 'schlagwort', 'sprache', 'zsaufsatz'}:
				<th data-sortable="">Weitere Angaben (wie Nebennamen, IDs)</th>
			  % end
			  % if kind in {'autor', 'person'}:
				<th>Heilig? Ja / Nein</th>
			  % elif kind == 'ort':
				<th>Quellenort? Ja / Nein</th>
			  % end
				<th>Link</th>
				<th>Ändern</th>
			</tr>
		</thead>
	</table>
	<a class="key" href="/eingabe/{{kind}}">Neuen Eintrag anlegen</a>
</article>