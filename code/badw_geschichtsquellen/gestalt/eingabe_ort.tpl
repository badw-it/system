% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Ort</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'ort')
</div>
<table data-id="{{!kwargs['item_id']}}">
	<tr>
		<th scope="row">Normdatei (GND, BnF …) 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_normverweis">{{!item.get('ort_normverweis', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Name</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_name">{{!item.get('ort_name', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Breitengrad</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_breite">{{!item.get('ort_breite', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Längengrad</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_länge">{{!item.get('ort_länge', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Orden</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_orden">{{!item.get('ort_orden', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Art<p class="petit">Ggf. “Bistum”, “Kloster” oder “Land”, sonst leer</p></th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_gattung">{{!item.get('ort_gattung', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="ort_anmerkung">{{!item.get('ort_anmerkung', '')}}</div></td>
	</tr>
</table>
</article>
</main>