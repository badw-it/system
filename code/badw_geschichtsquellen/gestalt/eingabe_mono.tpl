% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Monographie</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'mono')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="mono">
  % for n, subitem in enumerate([ subitem for subitem in item.get('person2lit', []) if subitem.get('person2lit_art', '') == 'autor' ] or [{}]):
	<tr data-id="{{!subitem.get('person2lit_id', 0)}}" data-kind="autor" data-rank="{{!subitem.get('person2lit_rang', '0')}}" data-parent_col="person2lit_lit_id">
		<th scope="row">Autor
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person2lit_person_id">{{!db.get_linktext('person', subitem.get('person2lit_person_id', 0))}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Sigle (wenn Std.werk)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_abkürzung">{{!item.get('lit_abkürzung', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Haupttitel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_haupttitel">{{!item.get('lit_haupttitel', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Untertitel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_untertitel">{{!item.get('lit_untertitel', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Band</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_teilnr">{{!item.get('lit_teilnr', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Auflage</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_auflage">{{!item.get('lit_auflage', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Buchserie</th>
		<td>
			<table>
				<tr>
					<th scope="row">Serientitel</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_reihentitel">{{!item.get('lit_reihentitel', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Seriennummer</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_reihennr">{{!item.get('lit_reihennr', '')}}</div></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<th scope="row">Erscheinungsort</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_ort">{{!item.get('lit_ort', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Erscheinungsjahr</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_jahr">{{!item.get('lit_jahr', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Neuauflage</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_neuauflage">{{!item.get('lit_neuauflage', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Nachdruck (Jahr)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_nachdruck">{{!item.get('lit_nachdruck', '')}}</div></td>
	</tr>
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'bv' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="bv" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">BV-Nummernlink 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'vd16' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="vd16" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">VD 16 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'vd17' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="vd17" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">VD 17 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'vd18' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="vd18" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">VD 18 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for n, subitem in enumerate([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == '' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Sonstiger 🔗
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_anmerkung">{{!item.get('lit_anmerkung', '')}}</div></td>
	</tr>
</table>
</article>
</main>