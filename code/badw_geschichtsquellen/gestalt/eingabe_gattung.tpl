% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Gattung</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'gattung')
</div>
<table data-id="{{!kwargs['item_id']}}">
	<tr>
		<th scope="row">Bezeichnung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="gattung_bezeichnung">{{!item.get('gattung_bezeichnung', '')}}</div></td>
	</tr>
</table>
</article>
</main>