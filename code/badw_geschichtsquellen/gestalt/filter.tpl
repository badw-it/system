% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a filter form and filter results.
% lang_id = kwargs['lang_id']
% if request.query.text:
<article class="index">
	<table id="index">
		<thead>
			<tr>
				<th>Autor / Werk</th>
				<th>Name und Link</th>
				<th>Fund</th>
			</tr>
		</thead>
	</table>
</article>
% else:
<main>
<article lang="{{lang_id}}">
	<h1><flag- style="font-size: 0.4em"><l- l="de">neu<br/>mit Karte</l-><l- l="en">new<br/>with map</l-></flag->
		<l- l="de">Erweiterte Suche nach Werken</l->
		<l- l="en">Advanced search within work articles</l->
		<small>(<l- l="de"><a href="/hilfe#suche_de">Hilfe</a></l-><l- l="en"><a href="/hilfe#suche_en">Search tips</a></l->)</small>
	</h1>
	<section class="just card" role="search">
		<form class="flextable" method="post">
			<input type="hidden" name="selects" value="{{!kwargs.get('selectstring', '')}}"/>
			<p>
				<button type="submit" formaction="filter?alt=#treffer">
					<l- l="de">Suchen</l->
					<l- l="en">Search</l->
				</button>
				<a class="key" href="/filter">
					<l- l="de">Leeren</l->
					<l- l="en">Clear</l->
				</a>
			</p>
			<p class="p petit">
				<l- l="de">Die folgenden Felder sind mit Angaben zu füllen oder zur Auswahl von Angaben anzuklicken. Diese Auswahl wird je nach den schon vorgenommenen Angaben vorgefiltert.</l->
				<l- l="en">In the following fields, you may either enter a search term or select items from the lists. If you combine search strategies, the entered search term pre-filters the list selection results.</l->
			</p>
			<label class="card">
				<div style="width: 11rem">
					<l- l="de">Volltext:</l->
					<l- l="en">Full text search:</l->
				</div>
				<input type="search" name="text" value="{{request.forms.text}}" aria-label="{{db.glosses['search_term'][lang_id]}}" autofocus=""/>
			</label>
			<p>
				<label class="key">
					<input type="checkbox" name="desc" value="1"{{!'' if ('desc' in request.forms and not request.forms.desc) else ' checked=""'}}/>
					<l- l="de">Nur in Beschreibungstexten suchen</l->
					<l- l="en">Search only in descriptive texts (not in bibliographical data).</l->
				</label>
			</p>
			<p class="p petit">
				<l- l="de">Der Unterstrich (<kbd>_</kbd>) ist Platzhalter für genau ein Zeichen. Das Prozentzeichen (<kbd>%</kbd>) ist Platzhalter für kein, ein oder mehr als ein Zeichen. Ganz am Anfang und ganz am Ende der Sucheingabe sind die Platzhalterzeichen überflüssig.</l->
				<l- l="en">The underscore (<kbd>_</kbd>) may be used as a wildcard for exactly one letter or numeral. The percent sign (<kbd>%</kbd>) may be used as a wildcard for 0, 1 or more letters or numerals. Wildcards at the beginning and at the end of a search term are unneccessary.</l->
			</p>
			<label class="card">
				<div style="width: 11rem">
					<l- l="de">Entstehungszeit von-bis</l-><l- l="en">Date of origin: from-to</l->:
				</div>
				<input type="text" name="entstehung_von_bis" value="{{request.forms.entstehung_von_bis}}"/>
			</label>
			<label class="card">
				<div style="width: 11rem">
					<l- l="de">Berichtszeit von-bis</l-><l- l="en">Date of events: from-to</l->:
				</div>
				<input type="text" name="bericht_von_bis" value="{{request.forms.bericht_von_bis}}"/>
			</label>
			<p class="p petit">
				<l- l="de">Regeln für die Zeitangaben: Einzugeben ist ein Jahr (wie <kbd>1203</kbd>) oder – um eine Zeitspanne auszudrücken – zwei Jahre mit Trennstrich getrennt (wie <kbd>1050-1350</kbd>) oder ein Jahr mit führendem oder schließendem Trennstrich: <kbd>-1203</kbd> heißt ‘bis 1203 einschließlich’; <kbd>1203-</kbd> heißt ‘ab 1203 einschließlich’.</l->
				<l- l="en">Date entries: Dates are entered as a single year (e.g. <kbd>1203</kbd>). To search in a limited time period, write the beginning and end dates separated by a hyphen (e.g. <kbd>1050-1350</kbd>). To search from a certain date onwards, write a hyphen following the beginning date (e.g. <kbd>1203-</kbd> = ‘from 1203 onwards’); conversely, to search for items up to a certain date, write the hyphen in front of the end date (e.g.: <kbd>-1203</kbd> = ‘until 1203’)</l->
			</p>
			<label class="key">
				<button type="submit" formaction="filter?filter=gattung" style="width: 11rem">
					<l- l="de">Gattung</l-><l- l="en">Genre</l->:
				</button>
				<p>{{kwargs.get('selects', {}).get('gattung', '')}}</p>
			</label>
			<label class="key">
				<button type="submit" formaction="filter?filter=region" style="width: 11rem">Region:</button>
				<p>{{kwargs.get('selects', {}).get('region', '')}}</p>
			</label>
			<label class="key">
				<button type="submit" formaction="filter?filter=schlagwort" style="width: 11rem">
					<l- l="de">Schlagwort</l-><l- l="en">Keyword</l->:
				</button>
				<p>{{kwargs.get('selects', {}).get('schlagwort', '')}}</p>
			</label>
			<label class="key">
				<button type="submit" formaction="filter?filter=sprache" style="width: 11rem">
					<l- l="de">Sprache</l-><l- l="en">Language</l->:
				</button>
				<p>{{kwargs.get('selects', {}).get('sprache', '')}}</p>
			</label>
			<label class="key">
				<button type="submit" formaction="filter?filter=ort" style="width: 11rem">
					<l- l="de">Quellenort</l-><l- l="en">Source places</l->:
				</button>
				<p>{{kwargs.get('selects', {}).get('ort', '')}}</p>
			</label>
			<label class="key">
				<button type="submit" formaction="filter?filter=person" style="width: 11rem">Person:</button>
				<p>{{kwargs.get('selects', {}).get('person', '')}}</p>
			</label>
			<p>
				<button type="submit" formaction="filter?alt=#treffer">
					<l- l="de">Suchen</l->
					<l- l="en">Search</l->
				</button>
				<a class="key" href="/filter">
					<l- l="de">Leeren</l->
					<l- l="en">Clear</l->
				</a>
			</p>
		</form>
	</section>
	<section>
	  % items = tuple(kwargs.get('items', ()))
		<h2 id="treffer">
			<l- l="de">Treffer</l->
			<l- l="en">Results</l->
			({{len(items)}})
		</h2>
	  % if len(items) > 0:
		<flag- class="petit"><l- l="de">Neu</l-><l- l="en">New</l-></flag->
		<button class="dark" onclick="geo(event, {'work_ID_string': '{{!','.join( str(item['werk_id']) for item in items )}}'})" type="button"><l- l="de">Karte</l-><l- l="en">Map</l-></button>
		<span id="geo-anchor"></span>
	  % end
		<ul>
	  % for item in items:
			<li><a class="key" href="/werk/{{!item['werk_id']}}" rel="noopener noreferrer" target="_blank"><cite>{{!item['werk_haupttitel']}}</cite>{{!' ' + item['person_hauptname'] if item['person_hauptname'] else ''}}</a></li>
	  % end
		</ul>
	</section>
</article>
</main>
% end