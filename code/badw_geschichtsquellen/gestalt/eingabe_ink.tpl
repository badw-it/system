% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Inkunabel</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'ink')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="ink">
	<tr>
		<th scope="row">Erscheinungsort</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_ort">{{item.get('lit_ort', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Erscheinungsjahr</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_jahr">{{item.get('lit_jahr', '')}}</div></td>
	</tr>
  % for n, subitem in enumerate([ subitem for subitem in item.get('person2lit', []) if subitem.get('person2lit_art', '') == 'drucker' ] or [{}]):
	<tr data-id="{{!subitem.get('person2lit_id', 0)}}" data-kind="drucker" data-rank="{{!subitem.get('person2lit_rang', '0')}}" data-parent_col="person2lit_lit_id">
		<th scope="row">Drucker 📋
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person2lit_person_id">\\
		  % ident = subitem.get('person2lit_person_id', 0)
		  % if ident:
<a href="/person/{{!ident}}">{{!db.get_linktext('person', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Titel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_haupttitel">{{!item.get('lit_haupttitel', '')}}</div></td>
	</tr>
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'hain' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="hain" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Hain (ID)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'copinger' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="copinger" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Copinger (ID)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'istc' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="istc" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">ISTC 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'gw' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="gw" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">GW 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'bsbink' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="bsbink" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">BSB-Ink. 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for n, subitem in enumerate([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == '' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Sonstiger 🔗
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_anmerkung">{{!item.get('lit_anmerkung', '')}}</div></td>
	</tr>
</table>
</article>
</main>