% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% item_id = kwargs['item_id']
% groupby = kwargs['groupby']
<main>
<article class="just sheet werk wide">
<h1 id="haupt"><cite>{{!item['werk_haupttitel']}}</cite></h1>
% if item['werk_nebentitel']:
<p><cite>({{!item['werk_nebentitel']}})</cite></p>
% end
<p><small>Repertorium Fontium {{!item['werk_rep'] if item['werk_rep'] else '–, –'}}</small></p>
<table class="card mapping">
<tr>
	<th scope="row">Autor</th>
	<td>
	% for num, subitem in enumerate(item['autoren']):
	  % if num:
		<br/>
	  % end
		<a href="/autor/{{!subitem['person_id']}}" id="autor">{{!subitem['person_hauptname']}}</a>
	% end
	</td>
</tr>
% for case in ('entstehungszeit', 'berichtszeit', 'gattung', 'region', 'schlagwort', 'sprache'):
<tr>
	<th scope="row">{{!case.title()}}</th>
	<td id="{{!case}}">{{!item[f'werk_{case}']}}</td>
</tr>
% end
</table>
<h2 data-not-for-ft-desc="">Beschreibung</h2>
{{!item['werk_beschreibung']}}
% case = item['werk_lit'].get('zeugenlit', ())
% if item['werk_zeuge'] or case:
<h2 data-not-for-ft-desc="">Handschriften – Mss.</h2>
<ul class="list">
  % for subitem in item['werk_zeuge']:
	<li><span data-not-for-ft-desc=""><a href="/hs/{{!subitem['lit_id']}}">{{!subitem['bibliothek_ort']}}, {{!subitem['bibliothek_institution']}}, {{!subitem['lit_signatur']}}</a>{{!', ' + subitem['werk2lit_stelle'] if subitem['werk2lit_stelle'] else ''}}</span> {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}
	  % if subitem['lit']:
		<ul class="list">
		  % for _, string, subsubitem in subitem['lit']:
			<li>{{!string}} {{!('<span class="anm">' + subsubitem['lit2lit_anmerkung'] + '</span>') if subsubitem['lit2lit_anmerkung'] else ''}}</li>
		  % end
		</ul>
	  % end
	</li>
  % end
</ul>
% end
% if case:
<h3 data-not-for-ft-desc="">Literatur zu den Handschriften allgemein</h3>
<ul class="list">
  % for _, string, subitem in case:
	<li>{{!string}} {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}</li>
  % end
</ul>
% end
% case = item['werk_lit'].get('faksimile', ())
% if case:
<h2 data-not-for-ft-desc="">Faksimile-Ausgaben</h2>
<ul class="list">
  % for _, string, subitem in case:
	<li>{{!string}} {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}</li>
  % end
</ul>
% end
% case = item['werk_lit'].get('altübersetzung', ())
% if case:
<h2 data-not-for-ft-desc="">Alte Übersetzungen – Vet. Transl.</h2>
  % for key, group in groupby(case, key = lambda x: x[0][0]):
<h3 data-not-for-ft-desc="">{{key.lstrip('0')}}</h3>
<ul class="list">
	% for _, string, subitem in group:
	<li>{{!string}} {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}</li>
	% end
</ul>
  % end
% end
% case = item['werk_lit'].get('ausgabe', ())
% if case:
<h2 data-not-for-ft-desc="">Ausgaben – Edd.</h2>
<ul class="list">
  % for _, string, subitem in case:
	<li>{{!string}} {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}</li>
  % end
</ul>
% end
% case = item['werk_lit'].get('übersetzung', ())
% if case:
<h2 data-not-for-ft-desc="">Übersetzungen – Transl.</h2>
  % for key, group in groupby(case, key = lambda x: x[0][0]):
<h3 data-not-for-ft-desc="">{{key.lstrip('0')}}</h3>
<ul class="list">
	% for _, string, subitem in group:
	<li>{{!string}} {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}</li>
	% end
</ul>
  % end
% end
% case = item['werk_lit'].get('zumwerk', ())
% if case:
<h2 data-not-for-ft-desc="">Literatur zum Werk – Comm.</h2>
  % for key, group in groupby(case, key = lambda x: x[0][0]):
	% if key:
<h3 data-not-for-ft-desc="">{{key.lstrip('0')}}</h3>
	% end
<ul class="list">
	% for _, string, subitem in group:
	<li>{{!string}} {{!('<span class="anm">' + subitem['werk2lit_anmerkung'] + '</span>') if subitem['werk2lit_anmerkung'] else ''}}</li>
	% end
</ul>
  % end
% end
% for subitem in item['autoren']:
  % if subitem['has_person_lit']:
<h2 data-not-for-ft-desc=""><a class="key" href="/autor/{{!subitem['person_id']}}#lit">Literatur zum Autor – Comm. gen.</a></h2>
  % end
% end
% if item['werk_erwähnung_in_person']:
<h2 data-not-for-ft-desc="">Erwähnungen in Autorartikeln</h2>
<ul class="list" data-not-for-ft-desc="">
  % for subitem in item['werk_erwähnung_in_person']:
    <li><a href="/autor/{{!subitem['person_id']}}">{{!subitem['person_hauptname']}}</a></li>
  % end
</ul>
% end
% if item['werk_erwähnung_in_werk']:
<h2 data-not-for-ft-desc="">Erwähnungen in Werkartikeln</h2>
<ul class="list" data-not-for-ft-desc="">
  % for subitem in item['werk_erwähnung_in_werk']:
	% person_hauptname = f''' ({subitem['person_hauptname']})''' if subitem['person_hauptname'] else ''
    <li><a href="/werk/{{!subitem['werk_id']}}">{{!subitem['werk_haupttitel']}}{{!person_hauptname}}</a></li>
  % end
</ul>
% end
<div class="citebox">
	<dl>
		<dt>Zitiervorschlag:</dt>
		<dd>https://www.geschichtsquellen.de/werk/{{item_id}} (Bearbeitungsstand: {{!item['werk_stand']}})</dd>
		<!-- <dd>https://www.geschichtsquellen.de/werk/{{item_id}}?version={{item['latest_version']}} (Bearbeitungsstand: {{!item['werk_stand']}})</dd> -->
	</dl>
</div>
<p><a class="key" href="https://geschichtsquellen.badw.de/kontakt.html">Korrekturen / Ergänzungen melden</a></p>
</article>
</main>