% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Bibliothek</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'bibliothek')
</div>
<table data-id="{{!kwargs['item_id']}}">
	<tr>
		<th scope="row">Ort</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="bibliothek_ort">{{!item.get('bibliothek_ort', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Institution</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="bibliothek_institution">{{!item.get('bibliothek_institution', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">ISIL-ID</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="bibliothek_isil">{{!item.get('bibliothek_isil', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="bibliothek_anmerkung">{{!item.get('bibliothek_anmerkung', '')}}</div></td>
	</tr>
</table>
</article>
</main>
