% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Sammelbandaufsatz</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'sammelbandaufsatz')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="sammelbandaufsatz">
  % for n, subitem in enumerate([ subitem for subitem in item.get('person2lit', []) if subitem.get('person2lit_art', '') == 'autor' ] or [{}]):
	<tr data-id="{{!subitem.get('person2lit_id', 0)}}" data-kind="autor" data-rank="{{!subitem.get('person2lit_rang', '0')}}" data-parent_col="person2lit_lit_id">
		<th scope="row">Autor
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person2lit_person_id">{{!db.get_linktext('person', subitem.get('person2lit_person_id', 0))}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Titel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_haupttitel">{{!item.get('lit_haupttitel', '')}}</div></td>
	</tr>
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem.get('lit2lit_art', '') == 'in' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="in" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Sammelband 📋</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">\\
		  % ident = subitem.get('lit2lit_rhema_lit_id', 0)
		  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
		  % end
</div></td>
	</tr>
  % end
	<tr>
		<th scope="row">Seiten</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_stelle">{{!item.get('lit_stelle', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_anmerkung">{{!item.get('lit_anmerkung', '')}}</div></td>
	</tr>
</table>
</article>
</main>