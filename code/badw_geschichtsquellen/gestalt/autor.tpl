% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% item_id = kwargs['item_id']
<main>
<article class="just sheet wide">
<h1 id="haupt">{{!item['person_hauptname']}}</h1>
% if item['person_nebenname']:
<p id="neben">({{!item['person_nebenname']}})</p>
% end
% if item['person_gnd']:
<p>GND <span id="gnd">{{!item['person_gnd']}}</span>
  % if item['person_dtbioverweis']:
	<a data-not-for-ft-desc="" href="https://www.deutsche-biographie.de/gnd{{!item['person_gnd']}}.html" class="key" rel="noopener noreferrer" target="_blank">Dt. Biographie</a>
  % end
	<a data-not-for-ft-desc="" href="http://d-nb.info/gnd/{{!item['person_gnd']}}" class="key" rel="noopener noreferrer" target="_blank">DNB</a>
  % if item['person_wikiverweis']:
	<a data-not-for-ft-desc="" href="{{item['person_wikiverweis']}}" class="key" rel="noopener noreferrer" target="_blank">Wikipedia</a>
  % end
</p>
% end
<h2 data-not-for-ft-desc="">Leben – Vita</h2>
{{!item['person_beschreibung']}}
<h2 data-not-for-ft-desc="">Werke</h2>
<ul class="list">
  % for subitem in item['person_werke']:
    <li><a href="/werk/{{!subitem['werk_id']}}">{{!subitem['werk_haupttitel']}}</a></li>
  % end
</ul>
% if item['person_lit']:
<h2 id="lit" data-not-for-ft-desc="">Allgemeine Literatur – Comm. gen.</h2>
<ul class="list">
  % for _, string, subitem in item['person_lit']:
    <li>{{!string}} {{!('<span class="anm">' + subitem['person2lit_anmerkung'] + '</span>') if subitem['person2lit_anmerkung'] else ''}}</li>
  % end
</ul>
% end
% if item['person_erwähnung_in_person']:
<h2 data-not-for-ft-desc="">Erwähnungen in Artikeln zu anderen Autoren</h2>
<ul class="list" data-not-for-ft-desc="">
  % for subitem in item['person_erwähnung_in_person']:
    <li><a href="/autor/{{!subitem['person_id']}}">{{!subitem['person_hauptname']}}</a></li>
  % end
</ul>
% end
% if item['person_erwähnung_in_werk']:
<h2 data-not-for-ft-desc="">Erwähnungen in Werkartikeln</h2>
<ul class="list" data-not-for-ft-desc="">
  % for subitem in item['person_erwähnung_in_werk']:
	% person_hauptname = f''' ({subitem['person_hauptname']})''' if subitem['person_hauptname'] else ''
    <li><a href="/werk/{{!subitem['werk_id']}}">{{!subitem['werk_haupttitel']}}{{!person_hauptname}}</a></li>
  % end
</ul>
% end
<div class="citebox">
	<dl>
		<dt>Zitiervorschlag:</dt>
		<dd>https://www.geschichtsquellen.de/autor/{{item_id}} (Bearbeitungsstand: {{!item['person_stand']}})</dd>
		<!-- <dd>https://www.geschichtsquellen.de/autor/{{item_id}}?version={{item['latest_version']}} (Bearbeitungsstand: {{!item['person_stand']}})</dd> -->
	</dl>
</div>
<p><a class="key" href="http://geschichtsquellen.badw.de/kontakt.html">Korrekturen / Ergänzungen melden</a></p>
</article>
</main>