% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Handschrift</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'hs')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="hs">
	<tr>
		<th scope="row">GND</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_gnd">{{!item.get('lit_gnd', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Bibliothek 📋</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_bibliothek_id">\\
		  % ident = item.get('lit_bibliothek_id', 0)
		  % if ident:
<a href="/bibliothek/{{!ident}}">{{!db.get_linktext('bibliothek', ident)}}</a>\\
		  % end
</div></td>
	</tr>
	<tr>
		<th scope="row">Signatur</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_signatur">{{!item.get('lit_signatur', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Anmerkung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_anmerkung">{{!item.get('lit_anmerkung', '')}}</div></td>
	</tr>
  % for n, subitem in enumerate([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == '' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Digitalisat 🔗
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
  % for n, subitem in enumerate([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] != '' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="????" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Katalog 🔗
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_verweis', ''))}}</div></td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
</table>
</article>
</main>