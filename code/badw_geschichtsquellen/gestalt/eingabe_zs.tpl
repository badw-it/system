% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form">
<div class="flexbase">
<h1>Zeitschrift</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'zs')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="zs">
	<tr>
		<th scope="row">Abkürzung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_abkürzung">{{!item.get('lit_abkürzung', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Titel</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit_haupttitel">{{!item.get('lit_haupttitel', '')}}</div></td>
	</tr>
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'zdb' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="zdb" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">ZDB-Link 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'zdbdigital' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="zdbdigital" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">ZDB-Link (digital) 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
  % for subitem in ([ subitem for subitem in item.get('lit2lit', []) if subitem['lit_art'] == 'verweis' and subitem['lit2lit_art'] == 'zsdigital' ] or [{}]):
	<tr data-id="{{!subitem.get('lit2lit_id', 0)}}" data-kind="zsdigital" data-parent_col="lit2lit_thema_lit_id">
		<th scope="row">Ersatzlink (digital) 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="lit2lit_rhema_lit_id">{{!db.get_linktext('lit', subitem.get('lit2lit_rhema_lit_id', 0))}}</div></td>
	</tr>
  % end
</table>
</article>
</main>