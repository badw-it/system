% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
<main>
<article class="card form" name="autor">
<div class="flexbase">
<h1>Autor</h1>
% include('form_release_delete.tpl', request = request, kwargs = kwargs, table = 'person')
</div>
<table data-id="{{!kwargs['item_id']}}" data-kind="autor">
	<tr>
		<th scope="row">GND</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_gnd">{{!item.get('person_gnd', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Name (Hauptform)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_hauptname">{{!item.get('person_hauptname', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Name (Nebenform)</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_nebenname">{{!item.get('person_nebenname', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Beschreibung</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_beschreibung">{{!item.get('person_beschreibung', '')}}</div></td>
	</tr>
	<tr>
		<th scope="row">Wikipedia 🔗</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_wikiverweis">{{!item.get('person_wikiverweis', '')}}</div></td>
	</tr>
  % for n, subitem in enumerate(item.get('person2lit', []) or [{}]):
	<tr data-id="{{!subitem.get('person2lit_id', 0)}}" data-kind="bezug" data-rank="{{!subitem.get('person2lit_rang', '0')}}" data-parent_col="person2lit_person_id">
		<th scope="row">Literatur
		  % if n:
			<div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>
		  % end
		</th>
		<td>
			<table>
				<tr>
					<th scope="row">Publikation 📋🔗</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="person2lit_lit_id">\\
					  % ident = subitem.get('person2lit_lit_id', 0)
					  % if ident:
<a href="/lit/{{!ident}}">{{!db.get_linktext('lit', ident)}}</a>\\
					  % end
</div></td>
				</tr>
				<tr>
					<th scope="row">Seite / ID</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="person2lit_stelle">{{!subitem.get('person2lit_stelle', '')}}</div></td>
				</tr>
				<tr>
					<th scope="row">Anmerkung</th>
					<td><div class="card rich" onmouseover="initEditor(this)" data-col="person2lit_anmerkung">{{!subitem.get('person2lit_anmerkung', '')}}</div></td>
				</tr>
			</table>
		</td>
	</tr>
  % end
	<tr>
		<td><button type="button" onclick="addBlock(this)">＋</button></td> <td></td>
	</tr>
	<tr>
		<th scope="row">Bearbeitungsstand</th>
		<td><div class="card rich" onmouseover="initEditor(this)" data-col="person_stand">{{!item.get('person_stand', '')}}</div></td>
	</tr>
</table>
<h2>Werke des Autors</h2>
% if item.get('werk2person', ''):
<ul class="list">
  % for linktext, werk_id in item.get('werk2person', ''):
	<li><a href="/werk/{{!werk_id}}">{{!linktext}}</a></li>
  % end
</ul>
% else:
<p>―</p>
% end
</article>
</main>