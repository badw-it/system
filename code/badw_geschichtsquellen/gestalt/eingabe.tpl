% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
<main>
<article class="sheet">
<h2>Eintragstafeln</h2>
<div class="flexrow ">
<ul>
	<li><a href="/eingabe/index/autor">Autor</a></li>
	<li><a href="/eingabe/index/werk">Werk</a></li>
	<li><a href="/eingabe/index/person">ma. Person, die kein Autor ist</a></li>
	<li><a href="/eingabe/index/ort">Ort</a></li>
	<li><a href="/eingabe/index/region">Region</a></li>
	<li><a href="/eingabe/index/gattung">Gattung</a></li>
	<li><a href="/eingabe/index/schlagwort">Schlagwort</a></li>
	<li><a href="/eingabe/index/sprache">Sprache</a></li>
</ul>
<ul>
	<li><a href="/eingabe/index/bibliothek">Bibliothek</a></li>
	<li><a href="/eingabe/index/hs">Handschrift</a></li>
	<li><a href="/eingabe/index/ink">Inkunabel</a></li>
	<li><a href="/eingabe/index/mono">Monographie</a></li>
	<li><a href="/eingabe/index/sammelband">Sammelband</a></li>
	<li><a href="/eingabe/index/sammelbandaufsatz">Sammelbandaufsatz</a></li>
	<li><a href="/eingabe/index/zs">Zeitschrift</a></li>
	<li><a href="/eingabe/index/zsaufsatz">Zeitschriftenaufsatz</a></li>
</ul>
</div>
% if 'redactor' in request.roles:
<h2>Freigabe</h2>
<form action="/redact" class="flexcol" method="post"><button class="lid" type="button">Alle Änderungen und Ergänzungen veröffentlichen?</button><button name="redaction" value="release">Alle Änderungen und Ergänzungen veröffentlichen!</button></form>
 % num = request.query.num
 % if num:
<p>{{'Keine weiteren' if num == '0' else num}} {{'Zeile wurde' if num == '1' else 'Zeilen wurden'}} zur Veröffentlichung freigegeben.</p>
 % end
% end
</article>
</main>
