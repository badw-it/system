% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a filterform of checkboxes.
% lang_id = kwargs['lang_id'] = request.lang_id
% kind = request.query.filter
% selects = kwargs.get('selects', {})
% include('head.tpl', db = db, request = request, kwargs = kwargs)
% kind_en = {'gattung': 'genre', 'ort': 'place', 'schlagwort': 'keyword', 'sprache': 'language'}.get(kind, kind)
<body>
<main class="filterform">
	<form action="filter" method="post">
		<p class="card" lang="{{lang_id}}">
			<button type="submit" formaction="/filter?alt={{kind}}"><l- l="de">Bestätigen</l-><l- l="en">OK</l-></button>
			<button type="submit" formaction="/filter?alt="><l- l="de">Abbrechen</l-><l- l="en">Cancel</l-></button>
			<input class="key" type="reset" value="{{'Reset' if lang_id == 'en' else 'Zurücksetzen'}}"/>
		</p>
		<input type="hidden" name="selects" value="{{request.forms.selects}}"/>
		<input type="hidden" name="text" value="{{request.forms.text}}"/>
		<input type="hidden" name="desc" value="{{request.forms.desc}}"/>
		<input type="hidden" name="entstehung_von_bis" value="{{request.forms.entstehung_von_bis}}"/>
		<input type="hidden" name="bericht_von_bis" value="{{request.forms.bericht_von_bis}}"/>
		<div lang="{{lang_id}}">
		% items = tuple(kwargs['items'])
			<h1><l- l="de">{{kind.title()}}-Auswahl (aus noch {{!len(items)}})</l-><l- l="en">{{kind_en.title()}} selection (out of remaining {{!len(items)}})</l-></h1>
			<ul class="dist">
		% if kind in {'gattung', 'region', 'schlagwort', 'sprache'}:
		  % item_id = kind + '_id'
		  % item_ids = selects.get(kind, frozenset())
		  % for item in items:
			<li>
				<label class="key">
					<input type="checkbox" name="id" value="{{!item[item_id]}}"{{!' checked=""' if item[item_id] in item_ids else ''}}/>
					<span>{{!item[kind + '_bezeichnung']}}</span>
				</label>
			</li>
		  % end
		% elif kind == 'ort':
		  % item_id = kind + '_id'
		  % item_ids = selects.get(kind, frozenset())
		  % for item in items:
			<li>
				<label class="key">
					<input type="checkbox" name="id" value="{{!item['ort_id']}}"{{!' checked=""' if item[item_id] in item_ids else ''}}/>
					<span>{{!item['ort_name']}}\\
					% if item['ort_orden']:
, {{!item['ort_orden']}}
					% end
					</span>
				</label>
			</li>
		  % end
		% elif kind == 'person':
		  % item_id = kind + '_id'
		  % item_ids = selects.get(kind, frozenset())
		  % for item in items:
			<li>
				<label class="key">
					<input type="checkbox" name="id" value="{{!item['person_id']}}"{{!' checked=""' if item[item_id] in item_ids else ''}}/>
					<span>{{!item['person_hauptname']}}</span>
				</label>
			</li>
		  % end
		% end
			</ul>
		</div>
	</form>
</main>
% if not db.debug:
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
% end
</body>
</html>
