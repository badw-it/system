% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
  % if 'redactor' in request.roles:
	<form action="/redact/{{'lit' if table in {'hs', 'ink', 'mono', 'sammelband', 'sammelbandaufsatz', 'zs', 'zsaufsatz'} else table}}/{{kwargs['item_id']}}" class="flexbase" method="post">
		<button onclick="saveAllEditors('.rich')" type="button">Alle Felder speichern</button>
		<div class="flexcol"><button class="lid" type="button">Löschen?!</button><button name="redaction" value="delete">Löschen!</button></div>
	  % if 'person_id' in item or 'werk_id' in item:
		<div class="flexcol"><button class="lid" type="button">Veröffentlichen?</button><button name="redaction" value="release">Veröffentlichen!</button></div>
	  % end
		<a class="key" href="/eingabe/index/{{table}}#{{kwargs['item_id']}}">In die Tafel zu diesem Eintrag springen</a>
	</form>
  % end