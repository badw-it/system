% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<!DOCTYPE html>
<html id="top" class="desk txt" lang="{{kwargs.get('lang_id', 'de')}}">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="{{request.path}}" rel="canonical"/>
	<link href="/-/icons/favicon.ico" rel="icon"/>
  % if kwargs.get('geo'):
	<link href="/cssjs/leaflet/leaflet.css" media="all" rel="stylesheet"/>
  % end
	<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/cssjs/badw_geschichtsquellen.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/cssjs/jquery/jquery.min.js"></script>
  % if request.path.startswith('/eingabe/'):
	<script src="/cssjs/tinymce/tinymce.min.js"></script>
  % end
  % if kwargs.get('geo'):
	<script src="/cssjs/leaflet/leaflet.js"></script>
  % end
	<script src="/cssjs/all.js?v={{db.startsecond}}"></script>
	<script src="/cssjs/badw_geschichtsquellen.js?v={{db.startsecond}}"></script>
	<title>Geschichtsquellen: {{request.path.lstrip('/').title()}}</title>
</head>