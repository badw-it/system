% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id'] = request.lang_id
% include('head.tpl', db = db, request = request, kwargs = kwargs)
<body>
% if 'note' in kwargs:
<input class="flip" id="flip1" type="checkbox"/><label class="key notice top" for="flip1"></label><article class="card">{{kwargs['note']}}</article>
% end
<header lang="{{lang_id}}">
	<a class="img" href="https://badw.de"><img src="/-/icons/badw_marke_name.svg" alt="BAdW"/></a>
	<h1>Geschichts&#173;quellen <br/><small>des deutschen Mittelalters</small></h1>
	<input class="flip" id="flip2" type="checkbox"/><label class="key menuswitch" data-checked="Menü einklappen" data-unchecked="Menü ausklappen" for="flip2"></label>
	<div>
		<nav>
			<ul>
			  % if 'editor' in request.roles:
				<li>{{!'<b>Eingabe</b>' if request.path == '/eingabe' else '<a href="/eingabe">Eingabe</a>'}}</li>
			  % end
				<li>{{!'<b>Start</b>' if request.path == '/start' else '<a href="/start">Start</a>'}}</li>
			</ul>
		</nav>
		<form action="/filter" method="get">
			<input type="search" name="text" value="{{request.query.text}}" aria-label="{{db.glosses['search_term'][lang_id]}}" placeholder="{{'Full-text search' if lang_id == 'en' else 'Volltextsuche'}}"/>
			<div class="droparea shadow">
				<p class="petit">
					<label class="key">
						<input type="checkbox" name="desc" value="1" checked=""/>
						<l- l="de">Nur in Beschreibungs­texten suchen</l->
						<l- l="en">Search only in descriptive texts (not in bibliographical data).</l->
					</label>
				</p>
				<table>
					<tr>
						<th><kbd>_</kbd></th>
						<td>
							<l- l="de">(der Unterstrich) ist Platzhalter für genau ein Zeichen.</l->
							<l- l="en">(the underscore) may be used as a wildcard for exactly one letter or numeral.</l->
						</td>
					</tr>
					<tr>
						<th><kbd>%</kbd></th>
						<td>
							<l- l="de">(das Prozentzeichen) ist Platzhalter für kein, ein oder mehr als ein Zeichen.</l->
							<l- l="en">(the percent sign) may be used as a wildcard for 0, 1 or more letters or numerals.</l->
						</td>
					</tr>
				</table>
				<p class="petit">
					<l- l="de">Ganz am Anfang und ganz am Ende der Sucheingabe sind die Platzhalterzeichen überflüssig.</l->
					<l- l="en">At the very beginning and at the very end of the search input, the wildcard characters are superfluous.</l->
				</p>
			</div><button type="submit">&#128270;&#65038;</button>
		</form>
		<nav>
			<ul>
				% name = '<l- l="de">Erweiterte Suche</l-><l- l="en">Advanced search</l->'
				<li>{{!f'<b>{name}</b>' if request.path == '/filter' and not request.query.text else f'<a href="/filter">{name}</a>'}}</li>
				<li><l- l="de">Quellen</l-><l- l="en">Sources</l->:</li>
				<li>
					<ul>
						% name = '<l- l="de">Autoren</l-><l- l="en">Authors</l->'
						<li>{{!f'<b>{name}</b>' if request.path == '/autor' else f'<a href="/autor">{name}</a>'}}</li>
						% name = '<l- l="de">Werke</l-><l- l="en">Works</l->'
						<li>{{!f'<b>{name}</b>' if request.path == '/werk' else f'<a href="/werk">{name}</a>'}}</li>
						% name = '<l- l="de">Neue Werke</l-><l- l="en">Recently added</l->'
						<li style="padding-left: 1em">{{!f'<b>{name}</b>' if request.path == '/werk_neu' else f'<a href="/werk_neu">{name}</a>'}}</li>
					</ul>
				</li>
				<li>Thesauri:</li>
				<li>
					<ul>
						% name = '<l- l="de">Schlagwörter</l-><l- l="en">Keywords</l->'
						<li>{{!f'<b>{name}</b>' if request.path == '/schlagwort' else f'<a href="/schlagwort">{name}</a>'}}</li>
						<li class="submenu">
							<details{{' open=""' if request.path in {'/kloster', '/ort', '/quellenort'} else ''}}>
								<summary><l- l="de">Orte</l-><l- l="en">Places</l-> <flag-><l- l="de">Neu</l-><l- l="en">New</l-></flag-><a class="key" href="/ort?geo"><l- l="de">Karte</l-><l- l="en">Map</l-></a></summary>
								<ul>
									% name = '<l- l="de">Quellenorte</l-><l- l="en">Source places</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/quellenort' else f'<a href="/quellenort">{name}</a>'}}</li>
									% name = '<l- l="de">Alle Orte</l-><l- l="en">All places</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/ort' else f'<a href="/ort">{name}</a>'}}</li>
									% name = '<l- l="de">Klöster</l-><l- l="en">Monasteries</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/kloster' else f'<a href="/kloster">{name}</a>'}}</li>
								</ul>
							</details>
						</li>
						% name = '<l- l="de">Personen</l-><l- l="en">Persons</l->'
						<li>{{!f'<b>{name}</b>' if request.path == '/person' else f'<a href="/person">{name}</a>'}}</li>
						% name = '<l- l="de">Heilige</l-><l- l="en">Saints</l->'
						<li>{{!f'<b>{name}</b>' if request.path == '/heilig' else f'<a href="/heilig">{name}</a>'}}</li>
						<li class="submenu">
							<details{{' open=""' if request.path in {'/autograph', '/hs', '/unediert', '/unikat'} else ''}}>
								<summary><l- l="de">Überlieferung</l-><l- l="en">Transmission</l-></summary>
								<ul>
									% name = '<l- l="de">Handschriften</l-><l- l="en">Manuscripts</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/hs' else f'<a href="/hs">{name}</a>'}}</li>
									% name = '<l- l="de">Autographe</l-><l- l="en">Autographs</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/autograph' else f'<a href="/autograph">{name}</a>'}}</li>
									% name = '<l- l="de">Unikate</l-><l- l="en">Solitary textual witnesses</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/unikat' else f'<a href="/unikat">{name}</a>'}}</li>
									% name = '<l- l="de">Unediert</l-><l- l="en">Not edited</l->'
									<li>{{!f'<b>{name}</b>' if request.path == '/unediert' else f'<a href="/unediert">{name}</a>'}}</li>
								</ul>
							</details>
						</li>
						<li>{{!'<b>Incipit</b>' if request.path == '/incipit' else '<a href="/incipit">Incipit</a>'}}</li>
					</ul>
				</li>
				<li><l- l="de"><a href="https://geschichtsquellen.badw.de">Über das Projekt</a></l-><l- l="en"><a href="https://geschichtsquellen.badw.de/en/project.html">About the project</a></l-></li>
			</ul>
		</nav>
		<nav class="extra lang">
			<hr/>
			<a href="https://creativecommons.org/licenses/by-sa/4.0/"><l- l="de">Lizenz</l-><l- l="en">Licence</l->:&#8199;CC BY-SA</a> ·
			<a href="/hilfe"><l- l="de">Hilfe</l-><l- l="en">Search&#8199;tips</l-></a> ·
			<l- l="de"><a href="https://geschichtsquellen.badw.de/kontakt.html">Kontakt</a></l-><l- l="en"><a href="https://geschichtsquellen.badw.de/en/contact.html">Contact</a></l-> ·
			<l- l="de"><a href="https://badw.de/data/footer-navigation/impressum.html">Impressum</a></l-><l- l="en"><a href="https://badw.de/en/data/footer-navigation/imprint.html">Legal&#8199;disclosure</a></l-> ·
			<l- l="de"><a href="https://badw.de/data/footer-navigation/datenschutz.html">Datenschutz</a></l-><l- l="en"><a href="https://badw.de/en/data/footer-navigation/privacy-statement.html">Privacy&#8199;policy</a></l->
		  % if request.roles:
			<hr/>
			<strong><a href="/auth_end">{{request.user_name}}: {{db.glosses['logout'][lang_id]}}</a></strong> · <a href="/auth_admin">{{db.glosses['administer'][lang_id]}}</a>
		  % end
		<hr/>
		% for target_lang_id in db.lang_ids:
		  % if target_lang_id == lang_id:
			<b>{{db.glosses['lang_name'][lang_id]}}</b>
		  % else:
			<a href="{{!request.fullpath}}?s={{!target_lang_id}}" hreflang="{{target_lang_id}}" lang="{{target_lang_id}}" rel="alternate" title="{{db.glosses['entails_cookie'][target_lang_id]}}">{{db.glosses['lang_name'][target_lang_id]}}</a>
		  % end
		% end
		</nav>
		<nav class="extra">
			<button onclick="window.print()" type="button"><l- l="de">Drucken</l-><l- l="en">Print</l-></button>
		</nav>
	</div>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>
