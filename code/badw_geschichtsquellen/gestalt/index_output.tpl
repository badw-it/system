% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
% # Template for displaying a datatable.
% lang_id = kwargs['lang_id']
% kind = kwargs['kind']
% item_id = kwargs.get('item_id', 0)
<article class="index" lang="{{lang_id}}">
  <%
	if item_id:
		name = '<i>' + kwargs.get('item_name', '') + '</i>'
		heading = {
			'heilig': 'Autor- und Werkartikel mit ' + name + ' als heiliger Person',
			'hs': 'Autor- und Werkartikel mit der Hs. ' + name,
			'ort': 'Autor- und Werkartikel mit dem Ort ' + name,
			'person': 'Autor- und Werkartikel mit der Person ' + name,
			'quellenort': 'Autor- und Werkartikel mit dem Quellenort ' + name,
			'schlagwort': 'Werkartikel mit dem Schlagwort ' + name,
		}.get(kind, '')
	else:
		heading = {
			'autograph': 'Autographs' if lang_id == 'en' else 'Autographe',
			'autor': 'Author' if lang_id == 'en' else 'Autoren',
			'heilig': 'Saints' if lang_id == 'en' else 'Heilige',
			'hs': 'Manuscripts' if lang_id == 'en' else 'Handschriften',
			'incipit': 'Incipit',
			'kloster': 'Monasteries' if lang_id == 'en' else 'Klöster',
			'ort': 'All places' if lang_id == 'en' else 'Alle Orte',
			'person': 'Persons' if lang_id == 'en' else 'Personen',
			'quellenort': 'Source' if lang_id == 'en' else 'Quellenorte',
			'schlagwort': 'Keywords' if lang_id == 'en' else 'Schlagwörter',
			'unediert': 'Not edited' if lang_id == 'en' else 'Unediert',
			'unikat': 'Solitary textual witnesses' if lang_id == 'en' else 'Unikate',
			'werk': 'Works' if lang_id == 'en' else 'Werke',
			'werk_neu': 'Recently added works' if lang_id == 'en' else 'Neue Werke',
		}.get(kind, '')
	end
  %>
	<h1>{{!heading}}</h1>
  % if kind in {'kloster', 'ort', 'quellenort'}:
	% if kind == 'kloster':
	<a class="key" href="/quellenort?geo"><l- l="de">Quellenorte</l-><l- l="en">Source places</l-></a>
	<a class="key" href="/ort?geo"><l- l="de">Alle Orte</l-><l- l="en">All places</l-></a>
	% elif kind == 'ort':
	<a class="key" href="/quellenort?geo"><l- l="de">Quellenorte</l-><l- l="en">Source places</l-></a>
	<a class="key" href="/kloster?geo"><l- l="de">Klöster</l-><l- l="en">Monasteries</l-></a>
	% elif kind == 'quellenort':
	<a class="key" href="/ort?geo"><l- l="de">Alle Orte</l-><l- l="en">All places</l-></a>
	<a class="key" href="/kloster?geo"><l- l="de">Klöster</l-><l- l="en">Monasteries</l-></a>
	% end
	<flag- class="petit"><l- l="de">Neu</l-><l- l="en">New</l-></flag->
	<button class="dark" id="geo-key" onclick="geo(event, {'place_ID_string': '{{item_id}}', 'kind': '{{kind}}'})" type="button"><l- l="de">Karte</l-><l- l="en">Map</l-></button>
	<l- l="de"><a href="/hilfe#karten-suche_de">Hilfe</a></l-><l- l="en"><a href="/hilfe#karten-suche_en">Search tips</a></l->
	<span id="geo-anchor"></span>
  % end
	<table id="index">
		<thead>
			<tr>
			  % if item_id and kind == 'schlagwort':
				<th><l- l="de">Werk</l-><l- l="en">Work</l-></th>
			  % elif item_id:
				<th><l- l="de">Autor oder Werk?</l-><l- l="en">Author or work?</l-></th>
				<th><l- l="de">Hauptbezeichnung des Autors oder Werkes</l-><l- l="en">Standard name of the author or work</l-></th>
			  % elif kind in {'autograph', 'unediert', 'unikat'}:
				<th><l- l="de">Werk</l-><l- l="en">Work</l-></th>
			  % elif kind == 'schlagwort':
				<th><l- l="de">Bezeichnung</l-><l- l="en">Term</l-></th>
				<th><l- l="de">Treffer (Werke)</l-><l- l="en">Hits (works)</l-></th>
			  % elif kind == 'incipit':
				<th style="width: 50%">Incipit</th>
				<th><l- l="de">Werk</l-><l- l="en">Work</l-></th>
			  % elif kind == 'hs':
				<th><l- l="de">Bibliotheksort, Bibliothek, Signatur</l-><l- l="en">Library and shelfmark</l-></th>
				<th><l- l="de">Treffer (Autoren)</l-><l- l="en">Hits (authors)</l-></th>
				<th><l- l="de">Treffer (Werke)</l-><l- l="en">Hits (works)</l-></th>
			  % elif kind == 'autor':
				<th><l- l="de">Hauptname</l-><l- l="en">Standard name</l-></th>
				<th><l- l="de">Nebenname</l-><l- l="en">Alternative name</l-></th>
				<th>GND</th>
			  % elif kind in {'heilig', 'person'}:
				<th><l- l="de">Hauptname (mit Hervorhebung der Autoren)</l-><l- l="en">Standard name (authors in bold print)</l-></th>
				<th>GND</th>
				<th><l- l="de">Treffer (Autoren)</l-><l- l="en">Hits (authors)</l-></th>
				<th><l- l="de">Treffer (Werke)</l-><l- l="en">Hits (works)</l-></th>
			  % elif kind in {'kloster', 'ort', 'quellenort'}:
				<th>Name</th>
			  % if kind == 'kloster':
				<th><l- l="de">Orden</l-><l- l="en">Monastic order</l-></th>
			  % else:
				<th><l- l="de">Kategorisierung (durch eine Ordensangabe oder als Bistum)</l-><l- l="en">Category (Monastic order or diocese)</l-></th>
			  % end
				<th><l- l="de">Treffer (Autoren)</l-><l- l="en">Hits (authors)</l-></th>
				<th><l- l="de">Treffer (Werke)</l-><l- l="en">Hits (works)</l-></th>
			  % elif kind in {'werk', 'werk_neu'}:
				<th><l- l="de">Haupttitel</l-><l- l="en">Standard title</l-></th>
				<th><l- l="de">Nebentitel</l-><l- l="en">Title translation</l-></th>
				<th><l- l="de">Sekundärtitel</l-><l- l="en">Alternative titles</l-></th>
				<th><l- l="de">Autor</l-><l- l="en">Author</l-></th>
			  % end
			</tr>
		</thead>
	</table>
</article>