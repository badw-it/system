# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
import sys
import urllib.parse
from html import unescape

import __init__
import fs
import sql_io
import web_io
import xmlhtml

def insert(
        url: str,
        text: str,
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.Cursor',
        ) -> None:
    '''
    Extract rows from data given as :param:`url` (which is the URL of a webpage)
    and :param:`text` (which is the HTML of said webpage). Insert said rows into
    a database with the cursor :param:`cur` of the connection :param:`con`.

    (For more information, look at the calling :func:`web_io.fulltext`.)
    '''
    scheme, netloc, path, *_ = urllib.parse.urlsplit(url)
    match = re.match(r'/?(autor|werk)/(\d+)', path)
    if not match:
        return
    art = 1 if match.group(1) == 'autor' else 0
    num = int(match.group(2))
    match = re.search(r'<h1 id="haupt">(.*?)</h1>', text)
    name = match.group(1) if match else ''
    match = re.search(r' id="autor">(.*?)</a>', text)
    if match:
        person = match.group(1)
        if person:
            name += f' ({person})'
    for desc_flag, replace_method in (
            (0, replace_element),
            (1, replace_element_more),
            ):
        new_text = xmlhtml.untag(
                text, replace_element = replace_method).replace('\n', ' ')
        if new_text:
            cur.execute(r"""
                    insert into _index_ft values (1, %s, %s, %s, %s, %s)
                    on duplicate key update
                    ft_new = 1, ft_text = %s, ft_name = %s
                    """,
                    (new_text, art, num, name, desc_flag, new_text, name))
            con.commit()

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    """
    Process :param:`tag` and :param:`attrs`.
    A return value of ``None`` does not change the element.
    If a string is returned, the element is replaced by it
    without escaping; if the empty string is returned, the
    element is deleted.
    If the element is void, :param:`startend` is ``True``.
    """
    if (
            (tag in {
                'aside',
                'details',
                'footer',
                'head',
                'header',
                'nav',
                'script',
                'style',
                })
            or ('data-not-for-ft' in attrs)
            ):
        return ''
    return None

def replace_element_more(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    if (
            (tag in {
                'aside',
                'details',
                'footer',
                'head',
                'header',
                'nav',
                'script',
                'style',
                })
            or ('data-not-for-ft' in attrs)
            or ('data-not-for-ft-desc' in attrs)
            or tag == 'lit-'
            ):
        return ''
    return None

if __name__ == '__main__':
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    urdata = sys.argv[2:]
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    web_io.fulltext(
            int(sys.argv[1]),
            sql_io.get_connect(*fs.get_keys(paths['db_access']), config),
            (
                r"""
                create table if not exists _index_ft
                (
                ft_new TINYINT UNSIGNED NOT NULL DEFAULT 1, -- Whether the record is newly inserted.
                ft_text LONGTEXT NOT NULL, -- Content.
                ft_art TINYINT UNSIGNED NOT NULL, -- Whether the text belongs to an author or not (i.e. to a work).
                ft_id INT UNSIGNED NOT NULL, -- ID of the work item or author item.
                ft_name VARCHAR(8000) NOT NULL, -- Shown in the list of matches as name of the page, e.g. title and author of the work.
                ft_desc TINYINT UNSIGNED NOT NULL, -- Whether the text is only the descriptive text (excluding secondary literature or so).
                UNIQUE (ft_desc, ft_art, ft_id)
                ) COLLATE utf8mb4_german2_ci
                """,
            ),
            config['ids']['start_url'],
            insert,
            headers = dict(config['bot_headers']),
            urls_considered_for_their_text_re = re.compile(
                config['ids']['urls_considered_for_their_text_re']),
            urls_considered_for_further_urls_re = re.compile(
                config['ids']['urls_considered_for_further_urls_re']),
            default_path = '/' + config['ids']['page'],
            after_loop_sqls = (
                r"delete from _index_ft where ft_new = 0",
                r"update _index_ft set ft_new = 0",
                ),
            )
