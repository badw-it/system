# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
Processing and generating of XML or HTML, especially for normalization and other
rewriting or for conversion from or to pythonic data structures.

Regarding rewriting: cf. e.g. :func:`replace`, :func:`untag`, :func:`unnest` and
– for more complicated tasks – :func:`mark` or :func:`itemize`.

Regarding conversion from or to data structures: cf. e.g. :func:`serialize` (and
the functions thereafter that start with “serialize_”) or :func:`table_to_doc`.
'''
import html
import html.entities
import html.parser
try: import regex as re
except ImportError: import re
import typing as ty
import xml.etree.ElementTree as ET
import xml.parsers.expat
from collections import deque
from copy import copy, deepcopy

HTML_CDATAS = { # whose content is treated as cdata, i.e. unescaped.
        'script',
        'style',
        }

HTML_LINEBREAKERS = { # i.e. block elements and br, summary, td, th.
        'address',
        'article',
        'aside',
        'blockquote',
        'br',
        'dd',
        'details',
        'dialog',
        'div',
        'dl',
        'dt',
        'fieldset',
        'figcaption',
        'figure',
        'footer',
        'form',
        'h1',
        'h2',
        'h3',
        'h4',
        'h5',
        'h6',
        'header',
        'hr',
        'li',
        'main',
        'nav',
        'ol',
        'p',
        'pre',
        'section',
        'summary',
        'table',
        'td',
        'th',
        'ul',
        }

HTML_LINEBREAKERS_STR = '|'.join(HTML_LINEBREAKERS)

# HTML self-closing tags according to:
# http://www.w3.org/TR/2014/CR-html5-20140731/syntax.html#void-elements
HTML_VOIDS = {
        'area',
        'base',
        'br',
        'col',
        'embed',
        'hr',
        'img',
        'input',
        'keygen',
        'link',
        'meta',
        'param',
        'source',
        'track',
        'wbr',
        }

STYLE: 'dict[str, dict[str, str]]' = {
        '#table': {
            'border': '1px solid #d0d0d0',
            'border-collapse': 'collapse',
            'max-width': '100%',
            'table-layout': 'fixed',
            },
        '#table td, #table th': {
            'border-bottom': '0.5px solid #d0d0d0',
            'border-left': '1px solid #d0d0d0',
            'border-right': '1px solid #d0d0d0',
            'border-top': '0.5px solid #d0d0d0',
            'padding': '2px 4px 2px 4px',
            'text-align': 'start',
            'vertical-align': 'top',
            },
        '#table th': {
            'border-bottom': '1.5px solid #d0d0d0',
            'border-top': '1.5px solid #d0d0d0',
            'font-weight': 'bolder',
            },
        }

# Functions for :class:`Replacer` and :func:`replace`:
def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'Replacer',
        ) -> 'None|str':
    """
    Consider the element :param:`tag` with :param:`attrs`; return:

    - either ``None``: Then nothing is changed.
    - or a string: Then the whole element – i.e. its starttag and its endtag and
      everything in between – is replaced with this string **unescaped**.

    If the string is the empty string, the whole element is deleted – that might
    be the most common use case, e.g. to delete the head-element of an HTML doc.

    :param startend: ``True``, if the element is self-closing.
    """
    return None

# A variant useful e.g. for :func:`untag`:
def replace_offtext_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'Replacer',
        ) -> 'None|str':
    return '' if tag in {'script', 'style', 'title'} else None

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    """
    Consider the element :param:`tag` with :param:`attrs`; return:

    1. a string: The tag name will be this string. If this is the empty one, the
       whole tag will be deleted.
    2. a dictionary: The attributes will be taken from it. And if itʼs empty, no
       no attributes will be set.
    3. a boolean: If ``True``, the element is made self-closing.

    :param startend: ``True``, if the element is self-closing.
    """
    return tag, attrs, tag in HTML_VOIDS

def replace_text(text: str, parser: 'Replacer') -> str:
    """
    Consider :param:`text`, which is a text part outside tags, to wit unescaped;
    return a string, likewise unescaped: This string will replace the old text.
    """
    return text

def replace_bracket_declaration(
        decl: str,
        parser: 'Replacer',
        ) -> str:
    return ''

def replace_comment(
        comment: str,
        parser: 'Replacer',
        ) -> str:
    return ''

def replace_declaration(
        decl: str,
        parser: 'Replacer',
        ) -> str:
    return ''

def replace_processing_instruction(
        pi: str,
        parser: 'Replacer',
        ) -> str:
    return ''

def consider_endtag(
        old_tag: str,
        old_attrs: 'dict[str, None|str]',
        new_tag: str,
        new_attrs: 'dict[str, None|str]',
        parser: 'Replacer',
        ) -> None:
    """
    React on encountering the endtag of an element whose old name and attributes
    are :param:`old_tag` and :param:`old_attrs` and whose new – in the functions
    :func:`replace_element` and :func:`replace_tag` possibly modified – name and
    attributes are :param:`new_tag` and :param:`new_attrs`. The old and new ones
    may differ or be the same.

    React by changing (not re-assigning) ``parser.info``. The information stored
    in ``parser_info`` can be used in the replace-functions.

    This function is not called at all:

    - if there never was a corresponding starttag.
    - for an endtag within a deleted element.
    - for self-closing tags (use :func:`replace_tag` instead).
    """
    return None
# – These were the functions for :class:`Replacer` and :func:`replace`.

class EscapeError(Exception):
    '''
    Base class for exceptions which are to be raised when a term is meant for an
    unescapable part of XML or HTML but contains characters that make the end of
    said part ambiguous.

    For details, see the warning in the docstring of :func:`serialize`.
    '''
    def __str__(self) -> str:
        return (self.message + ':\n' + self.term)

class CommentEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '“-->” appears in a comment'
        self.term = term

class DeclarationEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = 'There is a “>” in a document declaration '\
                'that is not balanced and not part of an attribute value'
        self.term = term

class DeclarationInstructionEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '“?>” appears in a “<?...?>” part '\
                '(i.e. an XML declaration or an XML processing instruction)'
        self.term = term

class ProcessingInstructionEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '“>” appears in an HTML processing instruction'
        self.term = term

class UnknownDeclarationEscapeError(EscapeError):
    def __init__(self, term: str):
        self.message = '“]>” appears in a “<![...]>” part (i.e. CDATA)'
        self.term = term

class Parser(html.parser.HTMLParser):
    '''
    Base class for specialized XML- or HTML-parser of this module.
    '''
    def __init__(
            self,
            cdatas: 'set[str]' = HTML_CDATAS,
            convert_charrefs: bool = True,
            ):
        super().__init__(convert_charrefs = convert_charrefs)
        self.CDATA_CONTENT_ELEMENTS = cdatas

    def error(self, message: str) -> None:
        raise Exception(f'Parsing Error: {message}')

class Itemizer(Parser):
    '''
    HTML-XML parser for getting a sequence of the parts of a document. The usage
    is illustrated by the testcode below. Further information is provided in the
    docstring of :func:`itemize`, a convenience function for this class.

    >>> doc = """
    ...         <tr><td><p align="left">
    ...           paragraph&apos;s text<br/>
    ...           <a></a>paragraph&#700;s text</tr>
    ...           <!-- comment -->
    ...         </non-match>end."""
    >>> parser = Itemizer()
    >>> parser.parse(doc)
    >>> for item in parser.items:
    ...     print(item)
    [0, '', {}, '', '\\n        ', '', '', '', '']
    [1, 'tr', {}, '', '', '', '', '', '']
    [2, 'td', {}, '', '', '', '', '', '']
    [3, 'p', {'align': 'left'}, '', '', '', '', '', '']
    [0, '', {}, '', "\\n          paragraph's text", '', '', '', '']
    [4, 'br', {}, 'br', '', '', '', '', '']
    [0, '', {}, '', '\\n          ', '', '', '', '']
    [5, 'a', {}, '', '', '', '', '', '']
    [5, '', {}, 'a', '', '', '', '', '']
    [0, '', {}, '', 'paragraphʼs text', '', '', '', '']
    [3, '', {}, 'p', '', '', '', '', '']
    [2, '', {}, 'td', '', '', '', '', '']
    [1, '', {}, 'tr', '', '', '', '', '']
    [0, '', {}, '', '\\n          ', '', '', '', '']
    [0, '', {}, '', '', ' comment ', '', '', '']
    [0, '', {}, '', '\\n        end.', '', '', '', '']
    '''
    def __init__(self, balance: bool = True, cdatas: 'set[str]' = HTML_CDATAS):
        super().__init__(cdatas = cdatas, convert_charrefs = True)
        self.items     = deque()
        self.open_tags = deque()
        self.balance   = balance
        self.num       = 0
        self.nums      = deque()

    def close(self):
        if self.balance:
            while self.open_tags:
                self.items.append([
                        self.nums.pop(),
                        '', {}, self.open_tags.pop(), '', '', '', '', ''])
        Parser.close(self)

    def handle_comment(self, comment: str):
        self.items.append([0, '', {}, '', '', comment, '', '', ''])

    def handle_data(self, data: str):
        if self.items and self.items[-1][4]:
            self.items[-1][4] += data
        else:
            self.items.append([0, '', {}, '', data, '', '', '', ''])

    def handle_decl(self, decl: str):
        self.items.append([0, '', {}, '', '', '', '', decl, ''])

    def handle_endtag(self, tag: str):
        if tag in self.open_tags:
            while self.open_tags:
                open_tag = self.open_tags.pop()
                self.items.append(
                        [self.nums.pop(), '', {}, open_tag, '', '', '', '', ''])
                if open_tag == tag:
                    break
        elif not self.balance:
            self.num += 1
            self.items.append([self.num, '', {}, tag, '', '', '', '', ''])

    def handle_pi(self, pi: str):
        self.items.append([0, '', {}, '', '', '', pi, '', ''])

    def handle_startendtag(self, tag: str, attrs: 'list[tuple[str, None|str]]'):
        self.num += 1
        self.items.append([self.num, tag, dict(attrs), tag, '', '', '', '', ''])

    def handle_starttag(self, tag: str, attrs: 'list[tuple[str, None|str]]'):
        self.open_tags.append(tag)
        self.num += 1
        self.nums.append(self.num)
        self.items.append([self.num, tag, dict(attrs), '', '', '', '', '', ''])

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

    def serialize(self) -> str:
        '''(Cf. :func:`serialize`.)'''
        return ''.join(self.serialize_items())

    def serialize_items(self) -> 'deque[str]':
        '''(Cf. :func:`serialize_items`.)'''
        return serialize_items(self.items)

    def unknown_decl(self, decl: str):
        self.items.append([0, '', {}, '', '', '', '', '', decl])

class Replacer(Parser):
    '''
    HTML-XML parser for changing or deleting the tokens of a document. The usage
    is illustrated by the testcode below. Further information is provided in the
    docstring of :func:`replace`, a convenience function for this class.

    >>> import xmlhtml
    >>>
    >>> def replace_tag(tag, attrs, startend, parser):
    ...     for key in ('style', 'align'):
    ...         attrs.pop(key, '')
    ...     tag = tag if (attrs or tag != 'span') else ''
    ...     startend = tag in xmlhtml.HTML_VOIDS
    ...     return tag, attrs, startend

    >>> def replace_text(text, parser):
    ...     return text.strip()
    >>>
    >>> doc = """
    ...         <tr>
    ...             <td>
    ...                 <p align="left">
    ...                     <span style="font-family: UglySans">
    ...                         some text <br>
    ...                         more text <ins/>
    ...                     </span>
    ...             </td>
    ...     """
    >>>
    >>> replacer = xmlhtml.Replacer(
    ...         replace_tag = replace_tag,
    ...         replace_text = replace_text,
    ...         )
    >>> replacer.parse(doc)
    >>> doc = ''.join(replacer.results)
    >>> print(doc)
    <tr><td><p>some text<br/>more text<ins></ins></p></td></tr>
    '''
    def __init__(
            self,
            replace_element: '''ty.Callable[
                [str, dict[str, None|str], bool, Replacer],
                None|str
                ]''' = replace_element,
            replace_tag: '''ty.Callable[
                [str, dict[str, None|str], bool, Replacer],
                tuple[str, dict[str, None|str], bool]
                ]''' = replace_tag,
            replace_text:                   'ty.Callable[[str, Replacer], str]'
                = replace_text,
            replace_comment:                'ty.Callable[[str, Replacer], str]'
                = replace_comment,
            replace_bracket_declaration:    'ty.Callable[[str, Replacer], str]'
                = replace_bracket_declaration,
            replace_declaration:            'ty.Callable[[str, Replacer], str]'
                = replace_declaration,
            replace_processing_instruction: 'ty.Callable[[str, Replacer], str]'
                = replace_processing_instruction,
            consider_endtag: '''None|ty.Callable[
                [str, dict[str, None|str], str, dict[str, None|str], Replacer],
                None
                ]''' = consider_endtag,
            info: dict = {},
            sort: bool = True,
            cdatas: 'set[str]' = HTML_CDATAS,
            ):
        super().__init__(cdatas = cdatas, convert_charrefs = True)
        self.consider_endtag                = consider_endtag
        self.replace_bracket_declaration    = replace_bracket_declaration
        self.replace_comment                = replace_comment
        self.replace_declaration            = replace_declaration
        self.replace_element                = replace_element
        self.replace_processing_instruction = replace_processing_instruction
        self.replace_tag                    = replace_tag
        self.replace_text                   = replace_text
        self.info                           = copy(info)
        # must be a copy – or else a former instance leaks into a newer one that
        # is called without specifying :param:`info` in the call!
        self.sort                           = sort
        self.results                        = deque()
        self.tags_old                       = deque()
        self.tags_new                       = deque()
        self.in_replaced_element            = False

    def close(self):
        self.tags_old = deque()
        while self.tags_new:
            endtag, attrs = self.tags_new.pop()
            if endtag is None:
                self.in_replaced_element = False
            elif not self.in_replaced_element:
                self.results.append(serialize_tag(
                        endtag, end = True, sort = self.sort))
        self.in_replaced_element = False
        Parser.close(self)

    def handle_comment(self, comment: str):
        if not self.in_replaced_element:
            comment = self.replace_comment(comment, self)
            if comment:
                if '-->' in comment:
                    raise CommentEscapeError(comment)
                self.results.append(f'<!--{comment}-->')

    def handle_data(self, data: str):
        if not self.in_replaced_element:
            self.results.append(escape(self.replace_text(data, self)))

    def handle_decl(self, decl: str):
        if not self.in_replaced_element:
            decl = self.replace_declaration(decl, self)
            if decl:
                if '>' in decl:
                    temp = re.sub(r'''("[^"]*"|'[^']*')''', '', decl)
                    if temp.count('<') != temp.count('>'):
                        raise DeclarationEscapeError(decl)
                self.results.append(f'<!{decl}>')

    def handle_endtag(self, tag: str):
        if tag in { tag for tag, _ in self.tags_old }:
            while self.tags_old:
                old_tag, old_attrs = self.tags_old.pop()
                new_tag, new_attrs = self.tags_new.pop()
                if new_tag is None:
                    self.in_replaced_element = False
                elif not self.in_replaced_element:
                    self.results.append(serialize_tag(
                            new_tag, end = True, sort = self.sort))
                    if self.consider_endtag:
                        self.consider_endtag(
                                old_tag, old_attrs,
                                new_tag, new_attrs, self)
                if old_tag == tag:
                    break

    def handle_pi(self, pi: str):
        if not self.in_replaced_element:
            pi = self.replace_processing_instruction(pi, self)
            if pi:
                if pi[-1] == '?' and '?>' in pi:
                    raise DeclarationInstructionEscapeError(pi)
                elif '>' in pi:
                    raise ProcessingInstructionEscapeError(pi)
                self.results.append(f'<?{pi}>')

    def handle_startendtag(self, tag: str, attrs: 'list[tuple[str, None|str]]'):
        attrs = dict(attrs)
        if not self.in_replaced_element:
            element = self.replace_element(tag, attrs, True, self)
            if element is None:
                tag, attrs, startend = self.replace_tag(tag, attrs, True, self)
                self.results.append(serialize_tag(
                        tag, attrs, startend = startend, sort = self.sort))
                if not startend:
                    self.results.append(serialize_tag(
                            tag, end = True, sort = self.sort))
            else:
                self.results.append(element)

    def handle_starttag(self, tag: str, attrs: 'list[tuple[str, None|str]]'):
        attrs = dict(attrs)
        self.tags_old.append((tag, attrs))
        if self.in_replaced_element:
            endtag = ''
        else:
            element = self.replace_element(tag, attrs, False, self)
            if element is None:
                tag, attrs, startend = self.replace_tag(tag, attrs, False, self)
                self.results.append(serialize_tag(
                        tag, attrs, startend = startend, sort = self.sort))
                if startend:
                    self.tags_old.pop()
                    return
                endtag = tag
            else:
                self.results.append(element)
                endtag = None
                self.in_replaced_element = True
        self.tags_new.append((endtag, attrs))

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

    def unknown_decl(self, undecl: str):
        if not self.in_replaced_element:
            undecl = self.replace_bracket_declaration(undecl, self)
            if undecl:
                if ']>' in undecl:
                    raise UnknownDeclarationEscapeError(undecl)
                self.results.append(f'<![{undecl}]>')

def elem_to_table(
        xmlhtml_table: 'None|ET.Element',
        row_xpath: str = './/tr',
        cell_xpath: str = '*',
        get: 'ty.Callable[[ET.Element], ty.Any]' =
            lambda cell: ''.join(cell.itertext()),
        ) -> 'deque[deque[str]]':
    '''
    Convert :param:`xmlhtml_table` to a table-like deque of deques.

    :param row_xpath: An XPath expression taken to find all rows in the table.
    :param cell_xpath: An XPath expression taken to find all cells within a row.
    :param get: takes each cell element as argument and returns a value which is
        taken as the cell value in the deque of deques.

    >>> import xml.etree.ElementTree as ET
    >>> import xmlhtml
    >>> doc = """<table>
    ...         <tr>
    ...             <td><i>a</i>....</td> <td>b....</td> <td>c....</td> <td>d....</td>
    ...         </tr>
    ...         <tr>
    ...             <td>a....</td> <td>e....</td> <td>f....</td> <td>g....</td> <td>h....</td>
    ...         </tr>
    ...         <tr>
    ...             <td>a....</td> <td>e....</td> <td>f....</td> <td>i....</td> <td>j....</td>
    ...         </tr>
    ...         <tr>
    ...             <td>a....</td> <td>e....</td> <td>k....</td> <td>l....</td>
    ...         </tr>
    ...         </table>"""
    >>> table = xmlhtml.elem_to_table(ET.XML(doc))
    >>> for row in table:
    ...     print(row)
    deque(['a....', 'b....', 'c....', 'd....'])
    deque(['a....', 'e....', 'f....', 'g....', 'h....'])
    deque(['a....', 'e....', 'f....', 'i....', 'j....'])
    deque(['a....', 'e....', 'k....', 'l....'])
    '''
    table = deque()
    if xmlhtml_table:
        for row in xmlhtml_table.iterfind(row_xpath):
            table.append(deque())
            for cell in row.iterfind(cell_xpath):
                table[-1].append(unescape(get(cell)))
    return table

def elem_to_tree(
        xmlhtml_table: 'None|ET.Element',
        row_xpath: str = './/tr',
        cell_xpath: str = '*',
        get: 'ty.Callable[[ET.Element], str]' = lambda cell: cell.text or '',
        dictify: 'ty.Callable' = dict,
        last_is_value: bool = True,
        ) -> 'dict[str, str|dict]':
    '''
    Convert a :param:`xmlhtml_table` to a tree-like dictionary of dictionaries.

    :param row_xpath: An XPath expression taken to find all rows in the table.
    :param cell_xpath: An XPath expression taken to find all cells within a row.
    :param get: takes each cell element as argument and returns a value which is
        taken as the cell value in the deque of deques.
    :param dictify: used to create the dictionaries.
    :param last_is_value: speficies the kind of tree: If ``True``, a branch will
        end with ``{prelast_column: last_column}`` – if ``False``, a branch will
        end with ``{last_column: {}}``.

    >>> import xml.etree.ElementTree as ET
    >>> from pprint import pprint
    >>> import xmlhtml
    >>> xmlhtml_table = ET.XML('<table>\
    ...         <tr>\
    ...             <td>a....</td> <td>b....</td> <td>c....</td> <td>d....</td>\
    ...         </tr>\
    ...         <tr>\
    ...             <td>a....</td> <td>e....</td> <td>f....</td> <td>g....</td> <td>h....</td>\
    ...         </tr>\
    ...         <tr>\
    ...             <td>a....</td> <td>e....</td> <td>f....</td> <td>i....</td> <td>j....</td>\
    ...         </tr>\
    ...         <tr>\
    ...             <td>a....</td> <td>e....</td> <td>k....</td> <td>l....</td>\
    ...         </tr>\
    ...         </table>')
    >>> pprint(xmlhtml.elem_to_tree(xmlhtml_table, row_xpath = 'tr'))
    {'a....': {'b....': {'c....': 'd....'},
               'e....': {'f....': {'g....': 'h....', 'i....': 'j....'},
                         'k....': 'l....'}}}
    '''
    tree = dictify()
    if xmlhtml_table is not None:
        if last_is_value:
            for row in xmlhtml_table.iterfind(row_xpath):
                subtree = tree
                cells = row.findall(cell_xpath)
                for cell in cells[:-2]:
                    subtree = subtree.setdefault(get(cell), dictify())
                if len(cells) > 1:
                    subtree[get(cells[-2])] = get(cells[-1])
                elif len(cells) == 1:
                    subtree[get(cells[-1])] = ''
        else:
            for row in xmlhtml_table.iterfind(row_xpath):
                subtree = tree
                for cell in row.iterfind(cell_xpath):
                    subtree = subtree.setdefault(get(cell), dictify())
    return tree

def escape(text: str) -> str:
    '''
    Escape :param:`text` for the use in XML or HTML:

    - “&” → “&amp;”
    - “<” → “&lt;”
    - “>” → “&gt;”
    - “"” → “&quot;”
    '''
    return text.replace('&', '&amp;'
              ).replace('<', '&lt;'
              ).replace('>', '&gt;'
              ).replace('"', '&quot;')

def get_charset(
        bits: bytes,
        charset_re: 're.Pattern[bytes]' =
            re.compile(rb'''(?i)\bcharset\s*=\s*["']?([-\w]+)'''),
        ) -> str:
    try:
        return charset_re.search(bits[:1024]).group(1).decode()
    except: # Sic.
        return ''

def get_styles(styleterm: str) -> 'dict[str, str]':
    '''
    Convert :param:`styleterm` (a CSS-string representation) into a mapping from
    style labels to style values.

    The inverse function is :func:`serialize_styles`.

    >>> get_styles(' ; display :block; color:red ;')
    {'display': 'block', 'color': 'red'}
    '''
    return dict(
            (style[0].strip(), style[1].strip())
            for style in
                ( term.split(':', 1) for term in styleterm.split(';') )
            if len(style) == 2 )

def get_text(
        element: 'None|ET.Element',
        xpath: str = '.',
        namespaces = None,
        ) -> str:
    '''
    In :param:`element`, search with :param:`xpath` and return all inner text of
    the first matching element – or the empty string. And if :param:`element` is
    ``None``, return the empty string, too.

    The default value of :param:`xpath` refers to :param:`element` itself.
    '''
    if element is None:
        return ''
    match = element.find(xpath, namespaces = namespaces)
    return '' if match is None else ''.join(match.itertext())

def html_to_fods(doc: str) -> str:
    def replace_element(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'Replacer',
            ) -> 'None|str':
        return '' if tag == 'head' else None

    def replace_tag(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'Replacer',
            ) -> 'tuple[str, dict[str, None|str], bool]':
        newtag = ''
        newattrs = {}
        if tag == 'table':
            parser.info['table_level'] += 1
        if parser.info['table_level'] == 1:
            if tag == 'a' and 'href' in attrs:
                newtag = 'text:a'
                newattrs['xlink:href'] = attrs['href']
            elif tag == 'br':
                newtag = 'br'
                startend = True
            elif tag == 'p':
                newtag = 'text:p'
            elif tag in {'td', 'th'}:
                newtag = 'table:table-cell'
                if tag == 'th':
                    newattrs['table:style-name'] = 'b'
            elif tag == 'tr':
                newtag = 'table:table-row'
            elif tag == 'table':
                newtag = 'table:table'
                newattrs['table:name'] = attrs.get(
                        'id', parser.info['table_id'])
                parser.info['table_id'] += 1
        return newtag, newattrs, startend

    def replace_text(text: str, parser: 'Replacer') -> str:
        return text if parser.info['table_level'] == 1 else ''

    def consider_endtag(
            old_tag: str,
            old_attrs: 'dict[str, None|str]',
            new_tag: str,
            new_attrs: 'dict[str, None|str]',
            parser: 'Replacer',
            ) -> None:
        if old_tag == 'table':
            parser.info['table_level'] -= 1
        return None

    replacer = Replacer(
            replace_element = replace_element,
            replace_tag = replace_tag,
            replace_text = replace_text,
            consider_endtag = consider_endtag,
            info = {
                'table_level': 0,
                'table_id': 1,
                },
            )
    replacer.parse(doc)
    doc = ''.join(replacer.results).strip()
    for old, new in (
            (
                r'<table:table-cell([^>]*)>((?:(?!</p|</table:table-cell).)*)</table:table-cell>',
                r'<table:table-cell\g<1>><text:p>\g<2></text:p></table:table-cell>'
            ),# because cell content must be in a text:p-element.
            (r'<br/>', r'</text:p><text:p>'),
            ):
        doc = re.sub(old, new, doc)
    doc = rf'''<?xml version="1.0" encoding="UTF-8"?>
<office:document
	office:mimetype="application/vnd.oasis.opendocument.spreadsheet"
	xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
	xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
	xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
	xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
	xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	xmlns:xlink="http://www.w3.org/1999/xlink"
	>
<office:styles>
	<style:style style:name="Default" style:family="table-cell">
		<style:table-cell-properties style:vertical-align="top"/>
		<style:text-properties style:font-name="Calibri" fo:font-family="Calibri" fo:font-size="11pt"/>
	</style:style>
	<style:style style:name="b" style:family="table-cell" style:parent-style-name="Default">
		<style:text-properties fo:font-weight="bold" style:font-weight-asian="bold" style:font-weight-complex="bold"/>
	</style:style>
</office:styles>
<office:body>
<office:spreadsheet>
{doc}
</office:spreadsheet>
</office:body>
</office:document>
'''
    return doc

def indent(element: ET.Element, level: int = 0, indenter: str = '\t') -> None:
    '''
    Attribution notice: by Fredrik Lundh; minor changes by Stefan Müller.

    See http://effbot.org/zone/element-lib.htm#prettyprint.
    '''
    i = "\n" + level * indenter
    if len(element):
        if not element.text or not element.text.strip():
            element.text = i + indenter
        if not element.tail or not element.tail.strip():
            element.tail = i
        for element in element:
            indent(element, level + 1)
        if not element.tail or not element.tail.strip():
            element.tail = i
    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = i

def itemize(
        doc: str,
        xmlformed: bool = False,
        balance: bool = True,
        cdatas: 'set[str]' = HTML_CDATAS,
        ) -> 'deque[list[int, str, dict[str, str], str, str, str, str, str, str]]':
    '''
    Parse the document :param:`doc` into a deque of items and return this deque.
    Every item is a list which contains the following nine subitems:

    0. a tag name, if the item is a starttag or a self-closing tag. If not, then
       just the empty string. A self-closing “<tag/>” would yield the following:
       ``['tag', {}, 'tag', '', '', '', '', '']``
    1. a dictionary of attribute keys and values, if the item is a starttag or a
       startendtag and has attributes. If not, then an empty dictionary.
    2. a tag name, if the item is an endtag or a self-closing tag (in the latter
       case, subitem 0 is also given). If not, just the empty string.
    3. a string, if the item is a passage of text between two non-text items. If
       not, then the empty string. The string is given unescaped (e.g. “>” for a
       “&gt;”).
    4. a string, if the item is a comment. If not, then the empty string.
    5. a string, if the item is a processing instruction. If not, then the empty
       string.
    6. a string, if the item is a declaration. If not, then the empty string.
    7. a string, if the item is some other bracket declaration. If not, then the
       empty string.
    8. an integer:

       - just zero for all non-tag items (text, declarations etc.).
       - greater than zero for startendtags, starttags and endtags. To wit:
       - A start- and an endtag which belong together have the same integer.
       - The numbering starts with ``1`` and is autoincremented.

    .. important::
        - The attribute names must be unique.
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute with a key but without a value will be reserialized as an
          attribute having the empty string as its value.
        - If :param:`balance` is ``True`` (the default value), :attr:`.items` is
          made balanced in the following way:

          - Missing endtags are inserted at the last possible place: at the very
            end of the input or immediately before an endtag whose starttag came
            **before** the starttag whose endtag is missing.
          - Endtags which do not match a starttag are omitted.

        - If :param:`xmlformed` is ``False`` (the default), :class:`Itemizer` is
          used to itemize :param:`doc`.

        - But if :param:`xmlformed` is ``True``, the expat XML parser is used to
          itemize :param:`doc` and **embedded DTDs are not fully supported**.

    You can iterate through a document e.g. as follows::

        new = collections.deque()
        for num, start, attrs, end, text, com, pi, decl, undecl in itemize(doc):
            ...
            new.append([num, start, attrs, end, text, com, pi, decl, undecl])

    Or, if you never need com, pi, decl, undecl::

        new = collections.deque()
        for num, start, attrs, end, text, *_ in itemize(doc):
            ...
            new.append([num, start, attrs, end, text, '', '', '', ''])

    The inverse function is ``serialize``.

    The use in general is shown by the following example:

    >>> doc = """
    ...         <tr><td><p align="left">
    ...           paragraph&apos;s text<br/>
    ...           <a></a>paragraph&#x2019;s text</tr>
    ...         </non-match>end."""
    >>> for num, start, attrs, end, text, com, pi, decl, undecl in itemize(doc):
    ...     if start:
    ...          print(num, num * '  ', start, 'on')
    ...     if end:
    ...          print(num, num * '  ', end, 'off')
    1    tr on
    2      td on
    3        p on
    4          br on
    4          br off
    5            a on
    5            a off
    3        p off
    2      td off
    1    tr off
    '''
    def handle_comment(comment: str):
        nonlocal dtd
        if dtd:
            dtd += f'\n\t<!-- {comment} -->'
        else:
            items.append([0, '', {}, '', '', comment, '', '', ''])

    def handle_cdata_end():
        nonlocal cdata
        nonlocal in_cdata
        in_cdata = False
        undecl = cdata
        items.append([0, '', {}, '', '', '', '', '', undecl])
        cdata = ''

    def handle_cdata_start():
        nonlocal in_cdata
        in_cdata = True

    def handle_data(data: str):
        nonlocal cdata
        nonlocal in_cdata
        if in_cdata:
            cdata += data
        else:
            if items and items[-1][3]:
                items[-1][3] += data
            else:
                items.append([0, '', {}, '', data, '', '', '', ''])

    def handle_dtd_end():
        nonlocal dtd
        nonlocal dtd_has_internal_subset
        if dtd_has_internal_subset:
            dtd += '\n]'
        items.append([0, '', {}, '', '', '', '', dtd, ''])
        dtd = ''

    def handle_dtd_start(
            doctypeName: str,
            systemId: 'None|str',
            publicId: 'None|str',
            has_internal_subset: bool,
            ):
        nonlocal dtd
        nonlocal dtd_has_internal_subset
        dtd_has_internal_subset = has_internal_subset
        dtd = f'DOCTYPE {doctypeName}'
        if publicId is None:
            if systemId is not None:
                dtd += f' SYSTEM "{systemId}"'
        else:
            dtd += f' PUBLIC "{publicId}" "{systemId or ""}"'
        if has_internal_subset:
            dtd += ' ['

    def handle_elementdecl(name: str, model: str):
        nonlocal dtd
        children = model[3]
        if children:
            value = ', '.join( child[2] for child in children )
        else:
            value = '#PCDATA'
        dtd += f'\n\t<!ELEMENT {name} ({value})>'

    def handle_entitydecl(
            entityName: str,
            is_parameter_entity: bool,
            value: 'None|str',
            base,
            systemId: 'None|str',
            publicId: 'None|str',
            notationName: 'None|str',
            ):
        nonlocal dtd
        if value:
            dtd += f'\n\t<!ENTITY {entityName} "{value}">'
        else:
            if publicId is None:
                if systemId is not None:
                    dtd += f' SYSTEM "{systemId}"'
            else:
                dtd += f' PUBLIC "{publicId}" "{systemId or ""}"'

    def handle_endtag(tag: str):
        if tag in open_tags:
            while open_tags:
                open_tag = open_tags.pop()
                items.append(
                        [nums.pop(), '', {}, open_tag, '', '', '', '', ''])
                if open_tag == tag:
                    break

    def handle_pi(first_word: str, rest: str):
        items.append([0, '', {}, '', '', '', f'{first_word} {rest}?', '', ''])

    def handle_starttag(tag: str, attrs: 'dict[str, str]'):
        nonlocal num
        open_tags.append(tag)
        num += 1
        nums.append(num)
        items.append([num, tag, attrs, '', '', '', '', '', ''])

    def handle_xmldecl(version: str, encoding: str, standalone: int):
        pi = f'xml version="{version}" encoding="{encoding}"'
        if standalone == 0:
            pi += ' standalone="no"'
        elif standalone == 1:
            pi += ' standalone="yes"'
        pi += '?'
        items.append([0, '', {}, '', '', '', pi, '', ''])

    if xmlformed:
        num = 0
        nums = deque()
        items = deque()
        open_tags = deque()
        in_cdata = False
        cdata = ''
        dtd = ''
        dtd_has_internal_subset = False
        parser = xml.parsers.expat.ParserCreate()
        parser.CharacterDataHandler = handle_data
        parser.CommentHandler = handle_comment
        parser.ElementDeclHandler = handle_elementdecl
        parser.EndCdataSectionHandler = handle_cdata_end
        parser.EndDoctypeDeclHandler = handle_dtd_end
        parser.EndElementHandler = handle_endtag
        parser.EntityDeclHandler = handle_entitydecl
        parser.ProcessingInstructionHandler = handle_pi
        parser.StartCdataSectionHandler = handle_cdata_start
        parser.StartDoctypeDeclHandler = handle_dtd_start
        parser.StartElementHandler = handle_starttag
        parser.XmlDeclHandler = handle_xmldecl
        parser.Parse(doc)
        return items
    else:
        parser = Itemizer(balance, cdatas = cdatas)
        parser.parse(doc)
        return parser.items

def mark(
        doc: str,
        xmlformed: bool = False,
        cdatas: 'set[str]' = HTML_CDATAS,
        ) -> str:
    '''
    In :param:`doc`, adorn start- and endtags with IDs in order to mark matching
    tags by prefixing the tag name with underscore plus ID plus hyphen; and turn
    any void element (with self-closing tag) into the synonymous sequence of one
    start tag and one end tag.

    .. note::
        By the way, the prefix with hyphen makes every element valid HTML5 – but
        this is hardly of much practical concern.

    If :param:`xmlformed` is ``False``, the following applies:

    .. important::
        - The attribute names must be unique.
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute with a key but without a value will be reserialized as an
          attribute having the empty string as its value.
        - The document is made balanced in the following way:

          - Missing endtags are inserted at the last possible place: at the very
            end of the input or immediately before an endtag whose starttag came
            **before** the starttag whose endtag is missing.
          - Endtags which do not (or: no longer) match a starttag are omitted.

    The doc gets serialized using stricter escaping and whitespace handling than
    the XML standard requires (see :func:`serialize` and the test code below).

    The marking can be used in order to refer to matching tags, e.g.:
    ``'(?s)<(_\\d+-).*?>.*?</\1.*?>'``.

    The marking can be removed with :func:`unmark`.

    >>> print(mark("""<div>…<div>…</div>…</div>"""))
    <_1-div>…<_2-div>…</_2-div>…</_1-div>
    >>> print(mark("""<div c b='"' A="x->x'"><DIV  />"""))
    <_1-div a="x-&gt;x'" b="&quot;" c=""><_2-div></_2-div></_1-div>
    '''
    old = itemize(doc, xmlformed = xmlformed, cdatas = cdatas)
    new = deque()
    while old:
        n, s, a, e, t, c, p, d, u = old.popleft()
        if s:
            s = f'_{n}-{s}'
        if e:
            e = f'_{n}-{e}'
        if s and e:
            # Resolve start-end-tags into start-tag plus end-tag:
            new.append((n, s, a, '', t, c, p, d, u))
            new.append((n, '', {}, e, t, c, p, d, u))
        else:
            new.append((n, s, a, e, t, c, p, d, u))
    return serialize(new)

def replace(
        doc: str,
        replace_element: '''ty.Callable[
            [str, dict[str, None|str], bool, Replacer],
            None|str
            ]''' = replace_element,
        replace_tag: '''ty.Callable[
            [str, dict[str, None|str], bool, Replacer],
            tuple[str, dict[str, None|str], bool]
            ]''' = replace_tag,
        replace_text:                   'ty.Callable[[str, Replacer], str]'
            = replace_text,
        replace_comment:                'ty.Callable[[str, Replacer], str]'
            = replace_comment,
        replace_bracket_declaration:    'ty.Callable[[str, Replacer], str]'
            = replace_bracket_declaration,
        replace_declaration:            'ty.Callable[[str, Replacer], str]'
            = replace_declaration,
        replace_processing_instruction: 'ty.Callable[[str, Replacer], str]'
            = replace_processing_instruction,
        consider_endtag: '''None|ty.Callable[
            [str, dict[str, None|str], str, dict[str, None|str], Replacer],
            None
            ]''' = consider_endtag,
        info: dict = {},
        sort: bool = True,
        cdatas: 'set[str]' = HTML_CDATAS,
        ) -> str:
    '''
    Make replacements in the document :param:`doc`. The replacements may affect:

    - entire elements.
    - tag names and attributes of elements.
    - text parts (outside tags).
    - bracket declarations.
    - comments.
    - doctype declarations.
    - processing instructions.

    That can be used e.g. for cleaning up an HTML document. For a removal of all
    tagging, i.e. for getting raw text, see :func:`untag`. It uses the very same
    :class:`Replacer` as this function here and works accordingly. For a removal
    of unnecessary nesting, see :func:`unnest`, which uses the same class, too.

    .. important::
        - The attribute names must be unique.
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute with a key but without a value will be reserialized as an
          attribute having the empty string as its value.
        - The document is made balanced in the following way:

          - Missing endtags are inserted at the last possible place: at the very
            end of the input or immediately before an endtag whose starttag came
            **before** the starttag whose endtag is missing.
          - Endtags which do not (or: no longer) match a starttag are omitted.

    The resulting document can be parsed with an XML parser – if you ensure that
    it has a root element, e.g. by enclosing it into “<root>” and “</root>”.

    If :param:`sort` is ``True`` (the default), the attributes are serialized in
    alphabetical order.

    The serialization is done via :func:`serialize` – see its docstring for more
    information.

    If the callable parameters (whose names start with “replace_”) are left with
    their default value:

    - all comments, declarations, processing instructions are deleted.
    - all tags are preserved.
    - the elements listed in :const:`HTML_VOIDS` (and only those) are written as
      self-closing elements, e.g. as “<br/>” instead of “<br>” or “<br></br>”.

    In order to make customized replacements you pass your own callables – to do
    so, you can just copy the functions that are passed here as defaults, insert
    them into your program, adapt them and pass these adapted ones instead.

    Tips for the adaptation:

    - :param:`replace_element` is called before :param:`replace_tag`.
    - About :param:`consider_endtag`, see the next item:
    - :param:`info` will be available in each callable as ``parser.info`` – i.e.
      as an attribute of the last argument (``parser``) of every callable. It is
      useful there as mutable memory for something on which later steps must act
      – e.g. ``if parser.info.get('in_table', 0)``, then do what must be done in
      tables but not elsewhere. In this example, the value of the “in_table” key
      is a number you incremented on every table-starttag and decrement on every
      table-endtag; the former would be done in :param:`replace_tag`, the latter
      in :param:`consider_endtag`. – The initial value ``0`` of the key could be
      set by passing ``{'in_table': 0}`` as the value of :param:`info` or by the
      use of ``setdefault`` in :param:`replace_tag`. In other cases, it might by
      useful to pass a defaultdict as value of :param:`info`.
    - In most cases, the most involved of the callables is :param:`replace_tag`.
      The more complex its task is, the more recommendable it may be …

      - to start it with ``newtag = ''`` and ``newattrs = {}``,
      - to assign to these new variables new values, derived from the old values
        of ``tag`` or of ``attrs`` or from ``startend`` or from ``parser.info``.
      - to return the new variables.

    - It is sometimes convenient to make the callables closures of your function
      which passes them to :func:`replace`.

    Example and testcode:

    >>> import xmlhtml
    >>>
    >>> def replace_tag(tag, attrs, startend, parser):
    ...     for key in ('style', 'align'):
    ...         attrs.pop(key, '')
    ...     tag = tag if (attrs or tag != 'span') else ''
    ...     startend = tag in xmlhtml.HTML_VOIDS
    ...     return tag, attrs, startend

    >>> def replace_text(text, parser):
    ...     return text.strip()
    >>>
    >>> doc = """
    ...         <tr id="xy">
    ...             <td>
    ...                 <p align="left">
    ...                     <span style="font-family: UglySans">
    ...                         some text <br>
    ...                         more text <ins/>
    ...                     </span>
    ...             </td>
    ...     """
    >>>
    >>> doc = xmlhtml.replace(
    ...         doc,
    ...         replace_tag = replace_tag,
    ...         replace_text = replace_text,
    ...         )
    >>> print(doc)
    <tr id="xy"><td><p>some text<br/>more text<ins></ins></p></td></tr>
    '''
    replacer = Replacer(
            replace_element                = replace_element,
            replace_tag                    = replace_tag,
            replace_text                   = replace_text,
            replace_comment                = replace_comment,
            replace_bracket_declaration    = replace_bracket_declaration,
            replace_declaration            = replace_declaration,
            replace_processing_instruction = replace_processing_instruction,
            consider_endtag                = consider_endtag,
            info                           = info,
            sort                           = sort,
            cdatas                         = cdatas,
            )
    replacer.parse(doc)
    return ''.join(replacer.results)

def serialize(
        items: 'deque[list[int, str, dict[str, str], str, str, str, str, str, str]]',
        sort: bool = True,
        ) -> str:
    '''
    Serialize :param:`items` into a string with markup and escaping as follows:

    - Certain characters are escaped in all text nodes and attribute values:

      - “&” as “&amp;”
      - “<” as “&lt;”
      - “>” as “&gt;”
      - “"” as “&quot;”

    - All other characters are given unescaped.
    - If :param:`sort` equals ``True`` (the default), the attributes are written
      in alphabetical order.
    - All attribute values are delimited by “"”.
    - The value of an otherwise valueless attribute is the empty string.
    - Within tags and outside attribute values, whitespace is not written except
      from one normal space (Unicode 0x20) before any attribute, i.e.:

      - “<tag>”
      - “<tag/>”
      - “</tag>”
      - “<tag key="value" key2="value">”
      - “<tag key="value" key2="value"/>”

    .. warning::
        - The XML declaration and XML processing instructions end with “?>”, but
          the HTML processing instructions end with “>”. Therefore the end added
          here is just “>”; and if the target is XML, then the given item itself
          must already have ended with “?”.
        - Some special parts of an XML or HTML document do not allow an escaping
          unfortunately – any item to become one of those parts must not contain
          characters making the right border of the part ambiguous:

          - A comment must not contain “-->”.
            If it does, an :class:`EscapeError` is raised.
          - A “<![...]>” part (i.e. CDATA) must not contain “]>”.
            If it does, an :class:`EscapeError` is raised.
          - A “<?...?>” part (i.e. an XML declaration or processing instruction)
            must not contain “?>”.
            If it does, an :class:`EscapeError` is raised.
          - A `<?...> part (i.e. an HTML processing instruction) mustnʼt contain
            “>”.
            If it does, an :class:`EscapeError` is raised.
          - A “<!...>” part (i.e. a declaration) mustnʼt contain any “>” that is
            not balanced by a “<” and not part of an attribute value.
            If it does, an :class:`EscapeError` is raised.

    :param:`items` is a deque of lists like the one produced by :func:`itemize`,
    which is the inverse function of this one.
    '''
    return ''.join(serialize_items(items, sort = sort))

def serialize_attrs(attrs: 'dict[str, str]', sort = True) -> str:
    if sort:
        return ''.join(
                f''' {key}="{escape(value or '')}"'''
                for key, value in sorted(attrs.items()) ) if attrs else ''
    else:
        return ''.join(
                f''' {key}="{escape(attrs[key] or '')}"'''
                for key in attrs ) if attrs else ''

def serialize_elem(
        element: 'None|ET.Element',
        method: str = 'htmlxml',
        tailless: bool = True,
        inner: bool = False,
        short_empty_elements: bool = False,
        html_voids_re: 're.Pattern[str]' =
            re.compile(f"></({'|'.join(HTML_VOIDS)})\\s*>"),
        inner_re: 're.Pattern[str]' =
            re.compile(r'(?s)(?:^<[^>]*/>$|^<[^>]*>(.*)</[^>]*>$)'),
        ) -> str:
    '''
    Convert :param:`element` to a string representation. If it is ``None``, then
    the empty string is returned.

    :param method: is the same as in :meth:`ET.tostring`, but with an additional
        option:

        - The value ``'text'`` leads to a string which consists of all texts and
          tails of the element and its descendants, unescaped and without tags.
        - The other allowed values lead to a string with escaping and with tags.
        - The additional option is ``'htmlxml'``:

          - The resulting string conforms with the XML syntax.
          - A sequence matching :param:`html_voids_re` is replaced with ``'/>'``
            to make a self-closing tag.
          - Any other self-closing tag is replaced with a non-self-closing tag.

    :param tailless: If ``True`` (which is the default), the tail of the element
        is left out! The tail is any text behind the closing tag of this element
        up to the next opening or closing tag of any element.
    :param inner: If ``True``, the elementʼs own tags and its tail are left out.
    :param short_empty_elements: is the same as in :meth:`ET.tostring`, but will
        be used only if :param:`method` is not ``'htmlxml'``.
    :param voids_re: matching the strings which are to be replaced with ``'/>'``
        in order to make self-closing tags for the HTML void elements. – This is
        done only if :param:`method` is ``'htmlxml'``! The default value matches
        the elements named in :const:`HTML_VOIDS`.
    :param inner_re: used if :param:`inner` is ``True``; its first group matches
        the sequence between the rightmost and the leftmost tag.
    '''
    if element is None:
        return ''
    if inner:
        tailless = True
    if tailless:
        element = deepcopy(element)
        element.tail = None
    if method == 'htmlxml':
        method = 'xml'
        short_empty_elements = False
        ensure_voids = True
    else:
        ensure_voids = False
    text = ET.tostring(element,
            encoding = 'unicode',
            method = method,
            short_empty_elements = short_empty_elements)
    if method != 'text':
        if inner:
            text = inner_re.sub(r'\g<1>', text)
        if ensure_voids:
            text = html_voids_re.sub(r'/>', text)
    return text.replace(' />', '/>')

def serialize_items(
        items: 'deque[list[int, str, dict[str, str], str, str, str, str, str, str]]',
        sort: bool = True,
        ) -> 'deque[str]':
    '''
    Serialize each item of :param:`items` into a string with markup and escaping
    as necessary (and possible). For details see :func:`serialize`.
    '''
    serialized_items = deque()
    while items:
        _, start, attrs, end, text, com, pi, decl, undecl = items.popleft()
        if start:
            slash = "/" if end else ""
            attrstr = serialize_attrs(attrs, sort = sort)
            item = f'<{start}{attrstr}{slash}>'
        elif end:
            item = f'</{end}>'
        elif text:
            item = escape(text)
        elif com:
            if '-->' in com:
                raise CommentEscapeError(com)
            item = f'<!--{com}-->'
        elif pi:
            if pi[-1] == '?' and '?>' in pi:
                raise DeclarationInstructionEscapeError(pi)
            elif '>' in pi:
                raise ProcessingInstructionEscapeError(pi)
            item = f'<?{pi}>'
        elif decl:
            if '>' in decl:
                temp = re.sub(r'''("[^"]*"|'[^']*')''', '', decl)
                if temp.count('<') != temp.count('>'):
                    raise DeclarationEscapeError(decl)
            item = f'<!{decl}>'
        elif undecl:
            if ']>' in undecl:
                raise UnknownDeclarationEscapeError(undecl)
            item = f'<![{undecl}]>'
        else:
            item = ''
        serialized_items.append(item)
    return serialized_items

def serialize_styles(styles: 'dict[str, str]', sort: bool = True) -> str:
    '''
    Convert :param:`styles` (a mapping from style labels to style values) into a
    CSS-string representation of the styles.

    The inverse function is :func:`get_styles`.

    >>> serialize_styles({'display': 'block', 'color': 'red'})
    'color: red; display: block'
    >>> serialize_styles({})
    ''
    '''
    styles = styles.items()
    return '; '.join(
            ': '.join(item) for item in (sorted(styles) if sort else styles) )

def serialize_tag(
        tag_name: str,
        attrs: 'dict[str, None|str]' = {},
        end: bool = False,
        startend: bool = False,
        sort: bool = False,
        ) -> str:
    '''
    Convert the given :param:`tag_name` and :param:`attrs` to a string that is a
    tag in XML and HTML. :param:`attrs` may be empty.

    - If :param:`tag_name` is empty, the empty string is returned.
    - If :param:`end` is :bool:`True`, a closing tag is returned,
    - if :param:`startend` is :bool:`True`, a self-closing tag is returned,
    - else an opening tag.
    - If :param:`sort` is ``True`` (the default), the attributes are returned in
      alphabetical order.

    Tags and escaping is handled as in :func:`serialize`; see its docstring.
    '''
    if tag_name:
        if end:
            return f'</{tag_name}>'
        if sort:
            attrs = sorted(attrs.items())
        else:
            attrs = attrs.items()
        attrs = ''.join( f' {k}="{escape(str(v or ""))}"' for k, v in attrs )
        slash = "/" if startend else ""
        return f'<{tag_name}{attrs}{slash}>'
    else:
        return ''

def serialize_tree(
        tree: 'deque[str|ty.Container[str]|list[str, dict[str, str], deque]]',
        all_void: bool = True,
        voids: 'set[str]' = HTML_VOIDS,
        check: bool = False,
        ) -> str:
    '''
    Serialize :param:`tree` to an XML or HTML string. The tree has the following
    structure *S*: A list or deque of items of the following types:

    - ``str``: normal text content, will be escaped.
    - (rarely) A container of one string: This string will be inserted as-is and
      should be used for any declaration, instruction, comment. All escaping and
      delimiters must already have been done!
    - A list or tuple of three items, representing an element:

      - A string: the tag name.
      - A dictionary (maybe empty): attributes.
      - A deque: the inner content: has the structure *S* again.

    .. important::
        Regarding the delimiters:

        - In XML, processing instructions and documenttype declarations end with
          “?>”
        - In HTML, they end just with “>”.

    :param all_void: If ``True``, all empty elements are written as self-closing
        like :xml:`<tag/>`. Else, only the elements in :param:`voids` are.
    :param voids: See :param:`all_void`.
    :param check: If ``True``, check whether the result is well-formed XML. That
        may not be the case e.g. if:

        - :param:`tree` did not contain exactly one root element.
        - Tag names or attribute names did contain invalid characters.
        - A string of the already escaped strings was not well-formed, e.g. if a
          comment did not end with :xml:`-->`.

    >>> import xmlhtml
    >>> tree = [
    ...         ('root', {}, [
    ...             'Siehe diesen ',
    ...             ('a', {'href': 'http://example.org'}, [
    ...                 'Beispiellink'
    ...                 ]),
    ...             '!',
    ...             ])
    ...        ]
    >>> print(serialize_tree(tree))
    <root>Siehe diesen <a href="http://example.org">Beispiellink</a>!</root>
    '''
    doc = deque()
    while tree:
        item = tree.pop()
        if isinstance(item, str):
            doc.appendleft(escape(item))
        elif len(item) == 1:
            doc.appendleft(item[0])
        else:
            tag, attrs, inner = item
            start = '<' + tag + ''.join(
                    ' ' + k + '="' + escape(v) + '"' for k, v in attrs.items() )
            if inner:
                doc.extendleft(('>', tag, '</'))
                tree.append((start + '>',))
                tree.extend(inner)
            else:
                if all_void or tag in voids:
                    doc.extendleft(('/>', start))
                else:
                    doc.extendleft(('>', tag, '></', start))
    doc = ''.join(doc)
    if check:
        ET.XML(doc)
    return doc

def strip_namespaces(element: ET.Element) -> ET.Element:
    for subelement in element.iter():
        # NB: The first subelement is the element itself.
        subelement.tag = subelement.tag.split('}')[-1]
    return element # not necessary, but could be useful.

def table_to_doc(
        table: 'ty.Iterable[ty.Iterable[None|float|int|str]]',
        head: 'ty.Iterable[None|float|int|str]' = [],
        width: str = 'auto',
        widths: 'dict[int, str]' = {},
        style: 'dict[str, dict[str, str]]' = STYLE,
        title: str = 'Data',
        convert: 'ty.Callable[[ty.Any], str]' = str,
        ) -> str:
    '''
    Convert :param:`table` to an HTML document ready to be opened in a browser.

    :param head: made into th elements and put into an thead element.
    :param width: is added as table width to :param:`style`. Set this to another
        CSS-width value than “auto” if the table-layout should be “fixed”, which
        may be useful in combination with the following parameter:
    :param widths: maps integers to CSS width values. An integer n means the nth
        column, counted from 1 (not 0) as the CSS selector nth-child counts.
    :param style: serialized to CSS, then inserted into the style element of the
        head of the document.
    :param thead: inserted into the table element, can be used to add headings.
    :param title: inserted into the title element of the head of the document.
    :param convert: a function that will be applied to every value, which may be
        useful for converting the value into a string or to escape it if this is
        not already done.
    '''
    style.setdefault('#table', {})['width'] = width
    if widths:
        for nth, width in widths.items():
            style.setdefault(
                    f'#table :is(td, th):nth-child({nth})', {})['width'] = width
    css = '\n\t\t'.join(
            f'{sel} {{{serialize_styles(block)}}}' for sel, block in style.items() )
    thead = '\n<thead><tr><th>{}</th></tr></thead>'.format(
            '</th><th>'.join(map(convert, head))) if head else ''
    data = '</td></tr>\n<tr><td>'.join(
            '</td><td>'.join(map(convert, row)) for row in table )
    return f'''<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<style>
		{css}
	</style>
	<title>{title}</title>
</head>
<body>
<table id="table">{thead}
<tbody>
<tr><td>{data}</td></tr>
</tbody>
</table>
</body>
</html>
'''

def table_to_tree(
        table: 'ty.Iterable[ty.Iterable[str]]',
        dictify: 'ty.Callable' = dict,
        last_is_value: bool = True,
        ) -> 'dict[str, str|dict]':
    '''
    Convert :param:`table` to a tree-like dictionary of dictionaries.

    :param dictify: used to create the dictionaries.
    :param last_is_value: speficies the kind of tree: If ``True``, a branch will
        end with ``{prelast_column: last_column}`` – if ``False``, a branch will
        end with ``{last_column: {}}``.

    >>> import xml.etree.ElementTree as ET
    >>> from pprint import pprint
    >>> import xmlhtml
    >>> table = deque([
    ...         deque(['a....', 'e....', 'f....', 'g....', 'h....']),
    ...         deque(['a....', 'e....', 'f....', 'i....', 'j....']),
    ...         deque(['a....', 'e....', 'k....', 'l....']),
    ...         deque(['a....', 'b....', 'c....', 'd....']),
    ...         ])
    >>> pprint(xmlhtml.table_to_tree(table))
    {'a....': {'b....': {'c....': 'd....'},
               'e....': {'f....': {'g....': 'h....', 'i....': 'j....'},
                         'k....': 'l....'}}}
    '''
    tree = dictify()
    if table:
        if last_is_value:
            for row in table:
                subtree = tree
                while len(row) > 2:
                    subtree = subtree.setdefault(row.popleft(), dictify())
                if len(row) > 1:
                    subtree[row[-2]] = row[-1]
                elif len(row) == 1:
                    subtree[row[-1]] = ''
        else:
            for row in table:
                subtree = tree
                for cell in row:
                    subtree = subtree.setdefault(cell, dictify())
    return tree

def tree_to_table(tree: 'dict[str, str|dict]') -> 'deque[deque[str]]':
    '''
    Convert :param:`tree` to a table-like deque of deques.

    >>> import xmlhtml
    >>> tree = {'a....': {'b....': {'c....': 'd....'},
    ...                   'e....': {'f....': {'g....': 'h....',
    ...                                       'i....': 'j....'},
    ...                             'k....': 'l....'}}}
    >>> for row in xmlhtml.tree_to_table(tree):
    ...     print(row)
    deque(['a....', 'b....', 'c....', 'd....'])
    deque(['a....', 'e....', 'f....', 'g....', 'h....'])
    deque(['a....', 'e....', 'f....', 'i....', 'j....'])
    deque(['a....', 'e....', 'k....', 'l....'])
    '''
    table = deque()
    row = deque()
    trees = deque()
    level = 0
    trees.appendleft((level, tree))
    while trees:
        level = trees[0][0]
        key, value = trees[0][1].popitem()
        if not trees[0][1]:
            trees.popleft()
        row.append(key)
        if value and isinstance(value, dict):
            trees.appendleft((level + 1, value))
        else:
            if value:
                row.append(value)
            table.appendleft(row)
            row = deque()
            if trees:
                for i in range(trees[0][0]):
                    row.append(table[0][i])
    return table

def unescape(text: str) -> str:
    '''
    Revert an escaping done with :func:`escape`.
    '''
    return text.replace('&quot;', '"'
              ).replace('&gt;', '>'
              ).replace('&lt;', '<'
              ).replace('&amp;', '&')

def unmark(
        doc: str,
        make_html_voids_self_closing: bool = True,
        mark_re: 're.Pattern[str]' = re.compile(r'<(/?)_\d*-'),
        html_voids_re: 're.Pattern[str]' =
            re.compile(rf"<({'|'.join(HTML_VOIDS)})(?= |>)([^>]*)></\1>"),
        ) -> str:
    '''
    Unmark the prefixed tag names as they are produced by :func:`mark`.

    :param make_html_voids_self_closing: If ``True`` (the default), make matches
        of :param:`html_voids_re` self-closing.
    '''
    doc = mark_re.sub(r'<\g<1>', doc)
    if make_html_voids_self_closing:
        doc = html_voids_re.sub(r'<\g<1>\g<2>/>', doc)
    return doc

def unnest(
        doc: str,
        test: '''ty.Callable[
            [
                str,
                dict[str, None|str],
                bool,
                dict[str, deque[str|dict[str, None|str]]]
            ],
            bool]''' = lambda tag, attrs, startend, info: tag in info['tags'],
        sort: bool = True,
        cdatas: 'set[str]' = HTML_CDATAS,
        ) -> str:
    '''
    Delete any tag – including its endtag, if the tag is not self-closing – from
    :param:`doc` if the tag, its attributes, its startend status (``True`` if an
    elementʼs tag is self-closing) and information about all parent tags and the
    belonging attributes return ``True`` if passed to :param:`test`.

    The information about all parent tags and the belonging attributes is passed
    to test in the parameter ``info``, a dictionary like::

        {
            'tags':  deque(['html',        'p', 'a']),
            'attrs': deque([{'lang': 'de'}, {}, {'href': 'http://example.org'}])
        }

    For :param:`sort` and :param:`cdatas`, see the docstring of :func:`replace`.

    .. important::
        - The attribute names must be unique.
        - All tags and attribute names are inevitably converted to lower case!
        - An attribute with a key but without a value will be reserialized as an
          attribute having the empty string as its value.
        - The document is made balanced in the following way:

          - Missing endtags are inserted at the last possible place: at the very
            end of the input or immediately before an endtag whose starttag came
            **before** the starttag whose endtag is missing.
          - Endtags which do not (or: no longer) match a starttag are omitted.

    >>> import xmlhtml
    >>>
    >>> print(xmlhtml.unnest('<i><b><i>…</i></b></i>'))
    <i><b>…</b></i>
    >>> print(xmlhtml.unnest(
    ...         '<li>…<ol><li>…<i><b><i>…</i></b></i>…</li></ol>…</li>',
    ...         test = lambda t, a, s, i:
    ...             t in i['tags'] and
    ...             t not in {'li', 'ol', 'table', 'td', 'th', 'tr', 'ul'},
    ...         ))
    <li>…<ol><li>…<i><b>…</b></i>…</li></ol>…</li>
    '''
    def replace_tag(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'Replacer',
            ) -> 'tuple[str, dict[str, None|str], bool]':
        if test(tag, attrs, startend, parser.info):
            tag = ''
        elif not startend:
            parser.info['tags'].append(tag)
            parser.info['attrs'].append(attrs)
        return tag, attrs, startend

    def consider_endtag(
            old_tag: str,
            old_attrs: 'dict[str, None|str]',
            new_tag: str,
            new_attrs: 'dict[str, None|str]',
            parser: 'Replacer',
            ) -> None:
        if new_tag:
            parser.info['tags'].pop()
            parser.info['attrs'].pop()

    replacer = Replacer(
            replace_tag                    = replace_tag,
            replace_bracket_declaration    = lambda decl, parser: decl,
            replace_comment                = lambda comment, parser: comment,
            replace_declaration            = lambda decl, parser: decl,
            replace_processing_instruction = lambda pi, parser: pi,
            consider_endtag                = consider_endtag,
            info                           = {'tags': deque(), 'attrs': deque()},
            sort                           = sort,
            cdatas                         = cdatas,
            )
    replacer.parse(doc)
    return ''.join(replacer.results)

def untag(
        doc: str,
        replace_element: '''ty.Callable[
            [str, dict[str, None|str], bool, Replacer],
            None|str
            ]''' = replace_offtext_element,
        replace_tag: '''ty.Callable[
            [str, dict[str, None|str], bool, Replacer],
            tuple[str, dict[str, None|str], bool]
            ]''' = lambda tag, attrs, startend, parser:
                ('div' if tag in HTML_LINEBREAKERS else '', {}, False),
        replace_text:                   'ty.Callable[[str, Replacer], str]'
            = lambda text, parser: re.sub(r'[\n\r\t]', ' ', text),
        replace_comment:                'ty.Callable[[str, Replacer], str]'
            = replace_comment,
        replace_bracket_declaration:    'ty.Callable[[str, Replacer], str]'
            = replace_bracket_declaration,
        replace_declaration:            'ty.Callable[[str, Replacer], str]'
            = replace_declaration,
        replace_processing_instruction: 'ty.Callable[[str, Replacer], str]'
            = replace_processing_instruction,
        consider_endtag: '''None|ty.Callable[
            [str, dict[str, None|str], str, dict[str, None|str], Replacer],
            None
            ]''' = consider_endtag,
        info: dict = {},
        sort: bool = True,
        cdatas: 'set[str]' = HTML_CDATAS,
        postreplacements: 'tuple[re.Pattern[str], str]' = (
            (re.compile(r'<[^>]*>'), '\n'),
            (re.compile(r'  +'), ' '),
            (re.compile(r' ?(\n ?)+'), '\n'),
            ),
        ) -> str:
    '''
    - Convert :param:`doc` as if :func:`replace` is applied with all the further
      arguments except :param:`postreplacements`. See :func:`replace` for more.
    - For each pair in :param:`postreplacements`, search for its first item – it
      is a compiled regular expression – and substitute the second one, i.e. the
      belonging replace pattern. **All tagging must be removed now.**
    - **Unescape the document.**
    - Strip leading and trailing newlines or normal spaces.
    - Return the document.
    '''
    replacer = Replacer(
            replace_element                = replace_element,
            replace_tag                    = replace_tag,
            replace_text                   = replace_text,
            replace_comment                = replace_comment,
            replace_bracket_declaration    = replace_bracket_declaration,
            replace_declaration            = replace_declaration,
            replace_processing_instruction = replace_processing_instruction,
            consider_endtag                = consider_endtag,
            info                           = info,
            sort                           = sort,
            cdatas                         = cdatas,
            )
    replacer.parse(doc)
    doc = ''.join(replacer.results)
    for old_re, new in postreplacements:
        doc = old_re.sub(new, doc)
    return unescape(doc).strip('\n ')

if __name__ == '__main__':
    import doctest
    doctest.testmod()
