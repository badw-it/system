# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
'''
# Make a new or compare given checksums with corresponding files. The checksums
# are hexdigests of sha256 hashes.
# This program can be called with an argument that is the path to a
# configuration file. If so, that file must be either ascii or UTF-8 encoded
# and must have the following structure (i.e. the structure of an INI file)::

    [Parameters]
    # If you want to make a new list of checksums, insert after ``parentpath:``
    # the path to the parent folder containing the very files of which the
    # checksums are to be calculated; ignore the further parameters thereafter.
    # Else, if you want to compare a given list of checksums with the
    # respective files that should have this checksum, write nothing after
    # ``parentpath:`` and specify instead the further parameters thereafter.

    parentpath=.
    # E:/RGASPI_FOND458_OPIS_9

    # The path to a file that contains in each line only a checksum,
    # whitespace and the path to the file that should have this checksum:

    old_hexdigests_path=../hexdigests.txt

    # The results of the check are summarized in a text either sent by email
    # or printed to ``sys.stdout``, if the email cannot be sent with the
    # parameters that may be specified beneath.

    # An email address from which the message about the check is to be sent:

    sender=email@address

    # One or more email addresses to which the message about the check is
    # to be sent; several addresses must be separated by whitespace:

    recipients=email@address1 email@adress2

    # SMTP host from which the email is served:

    smtp_host=ip.address.of.the.server

    # SMTP port from which this SMTP host serves the email:

    smtp_port=25

# Else, if the program is called without a further argument, this docstring
# is used instead (hence the `#` at the beginning of each line). So this
# docstring can also be altered instead of an external file.
'''

import configparser
import hashlib
import os
import smtplib
import sys
import traceback

from email.mime.text import MIMEText

import __init__
import fs

def get_config(config_text: str) -> 'tuple[str, str, str, list[str], str, int]':
    parser = configparser.ConfigParser()
    parser.read_string(config_text)
    return (
            parser['Parameters']['parentpath'],
            parser['Parameters']['old_hexdigests_path'],
            parser['Parameters']['sender'],
            parser['Parameters']['recipients'].split(),
            parser['Parameters']['smtp_host'],
            int(parser['Parameters']['smtp_port']),
            )

def get_hexdigest(filepath: str) -> str:
    with open(filepath, 'rb') as file:
        return hashlib.sha256(file.read()).hexdigest()

def save_checksums(parentpath: str) -> 'tuple[str, int]':
    counted = 0
    results_path = os.path.join(os.path.split(parentpath)[0], 'hexdigests.txt')
    results_path = fs.get_new_path(results_path)
    with open(results_path, 'w', encoding = 'utf-8') as outfile:
        for inpath in file.io.get_paths(parentpath):
            outfile.write(f'{get_hexdigest(inpath)} {inpath}{os.linesep}')
            counted += 1
            if counted % 10 == 0:
                print(counted)
    return results_path, counted

def save_deviations(old_hexdigests_path: str) -> 'tuple[str, int]':
    counted = 0
    results_path = os.path.join(os.path.split(old_hexdigests_path)[0],
            'deviations.txt')
    results_path = fs.get_new_path(results_path)
    with open(old_hexdigests_path, 'r', encoding = 'utf-8') as infile:
        with open(results_path, 'w', encoding = 'utf-8') as outfile:
            for line in infile:
                old_hexdigest, path = line.split(sep = None)[:2]
                if not old_hexdigest == get_hexdigest(path):
                    counted += 1
                    outfile.write(path + os.linesep)
    if counted == 0:
        os.remove(results_path)
        results_path = ''
    return results_path, counted

def send_message(
        config_path: str,
        old_hexdigests_path: str,
        results_path: str,
        counted: int,
        error: str,
        sender: str,
        recipients: 'list[str]',
        smtp_host: str = 'localhost',
        smtp_port: int = 25,
        ):
    text = '''
    Comparison of checksums:
        Hash method: sha256
        Format of checksums: hexadecimal
        File with configuration parameters: {}
        File with old checksums and with paths to the files to be checked: {}
        '''.format(
            config_path if config_path else
                '[None, the docstring of {} was used]'.format(__file__),
            old_hexdigests_path)
    if error:
        text += f'Error:\n\n{error}'
    else:
        text += f'''Errors: 0
        Deviations: {counted}'''
        if counted:
            text += f'''
        Paths of deviating files saved in: {results_path}'''
    try:
        message = MIMEText(text, _charset = 'utf-8')
        message['Subject'] = 'Checksums: {}'.format(
                'An error occured!' if error else
                '{} deviations'.format(counted))
        message['From'] = sender
        message['To'] = recipients
        with smtplib.SMTP(host = smtp_host, port = smtp_port) as server:
            server.send_message(message)
    except OSError:
        print(text)

def main(config_path: str):
    if config_path:
        config_path = os.path.abspath(config_path)
        with open(config_path, encoding = 'utf-8') as file:
            config_text = file.read()
    else:
        config_text = __doc__
    parentpath, old_hexdigests_path, sender, recipients, smtp_host, smtp_port =\
            get_config(config_text)
    if parentpath:
        print('I make a new list of checksums now. That will take some time.')
        results_path, counted = save_checksums(parentpath)
        print(f'{counted} checksums have been saved in:\n{results_path}')
    else:
        print('I compare a given list of checksums with the respective files. '\
              'That will take some time.')
        try:
            results_path, counted = save_deviations(old_hexdigests_path)
            error = ''
        except:
            results_path = ''
            counted = 0
            error = 'An error occured during the comparison:{0}{0}{1}'.format(
                    os.linesep,
                    traceback.format_exc())
        send_message(
                config_path, old_hexdigests_path, results_path, counted,
                error, sender, recipients, smtp_host, smtp_port)

if __name__ == '__main__':
    config_path = sys.argv[1] if len(sys.argv) > 1 else ''
    main(config_path)
