% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a fulltext search form.
% # In the example edition: http://localhost:8080/portal/search_fulltext.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="card" role="search">
	<h1>{{db.glosses['search_fulltext'][lang_id]}}</h1>
	<form action="search_results" method="get">
		<p><input aria-label="{{db.glosses['search_term'][lang_id]}}" autofocus="" name="term" pattern=".*\w.*" required="" title="{{db.glosses['at_least_one_letter'][lang_id]}}" type="search"/></p>
		<p><button type="submit">{{db.glosses['search'][lang_id]}}&#8239;►</button></p>
		<fieldset>
			<legend>{{db.glosses['settings'][lang_id]}}</legend>
			<label><input checked="" name="no_case" type="checkbox"/> {{db.glosses['no_case'][lang_id]}}</label> <br/>
			<label><input name="single_words" type="checkbox"/> {{db.glosses['whole_words'][lang_id]}} {{db.glosses['no_order'][lang_id]}}</label> <br/>
			<label><input name="regex" type="checkbox"/> {{db.glosses['regex'][lang_id]}} (<a href="/{{lang_id}}/{{db.site_id}}/user_guide#regex">{{db.glosses['click_for_more_info'][lang_id]}}</a>)</label>
		</fieldset>
		<fieldset>
			<legend>{{db.glosses['domain'][lang_id]}}</legend>
			<label><input checked="" name="status_id" type="checkbox" value="txtsrc"/> {{db.glosses['source'][lang_id]}}</label> <br/>
			<label><input checked="" name="status_id" type="checkbox" value="txt"/> {{db.glosses['introduction'][lang_id]}} / {{db.glosses['commentary'][lang_id]}}</label>
		</fieldset>
	  % if hasattr(db, 'site_ids'):
		<fieldset>
			<legend>{{db.glosses['publication'][lang_id]}}</legend>
		  % for db_site_id in db.site_ids:
		    % if not db_site_id == db.site_id:
			<label><input{{!' checked=""' if db_site_id == request.query.base_site_id else ''}} name="site_id" type="checkbox" value="{{db_site_id}}"/> {{!db.glosses[db_site_id][db.lang_id[db_site_id]].replace('\n', ' ')}}</label> <br/>
			% end
		  % end
		</fieldset>
	  % end
	</form>
</article>
</main>
