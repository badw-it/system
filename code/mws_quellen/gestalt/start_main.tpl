% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a portal overview.
% # In the example edition: http://localhost:8080 (showing only one edition in
% # the row of editions).
% from re import compile
% regex = compile(b'<a[ /][^>]*?>')
% lang_id = kwargs.get('lang_id', '')
<main class="flexcol txt">
% for target_site_id in db.site_ids:
	% text, tpl = db.get_text_and_template_name(target_site_id, db.page_id[target_site_id])
	% if text:
		% doc = db.ET.XML(text.decode())
		% article = doc.find(f'.//*[@lang="{lang_id}"]//article')
		% if not article:
			% article = doc.find('.//article')
		% end
		% text = db.xmlhtml.serialize_elem(article).encode() if article else b''
		% text = regex.sub(b'', text)
		% text = text.replace(b'</a>', b'')
		% text = text.replace(b'style="background: url(', b'style="margin: auto; background: url(/~/' + target_site_id.encode() + b'/')
		% text = text.replace(b' property="', b' data-property="')
		% text = text.strip()
		% if text:
<a class="key wide" style="font-size: smaller; max-width: 800px" href="/{{lang_id}}/{{target_site_id}}/{{db.page_id[target_site_id]}}">{{!text}}</a>
		% end
	% end
% end
</main>
