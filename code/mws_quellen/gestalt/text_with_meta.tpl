% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for HTML-XML text with a meta data card taken from a data source
% # specified in the text.
% # In the example edition: http://localhost:8080/de/example/00001.
% site_id = kwargs.get('site_id', '')
% text = kwargs['text']
<main>
% mapping = db.get_mapping(text, site_id)
% if mapping:
<article class="card mapping">
<table>
  % for key, value in mapping:
	<tr>{{!key.replace(' property="', ' data-property="')}}{{!value.replace(' property="', ' data-property="').replace(' class="rest"', '')}}</tr>
  % end
</table>
</article>
% end
{{!text}}
</main>
<script>
$('.parentlight').hover(
	function(){$(this).parent().addClass('mark')},
	function(){$(this).parent().removeClass('mark')}
);
$('.prevlight').hover(
	function(){$(this).prev().addClass('mark')},
	function(){$(this).prev().removeClass('mark')}
);
</script>