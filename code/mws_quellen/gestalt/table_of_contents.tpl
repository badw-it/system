% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a table of contents.
% # In the example edition: http://localhost:8080/de/example/table_of_contents.
% lang_id = kwargs.get('lang_id', '')
% site_id = kwargs.get('site_id', '')
<main>
<article class="sheet">
	<h1>{{db.glosses['table_of_contents'][lang_id]}}</h1>
	<nav>
	% for item_lang_id, item_page_id, item_element_link, item_text, indent in db.get_toc_items(site_id):
		<p{{!'' if indent else ' class="toc_item_main"'}} style="padding-left: {{indent * 4}}%">
			<a href="/{{item_lang_id}}/{{site_id}}/{{item_page_id}}{{item_element_link}}">{{!item_text}}</a>
		</p>
	% end
	</nav>
</article>
</main>
