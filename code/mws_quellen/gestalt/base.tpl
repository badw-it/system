% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% lang_id = kwargs['lang_id']
% site_id = kwargs['site_id']
% page_id = kwargs['page_id']
% is_default_page = bool(page_id == db.page_id[site_id])
<!DOCTYPE html>
<html id="top" about="/{{lang_id}}/{{site_id}}/{{page_id}}" class="desk txt" lang="{{lang_id}}" prefix="q: http://quellen-perspectivia.net/{{db.site_id}}/rdfvoc# rda: http://rdvocab.info/Elements/ rdfs: http://www.w3.org/2000/01/rdf-schema# schema: http://schema.org/">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. © http://maxweberstiftung.de -->
	<link href="{{db.favicon_path[site_id]}}" rel="icon"/>
	<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/cssjs/mws_quellen.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/cssjs/jquery/jquery.min.js"></script>
	<script src="/cssjs/all.js?v={{db.startsecond}}"></script>
	<title>{{!db.re.sub(r'<[^>]*>', '', db.glosses[site_id][lang_id].replace('\n', ' '))}}</title>
</head>
<body>
<header about="/{{site_id}}">
% for path, link, flat, br in db.main_icons_path_link_flat_br[site_id]:
  % if link:
	<a class="{{!'' if flat else 'shadow '}}img" href="{{link}}"{{!' property="schema:sourceOrganization"' if (site_id == db.site_id and is_default_page) else ''}}><img src="{{path}}" alt="{{path}}"/></a>
  % else:
	<img{{!'' if flat else ' class="shadow"'}} src="{{path}}" alt="{{path}}"/>
  % end
  % if br:
	<br/>
  % end
% end
% if site_id == db.site_id:
	<h1><a href="http://www.perspectivia.net/"{{!' property="schema:publisher"' if is_default_page else ''}}>perspectivia.net</a>
	<small><a href="http://quellen.perspectivia.net/frontpage/">{{!db.glosses['sources_and_databases'][lang_id]}}</a></small></h1>
% else:
	<h1>{{!db.glosses[site_id][lang_id]}}</h1>
% end
	<nav>
		<ul>
		% for menu_id in db.menu_ids[site_id]:
		  % if menu_id == page_id:
			<li>{{db.glosses[menu_id][lang_id]}}</li>
		  % else:
			<li><a href="{{menu_id}}">{{db.glosses[menu_id][lang_id]}}</a></li>
		  % end
		% end
		</ul>
	</nav>
	<nav class="extra">
	  % if site_id != db.site_id:
		<a href="/{{lang_id}}/{{db.site_id}}/search_fulltext?base_site_id={{site_id}}">{{db.glosses['search_fulltext'][lang_id]}}</a>&#160;· 
		<a href="/{{lang_id}}/{{db.site_id}}/user_guide">{{db.glosses['user_guide'][lang_id]}}</a>&#160;· 
	  % end
		<a href="/{{lang_id}}/{{site_id}}/{{page_id}}/rdf.nt">RDFa</a>
	</nav>
  % if len(db.lang_ids[site_id]) > 1:
	<nav class="extra lang">
	% for target_lang_id in db.lang_ids[site_id]:
	  % if target_lang_id == lang_id:
		<b>{{db.glosses['lang_name'][lang_id]}}</b>
	  % else:
		<a href="/{{target_lang_id}}/{{site_id}}/{{page_id}}{{'?' + request.query_string if request.query_string else ''}}" hreflang="{{target_lang_id}}" lang="{{target_lang_id}}" rel="alternate">{{db.glosses['lang_name'][target_lang_id]}}</a>
	  % end
	% end
	</nav>
  % end
  % if site_id != db.site_id:
	<section class="extra">
		<p>
			<a class="img" href="http://www.maxweberstiftung.de"{{!' property="schema:sourceOrganization"' if is_default_page else ''}}><img src="/-/{{db.site_id}}/icons/mws_half.svg" alt="Max-Weber-Stiftung"/></a>
			<a class="img" href="http://www.perspectivia.net"><img src="/-/{{db.site_id}}/icons/perspectivia.png" alt="Perspectivia"/></a>
		</p>
		<p>
			<a href="http://www.perspectivia.net"{{!' property="schema:publisher"' if is_default_page else ''}}>perspectivia.net</a> <br/>
			<a href="http://quellen.perspectivia.net/frontpage/">{{!db.glosses['sources_and_databases'][lang_id]}}</a>
		</p>
	</section>
  % end
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
</body>
</html>
