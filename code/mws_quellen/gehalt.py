# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
import html
import json
import mmap
import os
import re # Sic. Unusually, regex is much slower with the fulltext search here.
import unicodedata
import xml.etree.ElementTree as ET
import zipfile
from collections import deque
from functools import partial

import __init__
import fs
import xmlhtml
import web_io

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.

        Set additional configurations taken from a file specified in the
        first configurations at [ids][config_page]. This file must be in
        each subfolder representing a subwebsite.

        The configurations taken from there are:

        - :attr:`.favicon_path`
        - :attr:`.main_icons_path_link_flat_br`
        - :attr:`.menu_ids`
        - :attr:`.page_id`
        - :attr:`.lang_ids`
        - :attr:`.lang_id`
        '''
        super().__init__(urdata, __file__)

        self.site_id = self.config['ids']['site']
        self.site_ids = tuple(sorted(
                name for name in os.listdir(self.content_path)
                if os.path.isdir(self.content_path + name) ))
        self.favicon_path = {}
        self.main_icons_path_link_flat_br = {}
        self.menu_ids = {}
        self.page_id = {}
        self.lang_ids = {}
        self.lang_id = {}
        for site_id in self.site_ids:
            try:
                elem = ET.parse(os.path.join(
                        self.content_path,
                        site_id,
                        self.config['ids']['config_page']))
                self.favicon_path[site_id] = elem.find(
                        './/*[@id="favicon"]/tbody/tr/td').text or ''
                self.main_icons_path_link_flat_br[site_id] = tuple(
                        (
                            row[0].strip(),
                            row[1].strip(),
                            True if row[2].strip().lower() == 'yes' else False,
                            True if row[3].strip().lower() == 'yes' else False,
                        )
                        for row in xmlhtml.elem_to_table(
                            elem.find('.//*[@id="main_icons"]'),
                            cell_xpath = 'td')
                        if row)
                menu_ids = xmlhtml.elem_to_tree(
                        elem.find('.//*[@id="menu_ids"]'),
                        cell_xpath = 'td')
                self.menu_ids[site_id] = tuple(menu_ids.keys())
                self.page_id[site_id] = next(
                        key for key, value in menu_ids.items()
                        if value.strip().lower() == 'default' )
                lang_ids = xmlhtml.elem_to_tree(
                        elem.find('.//*[@id="lang_ids"]'),
                        cell_xpath = 'td')
                self.lang_ids[site_id] = tuple(lang_ids.keys())
                self.lang_id[site_id] = next(
                        key for key, value in lang_ids.items()
                        if value.strip().lower() == 'default' )
            except Exception as e:
                print('\nAn Error occurred in:', site_id)
                raise e

        fulltext_index_path = fs.pave(os.path.join(
                self.content_path,
                self.site_id,
                self.config['ids']['indices_folder'],
                'temp'))
        try:
            os.remove(fulltext_index_path)
        except FileNotFoundError:
            pass
        self.make_fulltext_index(fulltext_index_path, site_ids = self.site_ids)
        with open(fulltext_index_path, 'rb') as file:
            self.fulltext = mmap.mmap(
                    file.fileno(), 0, access = mmap.ACCESS_READ)

    def add_ajax_source(
            self,
            doc: str,
            site_id: str = '',
            table_config_re: 're.Pattern[str]' = re.compile(
                r'(?s)data-table_config_json\s*?=\s*?(?P<a>["\'])(.*?)(?P=a)'),
            ) -> str:
        '''
        Add data from an ajax source to :param:`doc` from :param:`site_id`,
        if there is such a source given in :param:`doc`. Return the doc, be
        it complemented or be it unchanged.
        '''
        if 'ajax' in doc and 'data-table_config_json' in doc:
            try:
                with open(
                        os.path.join(self.content_path, site_id, json.loads(
                            html.unescape(table_config_re.search(doc).group(2)))
                            .get('ajax', '')),
                        'r', encoding = 'utf-8') as file:
                    doc = doc.replace('</html>', '', 1) + ''.join(
                        ''.join(
                            '<td about="{}">'.format(row['rowAttrs']['about'])
                            + cell + '</td>' for cell in row.values()
                            if isinstance(cell, str) )
                        for row in json.load(file)['data']
                        ) + '</html>'
            except: # Sic. We prefer an empty page.
                pass
        return doc

    def get_mapping(
            self,
            text: bytes,
            site_id: str = '',
            keypath: str = './/thead/tr/th',
            valuepath: str = './/*[@id="{}"]/td',
            ajax_key: str = 'data-table_config_json',
            meta_source_re: 're.Pattern[str]' = re.compile(
                r'''data-meta_source_id\s*?=\s*?(?P<a>["'])(.*?)(?P=a)'''),
            ) -> 'tuple[tuple[str, str], ...]':
        '''
        Extract a key-value mapping from an XML source, whose path is given
        in the attribute `data-meta_source_id` of the first element of
        :param:`text`. In the XML source, the keys are found under
        :param:`keypath`, and the values are found under :param:`valuepath`.
        Both parameters can contain ``{}``, which will be substituted by any
        fragment identifier found in the path given in `data-meta_source_id`.
        Alternatively, the values are to be found in an ajax source, whose
        path is given in an attribute :param:`ajax_key` of the root element.
        '''
        try:
            meta_source_id, fragment_id = meta_source_re.search(
                    str(text.split(b'>', 1)[0], encoding = 'utf-8')
                    ).group(2).rsplit('#', 1)
            tree = ET.parse(os.path.join(
                    self.content_path, site_id, meta_source_id) + '.xml')
            keys = tuple(map(xmlhtml.serialize_elem,
                    tree.findall(keypath.format(fragment_id))))
            ajax_element = tree.find(f'[@{ajax_key}]')
            ajax_path = json.loads(ajax_element.attrib.get(ajax_key)
                    ).get('ajax', '') if ajax_element else None
            if ajax_path:
                with open(os.path.join(self.content_path, site_id, ajax_path),
                        encoding = 'utf-8') as file:
                    ajax_source = json.load(file)
                for row in ajax_source['data']:
                    if row['rowAttrs']['id'] == fragment_id:
                        break
                values = ( '<td>' + row[str(i)] + '</td>'
                        for i in range(len(keys)) )
            else:
                values = map(xmlhtml.serialize_elem,
                        tree.findall(valuepath.format(fragment_id)))
            return tuple(zip(keys, values))
        except: # Sic.
            return ()

    def get_toc_items(
            self,
            site_id: str = '',
            a_tag_re: 're.Pattern[str]' = re.compile(r'</?a\b[^>]*>'),
            ) -> 'Generator[tuple[str, str, str, str, str, int]]':
        '''
        Yield (as a generator) for every page of the site :param:`site_id` and
        for every heading of this page the following data: `item_lang_id`,
        `item_page_id`, `item_element_link`, `item_text`, `indent`.
        '''
        path = os.path.abspath(self.content_path) + os.sep
        if site_id:
            path += site_id + os.sep
        for element in ET.parse(
                path + '_table_of_contents_ids.xml').findall('.//tbody/tr/td'):
            item_page_id = element.text or ''
            langs = [self.lang_id[site_id]]
            tag_has_lang = []
            for event, elem in ET.iterparse(
                    path + item_page_id + '.xml', events = ('start', 'end')):
                if event == 'start':
                    if 'lang' in elem.attrib:
                        langs.append(elem.attrib['lang'])
                        tag_has_lang.append(True)
                    else:
                        tag_has_lang.append(False)
                    if elem.tag in ('h1', 'h2', 'h3', 'h4', 'h5', 'h6'):
                        yield (
                                langs[-1],
                                item_page_id,
                                '#' + elem.get('id', default = ''),
                                a_tag_re.sub(
                                    '',
                                    xmlhtml.serialize_elem(elem, inner = True)),
                                int(elem.tag[1]))
                else:
                    if tag_has_lang.pop():
                        langs.pop()

    def make_fulltext_index(
            self,
            fulltext_index_path: str,
            site_ids: 'Sequence[str]' = [''],
            ) -> None:
        '''
        Make a new fulltext search index at :param:`fulltext_index_path`.

        The index starts and ends with a newline. Between these newlines,
        there is an arbitrary number of records, delimited from each other
        by a newline. A record is structured as follows::

            status_id//lang_id/site_id/page_id#part_id//text-passage

        - `status_id` is a status flag; here: `txtsrc` or `txt`.
        - `lang_id/site_id/page_id#part_id` is the URL to `text-passage`;
          `#part_id` can be missing.
        - `text-passage` is a part of the fulltext.

        :param site_ids: a sequence of the IDs of all sites to be included
            in the fulltext index.
        '''
        print('\nI make a new fulltext-search index now at:\n',
                fulltext_index_path)
        preprocess_res = [ (re.compile(old), new) for old, new in
                (
                    # remove word-breaking hyphens at line ends:
                    (r'[-\xad]<br\s*?/>\s*', ''),
                ) ]
        postprocess_res = [ (re.compile(old), new) for old, new in
                (
                    (r'\s+', ' '),
                    # prevent DOS attacks of simple evil regular expressions:
                    (r'(.)\1{4,}', r'\g<1>\g<1>\g<1>'),
                ) ]
        try:
            with open(fulltext_index_path, 'w', encoding = 'utf-8') as testfile:
                pass
        except OSError:
            raise Exception(
                    '\n\nAn error occurred:\n'
                    'The index file is open and cannot be overwritten.\n'
                    'Most probably the system is already running.\n'
                    'So, shut down the system and then start anew.\n')
        with open(fulltext_index_path, 'w', encoding = 'utf-8') as outfile:
            outfile.write('\n')
            for site_id in site_ids:
                path_head = self.content_path
                if site_id:
                    path_head += site_id + os.sep
                for path_tail in ( name for name in os.listdir(path_head)
                        if os.path.isfile(path_head + name)
                        and name[0] != '_' ):
                    page_id = path_tail.rsplit('.', 1)[0]
                    lang_id = self.lang_id[site_id]
                    parser = FragmentTextExtractor(tracked_attrs =
                            {'class': ('txtsrc', 'txt')})
                    with open(path_head + path_tail, 'r', encoding = 'utf-8'
                            ) as infile:
                        try:
                            doc = infile.read()
                        except UnicodeDecodeError:
                            print('\nWARNING: {}/{} is not UTF-8 encoded and '
                                    'hence left out'.format(site_id, page_id))
                            continue
                    try:
                        for old, new in preprocess_res:
                            doc = old.sub(new, doc)
                        parser.parse(doc)
                        for extract in parser.extracts:
                            text = extract[1].strip()
                            if text:
                                for old, new in postprocess_res:
                                    text = old.sub(new, text)
                                part_id = extract[0]
                                status_id = extract[2].get('class', ['txt'])[0]
                                outfile.write('{}//{}/{}{}//{}\n'.format(
                                        status_id,
                                        lang_id,
                                        site_id + '/' + page_id,
                                        '#' + part_id if part_id else '',
                                        unicodedata.normalize('NFKD', text),
                                        ))
                    except Exception as e:
                        print('\nA fatal error occurred in:',
                                f'{site_id}/{page_id}')
                        raise e
        print('\n(Done.)\n')

    def search_fulltext(
            self,
            term: str,
            no_case: bool = True,
            regex: bool = False,
            single_words: bool = False,
            status_ids: 'Sequence[str]' = [],
            site_ids: 'Sequence[str]' = [],
            at_least_one_letter_re: 're.Pattern[str]' = re.compile(
                r'(?<!\\)\w'),
            flags_re: 're.Pattern[str]' = re.compile(r'\(\?[aiLmsux]+?\)'),
            ) -> 'Generator[tuple[str, str, str, set[str]]]':
        '''
        Search :param:`term` in the fulltext index.
        Yield (as a generator) tuples containing:

        - a status flag, either `txtsrc` or `txt`.
        - the URL to the text passage containing the match.
        - the text passage containing the match.
        - a set of the matching substrings within the passage.
          This may be useful, when a search is done with a regular
          expression and the matching substrings shall be marked
          e.g. in an HTML output.

        .. important::
            At the moment, only the first 500 results are returned.
            This limitation is set to avoid performance issues both
            on the host computer and even more in e.g. the browser
            processing the results.

        .. todo::
            Remove the above mentioned limitation with the help of a
            solution via ajax and paging (or infinite scroll?).

        :param no_case: If true, upper and lower case are not distinguished.
        :param regex: If true, the :param:`term` is used as a regular
            expression.
        :param status_ids: a possibly empty list of status flags to restrict
            the search to records starting with one of these flags. If empty,
            no restriction takes place.
        :param site_ids: a possibly empty list of site ids to restrict
            the search to records whose URL points to any site having one
            of these site ids. If empty, no restriction takes place. The
            site_id is the name of the folder containing the site.
        '''
        term = unicodedata.normalize('NFKD', term)
        if not at_least_one_letter_re.search(term):
            term = r'\Z.'
            regex = True
        if single_words:
            term = r'(\b{}\b)'.format(
                   r'\b)|(\b'.join(map(
                   r'\b.*?\b'.join, permutations(
                       term.split() if regex else
                       map(re.escape, term.split())))))
        elif regex:
            term = flags_re.sub('', ' '.join(term.split()))
            term = ''.join( '({})'.format(c)
                    if ord(c) > 127 else c for c in term )
        else:
            term = re.escape(' '.join(term.split()))
        try:
            term_re = re.compile(term, re.ASCII)
        except:
            term = r'\Z.'
            term_re = re.compile(term, re.ASCII)
        fullterm = r'\n{}//[^/\n]*/{}[^/\n]*//.*?({}).*'.format(
                r'(?:{})'.format('|'.join( s for s in status_ids ))
                        if len(status_ids) > 1 else
                    status_ids[0] if status_ids else
                    r'[^/\n]*',
                r'(?:{})/'.format('|'.join( s for s in site_ids ))
                        if len(site_ids) > 1 else
                    site_ids[0] + '/' if site_ids else
                    r'[^/\n]*/',
                term
                )
        if no_case:
            fullterm = '(?i)' + fullterm
        fullterm_b = fullterm.encode(encoding = 'utf-8', errors = 'ignore')
        for counter, match in enumerate(re.finditer(fullterm_b, self.fulltext)):
            status, url, text = match.group().decode().split('//', 2)
            matches = set( m.group() for m in term_re.finditer(text) )
            yield status, url, text, matches
            if counter > 500:
                yield '', '', 'Break: too many results.', {''}
                break

class FragmentTextExtractor(html.parser.HTMLParser):
    '''
    Parser to extract blocks of inner text along with fragment ids and
    certain status flags from the elements of an HTML document:

    .. important::
        Text before or after the root element is ignored.
        Text which is directly or indirectly embedded in the root element
        but neither directly nor indirectly embedded in an element with an
        id attribute **is** extracted with the fragment id being the empty
        string.

    These extracts can be useful for feeding a full-text search.

    The document is given to the parser with its method `parse`. Then, the
    extracts are stored in :attr:`FragmentTextExtractor.extracts`, which is
    a deque of 3-tuples, each containing a string as fragment id, a string as
    text and a dictionary of strings as status flag.

    Whether the inner text of an element is treated as a fragment (and not only
    as the part of a fragment), is determined by :param:`__init__.fragment_tags`
    and :param:`__init__.fragment_tags_attrs`: It is treated as a fragment,
    iff the tag of the element appears in the first parameter or the the tag
    appears in the second parameter *and* the set of pairs given alongside
    with each tagname in the second parameter is a subset of the key-value
    pairs of the attribute list of the element.

    The status flag is determined by :param:`tracked_attrs`: If this parameter
    is e.g. ``{'class': ('txtsrc', 'txt')}``, a status flag of each
    fragment stores the current or, if not present, the inherited
    **value of the attribute** `class`, **if** it is `txtsrc` or `txt`.

    .. important::
        In order to specify an attribute without value (a boolean attribute),
        specify the value as the empty string.

    >>> import re
    >>> doc = """<article>
    ...         <section class="txt">
    ...          <h1>Wessobrunner Gebet</h1>
    ...          <p id="1">The following poem is one of the most beautiful and most enigmatic OHG texts.</p>
    ...         </section>
    ...         <section class="xyz txtsrc">
    ...          <h2 id="2">De poeta</h2>
    ...          <p id="3">Dat <span class="txt">ga</span>fregin ih mit firahim &hellip;</p>
    ...         </section>
    ...        </article>"""
    >>> parser = FragmentTextExtractor(tracked_attrs = {'class': ('txtsrc', 'txt')})
    >>> parser.parse(doc)
    >>> for fragment_id, text, flag in parser.extracts:
    ...     text = re.sub('\\\\s+', ' ', text) # Simplify output.
    ...     print((fragment_id, text, flag))
    ('', 'Wessobrunner Gebet', {'class': ['txt']})
    ('1', 'The following poem is one of the most beautiful and most enigmatic OHG texts.', {'class': ['txt']})
    ('', ' ', {'class': ['txt']})
    ('2', 'De poeta', {'class': ['txtsrc']})
    ('3', 'Dat gafregin ih mit firahim …', {'class': ['txtsrc']})
    ('', ' ', {'class': ['txtsrc']})
    ('', ' ', {})
    '''
    def __init__(
            self,
            tracked_attrs: 'dict[str, Sequence[str]]' = {'': ('',)},
            fragment_tags: 'Sequence[str]' = (
                'article',
                'aside',
                'blockquote',
                'button',
                'canvas',
                'caption',
                'dd',
                'div',
                'dt',
                'embed',
                'fieldset',
                'figcaption',
                'figure',
                'footer',
                'form',
                'h1',
                'h2',
                'h3',
                'h4',
                'h5',
                'h6',
                'header',
                'li',
                'map',
                'object',
                'output',
                'p',
                'pre',
                'section',
                'textarea',
                'video',
                'th',
                'tr',
                ),
            fragment_tags_attrs: 'dict[str, Abstractset[tuple[str, str]]]' = {
                'small': {('class', 'note')},
                },
            ):
        '''
        :param tracked_attrs: See the docstring of the class.
        '''
        super().__init__(convert_charrefs = True)
        self.extracts       = deque()
        self.chunks         = deque([deque()])
        self.fragment_ids   = deque([''])
        self.tracked_attrs  = tracked_attrs
        self.flags          = deque([{}])
        self.fragment_flags = deque([True])
        self.fragment_tags  = fragment_tags
        self.fragment_tags_attrs = fragment_tags_attrs

    def handle_data(self, data: str):
        '''
        Append a text part to the current row of text parts.
        Text parts before or after the root element are ignored.
        '''
        self.chunks[-1].append(data)

    def handle_endtag(self, tag: str):
        '''
        Make a new extract, if the closed element is a to be treated
        as a fragment (and not only as a part of a fragment).
        '''
        if self.fragment_flags.pop():
            self.extracts.append((
                    self.fragment_ids.pop(),
                    ''.join(self.chunks.pop()),
                    self.flags.pop()
                    ))
        else:
            self.flags.pop()

    def handle_starttag(
            self,
            tag: str,
            attrs: 'list[tuple[str, None|str]]',
            ):
        '''
        If the new element is to be treated as a fragment (and not only as a
        part of the current fragment), append a new row, in which the text
        part of this element will be put. Memoize this status, the `id`
        value, if one is present, and the current or inherited status of
        the attribute values which are to be tracked.
        '''
        attrs_dict = dict(attrs)
        attrs_split = []
        for key, value in attrs:
            for subvalue in (value or '').split():
                attrs_split.append((key, subvalue))
        if tag in self.fragment_tags or (
                tag in self.fragment_tags_attrs and
                self.fragment_tags_attrs[tag].issubset(attrs_split)):
            self.fragment_flags.append(True)
            self.chunks.append(deque())
            if 'id' in attrs_dict:
                self.fragment_ids.append(attrs_dict['id'])
            else:
                self.fragment_ids.append(self.fragment_ids[-1])
        else:
            self.fragment_flags.append(False)
        flags = {}
        for key, status in self.tracked_attrs.items():
            if key in attrs_dict:
                values = [ val for _, val in attrs_split if val in status ]
                if values:
                    flags[key] = values
            if (key not in flags) and (key in self.flags[-1]):
                flags[key] = self.flags[-1][key]
        self.flags.append(flags)

    def parse(self, doc: str) -> None:
        self.feed(doc)
        self.close()

def zip_paths(paths: 'Sequence[str]', save_path: str) -> None:
    '''
    Zip all files and folders found at or within any path of :param:`paths`.
    Retain the folder structure. Save the zipfile at :param:`save_path`.

    .. warning::
        A file or folder already existing at :param:`save_path` will be
        overwritten.
    '''
    all_paths = set()
    for file_or_dirpath in paths:
        for path in fs.get_paths(file_or_dirpath):
            all_paths.add(path)
    all_paths = sorted(all_paths)
    prefix = os.path.dirname(os.path.commonprefix(all_paths))
    with zipfile.ZipFile(fs.pave(save_path), 'w') as file:
        for path in all_paths:
            file.write(path, path.split(prefix, 1)[1].strip(os.sep))

if __name__ == '__main__':
    import doctest
    doctest.testmod()
