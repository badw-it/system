# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import sys
import time

import gehalt
import __init__
import bottle
from bottle import HTTPError, redirect, request, static_file, template

import rdf

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        self.kwargs = {
                'app': self,
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': bottle.server_names[db.server_name](
                    host = db.host,
                    port = db.port,
                    ),
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/')
        @self.route('/<site_id>')
        @self.route('/<site_id>/<page_id>')
        def redirect_short_url(lang_id = '', site_id = '', page_id = ''):
            '''
            Redirect requests which specify only `site_id` and `page_id` or
            only `site_id` or nothing at all:

            - If `site_id` is either lacking or not known within this portal,
              `db.site_id` is assumed and written in the URL.
            - For `lang_id`, the default `lang_id` of the site is assumed and
              written in the URL.
            - For `page_id`, the default `page_id` of the site is assumed and
              written in the URL.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of this function.
            '''
            if not (site_id and site_id in db.site_ids):
                site_id = db.site_id
            lang_id = db.lang_id[site_id]
            if not page_id:
                page_id = db.page_id[site_id]
            redirect(f'/{lang_id}/{site_id}/{page_id}')

        @self.route(f'/<lang_id>/{db.site_id}/search_fulltext')
        def return_search_form(lang_id):
            '''
            Return a page with a full-text search form.

            :param lang_id: See :func:`.return_page`.
            '''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'search_form.tpl',
                        'lang_id': lang_id,
                        'site_id': db.site_id,
                        'page_id': 'search_fulltext',
                        })

        @self.route(f'/<lang_id>/{db.site_id}/search_results')
        def return_search_results(lang_id):
            '''
            Return a page containing the results of a search triggered by
            :func:`.return_search_form`.
            '''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'search_results.tpl',
                        'lang_id': lang_id,
                        'site_id': db.site_id,
                        'page_id': 'search_results',
                        })

        @self.route('/<lang_id>/<site_id>/<page_id>')
        def return_page(lang_id, site_id, page_id):
            '''
            Return the page specified by the URL.

            If :param:`site_id` is not known within this portal,
            redirect to the default site.

            If :param:`lang_id` does not denote a supported language,
            redirect to its default language.

            If :param:`page_id` does not denote a page of the site,
            raise the HTTP error 404 (`Not found`).

            :param lang_id: As a rule, it is an id conforming to ISO 639 and
                http://tools.ietf.org/html/rfc5646, page 4 (‘shortest ISO 639
                code sometimes followed by extended language subtags’).
                It must not be str:`-` in order to exclude any confusion
                with the route of :func:`.return_static_code`.
            '''
            if site_id not in db.site_ids:
                site_id = db.site_id
                redirect(f'/{lang_id}/{site_id}/{db.page_id[site_id]}')
            if lang_id not in db.lang_ids[site_id]:
                redirect(f'/{db.lang_id[site_id]}/{site_id}/{page_id}')
            text, tpl = db.get_text_and_template_name(site_id, page_id)
            if text:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'lang_id': lang_id,
                            'site_id': site_id,
                            'page_id': page_id,
                            'text': text,
                            'indexinfolink':
                                f'/{lang_id}/portal/user_guide#indexfilter',
                            })
            else:
                raise HTTPError(404, db.glosses['httperror404'][lang_id])

        @self.route('/<lang_id>/<site_id>/<page_id>/rdf.nt')
        def return_rdf_ntriples(lang_id, site_id, page_id):
            '''
            Return RDF triples as a UTF-8 encoded plain text of N-Triples.
            The triples are extracted from the RDFa annotation of the web page
            specified by :param:`lang_id`, :param:`site_id`, :param:`page_id`.
            '''
            try:
                doc = return_page(lang_id, site_id, page_id)
            except HTTPError:
                body = ''
            else:
                doc = db.add_ajax_source(doc, site_id)
                parser = rdf.RDFaParser(base = '{}://{}/{}/{}/{}'.format(
                        request.urlparts[0],
                        request.urlparts[1],
                        lang_id,
                        site_id,
                        page_id))
                parser.parse(doc)
                body = rdf.convert_triples_to_nt(parser.triples)
            return bottle.HTTPResponse(
                    body = body,
                    # black magic in bottle.py:
                    **{'Content-Type': 'text/plain; charset=utf-8'})

        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/site/icons/favicon.ico`
            - `/de/acta/secreta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.

            :param _: is included in the route so that in a page with the
                URL `/de/mysite/start` a relative URL `icons/favicon.ico`
                can be used. For the absolute version of this relative URL
                would be: `/de/mysite/icons/favicon.ico`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(1)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
