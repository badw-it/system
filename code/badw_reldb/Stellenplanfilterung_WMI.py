# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
import sys
from Stellenplanfilterung import main

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1].lower().endswith('.xlsx'):
        filepath = sys.argv[1]
    else:
        filepath = ''
    main(
            filepath = filepath,
            institution_re = re.compile(r'(?i)^\s*wmi\b'),
            )
