# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Access to the database of the Bavarian Academy of Sciences.
'''
try: import regex as re
except ImportError: import re
import socket
import sqlite3
import sys
import time
import xml.etree.ElementTree as ET
from collections import deque
from functools import partial
from itertools import chain

import __init__
import catalog_io
import fs
import mail
import sql_io
from parse import sortnum
from structs import empty

class DB:
    def __init__(
            self,
            urdata: 'list[str]',
            db_user: str,
            db_password: str,
            testing: bool,
            ):
        '''
        Aggregate a configuration object from :param:`urdata`, which may
        contain one or more absolute paths to files. Each of these named
        files must have the structure of ini-files as itʼs understood by
        :module:`configparser`.

        From this configuration object, take the basic attributes of the
        DB object initialized here.

        :param testing: whether the test-database and its connection parameters
            are to be used.
        :param db_user: name of a user who has already been created with read
            and write permissions.
        :param db_password: password of this user.
        '''
        self.config = config = fs.get_config(urdata)
        self.paths = paths = fs.get_abspaths(config, {
                'paths_from_program_folder': __file__,
                'paths_from_config_folder': urdata[0],
                })
        socket.setdefaulttimeout(config['reldb'].getint(
                'socket_defaulttimeout'))
        if not (db_user and db_password):
            db_user, db_password = fs.get_keys(paths['reldb_access'])
        suffix = '_test' if testing else ''
        self.connect = sql_io.get_connect(
                db_user,
                db_password,
                config,
                key = 'reldb',
                db_host_key = f'host{suffix}',
                db_port_key = f'port{suffix}',
                db_name_key = f'name{suffix}',
                )
        self.catalog_url            = config['catalog']['url']
        self.catalog_query_url      = config['catalog']['query_url']
        self.pub_query_key          = config['catalog']['query_key']
        self.roles                  = {
                k: tuple(map(int, (v.split(None, 2) + ['0', '0', '0'])[:2]))
                for k, v in config['roles'].items() }

        self.genre_id_col           = config['reldb']['genre_id_col']
        self.pub_id_col             = config['reldb']['pub_id_col']
        self.pub_catid_col          = config['reldb']['pub_catid_col']
        self.pub_link_col           = config['reldb']['pub_link_col']
        self.pub_altlink_col        = config['reldb']['pub_altlink_col']
        self.pub_status_col         = config['reldb']['pub_status_col']
        self.series_id_col          = config['reldb']['series_id_col']
        self.series_name_col        = config['reldb']['series_name_col']
        self.series_description_col = config['reldb']['series_description_col']
        self.series_catid_alias_col = config['reldb']['series_catid_alias_col']

        self.sql_get_genre_id_by_genre = """
                select badw_puf_id
                from badw_list_publikationsform
                where badw_puf_name = %s
                """
        self.sql_get_person_by_person_catid = """
                select badw_per_id
                from badw_data_personen
                where badw_per_pnd = %s
                """
        self.sql_get_pub_ids_and_links = """
                select
                    badw_pub_id,
                    badw_pub_bvnr,
                    badw_pub_digipublink,
                    badw_pub_link
                from badw_data_publikationen
                where (
                    badw_pub_digipublink <> ''
                    or badw_pub_link <> ''
                    )
                and badw_pub_deleted = 0
                """
        self.sql_get_pub_id_by_pub_catid = """
                select
                    badw_pub_id
                from badw_data_publikationen
                where badw_pub_bvnr = %s
                and badw_pub_deleted = 0
                """
        self.sql_get_pub_ids_and_status_from_db = """
                select
                    badw_pub_id,
                    badw_pub_bvnr,
                    badw_pub_neu
                from badw_data_publikationen
                where badw_pub_bvnr is not null
                and badw_pub_deleted = 0
                """
        self.sql_get_role_text = """
                select badw_pua_name
                from badw_list_publizistenart
                where badw_pua_id = %s
                """
        self.sql_get_series_by_series_catid = """
                select
                    badw_rei_id,
                    badw_rei_reihentitel,
                    badw_rei_reihentext
                from badw_data_reihen
                where badw_rei_nr = %s
                """
        self.sql_get_series_catid_alias = """
                select badw_bvz_ziel
                from badw_data_bvzuordnung
                where badw_bvz_quelle = %s
                """
        self.sql_insert_pub_catid_and_link = """
                insert into badw_data_publikationen
                    (badw_pub_bvnr, badw_pub_digipublink)
                values
                    (%s, %s)
                """
        self.sql_map_pub_genre_if_not_exists = """
                insert into badw_nm_publikationen_publikationsform
                    (badw_pub_id, badw_puf_id)
                select        %s,          %s
                from dual
                where not exists
                    (select * from badw_nm_publikationen_publikationsform
                     where badw_pub_id = %s
                        and badw_puf_id = %s)
                """
        self.sql_map_pub_person_if_not_exists = """
                insert into badw_nm_personen_publikationen
                    (badw_pub_id, badw_per_id, badw_pua_id)
                select        %s,          %s,          %s
                from dual
                where not exists
                    (select * from badw_nm_personen_publikationen
                     where badw_pub_id = %s
                        and badw_per_id = %s
                        and badw_pua_id = %s)
                """
        self.sql_map_pub_series_if_not_exists = """
                insert into badw_nm_publikationen_reihen
                    (badw_pub_id,
                     badw_rei_id,
                     badw_pub_bandnummer,
                     badw_pub_bandsort)
                select %s, %s, %s, %s
                from dual
                where not exists
                    (select * from badw_nm_publikationen_reihen
                     where badw_pub_id = %s
                        and badw_rei_id = %s)
                """
        self.sql_unmap_pub_genre = """
                delete from badw_nm_publikationen_publikationsform
                where badw_pub_id = %s
                """
        self.sql_unmap_pub_person = """
                delete from badw_nm_personen_publikationen
                where badw_pub_id = %s
                """
        self.sql_unmap_pub_series = """
                delete from badw_nm_publikationen_reihen
                where badw_pub_id = %s
                """
        self.sql_update_pub = """
                update badw_data_publikationen
                set
                    badw_pub_bandnr     = %s,
                    badw_pub_autoren    = %s,
                    badw_pub_jahr       = %s,
                    badw_pub_ort        = %s,
                    badw_pub_reihe      = %s,
                    badw_pub_reihetext  = %s,
                    badw_pub_titel      = %s,
                    badw_pub_untertitel = %s,
                    badw_pub_verlag     = %s,
                    badw_pub_seitenzahl = %s,
                    badw_pub_in_nummer  = %s,
                    badw_pub_neu        = 0
                where badw_pub_id       = %s
                """

    def check_and_add(
            self,
            adding: bool = False,
            permaloop: bool = False,
            ) -> None:
        '''
        Compare the items of the database :param:`self` with the items on
        the server that is specified in the configuration :attr:`self.config`.

        .. important::
            If :param:`adding` is ``True``, an item seemingly missing in
            the database is added and filled with the information available
            on the server.

        Inform a mail recipient (specified in the configuration) about the
        results and whether the database was changed. This information also
        contains a description of the performed checks (also specified in the
        configuration).

        :param permaloop: If ``True``, the program starts again after a run
            in an infinite loop. The waiting time between new starts is
            specified in the configuration.
        '''
        meta_path = self.paths['meta']
        catalog_link_prefix = self.config['catalog']['url'].rstrip('/') + '/'
        link_prefix = self.config['ids']['url'].rstrip('/') + '/de/'
        pub_link_re = re.compile(self.config['ids']['fullpattern'])
        mail_config = self.config['mail']
        if adding:
            catalog = catalog_io.Catalog(self.catalog_url)
        refresh_pause = int(self.config['reldb'].getint('pause_seconds'))
        while True:
            with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
                cur.execute('select * from publications')
                pub_linkids = {
                        row[0]: list(map(str, (
                            row[12], # person
                            row[2], # title
                            row[5], # place
                            row[6], # publisher
                            row[7], # year
                            row[9], # series
                            row[10], # serial_num
                            row[1], # pub_catid
                            )))
                        for row in cur.fetchall()
                        }
            db_pub_linkids = {}
            malformed_links = deque()
            duplicate_links = deque()
            other_in_db_not_in_publica = deque()
            try:
                with sql_io.DBCon(self.connect()) as (con, cur):
                    cur.execute(self.sql_get_pub_ids_and_links)
                    for row in cur.fetchall():
                        pub_id = str(row[self.pub_id_col])
                        pub_catid = (row[self.pub_catid_col] or '').strip()
                        pub_catlink = (catalog_link_prefix + pub_catid
                                ) if pub_catid else ''
                        pub_link = (row[self.pub_link_col] or ''
                                ).strip().rstrip('/')
                        pub_altlink = (row[self.pub_altlink_col] or ''
                                ).strip().rstrip('/')
                        if pub_link:
                            try:
                                pub_linkid = pub_link_re.search(pub_link).group(
                                        'pub_id')
                            except (AttributeError, IndexError):
                                malformed_links.append((
                                        pub_id,
                                        pub_link,
                                        pub_catlink,
                                        ))
                                continue
                            if pub_linkid in db_pub_linkids:
                                duplicate_links.append((
                                        pub_id,
                                        pub_link,
                                        pub_catlink,
                                        ))
                            else:
                                db_pub_linkids[pub_linkid] = (
                                        pub_link, pub_catid, pub_id)
                        elif pub_altlink and pub_catlink:
                            other_in_db_not_in_publica.append((
                                    pub_id,
                                    pub_catlink,
                                    pub_altlink,
                                    ))
            except Exception as e:
                print(e)
                time.sleep(12)
                continue
            diverging_pub_catids = deque()
            in_publica_not_in_db = deque()
            in_publica_not_in_db_but_matching_cat_link = deque()
            for pub_linkid, row in pub_linkids.items():
                pub_catid = row.pop()
                db_row = db_pub_linkids.get(pub_linkid)
                result = [
                        link_prefix + pub_linkid,
                        catalog_link_prefix + pub_catid,
                        ]
                if db_row:
                    if pub_catid != db_row[1]:
                        result += [
                                db_row[2], # pub_id
                                catalog_link_prefix + db_row[1],
                                ] + row
                        diverging_pub_catids.append(result)
                else:
                    pub_id = ''
                    with sql_io.DBCon(self.connect()) as (con, cur):
                        cur.execute(
                                self.sql_get_pub_id_by_pub_catid, (pub_catid,))
                        db_row = cur.fetchone()
                        # The database must ensure that a catid is unique.
                        if db_row:
                            pub_id = db_row[self.pub_id_col]
                    if adding and not db_row:
                        # Even if `adding`, do not add if catid is found.
                        with sql_io.DBCon(self.connect()) as (con, cur):
                            cur.execute(
                                    self.sql_insert_pub_catid_and_link,
                                    (pub_catid, link_prefix + pub_linkid))
                            pub_id = cur.lastrowid
                            con.commit()
                        item = catalog.read(
                                catalog_io.SRU |
                                {'query': self.pub_query_key + '=' + pub_catid})
                        if item and catalog.get_pub_id(item):
                            self.update_pub_and_relations(
                                    pub_id, item, catalog)
                    if pub_id:
                        result += [str(pub_id)]
                    result += row
                    if db_row:
                        in_publica_not_in_db_but_matching_cat_link.append(
                                result)
                    else:
                        in_publica_not_in_db.append(result)
            in_db_not_in_publica = deque(
                    [db_row[2], db_row[0], catalog_link_prefix + db_row[1]]
                    for db_pub_linkid, db_row in db_pub_linkids.items()
                    if db_pub_linkid not in pub_linkids )
            text = '\n\n'.join((
                    mail_config['title_reldb'],
                    '1.\n' + mail_config[
                        'in_publica_not_in_db_but_matching_cat_link'],
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        in_publica_not_in_db_but_matching_cat_link ) if
                        in_publica_not_in_db_but_matching_cat_link else
                            mail_config['no_item'],
                    '2.\n' + mail_config['in_publica_not_in_db'] + '\n\n' + (
                            mail_config['hint_at_adding'] if adding else
                            mail_config['hint_at_no_adding']),
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        in_publica_not_in_db ) if
                        in_publica_not_in_db else mail_config['no_item'],
                    '3.\n' + mail_config['in_db_not_in_publica'],
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        in_db_not_in_publica ) if
                        in_db_not_in_publica else mail_config['no_item'],
                    '4.\n' + mail_config['malformed_links'],
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        malformed_links ) if
                        malformed_links else mail_config['no_item'],
                    '5.\n' + mail_config['duplicate_links'],
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        duplicate_links ) if
                        duplicate_links else mail_config['no_item'],
                    '6.\n' + mail_config['diverging_pub_catids'],
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        diverging_pub_catids ) if
                        diverging_pub_catids else mail_config['no_item'],
                    '7.\n' + mail_config['other_in_db_not_in_publica'],
                    '█ ' + '\n█ '.join( ' █ '.join(row) for row in
                        other_in_db_not_in_publica ) if
                        other_in_db_not_in_publica else mail_config['no_item'],
                    )) + '\n'
            try:
                mail.send(
                        host = mail_config['smtp_host'],
                        port = int(mail_config['smtp_port']),
                        From = mail_config['sender'],
                        To = ', '.join(mail_config['recipients_reldb'].split()),
                        Subject = mail_config['title_reldb'].split('\n')[0],
                        text = text,
                        )
            except ConnectionRefusedError:
                print(text)
                if not permaloop:
                    print('ConnectionRefusedError')
            if not permaloop:
                break
            end = time.time() + refresh_pause
            while time.time() < end:
                time.sleep(2)
                # This loop is done instead of e.g. time.sleep(refresh_pause)
                # in order to avoid blocking and enable cancelling with ctrl-c.

    def combine_titles(
            self,
            toptitle: str,
            subtitle: str,
            sectiontitle: str,
            punctuation_re: 're.Pattern[str]' = re.compile(r'[.:!?]\W*$'),
            ) -> 'tuple[str, str]':
        '''
        Combine the :param:`toptitle`, :param:`subtitle`, :param:`sectiontitle`,
        (obtained from the library catalog) to a toptitle and a subtitle.
        '''
        toptitle = toptitle.strip()
        subtitle = subtitle.strip()
        sectiontitle = sectiontitle.strip()
        if subtitle and sectiontitle:
            if subtitle:
                subtitle = subtitle[0].upper() + subtitle[1:]
            if sectiontitle:
                sectiontitle = sectiontitle[0].upper() + sectiontitle[1:]
            if subtitle and sectiontitle:
                if not punctuation_re.search(subtitle):
                    subtitle += '.'
                subtitle += ' '
        subtitle = subtitle + sectiontitle
        for old, new in (
                ('\x98', ''),
                ('\x9c', ''),
                (' ;', ';'),
                (' :', ':'),
                ):
            toptitle = toptitle.replace(old, new)
            subtitle = subtitle.replace(old, new)
        return toptitle, subtitle

    def get_persontext_map_pub_person(
            self,
            pub_id: str,
            persons: 'list[tuple[str, str, str, str, str]]',
            ) -> str:
        '''
        Combine the information in :param:`persons` to a string serving as
        freetext information about the persons involved in the publication
        specified by :param:`pub_id` (the database ID).

        For each person, take a role information from :param:`persons`.
        (This information characterizes the personʼs role regarding the
        publication.) The term expressing the role information originates
        from the library catalog. By this term, get the role id valid in
        the database, namely from :attr:`.roles`. This is a mapping of such a
        term to a pair: the id and a flag. With the id, get the
        corresponding role text. If the flag is ``1``, add the text as a hint
        behind the personʼs name in the freetext information to be returned.

        .. important::
            If the role term from the catalog is not found in
            :attr:`.roles`, ``0`` is assumed as the id of the role and ``0``
            is assumed as the flag.

        Also for each person, search the given person id in the items of
        the table of persons. If it is found, map the corresponding item to
        the item of the publication specified by :param:`pub_id`. In
        this mapping, save also the personʼs role regarding the publication.

        .. important::
            All *old* mappings between the specified publication and a person
            are deleted.

        :param persons: as returned from
            :meth:`catalog_io.Catalog.get_personal_names_ids_roles_marc`.

            .. important::
                :param:`persons` from the catalog may contain duplicates that
                will be filtered out here.
        '''
        authors = []
        others = []
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(self.sql_unmap_pub_person, pub_id)
            for name, name_enum, name_add, person_catid, role in persons:
                name = name.replace(' ', '\u202F')
                if name_enum:
                    name += '\u202F' + name_enum
                if name_add:
                    name += f" ({name_add})"
                role_id, be_shown =\
                        self.roles[role] if role in self.roles else (0, 0)
                if be_shown:
                    cur.execute(self.sql_get_role_text, role_id)
                    temp = cur.fetchone()
                    if temp:
                        _, role_text = temp.popitem()
                        name += f" [{role_text}]"
                if role == 'aut':
                    if name not in authors:
                        authors.append(name)
                else:
                    if name not in others:
                        others.append(name)
                cur.execute(
                        self.sql_get_person_by_person_catid, person_catid)
                temp = cur.fetchone()
                if temp:
                    _, person_id = temp.popitem()
                    cur.execute(self.sql_map_pub_person_if_not_exists,
                            (pub_id, person_id, role_id,
                             pub_id, person_id, role_id))
                con.commit()
        return'; '.join(authors + others)

    def get_pub_ids_from_db(
            self,
            import_all: bool = False) -> 'Generator[tuple[int, str]]':
        '''
        From the publications table, yield the publication id of the database
        and the publication id of the catalog for all items ...

        - which are not set to deleted
        - which have a value in the field of the catalog id
        - and, if :param:`import_all` is ``False``, which are set to be fresh,
          i.e. recently inserted or updated in the catalog.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(self.sql_get_pub_ids_and_status_from_db)
            items = cur.fetchall()
        for item in items:
            if import_all or item[self.pub_status_col]:
                yield (item[self.pub_id_col], item[self.pub_catid_col])

    def get_series_texts_map_pub_series(
            self,
            pub_id: str,
            series: 'dict[str, deque[tuple[str, str]]]',
            ) -> 'tuple[str, str, str]':
        '''
        Get name and description of any series from :param:`series` by looking
        them up in the table of series. Combine the names and the descriptions,
        each into one string. Do the same with serial numbers of the belonging
        publication, specified by :param:`pub_id` (the database ID).
        Return these three strings.

        .. important::
            Before the lookup in the table of series, the identifying key
            from the catalog is looked up in a list with series key aliases
            and replaced by an alias if an alias exists. This is done in
            order to reduce the variety of series set by the library.

        Map the publication to each series that is given in :param:`series`
        *and* found in the tables of series of the database. To each mapping,
        assign the belonging serial number. From this serial number, form a
        string apt at sorting and assign this sortstring, too.

        .. important::
            All *old* mappings between the specified publication and a series
            are deleted.

        :param series: The dictionary keys are not used at the moment (they
            identify the source of the values). The values are pairs of a
            series key, used in the catalog to identify the series, and the
            serial number of the publication within its series.
        :param pub_id: id of the belonging publication.
        '''
        series_names        = []
        series_descriptions = []
        serial_nums         = []
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(self.sql_unmap_pub_series, pub_id)
            unfound_series_catids = []
            mappings_number = 0
            for series_catid, serial_num in chain.from_iterable(
                    series.values()):
                serial_nums.append(serial_num)
                cur.execute(self.sql_get_series_catid_alias, series_catid)
                series_catid = (cur.fetchone() or empty
                        )[self.series_catid_alias_col] or series_catid
                cur.execute(
                        self.sql_get_series_by_series_catid, series_catid)
                item = cur.fetchone()
                if item:
                    series_id = item[self.series_id_col]
                    series_name = item[self.series_name_col]
                    series_description = item[self.series_description_col]
                    series_names.append(series_name)
                    if series_description:
                        series_descriptions.append(series_description)
                    serial_sortnum = sortnum(serial_num)
                    self.sql_map_pub_series_if_not_exists
                    cur.execute(self.sql_map_pub_series_if_not_exists, (
                            pub_id, series_id, serial_num, serial_sortnum,
                            pub_id, series_id))
                    mappings_number += 1
                else:
                    unfound_series_catids.append(series_catid)
            for unfound_series_catid in unfound_series_catids:
                print(
                        f"Catalog series key “{series_catid}” is not found in "
                        f"the table of series and occurs with the publication "
                        f"of the database id “{pub_id}”, which could be mapped "
                        f"to a series {mappings_number} times.")
            con.commit()
        series_nametext    = '; '.join(series_names)
        series_description = '\n\n'.join(series_descriptions)
        serial_numtext     = '; '.join(serial_nums)
        return series_nametext, series_description, serial_numtext

    def import_from_catalog(self, import_all: bool = False) -> None:
        '''
        Import bibliographical information from the library catalog.

        The affected tables are:

        - the table of publications,
        - the table of mappings between publications and persons,
        - the table of mappings between publications and series.

        .. note::
            No longer affected are:

            - the table of mappings between publications and genres,
            - the table of genres.

            In the table of genres, a genre not found there but given in the
            catalog was added except if it is the empty string.

        (Other tables are only read in order to obtain the information to be
        fed in the aforementioned tables.)

        .. important::
            - A publication that is treated gets all of its *old* values
              overwritten and gets all of its *old* relations deleted.

        A publication is treated if the following conditions are met in its
        corresponding item in the tables of publications:

        - Its item has a valid value in the field with the catalog id.
          A value is valid if the URL generated out of this value leads to an
          item of the catalog.
        - Its item is not set to be deleted (according to the *deleted* field
          in the item).
        - Its item is set to be fresh (according to the *fresh* field in the
          item) or :param:`import_all` is ``True``. (An explanation follows:)

        After being refreshed, an item is set to be not fresh.

        .. important::
            If :param:`import_all` is ``False`` (the default), the import is
            done only for the items set as fresh. This can be done relatively
            often.

            If :param:`import_all` is ``True``, the import is done for all
            items meeting the first two conditions. This task may be so time
            consuming and may produce such an excessive load on the library
            catalog that it cannot be done often, but rather e.g. once a month.
        '''
        catalog = catalog_io.Catalog(self.catalog_query_url)
        print(
                f"A new import is started for "
                f"{'all' if import_all else 'the fresh'} "
                f"items having a catalog id.")
        for pub_id, pub_catid in self.get_pub_ids_from_db(import_all):
            item = catalog.read(
                    catalog_io.SRU |
                    {'query': self.pub_query_key + '=' + pub_catid})
            if not item or not catalog.get_pub_id(item):
                print(
                        f"The database ID “{pub_id}” has the catalog ID "
                        f"“{pub_catid}”; and the catalog "
                        f"“{self.catalog_query_url}” lacks this key "
                        f"or is down at the moment.")
                continue
            self.update_pub_and_relations(pub_id, item, catalog)
        print('The import is done.')

    def map_pub_genre(
            self,
            pub_id: str,
            genres: 'Generator[tuple[str, str, str]]'
            ) -> None:
        '''
        Map the publication :param:`pub_id` (the database ID) to each genre
        given in :param:`genres`.

        .. important::
            - All *old* mappings between the specified publication and a series
              are deleted (except if the given genre is the empty string).
            - In the table of genres: A genre not found there but given in the
              catalog *was* added except if it is the empty string.
              Now, it is simply skipped.

        :param series: The keys are not used at the moment (they identify the
            source of the values). The values are pairs of a series key, used
            in the catalog to identify the series, and the serial number of
            the publication within its series.
        :param pub_id: id of the belonging publication.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            old_mappings_are_deleted = False
            genres = set( genre for _, genre, _ in genres )
            for genre in genres:
                if genre:
                    if not old_mappings_are_deleted:
                        cur.execute(self.sql_unmap_pub_genre, pub_id)
                        con.commit()
                        old_mappings_are_deleted = True
                    cur.execute(self.sql_get_genre_id_by_genre, genre)
                    genre_id = cur.fetchone()
                    if genre_id is None:
                        print(
                                f"Genre “{genre}” is not found in the list of "
                                f"genres and occurs with the publication of "
                                f"the database id “{pub_id}”.")
                    else:
                        genre_id = genre_id.popitem()[1]
                        cur.execute(self.sql_map_pub_genre_if_not_exists,
                                (pub_id, genre_id,
                                 pub_id, genre_id))
                    con.commit()

    def update_pub_and_relations(
            self,
            pub_id: str,
            item: ET.Element,
            catalog: catalog_io.Catalog,
            ) -> None:
        '''
        For the publication specified by :param:`pub_id` (the database ID),
        update its fields and relations in:

        - the table of publications,
        - the table of mappings between publications and persons,
        - the table of mappings between publications and series.

        .. important::
            See also :meth:`import_from_catalog`.

        :param item: the item that is obtained from the library catalog
            :param:`catalog` and belongs to the publication :param:`pub_id`.
            The information extracted from this item is combined with
            information found in the database to perform the update.
        :param catalog: the model of the catalog associated with the database.
            Here, its methods for extracting data from :param:`item` are used.
        '''
        year           = catalog.get_year(item)
        year           = (re.search(r'\d{4}', year) or empty).group()
        # Database requires either an integer < 10000 or Null:
        year           = None if year == '' else int(year)
        place          = catalog.get_place(item)
        publisher      = catalog.get_publisher(item)
        persons        = list(catalog.get_personal_names_ids_roles(item))
        persontext     = self.get_persontext_map_pub_person(pub_id, persons)
        (
            toptitle,
            subtitle,
            sectiontitle
        )              = catalog.get_titles(item)
        (
            title,
            subtitle
        )              = self.combine_titles(toptitle, subtitle, sectiontitle)
        series         = catalog.get_series_id_serial_num(item)
        (
            series_nametext,
            series_description,
            serial_numtext,
        )              = self.get_series_texts_map_pub_series(pub_id, series)
        extent         = '; '.join(filter(None, (
                         catalog.get_extent(item),
                         catalog.get_extent_other(item),
                         catalog.get_extent_accompanying_material(item),
                         )))
        isbns_issns    = ', '.join(catalog.get_isbns_issns(item))
        # Obsolete: self.map_pub_genre(pub_id, catalog.get_genre(item))
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(self.sql_update_pub,
                    (
                    serial_numtext,
                    persontext,
                    year,
                    place,
                    series_nametext,
                    series_description,
                    title,
                    subtitle,
                    publisher,
                    extent,
                    isbns_issns,
                    pub_id,
                    ))
            con.commit()
