# -*- mode: python -*-

block_cipher = None


a = Analysis(['Stellenplanfilterung_LRZ.py'],
             pathex=['..', 'Z:\\Ablage2013\\6 IT\\6.1 Digitalisierung\\6.1.22 Applikationen\\StefanMüller\\code\\python\\badw_reldb'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='Stellenplanfilterung_LRZ',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
