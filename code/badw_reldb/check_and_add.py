# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017 ff. (© http://badw.de)
import getpass
import sys

import gehalt
import __init__
import sys_io
import fs

def main(
        db_access_path: str,
        config_path: str,
        testing: bool,
        adding: bool = False,
        permaloop: bool = False,
        ):
    '''
    Compare bibliographical items in the reldb and on the publication server.
    Take the configuration data for both of them from :param:`config_path`, and
    register any inconsistencies between the two sets of items. Configure and
    send a report about it to email addresses according to the configuration.

    .. important::
        If :param:`adding` is ``True``, an item seemingly missing in
        the database is added and filled with the information available
        on the server.

    .. note::
        A log will be saved in the folder at the relative path
        ``'../../../__logs__'`` starting from the folder that contains the very
        file wherein you are reading this; and the name of this file is also
        used to build the name of the log file. A suffix in this name ensures
        that the log file never overwrites something.

    :param db_access_path: path to a file that contains a representation of the
        user name and password of the database to be checked. If this is empty,
        does not lead to a file or leads to a file whose content canʼt be taken
        as a hash representing a user name and a password, then a user name and
        a password can be entered on input prompt after the program has started.
    :param config_path: path to an ini-file containing the configuration.
    :param testing: whether the test database is to be refreshed or the
        real database used in production.
    :param permaloop: If ``True``, the program will repeatedly run. The waiting
        time between new starts is specified in the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__', tell_path = True)
    sys.stderr = log
    sys.stdout = log
    (
    db_user,
    db_password
    ) = fs.get_keys(db_access_path) if db_access_path else ('', '')
    if not (db_user and db_password):
        db_user = input('Database Username: ')
        db_password = getpass.getpass('Password: ')
    db = gehalt.DB([config_path], db_user, db_password, testing)
    db.check_and_add(
            adding = adding,
            permaloop = permaloop,
            )

if __name__ == '__main__':
    main(**sys_io.get_call_args_for(main))
