# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017 ff. (© http://badw.de)
import getpass
import sys

import gehalt
import __init__
import sys_io
import fs

def main(
        db_access_path: str,
        config_path: str,
        testing: bool,
        import_all: bool = False,
        ):
    '''
    Import bibliographical information from the library catalog.

    .. note::
        The import and certain incidents occuring during the import are
        logged. Notably:
        - If a connection with the catalog or the database cannot be made
          within “socket_defaulttimeout” seconds (to be specified in the
          configuration), the import is cancelled without raising an
          exception.
        - If a series key from the catalog cannot be found in the table of
          series, this series key and the publication id are logged.

        The log will be saved in the folder at the relative path
        ``'../../../__logs__'`` starting from the folder that contains the
        very file wherein you are reading this; and the name of this file is
        also used to build the name of the log file. A suffix in this name
        ensures that the log file never overwrites something.

    The affected tables are:

    - the table of publications,
    - the table of mappings between publications and persons,
    - the table of mappings between publications and series.

    .. note::
        No longer affected are:

        - the table of mappings between publications and genres,
        - the table of genres.

        In the table of genres, a genre not found there but given in the
        catalog was added except if it is the empty string.

    (Other tables are only read in order to obtain the information to be
    fed in the aforementioned tables.)

    .. important::
        - A publication that is treated gets all of its *old* values
          overwritten and gets all of its *old* relations deleted.

    A publication is treated if the following conditions are met in its
    corresponding item in the tables of publications:

    - Its item has a valid value in the field with the catalog id.
      A value is valid if the URL generated out of this value leads to an
      item of the catalog.
    - Its item is not set to be deleted (according to the *deleted* field
      in the item).
    - Its item is set to be fresh (according to the *fresh* field in the
      item) or :param:`import_all` is ``True``. (An explanation follows:)

    After being refreshed, an item is set to be not fresh.

    .. important::
        If :param:`import_all` is ``False`` (the default), the import is
        done only for the items set as fresh. This can be done relatively
        often.

        If :param:`import_all` is ``True``, the import is done for all
        items meeting the first two conditions. This task may be so time
        consuming and may produce such an excessive load on the library
        catalog that it cannot be done often, but rather e.g. once a month.

    :param db_access_path: path to a file that contains a representation of
        the user name and password of the database to be checked. If not
        given or not found or unreadable, user name and password will be
        asked for.
    :param config_path: path to an ini-file containing the configuration.
    :param testing: whether the test database is to be refreshed or the
        real database used in production.
    '''
    log = fs.Log(__file__, '../../../__logs__', tell_path = True)
    sys.stderr = log
    sys.stdout = log
    (
    db_user,
    db_password
    ) = fs.get_keys(db_access_path) if db_access_path else ('', '')
    if not (db_user and db_password):
        db_user = input('Database Username: ')
        db_password = getpass.getpass('Password: ')
    db = gehalt.DB([config_path], db_user, db_password, testing)
    db.import_from_catalog(import_all = import_all)

if __name__ == '__main__':
    main(**sys_io.get_call_args_for(main))
