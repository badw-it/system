% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
<!doctype html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2016 ff. (© http://badw.de) -->
	<title>Akademiespiegel-Prüfblatt</title>
	<style>
*, *:after, *:before {
	border: 0;
	box-sizing: inherit;
	color: inherit;
	font-kerning: auto;
	line-height: 1.4em;
	margin: 0;
	padding: 0;
	position: relative;
}
.small-caps {
	font-variant: small-caps;
}
body {
	font-family: "TheSansOsF Light";
	font-size: 1em;
	margin: auto;
	max-width: 900px;
	padding: 20px
}
i {
	font-family: "TheSansOsF Light Italic";
	font-style: italic;
}
p {
	margin-bottom: 0.4em;
	margin-top: 0.4em;
}
	</style>
</head>
<body>
<h1>Mitglieder</h1>
% old_group = ''
% old_section = ''
% for e in items:
  % group = e['gruppe']
  % section = e['sektion']
  % birth = e['geburtsangabe']
  % official = e['kontaktdienstlich']
  % private = e['kontaktprivat']
  % mail_private = e['mailprivate']
  % mail_official = e['mailofficial']
  % if group != old_group:
	% old_group = group
	<h2>{{group}}</h2>
  % end
  % if section != old_section:
	% old_section = section
	<h3>{{section}}</h3>
  % end
	<p>
		<span class="small-caps">{{e['nachname']}}</span> {{e['vorname']}}, {{e['alletitel']}}, {{e['beruf']}} {{e['wahljahr']}}{{', ' + birth if birth else ''}}
	  % if official:
		<br/><i>dienstlich:</i> {{official}}
	  % end
	  % if mail_private:
		<br/>{{mail_private}}
	  % end
	  % if private:
		<br/><i>privat:</i> {{private}}
	  % end
	  % if mail_official:
		<br/>{{mail_official}}
	  % end
	</p>
% end
</body>
</html>
