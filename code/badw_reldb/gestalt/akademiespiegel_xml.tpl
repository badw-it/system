<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<root>
<h1>Mitglieder</h1>
% old_group = ''
% old_section = ''
% for e in items:
  % group = e['gruppe']
  % section = e['sektion']
  % birth = e['geburtsangabe']
  % official = e['kontaktdienstlich']
  % private = e['kontaktprivat']
  % mail_private = e['mailprivate']
  % mail_official = e['mailofficial']
  % if group != old_group:
	% old_group = group
	<h2>{{group}}</h2>
  % end
  % if section != old_section:
	% old_section = section
	<h3>{{section}}</h3>
  % end
	<p>
		<person><caps>{{e['nachname']}}</caps> {{e['vorname']}}, {{e['alletitel']}}, {{e['beruf']}} {{e['wahljahr']}}{{', ' + birth if birth else ''}}</person>
	  % if official:
		<address_official><i>dienstlich:</i> {{official}}</address_official>
	  % end
	  % if mail_private:
		<mail>{{mail_private}}</mail>
	  % end
	  % if private:
		<address_private><i>privat:</i> {{private}}</address_private>
	  % end
	  % if mail_official:
		<mail>{{mail_official}}</mail>
	  % end
	</p>
% end
</root>
