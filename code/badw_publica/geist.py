# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import json
try: import regex as re
except ImportError: import re
import sys
import time
from collections import deque
from urllib.parse import quote, unquote

import gehalt
import __init__
import bottle
from bottle import HTTPError, redirect, request, response, static_file, template
from parse import sortday

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        server.quiet = False if db.debug else True
        self.kwargs = {
                'app': self,
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which specify no page nor language.
            Assume the default page and the default language of the site.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            scheme, host, path, query_string, fragment = request.urlparts
            path = db.config['domain_map'].get(host, '')
            if path:
                redirect(path)
            redirect(f'/{db.lang_id}/meta/{db.page_id}')

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/favicon.ico')
        def redirect_favicon_request():
            redirect('/-/content/icons/favicon.ico')

        @self.route('/file/<pub_id>')
        def return_publication_file(pub_id):
            '''
            Return the content of the publication identified by :param:`pub_id`
            as one file – even if the publication is split up into e.g. several
            PDF files. That is the key difference to :func:`return_publication`.
            It was implemented as transfer URL for the OAI-PMH interface.

            :param _: See :func:`return_static_content`.
            '''
            filename, data = db.get_publication_file(pub_id)
            response.set_header(
                    'Content-Disposition',
                    f'attachment; filename="{filename}"')
            response.content_type = 'application/octet-stream'
            return data if data else b''

        @self.route(f'/{db.redirected_links_prefix}/<path:path>')
        def redirect_redirected_links(path):
            '''
            Redirect certain dead links which are to be reanimated here.

            All of these links start with :attr:`db.redirected_links_prefix`.
            The rest of the link is stored in :attr:`db.redirected_links` and
            mapped to the new link. If the rest is not found there, the
            request is redirected to the default page.
            '''
            redirect(db.redirected_links.get(path, '/'))

        @self.route('/<pub_id>')
        def redirect_short_permalink(pub_id):
            '''
            Redirect assuming the default language.

            The redirected request is sent to :func:`.return_publication_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect(f'/{db.lang_id}/{pub_id}')

        @self.route('/<lang_id>/api/oai')
        @self.route('/<lang_id>/api/oai', method = 'POST')
        def return_oai_filedata(
                lang_id,
                bad_date_re: 're.Pattern' = re.compile(r'[^-−+.:TZ\d\s]'),
                supergranular_re: 're.Pattern' = re.compile(r'\d-\d\d-\d\dT\d'),
                ):
            '''
            Return OAI data of the files containing publications.

            Response to the following types of queries:

            - /de/api/oai?verb=Identify
            - /de/api/oai?verb=GetRecord&identifier=badw:007457763
            - /de/api/oai?verb=ListRecords&from=2023-12-09&until=2024-07-30

            The identifier (in the second example) is the filename – prefixed by
            ``'badw:'``. A prefix with colon is necessary because the identifier
            has to be a URI.

            The date information after `from` or `until` must be provided in the
            extended format of ISO 6801. Only the year will be used. Both of the
            dates will be understood as inclusive.

            :param _: to be ignored. See :func:`return_static_content`.

            .. note::
                Neither the OAI PMH nor MARC XML is good. They are rather clumsy
                and bloated with a badly organized documentation. It is far more
                faster for both sender and recipient to develop and use a custom
                and minimal and tidily documented JSON API. One should certainly
                avoid the OAI PMH and MARC XML if possible, they are mistakes.

                Moreover, the use of OAI PMH and MARC XML here is aggravated due
                to an evolving set of requirements by the DNB – and we would not
                have implemented this OAI API if not for the DNB. Information on
                their requirements were kindly revealed in a very long series of
                letters exchanged with DNB employees, later on partly also given
                here: https://nbn-resolving.de/urn:nbn:de:101-2017011938

                The core issues of the implementation for the DNB are:

                - We see a retrodigitization as an image of the old publication,
                  the DNB sees it as a new publication (maybe to justify that we
                  must send it to them). That requires changes of the records we
                  get from our library catalog.
                - We had to split up some publications so that each one consists
                  of multiple PDFs – that was a request by the print publisher –
                  and had to re-bundle them again for the DNB.
                - Beyond that, there is a difference between how the DNB wants a
                  MARC XML record to be and how our library catalog sends it. If
                  two big state libraries of the same country (which had success
                  in library standardization already in the 19th century) do not
                  agree about the MARC-XML representation concerning one and the
                  same publication, the so-called standard is none.

                Affected fields in the MARC XML are mainly:

                - the leader field
                - controlfields
                - the publisher
                - the URL of the file
                - the license
                - fields about the original publication if itʼs retro-digitized.
            '''
            response.content_type = 'text/xml' # OAI-PMH: “must be text/xml”.
            slice_size = 50 # required by DNB.
            error = verb = form = pub_id = startterm = endterm = ''
            cursor = start = end = 0
            pub_ids: 'list[str]' = []
            resumption_token = request.params.resumptionToken
            if resumption_token:
                try:
                    params = json.loads(resumption_token)
                    verb = params['verb']
                    form = params['form']
                    pub_id = params['pub_id']
                    startterm = params['startterm']
                    endterm = params['endterm']
                    cursor = params['cursor']
                except: # Sic.
                    error = 'badResumptionToken'
            else:
                verb = request.params.verb
                form = request.params.metadataPrefix or 'marc'
                pub_id = request.params.identifier.split(':', 1)[-1]
                startterm = request.params.get('from', '')
                endterm = request.params.until
            if not error and form != 'marc':
                error = 'cannotDisseminateFormat'
            if not error and (startterm or endterm):
                if bad_date_re.search(startterm) or bad_date_re.search(endterm):
                    error = 'badArgument'
                elif (
                        supergranular_re.search(startterm) or
                        supergranular_re.search(endterm)):
                    error = 'badGranularity'
                else:
                    if startterm:
                        start = sortday(startterm + '-00-00')
                    if endterm:
                        end = sortday(endterm + '-99-99')
                    if not start and not end:
                        error = 'badArgument'
            if not error:
                if verb == 'Identify':
                    pass
                elif verb in {'GetRecord', 'ListRecords'}:
                    try:
                        pub_ids = tuple(db.get_pub_ids(pub_id, start, end))
                        assert pub_ids
                    except: # Sic.
                        error = 'idDoesNotExist' if verb == 'GetRecord'\
                                else 'noRecordsMatch'
                else:
                    error = 'badVerb'
            new_cursor = cursor + slice_size
            resumption_token =\
                    '' if len(pub_ids) <= new_cursor\
                    else json.dumps(
                        {
                            'verb': verb,
                            'form': form,
                            'pub_id': pub_id,
                            'startterm': startterm,
                            'endterm': endterm,
                            'cursor': new_cursor,
                        }, separators = (',', ':'))
            pub_ids = pub_ids[cursor:new_cursor]
            for source in (request.forms, request.query):
                attrs = tuple( (key, getattr(source, key)) for key in source )
                if attrs:
                    break
            attrs = ' '.join( '{}="{}"'.format(k, db.e(v)) for k, v in attrs )
            return template(
                    'oai.tpl', db = db, request = request,
                    kwargs = {
                        'attrs': attrs,
                        'error': error,
                        'records': deque(db.get_oai_records(pub_ids)),
                        'now': time.strftime(
                            '%Y-%m-%dT%H:%M:%SZ', time.gmtime()),
                            # gmtime, since it “must be expressed in UTC” (OAI).
                        'verb': verb,
                        'resumption_token': resumption_token,
                        })

        @self.route('/<lang_id>/api/<case:re:eigenlink(_nicht)?_in_katalog>')
        def return_rows(lang_id, case):
            '''
            Return a table showing all publications matching :param:`case`.
            '''
            export = request.query.export
            if export:
                filename = f'{case}.{export}'
                response.set_header(
                        'Content-Disposition',
                        f'attachment; filename="{db.q(filename)}"')
                response.content_type = f'text/{export}; charset=UTF8'
            return template(
                    'api_rows.tpl', db = db, request = request,
                    kwargs = {
                        'lang_id': lang_id,
                        'page_id': f'api/{case}',
                        'case': case,
                        },
                    )

        @self.route('/<lang_id>/api/<pub_id>/<form>/<path:path>')
        def return_urdata_items(lang_id, pub_id, form, path):
            '''
            Yield items created from urdata of the publication :param:`pub_id` –
            create them in accordance with :param:`form`. The urdata are located
            at :param:`path` in the folder ``db.repros_path``.

            Examples for URLs going down this route:

            - ``/de/api/bwb/index/bwb/urdata.json``.
            - ``/de/api/bwb/bdo-xml/bwb/urdata.json``.
            - ``/de/api/bwb/tr_a/bwb/urdata.json``.
            - ``/de/api/thesaurus/html-xml/thesaurus/index.json``.
            '''
            if '#' in pub_id:
                raise HTTPError(423, db.glosses['httperror423'][lang_id])
            if form == 'bdo-xml':
                response.content_type = 'application/xml'
                yield '<?xml version = "1.0" encoding = "UTF-8"?>\n<bdo>\n'
                for item in db.get_urdata_items(lang_id, pub_id, form, path):
                    yield template(
                            'bdo-xml.tpl', db = db, request = request,
                            kwargs = {
                                'pub_id': pub_id,
                                'path': path,
                                'item': item,
                                })
                yield '\n</bdo>'
            elif form == 'index':
                response.content_type = 'application/json'
                yield json.dumps(
                        {'data': tuple(
                            db.get_urdata_items(lang_id, pub_id, form, path))},
                        separators = (',', ':'))
            elif form.startswith('tr_') and pub_id == 'bwb':
                response.set_header('Access-Control-Allow-Origin', '*')
                response.content_type = 'application/json'
                yield json.dumps({
                        'data':
                        ''.join(db.get_urdata_items(
                            lang_id, pub_id, form, path))
                        })
            elif form == 'html-xml' and pub_id == 'thesaurus':
                response.content_type = 'text/html; charset=UTF-8'
                yield '''<!DOCTYPE html>
<html lang="la">
<head>
	<meta charset="utf-8" />
	<title>Index</title>
	<style>
		html {box-sizing: border-box; font-size: 18px; line-height: 1.3}
		table {border-collapse: collapse}
		th {font-weight: bold}
		th, td {border: 1px solid #bcd; padding: 2px 4px 2px 4px}
	</style>
</head>
<body>'''
                yield '''
<table>
<tr><th>Ord.</th><th>ID</th><th>Lemma</th><th>Sublemma?</th><th>Vol.:Col.</th></tr>
'''
                for rank, num, lem, sublem, loc in db.get_urdata_items(
                        lang_id, pub_id, form, path):
                    yield f'<tr>'\
                            f'<td>{rank}</td>'\
                            f'<td>{num}</td>'\
                            f'<td>{lem}</td>'\
                            f'<td>{sublem}</td>'\
                            f'<td>{loc}</td>'\
                            '</tr>\n'
                yield '</table></body></html>'
            # No ``else: raise HTTPError``: It would always rise in the end.

        @self.route('/<lang_id>/cit/<cite_id>')
        def return_citation(lang_id, cite_id):
            '''
            Return a suggested citation.

            :param lang_id: See :func:`.return_page`.
            :param cite_id: contains the pub_id and, after a dot, the format
                of the suggested citation (e.g. `bib`, `ris`, `txt`).
            '''
            pub_id, cite_form = \
                    cite_id.rsplit('.', 1) if '.' in cite_id else ('', '')
            pub_item = db.get_publication_by_pub_id(pub_id)
            response.set_header(
                    'Content-Disposition',
                    f'attachment; filename="{db.q(cite_id)}"')
            response.content_type = 'text/plain; charset=UTF8'
            return db.get_citation(
                    pub_item, cite_form, lang_id, unescaping = True
                    ) if cite_form and pub_item else ''

        @self.route('/<_>/content/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/icons/favicon.ico`
            - `/de/acta/secreta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.

            :param _: is included in the route so that in a page with the
                URL `/de/mysite/start` a relative URL `icons/favicon.ico`
                can be used. For the absolute version of this relative URL
                would be: `/de/mysite/icons/favicon.ico`.
            '''
            for _ in range(3):
                try:
                    return static_file(
                            path,
                            db.content_path,
                            download = ('export' in request.query),
                            )
                except: # Sic.
                    time.sleep(0.5)

        @self.route('/<lang_id>/meta/<page_id>')
        def return_page(lang_id, page_id):
            '''
            Return a page with information about the website, e.g.:

            - general information (:param:`page_id` being “information”).
            - information about the APIs (:param:`page_id` being “api”).
            - contact information (:param:`page_id` being “contact”).
            - the table of publications (:param:`page_id` being “index”).
            - the table prefiltered for the Abhandlungen (:param:`page_id` being
              “abhandlungen”).
            - the same for the Sitzungsberichte.

            If :param:`lang_id` does not denote a supported language,
            redirect to its default language.

            :param lang_id: As a rule, it is an id conforming to ISO 639 and
                http://tools.ietf.org/html/rfc5646, page 4 (‘shortest ISO 639
                code sometimes followed by extended language subtags’).
            '''
            if lang_id not in db.lang_ids:
                redirect(f'/{db.lang_id}/meta/{page_id}')
            if page_id == 'ersatzindex':
                text = db.get_ersatzindex()
                tpl = 'text.tpl'
            elif page_id in {'abhandlungen', 'sitzungsberichte'}:
                redirect(f'/{db.lang_id}/meta/index#~1-is-{quote(page_id)}')
            else:
                text, tpl = db.get_text_and_template_name('', page_id)
            if text:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'lang_id': lang_id,
                            'page_id': 'meta/' + page_id,
                            'text': text,
                            'ersatzlink': f'/{lang_id}/meta/ersatzindex'
                                if tpl == 'index.tpl' else '',
                            'indexinfolink':
                                f'/{lang_id}/meta/information#indexfilter',
                            })
            else:
                redirect('/')

        @self.route('/<lang_id>/<pub_id>')
        def return_publication_page(lang_id, pub_id):
            '''
            For the publication identified by the ID :param:`pub_id`,
            return its permalink page with bibliographical information
            and the link(s) to its file(s), if there is no retention.

            If :param:`lang_id` does not denote a supported language,
            redirect to its default language. See further information
            in the docstring of :func:`.return_page`.

            For :param:`pub_id`:

            - Delete any match of :attr:`db.del_from_id_re` from it.
              If that leads to a change, redirect to the new pub_id.
            - Look it up in :attr:`db.id_id_map` and, if present, get
              the new pub_id from this mapping of old to new pub_ids
              and redirect to the new one.
            '''
            if lang_id not in db.lang_ids:
                redirect(f'/{db.lang_id}/{pub_id}')
            new_pub_id = db.del_from_id_re.sub('', pub_id)
            if new_pub_id and new_pub_id != pub_id:
                redirect(f'/{lang_id}/{new_pub_id}')
            if pub_id in db.id_id_map:
                redirect(f'/{lang_id}/{db.id_id_map[pub_id]}')
            pub_item = db.get_publication_by_pub_id(pub_id)
            if pub_item:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'publication.tpl',
                            'lang_id': lang_id,
                            'page_id': pub_id,
                            'pub_id': pub_id,
                            'pub_item': pub_item,
                            })
            else:
                if '.' in pub_id:
                    pub_id = pub_id.rsplit(
                            '.', 1)[0].split('{', 1)[0].split('[', 1)[0]
                    redirect(f'/{lang_id}/{pub_id}')
                else:
                    redirect('/')

        @self.route('/<lang_id>/<pub_id>/-/<path:path>')
        def return_static_file_of_publication(lang_id, pub_id, path):
            '''
            Return a static file from a subfolder named :param:`pub_id` and
            belonging to the publication with the eigenkennung :param:`pub_id`.

            Such a subfolder may e.g. contain a file “index.json” (containing
            data for an overview of images) and belonging images.

            If the file specified by :param:`path` is named “urdata.json”,
            this file is preprocessed to generate a file of the structure apt
            for a datatable presentation.

            :param lang_id: See :func:`.return_page`.
            :param path: the relative path to the static file, seen from said
                subfolder.
            '''
            if '#' in pub_id:
                raise HTTPError(423, db.glosses['httperror423'][lang_id])
            else:
                return static_file(
                        path,
                        db.repros_path + pub_id,
                        download = ('export' in request.query),
                        )

        @self.route('/<lang_id>/<pub_id>/<name>')
        def return_publication(lang_id, pub_id, name):
            '''
            Return the file (meaning: the content) of the publication identified
            by :param:`pub_id`. This file is specified further by :param:`name`.
            If it contains a “.”, it is simply the name of the file. The file is
            located in the folder ``db.repros_path``. If it doesnʼt contain one,
            it is a secondary ID that will be looked up in :attr:`db.sub_id_map`
            to get the name of the file.
            '''
            page_id = f'{db.q(pub_id)}/{db.q(name)}'
            if '.' not in name:
                name = db.sub_id_map.get(f'{pub_id}/{name}', '')
            match = db.filename_re.search(name)
            if match is None:
                raise HTTPError(404, db.glosses['httperror404'][lang_id])
            forms = match['form'].split('.')
            if forms[-1] == 'tpl':
                with open(db.repros_path + name, encoding = 'utf-8') as file:
                    text = file.read()
                pub = template(
                        text, db = db, request = request,
                        kwargs = {
                            'lang_id': lang_id,
                            'page_id': page_id,
                            'pub_id': pub_id,
                            'table_config': {'click_nth_onclick_element': 0},
                            'indexinfolink':
                                f'/{lang_id}/meta/information#indexfilter',
                            })
            else:
                export = 'export' in request.query
                pub = static_file(name, db.repros_path, download = export)
                if export:
                    pub.set_header('Content-Disposition', f'attachment')
            if isinstance(pub, bottle.HTTPError):
                redirect(f'/{lang_id}/{pub_id}')
            else:
                return pub

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(2)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
