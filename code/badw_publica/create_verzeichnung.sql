-- For sqlite.

CREATE TABLE "Originale" (
"ID" INTEGER PRIMARY KEY AUTOINCREMENT,
-- Wird automatisch erzeugt, dient nur der dauerhaften Identifizierung.
"Projektkürzel" TEXT NOT NULL DEFAULT 'KEP', -- ██ ADAPT TO CURRENT PROJECT ██
-- Wenn gegeben, sonst Projektname.
"Beschreibung" TEXT NOT NULL DEFAULT '',
-- Bsp.: “Urkunde: Bischof Athanasios an Abt Hieronymos.” Sonderzeichen: […] umgeben Personen; {…} umgeben Orte.
"Standort" TEXT NOT NULL DEFAULT '',
-- Kurzname der Bibliothek, des Archives oder Museums (o. ä.), sofern bekannt.
"Datierung_Anfang" TEXT NOT NULL DEFAULT ''
	CHECK("Datierung_Anfang" regexp '^(-?[012]\d\d\d(-[01][0-9](-[0123][0-9])?)?)?$'),
-- Bsp.: “1203-12-02” oder “1203-12” oder “1203”
"Datierung_Ende" TEXT NOT NULL DEFAULT ''
	CHECK("Datierung_Ende" regexp '^(-?[012]\d\d\d(-[01][0-9](-[0123][0-9])?)?)?$'),
-- Bei Zeitpunkt statt ‑spanne `Originaldatierung Anfang` wiederholen
"Anmerkung" TEXT NOT NULL DEFAULT '',
-- In Ausnahmefällen zu nutzen.
"Projektpublikationen" TEXT NOT NULL DEFAULT ''
	CHECK("Projektpublikationen" regexp '^(https?://[^\s<]*(<[^\r\n>]+>)?(\r?\n|$))*$'),
-- Link(s) auf Vorschaltseite(n) von Veröffentlichung(en).
"Projektpublikationseinträge" TEXT NOT NULL DEFAULT ''
	CHECK("Projektpublikationseinträge" regexp '^(https?://[^\s<]*(<[^\r\n>]+>)?(\r?\n|$))*$'),
-- Link(s) auf Katalogseite(n) nur mit bibliographischen Angaben.
"Folgeeintrag-ID" INTEGER,
-- Um Einträge in eine Reihenfolge, die sich nicht aus der Sortierung nach einer oder mehreren der vorherigen Spalten ergibt, zu bringen, kann man hier die ID des nächstfolgenden Eintrages eingeben.
FOREIGN KEY("Folgeeintrag-ID") REFERENCES Originale(ID)
);

CREATE TABLE "Dokumente" (
"ID" INTEGER PRIMARY KEY AUTOINCREMENT,
-- Wird automatisch erzeugt, dient nur der dauerhaften Identifizierung. Wird erzeugt durch Addition von 1 bei jeder weiteren Eingabe. Es kann daher erwünscht sein, die Eingabe nach Sichtung und in sinnvoller Reihenfolge vorzunehmen.
"Projektkürzel" TEXT NOT NULL DEFAULT 'KEP', -- ██ ADAPT TO CURRENT PROJECT ██
-- Wenn gegeben, sonst Projektname.
"Standort" TEXT NOT NULL DEFAULT '',
-- Zum Beispiel der Karton, in dem das Dokument liegt.
"Signatur" TEXT NOT NULL DEFAULT '',
-- Zum Beispiel eine Regestennummer: Alte Signatur, wenn eine vorhanden ist, oder natürliche Signatur, wenn per analogiam ergänzbar. Nur zur Bewahrung alter Signaturen, hinfort nicht als Standorthinweis oder Identifikator verwendet.
"Typ" TEXT NOT NULL DEFAULT ''
	CHECK("Typ" in (
		'Abschrift',
		'Abzug', -- von einem Negativ usw.
		'Ausdruck',
		'Digitalisat',
		'Mikrofilm',
		'Negativ',
		'Karteikarte',
		'Objekt',
		'')),
"Stückzahl" INTEGER NOT NULL DEFAULT 1,
-- Bsp.: “2” mit Dokumenttyp “Negativ”, um anzugeben, dass es 2 Negative sind (die zusammengehören, etwa die Seiten derselben Urkunde zeigen).
"Beschreibung" TEXT NOT NULL DEFAULT '',
-- Bsp.: “Schreibmaschine mit lat. und griech. Buchstaben.” Sonderzeichen: […] umgeben Personen; {…} umgeben Orte.
"Datierung_Anfang" TEXT NOT NULL DEFAULT ''
	CHECK("Datierung_Anfang" regexp '^(-?[012]\d\d\d(-[01][0-9](-[0123][0-9])?)?)?$'),
-- Bsp.: “1903-12-02” oder “1903-12” oder “1903”.
"Datierung_Ende" TEXT NOT NULL DEFAULT ''
	CHECK("Datierung_Ende" regexp '^(-?[012]\d\d\d(-[01][0-9](-[0123][0-9])?)?)?$'),
-- Bei Zeitpunkt statt ‑spanne `Dokumentdatierung Anfang` wiederholen.
"Original-ID" INTEGER,
-- Die ID des zuzuordnenden Eintrages aus der Tabelle “Originale”.
"Anmerkung" TEXT NOT NULL DEFAULT '',
-- In Ausnahmefällen zu nutzen.
"Folgeeintrag-ID" INTEGER,
-- Um Einträge in eine Reihenfolge, die sich nicht aus der Sortierung nach einer oder mehreren der vorherigen Spalten ergibt, zu bringen, kann man hier die ID des nächstfolgenden Eintrages eingeben.
FOREIGN KEY("Folgeeintrag-ID") REFERENCES Dokumente(ID),
FOREIGN KEY("Original-ID") REFERENCES Originale(ID)
);

-- -- Lesefassung, enthält nicht die verkleinerten Spalten:
-- select
-- 	Dokumente.ID as ID,
-- 	Dokumente.Standort as `Standort Dokument`,
-- 	Signatur,
-- 	Typ,
-- 	Stückzahl,
-- 	Originale.Beschreibung as `Art und Beschreibung`,
-- 	Originale.Standort as `Standort Original`,
-- 	Originale.Datierung_Anfang as Datierung
-- from Dokumente
-- join Originale
-- on `Original-ID` = Originale.ID
-- order by ID
-- ;