% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id']
% page_id = kwargs['page_id']
<!DOCTYPE html>
<html id="top" class="desk txt" lang="{{lang_id}}">
<head>
% include('head.tpl', db = db, request = request, kwargs = kwargs)
<title>BAdW · {{page_id.split('/', 1)[-1]}}</title>
</head>
<body>
<header>
	<a class="img" href="http://badw.de"><img src="/-/content/icons/badw_marke_name.svg" alt="BAdW"/></a>
	<h1>{{db.glosses['publications'][lang_id]}}<br/>
	{{db.glosses['online_adv'][lang_id]}}</h1>
	<nav>
		<ul>
			<li>\\
			  % if page_id == 'meta/index':
				<b>{{db.glosses['full_list'][lang_id]}}</b>\\
			  % else:
				<a href="/{{lang_id}}/meta/index">{{db.glosses['full_list'][lang_id]}}</a>\\
			  % end
			</li>
			<li>\\
			  % if page_id == 'meta/information':
				<b>{{db.glosses['user_guide'][lang_id]}}</b>\\
			  % else:
				<a href="/{{lang_id}}/meta/information">{{db.glosses['user_guide'][lang_id]}}</a>\\
			  % end
			</li>
			<li>\\
			  % if page_id == 'meta/api':
				<b>API (OAI-PMH)</b>\\
			  % else:
				<a href="/{{lang_id}}/meta/api">API (OAI-PMH)</a>\\
			  % end
			</li>
		</ul>
	</nav>
% kwargs['exclude_from_header'] = {'meta/index'}
% include('header.tpl', db = db, request = request, kwargs = kwargs)
</header>
<a class="back_to_top key" href="#top">{{db.glosses['back_to_top'][lang_id]}}&#x202f;↑</a>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>
