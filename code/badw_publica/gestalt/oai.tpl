% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH
		xmlns="http://www.openarchives.org/OAI/2.0/"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
<responseDate>{{kwargs['now']}}</responseDate>
% if kwargs['error']:
<request>{{!'://'.join(request.urlparts[:2])}}</request>
<error code="{{kwargs['error']}}">{{kwargs['error']}}</error>
% else:
<request {{!kwargs['attrs']}}>{{!'://'.join(request.urlparts[:2])}}</request>
<{{kwargs['verb']}}>
  % if kwargs['verb'] == 'Identify':
<repositoryName>Publikationen der Bayerischen Akademie der Wissenschaften im Netz</repositoryName>
<baseURL>http://publikationen.badw.de/de/api/oai</baseURL>
<protocolVersion>2.0</protocolVersion>
<adminEmail>digitalisierung@badw.de</adminEmail>
<earliestDatestamp>{{db.get_earliest_datestamp()}}</earliestDatestamp>
<deletedRecord>no</deletedRecord>
<granularity>YYYY-MM-DD</granularity>
  % else:
<!-- Number of records delivered on this page: {{len(kwargs['records'])}}. (The DNB required a maximum number of 50.) -->
	% for record in kwargs['records']:
{{!record}}
	% end
	% if kwargs['verb'] == 'ListRecords':
<resumptionToken>{{kwargs['resumption_token']}}</resumptionToken>
	% end
  % end
</{{kwargs['verb']}}>
% end
</OAI-PMH>
