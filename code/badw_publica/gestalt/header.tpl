% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id']
% page_id = kwargs['page_id']
% contact = kwargs.get('contact_URL_path', '') or f'/{lang_id}/meta/contact' # will not be escaped!
<nav class="extra">
	<ul>
	  % if not 'meta/index' in kwargs.get('exclude_from_header', {}):
		<li><a href="/{{lang_id}}/meta/index">{{'BAdW publications' if lang_id == 'en' else 'BAdW-Publikationen'}}</a></li>
	  % end
	  % if not 'contact' in kwargs.get('exclude_from_header', {}):
		<li>\\
		  % if page_id == 'meta/contact':
			<b>{{db.glosses['contact'][lang_id]}}</b>\\
		  % else:
			<a href="{{!contact}}">{{db.glosses['contact'][lang_id]}}</a>\\
		  % end
		</li>
	  % end
		<li><a href="http://badw.de/data/footer-navigation/datenschutz.html">{{db.glosses['datenschutz'][lang_id]}}</a></li>
		<li><a href="http://badw.de/data/footer-navigation/impressum.html">Impressum</a></li>
	</ul>
</nav>
<nav class="extra lang">
	<ul>
	% for target_lang_id in db.lang_ids:
	  % if target_lang_id == lang_id:
		<b>{{db.glosses['lang_name'][lang_id]}}</b>
	  % else:
		<a href="/{{target_lang_id}}/{{kwargs['page_id']}}{{f'?s={request.query.s}' if request.query.s else ''}}" hreflang="{{target_lang_id}}" lang="{{target_lang_id}}" rel="alternate">{{db.glosses['lang_name'][target_lang_id]}}</a>
	  % end
	% end
	</ul>
</nav>
