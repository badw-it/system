% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Manuel Raab, Stefan Müller in 2020 ff. (© http://badw.de)
% (lem, num, exp, sem, vol, col, *_), exps, is_public = kwargs['item']
<artikel id="{{kwargs['pub_id']}}_{{lem.replace(' ', '_')}}" wb="{{kwargs['pub_id']}}" sort="{{lem.strip('-')}}" handverlesen="nein" dummy="ja">
	<lemma-position>
	  % for exp in exps:
		<such-lemma>{{exp}}</such-lemma>
	  % end
		<lemma>{{lem}}</lemma>
	</lemma-position>
	<bedeutung-position>
	  % if sem:
		<bedeutung>{{sem}}</bedeutung>
	  % end
	  % if is_public:
		<verweis frei="1" ziel-extern="https://publikationen.badw.de/de/{{kwargs['pub_id']}}/index#{{num}}">Band {{vol}}, Spalte {{col}}</verweis>
	  % else:
		<verweis frei="0" ziel-extern="https://publikationen.badw.de/de/{{kwargs['pub_id']}}/index#{{num}}">Band {{vol}}, Spalte {{col}}. Das Stichwort ist online noch nicht verfügbar, aber bereits in der gedruckten Ausgabe erschienen.</verweis>
	  % end
	</bedeutung-position>
</artikel>
