% # Licensed under http://www.apache.org/licenses/LICENSE-2.0.
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id']
% page_id = kwargs['page_id']
<main class="flexcol">
	{{!db.get_citation(kwargs['pub_item'], 'html_table', lang_id)}}
	<article class="card">
		<h2>{{db.glosses['citation_formats'][lang_id]}}</h2>
		<dl>
			<dt>{{db.glosses['plain_text'][lang_id]}} <a class="key" href="/{{lang_id}}/cit/{{page_id}}.txt">{{db.glosses['as_file'][lang_id]}}</a></dt>
			<dd>{{!db.get_citation(kwargs['pub_item'], 'txt', lang_id)}}</dd>
			<dt>RIS <a class="key" href="/{{lang_id}}/cit/{{page_id}}.ris">{{db.glosses['as_file'][lang_id]}}</a></dt>
			<dd><pre>{{!db.get_citation(kwargs['pub_item'], 'ris', lang_id)}}</pre></dd>
			<dt>BibTex <a class="key" href="/{{lang_id}}/cit/{{page_id}}.bib">{{db.glosses['as_file'][lang_id]}}</a></dt>
			<dd><pre>{{!db.get_citation(kwargs['pub_item'], 'bib', lang_id)}}</pre></dd>
		</dl>
	</article>
</main>