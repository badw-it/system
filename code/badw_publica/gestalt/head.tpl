% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<meta charset="utf-8"/>
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
% if db.debug:
<meta name="robots" content="noindex, nofollow"/>
% end
<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
<link href="/-/content/icons/favicon.ico" rel="icon"/>
<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
<link href="/cssjs/badw_publica.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
<script src="/cssjs/jquery/jquery.min.js"></script>
<script src="/cssjs/all.js?v={{db.startsecond}}"></script>
<script src="/cssjs/badw_publica.js?v={{db.startsecond}}"></script>
