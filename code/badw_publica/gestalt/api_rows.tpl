% # Licensed under http://www.apache.org/licenses/LICENSE-2.0.
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id']
% case = kwargs['case']
<!DOCTYPE html>
<html>
<head><meta charset="utf-8"/><title>{{case}}</title></head>
<body>
<table>
% for row in db.get_api_rows(lang_id, case):
{{!row}}
% end
</table>
</body>
</html>
% end