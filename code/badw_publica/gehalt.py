# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the publication server of the Bavarian Academy of Sciences.
'''
import json
import os
try: import regex as re
except ImportError: import re
import sqlite3
import subprocess
import sys
import time
import zipfile
from functools import partial
from html import unescape
from operator import itemgetter
from unicodedata import normalize

import __init__
import fs
import parse
import sql_io
import web_io

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.
        '''
        super().__init__(urdata, __file__)

        self.json_path = self.paths['json']
        self.meta_path = self.paths['meta']
        self.repros_path = self.paths['repros'] + os.sep
        self.temp_meta_dirpath = self.paths['temp_meta'] + os.sep
        self.temp_store_dirpath = self.paths['temp_store'] + os.sep
        self.perma_url = self.config['ids']['url'].rstrip('/')
        self.filename_re = re.compile(self.config['filenames']['pattern'])
        self.id_id_map = self.config['id_id_map']
        self.sub_id_map = self.config['sub_id_map']
        self.sub_id_map_reversed = { v: k for k, v in self.sub_id_map.items() }
        self.del_from_id_re = re.compile(self.config['ids']['del_from_id_re'])
        self.redirected_links = {}
        for ident, link in self.config['redirected_links'].items():
            prefix, link = link.split('/', 1)
            self.redirected_links[link] = ident
        self.redirected_links_prefix = prefix

        self.connect = partial(sqlite3.connect, self.meta_path)

        if self.config['modes'].getboolean('redacting'):
            self.process_pdfs = subprocess.Popen(
                    [
                        sys.executable,
                        fs.join(__file__, '../redact_pdfs.py'),
                        str(os.getpid()),
                        urdata[0],
                        urdata[1],
                    ],
                    )
        if self.config['modes'].getboolean('refreshing'):
            make_new_empty_db_if_absent(
                    self.meta_path, self.config['modes'].getboolean('WAL'))
            self.process_metadata = subprocess.Popen(
                    [
                        sys.executable,
                        fs.join(__file__, '../collect_metadata.py'),
                        str(os.getpid()),
                        urdata[0],
                        urdata[1],
                    ],
                    )

    def get_citation(
            self,
            pub_item: 'tuple',
            cite_form: str,
            lang_id: str = '',
            unescaping: bool = False,
            tag_re: 're.Pattern[str]' = re.compile(r'<.*?>'),
            add_re: 're.Pattern[str]' = re.compile(
                r'(?i)^(vorge\B|ed\.|ediert|editus|heraus|hrsg|hg\.)'),
            ) -> bytes:
        '''
        From the information in :param:`pub_item`, get a citation in the format
        :param:`cite_form`.

        :param lang_id: identifier of the language used in the context, in which
            the citation will be embedded, used to customize expressions with
            the help of ``self.glosses``. If not found in ``self.lang_ids``,
            the expression of ``self.lang_id`` will be selected.
        :param unescaping: whether the resulting citation is to be left escaped,
            so that it can be embedded in a webpage, or is to be unescaped,
            so that it can be delivered as download.
        :param tag_re: regular expression for removing all tags.
        '''
        (
            pub_id,
            pub_catid,
            title,
            title_addition,
            sorttitle,
            place,
            publisher,
            year,
            normyear,
            seriesstring,
            serial_num,
            backlink_in_catalog,
            personstring,
            linked_personstring,
            latest_change_raaftag,
        ) = (
                # Replace em- and en-spaces with normal spaces.
                str(item).replace(' ', ' ').replace(' ', ' ')
                for item in pub_item
            )
        infix = f'/{lang_id}/' if lang_id else '/'
        personstring = tag_re.sub('', personstring)
        title_addition = title_addition if add_re.search(title_addition) else ''
        if cite_form not in ('html_table', 'txt', 'ris', 'bib'):
            cite_form = 'txt'
        if cite_form == 'html_table':
            unescaping = False
            with sql_io.DBCon(self.connect()) as (con, cur):
                cur.execute("""
                        select name from files where pub_id = ?
                        order by name
                        """, (pub_id,))
                files = cur.fetchall()
            links = []
            for (name,) in files:
                try:
                    size = os.path.getsize(self.repros_path + name)
                except OSError:
                     # The file is inaccessible at the moment.
                    continue
                match = self.filename_re.search(name)
                if not match:
                    continue
                end_of_retention = match.group('end') or ''
                lic = match.group('lic') or 'CC BY'
                lic_url = self.q(lic.split(None, 1)[-1].lower())
                lic_nnbsp = lic.replace(' ', '\u202f')
                lic_lang = lang = '.de' if lang_id == 'de' else ''
                lic_link = f'<a href="https://creativecommons.org/licenses/{lic_url}/4.0/legalcode{lic_lang}">{lic_nnbsp}</a>'\
                        if lic.startswith('CC ') else lic
                part = match.group('part') or ''
                url = (
                        self.sub_id_map_reversed[name]
                            if name in self.sub_id_map_reversed
                        else f'{self.q(pub_id)}/{self.q(name)}'
                        )
                form = match.group('form') or ''
                forms = list(reversed(form.split('.')))
                forms[0] = forms[0].upper()
                form = ', '.join(forms)
                label = ' · '.join(filter(None, (
                        f'<strong>{part or title}</strong>',
                        'HTML' if form == 'TPL' else form,
                        (str(round(size / 1_000_000)) + ' MB')
                            if size > 1_000_000 else
                        (str(round(size / 1_000)) + ' KB'),
                        )))
                links.append(
                        f'''
		<li>
			{self.glosses['publication_available_after'][lang_id]}: {end_of_retention}. {label}
			<small>({self.glosses['license'][lang_id]}:\u00a0{lic_link})</small>
		</li>'''
                        if end_of_retention else
                        f'''
		<li>
			<a class="key" href="/{lang_id}/{url}"><span class="blinkblink">Link ☛</span> {label}</a>
			<small>({self.glosses['license'][lang_id]}:\u00a0{lic_link})</small>
		</li>'''
                )
            links = ''.join(links)
            citation = f'''<table class="card mapping">
		<tr>
			<th scope="row">{self.glosses['permalink_to_this_page'][lang_id]}:</th>
			<td><a href="{self.perma_url}/{lang_id}/{pub_id}">{self.perma_url}/{lang_id}/{pub_id}</a></td>
		</tr>
		<tr{'' if linked_personstring else ' hidden=""'}>
			<th scope="row">{self.glosses['author_s'][lang_id]}:</th>
			<td>{linked_personstring}</td>
		</tr>
		<tr>
			<th scope="row">{self.glosses['title'][lang_id]}:</th>
			<td>{title}</td>
		</tr>
		<tr{'' if title_addition else ' hidden=""'}>
			<th scope="row">{self.glosses['title_addition'][lang_id]}:</th>
			<td>{title_addition}</td>
		</tr>
		<tr{'' if place else ' hidden=""'}>
			<th scope="row">{self.glosses['place'][lang_id]}:</th>
			<td>{place}</td>
		</tr>
		<tr{'' if year else ' hidden=""'}>
			<th scope="row">{self.glosses['year'][lang_id]}:</th>
			<td>{year}</td>
		</tr>
		<tr{'' if seriesstring else ' hidden=""'}>
			<th scope="row">{self.glosses['series'][lang_id]}:</th>
			<td>{seriesstring}</td>
		</tr>
		<tr{'' if serial_num else ' hidden=""'}>
			<th scope="row">{self.glosses['serial_num'][lang_id]}:</th>
			<td>{serial_num}</td>
		</tr>
		<tr>
			<th scope="row">{self.glosses['catalog_entry'][lang_id]}:</th>
			<td><a href="https://gateway-bayern.de/{pub_catid}">https://gateway-bayern.de/{pub_catid}</a></td>
		</tr>
	</table>
	<ul class="card">{links}
	</ul>'''
        elif cite_form == 'txt':
            # Join the main sections with an em-space (Unicode x2003) and
            # the subsections with an en-space (Unicode x2002).
            # Purpose: Avoid ambiguity.
            citation = '. '.join(
                    filter(None, (
                        (personstring + ':') if personstring else '',
                        title,
                        ' '.join(filter(None, (place, year))),
                        ': '.join(filter(None, (seriesstring, serial_num))),
                        self.perma_url + infix + pub_id,
                        ))).replace(':. ', ': ', 1)
        elif cite_form == 'ris':
            personstring = personstring.replace('; ', '\r\nAU - ')
            citation = f'''
TY - BOOK
AU - {personstring}
T1 - {title}
CY - {place}
PY - {year}
T3 - {seriesstring}
VL - {serial_num}
UR - {self.perma_url}{infix}{pub_id}
ER -
'''
        elif cite_form == 'bib':
            bibtexkey = ((
                    ''.join(
                    name.split(',')[0] for name in personstring.split(';')
                    ) if personstring else '_') + year).replace(' ', '')
            personstring = personstring.replace('"', '{"}').replace('; ', ' and ')
            title = title.replace('"', '{"}')
            place = place.replace('"', '{"}')
            year = year.replace('"', '{"}')
            seriesstring = seriesstring.replace('"', '{"}')
            serial_num = serial_num.replace('"', '{"}')
            perma_domain = self.perma_url.replace('"', '{"}')
            pub_id = pub_id.replace('"', '{"}')
            citation = f'''
@Book&#123;{bibtexkey},
author  = "{personstring}",
title   = "{title}",
year    = "{year}",
address = "{place}",
series  = "{seriesstring}",
volume  = "{serial_num}",
url     = "{perma_domain}{infix}{pub_id}",
&#125;
'''
        if unescaping:
            citation = unescape(citation)
        return citation.replace('\u202F', ' ').replace('\u2009', ' ').encode()

    def get_earliest_datestamp(self) -> str:
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(r"""
                    select MIN(latest_change_raaftag)
                    from publications
                    """)
            latest_change_raaftag = cur.fetchone()[0]
        return parse.sortday_in_iso(latest_change_raaftag)

    def get_ersatzindex(self) -> bytes:
        '''
        Get an ersatz for the datatable index as xHTML5 table in bytes. The
        datatable needs Javascript, the ersatz does not.
        '''
        with open(self.json_path, 'r', encoding = 'utf-8') as file:
            table = json.load(file)['data']
        table = sorted(
                ([ (value['_'] if isinstance(value, dict) else value).encode()
                    for key, value in sorted(row.items()) ]
                for row in table ), reverse = True)
        html = b'<table class="index"><tr><td>'\
                + b'</td></tr><tr><td>'.join(
                    b'</td> <td>'.join(cells) for cells in table )\
                + b'</td></tr></table>'
        return html

    def get_oai_records(
            self,
            pub_ids: 'list[str]',
            ) -> 'Generator[str]':
        for pub_id, latest_change_raaftag in pub_ids:
            stamp = parse.sortday_in_iso(latest_change_raaftag)
            try:
                with open(
                        self.temp_meta_dirpath + pub_id + '.xml', 'r',
                        encoding = 'utf-8') as file:
                    yield file.read().replace(
                            '<datestamp></datestamp>',
                            f'<datestamp>{stamp}</datestamp>')
            except: # Sic.
                continue

    def get_pub_ids(
            self,
            pub_id: str,
            start: int,
            end: int,
            ) -> 'Generator[tuple[str, int]]':
        '''
        Get the ID and the raaftag of the latest change of every publication …

        - whose field “pub_id” equals :param:`pub_id`.
        - or whose field “latest_change_raaftag” lies between :param:`start` and
          :param:`end` inclusively.

        Exclude any publication that is still in retention (i.e. the name starts
        with “#”).

        Return the files sorted by their pub_id field, which is unique – this is
        necessary for the OAI resumption mechanism.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            if pub_id:
                cur.execute(r"""
                        select pub_id, latest_change_raaftag
                        from publications
                        where pub_id = ?
                        and not exists (
                            select 1 from files
                            where files.pub_id = publications.pub_id
                            and files.name like '#%' limit 1
                        )
                        limit 1
                        """, (pub_id,))
                for row in cur.fetchall():
                    yield row
            else:
                if start and end:
                    condition = 'between ? and ?'
                    args = [start, end]
                elif start:
                    condition = '>= ?'
                    args = [start]
                elif end:
                    condition = '<= ?'
                    args = [end]
                else:
                    condition = '<> ?'
                    args = [0]
                cur.execute(rf"""
                        select pub_id, latest_change_raaftag
                        from publications
                        where latest_change_raaftag {condition}
                        and not exists (
                            select 1 from files
                            where files.pub_id = publications.pub_id
                            and files.name like '#%' limit 1
                        )
                        order by pub_id -- necessary for resumption!
                        """, args)
                for row in cur.fetchall():
                    yield row

    def get_publication_by_pub_id(
            self,
            pub_id: str,
            ) -> 'None|tuple[str, ...]':
        '''
        For the publication identified by :param:`pub_id` in the interchange
        database, get the item with the bibliographical data about this
        publication.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute("select * from publications where pub_id = ? limit 1",
                    (pub_id,))
            return cur.fetchone()

    def get_publication_file(self, pub_id: str) -> 'tuple[str, bytes]':
        '''
        Return the name and the bytes of the file specified via :param:`pub_id`.
        The publication itself consists of either one file, which then is in the
        folder for all publications itself, or several files, which then are put
        together to an archive file saved at a folder dedicated to these archive
        files. The archive file is created and saved on the first request, after
        that the saved archive file is taken, if it has the same number of files
        as there are now associated with :param:`pub_id` and if it was generated
        after every of these associated files was. Otherwise, a new archive file
        is made.

        This is implemented mainly for the OAI interface.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute("""
                    select name from files where pub_id = ?
                    and not name like '#%'
                    and not name like '%.verkleinert.%'
                    """, (pub_id,))
            rows = cur.fetchall()
        if len(rows) == 1:
            name = rows[0][0]
            with open(os.path.join(self.repros_path, name), 'rb') as file:
                return name, file.read()
        elif len(rows) > 1:
            name = pub_id + '.zip'
            path = fs.pave(os.path.join(self.temp_store_dirpath, name))
            try:
                with zipfile.ZipFile(path, 'r') as file:
                    assert len(rows) == len(file.namelist())
                assert os.stat(path).st_mtime >= max(
                        os.stat(os.path.join(self.repros_path, row[0])).st_mtime
                        for row in rows )
            except (AssertionError, FileNotFoundError):
                with zipfile.ZipFile(path, 'w') as file:
                    for num, row in enumerate(rows, start = 1):
                        _, ext = os.path.splitext(row[0])
                        file.write(
                                os.path.join(self.repros_path, row[0]),
                                f'{num}{ext}', # The DNB canʼt handle all chars.
                                )
            with open(path, 'rb') as file:
                return name, file.read()
        return '', b''

    def get_rows(self, lang_id: str, case: str) -> 'Generator[str]':
        '''
        Get the metadata of all publications matching :param:`case`.
        '''
        case = case.lower()
        with sql_io.DBCon(self.connect()) as (con, cur):
            if case in {'eigenlink_in_katalog', 'eigenlink_nicht_in_katalog'}:
                yield '''<tr>
	<th>Nr.</th>
	<th>Eigenlink</th>
	<th>Kataloglink</th>
	<th>Person</th>
	<th>Titel</th>
	<th>Jahr</th>
	<th>Eigenreihe</th>
	<th>Reihennummer</th>
</tr>'''
                for num, (
                        pub_id,
                        pub_catid,
                        title,
                        title_addition,
                        sorttitle,
                        place,
                        publisher,
                        year,
                        normyear,
                        seriesstring,
                        serial_num,
                        backlink_in_catalog,
                        personstring,
                        linked_personstring,
                        latest_change_raaftag,
                        ) in enumerate(
                            cur.execute(r"""
                                select * from publications
                                where backlink_in_catalog = ?
                                """, (int('_nicht_' not in case),)),
                            start = 1):
                    personstring = personstring.replace('<br/>', ' ')
                    yield f'''<tr>
	<td>{num}</td>
	<td><a href="{self.perma_url}/{lang_id}/{pub_id}">{pub_id}</a></td>
	<td><a href="https://gateway-bayern.de/{pub_catid}">https://gateway-bayern.de/{pub_catid}</a></td>
	<td>{personstring}</td>
	<td>{title}</td>
	<td>{year}</td>
	<td>{seriesstring}</td>
	<td>{serial_num}</td>
</tr>'''

    def get_urdata_items(
            self,
            lang_id: str,
            pub_id: str,
            form: str,
            path: str,
            num_re: 're.Pattern[str]' = re.compile(r'\d+'),
            ) -> 'Generator':
        '''
        Yield items created from urdata of the publication :param:`pub_id` – and
        create them in accordance with :param:`form`. – The urdata will be found
        at :param:`path` in the folder ``db.repros_path``.
        '''
        fullpath = os.path.abspath(os.path.join(self.repros_path, path))
        if not fullpath.startswith(self.repros_path):
            return
        with open(fullpath, 'rb') as file:
            items = json.load(file)
        max_vol = int(self.config['index_pub_max_bwb']['vol'])
        max_odd_col = int(self.config['index_pub_max_bwb']['odd_col'])
        if form == 'index' and pub_id == 'bwb':
            for pos, (lem, num, exp, sem, vol, col, *_) in enumerate(items[1]):
                odd_col = int(num_re.search(col).group())
                if not odd_col % 2:
                    odd_col -= 1
                if not (vol < max_vol or (
                        vol == max_vol and odd_col <= max_odd_col)):
                    continue
                if sem:
                    sem = f' ‘{sem}’'
                yield {
                        '0': {
                            '_': f'''{exp}{sem} <a onclick=\"rI(event,'-/{vol}.{odd_col}.pdf',1,2,1330)\">{vol},{col}</a>''',
                            'f': exp,
                            's': pos,
                            },
                        'DT_RowId': num,
                        }
        elif form == 'bdo-xml' and pub_id == 'bwb':
            items = items[1]
            items.sort(key = itemgetter(0))
            i = 0
            while i < len(items):
                if items[i][-1] == '':
                    current_lem = items[i][0]
                    current_sem = items[i][3]
                    exps = [items[i][2]]
                    while (
                            (i + 1) < len(items) and
                            items[i + 1][0] == current_lem and
                            items[i + 1][3] == current_sem
                            ):
                        i += 1
                        exps.append(items[i][2])
                    vol = int(items[i][4])
                    odd_col = int(num_re.search(items[i][5]).group())
                    if not odd_col % 2:
                        odd_col -= 1
                    yield (
                            items[i],
                            exps,
                            vol < max_vol or
                                (vol == max_vol and odd_col <= max_odd_col),
                            )
                i += 1
        elif form.startswith('tr_') and pub_id == 'bwb':
            form, letter = form.split('_', 1)
            letter = letter.lower()
            for lem, num, exp, sem, vol, col, *_ in items[1]:
                if not normalize(
                        'NFKD', exp.lower().lstrip('-')).startswith(letter):
                    continue
                odd_col = int(num_re.search(col).group())
                if not odd_col % 2:
                    odd_col -= 1
                if sem:
                    sem = f' ‘{sem}’'
                ref = f' <a href="https://publikationen.badw.de/de/{pub_id}/index#{num}">↗</a>' \
                        if (vol < max_vol or
                            (vol == max_vol and odd_col <= max_odd_col)) else ''
                vol = parse.arabic_to_roman(vol)
                yield f'<tr><td>{exp}{sem}{ref}</td><td>{vol},{col}</td></tr>\n'
        elif form == 'html-xml' and pub_id == 'thesaurus':
            term_re = re.compile(r'^(<small>)?(.*?)<a[^>]*>([^<]*)')
            for item in items['data']:
                sublem, lem, loc = term_re.search(item['0']['_']).group(1, 2, 3)
                sublem = 'sic' if sublem else ''
                lem = lem.replace('</small>', '').strip()
                yield (item['0']['s'], item['DT_RowId'], lem, sublem, loc)
        else:
            return

def make_new_empty_db_if_absent(
        meta_path: str,
        wal_mode: bool,
        ) -> None:
    '''
    Make a new emtpy database at :param:`meta_path`, if none is
    present there or if the one present there is not readable.

    .. important::
        In the latter case, :param:`meta_path` is **replaced**
        by the new empty database.

    :param wal_mode: tells whether the WAL mode (i.e. write ahead logging)
        is used while operating on the SQLite database.

    .. important::
        WAL mode may be impossible e.g. on network drives.
        But non-WAL mode is not usable for production,
        because any write process blocks all read processes
        long enough to make the response fail.
    '''
    def build_db(wal_mode):
        with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
            if wal_mode:
                cur.execute('PRAGMA journal_mode=WAL')
            cur.execute('PRAGMA encoding = "UTF-8"')

            cur.execute(r"""
                    create table if not exists publications (
                        pub_id                TEXT PRIMARY KEY, -- 0
                        pub_catid             TEXT, -- 1
                        title                 TEXT, -- 2
                        title_addition        TEXT, -- 3
                        sorttitle             TEXT, -- 4
                        place                 TEXT, -- 5
                        publisher             TEXT, -- 6
                        year                  TEXT, -- 7
                        normyear              INTEGER, -- 8
                        seriesstring          TEXT, -- 9
                        serial_num            TEXT, -- 10
                        backlink_in_catalog   INTEGER, -- 11
                        personstring          TEXT, -- 12
                        linked_personstring   TEXT, -- 13
                        latest_change_raaftag INTEGER -- 14
                    )""")
            cur.execute(r"""
                    create index if not exists i_normyear
                    on publications (normyear)""")
            cur.execute(r"""
                    create index if not exists i_backlink
                    on publications (backlink_in_catalog)""")
            cur.execute(r"""
                    create table if not exists files (
                        pub_id TEXT REFERENCES publications(pub_id) ON DELETE CASCADE,
                        name   TEXT PRIMARY KEY
                    )""")
            cur.execute(r"""
                    create index if not exists i_pub_id
                    on files (pub_id)""")
            cur.execute(r"""
                    create index if not exists i_name
                    on files (name)""")
            con.commit()
    try:
        build_db(wal_mode)
    except sqlite3.DatabaseError:
        # Maybe the file at ``meta_path`` is not well formed
        # or the WAL mode does not match.
        os.remove(meta_path)
        build_db(wal_mode)
        print(
                'A DatabaseError occurred while building the SQLite db.'\
                ' Maybe the file at {} was not well-formed or the WAL mode'\
                ' ({}) did not match. The file was removed and built anew.'
                .format(meta_path, wal_mode))
