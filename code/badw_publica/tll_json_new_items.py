# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2020 ff. (© http://badw.de)
import json
try: import regex as re
except ImportError: import re
import xml.etree.ElementTree as ET
from html import escape

import __init__
import fs
import parse
import xmlhtml

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    if tag == 'head':
        return ''
    else:
        return None

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    if tag in {'table', 'td', 'tr'}:
        return tag, {}, False
    else:
        return '', {}, False

def main(
        index_path: str,
        index_addition_path: str,
        soffice_exe_path: str,
        ID_ante_quem_inserendum: int,
        default_vol: str,
        ext: str,
        colspan_distance_height: str,
        sep: str,
        bracket_re: 're.Pattern[str]' = re.compile(
            r'(?:\(([^)]*)\)|\[([^\]]*)\]|&lt;((?:(?!&gt;).)*)&gt;)'),
        varsstart: str = '<x->',
        varsend: str = '</x->',
        varseparator: str = '/',
        ) -> 'tuple[str, int]':
    '''
    - Read :param:`index_path` as an index of lemmata that is apt to be put into
      a datatable.
    - From the index, get the greatest ID and the position of the first entry of
      the onomastic part.
    - Read :param:`index_addition_path` as a spreadsheet of new lemmata. It must
      have the following form:

        ====== ======== ==== ======
        Lemma  Sublemma Band Spalte
        ====== ======== ==== ======
        reopto          11,2   1124
        reor            11,2   1124
               ratus    11,2   1130
               rata     11,2   1134
        …      …           …      …

    - From the spreadsheet, collect the new lemmata, convert them to the gestalt
      needed for the datatable (e.g. add an ID) and add them to the index.
    - Renumber each lemmaʼs number used for sorting.
    - Write the result into a new JSON file.

    :param ID_ante_quem_inserendum: ID of the lemma before which the new lemmata
        have to be inserted. Often, this is the first lemma of the onomata, i.e.
        the Lemma “C” with the ID ``76228``.
    :param default_vol: Volume to be assumed if not given in the xlsx file, e.g.
        ``'11,2'``.
    :param ext: File extension of the images of the pages.
    :param colspan_distance_height: for the onclick attribute, given in the form
        e.g. ``',1,2,1380'``.
    :param sep: Separator between volume and column (for the button label), e.g.
        ``':'``.
    '''
    index = json.loads(fs.read(index_path, None))
    lemmata = index['data']
    greatest_ID = 0
    for i in range(len(lemmata)):
        current_ID = lemmata[i]['DT_RowId']
        if current_ID == ID_ante_quem_inserendum:
            onoma_startpos = i
        if current_ID > greatest_ID:
            greatest_ID = current_ID
    index_addition = fs.read(
            fs.convert(index_addition_path, soffice_exe_path, '.html')[0],
            None,
            )
    index_addition = xmlhtml.replace(
            index_addition.decode(xmlhtml.get_charset(index_addition)
                ).replace('\r\n', '\n'),
            replace_element = replace_element,
            replace_tag = replace_tag,
            ).strip()
    root = ET.XML(index_addition)
    for pos, tr in enumerate(root.iterfind('./tr')):
        lem, sublem, vol, col = [
                '' if (elem := tr.find(f'./td[{pos}]')) is None
                else (elem.text or '').strip()
                for pos in range(1, 5)
                ]
        if pos < 2 and lem.lower().strip() == 'lemma':
            continue
        if not vol:
            assert default_vol
            vol = default_vol
        col = int(col)
        refcol = col if (col % 2) else (col - 1)
        if sublem:
            term = sublem
            is_sublem = True
        else:
            term = lem
            is_sublem = False
        term = re.sub(r'\s+', ' ', term)
        term = escape(term)
        ordinal, term = parse.extract(r'^([\d.]+\s+)', term)
        vs = sorted(parse.expand_brackets(term, bracket_re))
        term = ordinal + term
        if vs:
            varsstring = varseparator.join(vs)
            term = f'{term}{varsstart}{varseparator}{varsstring}{varsend}'
        if is_sublem:
            term = f'<small>{term}</small>'
        term = f'''{term} <a onclick=\"rI(event,'-/{vol}.{refcol}{ext}'{colspan_distance_height})\">{vol}{sep}{col}</a>'''
        greatest_ID += 1
        lemmata.insert(
                onoma_startpos,
                {
                    "0": {
                        "_": term,
                        "s": 0,
                        },
                    "DT_RowId": greatest_ID,
                })
        onoma_startpos += 1
    for i in range(len(lemmata)):
        lemmata[i]['0']['s'] = i + 1 # start at 1
    doc = json.dumps({'data': lemmata}, separators = (',', ':')).strip()
    for old, new in (
            (r'\{"data":\[\{', '{"data":[\n{'),
            (r'^\[\{', '{'),
            (r'\}\]$', '},\n'),
            (r'\},\{', '},\n{'),
            ):
        doc = re.sub(old, new, doc)
    return fs.write_new(doc, index_path)

if __name__ == '__main__':
    print(
            'I made a new, extended index:',
            main(
                r"Z:\ver\Ablage2013\6 IT\6.1 Digitalisierung\6.1.13 Publikationsserver\publikationen\thesaurus\index.json",
                r"Z:\ver\Ablage2013\6 IT\6.1 Digitalisierung\6.1.13 Publikationsserver\publikationen_neu\N4.xlsx",
                r'C:\Program Files\LibreOffice\program\soffice.exe',
                61298, # 76228,
                '9,1',
                '.pdf',
                ',1,2,1380',
                ':',
                ))
