# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Collection of bibliographical metadata
for the publication server of the Bavarian Academy of Sciences.
'''
import json
import os
try: import regex as re
except ImportError: import re
import sqlite3
import sys
import tempfile
import time
import xml.etree.ElementTree as ET
from collections import deque
from operator import itemgetter
from urllib.parse import quote, unquote, urlparse

import gehalt
import __init__
import catalog_io
import fs
import mail
import sql_io
import sys_io
import xmlhtml
from parse import sortday, sortnum, sortxtext
from xmlhtml import escape

child = ET.SubElement

def collect_archive(paths: 'dict[str, str]') -> None:
    '''
    About this function see the docstring of :func:`main`.

    Attribution notice: by Theresa Lang, Stefan Müller, Eckhart Arnold
    in 2022 ff. (© http://badw.de)
    '''
    def fit(term: str) -> str:
        return escape(term.strip().strip(',').strip()).replace('\n', '<br/>')

    nss = {'h1': 'http://www.startext.de/HiDA/DefService/XMLSchema'}
    for key, value in nss.items():
        ET.register_namespace(key, value)
    dirpath = os.path.abspath(paths['archive_urdata_dir']) + os.sep
    names = sorted(
            name for name in os.listdir(dirpath)
            if name.lower().endswith('.xml')
            )
    if names:
        # There is at least one new (i.e. unprocessed) file; take the last one.
        path = dirpath + names[-1]
    else:
        return
    root = ET.parse(path).getroot()
    acts = []
    for elem in root.iterfind('.//h1:Document/h1:Block[@Type="Vz"]/..', nss):
        subelem = elem.find(
                './h1:Block/h1:Field[@Type="Sperr_Unbestellbar_Bis"]', nss
                )
        if subelem is not None:
            blocking_end = subelem.attrib.get('ValueMax', '').strip()
            if blocking_end:
                try:
                    blocking_end = time.strptime(blocking_end, '%d.%m.%Y')
                    if blocking_end >= time.localtime():
                        continue
                except Exception: # Sic.
                    continue
        act = []
        subelem = elem.find(
                './h1:Block/h1:Field[@Type="Vz_Bestellsignatur"]', nss)
        act.append(fit(subelem.attrib['Value'].split("),", 1)[-1]))
        for xpath in (
                './h1:Block/h1:Field[@Type="St_Titel"]',
                './h1:Block/h1:Field[@Type="St_Entha"]',
                ):
            subelem = elem.find(xpath, nss)
            if subelem is None:
                act.append('')
            else:
                act.append(fit(subelem.attrib['value_plain']))
        subelem = elem.find('./h1:Block/h1:Field[@Type="Laufzeit"]', nss)
        if subelem is None:
            act.append('')
        else:
            date_min = fit(subelem.attrib.get('ValueMin', ''))
            date_max = fit(subelem.attrib.get('ValueMax', ''))
            if date_min and date_max and (date_min != date_max):
                act.append(f'{date_min}​–​{date_max}')
            elif date_min:
                act.append(date_min)
            else:
                act.append(date_max)
        parents = deque()
        parent_id = ''
        while parent_id != 'Tekt    1a0e552c-b6a9-4419-b275-656720c7c3c3':
            parents.appendleft(fit(elem.attrib['DocTitle']))
            subelem = elem.find(
                    './h1:Block/h1:Field/h1:Field/[@Type="Ref_DocKey"]',
                    nss)
            parent_id = subelem.attrib['Value']
            elem = root.find(f'.//h1:Document[@DocKey="{parent_id}"]', nss)
        if not (
                act[0] == parents[-1]
                or (
                    act[0] in parents[-1] and
                    parents[-1].startswith(act[0])
                )):
            parents.append(f'/{act[0]}')
        act[0] = ' '.join(
                f'<p style="margin-left: {1 * i}em">{parent}</p>'
                for i, parent in enumerate(parents)
                ).replace('ABAdW ', ''
                ).replace(' style="margin-left: 0em"', '')
        acts.append(act)
    doc = json.dumps({
            'data':
                [
                    {
                        '0':
                            {
                                '_': signature, 's': sortxtext(signature),
                            },
                        '1':
                            {
                                '_': subject, 's': sortxtext(subject),
                            },
                        '2': content,
                        '3':
                            {
                                '_': date, 's': sortxtext(date),
                            },
                    } for signature, subject, content, date in acts
                ]
            }, ensure_ascii = False, separators = (',', ':'))
    with tempfile.NamedTemporaryFile(
              'w',
              encoding = 'utf-8',
              dir = os.path.dirname(paths['archive_index']),
              delete = False,
              ) as file:
        file.write(doc)
        tempname = file.name
    os.replace(tempname, paths['archive_index'])
    today = time.strftime('%Y-%m-%d')
    os.replace(path, f'{path}.{today}')

def delete_items_without_files(
        meta_path: str,
        repros_path: str,
        temp_meta_dirpath: str,
        filename_re: 're.Pattern[str]',
        ) -> None:
    '''
    Delete any bibliographical item and any filename item in the database
    at :param:`meta_path` as well as any file in :param:`temp_meta_dirpath`,
    for which no corresponding repro file in :param:`repros_path` exists.
    '''
    names = set()
    pub_ids = set()
    for name in os.listdir(repros_path):
        match = filename_re.search(name)
        if match:
            names.add(name)
            pub_ids.add(match.group('pub_id'))
    with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
        cur.execute("select pub_id from publications")
        for (pub_id,) in tuple(cur.fetchall()):
            if pub_id not in pub_ids:
                cur.execute(
                        "delete from publications where pub_id = ?", (pub_id,))
                con.commit()
        cur.execute("select pub_id, name from files")
        for (pub_id, name) in tuple(cur.fetchall()):
            if name not in names:
                cur.execute("delete from files where name = ?", (name,))
                con.commit()
                try:
                    os.remove(os.path.join(temp_meta_dirpath, pub_id) + '.xml')
                except: # Sic.
                    pass
        try:
            cur.execute("vacuum")
            con.commit()
        except: # Sic.
            pass

def get_pub_item(
        config: 'fs.ConfigParser',
        catalog: 'catalog_io.Catalog',
        pub_id: str,
        pub_catid: str,
        ) -> '''
        tuple[
            None|ET.Element,
            list[tuple[str, str, str, str, str]],
            tuple[str, str, str, str, str, str, str, str, int, str, str, int]
            ]''':
    '''
    Get bibliographical information about the publication :param:`pub_id` from a
    library catalog :param:`catalog`. In the catalog, the publication has the ID
    :param:`pub_catid`. Return:

    - the element as it is received from the catalog.
    - the person-related information extracted from the element.
    - the other information extracted from the element.
    '''
    item = catalog.read(
            catalog_io.SRU |
            {'query': config['catalog']['query_key'] + '=' + pub_catid})
    if item is None or not catalog.get_pub_id(item):
        return None, [], ()
    persons = list(catalog.get_personal_names_ids_roles(item))
    title, sorttitle = catalog_io.format_title_sorttitle(
            *catalog.get_titles(item),
            substitutes = tuple(config['catalog'].getjson('substitutes')))
    place = catalog.get_place(item)
    publisher = catalog.get_publisher(item)
    year = catalog.get_year(item)
    normyear = int(m.group()) if (m := re.search(r'\d\d\d\d+', year)) else None
    series = catalog.get_series_id_serial_num(item)
    series = {
            series_id: series_num
            for series_values in series.values()
            for series_id, series_num in series_values }
    # If there is no series_id at all, take the pub_id for it.
    if not series:
        series = {pub_id: ''}
    # Look for a seriesstring first in the order given in the configuration
    # (which is preserved because configparser uses an ordered dictionary).
    for series_id, seriesstring in config['series_titles'].items():
        if series_id in series:
            serial_num = series[series_id]
            break
    else:
        # If no matching series ID is found in the configuration:
        seriesstring = serial_num = ''
    title_addition = catalog.get_title_addition(item)
    backlink_in_catalog = config['ids']['url'].split('//')[-1] in {
            urlparse(url).netloc for url in catalog.get_urls(item) }
    return item, persons, (
            pub_id,
            pub_catid,
            title,
            title_addition,
            sorttitle,
            place,
            publisher,
            year,
            normyear,
            seriesstring,
            serial_num,
            backlink_in_catalog,
            )

def get_personstrings(
        config: 'fs.ConfigParser',
        persons: 'Iterable[tuple[str, str, str, str, str]]',
        annotated: bool = True,
        ) -> 'tuple[str, str]':
    '''
    Combine the information in :param:`persons` to two strings:

    - a string which gives authorship information and – if :param:`annotated` is
      ``True`` – is styled with HTML tags.
    - the same with a link derived from the GND, if present.
    '''
    names = []
    linked_names = []
    seen_persons = set()
    for person in persons:
        if person[:-1] in seen_persons: # Equality apart from the role.
            continue
        seen_persons.add(person[:-1])
        if annotated:
            person = tuple(map(escape, person))
        name, name_enum, name_add, person_id, role = person
        name, *rest = name.split(',', 1)
        rest = ',' + rest[0] if rest else ''
        name = (f'<b>{name}</b>' if annotated else name) + rest
        name = name.replace(', ', ',\u202F') # narrow no-break space
        name = name.replace(' ', '\u2009') # thin space
        if name_enum:
            name += '\u202F' + name_enum
        if name_add:
            name += f' ({name_add})'
        role = ( # Cf. publica/app/data/config.ini sub voce “[roles]”.
                config['roles'].get(role, '').split(None, 2) + ['0', '0', '0']
                )[2]
        if role == '0' or not name:
            continue
        elif role == '1':
            role = ''
        else:
            role = f' [{role}]'
        name_role = f'{name}{role}'
        names.append(name_role)
        if annotated and person_id:
            linked_name = '{0} '\
                '<a class="key" href="https://d-nb.info/gnd/{1}">DNB</a>'\
                .format(name_role, person_id)
        else:
            linked_name = name_role
        linked_names.append(linked_name)
    joiner = '; <br/>' if annotated else ' ; '
    return (joiner.join(names), '; <br/>'.join(linked_names))

def make_new_json_file(meta_path: str, json_path: str) -> None:
    '''
    Make a new json file caching the metadata so that they can directly be
    fed into the table of publications shown on the website.
    Extract the data from the database at :param:`meta_path` and store the
    json file at :param:`json_path`.
    '''
    retention_note = '''<br/> – <i>
<l- l="de">Noch in Verlagsschutzfrist bis:</l->
<l- l="en">Still within the period of publisher protection until:</l->
{}</i>'''
    with tempfile.NamedTemporaryFile(
              'w',
              encoding = 'utf-8',
              dir = os.path.dirname(json_path),
              delete = False,
              ) as file:
        with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
            doc: str = json.dumps({'data': [
                    {
                        '0': {
                            '_': year,
                            's': normyear,
                            },
                        '1': seriesstring,
                        '2': {
                            '_': serial_num,
                            's': sortnum(serial_num),
                            },
                        '3': personstring,
                        '4': {
                            '_': '<a href="../{}" class="key">{}</a>'.format(
                                pub_id,
                                title + retention_note.format(retention_end)
                                    if retention_end
                                else title),
                            's': sorttitle,
                            },
                    }
                    for (
                        pub_id,
                        pub_catid,
                        title,
                        title_addition,
                        sorttitle,
                        place,
                        publisher,
                        year,
                        normyear,
                        seriesstring,
                        serial_num,
                        backlink_in_catalog,
                        personstring,
                        linked_personstring,
                        latest_change_raaftag,
                        retention_end,
                        )
                    in cur.execute("""
                        select
                            publications.*,
                            (
                                select
                                    substr(
                                        files.name,
                                        2,
                                        instr(substr(files.name, 2), '#') - 1
                                        )
                                from files
                                where files.pub_id = publications.pub_id
                                and files.name like '#%'
                                limit 1
                            )
                        from publications
                        where publications.pub_catid <> ''
                        order by normyear DESC, seriesstring ASC
                        """)
                    ]}, separators=(',', ':'))
            file.write(doc)
            tempname = file.name
    # The tempfile must have been closed before:
    os.replace(tempname, json_path)

def make_temp_meta_file(
        config: 'fs.ConfigParser',
        temp_meta_dirpath: str,
        full_item: 'ET.Element',
        pub_item: 'Tuple',
        persons: 'Iterable[tuple[str, str, str, str, str]]',
        lic: str,
        role776: str = 'Reproduktion von',
        ) -> None:
    '''
    Save in :param:`temp_meta_dirpath` an XML fragment made from the information
    given with :param:`full_item`, :param:`pub_item`, :param:`persons` – so that
    the requirements by the DNB for harvesting our publications with OAI PMH are
    met.

    :param lic: is the license.
    :param role776: value to be inserted in subfield i of MARC field 776, if the
        publication is a retro-digitization. If not, the field is skipped.
    '''
    (
        pub_id,
        pub_catid,
        title,
        title_addition,
        sorttitle,
        place,
        publisher,
        year,
        normyear,
        seriesstring,
        serial_num,
        backlink_in_catalog,
    ) = pub_item
    normyear = str(normyear)
    name_encoded = quote(pub_id)
    is_retro = pub_id[0].isdigit()
    medium = 'a' # Text. Everything is text.
    status = 'm' # Monographie
    leader = xmlhtml.get_text(
            full_item, './/marc:record/marc:leader',
            namespaces = {'marc': config['catalog']['xmlns_default']})
    leader = (
            leader[:6] + medium + status +
            leader[8:19] + ' ' +
            leader[20:])
    marc007 = 'cr|||||' # c: Elektronische Ressource; r: Fernzugriff.
    marc008 = xmlhtml.get_text(full_item, './/*[@tag="008"]')
    if is_retro:
        old_year = marc008[7:11]
        marc008 = marc008[:6] + 'r' + normyear + old_year + marc008[15:]
    else:
        # The following should just yield marc008 again, but we need
        # consistency with the field 264, subfield c in any case.
        marc008 = marc008[:6] + 's' + normyear + '||||' + marc008[15:]
    record = ET.Element('record')
    record.set('xmlns', config['catalog']['xmlns_default'])
    child(record, 'leader').text = leader
    child(record, 'controlfield', {'tag': '007'}).text = marc007
    child(record, 'controlfield', {'tag': '008'}).text = marc008
    e = child(record, 'datafield', {'tag': '856', 'ind1': '4', 'ind2': '0'})
    child(e, 'subfield', {'code': 'u'}).text = \
            config['ids']['url'].rstrip('/') + '/file/' + name_encoded
    child(e, 'subfield', {'code': 'x'}).text = 'Transfer-URL'
    e = child(record, 'datafield', {'tag': '540', 'ind1': ' ', 'ind2': ' '})
    if lic.startswith('CC BY'):
        child(e, 'subfield', {'code': 'f'}).text =\
                lic.lower().replace(' ', '-') + '-4.0.de'
        e.append(ET.Comment('The DNB required this spelling of the license.'))
        child(e, 'subfield', {'code': 'u'}).text = \
                'https://creativecommons.org/licenses/{}/4.0/legalcode.de'\
                .format(quote(lic.split(None, 1)[-1].lower()))
    else:
        child(e, 'subfield', {'code': 'a'}).text = lic
    e = child(record, 'datafield', {'tag': '264', 'ind1': ' ', 'ind2': '1'})
    child(e, 'subfield', {'code': 'a'}).text = config['publisher']['place']
    child(e, 'subfield', {'code': 'b'}).text = config['publisher']['name']
    child(e, 'subfield', {'code': 'c'}).text = year
    if is_retro:
        e = child(record, 'datafield', {'tag':'776','ind1':' ','ind2':' '})
        personstring = get_personstrings(config, persons[:1], False)[0]
        child(e, 'subfield', {'code': 'a'}).text = personstring
        e.append(ET.Comment(
                'The DNB required to name not more than one person here.'))
        child(e, 'subfield', {'code': 'd'}).text = ', '.join(
                (place, publisher, year))
        child(e, 'subfield', {'code': 'i'}).text = role776
        child(e, 'subfield', {'code': 't'}).text = title
    serial_num_addition = ''
    for num in (
            '041', # Sprachcode
            '082', # Notation nach der Dewey-Decimal-Classification
            '084', # Andere Notation (Klassifikationen / Thesauri)
            '100', # Personenname
            '110', # Körperschaftsname
            '245', # Titelangabe
            '246', # Titelvarianten
            '502', # Dissertationsvermerk
            '650', # Nebeneintragung unter einem Schlagwort - Sachschlagwort
            '653', # Indexierungsterm - nicht normiert
            '700', # Nebeneintragung - Personenname
            '710', # Nebeneintragung - Körperschaftsname
            ):
        for e in full_item.iterfind(f'.//*[@tag="{num}"]'):
            xmlhtml.strip_namespaces(e)
            record.append(e)
            if seriesstring and num == '245':
                for ee in e.findall('subfield'):
                    codeattr = ee.get('code', '')
                    if codeattr in {'n', 'p'}:
                        if codeattr == 'p':
                            serial_num_addition = ee.text or ''
                        e.remove(ee)
                        e.append(ET.Comment(
                                'The DNB required to delete a subfield here.'))
    if seriesstring:
        e = child(record, 'datafield', {'tag': '490', 'ind1': '0', 'ind2': ' '})
        child(e, 'subfield', {'code': 'a'}).text = seriesstring
        if serial_num_addition:
            serial_num += ', ' + serial_num_addition
        child(e, 'subfield', {'code': 'v'}).text = serial_num
        if serial_num_addition:
            e.append(ET.Comment(
                    'The DNB required this addition to the serial number.'))
    e = child(record, 'datafield', {'tag': '093', 'ind1': '0', 'ind2': ' '})
    if lic == 'CC BY':
        child(e, 'subfield', {'code': 'b'}).text = 'b'
    else:
        child(e, 'subfield', {'code': 'b'}).text = 'a'
        e.append(ET.Comment('Access restriction on behalf of publisher.'))
    e = child(record, 'datafield', {'tag': '856', 'ind1': '4', 'ind2': ' '})
    child(e, 'subfield', {'code': 'u'}
            ).text = f'http://publikationen.badw.de/de/{pub_id}'
    xmlhtml.indent(record)
    doc = ET.tostring(record, encoding = 'unicode').strip()
    doc = f'''<record>
<header>
<identifier>badw:{name_encoded}</identifier>
<datestamp></datestamp>
</header>
<metadata>
{doc}
</metadata>
</record>'''
    # “<datestamp>” will be filled from the database whenever the XML file is to
    # be delivered. Because here and now we can only know the date of the latest
    # change of the file corresponding to ``pub_item`` – but not the date of the
    # latest change among all files that have the same ``pub_id`` (i.e. when the
    # publication consists of more than one file).
    fs.pave_dir(temp_meta_dirpath)
    with tempfile.NamedTemporaryFile(
              'w',
              dir = temp_meta_dirpath,
              delete = False,
              encoding = 'utf-8',
              ) as file:
        file.write(doc)
        name = file.name
    os.replace(name, os.path.join(temp_meta_dirpath, pub_id) + '.xml')

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Refresh the temporary database and the temporary files which contain all the
    metadata for the table of publications, for any page dedicated to a specific
    publication and for any APIs, e.g. for the submission to the DNB.

    - Read the configuration given in :param:`config_path`.
    - Once in a while (specified in the configuration sub voce “timing”) process
      an export from the ABADW archive: look into the directory specified in the
      configuration sub voce “archive_urdata_dir”, get the alphanumerically last
      file ending with “.xml”, process it, turn the result into JSON ready to be
      rendered as a datatable, and save the JSON then at the path found sub voce
      “archive_index” in the configuration. Finally, rename the XML file (append
      the current date to its name), and store it as a backup.
    - Delete any item (and its affiliated items) which hasnʼt a publication file
      in the folder containing all publication files.
    - Refresh the JSON file feeding the table of publications.
    - Once in a while (specified in the configuration sub voce “timing”) consult
      the library catalog and refresh the database and the XML files:

      - Exit, if no process with :param`pid` is running anymore.
      - For any internal publication ID (used in the permalink), get the catalog
        ID that belongs to it.
      - Get the catalog data for this item.
      - If the catalog does not return data for this item:

        - The catalog may be unavailable at the moment. Skip this ID; try later.
          But also make a log item, which enables the second step:
        - The catalog ID may have been deleted – it appears in the log for every
          time. Human intervention is required to find the new catalog ID and to
          adapt the “id_map” in :param`config_path` accordingly.

      - Update the database with the new catalog data.
      - Make an XML file of it for the OAI interface, but only if itʼs no longer
        in retention and if it is a PDF (or a series of PDFs).

    - Check the end of retention, if any is given in the filename. When the date
      has passed, remove it from the filename and from the database.
    - Refresh the JSON file again.
    - Sleep a while.
    - Exit, if no process with :param`pid` is running anymore.
    - Start anew.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {
            'paths_from_program_folder': __file__,
            'paths_from_config_folder': urdata[0],
            })
    json_path = paths['json']
    meta_path = paths['meta']
    repros_path = paths['repros']
    temp_meta_dirpath = paths['temp_meta']
    filename_re = re.compile(config['filenames']['pattern'])
    end_re = re.compile(config['filenames']['remove_end'])
    id_map = config['id_map']
    catalog = catalog_io.Catalog(config['catalog']['query_url'])
    seconds_between_archive_update = int(
            config['timing']['seconds_between_archive_update'])
    seconds_between_complete_update = int(
            config['timing']['seconds_between_complete_update'])
    seconds_between_complete_update_with_report = int(
            config['timing']['seconds_between_complete_update_with_report'])
    # The first loop is done with all updates and reports:
    end_of_archive_update = end_of_complete_update = end_of_report = time.time()
    is_archive_update = True
    is_complete_update = True
    does_report = True
    while True:
        now = time.time()
        if seconds_between_archive_update < (now - end_of_archive_update):
            is_archive_update = True
        if seconds_between_complete_update < (now - end_of_complete_update):
            is_complete_update = True
        if seconds_between_complete_update_with_report < (now - end_of_report):
            is_complete_update = True
            does_report = True
        if is_archive_update:
            log.write('─ START processing the archive. ─')
            collect_archive(paths)
            log.write('─ END processing the archive. ─')
            end_of_archive_update = time.time()
            is_archive_update = False
        if is_complete_update:
            log.write('─ START updating all items. ─')
        delete_items_without_files(
                meta_path, repros_path, temp_meta_dirpath, filename_re)
        make_new_json_file(meta_path, json_path)
        with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
            cur.execute("select name from files")
            filenames_in_db = frozenset(map(itemgetter(0), cur.fetchall()))
        seen_pub_ids = set()
        for nth, name in enumerate(os.listdir(repros_path)):
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            time.sleep(0.1)
            if name in filenames_in_db and not is_complete_update:
                continue
            path = os.path.join(repros_path, name)
            if not os.path.isfile(path):
                continue
            match = filename_re.search(name)
            if not match:
                continue
            form = match.group('form')
            if not form:
                # It is a subfolder containing e.g. images of a publication.
                continue
            pub_id = match.group('pub_id') or ''
            part_title = match.group('part') or ''
            lic = match.group('lic') or 'CC BY'
            retention_end = match.group('end') or ''
            pub_catid = id_map.get(
                    pub_id,
                    config['catalog']['catid_template'].format(pub_id = pub_id))
            if not re.search(config['catalog']['catid_re'], pub_catid):
                continue
            full_item, persons, pub_item = get_pub_item(
                    config, catalog, pub_id, pub_catid)
            if not pub_item:
                log.write(f'{pub_id}: No catalog-reply to: {pub_catid}')
                continue # Keep the existing item if the catalog does not reply.
            if retention_end:
                try:
                    retention_endtime = time.strptime(retention_end, '%Y-%m-%d')
                except ValueError:
                    # ``retention_end`` cannot be understood as real date.
                    continue
                else:
                    if time.localtime() > retention_endtime:
                        retention_end = ''
                        with sql_io.DBCon(
                                sqlite3.connect(meta_path)) as (con, cur):
                            cur.execute(
                                    "delete from files where name = ?", (name,))
                            con.commit()
                        name = end_re.sub('', name)
                        new_path = os.path.join(repros_path, name)
                        os.replace(path, new_path)
                        path = new_path
                        make_new_json_file(meta_path, json_path)
            if (
                    pub_id not in seen_pub_ids
                    and pub_catid
                    and not retention_end
                    and form.rsplit('.', 1)[-1] == 'pdf'
                    ):
                seen_pub_ids.add(pub_id)
                make_temp_meta_file(
                        config,
                        temp_meta_dirpath,
                        full_item,
                        pub_item,
                        persons,
                        lic,
                        )
            personstring, linked_personstring = get_personstrings(
                    config, persons)
            pub_item = tuple(
                    escape(term) if isinstance(term, str)
                    else term
                    for term in pub_item )
            with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
                cur.execute(r"""
                        select latest_change_raaftag
                        from publications where pub_id = ?
                        """, (pub_id,))
                latest_change_raaftag = max((
                        sortday(os.stat(path).st_mtime),
                        (cur.fetchone() or [0])[0],
                        ))
                cur.execute(r"""
                        insert or replace into publications values
                        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                        """,
                            pub_item + (
                                personstring,
                                linked_personstring,
                                latest_change_raaftag,
                                )
                        )
                cur.execute(r"""
                        insert or replace into files values
                        (?, ?)
                        """, (pub_id, name))
                con.commit()
            if nth % 20 == 0: # Renew the json file after every 20th change.
                make_new_json_file(meta_path, json_path)
        if nth % 20 != 0: # Renew the json file, if not done just before.
            make_new_json_file(meta_path, json_path)
        if is_complete_update:
            log.write('─ END updating all items. ─')
            end_of_complete_update = time.time()
            is_complete_update = False
        if does_report:
            with sql_io.DBCon(sqlite3.connect(meta_path)) as (con, cur):
                cur.execute(r"""
                        select
                        pub_id, pub_catid, title, seriesstring, serial_num
                        from publications where backlink_in_catalog <> 1
                        """)
                no_backlink_items = cur.fetchall()
                if no_backlink_items:
                    text = '\n\n'.join(
                            f"<{config['ids']['url']}/de/{item[0]}>\n"\
                            f"<{config['catalog']['url']}/{item[1]}>\n"\
                            f"“{item[2]}” · {item[3]} {item[4]}"
                            for item in no_backlink_items )
                    try:
                        mail.send(
                                host = config['mail']['smtp_host'],
                                port = int(config['mail']['smtp_port']),
                                From = config['mail']['sender'],
                                To = ', '.join(
                                    config['mail']['recipients_lib'].split()),
                                Subject = config['mail']['title_lib'],
                                text = text,
                                )
                    except ConnectionRefusedError:
                        log.write(f'ConnectionRefusedError\n{text}\n██')
            end_of_report = time.time()
            does_report = False

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
