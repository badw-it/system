# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import sys
import time

import fitz

import __init__
import fs
import sys_io

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Make the PDFs tidier (remove metadata and redundancies) as well as linear
    (which might increase the file size a bit but enables partial loading and
    rendering, i.e. accelerates the rendering in a browser or so).

    - Read the configuration given in :param:`config_path`.
    - Cycle through all PDFs (in the folder specified by the configuration):

      - Exit, if no process with :param`pid` is running anymore.
      - If there is a certain mark in the metadata, skip the belonging file –
        this mark is taken from the config: ``config['ids']['pdf_sign'].
      - If not, save a new tidied and linear version of the file. It is saved
        gradually and hence elsewhere. When completely saved, it is moved and
        replaces the old version.

    - Sleep a while.
    - Exit, if no process with :param`pid` is running anymore.
    - Start anew.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stdout = log
    sys.stderr = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {
            'paths_from_program_folder': __file__,
            'paths_from_config_folder': urdata[0],
            })
    repros_path = paths['repros'] + os.sep
    temp_path = repros_path + '__temp__' + os.sep
    filename_re = re.compile(config['filenames']['pattern'])
    refresh_pause = 36000
    sign = config['ids']['pdf_sign']
    while True:
        for name in os.listdir(repros_path):
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            time.sleep(1)
            match = filename_re.search(name)
            if not match:
                continue
            if match.group('form').rsplit('.', 1)[-1] != 'pdf':
                continue
            try:
                doc = fitz.open(repros_path + name)
                metadata = doc.metadata
                metadata_new = {
                        k: '' for k, v in doc.metadata.items()
                        if k not in {'format', 'encryption'} }
                metadata_new['producer'] = sign
                metadata_new['title'] = 'BAdW · ' + match.group('pub_id')
                if metadata_new != metadata:
                    doc.del_xml_metadata()
                    doc.set_metadata(metadata)
                    doc.save(
                            fs.pave(temp_path + name),
                            garbage = 4,
                            clean = False, # If ``True``, corruption may occur!
                            deflate = True,
                            deflate_images = True,
                            deflate_fonts = True,
                            linear = False, # as download is more likely than view.
                            permissions = -1,
                            )
                doc.close()
                if os.path.getsize(temp_path + name) == 0:
                    # That may happen after some kind of semi-error.
                    # Do not change the original file, remove the new one.
                    os.remove(temp_path + name)
                else:
                    os.replace(temp_path + name, repros_path + name)
            except Exception as e:
                doc.close()
                print(repros_path + name, '█', e)
        end = time.time() + refresh_pause
        while time.time() < end:
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            time.sleep(4)
            # This loop is done instead of e.g. time.sleep(refresh_pause)
            # in order to avoid blocking and enable cancelling with ctrl-c.

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
