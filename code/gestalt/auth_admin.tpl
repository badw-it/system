% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
% lang_id = kwargs.get('lang_id', db.lang_id)
<main>
<article class="card">
	<h1>{{db.glosses['access_admin'][lang_id]}}</h1>
  % if request.query.note:
	<p><strong>{{db.glosses['error'][lang_id]}}:</strong></p>
	<pre>{{request.query.note}}</pre>
	<p><strong>{{db.glosses['no_changes'][lang_id]}}</strong></p>
  % end
	<form class="form_admin" action="auth_admin" method="post">
		<table style="hyphens: auto; word-wrap: break-word">
			<thead>
				<tr>
					<th rowspan="2">Name</th>
					<th colspan="4" style="width: 40%">{{db.glosses['role'][lang_id]}}</th>
					<th colspan="2">{{db.glosses['password_change'][lang_id]}}</th>
				  % if 'administrator' in request.roles:
					<th rowspan="2" style="width: 4.4em">{{db.glosses['delete'][lang_id]}}</th>
				  % end
				</tr>
				<tr style="font-size: smaller">
					<th>Inspector</th>
					<th>Inspector <br/>Editor</th>
					<th>Inspector <br/>Editor <br/>Redactor</th>
					<th>Inspector <br/>Editor <br/>Redactor <br/>Administrator</th>
					<th>{{db.glosses['new'][lang_id]}}</th>
					<th>{{db.glosses['new_repeated'][lang_id]}}</th>
				</tr>
			</thead>
			<tbody>
			  % for user_id, user in sorted(db.auth_data.items(), key = lambda item: item[1]['name']):
				% if 'administrator' not in request.roles and user_id != request.user_id:
				  % continue
				% end
				% roles_num = len(user['roles'])
				<tr>
					<td><input name="{{user_id}}-name" type="text" value="{{user['name']}}"{{!'' if 'administrator' in request.roles else ' disabled=""'}}/></td>
					<td><input name="{{user_id}}-roles" type="radio" value="i"{{!' checked=""' if roles_num == 1 else ''}}{{!'' if 'administrator' in request.roles else ' disabled=""'}}/></td>
					<td><input name="{{user_id}}-roles" type="radio" value="ie"{{!' checked=""' if roles_num == 2 else ''}}{{!'' if 'administrator' in request.roles else ' disabled=""'}}/></td>
					<td><input name="{{user_id}}-roles" type="radio" value="ier"{{!' checked=""' if roles_num == 3 else ''}}{{!'' if 'administrator' in request.roles else ' disabled=""'}}/></td>
					<td><input name="{{user_id}}-roles" type="radio" value="iera"{{!' checked=""' if roles_num == 4 else ''}}{{!'' if 'administrator' in request.roles else ' disabled=""'}}/></td>
					<td><input name="{{user_id}}-password" type="password" value=""/></td>
					<td><input name="{{user_id}}-password_repeated" type="password" value=""/></td>
				  % if 'administrator' in request.roles and user_id != request.user_id:
					<td><input name="{{user_id}}-delete" type="checkbox" value="delete"/></td>
				  % end
				</tr>
			  % end
			  % if 'administrator' in request.roles:
				<tr>
					<td><button type="button" onclick="addBlockUser(this)">＋</button></td>
					<td colspan="7"></td>
				</tr>
			  % end
			</tbody>
		</table>
		<p style="text-align: end"><button type="submit">Speichern</button></p>
	</form>
</article>
</main>
