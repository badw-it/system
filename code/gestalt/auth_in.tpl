% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="sheet just wide">
	<h1>{{db.glosses['login'][lang_id]}}</h1>
	<form action="{{request.path}}" method="post">
		<input type="hidden" name="query" value="{{request.query_string}}"/>
	  % url = request.query.url
	  % if url:
		<input type="hidden" name="url" value="{{url}}"/>
	  % end
		<div class="flextable">
			<label class="card">
				<div style="width: 8rem">{{db.glosses['username'][lang_id]}}:</div>
				<input type="text" name="user_name" aria-label="{{db.glosses['username'][lang_id]}}" autofocus=""/>
			</label>
			<label class="card">
				<div style="width: 8rem">{{db.glosses['password'][lang_id]}}:</div>
				<input type="password" name="password" aria-label="{{db.glosses['password'][lang_id]}}"/>
			</label>
		</div>
		<p><button type="submit">{{db.glosses['login'][lang_id]}}</button></p>
	</form>
	<p>{{db.glosses['data_protection_session_cookie'][lang_id]}}</p>
	<p class="p petit">O die Ihr Eingang heischt – tutʼs ohne Scheu.
	<br/>Denn wir verfolgen Eure Spuren nicht,
	<br/>wir speichern nichts, zu wissen, wer Ihr seid,
	<br/>und lüpfen nicht des Nutzernamens Larve.
	<br/>Ja, meldet Ihr Euch ab von dieser Seite,
	<br/>erlischt bei uns der Abdruck Eures Siegels
	<br/>und schwindet hin die Spur von Eurem Hiersein,
	<br/>wie Gischt im Wind verweht. So willʼs das Recht.
	</p>
</article>
</main>
