% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs.get('lang_id', db.lang_id)
<main>
<article class="just sheet wide">
	<h1>{{db.glosses['database_tables'][lang_id]}}</h1>
	<ul>
	  % for table in kwargs['tables']:
		<li><a class="key" href="/db/{{table['TABLE_NAME']}}" rel="noopener noreferrer" target="_blank">{{table['TABLE_NAME'].title()}}</a></li>
	  % end
	</ul>
</article>
</main>