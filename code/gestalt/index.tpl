% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for an index in the form of a datatable.
% lang_id = kwargs.get('lang_id', '')
% text = kwargs['text']
% thead_start = b'<thead>' if isinstance(text, bytes) else '<thead>'
% thead_end = b'</thead>' if isinstance(text, bytes) else '</thead>'
% kwargs['table_config'], kwargs['table_config_json'] = db.get_table_config_json(**kwargs)
% table_config, table_config_json = kwargs['table_config'], kwargs['table_config_json']
% roles = getattr(request, 'roles', set())
{{!kwargs.get('starttag', '<main>')}}
<noscript>
	<article class="just sheet" data-not-for-ft="">
		<p>{{db.glosses['index_needs_js'][lang_id]}}</p>
	  % if 'ersatzlink' in kwargs:
		<p><a href="{{kwargs['ersatzlink']}}">{{!db.glosses['or_click_here_for_ersatz'][lang_id]}}</a></p>
	  % end
	</article>
</noscript>
% if kwargs.get('with_filtercard', True):
<form id="filter_card" class="card txt" role="search" data-not-for-ft="">
	<table>
		<% columns = [
			(
				rank,
				db.xmlhtml.serialize_elem(e, inner = True),
				'data-select' in e.attrib,
				e.attrib.get('data-selectlabel', ''),
				'data-filterless' in e.attrib,
			) for rank, e in enumerate(
				db.ET.XML(text[text.find(thead_start) + 7:text.find(thead_end)]).findall('th')
			) if 'data-no_search' not in e.attrib
		]
		%>
		% has_selects = any( c[2] for c in columns )
		<thead>
			<tr>
				<th><button id="datatable-reset" type="reset" onclick="tableReset({{!table_config.get('order', [])}})">{{!db.glosses['reset'][lang_id]}}</button></th>
				<th>{{!table_config.get('my_th_search', '')}}</th>
				<th id="no_case" title="{{table_config.get('my_th_no_case_title', '')}}">{{!table_config.get('my_th_no_case', '')}}</th>
				<th id="no_order" title="{{table_config.get('my_th_no_order_title', '')}}">{{!table_config.get('my_th_no_order', '')}}</th>
				<th id="regex" title="{{table_config.get('my_th_regex_title', '')}}">{{!table_config.get('my_th_regex', '')}}</th>
			  % if has_selects:
				<th>{{!db.glosses['select_cell_value'][lang_id]}}</th>
			  % end
			</tr>
		</thead>
		<tbody>
		  % if len(columns) > 1:
			<tr data-column_rank="_" id="filter_global">
				<th scope="row">{{db.glosses['all_columns'][lang_id]}}</th>
				<td><input aria-label="{{db.glosses['search_term'][lang_id]}}" autofocus="" id="global_term" type="search"/></td>
				<td><input aria-labelledby="no_case" checked="" id="global_caseInsen" title="{{db.glosses['no_case'][lang_id]}}" type="checkbox"/></td>
				<td><input aria-labelledby="no_order" checked="" id="global_smart" title="{{db.glosses['no_order'][lang_id]}}" type="checkbox"/></td>
				<td><input aria-labelledby="regex" id="global_regex" title="{{db.glosses['regex'][lang_id]}}" type="checkbox"/></td>
			  % if has_selects:
				<td></td>
			  % end
			</tr>
		  % end
		  % for column_rank, column_title, has_select, selectlabel, filterless in columns:
		   % if not filterless:
			<tr data-column_rank="{{column_rank}}">
				<th scope="row">{{!column_title}}</th>
				<td><input aria-label="{{db.glosses['search_term'][lang_id]}}" class="column" id="column{{column_rank}}_term" type="{{!'hidden' if selectlabel else 'search'}}"/></td>
				<td><input aria-labelledby="no_case" checked="" class="column" id="column{{column_rank}}_caseInsen" title="{{db.glosses['no_case'][lang_id]}}" type="{{!'hidden' if selectlabel else 'checkbox'}}"/></td>
				<td><input aria-labelledby="no_order" checked="" class="column" id="column{{column_rank}}_smart" title="{{db.glosses['no_order'][lang_id]}}" type="{{!'hidden' if selectlabel else 'checkbox'}}"/></td>
				<td><input aria-labelledby="regex" class="column" id="column{{column_rank}}_regex" title="{{db.glosses['regex'][lang_id]}}" type="{{!'hidden' if selectlabel else 'checkbox'}}"/></td>
			  % if has_selects:
				<td>\\
				  % if selectlabel:
<label class="selectlabel">{{!selectlabel}}</label>
				  % end
				  % if has_select:
<select aria-label="{{db.glosses['search_term'][lang_id]}}"><option value=""></option></select>\\
				  % end
</td>
			  % end
			</tr>
		   % end
		  % end
		</tbody>
	</table>
</form>
% end
% if 'editor' in roles and kwargs.get('idcol', None) is not None:
<style>
.index td[contenteditable]:focus {background: #ffffff; box-shadow: 0.5px 0.5px 0.5px 0.5px #aaafff inset}
.index td:nth-child({{kwargs['idcol']['pos'] + 1}}) {box-shadow: 0 0 0 1.5px #aaafff inset; cursor: pointer}
.index td:nth-child({{kwargs['idcol']['pos'] + 1}}):is(:focus, :hover) {box-shadow: 0 0 1px 2.5px #66afff inset}
.index td:nth-child({{kwargs['idcol']['pos'] + 1}}):active {box-shadow: 0 0 2px 4px #66afff inset}
  % refcol_selector = ':is({})'.format(', '.join( f"td:nth-child({col['pos'] + 1})" for col in kwargs['cols'] if col['reftable_name'] ))
  % if refcol_selector:
.index {{!refcol_selector}} {box-shadow: 0 0 0 1.5px #fff444 inset; cursor: pointer}
.index {{!refcol_selector}}:is(:focus, :hover) {box-shadow: 0 0 1px 2.5px #f8c800 inset}
.index {{!refcol_selector}}:active {box-shadow: 0 0 2px 4px #f8c800 inset}
  % end
  % viewcol_selector = ':is({})'.format(', '.join( f"td:nth-child({col['pos'] + 1})" for col in kwargs['cols'] if col['name'][:1] == '_' ))
  % if viewcol_selector:
.index {{!viewcol_selector}} {background: #f4f4f4; cursor: default}
  % end
</style>
% end
<script src="{{kwargs.get('prepath', '')}}/cssjs/jquery/datatables.min.js"></script>
<script src="{{kwargs.get('prepath', '')}}/cssjs/jquery/row().show().js"></script>
<script src="{{kwargs.get('prepath', '')}}/cssjs/dataTablesExt.js"></script>
<script>
window.onload = function() {
	let datatable = tableSetup({{!table_config_json}});
	let input = document.querySelector('#index_filter > label > input');
	if (input && !document.querySelector('[autofocus]')) {input.focus()};
};
% if 'editor' in roles and 'cols_json' in kwargs:
$(document).on('init.dt', function(e, settings) {cellsEventListener('add', {{!kwargs['cols_json']}})});
% end
</script>
{{!text}}
{{!kwargs.get('endtag', '</main>')}}
