% if not db.debug:
<script type="text/javascript">// by Matomo
	var _paq = window._paq || [];
	_paq.push(["setDomains", {{!db.config['matomo']['domains']}}]);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	_paq.push(['disableCookies']);
	(function() {
		var u="//webstats.badw.de/";
		_paq.push(['setTrackerUrl', u+'matomo.php']);
		_paq.push(['setSiteId', '{{!db.config['matomo']['site_id']}}']);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<noscript><img src="//webstats.badw.de/matomo.php?idsite={{!db.config['matomo']['site_id']}}&amp;rec=1" style="border: 0" alt=""/></noscript>
% end