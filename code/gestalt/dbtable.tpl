% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a table structure for an index (which is then made via index.tpl).
<article class="index" data-template="index.tpl">
<h1>{{kwargs['table_name'].title()}}</h1>
% if 'redactor' in request.roles and 'table_name' in kwargs:
<button onclick="rowInsert(event, '{{kwargs['table_name']}}')">{{db.glosses['add_row'][kwargs.get('lang_id', db.lang_id)]}}</button>
% end
<table id="index">
<thead>
	<tr>
	  % for col in kwargs['cols']:
		<th{{!'' if col['id?'] else ' data-select=""'}}>{{col['name'].title()}}</th>
	  % end
	</tr>
</thead>
</table>
</article>
