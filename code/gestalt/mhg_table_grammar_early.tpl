% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
<meta charset="utf-8"/>
<p>Σ {{total}}</p>
% for case, cells in sorted(charts.items()):
<p>{{case}}:</p>
<table style="border-collapse: collapse; border-bottom: 1pt solid #000000; border-top: 1pt solid #000000; font-size: 0.85em; text-align: center">
	<thead>
		<tr>
			<th></th>
			<th colspan="5" style="border-right: 0.5pt solid #000000">obd.</th>
			<th>omd.</th>
			<th colspan="2">wmd.</th>
		</tr>
		<tr>
			<th style="width: 3.2em"></th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; border-right: 0.5pt solid #000000; width: 5.4em">Iw, Nib, Parz, Tris</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; width: 5.4em">alem.</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; width: 5.4em">alem.-bair.</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; width: 5.4em">bair.</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; border-right: 0.5pt solid #000000; width: 5.4em">ofrk.</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; width: 5.4em">²12, ¹13: hess.-thür.</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; width: 5.4em">rhfrk.-hess.</th>
			<th style="border-bottom: 1pt solid #000000; border-top: 0.5pt solid #000000; width: 5.4em">mfrk.</th>
		</tr>
	</thead>
	<tbody>
	  % for name in ('Will', 'WNot', 'andere'):
		<tr>
			<th style="text-align: right">²11&#x2011;¹12,&#xa0;{{name}}</th>
			<td style="border-right: 0.5pt solid #000000">–</td>
			% background, color, content = next(cells)
			<td colspan="4" style="background: {{background}}; border-right: 0.5pt solid #000000; color: {{color}}">{{!content}}</td>
			<td>–</td>
			<td>–</td>
			<td>–</td>
		</tr>
	  % end
		<tr>
			<th style="text-align: right">²12</th>
			<td style="border-right: 0.5pt solid #000000">–</td>
		  % for i in range(3):
			% background, color, content = next(cells)
			<td style="background: {{background}}; color: {{color}}">{{!content}}</td>
		  % end
			<td style="border-right: 0.5pt solid #000000">–</td>
		  % for i in range(3):
			% background, color, content = next(cells)
			<td style="background: {{background}}; color: {{color}}">{{!content}}</td>
		  % end
		</tr>
		<tr>
			<th style="text-align: right">¹13</th>
		  % for i in range(4):
			% background, color, content = next(cells)
			<td style="background: {{background}}; {{'border-right: 0.5pt solid #000000; ' if i == 0 else ''}} color: {{color}}">{{!content}}</td>
		  % end
			<td style="border-right: 0.5pt solid #000000">–</td>
		  % for i in range(3):
			% background, color, content = next(cells)
			<td style="background: {{background}}; color: {{color}}">{{!content}}</td>
		  % end
		</tr>
	  % for i in cells:
		<tr><td>{{i}}</td></tr>
	  % end
	</tbody>
</table>
% end
