# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
import sys
import time

import fs
import sys_io

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Delete files piled up by the authentication process.
    Read the config (see below about :param:`urdata`).
    Enter the following loop:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Delete session files (from the session folder specified in the config)
      that are older than a certain timeout span (specified in the config).
    - Delete empty folders.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: may contain zero, one or more absolute paths of files.
        Each of these files must have the structure of ini-files as it is
        understood by :module:`configparser`. They are read into a config
        and contain, among other information, paths.
    '''
    log = fs.Log(__file__, '../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {
            'paths_from_program_folder': __file__,
            'paths_from_config_folder': urdata[0],
            })
    session_dirpath = paths['session.data_dir'] + os.sep
    session_timeout = int(config['session']['session.timeout'])
    while True:
        try:
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            time.sleep(3)
            for filepath in sorted(fs.get_paths(session_dirpath)):
                if time.time() - os.path.getmtime(filepath) > session_timeout:
                    os.remove(filepath)
            for name in os.listdir(session_dirpath):
                path = session_dirpath + name + os.sep
                if not os.path.isdir(path):
                    continue
                for dirpath, _, _ in sorted(os.walk(path), reverse = True)[:-1]:
                    os.rmdir(dirpath)
        except (FileNotFoundError, OSError, ValueError):
            continue

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
