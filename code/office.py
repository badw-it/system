# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2017 ff.
'''
Macros for LibreOffice. To be saved in:
LibreOffice/<number>/user/Scripts/python
'''
def dedent_hanging():
    cur = get_cur()
    cur.ParaLeftMargin -= 500
    cur.ParaFirstLineIndent += 500

def dedent_left():
    get_cur().ParaLeftMargin -= 500

def dedent_right():
    get_cur().ParaRightMargin -= 500

def get_cur():
    return XSCRIPTCONTEXT.getDesktop(
            ).getCurrentComponent().CurrentController.ViewCursor

def indent_hanging():
    cur = get_cur()
    cur.ParaLeftMargin += 500
    cur.ParaFirstLineIndent -= 500

def indent_left():
    get_cur().ParaLeftMargin += 500

def indent_right():
    get_cur().ParaRightMargin += 500

def print(text):
    cur = get_cur()
    # Printing in cells is a special case.
    cell = cur.Cell
    if cell:
        cell.insertString(cur, text, False)
    else:
        cur.String = str(text)
        cur.collapseToEnd()

def set_font_size():
    get_cur().CharHeight = 11.0

def set_font_size_petit():
    get_cur().CharHeight = 10.0

def set_line_height():
    from com.sun.star.style import LineSpacing
    from com.sun.star.style.LineSpacingMode import FIX
    line_spacing = LineSpacing()
    line_spacing.Mode = FIX
    line_spacing.Height = 530
    get_cur().ParaLineSpacing = line_spacing

def set_line_height_petit():
    from com.sun.star.style import LineSpacing
    from com.sun.star.style.LineSpacingMode import FIX
    line_spacing = LineSpacing()
    line_spacing.Mode = FIX
    line_spacing.Height = 490
    get_cur().ParaLineSpacing = line_spacing

def set_style_source_ref():
    get_cur().CharStyleName = 'Quellenverweis'

def set_style_source_text():
    get_cur().CharStyleName = 'Source Text'

def set_style_normal():
    get_cur().CharStyleName = 'Standard'

def toggle_keep_with_next():
    cur = get_cur()
    cur.ParaKeepTogether = False if cur.ParaKeepTogether else True

def unindent_all():
    cur = get_cur()
    cur.ParaLeftMargin = 0
    cur.ParaFirstLineIndent = 0
    cur.ParaRightMargin = 0

def unset_vertical_margins():
    cur = get_cur()
    cur.ParaBottomMargin = 0
    cur.ParaTopMargin = 0
