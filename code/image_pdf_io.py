# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
import itertools
import os
try: import regex as re
except ImportError: import re
from base64 import b64encode
from functools import partial

from PIL import Image, ImageOps
try: import fitz
except ImportError: pass

import fs
import parse

def bisect_and_crop(
        indirpath: str,
        right_to_left: bool = False,
        start_cut: int = 990,
        end_cut: int = 1010,
        safety_distance: int = 10,
        left: int = 0,
        right: int = 0,
        upper: int = 0,
        lower: int = 0,
        exts: 'set[str]' = {'jpg', 'png', 'tif'},
        ) -> 'tuple[str, int]':
    '''
    Bisect and, if desired, crop the images in the folder :param:`indirpath`.

    Save the new images at a new folder. Return its path and size.

    :param right_to_left: ``True``, if the right part of an unsplit image cometh
        before the left part and if the cut is on the first image farther to the
        right, on the last image farther to the left.
    :param start_cut: pixels from the left to the fold, first double image.
    :param end_cut: pixels from the left to the fold, last double image.
    :param safety_distance: pixels added to every image on the side of the cut.
    :param left: pixels from the left to the new left border of verso pages.
    :param right: pixels from the left to the new right border of recto pages.
        But if ``0``, take the whole width!
    :param upper: pixels from the top to the new upper border of all pages.
    :param lower: pixels from the top to the new lower border of all pages.
        But if ``0``, take the whole height!
    '''
    indirpath = os.path.abspath(indirpath) + os.sep
    outdirpath = fs.get_new_path(indirpath) + os.sep
    names = sorted(os.listdir(indirpath), key = parse.sortxtext)
    names = [ n for n in names if os.path.splitext(n)[1][1:].lower() in exts ]
    num_of_double_pages = len(names)
    size = 0
    partnums = (1, 0) if right_to_left else (0, 1)
    stepfactor = (end_cut - start_cut) / num_of_double_pages
    for imagenum, imagename in enumerate(names):
        # In order to shift the cut proportionally:
        step = round(imagenum * stepfactor)
        basename, ext = os.path.splitext(imagename)
        image = Image.open(indirpath + imagename)
        ImageOps.exif_transpose(image, in_place = True)
        for partnum in partnums:
            l = (start_cut + step - safety_distance) if partnum else left
            r = (right or image.width) if partnum else (
                    start_cut + step + safety_distance)
            new_image = image.crop((l, upper, r, lower or image.height))
            if right_to_left:
                partnum = abs(partnum - 1)
            partpath = f'{outdirpath}{basename}.{partnum + 1}{ext}'
            new_image.save(fs.pave(fs.get_new_path(partpath)))
            size += os.path.getsize(partpath)
    return outdirpath, size

def convert(
        inpath: str,
        outpath: str,
        convert_kwargs: 'dict[str, str | int | tuple[float, ...]]' = {},
        save_kwargs: 'dict[str, str | int | tuple[float, ...]]' = {},
        ) -> 'tuple[str, int]':
    '''
    Convert the image at :param:`inpath` to the format which is indicated by the
    file extension of :param:`outpath`. The available formats are:

    1. https://pillow.readthedocs.io/en/latest/handbook/image-file-formats.html
    2. base64 with the extension “.b64”.

    In the first case and if :param:`convert_kwargs` is not empty, let precede a
    conversion with the key-value pairs of :param:`convert_kwargs` – these pairs
    must accord to the keyword arguments of the following function:

    http://pillow.readthedocs.io/en/latest/reference/Image.html#PIL.Image.Image.convert

    E.g. ``convert(inpath, outpath, convert_kwargs = {'mode': 'RGB'})``.

    Save the image at :param:`outpath`; do that with any keyword arguments given
    in :param:`save_kwargs`; these pairs must accord to the keyword arguments of
    the following function:

    http://pillow.readthedocs.io/en/latest/reference/Image.html#PIL.Image.Image.save

    Return the path and the size of the output file.
    '''
    _, ext = os.path.splitext(outpath)
    if ext.lower() == '.b64':
        outpath, size = fs.write(b64encode(fs.read(inpath, None)), outpath)
    else:
        image = Image.open(inpath)
        ImageOps.exif_transpose(image, in_place = True)
        if convert_kwargs is not None:
            image = image.convert(**convert_kwargs)
        image.save(fs.pave(outpath), **save_kwargs)
        size = os.path.getsize(outpath)
    return outpath, size

def extract_from_pdf(
        inpath: str,
        outdirpath: str,
        outdirpath_new: bool = True,
        ) -> 'tuple[str, int]':
    '''
    From the PDF at :param:`inpath`, extract all images and save these images in
    a folder. The path of this folder is taken from :param:`outdirpath` – but if
    :param:`outdirpath_new` is ``True`` (the default), it may be modified to get
    a new, unused path (also leaving out an extension, if given).

    Name each file with a consecutive number, and set the extension according to
    the format of the image.

    For each output file, return its path and its size.
    '''
    if outdirpath_new:
        outdirpath = fs.get_new_path(outdirpath, ext = '') # for the folder.
    doc = fitz.open(inpath)
    xrefs = [ x[0] for i in range(len(doc)) for x in doc.get_page_images(i) ]
    zerofill = len(str(len(xrefs)))
    size = 0
    num = 0
    for xref in xrefs:
        num += 1
        image = doc.extract_image(xref)
        if not image:
            continue
        ext = image['ext']
        outpath = fs.pave(
                fs.get_new_path( # for the file, not for the folder.
                    os.path.join(outdirpath, str(num).zfill(zerofill)),
                    ext = '.' + ('jpg' if ext == 'jpeg' else ext),
                    ))
        size += fs.write(image['image'], outpath)[1]
    return outdirpath, size

def merge_into_pdf(inpaths: 'Iterable[str]', outpath: str) -> 'tuple[str, int]':
    '''
    Merge the images at :param:`inpaths` into one PDF (in the given order); save
    this PDF at :param:`outpath`.
    '''
    doc = fitz.open()
    for inpath in inpaths:
        image = fitz.open(inpath)
        size = image[0].rect
        page = doc.new_page(width = size.width, height = size.height)
        page.show_pdf_page(size, fitz.open('pdf', image.convert_to_pdf()), 0)
        image.close()
    doc.del_xml_metadata()
    doc.save(
            outpath,
            garbage = 3,
            clean = False, # Sic! Otherwise, corruption may occur!
            deflate = True,
            incremental = False,
            linear = True,
            encryption = fitz.PDF_ENCRYPT_NONE,
            permissions = -1,
            )
    return outpath, os.path.getsize(outpath)

def merge_pdfs(dirpath: str) -> 'tuple[str, int]':
    '''
    Merge all PDFs found in the folder :param:`dirpath` into one PDF and save it
    as a file next to and with the same basename as the folder :param:`dirpath`.
    (If there is a file at the same path already, do not overwrite it but create
    a new similar path automatically and save the PDF there.)
    '''
    dirpath = os.path.abspath(dirpath) + os.sep
    outpath = fs.get_new_path(dirpath, ext = '.pdf')
    result = fitz.open()
    for name in sorted(os.listdir(dirpath)):
        path = dirpath + name
        with fitz.open(path) as file:
            result.insert_pdf(file)
    result.save(outpath)
    return outpath, os.path.getsize(outpath)

def merge_tiles(tiles: 'list[tuple[int, int, Image]]') -> Image:
    '''
    Merge the image tiles into one PIL image and return it.

    :param tiles: the image tiles, each of which is a tuple consisting of:

        - the row index (from 0 up to the number of rows)
        - the column index (from 0 up to the number of columns)
        - the PIL image of the tile.
    '''
    tiles.sort()
    tiles = [
            list(g) for k, g in itertools.groupby(tiles, key = lambda x: x[0])
            ]
    total_width = sum( tile.size[0] for _, _, tile in tiles[0] )
    total_height = sum(
            tile.size[1] for _, _, tile in ( row[0] for row in tiles )
            )
    image = Image.new('RGB', (total_width, total_height))
    current_width = 0
    current_height = 0
    for row in tiles:
        for row_num, col_num, tile in row:
            width, height = tile.size
            image.paste(im = tile, box = (current_width, current_height))
            current_width += width
        current_width = 0
        current_height += height
    return image

def reduce(
        inpath: str,
        outpath: str,
        maxwidth: int = 0,
        maxheight: int = 0,
        maxlength: int = 1000,
        resize_kwargs: 'dict[str, str | int | tuple[float, ...]]' = {},
        save_kwargs: 'dict[str, str | int | tuple[float, ...]]' = {},
        save_always: bool = True,
        ) -> 'tuple[str, int]':
    '''
    Open the image at :param:`inpath`, and look up its width and height.

    If :param:`maxwidth` is greater than zero and less than the width of the old
    image, reduce the width to :param:`maxwidth` and the length according to the
    same ratio.

    After that, if :param:`maxheight` is greater than zero and less than the old
    height, reduce correspondingly.

    If both parameters, however, equal zero and if :param:`maxlength` is greater
    than zero and less than the currently longer axis (be this width or height),
    then reduce this longer axis to :param:`maxlength` and reduce the other side
    according to the same ratio.

    Do the resize with the key-value pairs of :param:`resize_kwargs`; said pairs
    must accord to the keyword arguments of the following function:

    http://pillow.readthedocs.io/en/latest/reference/Image.html#PIL.Image.Image.resize

    Save the image at :param:`outpath`; do that with any keyword arguments given
    in :param:`save_kwargs`; these pairs must accord to the keyword arguments of
    the following function:

    http://pillow.readthedocs.io/en/latest/reference/Image.html#PIL.Image.Image.save

    Return the path and the size of the output file.

    If :param:`save_always` is ``False``, do not save the image if neither width
    nor height have changed; and return ``('', 0)`` in this case.
    '''
    image = Image.open(inpath)
    ImageOps.exif_transpose(image, in_place = True)
    width_old, height_old = image.size
    width = width_old
    height = height_old
    if maxwidth == maxheight == 0:
        maxlength_old = max((width_old, height_old))
        if maxlength_old > maxlength > 0:
            ratio = maxlength / maxlength_old
            width = round(ratio * width_old)
            height = round(ratio * height_old)
    else:
        if maxwidth > 0:
            if width_old > maxwidth:
                width = maxwidth
                height = round(height_old * maxwidth / width_old)
        if maxheight > 0:
            if height_old > maxheight:
                height = maxheight
                width = round(width_old * maxheight / height_old)
    length_changed = False
    if width != width_old or height != height_old:
        length_changed = True
        image = image.resize((width, height), **resize_kwargs)
    if save_always or length_changed:
        image.save(fs.pave(outpath), **save_kwargs)
        size = os.path.getsize(outpath)
    else:
        outpath = ''
        size = 0
    return outpath, size

def slice_pdf(
        inpath: str,
        part_pages: 'tuple[tuple[int, int]]',
        name_template: str = '{first_plus_offset}–{last_plus_offset}.pdf',
        offset: int = 0,
        step: int = 0,
        kwargs: 'dict' = {},
        ) -> 'tuple[str, int]':
    '''
    Slice the PDF at :param:`inpath` into as many parts as given in the tuple of
    pairs :param:`part_pages`, whose pairs give the first page and the last page
    of each slice. The page numbers refer to the original PDF; and the numbering
    starts with ``1`` (because that is what readers show).

    Save the slices into a folder with the same path as :param:`inpath`, however
    without the extension. If there already exists a folder at this path, create
    a slightly altered path (append “-”) until there is no name conflict.

    :param name_template: template for the name of each PDF-slice file. It could
        and should contain format-string-like references to all or some of these
        variables:

        - ``name``, i.e. ``os.path.basename(inpath)`` without extension.
        - ``num``, i.e. the ordinal number of the current slice – it starts with
          ``1``, too.
        - ``first``, i.e. what is given as the first page for the current slice.
        - ``last``, i.e. what is given as the last page for the current slice.
        - ``first_plus_offset``, i.e. the sum of ``first`` and :param:`offset` –
          which may be useful for shifting the page number to the number that is
          printed on the page, e.g. the first page of the PDF may show as a page
          number “123”, not “1” (e.g. in the case of fascicles); :param:`offset`
          is then ``122``, i.e. ``123 - 1``.
        - ``last_plus_offset``, i.e. the sum of ``last`` and :param:`offset`.

    :param step: added to :param:`offset` after each cycle – which may be useful
        for shifting the page increasingly. E.g. if there are two columns having
        a printed number each, then :param:`step` would be ``1``, for the offset
        has to be increased by an additional ``1`` after each cycle.
    :param kwargs: keyword arguments to be passed to ``insert_pdf``. This method
        is explained at: https://pymupdf.readthedocs.io/en/latest/document.html
    '''
    inpath = os.path.abspath(inpath)
    dirpath = fs.pave_dir(fs.get_new_path(os.path.splitext(inpath)[0]))
    name = os.path.basename(dirpath)
    urdoc = fitz.open(inpath)
    size = 0
    for num, (first, last) in enumerate(part_pages, start = 1):
        first_plus_offset = first + offset
        last_plus_offset = last + offset
        doc = fitz.open()
        doc.insert_pdf(
                urdoc, from_page = first - 1, to_page = last - 1, **kwargs)
        outpath = os.path.join(
                dirpath,
                name_template.format(
                    name = name,
                    num = num,
                    first = first,
                    last = last,
                    first_plus_offset = first_plus_offset,
                    last_plus_offset = last_plus_offset,
                    ))
        doc.save(outpath)
        size += os.path.getsize(outpath)
        offset += step
    return dirpath, size
