# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff.
try: import regex as re
except ImportError: import re
import time
import typing as ty
import unicodedata as ud
from collections import Counter
from collections import defaultdict
from collections import deque
from copy import deepcopy
from functools import reduce
from functools import partial
from itertools import islice, product
from itertools import zip_longest
from operator import mul

BASENAMES: 'dict[str, str]' = {
        ud.name(chr(i)): chr(i) for i in
                [32]                   # space
                + list(range(48, 58))  # digits
                + list(range(65, 91))  # latin majuscules
                + list(range(97, 123)) # latin minuscules
        }

DATE: str = r'''(?ix)
        ( # year-month-day pattern:
            (?P<y1>\d+)\s*-\s*
            (?P<m1>\d\d?)\s*-\s*
            (?P<d1>\d\d?)
        | # day.month.year pattern:
            (?P<d2>\d\d?)\.\s*
            (?P<m2>\d\d?)\.\s*
            (?P<y2>\d+)
        | # year-month pattern:
            (?P<y3>\d+)\s*-\s*
            (?P<m3>\d\d?)
        | # month.year pattern:
            (?P<m4>\d\d?)\.\s*
            (?P<y4>\d+)
        | # year pattern:
            (?P<y5>\d+)
        )
        (?P<minus>\s*(?:[ab]\.?\s*c|v\.))?
        '''

DIACRITICS: str = ''.join(
        char for i in range(int('0x10ffff', base = 16) + 1)
        if 'COMBINING' in ud.name((char := chr(i)), '') )

PRE: 'tuple[tuple[re.Pattern[str], str], ...]' = (
        (re.compile(f'[{DIACRITICS}]'), ''),
        (re.compile(r'\W+'), ' '),
        )

ROMAN_NUMBERS_MAP: 'tuple[tuple[int, str]]' = (
        (1000, 'M'),
        (900, 'CM'),
        (500, 'D'),
        (400, 'CD'),
        (100, 'C'),
        (90, 'XC'),
        (50, 'L'),
        (40, 'XL'),
        (10, 'X'),
        (9, 'IX'),
        (5, 'V'),
        (4, 'IV'),
        (1, 'I'),
        ) # By Mark Pilgrim

XTAG: str = r'''<(?:"[^"]*"|'[^']*'|[^>])*>'''

XTAG_PRE: 'tuple[tuple[re.Pattern[str], str], ...]' = (
        (re.compile(XTAG), ''),
        (re.compile(r'&[^;]*;'), ' '),
        ) + PRE

XTAGBODY: str = r'''(?:"[^"]*"|'[^']*'|[^>/])*'''

XTERM: str = rf'''# Note: You must use the verbose mode!
        (?P<xterm>
            (?:
                 <!--(?:(?!-->).)*--> # ··························· Comment
                |<!\[(?:(?!\]>).)*\]> # ··························· Unknown Declaration / CDATA
                |<!(?P<dd>(?:<(?&dd)>|"[^"]*"|'[^']*'|[^<>])*)> # · Declaration / DTD
                |<\?(?:(?!\?>).)*\?> # ···························· Declaration / Processing Instruction
                |<{XTAGBODY}/\s*> # ······························· Self-closing tag
                |<{XTAGBODY}>(?&xterm)</{XTAGBODY}> # ············· Inner Element
                |[^<] # ··········································· Inner Text
            )*
        )'''

def anon_ip(ip: str) -> str:
    '''
    Anonymize :param:`ip`:

    - IPv4: only the first 3 bytes are kept.
    - IPv6: only the first 6 (3 times 2) bytes are kept. (IPv6 addresses
      contain 128 bit of information, written as 8 blocks of 2 bytes, but
      this notation can be abbreviated.)

    .. note::
        For the anonymization of IPv6, see the following description on page 3
        at http://www.ietf.org/rfc/rfc2374.txt and the explanations on the
        subsequent pages::

            The aggregatable global unicast address format is as follows:

            | 3|  13 | 8 |   24   |   16   |          64 bits               |
            +--+-----+---+--------+--------+--------------------------------+
            |FP| TLA |RES|  NLA   |  SLA   |         Interface ID           |
            |  | ID  |   |  ID    |  ID    |                                |
            +--+-----+---+--------+--------+--------------------------------+
    '''
    if ':' in ip:
        ip_parts = ip.split(':')
        diff = 8 - len(ip_parts)
        if diff and '' in ip_parts:
            ip_parts.insert(ip_parts.index(''), diff * '0')
        return ':'.join(ip_parts[:4])
    else:
        return ip.rsplit('.', 1)[0]

def arabic_to_roman(
        number: int,
        numeral_map: 'tuple[tuple[int, str]]' = ROMAN_NUMBERS_MAP,
        ) -> str:
    '''
    Attribution notice:
    In essence by Mark Pilgrim, Paul Winkler, Tim Valenta, dln385.
    See http://code.activestate.com/recipes/81611-roman-numerals/.
    '''
    result = []
    for integer, numeral in numeral_map:
        count = number // integer
        result.append(numeral * count)
        number -= integer * count
    return ''.join(result)

def compare_lines(
        lines0: 'list[list[str]]',
        lines1: 'list[list[str]]',
        align: 'ty.Callable[[list[str], list[str]], bool]' =
            lambda line0, line1: line0 == line1,
        equal: 'ty.Callable[[list[str], list[str]], bool]' =
            lambda line0, line1: line0 == line1,
        maxplus: int = 10,
        marker: str = '██',
        precontext: int = 5,
        postcontext: int = 5,
        line_before_new_resultblock: str = '········',
        line_from_lines0_flag: str = '○',
        line_from_lines1_flag: str = '●',
        ) -> 'MutableSequence[list[str]]':
    '''
    Compare :param:`lines0` and :param:`lines1`. Both are lists of lines. Every
    line is split up into one or more segments. (The option to compare split-up
    lines is useful when using :param:`marker`: If this is not an empty string,
    it will be appended in the comparison result to any :param:`lines1`-segment
    found unequal to the corresponding :param:`lines0`-segment.)

    .. warning::
        Both lists may be changed by this function. If you might need unchanged
        versions of them afterwards, save a deep copy.

    First, try to re-align them so that insertions and deletions are taken into
    account. Doing this, i.e. looking for the next matching lines …

    - do not go further than :param:`maxplus` lines.
    - decide that two lines match if :param:`align` (called with the two lines)
      returns ``True``. So :param:`align` gives the opportunity to even out the
      lines before comparing them so that not every difference counts.

    Second, compare the lines, so decide that two lines match if :param:`equal`
    (called with the two lines) returns ``True`` – again, :param:`equal` gives
    the opportunity to even out the lines before comparing.

    Return the differing lines, prepending :param:`precontext` lines, appending
    :param:`postcontext` lines. The numbers of contextual lines are smaller, if
    there are no more lines (i.e. at the beginning or at the end of the list of
    lines). Overlapping contexts are merged. The then given blocks of differing
    lines plus context are separated with :param:`line_before_new_resultblock`.
    Lines from :param:`lines0` are prefixed with :param:`line_from_lines0_flag`
    and lines from :param:`lines1` with :param:`line_from_lines1_flag`.

    >>> doc0 = 'Aa Bb Cc|Dd Ee Ff|Gg Hh Ii|Jj Kk Ll|Mm Nn Oo|Pp Qq Rr|Ss Tt Uu|Vv Ww Xx|Yy Zz'
    >>> doc1 = 'Aa Bb Cc|Dq Ee Ff|Gg Hh Ii|Jj Kk Ll|Mm Nn Oo|Pp Qq Rr|Vv Ww Xx|Yy Zz'
    >>> lines0 = [ line.split(' ') for line in doc0.split('|') ]
    >>> lines1 = [ line.split(' ') for line in doc1.split('|') ]
    >>> diff = compare_lines(lines0, lines1)
    >>> for line in diff:
    ...     print(' '.join(line))
    ○ Aa Bb Cc
    ○ Dd Ee Ff
    ● Dq██ Ee Ff
    ○ Gg Hh Ii
    ○ Jj Kk Ll
    ○ Mm Nn Oo
    ○ Pp Qq Rr
    ○ Ss Tt Uu
    ● ██ ██ ██
    ○ Vv Ww Xx
    ○ Yy Zz
    '''
    maxplus += 1
    pos = -1
    while True:
        pos += 1
        if pos >= len(lines0):
            if pos >= len(lines1):
                break
            else:
                lines0.insert(pos, [''])
        elif pos >= len(lines1):
            lines1.insert(pos, [''])
        else:
            for pluspos0 in range(pos + 0, min(pos + maxplus, len(lines0))):
                if align(lines0[pluspos0], lines1[pos]):
                    pluspos0_matches = True
                    break
            else:
                pluspos0_matches = False
            if pluspos0 == pos:
                pluspos1 = pos + 1
                pluspos1_matches = False
            else:
                for pluspos1 in range(pos + 0, min(pos + maxplus, len(lines1))):
                    if align(lines0[pos], lines1[pluspos1]):
                        pluspos1_matches = True
                        break
                else:
                    pluspos1_matches = False
            diff0 = pluspos0 - pos
            diff1 = pluspos1 - pos
            if pluspos0_matches and diff0 and diff0 <= diff1:
                for _ in range(diff0):
                    lines1.insert(pos, [''])
                pos = pluspos0
            elif pluspos1_matches and diff1 and diff1 < diff0:
                for _ in range(diff1):
                    lines0.insert(pos, [''])
                pos = pluspos1
    for nth in range(len(lines1) - 1, - 1, - 1):
        is_unequal = False
        line0 = deepcopy(lines0[nth])
        line1 = deepcopy(lines1[nth])
        if not equal(line0, line1):
            is_unequal = True
            if marker:
                gap = len(line0) - len(line1)
                if gap < 0:
                    gap = -gap
                    line0 += ([''] * gap)
                    lines0[nth] += ([''] * gap)
                elif gap > 0:
                    line1 += ([''] * gap)
                    lines1[nth] += ([''] * gap)
                for subnth in range(len(line0)):
                    if line0[subnth] != line1[subnth]:
                        lines1[nth][subnth] += marker
        lines0[nth] = [line_from_lines0_flag] + lines0[nth]
        if is_unequal:
            lines0.insert(nth + 1, [line_from_lines1_flag] + lines1[nth])
    above = 0
    results = deque()
    for nth, line in enumerate(lines0, start = -1):
        if line[0] == line_from_lines1_flag:
            if above < nth - precontext:
                results.append([line_before_new_resultblock])
            for subnth in range(max(above, nth - precontext),
                                min(nth + postcontext + 2, len(lines0))):
                results.append(lines0[subnth])
            above = subnth + 1
    return results

def even(
        term: str,
        norm: str = 'NFKD',
        low: bool = True,
        pre: 'tuple[tuple[str|re.Pattern[str], str], ...]' = PRE,
        acts: 'tuple[ty.Callable[[str], str], ...]' = (),
        post: 'tuple[tuple[str|re.Pattern[str], str], ...]' = (),
        strip: bool = True,
        ) -> str:
    '''
    Get an evened form of the term :param:`term`, i.e. proceed as follows:

    - If :param:`norm` is not empty, unicode-normalize :param:`term` towards the
      unicode form given with :param:`norm`.
    - If :param:`low` is `True`, apply :meth:`str.lower`. – It may or may not be
      desired to get the more aggressive neutralization of :meth:`str.casefold`;
      if it is desired, put e.g. ``lambda x: x.casefold()`` into :param:`acts`.
    - For each pair A, B in :param:`pre`, replace any match of A with B. – A may
      be a string or a compiled regular expression.
    - For each pair in :param:`pre`, replace the first string with the second.
    - For each callable in :param:`acts`, the term is passed to the callable and
      replaced with the return value.
    - For each pair A, B in :param:`post`, do as in the case of :param:`pre`.
    - If :param:`strip` is `True`, apply :meth:`str.strip`.
    - Return the result.

    >>> even('<i>Thé</i> &amp; <b>Café</b> ...', pre = XTAG_PRE)
    'the cafe'
    '''
    if norm:
        term = ud.normalize(norm, term)
    if low:
        term = term.lower()
    for old, new in pre:
        term = (term.replace(old, new) if isinstance(old, str)
                else old.sub(new, term))
    for act in acts:
        term = act(term)
    for old, new in post:
        term = (term.replace(old, new) if isinstance(old, str)
                else old.sub(new, term))
    if strip:
        term = term.strip()
    return term

def expand_brackets(
        term: str,
        bracket_re: 're.Pattern[str]' = re.compile(r'<([^>]*)>'),
        with_brackets: bool = False,
        with_original: bool = False,
        ) -> 'ty.Generator[str]':
    '''
    >>> tuple(expand_brackets('o<u><wê>'))
    ('o', 'owê', 'ou', 'ouwê')
    >>> tuple(expand_brackets('o<u><wê>', with_brackets = True))
    ('o', 'owê', 'o<wê>', 'ou', 'ouwê', 'ou<wê>', 'o<u>', 'o<u>wê')
    >>> tuple(expand_brackets(
    ...        'o<u><wê>', with_brackets = True, with_original = True))
    ('o', 'owê', 'o<wê>', 'ou', 'ouwê', 'ou<wê>', 'o<u>', 'o<u>wê', 'o<u><wê>')
    >>> from re import compile
    >>> tuple(expand_brackets('o<u>‘wê’', compile(r'(?:<([^>]*)>|‘([^’]*)’)')))
    ('o', 'owê', 'ou', 'ouwê')
    '''
    def sub(m: 're.Match[str]') -> str:
        nonlocal pos_in_combi
        case = combi[pos_in_combi]
        pos_in_combi += 1
        return (
                '' if case == 0
                else ''.join(filter(None, m.groups())) if case == 1
                else m.group()
                )
    occurrences_num = len(bracket_re.findall(term))
    if occurrences_num:
        cases = (0, 1, 2) if with_brackets else (0, 1)
        for combi in product(cases, repeat = occurrences_num):
            pos_in_combi = 0
            variant = bracket_re.sub(sub, term)
            if with_original or variant != term:
                yield variant
    elif with_original:
        yield term

def extract(
        pattern: 'str|re.Pattern[str]',
        text: str,
        search: 'ty.Callable[[str, str], re.Match[str]]' = re.search,
        ) -> 'tuple[str, str]':
    r'''
    Search :param:`pattern` in :param:`text` with :param:`search`.

    Return a tuple containing:

    - from the match a join of all text covered by capturing parentheses.
    - the text without the entire matching part.

    If :param:`pattern` does not contain a capturing group or if it is not found
    in match :param:`text`, return a tuple containing:

    - the empty string.
    - the text unchanged.

    .. note::
        Capturing groups must not be nested. Within a capturing group, utilize a
        non-capturing group (i.e. with “(?:…)”) instead.

    >>> text = 'Hefe, Butter!, Zucker'
    >>> extractum, text = extract(r'\s*([^,]+)!\s*,?', text)
    >>> print(f'“{extractum}” apart from “{text}”')
    “Butter” apart from “Hefe, Zucker”
    '''
    match = search(pattern, text) if isinstance(pattern, str) else\
            pattern.search(text)
    if match:
        span = match.span()
        return ''.join(match.groups()), text[:span[0]] + text[span[1]:]
    else:
        return '', text

def group(
        items: 'ty.Iterable',
        n: int,
        discard: bool = True,
        fillvalue: 'ty.Any' = None,
        ) -> 'ty.Iterator[tuple]':
    '''
    Group :param:`items` into an iterable of :param:`n`-tuples.
    If :param:`discard` is ``True``, incomplete tuples are discarded.
    If not, they are replenished with :param:`fillvalue`.

    Attribution notice:
    by Brian Quinlan in 2004 (http://code.activestate.com/recipes/303060);
    upgraded and slightly extended by Stefan Müller in 2016.

    >>> print(list(group(range(10), 3)))
    [(0, 1, 2), (3, 4, 5), (6, 7, 8)]
    >>> print(list(group(range(10), 3, False)))
    [(0, 1, 2), (3, 4, 5), (6, 7, 8), (9, None, None)]
    '''
    myzip = zip if discard else partial(zip_longest, fillvalue = fillvalue)
    return myzip(*( islice(items, i, None, n) for i in range(n) ))

def iter_plaintext(
        text: str,
        startline: int = 0,
        startchar: int = 0,
        term_ends_with_termchar_re: str = r'[-\w]\Z',
        term_ends_with_hyphenchar_re: str = r'\w-\s*\Z',
        ) -> 'ty.Generator[tuple[str, tuple[int, int], tuple[int, int]]]':
    '''
    Yield (iteratively, incrementally) every item of :param:`text` in a tuple
    like ``('item', (1, 2), (3, 4))``, which would mean: In :param:`text`,
    the item :str:`item` starts in line 1 at character 2 and ends in line 3
    at character 4.

    This iterator is intended for plain text.
    '''
    endline = startline
    endchar = startchar
    term_ends_with_termchar_comp = re.compile(term_ends_with_termchar_re)
    term_ends_with_hyphenchar_comp = re.compile(term_ends_with_hyphenchar_re)
    in_term = False
    term = ''
    for char in text:
        if char == '\r':
            continue
        elif (term_ends_with_termchar_comp.search(term + char) or
                (char == '\n' and term_ends_with_hyphenchar_comp.search(term))):
            term += char
            if not in_term:
                in_term = True
                startline = endline
                startchar = endchar
        elif in_term:
            yield (term, (startline, startchar), (endline, endchar))
            term = ''
            in_term = False
        if char == '\n':
            endline += 1
            endchar = 0
        else:
            endchar += 1

def levenstein(
        seq1: 'ty.Iterable[ty.Any]',
        seq2: 'ty.Iterable[ty.Any]',
        delcost: float = 1,
        inscost: float = 1,
        transcost: float = 1.5,
        subcost: float = 1,
        subcosts: 'dict[tuple[str, str], float]' = {},
        ) -> float:
    '''
    Calculate the Levenstein distance between :param:`seq1` and :param:`seq2`.

    .. note:: The calculated value is an absolute measure; a relative measure
        can be calculated by dividing by ``max(len(seq1), len(seq2), 1)``.

    The calculation is done following Levenstein, Damerau (considering
    transpositions) and Homer (performance optimisation: do not store the
    whole matrix, but at most two preceding lines, and make the leftmost
    column to the rightmost). Added here is the option to determine a
    different cost for different substitutions, e.g. ``{('u', 'v'): 0.2}``.
    '''
    preline = None
    line = list(range(1, len(seq2) + 1)) + [0]
    for pos1 in range(len(seq1)):
        prepreline, preline, line = preline, line, [0] * len(seq2) + [pos1 + 1]
        for pos2 in range(len(seq2)):
            line[pos2] = min(
                    preline[pos2] + delcost,
                    line[pos2 - 1] + inscost,
                    preline[pos2 - 1] + subcosts.get(
                        (seq1[pos1], seq2[pos2]),
                        (seq1[pos1] != seq2[pos2]) * subcost))
            if (transcost != delcost + inscost
                    and pos1 > 0
                    and pos2 > 0
                    and seq1[pos1] == seq2[pos2 - 1]
                    and seq1[pos1 - 1] == seq2[pos2]
                    and seq1[pos1] != seq2[pos2]):
                line[pos2] = min(line[pos2], prepreline[pos2 - 2] + transcost)
    return line[len(seq2) - 1]

def ngrams(
        sequence: 'ty.Sequence[ty.Any]',
        n: int,
        ) -> 'ty.Generator[ty.Sequence]':
    '''
    Get n-grams from :param:`sequence` with :param:`n` being the length of
    each n-gram. At the start and at the end, the n-grams are *not* stuffed;
    i.e., the number of yielded n-grams is ``len(sequence) - n + 1``.
    '''
    for i in range(len(sequence) - n + 1):
        yield sequence[i:i + n]

def ngram_ratios(
        sequence: 'ty.Sequence[ty.Any]',
        n: int = 2, # For n = 1, just use ``Counter(sequence)``.
        ) -> 'ty.Generator[tuple[ty.Any, float]]':
    length = len(sequence)
    maximum = 2 # This is an arbitrary number chosen to enter the first loop.
    # If the length, i.e. the number of ngrams for each n, is greater than n
    # or if there are no longer repetitions of ngrams, the loop is left.
    while n <= length and maximum > 1:
        counter = Counter(get_ngrams(sequence, n))
        maximum = max(counter.values())
        for ngram, number in counter.items():
            yield (ngram, number / length)
        length -= 1
        n += 1

def ngram_ratios_diffs(
        sequence: 'ty.Sequence[ty.Any]'
        ) -> 'ty.Generator[tuple[ty.Any, float]]':
    length = len(sequence)
    mono_counter = {
            ngram: number / length
            for ngram, number in Counter(sequence).items() }
    for monogram, ratio in mono_counter.items():
        yield ((monogram,), 0)
    for ngram, ratio in get_ngram_ratios(sequence):
        monoratio = reduce(mul, ( mono_counter[item] for item in ngram ))
        yield (ngram, ratio - monoratio)

def pair(
        text: str,
        border: 're.Pattern[str]',
        opener_group_name: str = 'opener',
        closer_group_name: str = 'closer',
        ) -> 'tuple[int, int, int, int, str]':
    '''
    Get the first result of :func:`pairs`.
    If none is found at all, return ``(-1, -1, -1, -1, '')``.
    '''
    for result in pairs(
                text,
                border,
                opener_group_name,
                closer_group_name,
                ):
        return result
    return (-1, -1, -1, -1, '')

def pairs(
        text: str,
        border: 're.Pattern[str]',
        opener_group_name: str = 'opener',
        closer_group_name: str = 'closer',
        ) -> 'ty.Generator[tuple[int, int, int, int, str]]':
    r'''
    From :param:`text`, yield all matching pairs of delimiters described by
    :param:`border`. Delimiters may be nested. Nested delimiters are
    yielded from the inside outwards; in order to obtain document order,
    the yielded result just needs to be sorted. Because:

    Each pair is a tuple consisting of:

    - the integer position where the opening delimiter starts,
    - the integer position where the opening delimiter ends,
    - the integer position where the closing delimiter starts,
    - the integer position where the closing delimiter ends,
    - the so-called label of this pair. The label is derived from
      :param:`border`, which is explained next.

    :param:`border` must be built as follows (see also the testcode below):

    - It consists entirely of one non-capturing group.
    - This group contains exactly two alternatives.
    - Each of these alternatives is a capturing named group.
    - The one group is named with :param:`opener_group_name`.
    - The other group is named with :param:`closer_group_name`.
    - Note: In general, either of these two groups may come first;
      but if one describes a subsequence of the other, it must come second.
    - Each of the groups may contain one or more arbitrarily named subgroups.
      If a group matches, its named subgroups are joined together to a string,
      henceforth called “label”.
    - If there is no named subgroup, the label is always the empty string,
      and the pairing is not influenced by it.

    An opening and a closing delimiter are paired if and only if

    - they have the same label (which may be the empty string) and
    - they are on the same nesting level.

    .. testcode::
        import re
        import parse

        text = """da steigt <edit mode="add">ja <name>der lange</edit>
                <name>Nostitz</name></name> aus der Versenkung."""
        # Note: Keep calm and carry on.

        border = re.compile(r"""(?x)(?:
                (?P<opener><(?P<label1>[^>/\s]+)(?:"[^"]*"|'[^']*'|[^>/])*>)|
                (?P<closer></(?P<label2>[^>/\s]+)\s*>)
                )""")

        for o_start, o_end, c_start, c_end, label in parse.pairs(text, border):
            print(label, '=', re.sub(r'<.*?>', '', text[o_end:c_start]))

    .. testoutput::
        edit = ja der lange
        name = Nostitz
        name = der lange Nostitz
    '''
    openers = defaultdict(deque)
    for match in border.finditer(text):
        groups = match.groupdict(default = '')
        opener = groups.pop(opener_group_name, '')
        closer = groups.pop(closer_group_name, '')
        label = ''.join(groups.values())
        span = match.span()
        if opener:
            openers[label].append(span)
        elif closer:
            try:
                opening_span = openers[label].pop()
            except IndexError:
                # The closer has no corresponding opener.
                continue
            yield (opening_span[0], opening_span[1], span[0], span[1], label)

def passage(
        text: str,
        match: 're.Match[str]',
        sections_before: int   = 0,
        sections_after: int    = 0,
        section_delimiter: str = '\n',
        chars_before: int      = 0,
        chars_after: int       = 0,
        ) -> str:
    '''
    Get a passage of :param:`text`:

    - If :param:`sections_before` or :param:`sections_after` is not ``0``,
      and if :param:`section_delimiter` is given, then get a passage starting
      from :param`match` and adding as many sections before and after as is
      specified by :param:`sections_before` and :param:`sections_after`.
      Where a section ends and another starts, that is determined by the
      :param:`section_delimiter`.
    - Else, if :param:`chars_before` or :param:`chars_after` is not ``0``,
      then get a passage starting from :param:`match` and adding as many
      characters as is specified by :param:`chars_before` and
      :param:`chars_after`.
    - Else, get only the passage matched by :param:`match`.
    '''
    if any((sections_before, sections_after)) and section_delimiter:
        if sections_before < 0:
            sections_after = max((sections_after, -sections_before))
            sections_before = 0
        elif sections_after < 0:
            sections_before = max((sections_before, -sections_after))
            sections_after = 0
        start = match.start()
        end = match.end()
        for _ in range(sections_before + 1):
            start = text.rfind(section_delimiter, 0, start)
            if start == -1:
                start = 0
                break
        else:
            start += len(section_delimiter)
        for _ in range(sections_after + 1):
            end = text.find(section_delimiter, end)
            if end == -1:
                end = len(text)
                break
            end += len(section_delimiter)
        else:
            end -= len(section_delimiter)
        return text[start:end]
    elif any((chars_before, chars_after)):
        return text[max((match.start() - chars_before, 0)):
                    min((match.end() + chars_after, len(text)))]
    else:
        return match.group()

def roman_to_arabic(
        number: str,
        numeral_map: 'tuple[tuple[int, str]]' = ROMAN_NUMBERS_MAP,
        ) -> int:
    '''
    Attribution notice:
    In essence by Mark Pilgrim, Paul Winkler, Tim Valenta, dln385.
    See http://code.activestate.com/recipes/81611-roman-numerals/.
    '''
    number = number.upper()
    i = result = 0
    for integer, numeral in numeral_map:
        while number[i:i + len(numeral)] == numeral:
            result += integer
            i += len(numeral)
    if i < len(number):
        return 0
    return result

def sortday(
        value: 'float|int|str|time.struct_time',
        local: bool = False,
        date_re: 're.Pattern[str]' = re.compile(DATE),
        ) -> int:
    '''
    Get a comparison-friendly form of :param:`value` – i.e. turn this value into
    a tagesgequantelte raafzeit.

    The value may be:

    - an ``int`` or ``float``: then take it as seconds form the epoch in UTC, if
      :param:`local` is False (the default), otherwise in local time.
    - a ``str``: then take it as a date of the pattern :param:`date_re`.
    - a ``time.struct_time``.

    When in doubt, return ``0``.

    >>> sortday('1832-3-22')
    18320322
    >>> sortday('28.8.1749')
    17490828
    >>> sortday('19.8.14')
    140819
    >>> sortday('23. 9. 63 BC')
    -630923
    >>> sortday('399 v. Chr.')
    -3990000
    '''
    if isinstance(value, (float, int)):
        try:
            value: time.struct_time = (
                    time.localtime(value) if local
                    else time.gmtime(value)
                    ) # will be processed further below.
        except: # Sic.
            return 0
    if isinstance(value, time.struct_time):
        return int(time.strftime('%Y%m%d', value))
    elif isinstance(value, str) and (m := date_re.search(value)):
        return int(
                ('-' if m.group('minus') else '')
                + (m.group('y1') or m.group('y2')
                    or m.group('y3') or m.group('y4') or m.group('y5'))
                + (m.group('m1') or m.group('m2')
                    or m.group('m3') or m.group('m4') or '').zfill(2)
                + (m.group('d1') or m.group('d2') or '').zfill(2)
                )
    return 0

def sortday_in_iso(
        day: int,
        day_re: 're.Pattern[str]' = re.compile(r'(\d+)(\d\d)(\d\d)\Z'),
        ) -> str:
    return day_re.sub(r'\g<1>-\g<2>-\g<3>', str(day))

def sortnum(
        number: str,
        length: int = 8,
        strip_re: 'None|re.Pattern[str]' = re.compile(r'(^\D+|\D+$)'),
        split_re: 'None|re.Pattern[str]' = re.compile(r'\D+'),
        ) -> str:
    '''
    Convert :param:`number` to a string for alphanumerical sorting, so that e.g.
    ``'2'`` comes before ``'10'``:

    - Delete all matches of :param:`strip_re` (if it is not ``None``).
    - Split :param:`number` into subnumbers with :param:`split_re` (if it is not
      ``None``, otherwise :param:`number` is the one and only subnumber).
    - Stretch each subnumber to the length of :param:`length`.
    - Join the subnumbers to one string and return it.

    >>> sortnum('Bd. 12,3/8?')
    '000000120000000300000008'
    '''
    number = str(number)
    if strip_re:
        number = strip_re.sub(r'', number)
    if number:
        if split_re:
            numbers = split_re.split(number)
        else:
            numbers = [number]
        number  = ''.join( n.zfill(length) for n in numbers )
    return number

def sortnumtext(
        term: str,
        number_re: 're.Pattern[str]' = re.compile(r'\d+([^\w\d]+(?=\d))?'),
        length: int = 8,
        strip_re: 'None|re.Pattern[str]' = re.compile(r'(^\D+|\D+$)'),
        split_re: 'None|re.Pattern[str]' = re.compile(r'\D+'),
        ) -> str:
    '''
    In :param:`term`, substitute each match of :param:`number_re` with the value
    returned by :func:`sortnum` when applied to the match. See this function for
    the remaining arguments.

    >>> sortnumtext('Bd. 12,3/8?')
    'Bd. 000000120000000300000008?'
    '''
    return number_re.sub(
            lambda m: sortnum(m.group(), length, strip_re, split_re),
            term)

def sortxtext(
        term: str,
        pre: 'tuple[tuple[re.Pattern[str], str], ...]' = XTAG_PRE,
        acts: 'tuple[ty.Callable[[str], str], ...]' = (sortnumtext,),
        ) -> str:
    '''
    >>> sortxtext('<i>Thé</i> &amp; <b>Café</b> 47/11 ...')
    'the cafe 0000004700000011'
    '''
    return even(term, pre = pre, acts = acts)

def span(
        text: str,
        opener: str,
        closer: str,
        startpos: int = 0,
        ) -> 'tuple[int, int]':
    '''
    Search in :param:`text` from :param:`startpos` on.
    Return the position where the first :param:`opener` starts that has
    a matching :param:`closer`, and return the position where this
    :param:`closer` ends. If no pair is found, ``(-1, -1)`` is returned.

    .. note::
        In the following testcode, the start position and the insensible
        delimiters have the single purpose to show the capability of the
        function in a nutshell. The text is a quotation from A. Stifter.

    .. note::
        See also :func:`pair` and :func:`pairs`.

    >>> text = '.und.. .in dem .brechenden.. Herzen.. bist du...'
    >>> print(text[slice(*span(text, '.', '..', 3))])
    .in dem .brechenden.. Herzen..
    '''
    # Some precalculations to relieve the loops.
    len_text       = len(text)
    len_opener     = len(opener)
    len_closer     = len(closer)
    opener_shorter = len_opener < len_closer
    shorter        = opener if opener_shorter else closer
    longer         = closer if opener_shorter else opener
    len_shorter    = len_opener if opener_shorter else len_closer
    len_longer     = len_closer if opener_shorter else len_opener
    shorter_add    = 1 if opener_shorter else -1
    longer_add     = -1 if opener_shorter else 1
    endpos         = -1
    pos            = startpos
    level          = 0 # the nesting level
    # First, handle the case that opener and closer are identical;
    # thus, no nesting is possible.
    if opener == closer:
        pos = text.find(opener, startpos)
        if pos == -1:
            return -1, -1
        endpos = text.find(opener, pos + 1)
        if endpos == -1:
            return -1, -1
        return pos, endpos + len_closer
    # Handle the general case. First, find the first opener.
    while pos < len_text:
        if text[pos:pos + len_longer] == longer:
            startpos = pos
            pos += len_longer
            if longer == opener:
                level = 1
                break
        elif text[pos:pos + len_shorter] == shorter:
            startpos = pos
            pos += len_shorter
            if shorter == opener:
                level = 1
                break
        else:
            pos += 1
    if level == 1:
        while pos < len_text:
            if text[pos:pos + len_longer] == longer:
                level += longer_add
                pos += len_longer
            elif text[pos:pos + len_shorter] == shorter:
                level += shorter_add
                pos += len_shorter
            else:
                pos += 1
            if level == 0:
                endpos = pos
                break
    if endpos == -1:
        startpos = -1
    return startpos, endpos

def transletter(
        term: str,
        names: 'dict[str, str]' = BASENAMES,
        rename: 'ty.Callable[[str], str]' =
            lambda name: ' '.join(name.split()[:4]),
        ) -> str:
    '''
    Replace each character in :param:`term` with an ersatz derived from the name
    that the character has in the Unicode standard:

    - If the Unicode name is a key in :param:`names`, the ersatz is the value of
      this key.
    - If the Unicode name when having been changed with :param:`rename` is a key
      in :param:`names`, the ersatz is the value of this key.
    - Otherwise, the ersatz is the empty string.

    >>> transletter('ꬴ') # LATIN SMALL LETTER E WITH FLOURISH
    'e'
    '''
    return ''.join(
            names.get((name := ud.name(char, '')), names.get(rename(name), ''))
            for char in term
            )

def trunc_utf8(
        term: bytes,
        free: int,
        filler_byte: bytes = b'\n',
        ) -> 'tuple[bytes, int, int]':
    '''
    Truncate the string of bytes :param:`term` so that it is no longer than just
    :param:`free` bytes. Return:

    - the possibly truncated string of bytes.
    - the number of bytes lost due to a reduction down to :param:`free`. If this
      number is negative, itʼs the number of bytes :param:`term` is shorter than
      :param:`free`.
    - the number of bytes lost due to a repair – if a repair was necessary after
      truncation in order to regain a valid UTF-8 string of bytes.

    If for such a repair bytes must be discarded, each of the discarded bytes is
    replaced with :param:`filler_byte`, which must be one byte or empty.

    >>> new, reduction_loss, repair_loss = trunc_utf8('αβ'.encode(), 3, b'~')
    >>> print(new.decode(), f'{new=}', f'{reduction_loss=}', f'{repair_loss=}')
    α~ new=b'\xce\xb1~' reduction_loss=1 repair_loss=1
    '''
    assert len(filler_byte) in {0, 1}
    needed = len(term)
    reduction_loss = needed - free
    repair_loss = 0
    if free < needed:
        i = free
        while i > 0:
            i -= 1
            bytenum = term[i]
            if bytenum < 128:
                i += 1
                break
            elif 191 < bytenum < 248:
                diff = free - i
                if (
                        (diff == 2 and bytenum < 224) or
                        (diff == 3 and 223 < bytenum < 240) or
                        (diff == 4 and 239 < bytenum)
                        ):
                    i = free
                break
        repair_loss = free - i
        term = term[:i] + (filler_byte * repair_loss)
    return term, reduction_loss, repair_loss

if __name__ == '__main__':
    import doctest
    doctest.testmod()
