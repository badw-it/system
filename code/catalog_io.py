﻿# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Access to a library catalog via URL query and extraction of certain data
from the response sent back by the catalog.
'''
try: import regex as re
except ImportError: import re
import xml.etree.ElementTree as ET
from collections import deque
from urllib.parse import quote_plus

import parse
import xmlhtml
import web_io

SRU = {
        'query': '',
        'maximumRecords': 1,
        'startRecord': 1,
        'extraRequestData': '',
        'operation': 'searchRetrieve',
        'recordPacking': 'xml',
        'recordSchema': 'marcxml',
        'recordXPath': '',
        'resultSetTTL': 0,
        'sortKeys': '',
        'stylesheet': '',
        'version': '1.1',
        }

class Catalog:
    '''
    Provide access to a library catalog via URL query. Provide methods for
    extracting data from the items sent back by the catalog.

    .. important::
        - When extracting from the catalog item, leading and trailing
          whitespace is removed from the field values always except for
          the case of identifiers like the publication key.
        - Except from that, the content of the field is given as-is;
          unescaping is not done in order to abstract from
          implementation details of the catalog.

    >>> import catalog_io
    >>> catalog = catalog_io.Catalog('http://bvbr.bib-bvb.de:5661/bvb01sru')
    >>> item = catalog.read(
    ...         catalog_io.SRU | {'query': 'marcxml.idn=BV022951212'})
    >>> year = catalog.get_year(item)
    >>> print(year)
    2007
    '''
    def __init__(
            self,
            url: 'None|str',
            item_form: str = 'marc',
            ):
        '''
        :param url: URL to the library catalog, i.e. the URL of a valid query
            in this catalog *excluding* the query part starting with the
            question mark. If ``None``, no catalog gets connected. This option
            exists so that you can use the methods e.g. for conversion or
            extraction without a connection to an online catalog but instead
            with an export of catalog data.
        :param item_form: determines how to
            process the response of a query, especially how to extract
            information from the serialized items returned in the response.
            At the moment, only MARC-XML is implemented.
        '''
        self.url = url
        self.item_form = item_form

    def get_edition(self, item: ET.Element) -> 'tuple[str, str]':
        '''
        Extract from :param:`item` the so-called edition statement about the
        publication described by the item.
        The edition statement consists of the statement itself and an optional
        remainder in another subfield. They are returned as a pair of strings.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return (
                    xmlhtml.get_text(
                        item, './/*[@tag="250"]/*[@code="a"]').strip(),
                    xmlhtml.get_text(
                        item, './/*[@tag="250"]/*[@code="b"]').strip(),
                    )
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_extent(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` the extent of the publication
        described by the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            extent = xmlhtml.get_text(item, './/*[@tag="300"]/*[@code="a"]')\
                     .strip().replace(' ', '\u202f')
            if extent and extent[-1].isdigit():
                extent += '\u202fS.'
            return extent
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_extent_accompanying_material(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` a statement about any accompanying material
        of the publication described by the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return xmlhtml.get_text(
                    item, './/*[@tag="300"]/*[@code="e"]').strip()
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_extent_other(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` a statement about further extensional
        features of the publication described by the item, e.g., whether
        there are any illustrations.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return xmlhtml.get_text(
                    item, './/*[@tag="300"]/*[@code="b"]').strip()
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_genre(self, item: ET.Element) -> 'Generator[tuple[str, str, str]]':
        '''
        Extract from :param:`item` statements about the genre of the
        publication described in the item. The statements include, if
        available, an identifying key for the genre, the genre as a verbal
        expression and a hint at the source from which the ID and the
        expression are taken.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            for subitem in item.iterfind('.//*[@tag="655"]'):
                genre_id = xmlhtml.get_text(subitem, './/*[@code="0"]').strip()
                genre_text = xmlhtml.get_text(
                        subitem, './/*[@code="a"]').strip()
                genre_source = xmlhtml.get_text(
                        subitem, './/*[@code="2"]').strip()
                yield genre_id, genre_text, genre_source
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_isbns_issns(self, item: ET.Element) -> 'Generator[str]':
        '''
        Extract from :param:`item` the ISBNs or ISSNs of the publication
        described in the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            for subitem in item.iterfind('.//*[@tag="020"]'):
                number = xmlhtml.get_text(subitem, './/*[@code="9"]').strip()
                if number:
                    yield number
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_keywords(
            self,
            item: ET.Element,
            classification_system: str,
            ) -> 'Generator[tuple[str, str]]':
        '''
        Extract from :param:`item` keywords attached to the publication
        described by the item. Extract only the keywords that are given
        in the classification system :param:`classification_system`, and
        return them together with a string indicating the subsystem. For
        instance, ``'084'`` is the classification system ‘Other’ – which
        might be specified by the subsystem string ``'rvk'`` (that means
        ‘Regensburger Verbundkatalog’).
        '''
        for subitem in item.iterfind(f'.//*[@tag="{classification_system}"]'):
            keyword = xmlhtml.get_text(subitem, './/*[@code="a"]').strip()
            if keyword:
                yield (
                        xmlhtml.get_text(subitem, './/*[@code="2"]').strip(),
                        keyword,
                        )

    def get_note(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` a general note about the publication
        described by the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return xmlhtml.get_text(
                    item, './/*[@tag="500"]/*[@code="a"]').strip()
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_personal_names_ids_roles(
            self,
            item: ET.Element,
            delete_from_id: 'None|re.Pattern[str]' =
                    re.compile(r'^\(.*?\)'),
            ) -> 'Generator[tuple[str, str, str, str, str]]':
        '''
        Extract from :param:`item` for any recorded person the name and,
        if given, the person identifier (e.g. the GND) and the role this
        person plays with regard to the publication described by the item
        (e.g. a contributor to a lexicon). The name consists of the name
        itself, optionally an enumeration (e.g. in ``'Friedrich II.'``) and
        optionally another addition associated with the name, e.g. a title.
        An example return value could be:
        ``[('Friedrich', 'II.', 'König von Preußen', '118535749', 'aut')]``

        :param item: A catalog item in ``self.item_form``.
        :param delete_from_id: If not ``None``, a pattern whose match is
            deleted from the key. The default deletes a leading parenthesis,
            so that e.g. ``'(DE-588)11875145X'`` becomes ``'BV006182977'``.
        '''
        if self.item_form == 'marc':
            for field_id in (100, 700):
                for subitem in item.iterfind(f'.//*[@tag="{field_id}"]'):
                    name = xmlhtml.get_text(subitem, './/*[@code="a"]').strip()
                    name_enum = xmlhtml.get_text(
                            subitem, './/*[@code="b"]').strip()
                    name_add = xmlhtml.get_text(
                            subitem, './/*[@code="c"]').strip()
                    person_id = xmlhtml.get_text(
                            subitem, './/*[@code="0"]').strip()
                    role = xmlhtml.get_text(subitem, './/*[@code="4"]').strip()
                    if delete_from_id:
                        person_id = delete_from_id.sub('', person_id.strip())
                    yield (name, name_enum, name_add, person_id, role)
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_place(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` the place of the publication
        described by the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return xmlhtml.get_text(
                        item, './/*[@tag="264"]/*[@code="a"]').strip() \
                    or xmlhtml.get_text(
                        item, './/*[@tag="260"]/*[@code="a"]').strip()
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_pub_id(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` the catalog-specific key of the publication
        (e.g. the BV number of the B3Kat).

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return xmlhtml.get_text(item, './/*[@tag="001"]')
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_publisher(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` the publisher of the publication
        described by the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            return xmlhtml.get_text(
                        item, './/*[@tag="264"]/*[@code="b"]').strip() \
                    or xmlhtml.get_text(
                        item, './/*[@tag="260"]/*[@code="b"]').strip()
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_series_id_serial_num(
            self,
            item: ET.Element,
            delete_from_id: 'None|re.Pattern[str]' =
                    re.compile(r'(^\s+|\s+$|^\(.*?\))'),
            ) -> 'dict[str, deque[tuple[str, str]]]':
        '''
        Extract from :param:`item` the series keys and the serial numbers
        of the publication described by the item.
        Get them from all of the catalog fields that may contain them.
        Consider multiple occurrences of the same field.
        Map the id of the field (e.g. ``'810'``) to all the pairs of series key
        and serial number which are found with this field id.
        Return this mapping.

        :param item: A catalog item in ``self.item_form``.
        :param delete_from_id: If not ``None``, a pattern whose match is
            deleted from the key. The default deletes a leading parenthesis,
            so that e.g. ``'(DE-604)BV006182977'`` becomes ``'BV006182977'``.
        '''
        if self.item_form == 'marc':
            result = {}
            for field_id in ('830', '811', '810', '773', '772'):
                subitems = item.findall(f'.//*[@tag="{field_id}"]')
                subresults = deque()
                for subitem in subitems:
                    series_id = xmlhtml.get_text(
                            subitem, './/*[@code="w"]').strip()
                    if not series_id:
                        continue
                    subkey = 'g' if field_id in {'773', '772'} else 'v'
                    serial_num = xmlhtml.get_text(
                            subitem,
                            f'.//*[@code="{subkey}"]').strip()
                    subresults.append((
                            delete_from_id.sub('', series_id)
                                if delete_from_id else series_id,
                            serial_num,
                            ))
                if subresults:
                    result[field_id] = subresults
            return result
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_title_addition(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` a statement adding information to the
        title like further authors or editors. If this field is not
        available, an empty string is returned.

        :param item: A catalog item in ``self.item_form``.
        '''
        subitem = item.find('.//*[@tag="245"]')
        if subitem is None:
            return '', '', ''
        return xmlhtml.get_text(subitem, './/*[@code="c"]').strip()

    def get_titles(self, item: ET.Element) -> 'tuple[str, str, str]':
        '''
        Extract from :param:`item` the toptitle and, if given, the subtitle
        and the sectiontitle of the publication described by the item.
        For any of the three that is not given in the title field, the empty
        string is returned. If the field itself is not available, three
        empty strings are returned.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            subitem = item.find('.//*[@tag="245"]')
            if subitem is None:
                return '', '', ''
            toptitle = xmlhtml.get_text(subitem, './/*[@code="a"]').strip()
            subtitle = xmlhtml.get_text(subitem, './/*[@code="b"]').strip()
            sectiontitle = xmlhtml.get_text(subitem, './/*[@code="p"]').strip()
            return toptitle, subtitle, sectiontitle
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_urls(self, item: ET.Element) -> 'Generator[str]':
        '''
        Extract from :param:`item` URLs assigned to the publication described by
        the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            for subitem in item.iterfind('.//*[@tag="856"]'):
                yield xmlhtml.get_text(subitem, './/*[@code="u"]').strip()
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def get_year(self, item: ET.Element) -> str:
        '''
        Extract from :param:`item` the year of the publication
        described by the item.

        :param item: A catalog item in ``self.item_form``.
        '''
        if self.item_form == 'marc':
            marc008 = xmlhtml.get_text(item, './/*[@tag="008"]')
            return xmlhtml.get_text(
                        item, './/*[@tag="264"]/*[@code="c"]').strip() \
                    or xmlhtml.get_text(
                        item, './/*[@tag="260"]/*[@code="c"]').strip() \
                    or (marc008[7:11] if len(marc008) > 10 else '')
        else:
            raise NotImplementedError(
                    f'Item form “{self.item_form}” is not implemented.')

    def read(
            self,
            query: 'dict[str, float|int|str]',
            request_kwargs: 'dict[str, Any]' = {},
            urlopen_kwargs: 'dict[str, Any]' = {},
            ) -> ET.Element:
        '''
        Query the catalog with :param:`query` and return the answer as a string.
        For an example, see the docstring of this class.

        :param request_kwargs: Cf. the docstring of :func:`web_io.read`.
        :param urlopen_kwargs: Cf. the docstring of :func:`web_io.read`.
        '''
        return ET.XML(
                web_io.read(
                    self.url
                    + '?'
                    + '&'.join( '{}={}'.format(
                        key,
                        quote_plus(str(val), safe = '')
                        ) for key, val in query.items() if val ),
                    request_kwargs = request_kwargs,
                    urlopen_kwargs = urlopen_kwargs,
                    ))

def format_title_sorttitle(
        toptitle: str,
        subtitle: str,
        sectiontitle: str,
        punctuation_re: 'None|re.Pattern[str]' = re.compile(r'[.:!?]\W*$'),
        substitutes: 'tuple[tuple[str, str], ...]' = (),
        ) -> 'tuple[str, str]':
    '''
    Combine :param:`title`, :param:`subtitle`, :param:`sectiontitle`.

    Ensure, that the two latter ones start with an upper case letter.

    Join them with a full stop and a space – omit the full stop if the preceding
    part does not match :param:`punctuation_re`.

    Return the resulting string and a sort-friendly version derived from it.
    '''
    if toptitle and (subtitle or sectiontitle):
        if subtitle:
            subtitle = subtitle[0].upper() + subtitle[1:]
        if sectiontitle:
            sectiontitle = sectiontitle[0].upper() + sectiontitle[1:]
        if not punctuation_re.search(toptitle):
            toptitle += '.'
        toptitle += ' '
        if subtitle and sectiontitle:
            if not punctuation_re.search(subtitle):
                subtitle += '.'
            subtitle += ' '
    title = re.sub(r'\s+', ' ', toptitle + subtitle + sectiontitle)
    # Use the non-sorting marks:
    sorttitle = re.sub(r'\x98[^\x9c]*\x9c', '', title)
    sorttitle = parse.even(sorttitle, pre = substitutes + parse.PRE)
    for old, new in (
            ('\x98', ''),
            ('\x9c', ''),
            (' ;', ';'),
            (' :', ':'),
            ):
        title = title.replace(old, new)
    return title, sorttitle

if __name__ == '__main__':
    import doctest
    doctest.testmod()

    # exempla et specimina.
    bvs = r'''

            '''
    bvs = [ bv.strip() for bv in bvs.split() if bv.strip() ]

    catalog = Catalog('http://bvbr.bib-bvb.de:5661/bvb01sru')
    for rank, bv in enumerate(bvs):
        item = catalog.read(SRU | {'query': 'marcxml.idn=' + bv})
        print(xmlhtml.serialize_elem(item))
        # series = catalog.get_series_id_serial_num(item)
        # num = tuple(series.values())[0][-1][-1]
        # titles = catalog.get_titles(item)
        # print(bv, series, titles)
        # for person in catalog.get_personal_names_ids_roles(item):
        #     print(bv, person)
        print('███████████████████████████████████████████████████████████')
