# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)
import email.mime.text
import email.policy
import smtplib
import ssl

def del_cr_lf(term: str) -> str:
    return term.replace('\r', '').replace('\n', '')

def send(
        *,
        host: str = 'localhost',
        port: int = 25,
        user: str = '',
        password: str = '',
        subtype: str = 'plain',
        charset: str = 'utf-8',
        policy: 'email.policy.EmailPolicy' = email.policy.SMTP,
        From: str = '',
        Reply_to: str = '',
        To: str = '',
        Cc: str = '',
        Bcc: str = '',
        Subject: str = '',
        text: str = '',
        ) -> None:
    '''
    Send an email or raise a :err:`ConnectionRefusedError`.

    .. important::
        All arguments starting with a capital letter are email-header values and
        must not contain a carriage return or a linefeed. Accordingly, these two
        characters are deleted from them.

    :param host: Hostname of a SMTP server running on the same machine.
    :param port: Portnumber of said SMTP server.
    :param user: User known on said SMTP server.
    :param password: Password of said user.
    :param subtype: is :param:`email.mime.text.MIMEText._subtype`.
    :param charset: is :param:`email.mime.text.MIMEText._charset`.
    :param policy: is :param:`email.mime.text.MIMEText.policy`, but has another
        default in order to comply with https://tools.ietf.org/html/rfc5322.html
        (as required by e.g. web.de).
    :param From: The email address of the sender. In the case of automatically
        sent mails, this is often meaningless for recipients. It must not be
        spoofed, but can be decorated by a much more meaningful name, e.g.:
        :str:`"Ptolemaeus" <root+badwver-web01@srv.mwn.de>`.
    :param Reply_to: In the case of automatically sent mails, the sender should
        not be the recipient for replies. This entry is offered instead, e.g.
        :str:`info@ptolemaeus.badw.de`.
    :param To: One or more email addresses of visible addressees.
        A comma-plus-blank-separated list, if more than one.
    :param Cc: Zero or more email addresses of visible side-addressees.
        A comma-plus-blank-separated list, if more than one.
    :param Bcc: Zero or more email addresses of invisible side-addressees.
        A comma-plus-blank-separated list, if more than one.
    :param Subject: A subject or the empty string.
    :param text: The content of the email.
    '''
    message = email.mime.text.MIMEText(
            text, _subtype = subtype, _charset = charset, policy = policy)
    message['From'] = del_cr_lf(From)
    if Reply_to:
        message['Reply-to'] = del_cr_lf(Reply_to)
    message['To'] = del_cr_lf(To)
    if Cc:
        message['Cc'] = del_cr_lf(Cc)
    if Bcc:
        message['Bcc'] = del_cr_lf(Bcc)
    message['Subject'] = del_cr_lf(Subject)
    with smtplib.SMTP(host = host, port = port) as server:
        if user and password:
            server.starttls(context = ssl.create_default_context())
            server.login(user, password)
        server.send_message(message)
