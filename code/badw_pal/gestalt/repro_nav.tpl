% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% first_pagina = item['paginas'][0][0]
% last_pagina = item['paginas'][-1][0]
			<div{{!' dir="rtl"' if rtl else ''}}>\\
% if pagina != first_pagina:
<a class="arrowleftstop key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/{{first_pagina}}{{query}}" rel="first">First</a>\\
% else:
<span class="arrowleftstop disabled key">First</span>\\
% end
% if item['prev']:
<a class="arrowleft key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/{{item['prev']}}{{query}}" rel="prev">Previous</a>\\
% else:
<span class="arrowleft disabled key">Previous</span>\\
% end
% if item['next']:
<a class="arrowright key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/{{item['next']}}{{query}}" rel="next">Next</a>\\
% else:
<span class="arrowright disabled key">Next</span>\\
% end
% if pagina != last_pagina:
<a class="arrowrightstop key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/{{last_pagina}}{{query}}" rel="last">Last</a>\\
% else:
<span class="arrowrightstop disabled key">Last</span>\\
% end
</div>
