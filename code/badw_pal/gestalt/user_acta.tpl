% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% user = kwargs['user']
% title = user['user_title']
% degree = user['user_degree']
<main>
<article class="just_ahyph sheet wide">
  % if request.roles:
	<button onclick="listexport(event, 'article > p > .key', '', 0, '?export&amp;form=editor')">Word Export</button>
	<button onclick="listexport(event, 'article > p > .key', '', 0, '?export&amp;form=print')">LaTeX Export</button>
  % end
	<h1>Descriptions by <a href="/team/{{user['user_id']}}">{{title + ' ' if title else ''}}{{user['user_firstname']}} {{user['user_lastname']}}{{', ' + degree if degree else ''}}</a></h1>
  % pre_title = ''
  % for sortnum, sortname, title, name, item_id, link in sorted(kwargs['acta']):
	% if title != pre_title:
	<h2>{{title}}</h2>
	  % pre_title = title
	% end
	<p><a class="key" href="{{link}}">{{!name}}</a></p>
  % end
</article>
</main>