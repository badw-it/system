% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% modified_on = str(item['work_modified_on']).split()[0]
% modified_on_shown = '.'.join(reversed(modified_on.split('-')))
<main>
<article class="just_ahyph sheet wide">
	<nav class="catalognav">
	  % if item['prev']:
		<a class="arrowleft key" href="/work/{{item['prev'][0]}}" rel="prev" title="{{item['prev'][1]}} {{item['prev'][2]}}">Previous</a>
	  % else:
		<span class="arrowleft disabled key">Previous</span>
	  % end
	  % if item['next']:
		<a class="arrowright key" href="/work/{{item['next'][0]}}" rel="next" title="{{item['next'][1]}} {{item['next'][2]}}">Next</a>
	  % else:
		<span class="arrowright disabled key">Next</span>
	  % end
	</nav>
	<p>Work {{!item['work_siglum']}}</p>
	<h1{{!' style="font-size: 1.5em"' if (item['lang_id'] == 1 and len(item['work_title']) > 40) else ''}}>{{item['person_name']}}<br/>
  % if item['work_title_arabic']:
	<span dir="rtl">{{!item['work_title_arabic']}}</span><br/>
  % end
	{{!item['work_title']}}</h1>
% if item['lang_id'] == 1 and item['work_other_titles']:
	<section class="petit">{{!item['work_other_titles']}}</section>
% end
% if item['work_origin']:
	<section>{{!item['work_origin']}}</section>
% end
% if item['work_origin_ext']:
	<section>{{!item['work_origin_ext']}}</section>
% end
% if item['work_content']:
	<section>{{!item['work_content']}}</section>
% end
% if item['work_note']:
	<section>{{!item['work_note']}}</section>
% end
% if item['work_quotation']:
	<section>{{!item['work_quotation']}}</section>
% end
% if item['work_bibliography']:
	<section>{{!item['work_bibliography']}}</section>
% end
% if item['work_printhistory']:
	<section>{{!item['work_printhistory']}}</section>
% end
	<section>
	<table class="parts with_subitems">
	  % if item['printparts']:
		<tr>
			<th scope="row"><div>EDS</div></th>
			<td>
			  % for printpart in item['printparts']:
			  %     scan_link = printpart.get('scan_link', '')
			  %     tran_link = printpart.get('tran_link', '')
				<div>
				% if printpart['print_hidden'] == 'F':
					<a class="inline" href="/print/{{printpart['print_id']}}">{{printpart['place_name']}}, {{printpart['print_publisher']}}{{!printpart['printpart_info']}}</a>&#8195;
				  % if scan_link and tran_link:
					<a class="dark key" href="{{scan_link}}">Images|Text</a>
				  % elif scan_link:
					<a class="dark key" href="{{scan_link}}">Images</a>
				  % elif tran_link:
					<a class="dark key" href="{{tran_link}}">Text</a>
				  % end
				% else:
					<span class="inline">{{printpart['place_name']}}, {{printpart['print_publisher']}}{{!printpart['printpart_info']}}</span>
				% end
				</div>
			  % end
			</td>
		</tr>
	  % end
	  % if item['msparts']:
		<tr>
			<th scope="row"><div>MSS</div></th>
			<td>
			  % for mspart in item['msparts']:
			  %     scan_link = mspart.get('scan_link', '')
			  %     tran_link = mspart.get('tran_link', '')
			  %     place_name = mspart['place_name'].lstrip('?')
			  %     place_name = place_name + ', ' if place_name else ''
				<div>
				% if mspart['ms_hidden'] == 'F':
					<a class="inline" href="/ms/{{mspart['ms_id']}}">{{place_name}}<abbr title="{{mspart['lib_name']}}">{{mspart['lib_abbrev'] or mspart['lib_name']}}</abbr>, {{!mspart['ms_shelfmark']}}{{!mspart['mspart_info'] or ', ' + mspart['pagina_range']}}</a>&#8195;
				  % if scan_link and tran_link:
					<a class="dark key" href="{{scan_link}}">Images|Text</a>
				  % elif scan_link:
					<a class="dark key" href="{{scan_link}}">Images</a>
				  % elif tran_link:
					<a class="dark key" href="{{tran_link}}">Text</a>
				  % end
				% else:
					<span class="inline">{{place_name}}<abbr title="{{mspart['lib_name']}}">{{mspart['lib_abbrev'] or mspart['lib_name']}}</abbr>, {{!mspart['ms_shelfmark']}}{{!mspart['mspart_info'] or ', ' + mspart['pagina_range']}}</span>
				% end
				</div>
			  % end
			</td>
		</tr>
	  % end
	</table>
	</section>
  % if 'mspartdata' in request.query:
	<section>
  % for mspart in item['msparts']:
  %     scan_link = mspart.get('scan_link', '')
  %     tran_link = mspart.get('tran_link', '')
  %     place_name = mspart['place_name'].lstrip('?')
  %     place_name = place_name + ', ' if place_name else ''
	<h3 style="font-size: 1.1em; margin-bottom: 0"><a href="/ms/{{mspart['ms_id']}}">{{place_name}}{{mspart['lib_name']}}, {{!mspart['ms_shelfmark']}}</a></h3>
	<table class="parts">
		<tr id="{{mspart['part_id']}}" role="row">
			<th scope="row">
				<div title="part id: {{mspart['part_id']}}">{{!mspart['pagina_range']}}</div>
			  % if scan_link and tran_link:
				<div><a class="dark key" href="{{scan_link}}">Images|Text</a></div>
			  % elif scan_link:
				<div><a class="dark key" href="{{scan_link}}">Images</a></div>
			  % elif tran_link:
				<div><a class="dark key" href="{{tran_link}}">Text</a></div>
			  % end
			</th>
			<td>
				<div>{{!mspart['mspart_quotation']}}</div>
				<div class="inline">=&#8239;<a href="/work/{{item['work_id']}}">{{'' if 'Anonymous' in item['person_name'] else item['person_name'] + ', '}}{{!item['work_title']}} ({{item['work_siglum']}})</a>{{!mspart['mspart_note']}}</div>
			</td>
		</tr>
	</table>
  % end
	</section>
  % end
% if item['lang_id'] == 1:
  % for lang in db.get_langs_sorted():
  %     for label in ('translations', 'commentaries'):
  %         coworks = item.get(label, {}).get(lang['lang_id'], [])
  %         if coworks:
	<section>
		<h2>{{lang['lang_name']}} {{label}}</h2>
		<ul class="with_subitems">
  %             for cowork in coworks:
			<li><p><a href="/work/{{cowork['work_id']}}">{{'' if 'Anonymous' in cowork['person_name'] else cowork['person_name'] + ', '}}{{!cowork['work_title']}} ({{cowork['work_siglum']}})</a></p></li>
  %             end
		</ul>
	</section>
  %         end
  %     end
  % end
% end
% include('citebox.tpl', item = item, section = 'work', sectiontitle = 'Works')
</article>
</main>