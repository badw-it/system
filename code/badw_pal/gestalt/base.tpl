% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs.setdefault('lang_id', db.lang_id)
% note = kwargs.get('note', '')
% query = '?' + request.query_string if request.query_string else ''
% include('head.tpl', db = db, request = request, kwargs = kwargs)
<body>
% if note:
<input class="flip" id="flip1" type="checkbox"/><label class="key notice top" for="flip1"></label><article class="card">{{note}}</article>
% end
% if db.debug:
<p style="color: #ff4050; font-family: 'Trebuchet MS'; font-size: larger; font-weight: bold; position: fixed; right: 8px; text-shadow: 0.5px 0.5px 1px #a00030, 0 0 6px #ffffff; z-index: 100">TEST VERSION</p>
% end
<header>
	<a class="img" href="/"><img class="shadow" src="/icons/main.png" alt="PAL" title="MS Klosterneuburg, Stiftsbibliothek, 682, fol. 1r"/></a>
	<h1 lang="la">{{!'Ptolemaeus <small>Arabus et Latinus</small>' if request.path == '/start' else '<a href="/">Ptolemaeus <small>Arabus et Latinus</small></a>'}}</h1>
	<form action="/filter" method="get">
		<input type="search" name="text" value="{{request.query.text}}" aria-label="{{db.glosses['search_term'][lang_id]}}" placeholder="Fulltext search"/>
		<div class="droparea shadow">
			<table>
				<tr><th><kbd>_</kbd></th> <td> (the underscore) is the placeholder for exactly one character.</td></tr>
				<tr><th><kbd>%</kbd></th> <td> (the percent sign) is the placeholder for no, one or more than one character.</td></tr>
				<tr><th><kbd style="white-space: nowrap">%%</kbd></th> <td> (two percent signs) is the placeholder for no, one or more than one character, but not for blank space (so that a search ends at word boundaries).</td></tr>
			</table>
			<p class="petit">At the beginning and at the end, these placeholders are superfluous.</p>
		</div><button type="submit">&#128270;&#65038;</button>
	</form>
	<nav>
		<ul>
			<li>{{!'<b>Start</b>'        if request.path == '/start'        else '<a href="/start">Start</a>'}}</li>
			<li>{{!'<b>Project</b>'      if request.path == '/project'      else '<a href="/project">Project</a>'}}</li>
			<li>{{!'<b>Team</b>'         if request.path == '/team'         else '<a href="/team">Team</a>'}}</li>
			<li>{{!'<b>News</b>'         if request.path == '/news'         else '<a href="/news">News</a>'}}</li>
			<li>{{!'<b>Jobs</b>'         if request.path == '/jobs'         else '<a href="/jobs">Jobs</a>'}}</li>
			<li>{{!'<b>Publications</b>' if request.path == '/publications' else '<a href="/publications">Publications</a>'}}</li>
			<li>{{!'<b>Conference</b>'   if request.path == '/conference'   else '<a href="/conference">Conference</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Arabic Works</b>'       if request.path == '/works_arabic' else '<a href="/works_arabic">Arabic Works</a>'}}</li>
			<li>{{!'<b>Arabic Manuscripts</b>' if request.path == '/manuscripts_arabic' else '<a href="/manuscripts_arabic">Arabic Manuscripts</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Latin Works</b>'        if request.path == '/works_latin' else '<a href="/works_latin">Latin Works</a>'}}</li>
			<li>{{!'<b>Latin Manuscripts</b>'  if request.path == '/manuscripts_latin' else '<a href="/manuscripts_latin">Latin Manuscripts</a>'}}</li>
			<li>{{!'<b>Latin Early Prints</b>' if request.path == '/prints_latin' else '<a href="/prints_latin">Latin Early Prints</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Images</b>' if request.path == '/scans' else '<a href="/scans">Images</a>'}}</li>
			<li>{{!'<b>Texts</b>'  if request.path == '/texts' else '<a href="/texts">Texts</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li><sup style="color: #cc0000; font-size: 0.8em">&#8239;beta</sup>{{!'<b>Glossary</b>' if request.path == '/glossary' else '<a href="/glossary">Glossary</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li class="submenu">
				<details{{' open=""' if request.path in {'/astrobibl', '/bib', '/jordanus'} else ''}}>
					<summary>Resources</summary>
					<ul>
					  % if db.debug:
						<li>{{!'<b>Bibliography</b>' if request.path == '/bib' else '<a href="/bib">Bibliography</a>'}}</li>
					  % end
						<li>{{!'<b>AstroBibl</b>' if request.path == '/astrobibl' else '<a href="/astrobibl">AstroBibl</a>'}}</li>
						<li><a href="/jordanus">Jordanus</a></li>
					</ul>
				</details>
			</li>
		</ul>
		<hr/>
	</nav>
	<nav class="extra">
		<p>{{!'<b>Licenses</b>'  if request.path == '/licenses' else '<a href="/licenses">Licenses</a>'}}&#160;·
		{{!'<b>FAQ</b>' if request.path == '/information' else '<a href="/information">FAQ</a>'}}&#160;·
		{{!'<b>Contact</b>' if request.path == '/contact' else '<a href="/contact">Contact</a>'}}&#160;·
		<a href="https://badw.de/data/footer-navigation/impressum.html">Impressum</a>&#160;·
		<a href="https://badw.de/data/footer-navigation/datenschutz.html">Privacy</a>\\
	  % if request.roles:
&#160;· {{!'<b>Forum</b>' if request.path == '/forum' else '<a href="/forum">Forum</a>'}}&#160;·
		  % if 'Admin' in request.roles:
{{!'<b>Bib-Upload</b>' if (request.path.endswith('/bib/upload')) else '<a href="/bib/upload">Bib-Upload</a>'}}
		  % end
		</p>
		<p><strong><a href="/auth_end">{{request.user_name}}: {{db.glosses['logout'][lang_id]}}</a></strong></p>
	  % end
	</nav>
	<nav class="extra">
		<button onclick="window.print()" type="button">Print 🖨</button>
	  % if request.roles:
		% if kwargs.get('exportable', False):
		<br/>
		<a class="key" download="" href="{{request.path}}?export&amp;form=editor">Word Export</a><br/>
		<a class="key" download="" href="{{request.path}}?export&amp;form=print">Latex Export</a>
		% end
	  % end
	</nav>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)

<footer>
	<p>
		<a class="img" href="https://badw.de"><img src="/icons/badw_marke_name.svg" height="64" alt="BAdW"/></a><a
		   class="img" href="https://uni-wuerzburg.de"><img src="/icons/uni_labeled.svg" height="64" alt="Universität Würzburg"/></a> <br/>
		<a class="img" href="https://www.uai-iua.org/"><img src="/icons/uai_fr.svg" height="50" alt="Union Académique Internationale"/></a>
	</p>
</footer>
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>
