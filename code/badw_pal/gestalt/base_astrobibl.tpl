% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id'] = db.lang_id
% siglum = kwargs.get('siglum', '')
% include('head.tpl', db = db, request = request, kwargs = kwargs)
<body>
<header>
	<a class="img" href="/start"><img class="shadow" src="/icons/main.png" alt="PAL"/></a>
	<h1>{{!'AstroBibl' if request.path == '/astrobibl/start' else '<a href="/astrobibl">AstroBibl</a>'}}</h1>
	<nav class="start">
		<ul>
	% for siglum, section in db.bib_astl_sections.items():
	  % if isinstance(siglum, int):
		<li><h2>{{section}}</h2></li>
	  % elif request.path == '/astrobibl/section/' + siglum:
		<li><b style="color: #005599">{{section}}</b></li>
	  % else:
		<li><a href="/astrobibl/section/{{siglum}}">{{section}}</a></li>
	  % end
	% end
		</ul>
		<hr/>
	</nav>
	<nav>
		<ul>
			<li><a href="/start">Ptolemaeus Arabus et Latinus</a></li>
		</ul>
	</nav>
	<hr/>
	<nav class="extra">
		<a href="https://badw.de/data/footer-navigation/impressum.html">Impressum</a>&#160;·
		{{!'<b>Contact</b>' if request.path == '/contact' else '<a href="/contact">Contact</a>'}}&#160;·
		<a href="https://badw.de/data/footer-navigation/datenschutz.html">Privacy</a>
	</nav>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
<footer>
	<p>
		<a class="img" href="http://badw.de"><img src="/icons/badw_marke_name.svg" height="64" alt="BAdW"/></a>
	</p>
</footer>
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>