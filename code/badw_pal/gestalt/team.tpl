% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="just_ahyph sheet wide">
	<h1>The Team</h1>
  % for group in kwargs['team']:
	% heading = group['group_heading']
	<section>
	% if group['group_id'] == 23:
		<details><summary><h3 style="display: inline-block">{{heading}}</h3></summary>
	% else:
		<h3>{{heading}}</h3>
		% end
		<ul>
		% users = group['users']
		% for user in users:
			% institution = user['user_institution']
			% title = user['user_title']
			% degree = user['user_degree']
			% name = (title + ' ' if title else '') + user['user_firstname'] + ' ' + user['user_lastname'] + (', ' + degree if degree else '')
			% institution = ' ({})'.format(institution) if institution else ''
			% if group['group_id'] == 23:
			<li>{{name}}{{institution}}</li>
			% else:
			<li><p><a href="/team/{{user['user_id']}}">{{name}}</a>{{institution}}</p></li>
			% end
		% end
		</ul>
		% if group['group_id'] == 23:
		</details>
		% end
	</section>
  % end
	<section>
		<figure>
			<img class="card" src="/files/team.jpg" alt="The Ptolemy team in November 2019"/>
			<figcaption>The <i>Ptolemaeus Arabus et Latinus</i> team in November 2019. From left to right: Nadine Löhr, Paul Hullmeine, Jan Hogendijk, Colette Dufossé, Charles Burnett, Benno van Dalen, Stefan Müller, Pouyan Rezvani, Dag Nikolaus Hasse, Emanuele Rovati, Beatriz Eugenia Alfaro Pérez, Stefan Georges, Claudia Dorl-Klingenschmid, and David Juste.
			</figcaption>
		</figure>
	</section>
  % if 'Admin' in request.roles:
	<section>
		<h2>Export</h2>
		<p><a class="key" href="/team/contribution?form=print" download="">Contributors of the Arabic catalog</a></p>
	</section>
  % end
</article>
</main>