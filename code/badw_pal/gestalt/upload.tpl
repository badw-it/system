% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a file-upload form.
<main>
<article class="just_ahyph sheet">
	<h1>Upload</h1>
	<form enctype="multipart/form-data" method="post">
		<ol class="list">
			<li><label>Press the following button, navigate to the file, double-click it and wait until its name is shown next to the button:<br/>
				<input class="key" type="file" name="file"/></label></li>
			<li><button type="submit" formaction="/{{kwargs['kind']}}/upload">Start the upload of the selected file</button> and wait – a rather long time in the case of big files and uploading from home – until a message appears in the top left corner about success or failure.</li>
		</ol>
	</form>
</article>
</main>
