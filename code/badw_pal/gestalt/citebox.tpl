% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)
% has_preliminary = True if (item.get('work_add_credit') == 'T' or item.get('ms_add_credit') == 'T') else False
% if 'export' in request.query and request.query.form == 'editor':
  % names = ', '.join( name.split('▄')[-1] for name in item['compiler_names'].split('█') )
	<p class="end">
		{{names}}
	  % if has_preliminary:
		[MJPP]
	  % end
	</p>
% else:
  % names = ', '.join( ' '.join(name.split('▄')[:-1]) for name in item['compiler_names'].split('█') )
	<aside class="citebox">
		<p>How to cite this item?</p>
		<p>{{names}}, <cite class="parttitle">‘\\
		  % if section == 'ms':
MS {{place_name}}{{item['lib_name']}}, {{!item['ms_shelfmark']}}\\
		  % elif section == 'print':
Ed. {{item['place_name']}}, {{item['print_publisher']}}, {{item['print_date']}}\\
		  % elif section == 'work':
{{item['person_name']}}, {{!item['work_title']}}\\
		  % end
’</cite> (update: <time datetime="{{modified_on}}">{{modified_on_shown}}</time>), <cite>Ptolemaeus Arabus et Latinus. {{sectiontitle}}</cite>, URL&#8239;=&#8239;http://ptolemaeus.badw.de/{{section}}/{{item['ms_id' if section == 'ms' else 'print_id' if section == 'print' else 'work_id']}}.</p>
	</aside>
  % if has_preliminary:
	<aside class="citebox_add_credit">Preliminary work on this item was done by Mª José Parra.</aside><!--María José Parra Pérez-->
  % end
% end