% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<h1>Jobs Archive</h1>
% for vacancy in kwargs['vacancies']:
<article class="sheet wide" id="{{vacancy['job_id']}}">
	<h2><a href="#{{vacancy['job_id']}}" title="Link"></a>{{vacancy['job_title']}}</h2>
	<p><time>{{vacancy['job_posted']}}</time>{{!'<b>. Currently available.</b>' if vacancy['job_archived'] == 'F' else ''}}</p>
	{{!vacancy['job_description']}}
</article>
% end
</main>