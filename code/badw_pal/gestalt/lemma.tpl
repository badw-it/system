% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lemma = kwargs['lemma']
<main>
<article class="sheet wide">
	<h1><i dir="{{'rtl' if lemma['rtl'] else 'ltr'}}" lang="{{lemma['lang_abbr']}}">{{lemma['expression']}}</i> <span class="nowrap">‘{{lemma['concept']}}’</span></h1>
	<p><small>{{lemma['lang_name']}}\\
%if lemma['grammar']:
. – Grammar: {{lemma['grammar']}}.\\
% end
</small></p>
	<table class="lemma">
	% if lemma['refs']:
		<tr>
			<th scope="row">References:</th>
			<td>{{lemma['refs']}}</td>
		</tr>
	% end
	% if lemma['transl_froms']:
		<tr>
			<th colspan="2" scope="col">Translation of:</th>
		</tr>
	  % for sublemma in lemma['transl_froms']:
		<tr>
			<th scope="row">{{sublemma['lang_name']}}:</th>
			<td><i dir="{{'rtl' if sublemma['rtl'] else 'ltr'}}" lang="{{sublemma['lang_abbr']}}"><a href="/glossary/{{sublemma['concept_id']}}/{{db.q(db.q(sublemma['expression'], safe = ''))}}">{{sublemma['expression']}}</a></i>\\
% if sublemma['refs']:
 [{{sublemma['refs']}}]\\
% end
</td>
		</tr>
	  % end
	% end
	% if lemma['transl_intos']:
		<tr>
			<th colspan="2" scope="col">Translated into:</th>
		</tr>
	  % for sublemma in lemma['transl_intos']:
		<tr>
			<th scope="row">{{sublemma['lang_name']}}:</th>
			<td><i dir="{{'rtl' if sublemma['rtl'] else 'ltr'}}" lang="{{sublemma['lang_abbr']}}"><a href="/glossary/{{sublemma['concept_id']}}/{{db.q(db.q(sublemma['expression'], safe = ''))}}">{{sublemma['expression']}}</a></i>\\
% if sublemma['refs']:
 [{{sublemma['refs']}}]\\
% end
</td>
		</tr>
	  % end
	% end
	% if lemma['transl_intos_modern']:
		<tr>
			<th colspan="2" scope="col">Modern translations:</th>
		</tr>
	  % for sublemma in lemma['transl_intos_modern']:
		<tr>
			<th scope="row">{{sublemma['lang_name']}}:</th>
			<td><i dir="{{'rtl' if sublemma['rtl'] else 'ltr'}}" lang="{{sublemma['lang_abbr']}}"><a href="/glossary/{{sublemma['concept_id']}}/{{db.q(db.q(sublemma['expression'], safe = ''))}}">{{sublemma['expression']}}</a></i>\\
% if sublemma['refs']:
 [{{sublemma['refs']}}]\\
% end
</td>
		</tr>
	  % end
	% end
	% if lemma['synonyms']:
		<tr>
			<th colspan="2" scope="col">Synonyms:</th>
		</tr>
	  % for sublemma in lemma['synonyms']:
		<tr>
			<td><i dir="{{'rtl' if sublemma['rtl'] else 'ltr'}}" lang="{{sublemma['lang_abbr']}}"><a href="/glossary/{{sublemma['concept_id']}}/{{db.q(db.q(sublemma['expression'], safe = ''))}}">{{sublemma['expression']}}</a></i></td>\\
% if sublemma['refs']:
<td>[{{sublemma['refs']}}]</td>
% end
		</tr>
	  % end
	% end
	</table>
</article>
</main>