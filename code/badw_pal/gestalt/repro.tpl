% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% query = request.query_string
% query = '?' + query if query else ''
% item = kwargs['item']
% pagina = kwargs['pagina']
% tran = item['tran']
% work = item['work']
% volume = ', '.join( item[key] for key in ('place_name', 'lib_name', 'ms_shelfmark', 'print_publisher', 'print_date', 'work_title') if key in item )
% img_scale = kwargs['img_scale']
% rtl = 'rtl' if item['lang_name'] == 'Arabic' else ''
<main>
	<h1><a href="/work/{{work['work_id']}}">{{'' if 'Anonymous' in work['person_name'] else work['person_name'] + ', '}}{{!work['work_title']}}</a></h1>
	<div class="flexbase">
		<h2><a href="/{{item['doc_type']}}/{{item['doc_id']}}" rel="chapter">{{!volume}}</a> ·
	  % if item['lang_name'] == 'Arabic' and pagina[-1:].isdigit():
		<strong style="display: inline-block; width: {{max( len(db.remove_tags(p)) for _, p in item['paginas'] ) / 1.5 + 1}}em">p. {{!item['pagina_styled']}}</strong></h2>
	  % else:
		<strong style="display: inline-block; width: {{max( len(db.remove_tags(p)) for _, p in item['paginas'] ) / 1.5}}em">{{!item['pagina_styled']}}</strong></h2>
	  % end
		<nav class="flexbase">
			<div class="droparea" tabindex="0">
				<a class="key arrowdown">Zoom</a>
				<div class="droparea shadow wide">
				  % for new_img_scale in (0.4, 0.6, 0.8, 1, 1.2, 1.4, 1.6, 1.8, 2, 2.5, 3, 3.5, 4, 4.5, 5):
					% if new_img_scale == img_scale:
					<strong>{{round(100 * img_scale)}}</strong>
					% else:
					<a class="key" href="?img_scale={{new_img_scale}}">{{round(100 * new_img_scale)}}</a>
					% end
				  % end
				</div>
			</div>
			<div class="droparea" tabindex="0">
				<a class="key arrowdown">Select a page</a>
				<div class="droparea shadow" style="font-size: 0.9em; max-width: 60vw; width: 400%">
				  % for new_pagina, new_pagina_styled in item['paginas']:
					% if new_pagina == pagina:
					<strong>{{!new_pagina_styled}}</strong>
					% else:
					<a class="key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/{{new_pagina}}{{query}}">{{!new_pagina_styled}}</a>
					% end
				  % end
				</div>
			</div>
			% include('repro_nav.tpl', item = item, pagina = pagina, rtl = rtl)
		  % if item['scan_path'] and item['scan_path'] != item['min_scan_path']:
			<div><a class="dark key" href="{{item['scan_path']}}" rel="noopener noreferrer" target="_blank">High res.</a></div>
		  % end
		</nav>
	</div>
	<div class="repro">
	  % if item['min_scan_path']:
		<figure><img id="image" class="shadow" src="{{item['min_scan_path']}}" alt="Facsimile" style="height: {{img_scale * 83}}vh"/>\\
		% if item['scan_caption']:
			{{!item['scan_caption']}}\\
		% end
		</figure>
	  % end
	  % if tran:
		<article id="transcription" class="card just_ahyph">
			<nav class="flexbase"{{!' dir="rtl"' if rtl else ''}}>
			  % include('repro_nav.tpl', item = item, pagina = pagina, rtl = rtl)
			  % if item['fulltran_part']:
				<a class="key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/text/{{item['fulltran_part']}}#{{pagina}}">Text&#xa0;only</a>
			  % end
			</nav>
			{{!tran}}
			<nav class="flexbase"{{!' dir="rtl"' if rtl else ''}}>
			  % include('repro_nav.tpl', item = item, pagina = pagina, rtl = rtl)
			  % if item['fulltran_part']:
				<a class="key" href="/{{item['doc_type']}}/{{item['doc_id']}}/{{item['part_id']}}/text/{{item['fulltran_part']}}#{{pagina}}">Text&#xa0;only</a>
			  % end
			</nav>
		</article>
	  % end
	</div>
</main>
<script>
$('.idmark').hover(wrapTextNodes, unwrapTextNodes);
$('#transcription').draggable();
$('#image').draggable();
document.getElementById('image').addEventListener('wheel', zoom);
</script>