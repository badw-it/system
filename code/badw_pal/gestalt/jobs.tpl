% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% vacancies = kwargs['vacancies']
<main>
<article class="just_ahyph sheet wide">
	<h1>Jobs</h1>
	<section><a class="key" href="/subscribe">Subscribe to Jobs …</a> <a class="key" href="/jobs_archive">Jobs Archive …</a></section>
  % if not vacancies:
	<section>
	  % if kwargs['general']:
		{{!kwargs['general']}}
	  % else:
		<p>At the moment no position in our project is available.</p>
	  % end
	</section>
  % else:
	<section>
		<h2>Currently available positions:</h2>
	  % for vacancy in vacancies:
		<section>
			<h3>{{vacancy['job_title']}}</h3>
			<p><time>{{vacancy['job_posted']}}</time></p>
			{{!vacancy['job_description']}}
		</section>
	  % end
	</section>
  % end
</article>
</main>