% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)
<main>
<article class="just_ahyph sheet wide">
	<h1><a href="/astrobibl">AstroBibl</a></h1>
	<h2>{{db.bib_astl_sections[kwargs['siglum']]}}</h2>
	  % for item in db.get_bib_items_of_astl(kwargs['siglum'], rename = kwargs['rename']):
		<p style="font-size: 19px; margin-top: 10px">
		% include('astrobibl_item.tpl', invert_italics = kwargs['invert_italics'], rename = kwargs['rename'], **item)
		</p>
	  % end
</article>
</main>