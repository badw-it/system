% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% subsiglum = next(iter(item.keys()))
% ms = item.pop(subsiglum)
% used_fields = {1, 4, 5, 6}
% city_name, city_id, city = ms.get(4, ['', 0, {}])
% library_name, library_id, library = ms.get(5, ['', 0, {}])
% shelfmark_name, shelfmark_id, shelfmark = ms.get(6, ['', 0, {}])
% current_subsiglum = 0
<main>
<article class="card">
	<h1>
<span id="{{subsiglum}}_4">\\
% for num, (sentence_id, term) in enumerate(city.items()):
%     if num != 0:
 / \\
%     end
<a class="field" href="#{{sentence_id}}" id="{{sentence_id}}">{{term}}</a>\\
% end
</span>, <span id="{{subsiglum}}_5">\\
% for num, (sentence_id, term) in enumerate(library.items()):
%     if num != 0:
 / \\
%     end
<a class="field" href="#{{sentence_id}}" id="{{sentence_id}}">{{term}}</a>\\
% end
</span>\\
% if shelfmark:
, <span id="{{subsiglum}}_6">\\
% for num, (sentence_id, term) in enumerate(shelfmark.items()):
%     if num != 0:
 / \\
%     end
<a class="field" href="#{{sentence_id}}" id="{{sentence_id}}">{{term}}</a>\\
% end
</span>
% end
	</h1>
	<table class="mapping">
	  % for field_id, (name, hypernum, terms) in ms.items():
		% if field_id not in used_fields:
		<tr><th scope="row"><a class="plain" href="#{{subsiglum}}_{{field_id}}">{{name.replace(' ', ' ')}}</a></th> <td id="{{subsiglum}}_{{field_id}}">
			% for num, (sentence_id, term) in enumerate(terms.items()):
			%     if num != 0:
			 | \\
			%     end
			<a class="field" href="#{{sentence_id}}" id="{{sentence_id}}">{{term}}</a>\\
			% end
			</td>
		</tr>
		% end
	  % end
	</table>
	<section style="margin: 6px 0 0 0">
		<table class="mapping">
	  % for subsiglum, mspart in item.items():
		  % items = mspart.items()
		  % for row_num, (field_id, (name, hypernum, terms)) in enumerate(items):
			% if row_num == 0:
			<tr><th scope="row" class="light" rowspan="{{len(items)}}">Work&#8239;{{subsiglum}}</th><th scope="row"><a class="plain" href="#{{subsiglum}}_{{field_id}}">{{name.replace(' ', ' ')}}</a></th> <td id="{{subsiglum}}_{{field_id}}">
			% else:
			<tr><th scope="row"><a class="plain" href="#{{subsiglum}}_{{field_id}}">{{name.replace(' ', ' ')}}</a></th> <td id="{{subsiglum}}_{{field_id}}">
			% end
				% for num, (sentence_id, term) in enumerate(terms.items()):
				%     if num != 0:
				 | \\
				%     end
				<a class="field" href="#{{sentence_id}}" id="{{sentence_id}}">{{term}}</a>\\
				% end
				</td>
			</tr>
		  % end
			<tr style="height: 6px"></tr>
	  % end
		</table>
	</section>
</article>
</main>