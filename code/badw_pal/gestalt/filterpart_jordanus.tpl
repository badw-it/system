% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs.get('lang_id', '')
	<p style="margin-left: 0.5em">
		<kbd>_</kbd> in a search term is understood as ‘zero or one unknown character’. </br>
		<kbd>%</kbd> is understood as ‘zero or more unknown characters’.
	</p>
	<form action="filter" method="get">
		<div class="flextable">
			<label class="card">
				<div style="width: 10rem"><b>Any field</b>:</div>
				<input value="{{request.query.text}}" aria-label="{{db.glosses['search_term'][lang_id]}}" autofocus="" name="text" pattern=".*\w.*" title="{{db.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
			</label>
		  % mainfields, subfields = db.get_filterfields_jordanus()
		  % for row in mainfields:
			<label class="card">
				<div style="width: 10rem">{{row['field_hypername']}}:</div>
				<input value="{{getattr(request.query, str(row['field_hypernum']))}}" aria-label="{{db.glosses['search_term'][lang_id]}}" name="{{row['field_hypernum']}}" pattern=".*\w.*" title="{{db.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
			</label>
		  % end
		</div>
		  % if subfields:
			% vals = [ getattr(request.query, str(row['field_hypernum'])) for row in subfields ]
			<details class="flextable"{{' open=""' if any(vals) else ''}}><summary>Further fields:</summary>
			% for val, row in zip(vals, subfields):
				<label class="card">
					<div style="width: 10rem">{{row['field_hypername']}}:</div>
					<input value="{{val}}" aria-label="{{db.glosses['search_term'][lang_id]}}" name="{{row['field_hypernum']}}" pattern=".*\w.*" title="{{db.glosses['at_least_one_letter'][lang_id]}}" type="search"/>
				</label>
			% end
			</details>
		  % end
		<p><button type="submit">{{db.glosses['search'][lang_id]}}&#8239;►</button></p>
	</form>
