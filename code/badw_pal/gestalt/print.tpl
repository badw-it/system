% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% modified_on = str(item['print_modified_on']).split()[0]
% modified_on_shown = '.'.join(reversed(modified_on.split('-')))
<main>
<article class="just_ahyph sheet wide">
	<nav class="catalognav">
	  % if item['prev']:
		<a class="arrowleft key" href="/print/{{item['prev'][0]}}" rel="prev" title="{{item['prev'][1]}}, {{item['prev'][2]}}, {{item['prev'][3]}}">Previous</a>
	  % else:
		<span class="arrowleft disabled key">Previous</span>
	  % end
	  % if item['next']:
		<a class="arrowright key" href="/print/{{item['next'][0]}}" rel="next" title="{{item['next'][1]}}, {{item['next'][2]}}, {{item['next'][3]}}">Next</a>
	  % else:
		<span class="arrowright disabled key">Next</span>
	  % end
	</nav>
	<h1>{{item['place_name']}}, {{item['print_publisher']}}, {{item['print_date']}}</h1>
	<section class="inline"><b>Title&#8199;page:</b> {{!item['print_title']}}{{!'&#8195;<b>Last page:</b> ' + item['print_last_page'] if item['print_last_page'] else ''}}</section>
	<section>{{!item['print_content']}}</section>
  % if item['print_note']:
	<section>{{!item['print_note']}}</section>
  % end
	<section>{{!item['print_bibliography']}}</section>
  % if item['print_exemplar']:
	<section>{{!item['print_exemplar']}}</section>
  % end
	<section>
	<table class="parts">
	  % for printpart in item['printparts']:
	  %     scan_link = printpart.get('scan_link', '')
	  %     tran_link = printpart.get('tran_link', '')
		<tr id="{{printpart['part_id']}}" role="row">
			<th scope="row">
				<div title="part id: {{printpart['part_id']}}">{{!printpart['pagina_range']}}</div>
			  % if scan_link and tran_link:
				<div><a class="dark key" href="{{scan_link}}">Images|Text</a></div>
			  % elif scan_link:
				<div><a class="dark key" href="{{scan_link}}">Images</a></div>
			  % elif tran_link:
				<div><a class="dark key" href="{{tran_link}}">Text</a></div>
			  % end
			</th>
			<td>
				<div>{{!printpart['printpart_quotation']}}</div>
				<div class="inline">=&#8239;<a href="/work/{{printpart['work_id']}}">{{'' if 'Anonymous' in printpart['person_name'] else printpart['person_name'] + ', '}}{{!printpart['work_title']}} ({{printpart['work_siglum']}})</a>{{!printpart['printpart_note']}}</div>
			</td>
		</tr>
	  % end
	</table>
	</section>
  % end
% include('citebox.tpl', item = item, section = 'print', sectiontitle = 'Early Prints')
</article>
</main>