% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a subscription form.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="card just_ahyph">
	<h1>Subscription</h1>
	<form method="post">
		<label class="card">Your email address: <input type="email" name="email" autofocus=""/></label>
		<p>
			<button type="submit" formaction="/subscribe/all">Subscribe for Jobs and News</button>
			<button type="submit" formaction="/subscribe/jobs">Subscribe only for Jobs</button>
			<button type="submit" formaction="/subscribe/news">Subscribe only for News</button>
		</p>
	</form>
</article>
