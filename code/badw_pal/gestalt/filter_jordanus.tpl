% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying filtered results.
% cut_off = 80
<details class="card" role="search"><summary>Adjust search</summary>
% include('filterpart_jordanus.tpl', db = db, request = request, kwargs = kwargs)
</details>
<article class="index">
	<table id="index">
		<thead>
			<tr role="row">
				<th>Matching MS</th>
				<th>Matching Field</th>
				<th>Matching Field Content</th>
				<th>Add.&#8239;info: Persons</th>
				<th>Add.&#8239;info: Titles</th>
				<th>Add.&#8239;info: Incipits</th>
			</tr>
		</thead>
		<tbody>
		% for ms_id, subsiglum, field_id, field_name, term, location, persons, titles, incipits in db.filter_jordanus(request.query):
			<tr class="odd" role="row">
				<td>{{location}} <a class="dark key" href="/jordanus/ms/{{ms_id}}#{{subsiglum}}_{{field_id}}">MS</a></td>
				<td>{{field_name.replace(' ', ' ')}}</td>
				<td class="mark">{{term}}</td>
				% if len(persons) > cut_off:
				<td class="abbr">{{persons[:cut_off]}}<span class="rest">{{persons[min(cut_off, len(persons)):]}}</span></td>
				% else:
				<td>{{persons}}</td>
				% end
				% if len(titles) > cut_off:
				<td class="abbr">{{titles[:cut_off]}}<span class="rest">{{titles[min(cut_off, len(titles)):]}}</span></td>
				% else:
				<td>{{titles}}</td>
				% end
				% if len(incipits) > cut_off:
				<td class="abbr">{{incipits[:cut_off]}}<span class="rest">{{incipits[min(cut_off, len(incipits)):]}}</span></td>
				% else:
				<td>{{incipits}}</td>
				% end
			</tr>
		% end
		</tbody>
	</table>
	% if 'ms_id' not in locals():
	<p><b>Sorry, no results found.</b></p>
	% end
</article>
