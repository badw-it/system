% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying downloadable files of a certain (possibly obscure) folder.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="sheet wide">
	<h1>{{kwargs['foldername'].split('-')[0]}} – Files for Download</h1>
  % for filename in kwargs['filenames']:
	<p><a class="key" href="/files/{{db.q(kwargs['foldername'])}}/{{db.q(filename)}}?export">{{filename}}</a></p>
  % end
</article>
</main>
