% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2016 ff. (© http://badw.de)
<main>
{{!kwargs['text']}}
</main>
<script>
$('.idmark').hover(wrapTextNodes, unwrapTextNodes);
</script>