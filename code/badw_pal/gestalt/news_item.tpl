% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% news_item = kwargs['news_item']
<main>
<article class="sheet wide">
	<p><time>{{news_item['news_posted']}}</time></p>
	<h1>{{news_item['news_headline']}}</h1>
	{{!news_item['news_content']}}
</article>
</main>