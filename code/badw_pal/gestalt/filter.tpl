% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for displaying search results.
<article class="index">
	<table id="index">
		<thead>
			<tr>
				<th>Realm</th>
				<th>Link</th>
				<th>Result</th>
			</tr>
		</thead>
	</table>
</article>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
	$(document).on(
		'init.dt',
		function(e, settings) {
			var table = $('#index').DataTable({'retrieve': true});
			var matches_num = table.ajax.json().matches_num;
			var term = matches_num === 1 ? 'hit' : 'hits altogether';
			$('#index_info').after(`<span>${matches_num} ${term}</span>`);
		}
	)
});
</script>