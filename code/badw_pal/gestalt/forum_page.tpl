% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="forum">
  % for post in kwargs['posts']:
	<section class="card wide" id="{{!post['post_id']}}">
		<div class="card">
			<strong>{{!post['first_name']}} {{!post['last_name']}}</strong> · {{!post['von_']}} – {{!post['bis_']}}
		  % if request.user_id == post['user_id'] or 'Admin' in request.roles:
			· <a class="key" href="{{post['page_id']}}/{{post['post_id']}}">Edit</a>
		  % end
		</div>
		<div>
			<h2>{{!post['title']}}</h2>
			<div>{{!post['doc']}}</div>
		</div>
	</section>
  % end
	<form action="/forum" method="post">
		<button name="page_id" value="{{!post['page_id']}}" type="submit">New Post</button>
		<a class="key" href="/forum">Cancel and back to the forum</a>
	</form>
  % if 'Admin' in request.roles:
	<form action="/forum/redact/{{!post['page_id']}}" method="POST">
		<p><button class="lid" type="button">Delete the whole thread?!</button><button name="redaction" value="delete">Delete the whole thread!</button></p>
	</form>
  % end
</article>
</main>