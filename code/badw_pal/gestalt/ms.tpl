% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% modified_on = str(item['ms_modified_on']).split()[0]
% modified_on_shown = '.'.join(reversed(modified_on.split('-')))
% place_name = item['place_name'].lstrip('?')
% place_name = place_name + ', ' if place_name else ''
<main>
<article class="just_ahyph sheet wide">
	<nav class="catalognav">
	  % if item['prev']:
		<a class="arrowleft key" href="/ms/{{item['prev'][0]}}" rel="prev" title="{{item['prev'][1]}}, {{item['prev'][2]}}, {{item['prev'][3]}}">Previous</a>
	  % else:
		<span class="arrowleft disabled key">Previous</span>
	  % end
	  % if item['next']:
		<a class="arrowright key" href="/ms/{{item['next'][0]}}" rel="next" title="{{item['next'][1]}}, {{item['next'][2]}}, {{item['next'][3]}}">Next</a>
	  % else:
		<span class="arrowright disabled key">Next</span>
	  % end
	</nav>
	<h1{{!' style="font-size: 1.5em"' if (item['lang_id'] == 1 and len(item['lib_name']) > 10) else ''}}>{{place_name}}{{item['lib_name']}}, {{!item['ms_shelfmark']}}</h1>
  % if item['ms_avail_source']:
	<section class="petit">[{{item['ms_avail_source']}}]</section>
  % end
  % if item['ms_short']:
	<section>{{!item['ms_short']}}</section>
  % elif item['lang_id'] == 1:
	<section class="inline">
	  % if item['ms_info']:
		{{!item['ms_info']}}&#8195;
	  % end
	  % if item['ms_datestring']:
		<b>Date:</b> {{!item['ms_datestring']}}&#8195;
	  % end
	  % if item['ms_origin']:
		<b title="Origin">Or.:</b> {{!item['ms_origin']}}&#8195;
	  % end
	  % if item['ms_provenance']:
		<b title="Provenance">Prov.:</b> {{!item['ms_provenance']}}
	  % end
	</section>
  % else:
	<section class="inline">
	  % if item['ms_datestring']:
		{{!item['ms_datestring']}}&#8195;
	  % end
	  % if item['ms_origin']:
		<b title="Origin">Or.:</b> {{!item['ms_origin']}}&#8195;
	  % end
	  % if item['ms_provenance']:
		<b title="Provenance">Prov.:</b> {{!item['ms_provenance']}}
	  % end
	</section>
  % end
	<section>{{!item['ms_codicology']}}</section>
	<section>{{!item['ms_content']}}</section>
  % if item['ms_note']:
	<section>{{!item['ms_note']}}</section>
  % end
	<section>{{!item['ms_bibliography']}}</section>
	<section>
	<table class="parts">
	  % for mspart in item['msparts']:
	  %     scan_link = mspart.get('scan_link', '')
	  %     tran_link = mspart.get('tran_link', '')
		<tr id="{{mspart['part_id']}}" role="row">
			<th scope="row">
				<div title="part id: {{mspart['part_id']}}">{{!mspart['pagina_range']}}</div>
			  % if scan_link and tran_link:
				<div><a class="dark key" href="{{scan_link}}">Images|Text</a></div>
			  % elif scan_link:
				<div><a class="dark key" href="{{scan_link}}">Images</a></div>
			  % elif tran_link:
				<div><a class="dark key" href="{{tran_link}}">Text</a></div>
			  % end
			</th>
			<td>
				<div>{{!mspart['mspart_quotation']}}</div>
				<div class="inline">=&#8239;<a href="/work/{{mspart['work_id']}}">{{'' if 'Anonymous' in mspart['person_name'] else mspart['person_name'] + ', '}}{{!mspart['work_title']}} ({{mspart['work_siglum']}})</a>{{!mspart['mspart_note']}}</div>
			</td>
		</tr>
	  % end
	</table>
	</section>
  % if item['lang_id'] == 1 and item['ms_bibl_special']:
	<section>{{!item['ms_bibl_special']}}</section>
  % end
% include('citebox.tpl', item = item, section = 'ms', sectiontitle = 'Manuscripts')
</article>
</main>