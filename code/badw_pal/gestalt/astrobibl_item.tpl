% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)
% autor = rename(bib_autor, bib_art == 'collection')
% hgautor = rename(bib_hgautor, True)
% hg = rename(bib_hg, True)
% zstitel = invert_italics(bib_zstitel)
% titel = invert_italics(bib_titel)
% hefttitel = invert_italics(bib_hefttitel)
% si_term = '' if bib_hefttitel == 'Micrologus' else 'special issue of '
% bandtitel = invert_italics(bib_bandtitel)
% bib_jahr = bib_jahr.split('█', 1)[-1]
% bib_kapitel = bib_kapitel if any( roman in bib_kapitel for roman in 'IVXLCDM' ) else ''
% bib_ort = bib_ort.replace(' / ', '-')
% bib_seitennr = bib_seitennr if bib_anm_astl else bib_seitennr.rstrip('.')
% bib_verlag = '' # The publisher should not appear even if the information is given.
% if bib_art == 'article':
  % if autor:
{{!autor}},
  % end
‘{{!bib_titel}}’,
{{!zstitel}} {{!bib_bandnr}}\\
  % if bib_heftnr:
/{{bib_heftnr}}\\
  % end
 ({{!bib_jahr}})\\
  % if bib_kapitel:
, {{bib_kapitel}}
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
  % if bib_anm_astl:
 [{{!bib_anm_astl}}]\\
  % end
.\\
% elif bib_art == 'book':
  % if autor:
{{!autor}},
  % end
{{!titel}},
  % if bib_bandanzahl:
{{!bib_bandanzahl}},
  % end
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [{{!si_term}}{{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
  % if bib_anm_astl:
 [{{!bib_anm_astl}}]\\
  % end
.\\
% elif bib_art == 'collection':
{{!titel}},
  % if bib_bandanzahl:
{{!bib_bandanzahl}},
  % end
  % if autor:
{{'eds' if '█' in bib_autor else 'ed.'}} {{!autor}},
  % end
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [{{!si_term}}{{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
  % if bib_anm_astl:
 [{{!bib_anm_astl}}]\\
  % end
.\\
% elif bib_art == 'inbook':
  % if autor:
{{!autor}},
  % end
‘{{!bib_titel}}’, in <i>{{!hgautor}}</i>, {{!bandtitel}},
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [{{!si_term}}{{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_kapitel:
, {{bib_kapitel}}\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
  % if bib_anm_astl:
 [{{!bib_anm_astl}}]\\
  % end
.\\
% elif bib_art == 'incollection':
  % if autor:
{{!autor}},
  % end
‘{{!bib_titel}}’, in
{{!bandtitel}},
  % if hg:
{{'eds' if '█' in bib_hg else 'ed.'}} {{!hg}},
  % end
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [{{!si_term}}{{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_bandnr:
, {{!bib_bandnr}}\\
  % end
  % if bib_kapitel:
, {{bib_kapitel}}\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
  % if bib_anm_astl:
 [{{!bib_anm_astl}}]\\
  % end
.\\
% elif bib_art == 'thesis':
  % if autor:
{{!autor}},
  % end
{{!titel}},
{{!bib_abschlussarbeit}},
  % if bib_bandanzahl:
{{!bib_bandanzahl}},
  % end
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_ort and bib_institution:
{{!bib_ort}}: {{!bib_institution}},
  % elif bib_ort:
{{!bib_ort}},
  % elif bib_institution:
{{!bib_institution}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [{{!si_term}}{{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
  % if bib_anm_astl:
 [{{!bib_anm_astl}}]\\
  % end
.\\
% end