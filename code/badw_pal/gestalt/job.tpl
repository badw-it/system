% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% vacancy = kwargs['vacancy']
<main>
<article class="sheet wide">
	<h1>{{vacancy['job_title']}}</h1>
	<p><time>{{vacancy['job_posted']}}</time>{{!'<b>. Currently available.</b>' if vacancy['job_archived'] == 'F' else ''}}</p>
	{{!vacancy['job_description']}}
</article>
</main>