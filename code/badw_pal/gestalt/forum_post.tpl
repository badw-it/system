% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% post = kwargs['post']
<main>
	<form action="/forum/{{!post['page_id']}}/{{!post['post_id']}}" method="POST">
		<p>
			Creator: {{!post['first_name']}} {{!post['last_name']}}<br/>
			Time of Creation: {{!post['von_']}}<br/>
			Time of Latest Change: {{!post['bis_']}}
		</p>
		<p>
			<label for="title">Title:</label>
			<input id="title" name="title" size="64" type="text" value="{{post['title']}}" autofocus=""/>
		</p>
		<p>
			<label for="doc">Content:</label>
			<input id="doc" name="doc" size="64" type="text" value="{{post['doc']}}"/>
		</p>
		<button name="aim" type="submit" value="back_to_thread">Submit and back to the thread</button>
		<button name="aim" type="submit" value="back_to_forum">Submit and back to the forum</button>
		<a class="key" href="/forum/{{!post['page_id']}}#{{!post['post_id']}}">Cancel and back to the thread</a>
		<a class="key" href="/forum">Cancel and back to the forum</a>
	</form>
  % if 'Admin' in request.roles:
	<form action="/forum/redact/{{!post['page_id']}}/{{!post['post_id']}}" method="POST">
		<p><button class="lid" type="button">Delete this post?!</button><button name="redaction" value="delete">Delete this post!</button></p>
	</form>
  % end
</article>
<script src="/cssjs/tinymce/tinymce.min.js"></script>
<script>initEditor('#doc');</script>
</main>