% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs['lang_id'] = db.lang_id
% include('head.tpl', db = db, request = request, kwargs = kwargs)
<body>
<header>
	<a class="img" href="/start"><img class="shadow" src="/icons/main.png" alt="PAL"/></a>
	<h1 lang="la">{{!'Jordanus' if request.path == '/jordanus/start' else '<a href="/jordanus">Jordanus</a>'}}</h1>
	<nav>
		<ul>
			<li>{{!'<b>Start</b>' if request.path == '/jordanus/start' else '<a href="/jordanus/start">Start</a>'}}</li>
			<li>{{!'<b>Search the Catalogue</b>' if request.path == '/jordanus/filterform' else '<a href="/jordanus/filterform">Search the Catalogue</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li>{{!'<b>Index of Manuscripts</b>' if request.path == '/jordanus/manuscripts' else '<a href="/jordanus/manuscripts">Index of Manuscripts</a>'}}</li>
			<li>{{!'<b>Index of Authors</b>' if request.path == '/jordanus/authors' else '<a href="/jordanus/authors">Index of Authors</a>'}}</li>
			<li>{{!'<b>Index of Titles</b>' if request.path == '/jordanus/titles' else '<a href="/jordanus/titles">Index of Titles</a>'}}</li>
			<li>{{!'<b>Index of Incipits</b>' if request.path == '/jordanus/incipits' else '<a href="/jordanus/incipits">Index of Incipits</a>'}}</li>
		</ul>
		<hr/>
		<ul>
			<li><small>{{!'<b>History of Jordanus</b>' if request.path == '/jordanus/history' else '<a href="/jordanus/history">History of Jordanus</a>'}}</small></li>
			<li><small>{{!'<b>The Data Available</b>' if request.path == '/jordanus/data' else '<a href="/jordanus/data">The Data Available</a>'}}</small></li>
		</ul>
		<hr/>
		<ul>
			<li><a href="/start">Ptolemaeus Arabus et Latinus</a></li>
		</ul>
	</nav>
	<hr/>
	<nav class="extra">
		<a href="https://badw.de/data/footer-navigation/impressum.html">Impressum</a>&#160;·
		{{!'<b>Contact</b>' if request.path == '/jordanus/contact' else '<a href="/jordanus/contact">Contact</a>'}}&#160;·
		<a href="https://badw.de/data/footer-navigation/datenschutz.html">Privacy</a>
	</nav>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
<footer>
	<p>
		<a class="img" href="http://badw.de"><img src="/icons/badw_marke_name.svg" height="64" alt="BAdW"/></a>
	</p>
</footer>
% include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>
