% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% page_id = request.path[1:]
% if page_id == db.page_id:
%     title = 'Ptolemaeus Arabus et Latinus'
% else:
%     item = kwargs.get('item', None)
%     if item:
%         title = db.xmlhtml.untag('PAL: ' + ', '.join( str(item[key]) for key in ('place_name', 'lib_name', 'ms_shelfmark', 'print_publisher', 'print_date', 'person_name', 'work_title', 'pagina') if key in item ).lstrip('?, '))
%     else:
%         title = 'PAL: ' + page_id.replace(r'scans', 'images').replace('_', ' ').title()
%     end
% end
<!DOCTYPE html>
<html id="top" class="{{kwargs.get('root_class', 'desk txt')}}{{' sans_auth' if not request.roles else ''}}" lang="{{kwargs['lang_id']}}">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="{{request.path}}" rel="canonical"/>
	<link href="/icons/favicon.ico" rel="icon"/>
	<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/cssjs/badw_pal.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/cssjs/jquery/jquery.min.js"></script>
	<script src="/cssjs/jquery/jquery-ui.min.js"></script>
	<script src="/cssjs/all.js?v={{db.startsecond}}"></script>
	<script src="/cssjs/badw_pal.js?v={{db.startsecond}}"></script>
	<title>{{title}}</title>
</head>
