% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% user = kwargs['user']
% title = user['user_title']
% degree = user['user_degree']
% email = user['user_email'] or ''
% mailname, domain = email.split('@', 1) if '@' in email else ('', '')
<main>
<article class="just_ahyph sheet wide">
  % if user['user_picturefile']:
	<img class="embed_right" src="/files/user_upload/{{user['user_picturefile']}}" width="100" alt="An image of {{user['user_lastname']}}"/>
  % end
	<h1>{{title + ' ' if title else ''}}{{user['user_firstname']}} {{user['user_lastname']}}{{', ' + degree if degree else ''}}</h1>
	<p><i>{{user['group_role']}}</i></p>
  % if mailname:
	<p><a class="refresh" href=""></a></p>
  % end
  % if user['user_description']:
	<h2>General Information</h2>
	{{!user['user_description']}}
  % end
  % if user['user_cv']:
	<h2>Short Curriculum Vitae</h2>
	{{!user['user_cv']}}
  % end
  % if user['user_publications']:
	<h2>Selected Publications</h2>
	{{!user['user_publications']}}
  % end
  % if user['user_cvlink']:
	<h2><a class="key" href="/files/user_upload/{{user['user_cvlink']}}">Full CV and List of Publications&#xa0;…</a></h2>
  % end
</article>
<script type="text/javascript">
	var anew = '{{mailname}}';
	anew += String.fromCharCode(64);
	anew += '{{domain}}';
	var proto_anew = String.fromCharCode(109, 97, 105, 108, 116, 111, 58) + anew;
	$('.refresh').attr('href', proto_anew).html(anew);
</script>
</main>