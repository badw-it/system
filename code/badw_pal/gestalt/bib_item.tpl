% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)
% autor = rename(bib_autor, invert_til_authors, bib_art == 'collection')
% hgautor = rename(bib_hgautor)
% hg = rename(bib_hg, 0, True)
% zstitel = invert_italics(bib_zstitel)
% titel = invert_italics(bib_titel)
% hefttitel = invert_italics(bib_hefttitel)
% bandtitel = invert_italics(bib_bandtitel)
% bib_jahr = bib_jahr.split('█', 1)[-1]
% if bib_art == 'article':
  % if autor:
{{!autor}},
  % end
‘{{!bib_titel}}’,
{{!zstitel}} {{!bib_bandnr}}\\
  % if bib_heftnr:
/{{!bib_heftnr}}\\
  % end
 ({{!bib_jahr}})\\
  % if bib_hefttitel:
 [special issue {{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_kapitel:
, {{!bib_kapitel}}
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}.\\
  % end
% elif bib_art in {'book', 'collection'}:
  % if autor:
{{!autor}},
  % end
{{!titel}},
  % if bib_bandanzahl:
{{!bib_bandanzahl}},
  % end
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_bandnr:
{{!bib_bandnr}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [special issue of {{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
.\\
% elif bib_art == 'inbook':
  % if autor:
{{!autor}},
  % end
‘{{!bib_titel}}’, in <i>{{!hgautor}}</i>, {{!bandtitel}},
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_bandnr:
{{!bib_bandnr}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [special issue of {{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_kapitel:
, {{!bib_kapitel}}\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
.\\
% elif bib_art == 'incollection':
  % if autor:
{{!autor}},
  % end
‘{{!bib_titel}}’, in
  % if hg:
{{!hg}},
  % end
{{!bandtitel}},
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_ort and bib_verlag:
{{!bib_ort}}: {{!bib_verlag}},
  % elif bib_ort:
{{!bib_ort}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [special issue of {{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
  % if bib_bandnr:
, {{!bib_bandnr}}\\
  % end
  % if bib_kapitel:
, {{!bib_kapitel}}\\
  % end
  % if bib_seitennr:
, {{!bib_seitennr}}\\
  % end
.\\
% elif bib_art == 'thesis':
  % if autor:
{{!autor}},
  % end
{{!titel}},
  % if bib_bandanzahl:
{{!bib_bandanzahl}},
  % end
  % if bib_abschlussarbeit:
{{!bib_abschlussarbeit}},
  % end
  % if bib_auflage:
{{!bib_auflage}},
  % end
  % if bib_bandnr:
{{!bib_bandnr}},
  % end
  % if bib_ort and bib_institution:
{{!bib_ort}}: {{!bib_institution}},
  % elif bib_ort:
{{!bib_ort}},
  % elif bib_institution:
{{!bib_institution}},
  % end
{{!bib_jahr}}\\
  % if bib_hefttitel:
 [special issue of {{!hefttitel}}\\
    % if bib_reihenbandnr:
, vol. {{!bib_reihenbandnr}}\\
    % end
]\\
  % end
.\\
% end