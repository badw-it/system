% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>
<article class="sheet wide">
	<h1>News and Announcements</h1>
	<p><a class="key" href="/subscribe">Subscribe to News …</a></p>
  % for news_item in kwargs['news']:
	<h2><a href="/news/{{news_item['news_id']}}">{{news_item['news_headline']}}</a></h2>
	<time>{{news_item['news_posted']}}</time>
	<div class="inline">
		{{!news_item['news_teaser']}}
		<a class="key" href="/news/{{news_item['news_id']}}">More</a>
	</div>
  % end
</article>
</main>