% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
<main>

% if request.path == '/start':

<article class="just_ahyph sheet wide">
<!-- <article about="/" class="cover just_ahyph" style="background: url(/icons/cover.jpg); background-position: top 0px right 0px; background-size: cover">

<section class="covertitle">
<h1><small-caps>Ptolemaeus Arabus et Latinus</small-caps></h1> -->
{{!kwargs['texts']['f_desc']}}
<!-- </section> -->

</article>

% else:

<article class="just_ahyph sheet wide">

<h1>Project Outline</h1>

{{!kwargs['texts']['f_output']}}

</article>

% end

</main>