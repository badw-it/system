% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a fulltext filterform.
<main>
<article class="card" role="search">
% include('filterpart_jordanus.tpl', db = db, request = request, kwargs = kwargs)
</article>
</main>