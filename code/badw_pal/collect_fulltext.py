# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
import sys
import urllib.parse
from html import escape, unescape
from urllib.parse import unquote

import gehalt
import __init__
import fs
import sql_io
import web_io
import xmlhtml

def insert(
        url: str,
        text: str,
        con: 'sql_io.mysql.Connection',
        cur: 'sql_io.mysql.cursors.Cursor',
        ) -> None:
    '''
    Extract rows from data given as :param:`url` (which is the URL of a webpage)
    and :param:`text` (which is the HTML of said webpage). Insert said rows into
    a database with the cursor :param:`cur` of the connection :param:`con`.

    (For more information, look at the calling :func:`web_io.fulltext`.)
    '''
    scheme, netloc, path, *_ = urllib.parse.urlsplit(url)
    area = ''
    match = re.match(r'/(work)/(\d+)$', path)
    if match:
        cur.execute("set @work_id = %s", (match.group(2),))
        cur.execute(gehalt.SQL_GET_WORK)
        item = cur.fetchone()
        if item:
            area = f"Work {item['work_siglum']} {item['person_name']}, {item['work_title']}"
    match = re.match(r'/(ms|print)/(\d+)$', path)
    if match:
        doc_type = match.group(1)
        area = 'MS ' if doc_type == 'ms' else 'Print '
        area += gehalt.get_location(doc_type, match.group(2), con, cur)
    match = re.match(r'/(edition|ms|print)/(\d+)/(\d+)/text/(\d+)', path)
    if match:
        doc_type, doc_id, part_id, volume_id = match.group(1, 2, 3, 4)
        if doc_type == 'edition':
            work_id = part_id
        else:
            prefix = 'Ed' if doc_type == 'print' else 'MS'
            work_id = con.get(
                    f"f_part_of_work from {prefix}_Parts where id = %s", part_id
                    ).get('f_part_of_work', 0)
        cur.execute("set @work_id = %s", (work_id,))
        cur.execute(gehalt.SQL_GET_WORK)
        item = cur.fetchone()
        if item:
            area = f"Text {item['work_siglum']} {item['person_name']}, {item['work_title']}"
        else:
            area = 'Text'
    match = re.match(r'/(glossary)/(\d+)/(.*)', path)
    if match:
        expression = escape(unquote(unquote(match.group(3))))
        concept = escape(con.get(
                r"name from Gloss_Concepts where id = %s",
                match.group(2)).get('name', ''))
        area = f"Lemma <i>{expression}</i> ‘{concept}’"
    if not area:
        area = path.lstrip('/').split('/', 1)[0].title()
    text = xmlhtml.untag(
            text, replace_element = replace_element)
    text = re.sub(r'[\u00ad\u200b]', '', text) # Delete SHY, ZWS.
    text = re.sub(r'\s+', ' ', text)
    cur.execute(r"""
            insert into ft values (1, %s, %s, %s, %s)
            on duplicate key update
            ft_new = 1, ft_text = %s, ft_area = %s
            """,
            (text, path, '', area, text, area))
    con.commit()

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    """
    Process :param:`tag` and :param:`attrs`.
    A return value of ``None`` does not change the element.
    If a string is returned, the element is replaced by it
    without escaping; if the empty string is returned, the
    element is deleted.
    If the element is void, :param:`startend` is ``True``.
    """
    if (
            (tag in {
                'aside',
                'details',
                'footer',
                'head',
                'header',
                'nav',
                'script',
                'style',
                })
            or ('data-not-for-ft' in attrs)
            or (tag == 'a' and 'pagina' in attrs.get('class', '').split())
            ):
        return ''
    return None

if __name__ == '__main__':
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    urdata = sys.argv[2:]
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    web_io.fulltext(
            int(sys.argv[1]),
            sql_io.get_connect(*fs.get_keys(paths['db_access']), config),
            (
                r"""
                create table if not exists ft
                (
                ft_new TINYINT UNSIGNED NOT NULL DEFAULT 1, -- Whether the record is newly inserted.
                ft_text LONGTEXT NOT NULL, -- Content.
                ft_path VARCHAR(8000) NOT NULL, -- URL of a page without domain, but with a leading slash.
                ft_part VARCHAR(191) NOT NULL, -- Fragment ID of the part of the page, with leading hashtag. Empty, if it is the whole page.
                ft_area VARCHAR(8000) NOT NULL, -- Area to which the page belongs within the website, inferred e.g. from the path.
                INDEX (ft_area(191)),
                UNIQUE (ft_path, ft_part)
                ) COLLATE utf8mb4_german2_ci
                """,
            ),
            config['ids']['start_url'],
            insert,
            headers = dict(config['bot_headers']),
            urls_considered_for_their_text_re = re.compile(
                config['ids']['urls_considered_for_their_text_re']),
            urls_considered_for_further_urls_re = re.compile(
                config['ids']['urls_considered_for_further_urls_re']),
            default_path = '/' + config['ids']['page'],
            after_loop_sqls = (
                r"delete from ft where ft_new = 0",
                r"update ft set ft_new = 0",
                ),
            )
