# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
'''
Data access for the project `Ptolemaeus Arabus et Latinus`.
'''
import itertools
import os
try: import regex as re
except ImportError: import re
import secrets
import subprocess
import sys
import time
import typing as ty
import unicodedata
from binascii import hexlify
from collections import defaultdict
from collections import deque
from decimal import Decimal as Dec
from functools import partial
from hashlib import pbkdf2_hmac # for Web2py passwords
from operator import itemgetter
from urllib.parse import unquote

try:
    import pymysql as mysql
    from pymysql import IntegrityError
except ImportError:
    pass

import __init__
import bottle
import fs
import mail
import parse
import sql_io
import web_io
import xmlhtml
from structs import void

INTEXTREF_RE: 're.Pattern[str]' = re.compile(r'''(?x)
        \\(?P<key>externalms|msshelfmark|worksiglum|worktitle)\s*
        \{\s*
            (?!█)
            (?P<num>[^#}\\]*)
            \#?\s*
            (?P<name>[^}\\]*)
        \}''')

MULTIPLES_MAP = {
        'za': 'bis',
        'zb': 'ter',
        'zc': 'quater',
        'zd': 'quinqies',
        'ze': 'sexies',
        'zf': 'septies',
        'zg': 'octies',
        'zh': 'nonies',
        'zi': 'decies',
        }

PAGINA_RE = re.compile(r'\d+[rv]?')

SQL_GET_COMMENTARIES_OF_WORK = """
        select
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name,
            Languages.id           as lang_id,
            Languages.f_lang       as lang_name
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Works.f_hidden = 'F'
        and Works.f_commentary_on = %s
        order by Works.f_code
        """
SQL_GET_GLOSSES = """
        SELECT
            T.id                                as tid,
            IFNULL(C.id, IFNULL(CC.id, CCC.id)) as cid,
            T.term                              as expression,
            L.id                                as lang_id,
            L.f_lang                            as lang_name,
            L.f_lang_sorter                     as lang_sorter,
            L.bcp47                             as lang_abbr,
            L.f_rtl                             as rtl,
            T.grammar                           as grammar,
            IFNULL(C.id, IFNULL(CC.id, CCC.id)) as concept_id,
            IFNULL(C.name, IFNULL(CC.name, CCC.name))
                                                as concept,
            TT.term                             as translated_from,
            TT.lang                             as source_lang,
            R.generic_work                      as generic_work,
            R.short_title                       as short_title,
            R.title                             as title,
            GROUP_CONCAT(DISTINCT TR.location ORDER BY TR.location SEPARATOR '; ')
                                                as occurrences
        FROM Gloss_Terms T
        LEFT JOIN Gloss_Terms TT ON T.translated_from = TT.id
        LEFT JOIN Gloss_Terms TTT ON TT.translated_from = TTT.id
        LEFT JOIN Gloss_Concepts C ON T.concept = C.id
        LEFT JOIN Gloss_Concepts CC ON TT.concept = CC.id
        LEFT JOIN Gloss_Concepts CCC ON TTT.concept = CCC.id
        LEFT JOIN Gloss_Termrefs TR ON T.id = TR.termid
        LEFT JOIN Gloss_Refs R ON TR.refid = R.id
        LEFT JOIN Languages L ON L.id = T.lang
        WHERE T.is_modern = 0 or T.is_modern is NULL
        GROUP BY concept, T.term, R.title, R.short_title, T.lang, T.grammar, TT.term, T.id, TT.id, TTT.id
        ORDER BY concept, IFNULL(TTT.id, IFNULL(TT.id, T.id)), CASE T.lang WHEN 2 THEN 0 WHEN 1 THEN 1 WHEN 3 THEN 2 ELSE 3 END, T.term, TR.refid
        """ # Attribution notice: by François Charette in 2016; minuscule modifications by Stefan Müller in 2017 ff.
SQL_GET_GROUPS = """
        select
            id       as group_id,
            plural   as group_heading
        from auth_group
        where do_list = 'T'
        order by do_order
        """
SQL_GET_JOB = """
        select
            f_jobdesc    as job_description,
            f_jobposted  as job_posted,
            f_jobtitle   as job_title,
            f_jobarchive as job_archived
        from t_jobs
        where t_jobs.id = %s
        limit 1
        """
SQL_GET_JOB_VACANCIES = """
        select
            f_jobdesc   as job_description,
            f_jobposted as job_posted,
            f_jobtitle  as job_title
        from t_jobs
        where is_active = 'T'
        and f_jobarchive = 'F'
        """
SQL_GET_JOBS = """
        select
            id           as job_id,
            f_jobdesc    as job_description,
            f_jobposted  as job_posted,
            f_jobtitle   as job_title,
            f_jobarchive as job_archived
        from t_jobs
        where is_active = 'T'
        order by f_jobposted desc
        """
SQL_GET_JOBS_DESCRIPTION = """
        select f_jobs_desc as jobs_description
        from t_jobs_desc
        limit 1
        """
SQL_GET_MS = """
        select
            Manuscripts.id                      as ms_id,
            Manuscripts.f_add_credit            as ms_add_credit,
            Manuscripts.f_available_source      as ms_avail_source,
            Manuscripts.f_bibliography          as ms_bibliography,
            Manuscripts.f_bibl_special          as ms_bibl_special,
            IF(Manuscripts.f_cat_lang = 'Arabic', 1, 0) as lang_id,
            Manuscripts.f_cat_lang              as lang_name,
            Manuscripts.f_codicology            as ms_codicology,
            Manuscripts.f_content               as ms_content,
            Manuscripts.f_date_string           as ms_datestring,
            Manuscripts.f_ms_info               as ms_info,
            Manuscripts.f_note                  as ms_note,
            Manuscripts.f_place_compiled_string as ms_origin,
            Manuscripts.f_provenance            as ms_provenance,
            Manuscripts.f_shelfmark             as ms_shelfmark,
            Manuscripts.f_short                 as ms_short,
            Manuscripts.modified_on             as ms_modified_on,
            Libraries.f_lib_name                as lib_name,
            IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev) as lib_abbrev,
            Places.f_placename                  as place_name,
            CONCAT_WS('█',
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Manuscripts ON Manuscripts.id = @doc_id AND Manuscripts.f_compiler = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Manuscripts ON Manuscripts.id = @doc_id AND Manuscripts.f_compiler2 = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Manuscripts ON Manuscripts.id = @doc_id AND Manuscripts.f_compiler3 = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Manuscripts ON Manuscripts.id = @doc_id AND Manuscripts.f_compiler4 = auth_user.id LIMIT 1)
            ) as compiler_names
        from Manuscripts
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        where Manuscripts.id = @doc_id
        and Manuscripts.f_hidden = 'F'
        limit 1
        """
SQL_GET_MSS_PAGINA_RANGE = """
        select distinct
            Manuscripts.id          as ms_id,
            Manuscripts.f_shelfmark as ms_shelfmark,
            Libraries.f_lib_name    as lib_name,
            IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev) as lib_abbrev,
            Places.f_placename      as place_name,
            MS_Parts.id             as part_id,
            MS_Parts.f_range        as pagina_range,
            Works.id                as work_id,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Persons.f_person_name   as person_name,
            Languages.id            as lang_id,
            Languages.f_lang        as lang_name
        from Manuscripts
        join Libraries
            on Libraries.id = Manuscripts.f_place_library
        join Places
            on Places.id = Libraries.f_place
        join MS_Parts
            on MS_Parts.f_part_of_ms = Manuscripts.id
        join Works
            on Works.id = MS_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Manuscripts.f_hidden = 'F'
        and MS_Parts.f_hidden = 'F'
        and Works.f_hidden = 'F'
        """
SQL_GET_NEWS = """
        select
            id         as news_id,
            f_content  as news_content,
            f_headline as news_headline,
            f_posted   as news_posted
        from t_news
        where is_active = 'T'
        order by f_posted desc
        """
SQL_GET_NEWS_ITEM = """
        select
            id         as news_id,
            f_content  as news_content,
            f_headline as news_headline,
            f_posted   as news_posted
        from t_news
        where id = %s
        limit 1
        """
SQL_GET_PRINT = """
        select
            Editions.id             as print_id,
            Editions.f_bibliography as print_bibliography,
            Editions.f_content      as print_content,
            CAST(Editions.f_date_num_exact AS CHAR) as print_date,
            Editions.f_exemplar     as print_exemplar,
            Editions.f_last_page    as print_last_page,
            Editions.f_note         as print_note,
            Editions.f_publisher    as print_publisher,
            Editions.f_title        as print_title,
            Editions.modified_on    as print_modified_on,
            Places.f_placename      as place_name,
            3                       as lang_id, -- Only Latin so far.
            CONCAT_WS('█',
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Editions ON Editions.id = @doc_id AND Editions.f_compiler = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Editions ON Editions.id = @doc_id AND Editions.f_compiler2 = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Editions ON Editions.id = @doc_id AND Editions.f_compiler3 = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Editions ON Editions.id = @doc_id AND Editions.f_compiler4 = auth_user.id LIMIT 1)
            ) as compiler_names
        from Editions
        join Ed_Parts
            on Ed_Parts.f_part_of_edition = Editions.id
        join Places
            on Places.id = Editions.f_place_compiled
        where Editions.id = @doc_id
        and Editions.f_hidden = 'F'
        and Ed_Parts.f_hidden = 'F'
        limit 1
        """
SQL_GET_PRINTPARTS_OF_PRINT = """
        select
            Ed_Parts.id            as part_id,
            Ed_Parts.f_note        as printpart_note,
            Ed_Parts.f_quotation   as printpart_quotation,
            Ed_Parts.f_range       as pagina_range,
            Ed_Parts.modified_on   as printpart_modified_on,
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name
        from Ed_Parts
        join Editions
            on Editions.id = Ed_Parts.f_part_of_edition
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        where Creators.f_creator_role = 1
        and Editions.id = %s
        and Ed_Parts.f_hidden = 'F'
        and Works.f_hidden = 'F'
        order by Ed_Parts.f_order_no
        """
SQL_GET_PRINTPARTS_OF_WORK = """
        select
            Ed_Parts.id          as part_id,
            REPLACE(REPLACE(Ed_Parts.f_date_folio_info, '<p>', ''), '</p>', '') as printpart_info,
            Ed_Parts.f_range     as pagina_range,
            Ed_Parts.modified_on as printpart_modified_on,
            Editions.id          as print_id,
            CAST(Editions.f_date_num_exact AS CHAR) as print_date,
            Editions.f_publisher as print_publisher,
            Editions.f_hidden    as print_hidden,
            Works.id             as work_id,
            Places.f_placename   as place_name
        from Ed_Parts
        join Editions
            on Editions.id = Ed_Parts.f_part_of_edition
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Places
            on Places.id = Editions.f_place_compiled
        where Creators.f_creator_role = 1
        and Works.id = %s
        and Ed_Parts.f_hidden = 'F'
        order by
            Editions.f_date_num_exact,
            Places.f_placename,
            Editions.f_publisher
        """
SQL_GET_PRINTS_PAGINA_RANGE = """
        select distinct
            Editions.id            as print_id,
            CAST(Editions.f_date_num_exact AS CHAR) as print_date,
            Editions.f_publisher   as print_publisher,
            Editions.f_title       as print_title,
            Places.f_placename     as place_name,
            Ed_Parts.id            as part_id,
            Ed_Parts.f_range       as pagina_range,
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name,
            Languages.id           as lang_id,
            Languages.f_lang       as lang_name
        from Editions
        join Places
            on Places.id = Editions.f_place_compiled
        join Ed_Parts
            on Ed_Parts.f_part_of_edition = Editions.id
        join Works
            on Works.id = Ed_Parts.f_part_of_work
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Editions.f_hidden = 'F'
        and Ed_Parts.f_hidden = 'F'
        and Works.f_hidden = 'F'
        """
SQL_GET_TERMS = """
        select
            Gloss_Terms.id              as id,
            Gloss_Terms.term            as expression,
            Gloss_Terms.concept         as concept_id,
            Gloss_Terms.translated_from as from_id,
            Gloss_Terms.is_modern       as is_modern,
            Gloss_Terms.grammar         as grammar,
            Gloss_Terms.note            as note,
            Gloss_Concepts.name         as concept,
            Languages.id                as lang_id,
            Languages.f_lang            as lang_name,
            Languages.bcp47             as lang_abbr,
            Languages.f_lang_sorter     as lang_sorter,
            Languages.f_rtl             as rtl
        from Gloss_Terms
        join Gloss_Concepts
            on Gloss_Concepts.id = Gloss_Terms.concept
        join Languages
            on Languages.id = Gloss_Terms.lang
        where Gloss_Terms.is_active = 'T'
        """
SQL_GET_TRANSLATIONS_OF_WORK = """
        select
            Works.id               as work_id,
            Works.f_siglum         as work_siglum,
            Works.f_standard_title as work_title,
            Persons.f_person_name  as person_name,
            Languages.id           as lang_id,
            Languages.f_lang       as lang_name
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Works.f_hidden = 'F'
        and Works.f_translation_of = %s
        order by Works.f_code
        """
SQL_GET_USER = """
        select
            auth_user.id          as user_id,
            auth_user.first_name  as user_firstname,
            auth_user.last_name   as user_lastname,
            auth_user.institution as user_institution,
            auth_user.title       as user_title,
            auth_user.deg         as user_degree,
            auth_user.cv          as user_cv,
            auth_user.cv_link     as user_cvlink,
            auth_user.description as user_description,
            auth_user.email       as user_email,
            auth_user.picture     as user_picturefile,
            auth_user.pubs        as user_publications,
            auth_group.role       as group_role
        from auth_user
        join auth_membership
            on auth_membership.user_id = auth_user.id
        join auth_group
            on auth_group.id = auth_membership.group_id
        where auth_group.do_list = 'T'
        and auth_user.id = %s
        """
SQL_GET_USERS = """
        select
            auth_user.id          as user_id,
            auth_user.first_name  as user_firstname,
            auth_user.last_name   as user_lastname,
            auth_user.institution as user_institution,
            auth_user.title       as user_title,
            auth_user.deg         as user_degree
        from auth_user
        join auth_membership
            on auth_membership.user_id = auth_user.id
        where auth_membership.group_id = %s
        and auth_user.last_name is not null
        and auth_user.last_name <> ''
        order by
            auth_user.last_name,
            auth_user.first_name
        """
SQL_GET_WORK = """
        select
            Works.id                as work_id,
            Works.f_add_credit      as work_add_credit,
            Works.f_bibliography    as work_bibliography,
            Works.f_commentary_on   as work_commentatum,
            Works.f_content         as work_content,
            Works.f_note            as work_note,
            Works.f_origin          as work_origin,
            Works.f_origin_ext      as work_origin_ext,
            Works.f_edition_history as work_printhistory,
            Works.f_other_titles    as work_other_titles,
            Works.f_quotation       as work_quotation,
            Works.f_siglum          as work_siglum,
            Works.f_standard_title  as work_title,
            Works.f_standard_arabic as work_title_arabic,
            Works.f_translation_of  as work_translated_from,
            Works.modified_on       as work_modified_on,
            Persons.f_person_name   as person_name,
            Languages.id            as lang_id,
            Languages.f_lang        as lang_name,
            CONCAT_WS('█',
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Works ON Works.id = @work_id AND Works.f_compiler = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Works ON Works.id = @work_id AND Works.f_compiler2 = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Works ON Works.id = @work_id AND Works.f_compiler3 = auth_user.id LIMIT 1),
            	(SELECT CONCAT_WS('▄', auth_user.first_name, auth_user.last_name, auth_user.initials) FROM auth_user JOIN Works ON Works.id = @work_id AND Works.f_compiler4 = auth_user.id LIMIT 1)
            ) as compiler_names
        from Works
        join Creators
            on Creators.f_work = Works.id
        join Persons
            on Persons.id = Creators.f_creator
        join Languages
            on Languages.id = Works.f_lang
        where Creators.f_creator_role = 1
        and Works.id = @work_id
        and Works.f_hidden = 'F'
        limit 1
        """

XML_STAMP = '<!--XML-->' # appended to all XML cells, i.e. cells with type text.
EXTRA_XML_COLS = {
        'pagina_range',      # Ed_Parts.f_range *and* MS_Parts.f_range
        'ms_shelfmark',      # Manuscripts.f_shelfmark
        'work_title',        # Works.f_standard_title
        'work_title_arabic', # Works.f_standard_arabic
        } # taken as XML cols but: no XML_STAMP, coltype is varchar, input raw.

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.

        Overview over the three categories of string columns:

        1. XML fields (all is “X”):

        - [X] input with editor (TinyMCE).
        - [X] stored in a column with a “TEXT” datatype.
        - [X] stored with escaping, i.e. “&” as “&amp;”.
        - [X] undergo background clean-up.
        - [X] get the (invisible) stamp “<!--XML-->” by the background process.
        - [X] no escaping when put into HTML for display on website.
        - [X] undergo *all* conversions from HTML markup to Latex markup.

        2. raw-text fields (all is “R”):

        - [R] input with simple text field.
        - [R] stored in a column with a “VARCHAR” datatype.
        - [R] stored without escaping, i.e. “&” is stored  as “&”.
        - [R] no background clean-up.
        - [R] no XML-stamp.
        - [R] escaped when put into HTML for display on website.
        - [R] undergo *not all* conversions from HTML markup to Latex markup.

        3. mixed fields (``EXTRA_XML_COLS``):

        - [R] input with simple text field.
        - [R] stored in a column with a “VARCHAR” datatype.
        - [X] stored with escaping, i.e. “&” as “&amp;”.
        - [X] undergo background clean-up.
        - [R] no XML-stamp.
        - [X] no escaping when put into HTML for display on website.
        - [X] undergo *all* conversions from HTML markup to Latex markup.
        '''
        super().__init__(urdata, __file__)

        self.repros_path = self.paths['repros'] + os.sep
        self.glossary_sigla = dict(self.config['glossary'].getjson('sigla'))
        self.bib_astl_sections = dict(
                self.config['bib_astl'].getjson('sections'))
        self.config['mail']['user'], self.config['mail']['password'] =\
                fs.get_keys(self.paths['mailserver_access'])
        self.connect = sql_io.get_connect(
                *fs.get_keys(self.paths['db_access']),
                self.config,
                )
        self.connect_jordanus = sql_io.get_connect(
                *fs.get_keys(self.paths['db_access']),
                self.config,
                db_name_key = 'name_jordanus',
                )
        if self.config['modes'].getboolean('refreshing'):
            self.process_bib = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../collect_bib.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_fulltext = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../collect_fulltext.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_repros = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../collect_repros.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_db = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../process_db.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
        self.make_temptable_jordanus()

    def amend_cells(
            self,
            item: 'dict[str, str|int|None]',
            request: 'bottle.LocalRequest|void' = void,
            doc_type: str = '',
            doc_id: int = None,
            sectionlabels = {
                'ms_codicology': 'Cod.',
                'ms_content': 'Cont.',
                'ms_bibliography': 'Bibl.',
                'ms_bibl_special': 'Bibl.',
                'print_bibliography': 'Bibl.',
                'print_exemplar': 'Exemplar',
                'work_origin_ext': 'Origin',
                'work_content': 'Content',
                'work_bibliography': 'Bibl.',
                'work_printhistory': 'Modern ed.',
                'work_quotation': 'Text',
                },
            bib_re: 're.Pattern[str]' = re.compile(r'''(?x)
                \\bibtexkey\s*
                \{(?P<siglum>[^}\\]*)\}
                (?:\s*\{(?P<passage>[^}\\]*)\})?
                # Negation of \\ prevents from running into next bibtexkey.
                '''),
            p_tag_re: 're.Pattern[str]' = re.compile(r'<p[^>]*>(?!<b>)'),
            space_re: 're.Pattern[str]' = re.compile(r'\s+'),
            ) -> 'dict[str, str]':
        '''
        In :param:`item`, ensure that every value is a string. Exception: values
        which can be an integer and whose key ends with “_id”: They are returned
        as integer.

        If the item has a key `pagina_range`, check whether there is a belonging
        facsimile or transcription or both, and accordingly add zero, one or two
        links to the item – such a link leads to a page presenting the facsimile
        or transcription or both.
        '''
        def consider_endtag_tex(
                old_tag: str,
                old_attrs: 'dict[str, None|str]',
                new_tag: str,
                new_attrs: 'dict[str, None|str]',
                parser: 'xmlhtml.Replacer',
                ) -> None:
            if old_attrs.get('dir', '') == 'ltr':
                parser.info['in_ltr'] -= 1
            if old_tag == 'a':
                parser.info['in_a'] -= 1
            elif new_tag.endswith('arabic'):
                parser.info['in_arabic'] -= 1
            elif new_tag.endswith('hebrew'):
                parser.info['in_hebrew'] -= 1
            elif old_tag == 'latex-only':
                parser.info['in_latex-only'] -= 1
            return None

        def escape_tex_except_ampersand(term: str) -> str:
            '''Tex escaping except for “&”.'''
            return term.replace(
                    '\\', '\\textbackslash').replace(
                    '{', '\\{').replace(
                    '}', '\\}').replace(
                    '\\textbackslash', '{\\textbackslash}').replace(
                    '{\\textbackslash}!', '\\!').replace(
                    '^', '{\\textasciicircum}').replace(
                    '~', '{\\textasciitilde}').replace(
                    '_', '\\_').replace(
                    '#', '\\#').replace(
                    '$', '\\$').replace(
                    '%', '\\%')

        def insert_bib(con, cur, lang_id: int, match: 're.Match[str]') -> str:
            siglum = match.group('siglum')
            full_form_display = False
            if '@' in siglum:
                display_siglum, siglum = siglum.split('@', 1)
                display_siglum = display_siglum.strip()
                if display_siglum == '#':
                    full_form_display = True
                    display_siglum = ''
            else:
                display_siglum = ''
            siglum = space_re.sub('', siglum)
            passage = (match.group('passage') or '').strip()
            if passage:
                passage = ', ' + passage
            item = con.get(r"* from bib where bib_sigle = %s", siglum)
            if not item:
                return (display_siglum or siglum) + passage
            lit = bottle.template(
                    'astrobibl_item.tpl' if lang_id == 3 else 'bib_item.tpl',
                    invert_italics = self.invert_italics,
                    rename = partial(self.rename_bib2, invert = False)
                        if lang_id == 3 else self.rename_bib1,
                    invert_til_authors = 0,
                    **item)[:-1].replace('\n', ' ')
            if not display_siglum and not full_form_display:
                display_siglum = item['bib_kurzform']
            classes = 'card resolution' if display_siglum else 'unabbreviated'
            return f'<abbr class="siglum" tabindex="0">{display_siglum}</abbr>'\
                    f'<span class="{classes}">{lit}</span>{passage}'

        def prepend_label(match: 're.Match[str]') -> str:
            nonlocal label_num
            label_num += 1
            return f'{match.group()}<b>Note\u2007{label_num}</b>\u2003'

        def printprocess(term: str) -> str:
            if XML_STAMP in term or key in EXTRA_XML_COLS:
                # Convert HTML-XML markup in Tex markup (must not use “&”):
                term = xmlhtml.replace(
                        term,
                        replace_element = replace_element_tex,
                        replace_tag = replace_tag_tex,
                        replace_text = replace_text_tex,
                        consider_endtag = consider_endtag_tex,
                        info = {
                            'in_a': 0,
                            'in_arabic': 0,
                            'in_hebrew': 0,
                            'in_latex-only': 0,
                            'in_ltr': 0,
                            'key': key,
                            },
                        )
                for old, new in (
                        # </tag₁·tag₂> → </tag₂></tag₁>:
                        (r'</([^·]*)·([^·]*)>', r'</\g<2>></\g<1>>'),
                        # <tag₁·tag₂ attrs> → <tag₁ attrs><tag₂>:
                        (r'<([^·]*)·([^>\s]*)([^>]*)>', r'<\g<1>\g<3>><\g<2>>'),
                        # starttag → tex prefix:
                        (r'(?s)<a\b[^>]*?href=""[^>]*>((?:(?!</a>).)*)</a>',
                            r'\\UNDEFLINK{}{\g<1>}'),
                        (r'(?s)<a\b[^>]*?href="(/ms/[^"]*)"[^>]*>((?:(?!</a>).)*)</a>',
                            r'\\MSLINK{\g<1>}{\g<2>}'),
                        (r'(?s)<a\b[^>]*?href="(/work/[^"]*)"[^>]*>((?:(?!</a>).)*)</a>',
                            r'\\WORKLINK{\g<1>}{\g<2>}'),
                        (r'(?s)<a\b[^>]*?href="([^"]*)"[^>]*>((?:(?!</a>).)*)</a>',
                            r'\\EXTLINK{\g<1>}{\g<2>}'),
                        (r'(?s)<abbr>(.*?)</abbr><(?:resolution|unabbreviated)>(.*?)</(?:resolution|unabbreviated)>',
                            r'\\BIBLITEM{\g<1>}{\g<2>}'),
                        (r'\s*<arabic>', r'\n\\begin{arab}\\arabicsettings{}'),
                        (r'</arabic>\s*', r'\\end{arab}\n'),
                        (r'<arabic_u>', r'\\arabol{'),
                        (r'<hebrew_u>', r'\\hebrol{'),
                        (r'<b>', r'\\textbf{'),
                        (r'<ext-ms>', r'\\EXTMS{'),
                        (r'\s*<hebrew>', r'\n\\begin{hebrew}\\hebrewsettings{}'),
                        (r'</hebrew>\s*', r'\\end{hebrew}\n'),
                        (r'<i>', r'\\textit{'),
                        (r'<li>', r'\\item '),
                        (r'</li>', ''),
                        (r'<non-public>', r'\\NONPUBLIC{'),
                        (r'<ol>', r'\\begin{enumerate}'),
                        (r'<olABC>', r'\\begin{enumerate}[A.]'),
                        (r'</ol(ABC)?>', r'\\end{enumerate}'),
                        (r'<p>', r''),
                        (r'</p>\s*', r'\\par\n'),
                        (r'<small-caps>', r'\\textsc{'),
                        (r'<sup>', r'\\textsuperscript{'),
                        (r'<textarabic>', r'\\textarab{'),
                        (r'<textenglish>', r'\\textenglish{'),
                        (r'<texthebrew>', r'\\texthebrew{'),
                        (r'<u>', r'\\underline{'),
                        (r'<ul>', r'\\begin{itemize}'),
                        (r'</ul>', r'\\end{itemize}'),
                        # endtag → closing bracket:
                        (r'</[^>]*>', r'}'),
                        # etc.:
                        (r'\b\u0644\u0644\u0647\b', r'{\\lillah}'),
                        (r'\\end\{(arabic|hebrew)\}\s*\\begin\{(arabic|hebrew)\}',
                            r'\\par\n'),
                        (r'\n(\s*\n)+', r'\n'),
                        ):
                    term = re.sub(old, new, term)
                # Unescape HTML-XML escapings:
                term = xmlhtml.unescape(term)
            else:
                term = escape_tex_except_ampersand(term)
            # Tex escaping for “&”:
            term = term.replace('&', '\\&')
            return term

        def replace_element(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'None|str':
            if tag == 'latex-only':
                return ''
            elif tag == 'non-public' and not request.roles:
                return ''
            else:
                return None

        def replace_element_tex(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'None|str':
            if tag == 'web-only':
                return ''
            else:
                return None

        def replace_tag_tex(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'tuple[str, dict[str, None|str], bool]':
            if tag == 'latex-only':
                parser.info['in_latex-only'] += 1
            elif tag == 'cite':
                tag = 'i'
            if tag not in {
                    'a',
                    'abbr',
                    'b',
                    'ext-ms',
                    'i',
                    'li',
                    'non-public',
                    'ol',
                    'p',
                    'small-caps',
                    'sup',
                    'u',
                    'ul',
                    }:
                tag = ''

            if tag == 'ol' and 'ABC' in attrs.get('class', ''):
                tag = 'olABC'
            elif attrs.get('lang', '') == 'he':
                if tag == 'p':
                    tag = 'hebrew'
                else:
                    tag += '·texthebrew'
                parser.info['in_hebrew'] += 1
            elif attrs.get('lang', '') == 'ar' or attrs.get('dir', '') == 'rtl':
                if tag == 'p':
                    tag = 'arabic'
                else:
                    tag += '·textarabic'
                parser.info['in_arabic'] += 1
            elif attrs.get('dir', '') == 'ltr':
                tag += '·textenglish'
                parser.info['in_ltr'] += 1
            elif 'resolution' in attrs.get('class', ''):
                tag += '·resolution'
            elif 'unabbreviated' in attrs.get('class', ''):
                tag += '·unabbreviated'
            tag = tag.lstrip('·')

            if tag == 'a':
                attrs = {'href': attrs.get('href', '')} # even if href is empty.
                parser.info['in_a'] += 1
            else:
                attrs = {}

            if parser.info['in_arabic'] and tag in {'u', 'u·textarabic'}:
                tag = tag.replace('u', 'arabic_u', 1)
            elif parser.info['in_hebrew'] and tag in {'u', 'u·texthebrew'}:
                tag = tag.replace('u', 'hebrew_u', 1)

            return tag, attrs, False

        def replace_text_tex(text: str, parser: 'xmlhtml.Replacer') -> str:
            if not parser.info['in_latex-only']:
                text = escape_tex_except_ampersand(text)
            if parser.info['key'] in {
                    'mspart_quotation',
                    'work_quotation',
                    'work_title_arabic',
                    } and not parser.info['in_ltr'] and not parser.info['in_a']:
                text = text.replace(
                        '〈', '{\\CLANBR}').replace(
                        '〉', '{\\OPANBR}')
            return text

        def sub_intextref(
                con: 'mysql.Connection',
                cur: 'mysql.cursors.DictCursor',
                match: 're.Match[str]',
                ) -> str:
            key, num, name, title, published = intextref(con, cur, match)
            if num[:1] == '█': # A new case of “█”.
                return f'\\{key}{{{num}#{name}}}'
            elif key == 'externalms':
                num_name = f'{num}#{name}' if name else num
                return f'<ext-ms>{num_name}</ext-ms>'
            else:
                kind = 'ms' if key == 'msshelfmark' else 'work'
                if key == 'worktitle':
                    tag = 'span'
                    href = ''
                else:
                    tag = 'a'
                    href = '' if num == '0' else f' href="/{kind}/{num}"'
                if published:
                    title = f' title="{title}"' if title else ''
                    return f'<{tag}{href}{title} '\
                            f'data-crossref="{kind}">{name}</{tag}>'
                else:
                    if title:
                        title += ' · '
                    title += 'not published yet'
                    return f'<{tag} title="{title}" '\
                            f'data-crossref="{kind}">{name}</{tag}>'

        lang_id = item.get('lang_id', 0)
        for key in item:
            term = item[key]
            if term is None:
                term = ''
            elif isinstance(term, str) and term:
                with sql_io.DBCon(self.connect()) as (con, cur):
                    if '\\bibtexkey' in term:
                        term = bib_re.sub(
                                partial(insert_bib, con, cur, lang_id), term)
                    term = INTEXTREF_RE.sub(
                            partial(sub_intextref, con, cur), term)
                if doc_type == 'ms' and key == 'place_name' and term == '?':
                    term = '―'
                if key in sectionlabels:
                    label = sectionlabels[key]
                    if lang_id == 1:
                        if key == 'work_printhistory':
                            label = 'Ed.'
                        label += ':'
                    elif key in {'ms_codicology', 'ms_content'}:
                        label = ''
                    if label:
                        label += '\u2003'
                    term = term.replace('<p>', f'<p><b>{label}</b>', 1)
                if key in {'ms_note', 'print_note', 'work_note'}:
                    label_num = 0
                    term = p_tag_re.sub(prepend_label, term)
                    if label_num == 1:
                        term = term.replace(
                                '<b>Note\u20071</b>\u2003',
                                '<b>Note</b>\u2003')
                elif key in {'mspart_quotation', 'work_quotation'}:
                    term = term.replace('<u>', '<u class="num">')
                elif key == 'work_title':
                    term = term.replace('(tr. ', '(tr.\xa0').split(' (by')[0]
                    if ' (' in term:
                        term, tail = term.split(' (', 1)
                        term = f'<cite>{term}</cite> ({tail}'
                    else:
                        term = f'<cite>{term}</cite>'
                if request.query.form == 'print':
                    term = printprocess(term)
                else:
                    if XML_STAMP in term or key in EXTRA_XML_COLS:
                        term = xmlhtml.replace(
                                term,
                                replace_element = replace_element,
                                )
                    term = term.replace('␣', '\u00a0' # no-break space
                            ).replace('˷', '\u00ad' # soft hyphen
                            ).replace('◑', '\u200b' # zero width space
                            ).replace('⁻', '-' # for Tex, normal hyphen else.
                            )
            item[key] = term
        if item.get('pagina_range', '').strip():
            part_id = item.get('part_id', '')
            with sql_io.DBCon(self.connect()) as (con, cur):
                result = con.get("""
                            substring_index(pagina_sorter, '/', -1) as pagina,
                            scan_path,
                            tran_path
                        from repros
                        where doc_type = %s and part_id = %s
                        order by isnull(fulltran_part), pagina_sorter
                        collate utf8mb4_bin
                        """, doc_type, part_id)
            if result:
                pagina = result['pagina']
                scan_path = result['scan_path']
                tran_path = result['tran_path']
            else:
                pagina = ''
                scan_path = ''
                tran_path = ''
            link = f'/{doc_type}/{doc_id}/{part_id}/{pagina}'
            item['scan_link'] = link if scan_path else ''
            item['tran_link'] = link if tran_path else ''
        return item

    def auth(
            self,
            request: 'bottle.LocalRequest',
            ) -> 'tuple[None|int, None|str, set[str]]':
        '''
        For the web request :param:`request` and the belonging session, get
        the stored ID, name and the set of roles of the user. If no session
        is registered or no user ID is known, return ``None``, ``None`` and
        the empty set instead.

        This is used to decide if a request comes from a user who is logged
        in and has the required roles.
        '''
        try:
            user_id = request.environ['beaker.session']['user_id']
            assert user_id not in (None, '')
            with sql_io.DBCon(self.connect()) as (con, cur):
                cur.execute(r'''
                        select
                            auth_user.email as name,
                            auth_group.role as role
                        from auth_user
                        join auth_membership on
                            auth_membership.user_id = auth_user.id
                        join auth_group on
                            auth_group.id = auth_membership.group_id
                        where auth_user.id = %s
                        ''', (user_id,))
                results = cur.fetchall()
            name = results[0]['name']
            roles = { result['role'] for result in results }
            return user_id, name, roles
        except: # Sic.
            return None, None, set()

    def auth_in(self, user_name: str, password: str) -> 'None|int':
        '''
        For the combination of :param:`user_name` and :param:`password`, if
        this combination can be verified, return the belonging user ID; but
        if the combination is not known, return ``None``.

        This is used to perform the login of a user.
        '''
        try:
            user_name = user_name.strip()
            # The user_name is the email. It may be empty and is then invalid.
            assert '@' in user_name
            with sql_io.DBCon(self.connect()) as (con, cur):
                cur.execute(r'''
                        select id, password from auth_user where email = %s
                        ''', (user_name,))
                results = cur.fetchall()
            assert len(results) == 1
            user_id = results[0]['id']
            _, salt, password_hash = results[0]['password'].split('$')
            salt = salt.encode()
            password_hash = password_hash.encode()
            password = password.encode()
            assert password_hash == hexlify(pbkdf2_hmac(
                    'sha512', password, salt, iterations = 1000, dklen = 20
                    ))
            return user_id
        except Exception as e: # Sic.
            return None

    def add_sentence_jordanus(
            self,
            ms_id: 'None|int',
            subsiglum: 'None|int',
            field_id: int,
            term: str,
            ) -> int:
        '''
        In Jordanus, add a new sentence to the database: about the manuscript
        :param:`ms_id` and its part :param:`subsiglum`, which is ``0`` if the
        whole manuscript is meant. The predicate (or verb) of the sentence is
        :param:`field_id`, the predicative is the term :param:`term`.

        If the term is not already given in the database, it is added. Its ID
        is used in the sentence. Return the ID of the new sentence.

        If :param:`ms_id` is ``None``, a new manuscript is added. The ID will
        be the greatest manuscript ID so far incremented by ``1``. If, at the
        same time, :param:`subsiglum` is ``None``, too, the subsiglum will be
        set to ``0``.

        If only :param:`subsiglum` is none, the subsiglum will be the highest
        subsiglum so far incremented by ``1``.
        '''
        with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
            while True:
                cur.execute(r'''
                        select term_id from terms where term = %s limit 1
                            LOCK IN SHARE MODE''', term)
                match = cur.fetchone()
                if match:
                    term_id = match['term_id']
                    break
                cur.execute(r'insert into terms (term) values (%s)', term)
            if ms_id is None:
                cur.execute(r'''
                        select @num := max(ms_id) from sentences FOR UPDATE;
                        insert into sentences
                            (ms_id, subsiglum, field_id, term_id) values (
                                @num + 1,
                                %s, %s, %s
                                );
                        ''', (subsiglum or 0, field_id, term_id))
            elif subsiglum is None:
                cur.execute(r'''
                        select @num := max(subsiglum) from sentences
                            where ms_id = %s FOR UPDATE;
                        insert into sentences
                            (ms_id, subsiglum, field_id, term_id) values (
                                %s,
                                @num + 1,
                                %s, %s
                                );
                        ''', (ms_id, ms_id, field_id, term_id))
            else:
                cur.execute(r'''
                        insert into sentences
                            (ms_id, subsiglum, field_id, term_id) values
                            (%s, %s, %s, %s);
                        ''', (ms_id, subsiglum, field_id, term_id))
            con.commit()
            sentence_id = cur.lastrowid
        return sentence_id

    def bib_upload(
            self,
            request: 'bottle.LocalRequest',
            ext: str = '.bib',
            max_size: int = 1024 ** 3, # 1 gibibyte
            ) -> str:
        note = ''
        try:
            file = request.files.file
            if not file:
                raise Exception('No file was given.')
            names = sorted(
                    n for n in os.listdir(self.paths['bib'])
                    if n.endswith(ext) )
            if names:
                infilename = names[-1]
            else:
                infilename = fs.sanitize(file.raw_filename)
                if not infilename.endswith(ext):
                    infilename += ext
            infilepath = os.path.join(self.paths['bib'], infilename)
            file.save(infilepath, overwrite = True, max_size = max_size)
        except Exception as e:
            note = str(e)
        else:
            note = 'The new bibliography file is saved.'\
                   ' Please wait until it is ingested into the database.'
        return note

    def change_sentence_term_jordanus(
            self,
            sentence_id: int,
            term: str,
            ) -> 'None|int':
        '''
        In Jordanus, change the sentence with the ID :param:`sentence_id`. It
        is :param:`term` the new predicative (or verb) of the sentence.

        If the term is not already given in the database, it is added. Its ID
        is used in the sentence. Return the ID of the eventually used term.

        If the old term is no longer used, delete it. - This may leave a term
        behind that is no longer used in any sentence. Delete this term, too.

        If :param:`sentence_id` is unknown, return ``None``.
        '''
        term_id = None
        with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
            # We check whether a sentence with sentence_id exists and, if so,
            # get its old term_id for deleting the term if it is no longer used.
            cur.execute(r'''
                    select term_id from sentences where sentence_id = %s
                    ''', (sentence_id,))
            match = cur.fetchone()
            if match:
                old_term_id = match['term_id']
                while True:
                    cur.execute(r'''
                            select term_id from terms where term = %s limit 1
                                LOCK IN SHARE MODE''', (term,))
                    match = cur.fetchone()
                    if match:
                        term_id = match['term_id']
                        break
                    cur.execute(r'''
                            insert into terms (term) values (%s)
                            ''', (term,))
                cur.execute(r'''
                        update sentences set term_id = %s where sentence_id = %s
                        ''', (term_id, sentence_id))
                con.commit()
                try:
                    cur.execute(r'''
                            delete from terms where term_id = %s
                            ''', (old_term_id,))
                    con.commit()
                except IntegrityError:
                    con.rollback()
            return term_id

    def clean_up_forum(self) -> None:
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(
                    r"delete from forum_posts where title = '' and doc = ''")
            con.commit()

    def create_forum_post(self, user_id: int, page_id: int) -> int:
        '''
        Create a new post of the user :param:`user_id` and the page
        :param:`page_id`. If this page does not exist, create a new
        page. Return the ID of the page and the post.
        '''
        von = int(time.strftime('%Y%m%d%H%M'))
        bis = von
        with sql_io.DBCon(self.connect()) as (con, cur):
            is_new = False
            if not con.get(r"1 from forum_pages where page_id = %s", page_id):
                is_new = True
                cur.execute(r"insert into forum_pages (page_id) values (null)")
                con.commit()
                page_id = cur.lastrowid
            cur.execute(r"""
                    insert into forum_posts (
                        page_id,
                        user_id,
                        von,
                        bis,
                        doc
                        ) values (%s, %s, %s, %s, '')
                    """, (page_id, user_id, von, bis,))
            con.commit()
            post_id = cur.lastrowid
        return page_id, post_id

    def del_forum_page(self, page_id: int) -> None:
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(r"delete from forum_posts where page_id = %s", page_id)
            cur.execute(r"delete from forum_pages where page_id = %s", page_id)
            con.commit()

    def del_forum_post(self, page_id: int, post_id: int) -> bool:
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(r"delete from forum_posts where post_id = %s", post_id)
            if not con.get(r"1 from forum_posts where page_id = %s", page_id):
                cur.execute(
                        r"delete from forum_pages where page_id = %s", page_id)
                page_deleted = True
            else:
                page_deleted = False
            con.commit()
        return page_deleted

    def del_sentence_jordanus(self, sentence_id: int) -> None:
        '''
        In Jordanus, delete the sentence with the ID :param:`sentence_id`.

        This may leave a term or a field behind that is no longer used in
        any sentence. A dangling field should not be removed - a dangling
        term, however, is removed.
        '''
        with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
            cur.execute(r'select term_id from sentences where sentence_id = %s',
                    (sentence_id,))
            sentence = cur.fetchone()
            if sentence is None:
                return
            cur.execute(r'delete from sentences where sentence_id = %s',
                    (sentence_id,))
            con.commit()
            term_id = sentence.get('term_id', None)
            if term_id is None:
                return
            try:
                cur.execute(r'delete from terms where term_id = %s', (term_id,))
                con.commit()
            except IntegrityError:
                con.rollback()

    def export_contributors(
            self,
            contributors:
                'dict[tuple[str, str], dict[str, list[dict[str, str]]]]',
            ) -> str:
        def serialize_ms(ms: 'dict[str, str]') -> str:
            return f"{ms['place']}, {ms['lib']}, {ms['shelfmark']}"

        def serialize_work(work: 'dict[str, str]') -> str:
            return f"{work['siglum']} {work['author']}, {work['title']}"

        def serialize_contributor(
                contributor:
                    'tuple[tuple[str, str], dict[str, list[dict[str, str]]]]',
                ) -> str:
            last_name, first_name = contributor[0]
            mss = sorted(
                    contributor[1]['mss'],
                    key = lambda ms: parse.sortnumtext(serialize_ms(ms)),
                    )
            mss = '; \\ \n'.join(map(serialize_ms, mss))
            works = sorted(
                    contributor[1]['works'],
                    key = lambda w: parse.sortnumtext(serialize_work(w)),
                    )
            works = '; \\ \n'.join(map(serialize_work, works))
            return rf'''\textbf{{{first_name} {last_name}}}\smallskip

\textit{{Work descriptions}}: \ 
{works}.

\textit{{Manuscript descriptions}}: \ 
{mss}.'''

        contributors = '\\bigskip\n\n\n'.join(map(
                serialize_contributor,
                sorted(
                    contributors.items(),
                    key = lambda x: x[0][0].removeprefix('van').strip()),
                ))
        doc = rf'''
\chapter[The contributors]{{The contributors\\\footnotesize (in alphabetical order)\bigskip}}

{{\parindent0pt \parskip0pt plus0.25pt \small
 \everypar{{\hangindent4.5mm}}


{contributors}\par
\leavevmode }} % end of \everypar

\endinput
'''
        doc = amend_tex_doc(doc)
        return doc

    def export_db(self) -> 'ty.Generator[dict[str, tuple[dict[str, str]]]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            for table, where in (
                    ('Aliases',        "is_active = 'T' and f_hidden = 'F'"),
                    ('Creator_Roles',  "is_active = 'T'"),
                    ('Creators',       "is_active = 'T'"),
                    ('Ed_Parts',       "is_active = 'T' and f_hidden = 'F'"),
                    ('Editions',       "is_active = 'T' and f_hidden = 'F'"),
                    ('Gloss_Concepts', "is_active = 'T'"),
                    ('Gloss_Refs',     "is_active = 'T'"),
                    ('Gloss_Termrefs', "is_active = 'T'"),
                    ('Gloss_Terms',    "is_active = 'T'"),
                    ('Languages',      "is_active = 'T'"),
                    ('Libraries',      "is_active = 'T' and f_hidden = 'F'"),
                    ('MS_Parts',       "is_active = 'T' and f_hidden = 'F'"),
                    ('Manuscripts',    "is_active = 'T' and f_hidden = 'F'"),
                    ('Persons',        "is_active = 'T' and f_hidden = 'F'"),
                    ('Places',         "is_active = 'T' and f_hidden = 'F'"),
                    ('Source_types',   "is_active = 'T'"),
                    ('Titles',         "is_active = 'T' and f_hidden = 'F'"),
                    ('Works',          "is_active = 'T' and f_hidden = 'F'"),
                    ):
                if where:
                    where = ' where ' + where
                yield {table: tuple(
                        { k: str(v) for k, v in row.items() }
                        for row in con.geteach(rf"* from {table}{where}")
                        )}

    def export_doc(
            self,
            item: 'str|dict[str, str|int|None]',
            doc_type: str,
            doc_id: int,
            form: str,
            roles: 'set[str]',
            font_re: 're.Pattern[str]' = re.compile(
                r'(@font-face \{[^}]*}|font-family\s*:[^\n]*)\s*'),
            note_re: 're.Pattern[str]' = re.compile(r'''(?x)
                <span
                    \s+class\s*=\s*"[^"]*note[^"]*"
                    (?:\s+data-off="[^"]*")?
                \s*>
                    <a
                        \s+aria-label\s*=\s*"[^"]*"
                        \s+class\s*=\s*"[^"]*"
                        \s+href\s*=\s*"[^"]*"
                    \s*>
                    </a>
                    <small
                        \s+class\s*=\s*"note"
                        \s+data-sign\s*=\s*"[^"]*"
                        (?:\s+dir\s*=\s*"[^"]*")?
                        \s+id\s*=\s*"[^"]*"
                    \s*>
                        \s*((?:(?!</small></span>).)*)
                    </small>
                </span>
                '''),
            newline_re: 're.Pattern[str]' = re.compile(r'\s*\n\s*'),
            para_re: 're.Pattern[str]' = re.compile(r'</p>\s*<p\b'),
            space_re: 're.Pattern[str]' = re.compile(r'[ \t]+'),
            ) -> str:
        '''
        Make :param:`item` to a string representation which can be downloaded as
        a standalone file. If :param:`item` is a string, rewrite it according to
        :param:`form`, i.e.:

        - “editor”: write the HTML so that it could be opened in an editor (like
          LibreOffice Writer or Microsoft Word).
        - “print”: write the HTML so that it is ready to be sent to the printer.

        If :param:`item` is not a string, make it to a string in tex format.
        '''
        def consider_endtag(
                old_tag: str,
                old_attrs: 'dict[str, None|str]',
                new_tag: str,
                new_attrs: 'dict[str, None|str]',
                parser: 'xmlhtml.Replacer',
                ) -> None:
            if old_tag == 'main' and parser.info['in_main']:
                parser.info['in_main'].pop()
            elif old_tag == 'non-public':
                parser.info['in_non-public'] -= 1
            if form in {'editor', 'print'}:
                if old_tag in parser.info['inline']:
                    parser.info['inline'][old_tag] -= 1
                    if parser.info['inline'][old_tag] == 0:
                        parser.info['inline'].pop(old_tag, 0)

        def replace_element(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'None|str':
            classes = set(attrs.get('class', '').split())
            if tag == 'main':
                parser.info['in_main'].append(True)
            if tag in {'footer', 'link', 'nav', 'noscript', 'script'}:
                return ''
            elif tag == 'header' and not parser.info['in_main']:
                return ''
            elif tag == 'non-public' and (not roles or form == 'print'):
                return ''
            elif (form in {'editor', 'print'} and 'resolution' in classes):
                return ''
            return None

        def replace_tag(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'tuple[str, dict[str, None|str], bool]':
            if form in {'editor', 'print'}:
                # Some seemingly pointless – or even harmful – substitutions are
                # necessary to accommodate certain rendering devices.
                styles = xmlhtml.get_styles(attrs.get('style', ''))
                classes = set(attrs.get('class', '').split())
                if 'inline' in classes:
                    parser.info['inline'][tag] += 1
                elif sum(parser.info['inline'].values()):
                    if tag in {'h2', 'h3', 'h4', 'h5', 'h6'}:
                        tag = 'h-b'
                    elif tag == 'p':
                        tag = ''
                if tag == 'a':
                    href = attrs.get('href', '')
                    if form == 'print':
                        if re.search(r'''(?ix)
                                ^https?://(www\.)?(
                                    archive\.org
                                    |scholarlyeditions\.brill\.com
                                )''', href):
                            tag = ''
                        elif 'data-crossref' in attrs:
                            tag = ''
                            parser.info['crossrefmark'] = '*' if href else '*!'
                    if tag:
                        if not href:
                            tag = ''
                        elif href.startswith('/'):
                            attrs['href'] = f"{self.config['ids']['url']}{href}"
                        if parser.info['in_non-public']:
                            styles['color'] = '#ff0000'
                elif tag == 'aside':
                    tag = 'div'
                elif tag == 'bdo':
                    tag = 'span'
                elif tag == 'h1':
                    styles['font-size'] = '16pt'
                elif tag == 'header':
                    tag = 'div'
                elif tag == 'non-public':
                    parser.info['in_non-public'] += 1
                    tag = 'span'
                    styles['color'] = '#ff0000'
                elif tag == 'section':
                    tag = 'div'
                    if 'inline' in classes:
                        styles['font-size'] = '12pt'
                elif tag == 'small-caps':
                    tag = 'span'
                    styles['font-variant'] = 'small-caps'
                elif tag in {'sub', 'sup'}:
                    styles['font-size'] = '12pt'
                elif tag in {'div', 'li', 'p', 'summary'}:
                    styles['text-align'] =\
                            'right' if 'end' in classes\
                            else 'justify'
                    styles['font-size'] = '12pt'
                    styles['margin'] = '4pt 0' if tag == 'div' else '1pt 0'
                if 'petit' in classes:
                    styles['font-size'] = '10.5pt'
                if classes:
                    attrs['class'] = ' '.join(classes)
                else:
                    attrs.pop('class', '')
                if styles:
                    attrs['style'] = xmlhtml.serialize_styles(styles)
                else:
                    attrs.pop('style', '')
            return tag, attrs, startend

        def replace_text(text: str, parser: 'xmlhtml.Replacer') -> str:
            if parser.info['crossrefmark']:
                text = parser.info['crossrefmark'] + text
                parser.info['crossrefmark'] = ''
            return text

        if isinstance(item, str):
            doc = xmlhtml.replace(
                    item,
                    consider_endtag = consider_endtag,
                    replace_declaration = lambda decl, parser: decl,
                    replace_element = replace_element,
                    replace_tag = replace_tag,
                    replace_text = replace_text,
                    info = {
                        'inline': defaultdict(int),
                        'in_main': [],
                        'in_non-public': 0,
                        'crossrefmark': '',
                        },
                    )
            doc = newline_re.sub(r'\n', doc)
            doc = space_re.sub(r' ', doc)
            for old, new in (
                    ('<ext-ms>', '*'),
                    ('</ext-ms>', ''),
                    ('<h-b', '<b'),
                    ('</h-b>', '</b> '), # The space is necessary.
                    ('<non-non-public>', '<non-public>'),
                    ('</non-non-public>', '</non-public>'),
                    ):
                doc = doc.replace(old, new)
            if form in {'editor', 'print'}:
                doc = note_re.sub(r'#beginnote\g<1>#endnote', doc)
            if form == 'print':
                doc = para_re.sub(r'</p>\n<p style="text-indent: 4.5mm"', doc)
            css = fs.read(self.paths['cssjs'] + '/all.css').replace(
                    '@charset "utf-8";', '').strip() + '\n'
            css += fs.read(self.paths['cssjs'] + '/badw_pal.css').replace(
                    '@charset "utf-8";', '').strip()
            if form in {'editor', 'print'}:
                css = font_re.sub('', css)
            doc = doc.replace('</head>', f'<style>\n{css}\n</style>\n</head>')
        else:
            doc = ''
            compiler_string = ', '.join(
                    name.split('▄')[-1]
                    for name in item['compiler_names'].split('█')
                    )
            if (
                    item.get('work_add_credit') == 'T' or
                    item.get('ms_add_credit') == 'T'
                    ):
                compiler_string += ' \\MJPP'
            if doc_type == 'ms':

                place_name = item['place_name'].lstrip('?')
                place_name = place_name + ', ' if place_name else ''

                if item['ms_short']:
                    head = f'''\\SHORTDESCRIPTION{{{item['ms_short']}}}'''
                else:
                    head = f'''\\CHARACTERISATION{{{item['ms_info']}}}
\\DATE{{{item['ms_datestring']}}}
\\MSORIGIN{{{item['ms_origin']}}}
\\PROVENANCE{{{item['ms_provenance']}}}'''

                msparts = []
                for mspart in item['msparts']:
                    msparts.append(
f'''\\MSPARTTABLE{{{mspart['pagina_range']}}}{{{mspart['mspart_quotation']}}}
    {{\\EQUALSWORK{{{mspart['person_name']}, {mspart['work_title']} ({mspart['work_siglum']})}}{mspart['mspart_note']}}}''')

                msparts = '\n'.join(msparts)
                doc = f'''\\BEGINMANUSCRIPT
\\SHELFMARK{{{place_name}{item['lib_name']}, {item['ms_shelfmark']}}}{{{place_name}{item['lib_abbrev'] or item['lib_name']}, {item['ms_shelfmark']}}}

\\USEDSOURCE{{{item['ms_avail_source']}}}

{head}

\\CODICOLOGY{{{item['ms_codicology']}}}

\\MSCONTENT{{{item['ms_content']}}}

\\MSNOTE{{{item['ms_note']}}}

\\MSBIBLIOGRAPHY{{{item['ms_bibliography']}}}

\\MSPARTS
{msparts}
\\SIGNED{{{compiler_string}}}
\\ENDMANUSCRIPT
'''
            elif doc_type == 'work':

                mss = []
                for ms in item['msparts']:
                    place_name = ms['place_name'].lstrip('?')
                    place_name = place_name + ', ' if place_name else ''
                    lib = ms['lib_abbrev'] or ms['lib_name']
                    pagina = ms['mspart_info'] or ', ' + ms['pagina_range']
                    mss.append(
                            (
                                '\\MSREF{' if ms['ms_hidden'] == 'F' else
                                '\\MSREFUNPUBLISHED{'
                            ) + (
                                '{\\SHORTDESCSYMBOL}' if ms['ms_has_short'] else
                                ''
                            ) +
                                f"{place_name}"
                                f"\\LIBABBREV{{{lib}}}, "
                                f"{ms['ms_shelfmark']}"
                                f"{pagina}"
                                "}")

                mss = '\n'.join(mss)
                if mss:
                    mss = f'\n\\BEGINMSLIST\n{mss}\n\\ENDMSLIST'
                doc = f'''\\BEGINWORK
\\SIGLUM{{{item['work_siglum']}}}
\\AUTHORTITLE{{{item['person_name']}}}{{{item['work_title']}}}{{{item['work_title_arabic']}}}

\\OTHERTITLES{{{item['work_other_titles']}}}

\\WORKGENERALINFO{{{item['work_origin']}}}

\\WORKORIGIN{{{item['work_origin_ext']}}}

\\WORKCONTENT{{{item['work_content']}}}

\\WORKNOTE{{{item['work_note']}}}

\\WORKQUOTATION{{{item['work_quotation']}}}

\\WORKBIBLIOGRAPHY{{{item['work_bibliography']}}}

\\EDITIONHISTORY{{{item['work_printhistory']}}}
{mss}
\\SIGNED{{{compiler_string}}}
\\ENDWORK
'''
            doc = amend_tex_doc(doc) + '\n\n\n'
        return doc

    def filter_jordanus(self, query: 'bottle.Formsdict'):
        '''
        Filter the data of Jordanus applying :param:`query`, which are
        query parameters sent from the website.
        '''
        filters = []
        for hypernum in query:
            term = unquote(getattr(query, hypernum))
            try:
                term = term.strip(' %_')
                if term:
                    term = '%' + term + '%'
                    hypernum = 0 if hypernum == 'text' else Dec(hypernum)
                    filters.append((hypernum, term))
            except:
                pass
        if filters:
            with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
                items = deque()
                ms_ids = deque()
                for hypernum, term in filters:
                    if hypernum:
                        cur.execute(r'''
                                select field_id from fields
                                where field_public > 1 and field_hypernum = %s
                                ''', hypernum)
                    else: # The term might be in the filterfield for any field.
                        cur.execute(r'''
                                select field_id from fields
                                where field_public > 1
                                ''')
                    field_ids = ', '.join(
                            str(item['field_id']) for item in cur.fetchall() )
                    if field_ids:
                        comparator = 'like' if ('_' in term or '%' in term)\
                                else '='
                        cur.execute(r'''
                                select
                                    sentences.ms_id as ms_id,
                                    subsiglum,
                                    sentences.field_id as field_id,
                                    field_name,
                                    term,
                                    location,
                                    persons,
                                    titles,
                                    incipits
                                from sentences
                                join temp on
                                    temp.ms_id = sentences.ms_id
                                join fields on
                                    fields.field_id = sentences.field_id
                                join terms on
                                    terms.term_id = sentences.term_id
                                where
                                    sentences.field_id in ({})
                                    and
                                    terms.term {} %s
                                order by location, fields.field_rank
                                '''.format(field_ids, comparator), term)
                        ms_ids.append(set())
                        for item in cur.fetchall():
                            items.append(item)
                            ms_ids[-1].add(item['ms_id'])
                if items:
                    ms_ids_intersection = set.intersection(*ms_ids)
                    for item in items:
                        ms_id = item['ms_id']
                        if ms_id in ms_ids_intersection:
                            yield (
                                    ms_id,
                                    item['subsiglum'],
                                    item['field_id'],
                                    item['field_name'],
                                    item['term'],
                                    item['location'],
                                    item['persons'],
                                    item['titles'],
                                    item['incipits'],
                                    )

    def filter_text(
            self,
            term: str,
            trim_re_nospace: 're.Pattern[str]' = re.compile('%%%+'),
            trim_re: 're.Pattern[str]' = re.compile('%%+'),
            realm_sorting: 'dict[str, int]' = {
                realm: num for num, realm in enumerate((
                    'Work', 'MS', 'Print', 'Text', 'Lemma',
                    'Bibliography', 'Astrobibl', 'Jordanus',
                    )) },
            ) -> 'tuple[list[dict], int]':
        nospace = '%%' if '%%' in term else ''
        term, term_re_str, term_re = web_io.get_filterterms(
                term,
                trim_re = trim_re_nospace if nospace else trim_re,
                adapt_regex = adapt_regex,
                )
        if not term:
            return
        with sql_io.DBCon(self.connect()) as (con, cur):
            realm_sorting_max = len(realm_sorting) + 1
            comparator = 'rlike' if nospace else 'like'
            matches = []
            matches_num = 0
            for row in con.geteach(rf"""
                    DISTINCT
                        ft_text,
                        ft_path,
                        ft_part,
                        ft_area
                    from ft
                    where ft_text {comparator} %s
                    and ft_part = ''
                    and not ft_area = 'Bibliography'
                    and not ft_area = 'Astrobibl'
                    COLLATE utf8mb4_german2_ci
                    """, (
                        term_re_str if comparator == 'rlike' else
                        ('%' + term + '%')
                        )):
                path = row['ft_path']
                area = row['ft_area']
                if ' ' in area:
                    realm, ref = area.split(' ', 1)
                else:
                    realm = ''
                    ref = area
                splits = tuple(web_io.get_filtersplits(
                        unicodedata.normalize('NFKD', row['ft_text']), term_re))
                matches_num += (len(splits) - 1) // 2
                matches.append({ str(rank): row for rank, row in enumerate((
                        {
                            '_': realm,
                            's': realm_sorting.get(realm, realm_sorting_max),
                        },
                        {
                            '_': f'<a class="dark key" href="{path}'
                                 f'?mark={self.q(term_re_str)}">{ref}</a>',
                            's': parse.sortxtext(area),
                        },
                        ''.join(splits),
                        )) })
            return matches, matches_num

    def get_bib_item(self, ident: int, mode: str) -> 'dict[str, str]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(
                    r"select * from bib where bib_sigle = %s limit 1",
                    (ident,))
            return cur.fetchone()

    def get_bib_items_of_astl(
            self,
            siglum: str,
            rename: callable,
            ) -> 'list[dict[str, str]]':
        def sort(item: 'dict[str, str]') -> str:
            first = (
                    item['bib_titel'] if item['bib_art'] == 'collection' else
                    rename(item['bib_autor'])
                    ).lstrip("ʿ`´'")
            return parse.even(
                    first + ' ' + item['bib_jahr'].split('█', 1)[0] + ' ' +
                    item['bib_titel']
                    )
        siglum = siglum.replace('\\', '').replace("'", '')
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(rf"""
                    select * from bib
                    where bib_einordnung_astl like '%{siglum}%'
                    """)
            items = cur.fetchall()
        return sorted(items, key = sort)

    def get_catalog_items_per_user(
            self,
            user_id: str,
            ) -> 'ty.Generator[tuple[int, str, str, str, int, str]]':
        '''
        Yield all catalog items compiled by the user :param:`user_id` in a
        well-sortable iterator of tuples.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            for row in con.geteach(r'''
                        Works.id               as work_id,
                        Works.f_siglum         as work_siglum,
                        Works.f_standard_title as work_title,
                        Persons.f_person_name  as person_name
                    from Works
                    join Creators
                        on Creators.f_work = Works.id
                    join Persons
                        on Persons.id = Creators.f_creator
                    where %s in (
                            Works.f_compiler,
                            Works.f_compiler2,
                            Works.f_compiler3,
                            Works.f_compiler4)
                        and Works.f_hidden = 'F'
                        and Creators.f_creator_role = 1
                    ''', user_id):
                sortname = parse.sortnumtext(row['work_siglum'])
                siglum = self.e(row['work_siglum'])
                title = self.e(row['work_title'])
                author = self.e(row['person_name'])
                if author:
                    author += ', '
                name = f"{siglum} · {author}<cite>{title}</cite>"
                yield (
                        2,
                        sortname,
                        'Works',
                        name,
                        row['work_id'],
                        f"/work/{row['work_id']}",
                        )
            for row in con.geteach(r'''
                        Manuscripts.id          as ms_id,
                        Manuscripts.f_shelfmark as ms_shelfmark,
                        Libraries.f_lib_name    as lib_name,
                        IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev) as lib_abbrev,
                        Places.f_placename      as place_name
                    from Manuscripts
                    join Libraries
                        on Libraries.id = Manuscripts.f_place_library
                    join Places
                        on Places.id = Libraries.f_place
                    where %s in (
                            Manuscripts.f_compiler,
                            Manuscripts.f_compiler2,
                            Manuscripts.f_compiler3,
                            Manuscripts.f_compiler4)
                        and Manuscripts.f_hidden = 'F'
                    order by
                        Places.f_placename,
                        IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev),
                        Manuscripts.f_shelfmark
                    ''', user_id):
                lib = row['lib_abbrev'] or row['lib_name']
                name = f"{row['place_name']}, {lib}, {row['ms_shelfmark']}"
                yield (
                        0,
                        parse.sortxtext(name),
                        'Manuscripts',
                        name,
                        row['ms_id'],
                        f"/ms/{row['ms_id']}",
                        )
            for row in con.geteach(r'''
                        Editions.id          as print_id,
                        CAST(Editions.f_date_num_exact AS CHAR) as print_date,
                        Editions.f_publisher as print_publisher,
                        Places.f_placename   as place_name
                    from Editions
                    join Places
                        on Places.id = Editions.f_place_compiled
                    where %s in (
                            Editions.f_compiler,
                            Editions.f_compiler2,
                            Editions.f_compiler3,
                            Editions.f_compiler4)
                        and Editions.f_hidden = 'F'
                    order by Places.f_placename, Editions.f_publisher, Editions.f_date_num_exact
                    ''', user_id):
                yield (
                        1,
                        parse.sortxtext(
                            f"{row['print_date']}, {row['place_name']}, {row['print_publisher']}"
                            ),
                        'Prints',
                        f"{row['place_name']}, {row['print_publisher']}, {row['print_date']}",
                        row['print_id'],
                        f"/print/{row['print_id']}",
                        )

    def get_contributors(
            self,
            request: 'bottle.LocalRequest',
            ) -> 'dict[tuple[str, str], dict[str, list[dict[str, str]]]]':
        def get_name(person_id: int, con: 'sql_io.connection') -> str:
            r = con.get(
                    r"first_name, last_name FROM auth_user WHERE id = %s",
                    person_id)
            return (r['last_name'], r['first_name'])

        contributors = defaultdict(lambda: {'mss': [], 'works': []})
        with sql_io.DBCon(self.connect()) as (con, cur):
            for item in con.getall(r'''
                        Manuscripts.id              as ms_id,
                        Manuscripts.f_shelfmark     as ms_shelfmark,
                        Libraries.f_lib_name        as lib_name,
                        IF(Manuscripts.f_cat_lang = 'Arabic',
                            Libraries.f_lib_abbrev_arabic,
                            Libraries.f_lib_abbrev) as lib_abbrev,
                        Places.f_placename          as place_name,
                        Manuscripts.f_compiler      as comp1,
                        Manuscripts.f_compiler2     as comp2,
                        Manuscripts.f_compiler3     as comp3,
                        Manuscripts.f_compiler4     as comp4
                    from Manuscripts
                    join Libraries
                        on Libraries.id = Manuscripts.f_place_library
                    join Places
                        on Places.id = Libraries.f_place
                    where Manuscripts.f_cat_lang = 'Arabic'
                    and Manuscripts.f_hidden = 'F'
                    '''):
                item = self.amend_cells(item, request)
                for key in ('comp1', 'comp2', 'comp3', 'comp4'):
                    if person_id := item[key]:
                        contributors[get_name(person_id, con)]['mss'].append({
                                'place': item['place_name'],
                                'lib': item['lib_abbrev'] or item['lib_name'],
                                'shelfmark': item['ms_shelfmark'],
                                })
            for item in con.getall(r'''
                        Works.id                as work_id,
                        Works.f_siglum          as work_siglum,
                        Works.f_standard_title  as work_title,
                        Works.f_standard_arabic as work_title_arabic,
                        Persons.f_person_name   as person_name,
                        Languages.id            as lang_id,
                        Languages.f_lang        as lang_name,
                        Works.f_compiler        as comp1,
                        Works.f_compiler2       as comp2,
                        Works.f_compiler3       as comp3,
                        Works.f_compiler4       as comp4
                    from Works
                    join Creators
                        on Creators.f_work = Works.id
                    join Persons
                        on Persons.id = Creators.f_creator
                    join Languages
                        on Languages.id = Works.f_lang
                    where Creators.f_creator_role = 1
                    and Works.f_hidden = 'F'
                    and Languages.id = 1
                    and Works.f_hidden = 'F'
                    '''):
                item = self.amend_cells(item, request)
                for key in ('comp1', 'comp2', 'comp3', 'comp4'):
                    if person_id := item[key]:
                        contributors[get_name(person_id, con)]['works'].append({
                                'siglum': item['work_siglum'],
                                'author': item['person_name'],
                                'title': item['work_title'],
                                })
        return contributors

    def get_dir_filenames(self, foldername: str) -> 'None|list[str]':
        dirpath = os.path.join(self.content_path, 'files', foldername)
        if os.path.isdir(dirpath):
            return sorted(os.listdir(dirpath))
        else:
            return None

    def get_filterfields_jordanus(self):
        '''
        Get the filterfields (hyperfields) for the filterform of Jordanus.
        '''
        with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
            cur.execute(r'''
                    select distinct
                        field_public,
                        field_hypernum,
                        field_hypername,
                        field_hypername_de
                    from fields
                    where field_public > 1
                    order by field_public DESC, field_rank ASC
                    ''')
            fieldgroups = [ list(group) for key, group in itertools.groupby(
                    cur.fetchall(),
                    lambda x: x['field_public'],
                    ) ]
            fieldgroups.append(None)
            return fieldgroups[:2]

    def get_forum_page(self, page_id: int) -> 'ty.Generator[dict[str, ty.Any]]':
        '''
        Get the posts of the forum page identified by :param:`page_id`.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            for post in con.getall(r"""
                    *, first_name, last_name
                    from forum_posts join auth_user
                        on auth_user.id = user_id
                    where page_id = %s order by post_id
                    """, page_id):
                post['von_'] = raafzeit_to_isodate(post['von'])
                post['bis_'] = raafzeit_to_isodate(post['bis'])
                yield post

    def get_forum_post(self, post_id: int) -> 'dict[str, ty.Any]':
        '''
        Get the post identified by :param:`post_id` or, if it does not exist,
        an empty dictionary.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            post = con.get(r"""
                    *, first_name, last_name
                    from forum_posts join auth_user
                        on auth_user.id = user_id
                    where post_id = %s
                    """, post_id)
            if post:
                post['von_'] = raafzeit_to_isodate(post['von'])
                post['bis_'] = raafzeit_to_isodate(post['bis'])
            return post

    def get_index(
            self,
            kind: str,
            request: 'bottle.LocalRequest',
            ) -> 'ty.Generator[dict[str, str]]':
        '''
        Yield all database entities of a certain :param:`kind`.
        The format is apt for a datatable.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            if kind in {'bib', 'bib_omnium'}:
                if kind == 'bib_omnium':
                    cur.execute(r"select * from bib")
                else:
                    cur.execute(r"""
                            select * from bib where bib_einordnung not in (
                                '', '???', 'AstlBibl', 'Catl', 'China', 'Ency',
                                'Lang', 'Math', 'no', 'Review')
                                """)
                for item in cur.fetchall():
                    siglum_quoted = self.q(item['bib_sigle'])
                    a = f'''<a id="{siglum_quoted}" class="key" '''\
                        f'''onclick="rowOver(event, '/api/bib/{siglum_quoted}/s', 8)">'''
                    autor = self.rename_bib(item['bib_autor'])
                    even_autor = parse.even(autor, low = False)
                    sortjahr, jahr = item['bib_jahr'].split('█', 1)
                    art = item['bib_art']
                    hg = self.rename_bib(item['bib_hg'])
                    titel = item['bib_titel']
                    untertitel = item['bib_untertitel']
                    bandtitel = item['bib_bandtitel']
                    zstitel = item['bib_zstitel']
                    titel = titel + ('. ' + untertitel if untertitel else '')
                    obertitel = '. '.join(filter(None, (
                            bandtitel,
                            zstitel,
                            )))
                    if art == 'thesis':
                        ort = ': '.join(filter(
                                None,
                                (item['bib_ort'], item['bib_institution'])))
                    else:
                        ort = ': '.join(filter(
                                None,
                                (item['bib_ort'], item['bib_verlag'])))
                    row = { str(rank): field for rank, field in enumerate((
                            {
                                '_': autor + '<div hidden="">█' +
                                     even_autor + '</div>',
                                's': even_autor,
                            },
                            {
                                '_': jahr,
                                's': sortjahr,
                            },
                            {
                                '_': a + titel + '</a>',
                                's': parse.sortxtext(titel),
                            },
                            {
                                '_': obertitel,
                                's': parse.sortxtext(obertitel),
                            },
                            hg,
                            ort,
                            item['bib_einordnung'],
                            )) }
                    num = len(row) - 1
                    if kind == 'bib_omnium':
                        num += 1
                        row[str(num)] = '; '.join(filter(None, (
                                item['bib_einordnung_klasse'],
                                item['bib_einordnung_personen'],
                                item['bib_einordnung_raum'],
                                item['bib_einordnung_schlagwörter'],
                                item['bib_einordnung_werke'],
                                )))
                    num += 1
                    row[str(num)] = art
                    yield row
            elif kind == 'forum':
                user_id, user_name, roles = self.auth(request)
                if roles:
                    for page_id, posts in itertools.groupby(
                            con.getall(r"""
                                    *, first_name, last_name
                                from forum_posts join auth_user
                                    on auth_user.id = user_id
                                order by page_id, post_id
                                """),
                            key = itemgetter('page_id')):
                        posts = tuple(posts)
                        titles = (
                                ' · '.join(
                                    '<a href="/forum/{}#{}">{}</a>'.format(
                                        page_id, post_id, title or '(s. t.)')
                                    for post_id, title in map(
                                        itemgetter('post_id', 'title'), posts)
                                    ))
                        name = (posts[0]['last_name'] + ', ' +
                                posts[0]['first_name'])
                        von = posts[0]['von']
                        bis = max(map(itemgetter('bis'), posts))
                        yield { str(rank): field for rank, field in enumerate((
                                {
                                    '_': raafzeit_to_isodate(von),
                                    's': von,
                                },
                                {
                                    '_': raafzeit_to_isodate(bis),
                                    's': bis,
                                },
                                {
                                    '_': titles,
                                    's': parse.sortxtext(titles),
                                },
                                {
                                    '_': name,
                                    's': parse.sortxtext(name),
                                },
                                )) }
            elif kind == 'glossary':
                cur.execute(SQL_GET_GLOSSES)
                for item in cur.fetchall():
                    concept_id = item['concept_id']
                    concept = item['concept'] or ''
                    concept_sorter = concept.lower()
                    concept = self.e(concept)
                    expression = item['expression'] or ''
                    expression_in_url = self.q(self.q(expression, safe = ''))
                    # has to be double-quoted to prevent bottleʼs router
                    # from misreading routes with ``'%2F'`` (slash) etc.
                    expression = self.e(expression)
                    expression_sorter = parse.even(expression)
                    expression = \
                            f'<a class="key" href="/glossary/{concept_id}/{expression_in_url}">{expression}</a>'\
                            f'<div hidden="">█{expression_sorter}</div>'
                    lang_abbr = item['lang_abbr'] or ''
                    if item['rtl']:
                        expression = f'<div lang="{lang_abbr}" dir="rtl">{expression}</div>'
                    else:
                        expression = f'<div lang="{lang_abbr}">{expression}</div>'
                    occurrences = self.e(
                            '; '.join(
                            sorted(
                            filter(
                                None,
                                (item['occurrences'] or '').split('; ')
                            ),
                            key = lambda x: parse.sortnum(x))))
                    yield { str(rank): field for rank, field in enumerate((
                            {
                                '_': concept,
                                's': concept_sorter,
                            },
                            {
                                '_': expression,
                                's': expression_sorter,
                            },
                            {
                                '_': item['lang_name'],
                                's': item['lang_sorter'],
                            },
                            self.e(item['translated_from'] or ''),
                            self.e(item['generic_work'] or ''),
                            self.e(item['short_title'] or ''),
                            occurrences,
                            )) }
            elif kind in {'manuscripts', 'manuscripts_arabic', 'manuscripts_latin'}:
                sql = """
                        select
                            Manuscripts.id            as ms_id,
                            Manuscripts.f_shelfmark   as ms_shelfmark,
                            IF(Manuscripts.f_cat_lang = 'Arabic', 1, 0) as lang_id,
                            Manuscripts.f_cat_lang    as lang_name,
                            Libraries.f_lib_name      as lib_name,
                            IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev) as lib_abbrev,
                            Places.f_placename        as place_name,
                            Works.id                  as work_id,
                            Works.f_siglum            as work_siglum,
                            Works.f_standard_title    as work_title,
                            Persons.f_person_name     as person_name,
                            repros.doc_type           as doc_type,
                            repros.doc_id             as doc_id,
                            repros.part_id            as part_id,
                            min(repros.fulltran_part) as fulltran_part,
                            substring_index(ifnull(min(repros.pagina_sorter), '/'), '/', -1) as pagina,
                            repros.scan_path          as scan_path
                        from Manuscripts
                        join Libraries
                            on Libraries.id = Manuscripts.f_place_library
                        join Places
                            on Places.id = Libraries.f_place
                        join MS_Parts
                            on MS_Parts.f_part_of_ms = Manuscripts.id
                        join Works
                            on Works.id = MS_Parts.f_part_of_work
                        join Creators
                            on Creators.f_work = Works.id
                        join Persons
                            on Persons.id = Creators.f_creator
                        left join repros
                            on repros.part_id = MS_Parts.id
                            and repros.doc_type = 'ms'
                        where Creators.f_creator_role = 1
                        and Manuscripts.f_hidden = 'F'
                        and MS_Parts.f_hidden = 'F'
                        and Works.f_hidden = 'F'
                        """
                sql += """
                        and Manuscripts.f_cat_lang = 'Arabic'
                        """ if kind == 'manuscripts_arabic' else """
                        and Manuscripts.f_cat_lang = 'Latin'
                        """ if kind == 'manuscripts_latin' else ''
                sql += """
                        group by -- for min (of repros) and pre-sorting
                            Libraries.f_lib_name,
                            Manuscripts.f_shelfmark,
                            MS_Parts.id
                        """
                cur.execute(sql)
                for item in cur.fetchall():
                    place = item['place_name']
                    if place == '?':
                        place = '―'
                    place += ', {}<span class="trim">, {}</span>'.format(
                            item['lib_abbrev'] or item['lib_name'],
                            item['ms_shelfmark']
                            )
                    place_sorter = parse.sortxtext(place)
                    place = f"<a href=\"/ms/{item['ms_id']}\">{place}</a>"
                    scan_link, tran_link = get_repro_links(item)
                    if kind in {'manuscripts_arabic', 'manuscripts_latin'}:
                        yield { str(rank): field for rank, field in enumerate((
                                {
                                    '_': place,
                                    's': place_sorter
                                },
                                {
                                    '_': item['work_siglum'],
                                    's': parse.sortnumtext(item['work_siglum']),
                                },
                                get_person_name(item),
                                get_title(item),
                                scan_link,
                                tran_link,
                                )) }
                    else:
                        yield { str(rank): field for rank, field in enumerate((
                                {
                                    '_': place,
                                    's': place_sorter
                                },
                                {
                                    '_': item['work_siglum'],
                                    's': parse.sortnumtext(item['work_siglum']),
                                },
                                get_person_name(item),
                                get_title(item),
                                scan_link,
                                tran_link,
                                item['lang_name'],
                                )) }
            elif kind in {'prints', 'prints_latin'}:
                sql = """
                        select
                            Editions.id            as print_id,
                            CAST(Editions.f_date_num_exact AS CHAR) as print_date,
                            Editions.f_publisher   as print_publisher,
                            Editions.f_title       as print_title,
                            Places.f_placename     as place_name,
                            Works.id               as work_id,
                            Works.f_siglum         as work_siglum,
                            Works.f_standard_title as work_title,
                            Persons.f_person_name  as person_name,
                            Languages.id           as lang_id,
                            Languages.f_lang       as lang_name,
                            repros.doc_type        as doc_type,
                            repros.doc_id          as doc_id,
                            repros.part_id         as part_id,
                            min(repros.fulltran_part) as fulltran_part,
                            substring_index(ifnull(min(repros.pagina_sorter), '/'), '/', -1) as pagina,
                            repros.scan_path       as scan_path
                        from Editions
                        join Places
                            on Places.id = Editions.f_place_compiled
                        join Ed_Parts
                            on Ed_Parts.f_part_of_edition = Editions.id
                        join Works
                            on Works.id = Ed_Parts.f_part_of_work
                        join Creators
                            on Creators.f_work = Works.id
                        join Persons
                            on Persons.id = Creators.f_creator
                        join Languages
                            on Languages.id = Works.f_lang
                        left join repros
                            on repros.part_id = Ed_Parts.id
                            and repros.doc_type = 'print'
                        where Creators.f_creator_role = 1
                        and Editions.f_hidden = 'F'
                        and Ed_Parts.f_hidden = 'F'
                        and Works.f_hidden = 'F'
                        group by -- for min (of repros) and pre-sorting
                            Editions.f_date_num_exact,
                            Places.f_placename,
                            Editions.f_publisher,
                            Ed_Parts.id
                        """
                cur.execute(sql)
                for item in cur.fetchall():
                    print_title = item['print_title']
                    scan_link, tran_link = get_repro_links(item)
                    yield { str(rank): field for rank, field in enumerate((
                            {
                                '_': '<a href="/print/{}">{}, {}, {}</a>'.format(
                                    item['print_id'],
                                    item['place_name'],
                                    item['print_publisher'],
                                    item['print_date'],
                                    ),
                                's': parse.sortxtext(' '.join((
                                    item['print_date'],
                                    item['place_name'],
                                    item['print_publisher'],
                                    )))
                            },
                            {
                                '_': item['work_siglum'],
                                's': parse.sortnumtext(item['work_siglum']),
                            },
                            get_person_name(item),
                            get_title(item),
                            scan_link,
                            tran_link,
                            item['lang_name'],
                            )) }
            elif kind == 'scans':
                cur.execute("""
                        select
                            doc_type,
                            doc_id,
                            work_id,
                            part_id,
                            min(repros.fulltran_part) as fulltran_part,
                            substring_index(ifnull(min(repros.pagina_sorter), '/'), '/', -1) as pagina,
                            repros.scan_path as scan_path
                        from repros where scan_path <> ''
                        group by doc_type, part_id, doc_id
                        """)
                repros = cur.fetchall()
                for repro in repros:
                    doc_type = repro['doc_type']
                    doc_id = repro['doc_id']
                    cur.execute("set @work_id = %s", (repro['work_id'],))
                    cur.execute(SQL_GET_WORK)
                    work = cur.fetchone()
                    location = get_location(doc_type, doc_id, con, cur)
                    if work and location:
                        scan_link, tran_link = get_repro_links(repro)
                        yield {
                                str(rank): field for rank, field in enumerate((
                                    {
                                        '_': work['work_siglum'],
                                        's':
                                        parse.sortnumtext(work['work_siglum']),
                                    },
                                    get_person_name(work),
                                    get_title(work),
                                    {
                                        '_': '<a href="/{}/{}">{}</a>'\
                                            .format(doc_type, doc_id, location),
                                        's': parse.sortxtext(location),
                                    },
                                    scan_link,
                                    tran_link,
                                    work['lang_name'],
                                    )) }
            elif kind == 'texts':
                cur.execute("""
                        select
                            doc_type,
                            doc_id,
                            work_id,
                            part_id,
                            min(repros.fulltran_part) as fulltran_part,
                            substring_index(ifnull(min(repros.pagina_sorter), '/'), '/', -1) as pagina,
                            repros.scan_path as scan_path
                        from repros where fulltran_part is not NULL
                        group by doc_type, part_id, doc_id
                        """)
                repros = cur.fetchall()
                for repro in repros:
                    doc_type = repro['doc_type']
                    doc_id = repro['doc_id']
                    part_id = repro['part_id']
                    fulltran_part = repro['fulltran_part']
                    cur.execute("set @work_id = %s", (repro['work_id'],))
                    cur.execute(SQL_GET_WORK)
                    work = cur.fetchone()
                    location = '' if doc_type == 'edition' else\
                               get_location(doc_type, doc_id, con, cur)
                    if work:
                        scan_link, tran_link = get_repro_links(repro)
                        yield { str(rank): field for rank, field in enumerate((
                                {
                                    '_': work['work_siglum'],
                                    's': parse.sortnumtext(work['work_siglum']),
                                },
                                get_person_name(work),
                                get_title(work),
                                {
                                    '_': '<a href="/{}/{}">{}</a>'\
                                        .format(doc_type, doc_id, location),
                                    's': parse.sortxtext(location),
                                } if location else {'_': '', 's': ''},
                                scan_link,
                                tran_link,
                                work['lang_name'],
                                )) }
            elif kind in {'works', 'works_arabic', 'works_latin'}:
                sql = """
                        select
                            Works.id                  as work_id,
                            Works.f_siglum            as work_siglum,
                            Works.f_sourcetype        as sourcetype,
                            Works.f_standard_title    as work_title,
                            Works.f_altwork           as alt_id,
                            Works.f_altimagecol       as alt_imagecol,
                            altworks.f_siglum         as alt_siglum,
                            Persons.f_person_name     as person_name,
                            Languages.id              as lang_id,
                            Languages.f_lang          as lang_name,
                            repros.doc_type           as doc_type,
                            repros.doc_id             as doc_id,
                            repros.part_id            as part_id,
                            min(repros.fulltran_part) as fulltran_part,
                            substring_index(ifnull(min(repros.pagina_sorter), '/'), '/', -1) as pagina,
                            repros.scan_path          as scan_path
                        from Works
                        join Creators
                            on Creators.f_work = Works.id
                        join Persons
                            on Persons.id = Creators.f_creator
                        join Languages
                            on Languages.id = Works.f_lang
                        left join repros
                            on repros.work_id = Works.id
                        left join Works as altworks
                            on altworks.id = Works.f_altwork
                        where Creators.f_creator_role = 1
                        """
                sql += """
                        and (Works.f_hidden = 'F' or Works.f_code like 'H%')
                        """ if request.roles else """
                        and Works.f_hidden = 'F'
                        """
                sql += """
                        and Languages.id in (2, 1)
                        """ if kind == 'works_arabic' else """
                        and Languages.id in (2, 3)
                        """ if kind == 'works_latin' else ''
                sql += """
                        group by
                            Works.id,
                            Works.f_siglum,
                            Works.f_standard_title,
                            Persons.f_person_name,
                            Languages.id,
                            Languages.f_lang,
                            repros.doc_type,
                            repros.part_id
                        """
                cur.execute(sql)
                for _, works in itertools.groupby(
                        sorted(
                            cur.fetchall(),
                            key = itemgetter('work_id', 'scan_path')),
                        key = itemgetter('work_id')):
                    scan_links = []
                    tran_links = []
                    for work in works:
                        scan_link, tran_link = get_repro_links(work)
                        scan_links.append(scan_link)
                        tran_links.append(tran_link)
                    scan_link = ' '.join(scan_links)
                    tran_link = ' '.join(tran_links)
                    if kind == 'works_arabic' and work['work_id'] in {
                            24, 83, 242, 251, 289}:
                        continue # Not a generic work for Arabic.
                    elif kind == 'works_latin' and work['work_id'] in {
                            331, 333}:
                        continue # Not a generic work for Latin.
                    is_heading = work['sourcetype'] == 5
                    if is_heading:
                        scan_link = tran_link = ''
                    elif work['alt_imagecol'] and not scan_link:
                        scan_link = work['alt_imagecol']
                    elif work['alt_id'] and not scan_link:
                        scan_link = (
                                '<a href="/work/{}">See {}</a>'
                                .format(work['alt_id'], work['alt_siglum']))
                    lang = work['lang_name']
                    if kind in {'works_arabic', 'works_latin'}:
                        siglum = {
                                '_': work['work_siglum'],
                                's': parse.sortnumtext(work['work_siglum']),
                                }
                        if is_heading and kind == 'works_latin':
                            siglum['_'] = ''
                        item = { str(rank): field for rank, field in enumerate((
                                    siglum,
                                    get_person_name(work),
                                    get_title(
                                        work, False if is_heading else True),
                                    scan_link,
                                    tran_link,
                                    )) }
                    else:
                        item = { str(rank): field for rank, field in
                                enumerate((
                                    {
                                        '_': work['work_siglum'],
                                        's':
                                        parse.sortnumtext(work['work_siglum']),
                                    },
                                    get_person_name(work),
                                    get_title(
                                        work, False if is_heading else True),
                                    scan_link,
                                    tran_link,
                                    lang,
                                    )) }
                    if lang == 'Greek':
                        item['DT_RowClass'] = 'greyed'
                    elif is_heading:
                        item['DT_RowClass'] = 'headingrow'
                    yield item
            else:
                raise Exception(f'Unknown :param:`kind` of index: {kind}')

    def get_index_jordanus(
            self,
            kind: str,
            pagina_re: 're.Pattern[str]' = re.compile(r'^\s*\(.*?\)\s*'),
            ) -> 'ty.Generator[dict[str, str]]':
        '''
        Yield all database entities of a certain :param:`kind` from Jordanus.
        The format is apt for a datatable.
        '''
        if kind == 'mss':
            with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
                for item in con.geteach('ms_id, location from temp'):
                    ms_id = item['ms_id']
                    term = item['location']
                    even_term = parse.even(term, low = False)
                    if ms_id and term:
                        yield { '0': {
                                '_': '<a href="/jordanus/ms/{0}">{1}'\
                                     '</a><div hidden="">█{2}</div>'.format(
                                         ms_id,
                                         self.e(term),
                                         even_term),
                                's': even_term,
                                }}
        else:
            fields, hypernum = {
                    'authors': ((57, 58), 2),
                    'titles': ((9, 10, 11), 3),
                    'incipits': ((65, 66), 5),
                    }.get(kind, ((), 0))
            if not fields:
                return
            with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
                cur.execute('''
                        select distinct term
                        from sentences
                        join terms on terms.term_id = sentences.term_id
                        where sentences.field_id in {}
                        '''.format(fields))
                for item in cur.fetchall():
                    term = item['term']
                    if kind == 'incipits':
                        even_term = parse.even(
                                term,
                                low = False,
                                acts = (partial(pagina_re.sub, ''),),
                                )
                    else:
                        even_term = parse.even(term, low = False)
                    yield {
                            '0': {
                                '_': f'<a href="/jordanus/filter?{hypernum}={self.q(term)}&exact=1">'\
                                     f'{self.e(term)}</a><div hidden="">█{even_term}</div>',
                                's': even_term,
                            }}

    def get_item_and_template_name(
            self,
            doc_type: str,
            doc_id: int,
            request: 'bottle.LocalRequest',
            ) -> 'tuple[dict[str, str|int], str]':
        '''
        Get the item with :param:`doc_type` (e.g. ``'ms'``) and :param:`doc_id`
        (e.g. ``'12'``) as well as the name of the corresponding template (e.g.
        ``'ms.tpl'``). The item is a dictionary describing e.g. the manuscript
        with the id 12.
        '''
        tpl = ''
        item = {}
        with sql_io.DBCon(self.connect()) as (con, cur):
            if doc_type == 'print':
                tpl = 'print.tpl'
                cur.execute("set @doc_id = %s", (doc_id,))
                cur.execute(SQL_GET_PRINT)
                item = cur.fetchone() or {}
                if item:
                    self.amend_cells(item, request)
                    rows = sorted((
                            (
                                row['print_id'],
                                row['place_name'],
                                row['print_publisher'],
                                row['print_date'],
                            ) for row in con.geteach(r"""
                                Editions.id          as print_id,
                                CAST(Editions.f_date_num_exact as CHAR)
                                                     as print_date,
                                Editions.f_publisher as print_publisher,
                                Places.f_placename   as place_name
                                from Editions
                                join Places
                                    on Places.id = Editions.f_place_compiled
                                and Editions.f_hidden = 'F'
                                """)
                            ), key = lambda row: parse.sortxtext(
                                ' '.join((row[3], row[1], row[2]))))
                    (item['prev'], item['next']) = get_prev_next(doc_id, rows)
                    cur.execute(SQL_GET_PRINTPARTS_OF_PRINT, doc_id)
                    item['printparts'] = tuple(
                            self.amend_cells(row, request, doc_type, doc_id)
                            for row in cur.fetchall() )
                    item['print_modified_on'] = sorted(
                            [item['print_modified_on']] +
                            [ e['printpart_modified_on']
                                for e in item['printparts'] ]
                            )[-1]
            elif doc_type == 'ms':
                tpl = 'ms.tpl'
                cur.execute("set @doc_id = %s", (doc_id,))
                cur.execute(SQL_GET_MS)
                item = cur.fetchone() or {}
                if item:
                    self.amend_cells(item, request)
                    rows = sorted((
                            (
                                row['ms_id'],
                                row['place_name'],
                                row['lib_abbrev'] or row['lib_name'],
                                row['ms_shelfmark'],
                            ) for row in con.geteach(r"""
                                Manuscripts.id          as ms_id,
                                Manuscripts.f_shelfmark as ms_shelfmark,
                                Libraries.f_lib_name    as lib_name,
                                IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev)
                                                        as lib_abbrev,
                                Places.f_placename      as place_name
                                from Manuscripts
                                join Libraries
                                    on Libraries.id =
                                        Manuscripts.f_place_library
                                join Places
                                    on Places.id = Libraries.f_place
                                where Manuscripts.f_cat_lang = %s
                                and Manuscripts.f_hidden = 'F'
                                """, item['lang_name'])
                            ), key = lambda row: parse.sortxtext(
                                ' '.join((row[1], row[2], row[3]))))
                    (item['prev'], item['next']) = get_prev_next(doc_id, rows)
                    cur.execute(r"""
                            select
                                MS_Parts.id            as part_id,
                                MS_Parts.f_range       as pagina_range,
                                MS_Parts.f_note        as mspart_note,
                                MS_Parts.f_quotation   as mspart_quotation,
                                MS_Parts.modified_on   as mspart_modified_on,
                                Works.id               as work_id,
                                Works.f_siglum         as work_siglum,
                                Works.f_standard_title as work_title,
                                Persons.f_person_name  as person_name
                            from MS_Parts
                            join Manuscripts
                                on Manuscripts.id = MS_Parts.f_part_of_ms
                            join Works
                                on Works.id = MS_Parts.f_part_of_work
                            join Creators
                                on Creators.f_work = Works.id
                            join Persons
                                on Persons.id = Creators.f_creator
                            where Creators.f_creator_role = 1
                            and Manuscripts.id = %s
                            and MS_Parts.f_hidden = 'F'
                            and Works.f_hidden = 'F'
                            order by MS_Parts.f_order_no
                            """, doc_id)
                    item['msparts'] = tuple(
                            self.amend_cells(row, request, doc_type, doc_id)
                            for row in cur.fetchall() )
                    item['ms_modified_on'] = sorted(
                            [item['ms_modified_on']] +
                            [ e['mspart_modified_on'] for e in item['msparts'] ]
                            )[-1]
            elif doc_type == 'work':
                tpl = 'work.tpl'
                cur.execute("set @work_id = %s", (doc_id,))
                sql = SQL_GET_WORK
                if request.roles:
                    sql = sql.replace(
                            "Works.f_hidden = 'F'",
                            "(Works.f_hidden = 'F' or Works.f_code like 'H%')")
                cur.execute(sql)
                item = cur.fetchone() or {}
                if item:
                    self.amend_cells(item, request)
                    rows = sorted((
                            (
                                row['id'],
                                row['f_siglum'],
                                row['f_standard_title'],
                            ) for row in con.geteach(r"""
                                id, f_siglum, f_standard_title from Works
                                where f_lang = %s and f_hidden = 'F'
                                """, item['lang_id'])
                            ), key = lambda row: parse.sortnumtext(row[1]))
                    (item['prev'], item['next']) = get_prev_next(doc_id, rows)
                    cur.execute(SQL_GET_PRINTPARTS_OF_WORK, doc_id)
                    printparts = [
                            self.amend_cells(
                                    row, request, 'print', row['print_id'],
                                    ) for row in cur.fetchall() ]
                    printparts.sort(key = lambda e: (
                            e['print_date'],
                            parse.sortxtext(e['place_name']),
                            parse.sortxtext(e['print_publisher']),
                            get_pagina_sorter(e['pagina_range']),
                            ))
                    item['printparts'] = printparts
                    cur.execute(r"""
                            select
                                MS_Parts.id                as part_id,
                                MS_Parts.f_range           as pagina_range,
                                MS_Parts.f_note            as mspart_note,
                                MS_Parts.f_quotation       as mspart_quotation,
                                REPLACE(REPLACE(MS_Parts.f_date_folio_info, '<p>', ''), '</p>', '') as mspart_info,
                                MS_Parts.modified_on       as mspart_modified_on,
                                Manuscripts.id             as ms_id,
                                Manuscripts.f_hidden       as ms_hidden,
                                Manuscripts.f_shelfmark    as ms_shelfmark,
                                IF(Manuscripts.f_short = '', 0, 1) as ms_has_short,
                                Works.id                   as work_id,
                                Libraries.f_lib_name       as lib_name,
                                IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev) as lib_abbrev,
                                Places.f_placename         as place_name
                            from MS_Parts
                            join Manuscripts
                                on Manuscripts.id = MS_Parts.f_part_of_ms
                            join Works
                                on Works.id = MS_Parts.f_part_of_work
                            join Creators
                                on Creators.f_work = Works.id
                            join Persons
                                on Persons.id = Creators.f_creator
                            join Libraries
                                on Libraries.id = Manuscripts.f_place_library
                            join Places
                                on Places.id = Libraries.f_place
                            where Creators.f_creator_role = 1
                            and Works.id = %s
                            and MS_Parts.f_hidden = 'F'
                            order by
                                Places.f_placename,
                                IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev),
                                Manuscripts.f_shelfmark,
                                MS_Parts.f_range
                            """, doc_id)
                    msparts = [
                            self.amend_cells(
                                    row, request, 'ms', row['ms_id'],
                                    ) for row in cur.fetchall() ]
                    msparts.sort(key = lambda e: (
                            parse.sortxtext(e['place_name']),
                            parse.sortxtext(e['lib_abbrev'] or e['lib_name']),
                            parse.sortxtext(e['ms_shelfmark']),
                            get_pagina_sorter(e['pagina_range']),
                            ))
                    item['msparts'] = msparts
                    for key, sql in (
                            ('translations', SQL_GET_TRANSLATIONS_OF_WORK),
                            ('commentaries', SQL_GET_COMMENTARIES_OF_WORK),
                            ):
                        cur.execute(sql, doc_id)
                        rows_per_lang = defaultdict(list)
                        for row in cur.fetchall():
                            rows_per_lang[row['lang_id']].append(
                                    self.amend_cells(row, request))
                        item[key] = rows_per_lang
        return item, tpl

    def get_jobs(self, job_id: str = '') -> 'list[dict[str, str]]':
        '''
        If :param:`job_id` is empty, get all current and former vacancies.
        Else, get the vacancy with the id :param:`job_id`.
        In any case, return a list (consisting of one or more items).
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            if job_id:
                cur.execute(SQL_GET_JOB, job_id)
            else:
                cur.execute(SQL_GET_JOBS)
            return [ self.amend_cells(item) for item in cur.fetchall() ]

    def get_jobs_information(self) -> 'tuple[str, list[dict[str, str]]]':
        '''
        Get a description of the current job positions in general and the
        description of any open jobs.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(SQL_GET_JOBS_DESCRIPTION)
            general = self.amend_cells(
                    cur.fetchone() or {'': ''}).popitem()[1]
            cur.execute(SQL_GET_JOB_VACANCIES)
            vacancies = [
                    self.amend_cells(item) for item in cur.fetchall() ]
            return general, vacancies

    def get_langs_sorted(self) -> 'list[dict[str, str]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(r"""
                    select
                        id     as lang_id,
                        f_lang as lang_name,
                        f_lang_sorter as lang_sorter
                    from Languages
                    where is_active = 'T'
                    order by f_lang_sorter
                    """)
            return cur.fetchall()

    def get_lemma(
            self,
            concept_id: 'None|int',
            expression: 'None|str',
            item_id: 'None|int' = None,
            with_transl_froms: bool = False,
            with_transl_intos: bool = False,
            with_synonyms: bool = False,
            ) -> 'dict[str, str|dict[str, str]]':
        '''
        From the glossary, get information related both to the concept
        :param:`concept_id` and to the expression :param:`expression`.
        '''
        lemma = {}
        with sql_io.DBCon(self.connect()) as (con, cur):
            if item_id is None:
                cur.execute((SQL_GET_TERMS + """
                        and Gloss_Terms.concept = %s
                        and Gloss_Terms.term = %s
                        """), (concept_id, expression))
                items = cur.fetchall()
                if not items:
                    return {}
            else:
                cur.execute(
                        (SQL_GET_TERMS + "and Gloss_Terms.id = %s"), (item_id,))
                item = cur.fetchone()
                if item is None:
                    return {}
                concept_id = item['concept_id']
                expression = item['expression']
                items = [item]
            lemma['expression'] = expression
            lemma['concept_id'] = concept_id
            lemma['concept'] = items[0]['concept']
            lemma['lang_id'] = items[0]['lang_id']
            lemma['lang_name'] = items[0]['lang_name']
            lemma['lang_abbr'] = items[0]['lang_abbr']
            lemma['lang_sorter'] = items[0]['lang_sorter']
            lemma['rtl'] = items[0]['rtl']
            lemma['grammar'] = '. – '.join(sorted(set(filter(None,
                    ( (item['grammar'] or '').strip() for item in items )))))
            lemma['refs'] = self.get_refs(cur, items)
            if with_transl_froms:
                idents = set(filter(None,
                        ( item['from_id'] for item in items )))
                lemma['transl_froms'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents )),
                        key = lambda x: x['lang_sorter'])
            if with_transl_intos:
                idents = set()
                idents_modern = set()
                for item in items:
                    cur.execute("""
                            select id, is_modern from Gloss_Terms
                            where translated_from = %s
                            """, (item['id'],))
                    for item in cur.fetchall():
                        ident = item['id']
                        if ident is not None:
                            if item['is_modern'] != 1:
                                idents.add(ident)
                            else:
                                idents_modern.add(ident)
                lemma['transl_intos'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents )),
                        key = lambda x: x.get('lang_sorter', 0))
                lemma['transl_intos_modern'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents_modern )),
                        key = lambda x: x.get('lang_sorter', 0))
            if with_synonyms:
                cur.execute("""
                        select id from Gloss_Terms
                        where Gloss_Terms.concept = %s
                        and Gloss_Terms.term <> %s
                        and Gloss_Terms.lang = %s
                        order by term
                        """, (concept_id, expression, lemma['lang_id']))
                idents = { item['id'] for item in cur.fetchall() }
                lemma['synonyms'] = sorted(filter(None,
                        ( self.get_lemma(None, None, ident)
                          for ident in idents )),
                        key = lambda x: x.get('expression', ''))
            return lemma

    def get_ms_jordanus(self, ms_id) -> 'dict':
        '''
        Find all parts and pieces of information belonging to :param`ms_id`
        and return them ordered in a main part and ms parts.
        '''
        with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
            cur.execute(r'''
                    select
                        subsiglum,
                        field_rank,
                        field_name,
                        field_hypernum,
                        term,
                        sentences.sentence_id as sentence_id,
                        sentences.field_id    as field_id
                    from sentences
                    join terms on
                        terms.term_id = sentences.term_id
                    join fields on
                        fields.field_id = sentences.field_id
                    where
                        sentences.ms_id = %s
                        and
                        fields.field_public > 0
                    order by
                        subsiglum,
                        field_rank,
                        field_name,
                        term
                    ''', ms_id)
            item = {}
            for subsiglum, fields in itertools.groupby(
                    cur.fetchall(),
                    itemgetter('subsiglum'),
                    ):
                item[subsiglum] = {}
                for field in fields:
                    key = field['field_id']
                    # There may be multiple values for one field and one mspart.
                    # The sentence_id, by the way, is unique.
                    if key not in item[subsiglum]:
                        item[subsiglum][key] = [
                                field['field_name'],
                                field['field_hypernum'],
                                {}]
                    item[subsiglum][key][2][field['sentence_id']] =\
                            field['term']
            return item

    def get_news(
            self,
            news_id: int = 0,
            no_space_before_p_or_h: 're.Pattern[str]' =
                re.compile(r'(\S)(<(?:p|h\d)[>\s])'),
            teaser_re: 're.Pattern[str]' =
                re.compile(r'(&[^;]*;|<[^>]*>|.){,500}'),
            ) -> 'list[dict[str, str]]':
        '''
        Get a list of all news items or the news item with :param:`news_id`.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            if news_id:
                cur.execute(SQL_GET_NEWS_ITEM, news_id)
            else:
                cur.execute(SQL_GET_NEWS)
            news = []
            for item in cur.fetchall():
                headline = item['news_headline'].lstrip()
                if headline.startswith('*'):
                    item['news_headline'] = headline[1:]
                teaser = xmlhtml.replace(item['news_content'])
                teaser = no_space_before_p_or_h.sub(r'\g<1> \g<2>', teaser)
                new = teaser_re.match(teaser).group()
                if teaser != new:
                    teaser = new + '…'
                item['news_teaser'] = teaser
                news.append(self.amend_cells(item))
        return news

    def get_num(self, kind, lang_name: str = '') -> int:
        '''
        Get the number of db items of the type :param:`kind`.

        :param lang_name: If not the empty string, only items of the
            specified language are included.
        '''
        kind_sql = {
                'ms': 'Manuscripts',
                'print': 'Editions',
                }.get(kind, '')
        if not kind:
            return 0
        sql = """
                select count(1) as num from `{}` where f_hidden = 'F'
                """.format(kind_sql)
        if kind == 'ms' and lang_name in {'Greek', 'Arabic', 'Latin'}:
            sql += f"""
                    and Manuscripts.f_cat_lang = '{lang_name}'
                    """
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(sql)
            return cur.fetchone().get('num', 0)

    def get_project(self) -> 'dict[str, str]':
        '''Get an introduction, description, overview of the project.'''
        with sql_io.DBCon(self.connect()) as (con, cur):
            row = con.get(r"* from t_project")
        return self.amend_cells(row)

    def get_refs(self, cur, items) -> str:
        '''
        With :param:`cur` in the pertinent database, get all references
        assigned to :param:`items`. The items are term-concept-translation
        items. Combine the references to a string representation.
        '''
        cur.execute("""
                select
                    Gloss_Termrefs.location as token_location,
                    Gloss_Refs.type         as ref_type,
                    Gloss_Refs.author       as ref_author,
                    Gloss_Refs.title        as ref_title,
                    Gloss_Refs.year         as ref_year,
                    Gloss_Refs.generic_work as ref_generic_work,
                    Gloss_Refs.short_title  as ref_short_title
                from Gloss_Termrefs
                join Gloss_Refs
                    on Gloss_Refs.id = Gloss_Termrefs.refid
                where
                    Gloss_Termrefs.termid in ({})
                order by Gloss_Refs.short_title, Gloss_Termrefs.location
                """.format(', '.join( str(item['id']) for item in items )))
        refs = defaultdict(list)
        for ref in cur.fetchall():
            ref_short_title = ref['ref_short_title']
            work, edition = (
                    ref_short_title.rsplit(' ', 1) if ' ' in ref_short_title
                    else ('', ref_short_title))
            refs[ref['ref_short_title']].append(ref['token_location'])
        return '. – '.join(sorted(
                title + ': ' + '; '.join(
                    sorted(location, key = lambda x: parse.sortnum(x)))
                for title, location in refs.items() ))

    def get_repro_item(
            self,
            doc_type: str,
            doc_id: int,
            part_id: str,
            pagina: str,
            ) -> 'dict[str, str|bytes|int]':
        '''
        For the given type, IDs and pagina, get the information needed for
        the page which shows the corresponding facsimile or transcription
        (or both). Get this information from the temporary table.
        '''
        item = {}
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute("set @doc_id = %s", (doc_id,))
            if doc_type == 'ms':
                cur.execute(SQL_GET_MS)
            else:
                cur.execute(SQL_GET_PRINT)
            catalog_doc = cur.fetchone()
            cur.execute("""
                    select doc_type, doc_id, work_id, part_id,
                        substring_index(pagina_sorter, '/', -1) as pagina,
                        pagina_styled, fulltran_part, scan_path, tran_path,
                        scan_caption, scan_uri
                    from repros
                    where doc_type = %s and part_id = %s
                    order by pagina_sorter collate utf8mb4_bin
                    """, (doc_type, part_id))
            repros = cur.fetchall()
            if not repros:
                return {}
            cur.execute("set @work_id = %s", (repros[0]['work_id'],))
            cur.execute(SQL_GET_WORK)
            work = cur.fetchone()
            if not (catalog_doc and work):
                return {}
        for rank, repro in enumerate(repros):
            if pagina == repro['pagina']:
                break
        else:
            return {}
        repro = repros[rank]
        item['scan_caption'] = repro['scan_caption']
        item['scan_uri'] = repro['scan_uri']
        scan_path = repro['scan_path']
        if item['scan_uri']:
            item['scan_path'] = item['min_scan_path'] = item['scan_uri']
        elif scan_path:
            item['scan_path'] = f'/repros/{self.q(doc_type)}/{self.q(scan_path)}'
            dirpath, name = os.path.split(scan_path)
            min_scan_path = f'{dirpath}/min/{name}.jpg'
            min_scan_path = min_scan_path if os.path.exists(
                    os.path.join(self.repros_path, doc_type, min_scan_path)
                    ) else scan_path
            item['min_scan_path'] = f'/repros/{self.q(doc_type)}/{self.q(min_scan_path)}'
        else:
            item['scan_path'] = item['min_scan_path'] = ''
        tran = b''
        if repro['tran_path']:
            tran_path = '/'.join(
                    (self.repros_path, doc_type, repro['tran_path']))
            try:
                with open(tran_path, 'rb') as file:
                    tran = file.read()
            except OSError:
                pass # Sic.
        item['tran'] = tran
        item['fulltran_part'] = repro['fulltran_part']
        item['lang_name'] = catalog_doc.get('lang_name', '')
        if doc_type == 'ms':
            place_name = catalog_doc['place_name']
            item['place_name'] = ('―' if place_name == '?' else
                    catalog_doc['place_name'])
            item['lib_name'] = (
                    catalog_doc['lib_abbrev'] or
                    catalog_doc['lib_name'])
            item['ms_shelfmark'] = catalog_doc['ms_shelfmark']
        else:
            item['place_name'] = catalog_doc['place_name']
            item['print_publisher'] = catalog_doc['print_publisher']
            item['print_date'] = catalog_doc['print_date']
        item['work'] = work
        item['doc_type'] = doc_type
        item['doc_id'] = doc_id
        item['part_id'] = part_id
        item['pagina_styled'] = repro['pagina_styled']
        item['paginas'] = [
                (repro['pagina'], repro['pagina_styled']) for repro in repros ]
        item['prev'] = repros[rank - 1]['pagina'] if rank > 0 else ''
        item['next'] = repros[rank + 1]['pagina'] if (rank + 1) < len(repros)\
                else ''
        return item

    def get_team(self) -> 'list[dict[str, str|list[dict[str, str]]]]':
        '''
        Get the description of the team as a list of groups,
        which are role categories, and sublists of group members.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(SQL_GET_GROUPS)
            groups = cur.fetchall() or []
            for i, group in enumerate(groups):
                cur.execute(SQL_GET_USERS, group['group_id'])
                groups[i]['users'] = cur.fetchall() or []
        return groups

    def get_tran(
            self,
            doc_type: str,
            doc_id: int,
            text_id: int,
            page_id: str,
            ) -> bytes:
        '''
        Get the page :param:`page_id` of the full transcription of the
        document part or work :param:`part_id` of the print or manuscript or
        edition (decided by :param:`doc_type`) with :param:`doc_id`.
        '''
        if doc_type == 'edition':
            text_id_case = 'work_id'
            upward = '..'
        else:
            text_id_case = 'part_id'
            upward = '../..'
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(rf"""
                    select tran_path, fulltran_updated
                    from repros
                    where doc_type = %s
                    and {text_id_case} = %s
                    and tran_path <> ''
                    limit 1
                    """, (doc_type, text_id))
            item = cur.fetchone()
        if not item:
            return b''
        path = os.path.abspath(os.path.join(
                self.repros_path,
                doc_type,
                item['tran_path'],
                upward,
                f'{page_id}.xml',
                ))
        try:
            with open(path, 'rb') as file:
                bits = file.read()
        except OSError:
            return b''
        return bits[3:] if bits[:3] == b'\xef\xbb\xbf' else bits

    def get_tran_data(
            self,
            doc_type: str,
            text_id: int,
            ) -> 'ty.Generator[dict[str, str|bytes|int]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            if doc_type == 'edition':
                text_id_col = 'doc_id'
                sub_id_col = 'work_id'
            else:
                text_id_col = 'part_id'
                sub_id_col = 'part_id'
            for item in con.geteach(rf"""
                    * from repros
                    where doc_type = %s and {text_id_col} = %s
                    and fulltran_part IS NOT NULL
                    order by fulltran_part, pagina_sorter
                    """, doc_type, text_id):
                item['sub_id'] = item[sub_id_col]
                yield item

    def get_user(self, user_id: str) -> 'dict[str, str]':
        '''Get the team member :param:`user_id`.'''
        with sql_io.DBCon(self.connect()) as (con, cur):
            cur.execute(SQL_GET_USER, user_id)
            return self.amend_cells(cur.fetchone() or {})

    @staticmethod
    def invert_italics(
            term: str,
            italics_re = re.compile(rf'(?x)<i>{parse.XTERM}</i>'),
            ) -> str:
        '''
        Invert the italics markup in :param:`term` so that an italicized part
        becomes recte and the reverse.
        '''
        parts = []
        i = 0
        for m in italics_re.finditer(term):
            parts.append(term[i:m.start()])
            parts.append(term[m.start() + 3:m.end() - 4])
            i = m.end()
        parts.append(term[i:])
        for i in range(0, len(parts), 2):
            if parts[i]:
                parts[i] = '<i>{}</i>'.format(
                        parts[i].replace('<i>', '').replace('</i>', ''))
        term = ''.join(parts)
        return term

    def make_temptable_jordanus(self):
        '''
        Make a temporary table for ms metadata in jordanus in order to
        hold these data available precomputed. They are to be shown in
        results list, which are also sorted by them.

        Do nothing, if there already is a table named `temp`.
        '''
        with sql_io.DBCon(self.connect_jordanus()) as (con, cur):
            if 'temp' in set(sql_io.tables(cur, 'jordanus')):
                return
            print('I create a temporary table in the Jordanus database and '\
                  'insert many precomputed values (for faster queries) ...')
            cur.execute(r'''
                    create table temp
                    (
                    ms_id  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                    location TEXT,
                    persons TEXT,
                    titles TEXT,
                    incipits TEXT,
                    PRIMARY KEY (ms_id)
                    );''')
            cur.execute(r'create index i_temp_ms_id on temp (ms_id)')
            con.commit()
            cur.execute(r'select distinct ms_id from sentences')
            items = cur.fetchall()
            for item in items:
                ms_id = item['ms_id']
                temps = [ms_id]
                for delimiter, field_ids in (
                        (', ', '(4, 5, 6)'),        # City, Library, Shelfmark
                        ('; ', '(32, 57, 58, 59)'), # Persons
                        ('; ', '(9, 10, 11)'),      # Titles
                        ('; ', '(65, 66)'),         # Incipits
                        ):
                    cur.execute(r'''
                            select distinct term
                            from terms
                            join sentences on sentences.term_id = terms.term_id
                            join fields on fields.field_id = sentences.field_id
                            where sentences.ms_id = %s
                            and sentences.field_id in {}
                            order by field_rank
                            '''.format(field_ids), ms_id)
                    temps.append(
                            delimiter.join( r['term'] for r in cur.fetchall() ))
                cur.execute('''
                        insert into temp values (%s, %s, %s, %s, %s)
                        ''', temps)
            con.commit()
            print('... I am done.')

    def remove_tags(
            self,
            term: str,
            tag_re: 're.Pattern[str]' = re.compile(r'<[^>]*>'),
            ) -> str:
        return tag_re.sub('', term)

    def rename_bib(self, term: str) -> str:
        '''
        Convert :param:`term` – containing author names or editor names – into a
        string complying with a given structure.
        '''
        if not term:
            return ''
        names = []
        for name in term.split('█'):
            pre, pref, sur, suf = name.split('▄')
            names.append(
                    (sur + suf + ', ' + pre + pref).replace('  ', ' ').rstrip(' ,')
                    )
        return ' / '.join(names)

    def rename_bib1(
            self,
            term: str,
            invert_til: int = 0,
            editors: bool = False,
            ) -> str:
        '''
        Convert :param:`term` – containing author names or editor names – into a
        string complying with a given special structure.

        :param invert_til: tells, until which name the names shall be inverted.
            If, e.g., ``1`` then the first name will be inverted, if ``2`` then
            the two first names will be inverted, if ``0`` then no one.
        :param editors: If ``True``, an editor tag is appended to the name:
            ``'(ed.)'`` behind one and ``'(eds)'`` behind several editors.
        '''
        if not term:
            return ''
        names = []
        for num, name in enumerate(term.split('█')):
            pre, pref, sur, suf = name.split('▄')
            if num < invert_til:
                name = (pref + sur + suf + ', ' + pre).rstrip(' ,')
            else:
                name = pre + pref + sur + suf
            names.append(name)
        if len(names) > 1:
            names[-1] = 'and ' + names[-1]
        len_names = len(names)
        term = ', '.join(names)
        if (invert_til + 1) != len_names:
            term = term.replace(', and ', ' and ')
        if editors:
            if len_names > 1:
                term += ' (eds)'
            else:
                term += ' (ed.)'
        term = term.replace(' and others', ' et al.')
        return term

    def rename_bib2(
            self,
            term: str,
            editors: bool = False,
            invert: bool = True,
            name_parts_re: 're.Pattern[str]' = re.compile(r'[^-, ]+'),
            abbreviandum_re: 're.Pattern[str]' =
                re.compile(r'^[A-ZÄÖÜÁÉÍÓÚÀÈÌÒÙÂÊÎÔÛ].*?[^.]$'),
            alii_re: 're.Pattern[str]' = re.compile(r', others$'),
            ) -> str:
        '''
        Convert :param:`term` – containing author names or editor names – into a
        string complying with a given special structure.

        .. note::
            For the position of the nobility prefix, cp. e.g.
            “Alverny (M.-T. dʼ), Avicenna Latinus. Codices” of the original.

        :param editors: If ``True``, the names are given uninverted.
        '''
        def derive_initials(match: 're.Match[str]') -> str:
            term = match.group()
            return (term[0] + '.') if abbreviandum_re.search(term) else term
        if not term:
            return ''
        names = []
        for num, name in enumerate(term.split('█')):
            pre, pref, sur, suf = name.split('▄')
            pre = name_parts_re.sub(derive_initials, pre)
            if editors:
                name = pre + pref + sur + suf
            else:
                name = sur
                if pre:
                    if suf:
                        suf = suf[1:] # No leading blank: pre has one trailing.
                    if invert:
                        if pref.lower()[:3] in {'von', 'van'}:
                            name = f'{pref}{name} ({pre}{suf}'.rstrip() + ')'
                        else:
                            name = f'{name} ({pre}{suf} {pref}'.rstrip() + ')'
                    else:
                        name = f'{pre}{pref}{name}{suf}'
                else:
                    name += suf
            names.append(name)
        term = ', '.join(names)
        term = alii_re.sub(' et al.', term)
        return term

    def process_row(self, table: str, row_id: int) -> None:
        table = {
                'manuscripts': 'Manuscripts',
                'ms_parts':    'MS_Parts',
                'news':        't_news',
                'works':       'Works',
                }.get(table, '')
        if table:
            with sql_io.DBCon(self.connect()) as (con, cur):
                cols = sql_io.cols(cur, self.config['db']['name'], table)
                process_row(table, cols, row_id, con, cur)

    def subscribe(
            self,
            cat: str,
            email: str,
            ) -> str:
        note = ''
        all_cats = {'jobs', 'news'}
        if cat == 'all':
            cats = all_cats
        else:
            cats = {cat}
        if not cats <= all_cats:
            note = f'No subscriptions exist for the category “{cat}”.'
        else:
            with sql_io.DBCon(self.connect()) as (con, cur):
                for cat in cats:
                    row = con.get(r"id from subscribers where email = %s", email)
                    if row:
                        cur.execute(rf"""
                                update subscribers
                                set gets_{cat} = 1
                                where id = %s
                                """, (row['id'],))
                    else:
                        secret = secrets.randbelow(18_000_000_000_000_000_000)
                        cur.execute(rf"""
                                insert into subscribers
                                (email, secret, gets_{cat}) values
                                (%s, %s, 1)""",
                                (email, secret))
                    con.commit()
        return note

    def update_forum_post(
            self,
            page_id: int,
            post_id: int,
            request: 'bottle.LocalRequest',
            ) -> None:
        user_id, user_name, roles = self.auth(request)
        with sql_io.DBCon(self.connect()) as (con, cur):
            post = con.get(r"* from forum_posts where post_id = %s", post_id)
            assert post, f'Post with ID {post_id} does not exist.'
            assert (post['user_id'] == user_id or 'Admin' in roles
                    ), 'You can edit only your posts.'
            title = request.forms.title.strip()
            doc = request.forms.doc.strip()
            if title != post['title'] or doc != post['doc']:
                until = int(time.strftime('%Y%m%d%H%M'))
                cur.execute(r"""
                        update forum_posts set
                            bis = %s,
                            title = %s,
                            doc = %s
                        where post_id = %s
                        """, (until, title, doc, post_id))
                con.commit()
                first_post = con.get(r"""
                        post_id, title from forum_posts
                        where page_id = %s
                        order by post_id
                        """, page_id)
                first_post_id = first_post.get('post_id', 0)
                first_title = first_post.get('title', '')
                if first_post_id == post_id:
                    trigger = 'A new thread has been started in our forum.'
                else:
                    trigger = 'There is a new contribution to one of the threads in our forum.'
                To = ', '.join( item['email'] for item in con.getall(r"""
                        email from auth_user
                        join auth_membership on user_id = auth_user.id
                        where group_id = 27
                        """) )
                user_name = con.get(r"""
                        concat_ws(' ', first_name, last_name) as name
                        from auth_user where id = %s
                        """, user_id).get('name', '')
                user_email = con.get(
                        r"email from auth_user where id = %s", user_id
                        ).get('email', '')
                term = self.config['mail']['template_forum_new_ar'].format(
                        trigger = trigger,
                        page_id = page_id,
                        post_id = post_id,
                        first_title = first_title,
                        contributor = f'{user_name} (<{user_email}>)',
                        doc = xmlhtml.untag(doc).replace('\n', '\n\n'),
                        ) + '\n\n'
                if To and term and not self.debug:
                    try:
                        mail.send(
                                user = self.config['mail']['user'],
                                password = self.config['mail']['password'],
                                host = self.config['mail']['host'],
                                port = self.config['mail']['port'],
                                From = self.config['mail']['From'],
                                Reply_to =
                                    self.config['mail']['Reply_to_forum'],
                                To = To,
                                Subject = f'Message from PAL forum: {trigger}',
                                text = term,
                                )
                    except ConnectionRefusedError:
                        pass

def adapt_regex(
        term: str,
        base: str = r'([AEHIOUTaehioutı])',
        diacritics: str = r'[̣́̀̄]*',
        ) -> str:
    '''
    In :param:`term`:

    - Remove any :param:`diacritics` after :param:`base` characters.
    - Replace any sequence of whitespace with ``'\\s+'``.
    - Add possible characters between each two characters.
    - Add all :param:`diacritics` as optional after :param:`base` characters.
    '''
    term = unicodedata.normalize('NFKD', term)
    term = re.sub(base + diacritics, r'\g<1>', term)
    term = re.sub(r'\s+', r'\\s+', term)
    term = re.sub(r'([^\\])(?=.)', r'\g<1>[\u00ad\u200b]*', term)
    term = re.sub(base, rf'\g<1>{diacritics}', term)
    return f'(?is)({term})'

def amend_tex_doc(doc: str) -> str:
    for old, new in (
            (r'\\par\n\}', r'}'),
            (r'\\CODICOLOGY\{(\\textbf\{)?Cod\.:?\s*\}?\s*',
                r'\\CODICOLOGY{'),
            (r'\\MSCONTENT\{(\\textbf\{)?Cont\.:?\s*\}?\s*',
                r'\\MSCONTENT{'),
            (r'\\MSBIBLIOGRAPHY\{(\\textbf\{)?Bibl\.:?\s*\}?\s*',
                r'\\MSBIBLIOGRAPHY{'),
            (r'\\WORKORIGIN\{(\\textbf\{)?Origin:?\s*\}?\s*',
                r'\\WORKORIGIN{'),
            (r'\\WORKCONTENT\{(\\textbf\{)?Content:?\s*\}?\s*',
                r'\\WORKCONTENT{'),
            (r'\\WORKQUOTATION\{(\\textbf\{)?Text:?\s*\}?\s*',
                r'\\WORKQUOTATION{'),
            (r'\\WORKBIBLIOGRAPHY\{(\\textbf\{)?Bibl\.:?\s*\}?\s*',
                r'\\WORKBIBLIOGRAPHY{'),
            (r'\\EDITIONHISTORY\{(\\textbf\{)?(?:Ed|Modern ed)\.:?\s*\}?\s*',
                r'\\EDITIONHISTORY{'),
            (r'\u2003', r'{\\EMSP}'),
            (r'\uf500', r'{\\abjadzero}'),
            (r'[␣\u00a0]', r'~'), # no-break space
            (r'˷', r'\\-'), # soft hyphen
            (r'◑', r'\\allowbreak{}'), # zero width space
            (r'⁻', r'\\-/'), # specialty for Tex.
            # Delete most empty fields:
            (r'(?i)\\(?!allowbreak|arabicsettings|hebrewsettings)[^\\{}]+\{\}(?!\{)\n?\n?', r''),
            (r'\n\n+(?=\\SIGNED)', r'\n'),
            ):
        doc = re.sub(old, new, doc)
    return doc.strip() + '\n'

def correct_html(
        term: str,
        col: str,
        arabic: bool,
        con: 'mysql.Connection',
        cur: 'mysql.cursors.DictCursor',
        to_be_joined_re: 're.Pattern[str]' = re.compile(
            r'</(b|cite|del|i|ins|non-public|s|small-caps|su[bp]|u)><\1>'),
        blocky: str = 'address|dd|dl|dt|li|ol|p|table|td|th|tr|ul',
        keep_space: str = 'bdo|latex-only|non-public|web-only',
        ) -> str:
    '''
    Cf. func:`process_row`, which calls this function.
    '''
    def replace_element(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'xmlhtml.Replacer',
            ) -> 'None|str':
        if tag in {'head', 'script', 'style', 'xml'}:
            return ''
        return None

    def replace_tag(
            tag: str,
            attrs: 'dict[str, None|str]',
            startend: bool,
            parser: 'xmlhtml.Replacer',
            ) -> 'tuple[str, dict[str, None|str], bool]':
        reds = {'#bd0014', '#ff0000', 'red'}
        styles = xmlhtml.get_styles(attrs.get('style', ''))
        lang = attrs.get('lang', '')
        if lang not in {'ar', 'grc', 'he', 'la', 'syc'}:
            lang = ''
        if 'dir' in attrs:
            attrs['dir'] = attrs['dir'].lower()
        if tag == 'a' and attrs.get('href'):
            attrs = {
                    k: v for k, v in attrs.items() if (
                        (k == 'href') or
                        (k == 'lang') or
                        (k == 'title') or
                        (k == 'target' and v == '_blank')
                    )}
            if attrs.get('target') == '_blank':
                attrs['rel'] = 'noopener noreferrer'
        elif tag == 'em':
            tag, attrs = 'i', {}, False
        elif tag == 'img':
            attrs = {
                    k: v for k, v in attrs.items()
                    if v and k in {'alt', 'height', 'loading', 'src', 'width'}
                    }
        elif tag in {'ol', 'ul'}:
            liststyle = 'ABC ' if 'ABC' in attrs.get('class', '') else ''
            attrs['class'] = f'{liststyle}list'
        elif tag == 'p':
            attrs = {'dir': attrs['dir']} if 'dir' in attrs else {}
        elif tag == 'span' and lang:
            attrs = {}
        elif tag == 'strong':
            tag, attrs = 'b', {}
        elif tag in {'td', 'th'}:
            attrs = {
                    k: v for k, v in attrs.items()
                    if v and k in {'colspan', 'rowspan', 'style'}
                    }
        elif tag == 'time':
            attrs = {'datetime': attrs['datetime']} if 'datetime' in attrs\
                    else {}
        elif (
                attrs.get('class') == 'small-caps' or
                styles.get('font-variant') == 'small-caps'
                ):
            tag, attrs = 'small-caps', {}
        elif attrs.get('color') in reds or styles.get('color') in reds:
            tag, attrs = 'non-public', {}
        elif styles.get('text-decoration') == 'line-through':
            tag, attrs = 's', {}
        elif styles.get('text-decoration') == 'underline':
            tag, attrs = 'u', {}
        elif 'dir' in attrs:
            tag, attrs = 'bdo', {'dir': attrs['dir']}
        elif tag in {
                'address',
                'b',
                'cite',
                'dd',
                'del',
                'dl',
                'dt',
                'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
                'i',
                'ins',
                'latex-only',
                'li',
                'non-public',
                'ol',
                's',
                'small-caps',
                'sub',
                'sup',
                'table',
                'tr',
                'u',
                'ul',
                'web-only',
                }:
            attrs = {}
        else:
            tag = ''
        if lang:
            attrs['lang'] = lang
        return tag, attrs, tag in xmlhtml.HTML_VOIDS

    def sub_intextref(match: 're.Match[str]') -> str:
        key, num, name, title, published = intextref(con, cur, match, arabic)
        return f'\\{key}{{{num}#{name}}}' if name else f'\\{key}{{{num}}}'

    temp = re.sub(r'<[^>]*>', '', term).strip()
    if not temp or temp.lower() == 'none':
        return ''
    term = xmlhtml.replace(
            term,
            replace_element = replace_element,
            replace_tag = replace_tag,
            )
    term = to_be_joined_re.sub('', term)
    term = term.replace(
            '</i-s><i-s>', '').replace(
            '</i-u><i-u>', '').replace(
            '<i-s>', '<i><s>').replace(
            '</i-s>', '</s></i>').replace(
            '<i-u>', '<i><u>').replace(
            '</i-u>', '</s></u>')
    term = xmlhtml.unnest(
            term,
            test = lambda tag, attrs, startend, info:
                tag in info['tags'] and
                tag not in {'li', 'ol', 'table', 'td', 'th', 'tr', 'ul'},
            )
    term = re.sub(r'[\n\r\t]', ' ', term)
    n = 1
    while n:
        term, n = re.subn( # Shift whitespace to the left of most starttags.
                rf'(<(?!/|{keep_space})[^>]*>)(\s+)', r'\g<2>\g<1>', term)
    n = 1
    while n:
        term, n = re.subn( # Shift whitespace to the right of most endtags.
                rf'(\s+)(</(?!{keep_space})[^>]*>)', r'\g<2>\g<1>', term)
    term = re.sub(rf'\s*<({blocky})>\s*', r'\n<\g<1>>', term)
    term = re.sub(rf'\s*</({blocky})>\s*', r'</\g<1>>\n', term)
    n = 1
    while n:
        term, n = re.subn(
                r'<(?!/|td|th)(?P<tag>[^/>]*)></(?P=tag)>', '', term)
    n = 1
    while n:
        term, n = re.subn(
                r'</(?P<tag>b|i|s|small-caps|sub|sup)>(?P<in>\s*)<(?P=tag)>',
                r'\g<in>', term)
    term = re.sub(rf'\s*<({blocky})>\s*', r'\n<\g<1>>', term)
    term = re.sub(rf'\s*</({blocky})>\s*', r'</\g<1>>\n', term)
    term = re.sub(r'(\s)\s+', r'\g<1>', term).strip()
    term = re.sub(r'</p>\s*<p\b', '</p>\n<p', term)
    term = term.strip()
    if (
            '\\msshelfmark' in term or
            '\\worksiglum' in term or
            '\\worktitle' in term
            ):
        term = INTEXTREF_RE.sub(sub_intextref, term)
    return term

def del_items_without_files(
        repros_path: str, connect: 'functools.partial') -> None:
    '''
    Delete any row in the table of repros for which
    neither a facsimile nor a transcription exists.
    '''
    with sql_io.DBCon(connect()) as (con, cur):
        cur.execute("""
                select
                    doc_type,
                    part_id,
                    doc_id,
                    pagina_sorter,
                    scan_path,
                    tran_path
                from repros
                """)
        for item in cur.fetchall():
            folder = os.path.join(repros_path, item['doc_type'])
            scan_path_exists = os.path.isfile(
                    os.path.join(folder, item['scan_path'])
                    ) if item['scan_path'] else False
            tran_path_exists = os.path.isfile(
                    os.path.join(folder, item['tran_path'])
                    ) if item['tran_path'] else False
            if not (scan_path_exists or tran_path_exists):
                cur.execute("""
                        delete from repros
                        where doc_type = %s
                        and part_id = %s
                        and doc_id = %s
                        and pagina_sorter = %s
                        """,
                        (
                            item['doc_type'],
                            item['part_id'],
                            item['doc_id'],
                            item['pagina_sorter'],
                            ))
            else:
                if item['scan_path'] and not scan_path_exists:
                    cur.execute("""
                            update repros
                            set scan_path = ''
                            where doc_type = %s
                            and part_id = %s
                            and doc_id = %s
                            and pagina_sorter = %s
                            """,
                            (
                                item['doc_type'],
                                item['part_id'],
                                item['doc_id'],
                                item['pagina_sorter'],
                                ))
                if item['tran_path'] and not tran_path_exists:
                    cur.execute("""
                            update repros
                            set tran_path = '', fulltran_part = NULL
                            where doc_type = %s
                            and part_id = %s
                            and doc_id = %s
                            and pagina_sorter = %s
                            """,
                            (
                                item['doc_type'],
                                item['part_id'],
                                item['doc_id'],
                                item['pagina_sorter'],
                                ))
        cur.execute("delete from repros where scan_path = '' and tran_path = ''")
        con.commit()
        try:
            cur.execute("optimize table repros")
            con.commit()
        except:
            pass # Sic.

def get_bib_paths(dirpath: str) -> 'tuple[str, str, str]':
    dirpath = os.path.abspath(dirpath) + os.sep
    name = sorted(
            n for n in os.listdir(dirpath) if n.endswith('.bib')
            )[-1]
    return dirpath, dirpath + name, name

def get_location(
        doc_type: str,
        doc_id: int,
        con: 'mysql.Connection',
        cur: 'mysql.cursors.Cursor',
        ) -> str:
    '''
    Get the location of :param:`doc_id` according to :param:`doc_type`:

    - for MSS: place of the library; abbreviation of the library or, if
      not available, full name; shelfmark.
    - for prints: place of the publisher; publisher; year.
    '''
    if doc_type == 'ms':
        SQL = SQL_GET_MS
        house_a = 'lib_abbrev'
        house_b = 'lib_name'
        number = 'ms_shelfmark'
    else:
        SQL = SQL_GET_PRINT
        house_a = 'print_publisher'
        house_b = 'print_publisher'
        number = 'print_date'
    cur.execute("set @doc_id = %s", (doc_id,))
    cur.execute(SQL)
    doc = cur.fetchone()
    if doc is None:
        return ''
    place_name = doc['place_name']
    return '{}, {}, {}'.format(
            '―' if place_name == '?' else place_name,
            doc[house_a] or doc[house_b],
            doc[number])

def get_pagina(
        filename: str,
        redundant_zeros_re: 're.Pattern[str]' = re.compile(
            r'^0+(?=[-\d()IVXLC])'),
        ) -> str:
    return redundant_zeros_re.sub(
            '',
            filename.rsplit('.', 1)[0].strip().split(' ', 1)[0])

def get_pagina_sorter(pagina: str) -> str:
    pagina = parse.sortnumtext(pagina)
    if pagina and pagina[0] not in '0123456789[':
        pagina = '00000000' + pagina
    return pagina

def get_pagina_styled(
        pagina: str,
        sup_pattern: 're.Pattern[str]' = re.compile(r'({})'.format(
            '|'.join(
                sorted(MULTIPLES_MAP.values(), key = len, reverse = True)
                ))),
        ) -> str:
    pagina = pagina.lstrip('0')
    for old, new in sorted(MULTIPLES_MAP.items(), key = len, reverse = True):
        if old in pagina:
            pagina = pagina.replace(old, new)
            break
    return sup_pattern.sub(r'<sup>\g<1></sup>', pagina)

def get_person_name(
        item: 'dict[str, None|str]',
        key: str = 'person_name',
        ) -> 'dict[str, str]':
    '''
    From :param:`item`, get the value assigned to :param:`key` and convert it to
    the form used in the JSON for the datatable representation.
    '''
    name = item[key]
    even_name = parse.even(name, low = False)
    return {
            '_': name + '<div hidden="">█' + even_name + '</div>',
            's': even_name,
            }

def get_prev_next(doc_id: int, rows: 'list[tuple]') -> 'tuple[tuple, tuple]':
    prev_item = next_item = ()
    for n, (row_id, *_) in enumerate(rows):
        if row_id == doc_id:
            if n > 0:
                prev_item = rows[n - 1]
            if n < len(rows) - 1:
                next_item = rows[n + 1]
            break
    return prev_item, next_item

def get_repro_links(item: 'dict[str, str]') -> 'tuple[str, str]':
    '''
    From :param:`item`, build a link to the first page of the belonging scan,
    if available, and a link to the first page of the belonging transcription,
    if available.
    '''
    doc_type = item['doc_type']
    doc_id = item['doc_id']
    text_id = item['work_id'] if doc_type == 'edition' else item['part_id']
    pagina = item['pagina']
    fulltran_part = item['fulltran_part']
    scan_link = f'<a class="dark key" href="/{doc_type}/{doc_id}/{text_id}/{pagina}" target="_blank" rel="noopener noreferrer">Images</a>'\
                if pagina and item['scan_path'] else ''
    tran_link = f'<a class="dark key" href="/{doc_type}/{doc_id}/{text_id}/text/{fulltran_part}" target="_blank" rel="noopener noreferrer">Text</a>'\
                if fulltran_part else ''
    return scan_link, tran_link

def get_title(
        item: 'dict[str, None|str]',
        with_link: bool = True,
        ) -> 'dict[str, str]':
    '''
    Get the title field of the item :param:`item` and convert it to the field as
    it is used in the JSON for the datatable representation.

    :param with_link: If ``True`` (the default), wrap the title with the link to
        the belonging work entry.
    '''
    title = item['work_title'].split(' (by')[0]
    even_title = parse.even(title, low = False)
    if with_link:
        title = f'<a href="/work/{item["work_id"]}">{title}</a>'
    return {
            '_': title + '<div hidden="">█' + even_title + '</div>',
            's': even_title,
            }

def intextref(
        con: 'mysql.Connection',
        cur: 'mysql.cursors.DictCursor',
        match: 're.Match[str]',
        arabic: bool = False,
        ) -> 'tuple[str, str, str, str, bool]':
    key = match.group('key').strip()
    num = match.group('num').strip()
    name = match.group('name').strip()
    if num == '0' or key == 'externalms':
        return key, num, name, '', True
    elif key == 'msshelfmark':
        item = con.get(r"""
                    Manuscripts.f_shelfmark              as ms_shelfmark,
                    IF(Manuscripts.f_hidden = 'F', 1, 0) as ms_published,
                    Libraries.f_lib_name                 as lib_name,
                    IF(Manuscripts.f_cat_lang = 'Arabic' AND Libraries.f_lib_abbrev_arabic <> '', Libraries.f_lib_abbrev_arabic, Libraries.f_lib_abbrev) as lib_abbrev,
                    Places.f_placename                   as place_name
                from Manuscripts
                join Libraries
                    on Libraries.id = Manuscripts.f_place_library
                join Places
                    on Places.id = Libraries.f_place
                where Manuscripts.id = %s
                """, num)
        if item:
            lib_name = item['lib_abbrev'] or item['lib_name']
            lib = xmlhtml.escape(f"{item['place_name']}, {lib_name}")
            return (
                    key,
                    num,
                    f"{lib}, {item['ms_shelfmark']}",
                    '',
                    bool(item['ms_published']),
                    )
    elif key == 'worksiglum':
        sql = """
                    Works.id                       as work_id,
                    Works.f_siglum                 as work_siglum,
                    Works.f_standard_title         as work_title,
                    IF(Works.f_hidden = 'F', 1, 0) as work_published,
                    Persons.f_person_name          as person_name
                from Works
                join Creators
                    on Creators.f_work = Works.id
                join Persons
                    on Persons.id = Creators.f_creator
                where Creators.f_creator_role = 1
                """
        if num:
            item = con.get(sql + "and Works.id = %s", num)
            if item:
                return (
                        key,
                        num,
                        xmlhtml.escape(item['work_siglum']),
                        xmlhtml.escape(
                            f"{item['person_name']}: {item['work_title']}"),
                        bool(item['work_published']),
                        )
        elif name:
            sql += "and Works.f_siglum = %s\n"
            if arabic:
                sql += "and Works.f_lang = 1"
            else:
                sql += "and Works.f_lang <> 1"
            item = con.get(sql, name)
            if item:
                return (
                        key,
                        str(item['work_id']),
                        name,
                        xmlhtml.escape(
                            f"{item['person_name']}: {item['work_title']}"),
                        bool(item['work_published']),
                        )
    elif key == 'worktitle':
        item = con.get(r"""
                    Works.id                       as work_id,
                    Works.f_siglum                 as work_siglum,
                    Works.f_standard_title         as work_title,
                    IF(Works.f_hidden = 'F', 1, 0) as work_published,
                    Persons.f_person_name          as person_name
                from Works
                join Creators
                    on Creators.f_work = Works.id
                join Persons
                    on Persons.id = Creators.f_creator
                where Creators.f_creator_role = 1
                and Works.id = %s
                """, num)
        if item:
            return (
                    key,
                    str(item['work_id']),
                    f"<cite>{xmlhtml.escape(item['work_title'])}</cite>",
                    xmlhtml.escape(item['work_siglum']),
                    bool(item['work_published']),
                    )
    return (key, f'█{num}', name, '', False)

def process_row(
        table: str,
        cols: 'dict[str, dict[str, str|int]]',
        row_id: int,
        con: 'mysql.Connection',
        cur: 'mysql.cursors.DictCursor',
        ):
    '''
    In the database table :param:`table` with the columns :param:`cols`, go over
    the cells of row :param:`row_id` and revise them. If it is necessary for any
    information, access the database with :param:`con`, :param:`cur`.

    The revision of the cell value comprises the following measures:

    - Return the empty string if the cell value is an empty string or the string
      ``'none'``, after the value is lowered and stripped from tags and from any
      leading or trailing whitespace. The condition with ``'none'`` is necessary
      due to one of the many wrong turns of Web2Py.
    - Normalize the HTML and allow only certain tag-attribute combinations:

        - allow the `lang` attribute for every element.
        - `a[href|title]` ‘link’
        - `a[href|title|rel|target="_blank"]` ‘link, opens in new tab’
        - `address`
        - `b` ‘bold’
        - `bdo[dir="ltr"]` ‘left-to-right run’
        - `bdo[dir="rtl"]` ‘right-to-left run’
        - `cite` ‘title of a referenced work’
        - `dd` ‘definiens in a definition listʼ
        - `del` ‘deletion’
        - `dl` ‘definition list’
        - `dt` ‘definiendum in a definition list’
        - `h1`, `h2`, `h3`, `h4`, `h5`, `h6`
        - `i` ‘italic’
        - `img[alt|height|loading|src|width]`
        - `ins` ‘insertion’
        - `latex-only` for print
        - `li` ‘list item’ (so far only in t_ tables)
        - `non-public` ‘non-public’ (the red parts)
        - `ol[class="list"]` ‘ordered list’ (“list” is set automatically)
        - `ol[class="ABC list"]` ‘ordered list’ (“list” is set automatically)
        - `p` ‘paragraph’
        - `p[dir="rtl"]` ‘right-to-left paragraph’
        - `s` ‘strikethrough’
        - `small-caps` ‘small caps’
        - `span[lang="ar|grc|he|la|syc"]` ‘language specification’
        - `sub` ‘subscript’
        - `sup` ‘superscript’
        - `table` ‘table’
        - `time[datetime]` ‘time info’ (norm form may be shown in the attribute)
        - `td[colspan|rowspan|style]` ‘table cell’
        - `th[colspan|rowspan|style]` ‘headerlike table cell’
        - `tr` ‘table row’
        - `u` ‘underline’
        - `ul[class="list"]` ‘unnumbered list’ (“list” is set automatically)
        - `ul[class="ABC list"]` ‘unnumbered list’ (“list” is set automatically)
        - `web-only` for exclusion from print

    - Delete in the text:

      - The control character Bell (U+0007) (backslash-a)
      - The control character Vertical Tab (U+000B) (backslash-v)
      - LRM mark (U+200E)
      - RLM mark (U+200F)

    - Change in the text:

      - three-em dash to em dash
      - four-per-em space (U+2005) to normal space

    - Remove tagging nested into tagging of the same tag name except the tagging
      of lists and tables.
    - Turn tabs and newlines into blanks.
    - Put whitespace found after a starttag before it.
    - Put whitespace found before an endtag after it.
    - Remove empty elements except td and th.
    - Merge certain elements, when they are equal and adjacent to each other.
    - Normalize whitespace around block elements.
    - Of a sequence of two or more whitespaces, keep only the first.
    - Ensure that there is exactly one newline between p elements.
    - Remove leading and trailing whitespace.
    - Correct any sigla attached to “worksiglum” shortcuts in the text.
    '''
    tablename = table.lower()
    con.begin()
    cur.execute(f"""
            select * from `{table}` where id = %s
            LOCK IN SHARE MODE
            """, row_id)
    # LOCK IN SHARE MODE: Prevent writing until we have written – so that rather
    # we get overwritten in a race condition. Do not prevent reading (that would
    # be `FOR UPDATE`) because this could hamper editors too much.
    row = cur.fetchone()
    if row:
        arabic = False
        if tablename == 'manuscripts' and row['f_cat_lang'] == 'Arabic':
            arabic = True
        elif tablename == 'ms_parts':
            if con.get(f"""
                    f_lang
                    from Works
                    join MS_Parts
                        on Works.id = MS_Parts.f_part_of_work
                    where MS_Parts.id = %s
                    """, row_id)['f_lang'] == 1:
                arabic = True
        elif tablename == 'works' and row['f_lang'] == 1:
            arabic = True
        for col, col_info in cols.items():
            if not (
                    'text' in col_info['COLUMN_TYPE'] or
                    'varchar' in col_info['COLUMN_TYPE']
                    ):
                continue
            val = new_val = row.get(col)
            if val is None:
                new_val = ''
            # Delete never-normal characters used for temporary segmentation and
            # perform other character substitutions:
            new_val = new_val.replace(
                    '\a', '').replace(
                    '\v', '').replace(
                    '\u2005', ' ').replace(
                    '\u200e', '').replace(
                    '\u200f', '').replace(
                    '⸻', '—').replace(
                    '\N{MATHEMATICAL LEFT ANGLE BRACKET}',
                        '\N{LEFT-POINTING ANGLE BRACKET}').replace(
                    '\N{MATHEMATICAL RIGHT ANGLE BRACKET}',
                        '\N{RIGHT-POINTING ANGLE BRACKET}')
            if col == 'f_range':
                new_val = re.sub(
                        r'[\s\u2060]*[-‑–][\s\u2060]*',
                        '–\u2060', new_val)
            if new_val and (
                    'text' in col_info['COLUMN_TYPE'] or
                    col in EXTRA_XML_COLS):
                new_val = correct_html(new_val, col, arabic, con, cur)
                if new_val and col not in EXTRA_XML_COLS:
                    new_val += XML_STAMP
            if new_val != val:
                cur.execute(f"""
                        update `{table}`
                        set `{col}` = %s
                        where id = %s
                        """, (new_val, row_id))
    con.commit() # commits and releases the lock.

def raafzeit_to_isodate(num: int) -> str:
    '''
    Turn the raafzeit :param:`num` into an ISO-8601-string.

    .. important::
        The raafzeit must have a granularity of minutes.
    '''
    num = str(num)
    if num == '0':
        return ''
    return (
        num[0:-8] + '-' + num[-8:-6] + '-' + num[-6:-4] + ', ' +
        num[-4:-2] + ':' + num[-2:]
        )
