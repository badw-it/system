# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Benno van Dalen and Stefan Müller in 2019 ff.
# (© http://badw.de)
import os
import random
try: import regex as re
except ImportError: import re
import sys
import unicodedata
from decimal import Decimal as D

import mmcalendars
import mmtables

class By1(mmcalendars.By):
    variantname = 'first month Adhār, leap month Shubāṭ'

    def __init__(self, shift: int = 0):
        super().__init__(shift = shift)

class By2(mmcalendars.By):
    variantname = 'first month Aylūl, leap month Shubāṭ'

    def __init__(self, shift: int = 6):
        super().__init__(shift = shift)

class By3(mmcalendars.By):
    variantname = 'first month Tishrīn I, leap month Shubāṭ'

    def __init__(self, shift: int = 7):
        super().__init__(shift = shift)

def get_config(
        doc: str,
        ) -> 'Generator[tuple[str, dict[str, bool|list[int]]]]':
    '''
    Get a config given in ini style but with possibly repeated section titles.
    '''
    for m in re.finditer(r'(?ms)^\s*\[(.*?)\]\s+(.*?)(?=\[)', doc):
        yield (
                m.group(1).strip(), # The tableclassname as section title.
                dict(
                    spec for row in m.group(2).strip().splitlines()
                    if (spec := get_spec(row))
                    ), # The section parsed into a dictionary.
                )

def get_rows(
        config: 'list[tuple[str, dict[str, bool|list[int]]]]',
        ) -> 'Generator[str]':
    '''
    Get the rows of subtables as specified in :param:`config`.
    '''
    for tableclassname, specs in config:
        Table = getattr(mmtables, tableclassname)
        Calendars = (
                mmcalendars.Ar1,
                mmcalendars.Ar2,
                By1,
                By2,
                By3,
                mmcalendars.Eg1,
                mmcalendars.Eg2,
                mmcalendars.Ju1,
                mmcalendars.Ju2,
                mmcalendars.Ju3,
                mmcalendars.Pe1,
                mmcalendars.Pe2,
                ) if Table in {mmtables.Ext, mmtables.Mon} else (
                mmcalendars.Ar1,
                By1,
                mmcalendars.Eg1,
                mmcalendars.Ju1,
                mmcalendars.Pe1,
                )
        yield f'Subtable of {Table.name}'
        is_leap_year = specs.get('intercal', False)
        if 'first_is_zero' in specs:
            first_is_zero = specs['first_is_zero']
            if first_is_zero:
                yield '(first value equal to zero)'
            else:
                yield '(first value NOT equal to zero)'
        else:
            first_is_zero = False
        complete = bool(abs(first_is_zero - 1))
        if 'longitude' in specs:
            yield f'(longitude {specs["longitude"]}°)'
        yield 'arguments ar1 ar2 by1 by2 by3 eg1 eg2 ju1 ju2 ju3 pe1 pe2'
        for index, arg in enumerate(specs['args'], start = 1):
            row = []
            row.append(str(index))
            for subindex, Calendar in enumerate(Calendars):
                cal = Calendar()
                if Table == mmtables.Col:
                    table = Table(
                            cal,
                            complete = complete,
                            longitude = specs.get('longitude', 0),
                            epoch = specs.get('epoch', 0),
                            )
                elif Table == mmtables.Mon:
                    table = Table(
                            cal,
                            complete = complete,
                            longitude = specs.get('longitude', 0),
                            is_leap_year = is_leap_year,
                            )
                else:
                    table = Table(
                            cal,
                            complete = complete,
                            longitude = specs.get('longitude', 0),
                            )
                if subindex == 0:
                    row.append(table.get_argstring(arg))
                days_in_arg = table.get_days_in_arg(arg)
                if Table in {
                        mmtables.Col,
                        mmtables.Day,
                        mmtables.Ext,
                        mmtables.Mon,
                        mmtables.Sin,
                        }:
                    row.append(str(round(days_in_arg)))
                else:
                    row.append(f'{days_in_arg:.5f}')
            yield ' '.join(row)

def get_spec(row: str) -> 'tuple[str, bool|list[int]]':
    '''
    Parse a :param:`row` of a config section into a key-value tuple.
    Return an empty tuple if the row does not contain expected keys and values.
    '''
    try:
        key, val = row.split('=', 1)
    except ValueError:
        return ()
    key = key.strip()
    val = val.strip()
    val = (
            True if val.lower() == 'true' else
            False if val.lower() == 'false' else
            [ int(num) for num in val.split() ] if key == 'args' else
            int(val) if key in {'epoch', 'longitude'} else
            None
            )
    if val is None:
        return ()
    else:
        return (key, val)

def test_main(
        paramfilepath: str,
        paramfile_encoding: str,
        checkfilepath: str,
        checkfile_encoding: str,
        ) -> None:
    def get_standardrows() -> 'Generator[str]':
        with open(checkfilepath, encoding = checkfile_encoding) as file:
            for row in file:
                row = trim(row)
                if row:
                    yield row

    with open(paramfilepath, encoding = paramfile_encoding) as file:
        config = get_config(file.read())
    results = []
    for standardrow, row in zip(get_standardrows(), get_rows(config)):
        row = trim(row)
        if standardrow == row:
            results.append(row)
        else:
            results.append('#ideal█' + standardrow)
            results.append('#real █' + row)
    path = checkfilepath + '.result.txt'
    with open(path, 'w', encoding = 'utf-8') as file:
        file.write('\n'.join(results))
    print(f'Output in {path}.')

def test_squeeze(
        checkfilepath: str,
        checkfile_encoding: str,
        tolerance: int,
        ) -> int:
    errors_in_tests = 0
    results = []
    is_leap_year = False
    for Calendar in (
            mmcalendars.Ar1,
            mmcalendars.Ar2,
            By1,
            By2,
            By3,
            mmcalendars.Eg1,
            mmcalendars.Eg2,
            mmcalendars.Ju1,
            mmcalendars.Ju2,
            mmcalendars.Ju3,
            mmcalendars.Pe1,
            mmcalendars.Pe2,
            ):
        for Table in (
                mmtables.Mon,
                mmtables.Ext,
                mmtables.Sin,
                mmtables.Col,
                ):
            cal = Calendar()
            if Table == mmtables.Mon:
                table = Table(cal, is_leap_year = is_leap_year)
            else:
                table = Table(cal)
            epoch = table.epoch if Table == mmtables.Col else 0
            results.append(trim(f'{cal.name} calendar; epoch {epoch}'))
            if cal.variantname:
                results.append(trim(f'({cal.variantname})'))
            results.append(trim(f'Subtable of {table.name}'))
            results.append(
                    f'first_is_zero = False; '\
                    f'is_intercal = {is_leap_year}')
            for i in range(1, 100): # or any other number of tests
                last_arg = random.randint(10, 30) # produces sensible test values
                arg = last_arg
                if Table in {mmtables.Col, mmtables.Sin}:
                    if Calendar in {mmcalendars.Ar1, mmcalendars.Ar2}:
                        arg = 30 * last_arg
                    elif Calendar in {
                            By1, By2, By3,
                            mmcalendars.Ju1,
                            mmcalendars.Ju2,
                            mmcalendars.Ju3,
                            }:
                        arg = 28 * last_arg
                    elif Calendar in {
                            mmcalendars.Eg1,
                            mmcalendars.Eg2,
                            mmcalendars.Pe1,
                            mmcalendars.Pe2,
                            }:
                        arg = 20 * last_arg
                elif Table in {mmtables.Loa, mmtables.Lod}:
                    arg = last_arg - 15
                elif Table == mmtables.Mon:
                    arg = random.randint(8, 14)
                days_in_arg = table.get_days_in_arg(arg)
                result = [f'{i:>3}']
                exact_param = D(random.random() * 25)
                total_mean_motion = days_in_arg * exact_param
                while total_mean_motion >= 360:
                    total_mean_motion -= 360
                total_mean_motion = D(round(total_mean_motion * 3600) / 3600)
                preliminary_param = D(round(exact_param * tolerance) / tolerance)
                estimated_param = table.get_squeeze_param(
                        days_in_arg, total_mean_motion, preliminary_param)
                result.extend(map(str, (
                        f'{last_arg:>3}',
                        f'{exact_param: 12.6f}',
                        f'{preliminary_param: 12.6f}',
                        f'{estimated_param: 12.6f}',
                        f'{estimated_param - exact_param: 12.6f}',
                        )))
                if round(3600 * (estimated_param - exact_param)):
                    errors_in_tests += 1
                    result.append('#Error!█')
                results.append(' '.join(result))
    if errors_in_tests:
        print(f'Squeeze tests concluded with {errors_in_tests} errors.')
    else:
        print('Squeeze tests successfully concluded.')
    path = os.path.dirname(checkfilepath) + os.sep + 'squeeze.result.txt'
    with open(path, 'w', encoding = checkfile_encoding) as file:
        file.write('\n'.join(results))
    print(f'Output in {path}.')

def trim(
        term: str,
        white_re: 're.Pattern[str]' = re.compile(r'\s\s+'),
        ) -> str:
    term = white_re.sub(' ', term.strip().upper())
    term = unicodedata.normalize('NFKD', term)
    term = ''.join( c for c in term if 31 < ord(c) < 91 )
    for old, new in [
            ('JUMADA L-ULA', 'JUMADA AL-ULA'),
            ('MONTH AYLUL', 'MONTH ILUL'),
            ('PHARMUTHI', 'PHARMOUTHI'),
            ('EPAGOMENAI', 'EPAGOMENAE'),
            ('IANUARIUS', 'JANUARY'),
            ('FEBRUARIUS', 'FEBRUARY'),
            ('MARTIUS', 'MARCH'),
            ('APRILIS', 'APRIL'),
            ('MAIUS', 'MAY'),
            ('IUNIUS', 'JUNE'),
            ('IULIUS', 'JULY'),
            ('AUGUSTUS', 'AUGUST'),
            ('SEPTEMBER', 'SEPTEMBER'),
            ('OCTOBER', 'OCTOBER'),
            ('NOVEMBER', 'NOVEMBER'),
            ('DECEMBER', 'DECEMBER'),
            ('URDIBIHISHT', 'URDIBIHIST'),
            ]:
        term = term.replace(old, new)
    return term

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print(
                '''I cannot do without two or three command line arguments:

1. the path to a file specifying the parameters, formatted in ini-format, e.g.:
   [Col]
   args=1 21 42 61 81 101 121 141 161 181 201 221 241 261 281 301 321 341 361 381 401 421 441 461 481 501 521 541 561 581

   [Sin]
   args=40 60 80 100 200 300 400 500 600

   [Ext]
   args=1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20

   [Mon]
   first_is_zero=TRUE
   args=1 2 3 4 5 6 7 8 9 10 11 12 14

2. the path to a checkfile containing the tables resulting from the given parameters.

3. (optional) An integer specifying the tolerance used for the squeeze tests.
   A tolerance of 100 should produce ca. 1850 errors.
   A tolerance of 300 should produce ca. 850 errors.
   A tolerance of 900 and above should produce 0 errors.
''')
    else:
        print('Start the tests of get_squeeze_param …')
        test_squeeze(
                sys.argv[2],
                'cp437',
                int(sys.argv[3]) if len(sys.argv) > 3 else 900,
                )
        print('·········')
        print('Start the main tests …')
        test_main(sys.argv[1], 'cp437', sys.argv[2], 'cp437')
