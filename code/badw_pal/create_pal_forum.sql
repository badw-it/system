-- Licensed under http://www.apache.org/licenses/LICENSE-2.0
-- Attribution notice: by Stefan Müller in 2019 ff. (© http://badw.de)

create table if not exists forum_pages (
	page_id SERIAL
)
ROW_FORMAT = COMPRESSED
;
create table if not exists forum_posts (
	post_id SERIAL,
	page_id BIGINT UNSIGNED NOT NULL,
	user_id INTEGER NOT NULL,
	von BIGINT NOT NULL DEFAULT 0, -- minutengequantelte Raafzeit
	bis BIGINT NOT NULL DEFAULT 0, -- minutengequantelte Raafzeit
	title VARCHAR(768) NOT NULL DEFAULT '',
	doc LONGTEXT NOT NULL, -- DEFAULT '' sometimes not possible in MariaDB.
	FOREIGN KEY (page_id) REFERENCES forum_pages(page_id) ON DELETE RESTRICT,
	FOREIGN KEY (user_id) REFERENCES auth_user(id) ON DELETE RESTRICT,
	PRIMARY KEY (post_id)
)
ROW_FORMAT = COMPRESSED
;
