# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
import secrets
import sys
import time
import traceback
from collections import deque
from urllib.parse import quote

import gehalt
import __init__
import fs
import mail
import sql_io
import sys_io
import xmlhtml

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Perform an ongoing background process:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Curate subscribers.
    - Send to-be-sent posts of jobs and news to subscribers. Include a link for
      unsubscription.
    - Apply :func:`gehalt.process_row`.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    config['mail']['user'], config['mail']['password'] =\
            fs.get_keys(paths['mailserver_access'])
    connect = sql_io.get_connect(*fs.get_keys(paths['db_access']), config)
    db = config['db']['name']
    while True:
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        time.sleep(5)
        with sql_io.DBCon(connect()) as (con, cur):
            for user_row in con.getall(r"""
                    distinct
                    auth_user.id as id,
                    auth_user.email as email
                    from auth_user
                    join auth_membership
                        on auth_membership.user_id = auth_user.id
                    where auth_membership.group_id in
                        (2, 3, 4, 7, 8, 9, 11, 12, 13, 16, 22)
                    """):
                subscriber_row = con.get(r"""
                        * from subscribers where user_id = %s or email = %s
                        """, user_row['id'], user_row['email'])
                if subscriber_row:
                    cur.execute(r"""
                            update subscribers
                            set user_id = %s, email = %s
                            where id = %s""",
                            (
                                user_row['id'],
                                user_row['email'],
                                subscriber_row['id'],
                            ))
                else:
                    secret = secrets.randbelow(18_000_000_000_000_000_000)
                    cur.execute(r"""
                            insert into subscribers
                            (user_id, email, secret, gets_jobs, gets_news)
                            values (%s, %s, %s, 1, 1)""",
                            (user_row['id'], user_row['email'], secret))
                con.commit()
            if config['modes'].getboolean('announcing', fallback = False):
                for case, title_key, doc_key in (
                        ('jobs', 'f_jobtitle', 'f_jobdesc'),
                        ('news', 'f_headline', 'f_content'),
                        ):
                    case_rows = con.getall(rf"""
                            * from t_{case} where send_me = 1
                            """)
                    cur.execute(rf"""
                            update t_{case} set send_me = 0 where send_me = 1
                            """)
                    con.commit()
                    if not case_rows:
                        continue
                    for subscriber_row in con.geteach(rf"""
                                email, secret
                                from subscribers
                                where gets_{case} = 1
                                and email like '%@%'
                                """):
                        email = subscriber_row['email']
                        secret = subscriber_row['secret']
                        for case_row in case_rows:
                            doc = xmlhtml.untag(
                                    case_row[doc_key] or ''
                                    ).replace('␣', '\xa0').replace('\n', '\n\n')
                            doc = f'''{doc}
____________________
If you want to unsubscribe from PAL news or PAL jobs,
please let us know: info@ptolemaeus.badw.de
(please, make clear if you want to unsubscribe from PAL jobs or PAL news or both).
'''
### Unsubscription per click on link in mail was too prone to user errors
### and was replaced by unsubscription per manual mail.
##{config['ids']['url'].replace('http://', 'https://')}/unsubscribe/{case}/{quote(email)}/{secret}
                            mail.send(
                                    user = config['mail']['user'],
                                    password = config['mail']['password'],
                                    host = config['mail']['host'],
                                    port = config['mail']['port'],
                                    From = config['mail']['From'],
                                    Reply_to = config['mail']['Reply_to'],
                                    To = email,
                                    Subject =
                                        f"PAL {case}: {case_row[title_key]}",
                                    text = doc,
                                    )
            for table in tuple(sql_io.tables(cur, db)):
                table = table.replace('\\', '').replace('`', '')
                tablename = table.lower()
                if re.search(
                        r'^(archive_|bib$|ft$|gloss_terms$|repros$)',
                        tablename):
                    continue
                cols = sql_io.cols(cur, db, table)
                if 'id' not in cols:
                    continue
                row_ids = [ r['id'] for r in con.geteach(rf"id from {table}") ]
                for row_id in row_ids:
                    time.sleep(0.2)
                    gehalt.process_row(table, cols, row_id, con, cur)

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
