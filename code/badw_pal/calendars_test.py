# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Benno van Dalen and Stefan Müller in 2019.
# (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import sys
from collections import deque
from decimal import Decimal as D
import calendars

CALENDARS = {
        'a1': calendars.ArabicCalendar(kind = 1),
        'a2': calendars.ArabicCalendar(kind = 2),
        'a3': calendars.ArabicCalendar(kind = 3),
        'a4': calendars.ArabicCalendar(kind = 4),
        'b1': calendars.ByzantineCalendar(kind = 1),
        'b2': calendars.ByzantineCalendar(kind = 2),
        'c1': calendars.CopticCalendar(kind = 1),
        'c2': calendars.CopticCalendar(kind = 2),
        'c3': calendars.CopticCalendar(kind = 3),
        'c4': calendars.CopticCalendar(kind = 4),
        'e1': calendars.EgyptianCalendar(kind = 1),
        'e2': calendars.EgyptianCalendar(kind = 2),
        'e3': calendars.EgyptianCalendar(kind = 3),
        'e4': calendars.EgyptianCalendar(kind = 4),
        'g1': calendars.GregorianCalendar(kind = 1),
        'h1': calendars.HebrewCalendar(kind = 1),
        'j1': calendars.JulianCalendar(kind = 1),
        'j2': calendars.JulianCalendar(kind = 2),
        'p1': calendars.PersianCalendar(kind = 1),
        'p2': calendars.PersianCalendar(kind = 2),
        'p3': calendars.PersianCalendar(kind = 3),
        }

def test_jdn_date_equivalence(
        test_cases_path: str,
        test_cases_encoding: str,
        ) -> None:
    '''
    :param test_cases_path: Path to a file with lines like:

    2000000 G1= 763- 9-18 J1= 763- 9-14 J2= 801- 9-14 H1=4524- 1- 3 E1=1511- 8- 3 E2=1087- 8- 3 E3= 648- 8- 3 E4= 627- 8- 3 B1=1074-12-14 B2=1075- 1-14 C1= 480- 1-16 C2= 480- 1-16 C3= 793- 1-16 C4=1087- 1-16 I1= 146- 7- 2 I2= 146- 7- 2 I3= 146- 7- 1 I4= 146- 7- 1 P1= 132- 5- 3 P2= 132- 5- 3 P3=-131- 4- 6
    '''
    errors_to_jdn = deque()
    errors_from_jdn = deque()
    print('Date-JDN-equivalence tests have started …')
    date_pattern = r'(?:[a-zA-Z]\d+)\s*=\s*(?:-?\d+)\s*-\s*(?:\d+)\s*-\s*(?:\d+)\s*'
    date_pattern_grouped = date_pattern.replace('(?:', '(')
    try:
        path = os.path.abspath(test_cases_path)
        with open(path, encoding = test_cases_encoding) as file:
            num = 0
            for line in file:
                line = line.strip()
                match = re.match(rf'(\d+)\s*((?:{date_pattern})+)', line)
                if match:
                    jdn = D(match.group(1))
                    dates = match.group(2)
                    for date in re.split(r'(?=[a-zA-Z]\d+\s*=)', dates)[1:]:
                        match = re.match(rf'\s*{date_pattern_grouped}', date)
                        calendar = CALENDARS[match.group(1).lower()]
                        year = int(match.group(2))
                        month = int(match.group(3))
                        day = int(match.group(4))
                        num += 1
                        try:
                            assert jdn == calendar.to_jdn(year, month, day)
                        except AssertionError:
                            errors_to_jdn.append(
                                    f'{year}-{month}-{day} (given as “{date}”) was not '\
                                    f'turned into JDN {jdn}. '\
                                    f'{calendar.__class__.__name__}, {calendar.kind}.')
                        num += 1
                        try:
                            assert (year, month, day) == calendar.from_jdn(jdn)
                        except AssertionError:
                            errors_from_jdn.append(
                                    f'JDN {jdn} was not turned into {year}-{month}-{day} '\
                                    f'(given as “{date}”). '\
                                    f'{calendar.__class__.__name__}, {calendar.kind}.')
        if errors_to_jdn or errors_from_jdn:
            print(f'{num} tests done. See errors in:')
            if errors_to_jdn:
                path = os.path.abspath('calendars_test_log_equivalence_to_jdn.txt')
                with open(path, 'w', encoding = 'utf-8') as file:
                    size = file.write('\n'.join(errors_to_jdn))
                print(f' {path} ({size})')
            if errors_from_jdn:
                path = os.path.abspath('calendars_test_log_equivalence_from_jdn.txt')
                with open(path, 'w', encoding = 'utf-8') as file:
                    size = file.write('\n'.join(errors_from_jdn))
                print(f' {path} ({size})')
        else:
            print(f'{num} tests done. No errors occurred.')
    except Exception as e:
        print(f'The tests have been interrupted with the error: {e}')
        print('The arguments were:')
        print(f'Path to a file with the test cases: {path}')
        print(f'Encoding of this file: {test_cases_encoding}')

def test_first_and_last_day_of_each_month() -> None:
    errors = deque()
    print('First-and-last-day-of-each-month tests have started …')
    num = 0
    for Calendar, kind_range, year_range in (
            (calendars.ArabicCalendar,    range(1, 5), range(1, 2001)),
            (calendars.ByzantineCalendar, range(1, 3), range(1, 2001)),
            (calendars.CopticCalendar,    range(1, 5), range(1, 2001)),
            (calendars.EgyptianCalendar,  range(1, 5), range(1, 2001)),
            (calendars.GregorianCalendar, range(1, 2), range(1, 2001)),
            (calendars.HebrewCalendar,    range(1, 2), range(1, 2001)),
            (calendars.JulianCalendar,    range(1, 2), range(1, 2001)),
            (calendars.PersianCalendar,   range(1, 4), range(1, 2001)),
            ):
        for kind in kind_range:
            calendar = Calendar(kind = kind)
            begin = year_range[0]
            if isinstance(calendar, calendars.HebrewCalendar):
                nextjdn = calendar.new_year(begin)
            for year in year_range:
                try:
                    intercalary = calendar.intercalary(year)
                    if isinstance(calendar, calendars.HebrewCalendar):
                        thisjdn = nextjdn
                        nextjdn = calendar.new_year(year + 1)
                    for month in range(1, calendar.number_of_months + 1):
                        if isinstance(calendar, calendars.HebrewCalendar):
                            day = calendar.len_month(month, intercalary, nextjdn - thisjdn)
                        else:
                            day = calendar.len_month(month, intercalary)
                        if day > 0:
                            jdn = calendar.to_jdn(year, month, 1)
                            y, m, d = calendar.from_jdn(jdn)
                            num += 1
                            assert y == year and m == month and d == 1,\
                                    f'{year}-{month}-{1} unequal to derived {y}-{m}-{d}'
                            jdn = calendar.to_jdn(year, month, day)
                            y, m, d = calendar.from_jdn(jdn)
                            num += 1
                            assert y == year and m == month and d == day,\
                                    f'{year}-{month}-{day} unequal to derived {y}-{m}-{d}'
                except Exception as e:
                    errors.append((
                            f'Calendar: {calendar.__class__.__name__}',
                            f'kind: {calendar.kind}',
                            f'JDN: {jdn:>6}',
                            f'Error: {e}',
                            ))
    if errors:
        path = os.path.abspath('calendars_test_log_first_and_last_day_of_each_month.txt')
        with open(path, 'w', encoding = 'utf-8') as file:
            size = file.write('\n'.join( '█'.join(error) for error in errors ))
        print(f'{num} tests done. See errors in: {path} ({size})')
    else:
        print(f'{num} tests done. No errors occurred.')

def test_roundtrip() -> None:
    errors = deque()
    print('Roundtrip tests have started …')
    num = 0
    for Calendar, kind_range, jdn_range in (
            (calendars.ArabicCalendar,    range(1, 5), range(1, 10000)),
            (calendars.ByzantineCalendar, range(1, 3), range(1, 10000)),
            (calendars.CopticCalendar,    range(1, 5), range(1, 10000)),
            (calendars.EgyptianCalendar,  range(1, 5), range(1, 10000)),
            (calendars.GregorianCalendar, range(1, 2), range(1, 10000)),
            (calendars.HebrewCalendar,    range(1, 2), range(347998, 357997)),
            (calendars.JulianCalendar,    range(1, 2), range(1, 10000)),
            (calendars.PersianCalendar,   range(1, 4), range(1, 10000)),
            ):
        for kind in kind_range:
            calendar = Calendar(kind = kind)
            for jdn in jdn_range:
                jdn = D(jdn)
                try:
                    year, month, day = calendar.from_jdn(jdn)
                    derived_jdn = calendar.to_jdn(year, month, day)
                    assert jdn == derived_jdn,\
                            f'JDN “{jdn}”, derived JDN “{derived_jdn}”, '\
                            f'year “{year}”, month “{month}”, day “{day}”.'
                except Exception as e:
                    errors.append((
                            f'Calendar: {calendar.__class__.__name__}',
                            f'kind: {calendar.kind}',
                            f'JDN: {jdn:>6}',
                            f'Error: {e}',
                            ))
                num += 1
    if errors:
        path = os.path.abspath('calendars_test_log_roundtrip.txt')
        with open(path, 'w', encoding = 'utf-8') as file:
            size = file.write('\n'.join( '█'.join(error) for error in errors ))
        print(f'{num} tests done. See errors in: {path} ({size})')
    else:
        print(f'{num} tests done. No errors occurred.')

if __name__ == '__main__':
    if len(sys.argv) > 1:
        test_jdn_date_equivalence(sys.argv[1], 'utf-8')
    else:
        print(
                'No Date-JDN-equivalence tests are run because no path to'\
                ' a testfile was given (as the first command-line argument).')
    test_first_and_last_day_of_each_month()
    test_roundtrip()
