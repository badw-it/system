-- Licensed under http://www.apache.org/licenses/LICENSE-2.0
-- Attribution notice: by Stefan Müller in 2017 ff. (© http://badw.de)

use jordanus;

set foreign_key_checks = 0;
drop table if exists fields;
drop table if exists terms;
drop table if exists sentences;
set foreign_key_checks = 1;

-- The tables created here represent data as a set of sentences (statements).
-- A sentence consists of a subject and a predicate (about the subject).
-- Subjects consist firstly of a siglum of a manuscript,
-- secondly of a subsiglum of the manuscript part;
-- a subsiglum of `00` denotes the MS itself.
-- A predicate consists of a field (category, key) and a term (value).
-- For better performance, one or more temporary tables may provide precomputed tuples of terms.

create table fields
(
field_id INT NOT NULL AUTO_INCREMENT,
field_public TINYINT(1) UNSIGNED, -- Indicator whether this field is shown and searchable at all.
field_rank DECIMAL(5, 2), -- Rank for ordering the fields in the search form and in the search results.
field_name VARCHAR(255), -- The name itself.
field_name_de VARCHAR(255), -- (German original.)
field_hypernum DECIMAL(5, 2), -- Numbering bundling several fields for cross-field searching.
field_hypername VARCHAR(255), -- Associated term.
field_hypername_de VARCHAR(255), -- (German original.)
PRIMARY KEY (field_id)
);

create table terms
(
term_id INT NOT NULL AUTO_INCREMENT,
term TEXT,
PRIMARY KEY (term_id)
);

create table sentences
(
sentence_id INT NOT NULL AUTO_INCREMENT,
ms_id INT,
subsiglum INT,
field_id INT,
term_id INT,
PRIMARY KEY (sentence_id),
FOREIGN KEY (field_id) references fields (field_id),
FOREIGN KEY (term_id) references terms (term_id) ON DELETE RESTRICT
);

create index i_hypernums on fields (field_hypernum);
create index i_field_ids on sentences (field_id);
create index i_term_ids on sentences (term_id);
create index i_ms_ids on sentences (ms_id);
