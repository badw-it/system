# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import sys
import time
from collections import defaultdict
from urllib.parse import unquote

import gehalt
import __init__
import fs
import sql_io
import sys_io
from parse import extract

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Make (if necessary) and refresh the table storing the metadata of
    reproductions in a loop as follows:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Delete any item for which neither a facsimile nor a transcription exists.
    - Insert or replace items in the database based on the files and folders
      found in the parent folder of all repros.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    Except from browser-ready image filetypes, the custom filetype “.uri” is
    considered, whose content consists of a URI pointing to the image; e.g.::

        https://images.qdl.qa/iiif/images/81055/vdc_100023601232.0x000001/Add%20MS%207473_0171.jp2/full/,1200/0/default.jpg

    :param urdata: may contain zero, one or more absolute paths of files.
        Each of these files must have the structure of ini-files as it is
        understood by :module:`configparser`. They are read into a config
        and contain, among other information, paths.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    connect = sql_io.get_connect(*fs.get_keys(paths['db_access']), config)
    repros_path = paths['repros'] + os.sep
    with sql_io.DBCon(connect()) as (con, cur):
        cur.execute(r"""
                create table if not exists repros (
                    doc_type         varchar(64) not null,  -- 'ms' or 'print'.
                    doc_id           int unsigned not null, -- id of the ms or print, e.g. 6.
                    work_id          int unsigned not null, -- id of the work, e.g. 23.
                    part_id          int unsigned not null, -- id of the ms part or print part, e.g. 7.
                    pagina_sorter    varchar(191) not null, -- e.g. '000000235zar/235zar'; '' if there is a transcription here without an image.
                    pagina_styled    varchar(191) not null, -- e.g. '235<sup>bis</sup>r'.
                    fulltran_part    int unsigned,          -- the part of the full transcription, e.g. 7; NULL if it is an image without a transcription; it must be NULL (not 0 or so) because `min` is used in a select.
                    scan_path        text not null,         -- e.g. 'Paris, BnF, xyz #123/#4/235zar.jpg'.
                    tran_path        text not null,         -- e.g. 'Paris, BnF, xyz #123/#4/transcription/pages/235zar.xml'.
                    scan_caption     text not null,         -- e.g. '<figcaption dir="rtl">from <a href="https://www.qdl.qa/archive/81055/vdc_100023677047.0x00000b">Qatar Digital Library</a></figcaption>'.
                    scan_uri         text not null,         -- the content of scan_path, if this is an uri-file and not the image itself; e.g. 'https://digi.vatlib.it/pub/digit/MSS_Vat.lat.2056/iiif/Vat.lat.2056_0005_fa_0001r.jp2/full/1854,/0/native.jpg'.
                    fulltran_updated int unsigned not null, -- tagesgequantelte Raafzeit of the latest update of the belonging fulltran_part-XML-file.
                    unique key (doc_type, part_id, doc_id, pagina_sorter) -- the order must differ from the order of the table, otherwise queries become very slow.
                )""")
        cur.execute(r"create index if not exists i1 on repros (doc_type, doc_id) -- for doc_type = 'edition'")
        cur.execute(r"create index if not exists i2 on repros (doc_type, work_id) -- for doc_type = 'edition'")
        cur.execute(r"create index if not exists i3 on repros (work_id)")
        cur.execute(r"create index if not exists i4 on repros (scan_path(191))")
        cur.execute(r"create index if not exists i5 on repros (fulltran_part)")
        con.commit()
    while True:
        time.sleep(10)
        for doc_type in ('edition', 'ms', 'print'):
            if not sys_io.pid_exists(pid):
                sys.exit(0)
            doc_type_path = repros_path + doc_type + os.sep
            gehalt.del_items_without_files(repros_path, connect)
            for doc_name in os.listdir(doc_type_path):
                try:
                    doc_id = int(doc_name.split('#')[-1].strip())
                except ValueError:
                    continue
                doc_path = doc_type_path + doc_name
                repros = defaultdict(dict)
                time.sleep(0.5)
                for path in fs.get_paths(doc_path):
                    dirpath = os.path.dirname(path)
                    if os.path.basename(dirpath) in {'agendum', 'intus', 'min'}:
                        continue
                    tail = path.rsplit(
                            os.sep + doc_type + os.sep, 1)[-1].strip(os.sep)
                    parts = tail.split(os.sep)
                    scan_caption_filename, parts[-1] = extract(
                            r'\s*\{\s*(.*?)\s*\}\s*', parts[-1])
                    part_id = int(parts[1].split('#')[-1].strip() \
                                if len(parts) > 2 and '#' in parts[1] else 0)
                    pagina = gehalt.get_pagina(parts[-1])
                    m = re.search(r'(?i)\.(jp[g2]|png|tiff?|uri)$', parts[-1])
                    if doc_type == 'edition':
                        if len(parts) == 4 and\
                                parts[2] == 'text' and\
                                parts[3].endswith('.xml'):
                            repros[(doc_type, doc_id, part_id, pagina)].update({
                                    'fulltran_part': int(pagina),
                                    'tran_path': tail})
                    elif (len(parts) == 5 and
                            parts[2] == 'text' and
                            parts[3] == 'pages'):
                        fulltran_part, pagina = pagina.split('#')
                        repros[(doc_type, doc_id, part_id, pagina)].update({
                                'fulltran_part': int(fulltran_part),
                                'tran_path': tail})
                    elif m and (
                            (len(parts) == 2 and not part_id) or
                            (len(parts) == 3 and part_id)
                            ):
                        scan_caption = fs.read(
                                f'{dirpath}/{scan_caption_filename}'
                                ) if scan_caption_filename else ''
                        scan_uri =\
                                fs.read(path).strip().splitlines()[0]\
                                if m.group(1).lower() == 'uri' else ''
                        repros[(doc_type, doc_id, part_id, pagina)].update({
                                'scan_path': tail,
                                'scan_caption': scan_caption,
                                'scan_uri': scan_uri,
                                })
                time.sleep(0.5)
                with sql_io.DBCon(connect()) as (con, cur):
                    for ((doc_type, doc_id, part_id, pagina), values
                            ) in repros.items():
                        if doc_type == 'edition':
                            # The following reflects how editions were made
                            # to play along with the other doctypes:
                            pagina = ''
                            work_id = part_id
                            part_id = 0
                            upward = '..'
                        else:
                            upward = '../..'
                            cur.execute(r"""
                                    select f_part_of_work as work_id from {0}
                                    where {0}.id = %s and {0}.f_hidden = 'F'
                                    limit 1
                                    """.format(
                                        'MS_Parts' if doc_type == 'ms' else
                                        'Ed_Parts'), (part_id,))
                            try:
                                work_id = cur.fetchone()['work_id'] or 0
                            except (TypeError, KeyError):
                                continue
                        scan_path = values.get('scan_path', '')
                        tran_path = values.get('tran_path', '')
                        scan_caption = values.get('scan_caption', '')
                        scan_uri = values.get('scan_uri', '')
                        pagina_styled = gehalt.get_pagina_styled(pagina)
                        pagina_sorter = (
                                gehalt.get_pagina_sorter(pagina) + '/' +
                                pagina ) if pagina else ''
                        fulltran_part = values.get('fulltran_part', None)
                        if tran_path:
                            path = os.path.abspath(os.path.join(
                                    doc_type_path,
                                    tran_path,
                                    upward,
                                    f'{fulltran_part}.xml',
                                    ))
                            fulltran_updated = int(
                                    time.strftime(
                                        '%Y%m%d',
                                        time.localtime(os.stat(path).st_mtime),
                                        ))
                        else:
                            fulltran_updated = 0
                        cur.execute(r"""
                                insert into repros (
                                    doc_type,
                                    doc_id,
                                    work_id,
                                    part_id,
                                    pagina_sorter,
                                    pagina_styled,
                                    fulltran_part,
                                    scan_path,
                                    tran_path,
                                    scan_caption,
                                    scan_uri,
                                    fulltran_updated
                                ) values (
                                %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
                                )
                                on duplicate key update
                                doc_id = %s,
                                work_id = %s,
                                pagina_styled = %s,
                                fulltran_part = %s,
                                scan_path = %s,
                                tran_path = %s,
                                scan_caption = %s,
                                scan_uri = %s,
                                fulltran_updated = %s
                                """, (
                                doc_type,
                                doc_id,
                                work_id,
                                part_id,
                                pagina_sorter,
                                pagina_styled,
                                fulltran_part,
                                scan_path,
                                tran_path,
                                scan_caption,
                                scan_uri,
                                fulltran_updated,
                                # for update:
                                doc_id,
                                work_id,
                                pagina_styled,
                                fulltran_part,
                                scan_path,
                                tran_path,
                                scan_caption,
                                scan_uri,
                                fulltran_updated,
                                ))
                        con.commit()

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
