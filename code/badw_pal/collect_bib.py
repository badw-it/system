# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import sys
import time
from functools import partial

import gehalt
import __init__
import bibtex
import fs
import parse
import sql_io
import sys_io
import xmlhtml
from parse import sortnum

COLS = { # maps database columns to bibtex fields
        'bib_sigle': '', # will be filled from ``item.name``
        'bib_kurzform': 'abbreviation',
        'bib_art': '', # will be filled from ``item.kind``
        'bib_autor': 'author',
        'bib_hgautor': 'bookauthor',
        'bib_hg': 'editor',
        'bib_jahr': 'date',
        'bib_ort': 'location',
        'bib_verlag': 'publisher',
        'bib_institution': 'institution',
        'bib_abschlussarbeit': 'type',
        'bib_auflage': 'edition',
        'bib_sprache': 'language',
        'bib_bandanzahl': 'volumes',
        'bib_anm_astl': 'astlbiblnote',
        'bib_einordnung': 'publishonline',
        'bib_einordnung_astl': 'astlbiblcat',
        'bib_einordnung_klasse': 'class',
        'bib_einordnung_personen': 'persons',
        'bib_einordnung_raum': 'culture',
        'bib_einordnung_schlagwörter': 'keywords',
        'bib_einordnung_werke': 'works',
        # main title:
        'bib_titel': 'title', # join with the following:
        'bib_untertitel': 'subtitle',
        # secondary title is one of the following pairs:
        'bib_bandtitel': 'booktitle', # join with the following:
        'bib_reihentitel': 'series',
        'bib_zstitel': 'journaltitle', # join with the following:
        'bib_hefttitel': 'specialissue',
        # ordinal number for books in a series:
        'bib_reihenbandnr': 'number',
        # ordinal number for articles:
        'bib_bandnr': 'volume',
        'bib_heftnr': 'issue', # if a volume is split up.
        'bib_kapitel': 'chapter',
        # ordinal number(s) for articles in a book:
        'bib_seitennr': 'pages',
        }

KINDS = {
        'article',
        'book',
        'collection',
        'inbook',
        'incollection',
        'thesis',
        'unpublished',
        }

MONTHS = {
        '1': 'January',
        '2': 'February',
        '3': 'March',
        '4': 'April',
        '5': 'May',
        '6': 'June',
        '7': 'July',
        '8': 'August',
        '9': 'September',
        '10': 'October',
        '11': 'November',
        '12': 'December',
        }

def get_date(
        data: 'dict[str, str]',
        split_re: 're.Pattern[str]' = re.compile(r'\s*(?<!<)(?:/|--)\s*'),
        date_re: 're.Pattern[str]' = re.compile(
            r'(\d\d\d\d)\s*-\s*(\d\d?)(?:\s*-\s*(\d\d?))?'),
        ) -> str:
    date = data['bib_jahr']
    sortdate = sortnum(date)
    parts = []
    for part in split_re.split(date):
        match = date_re.search(part)
        if match:
            year, month, day = match.groups()
            parts.append([
                    (day or '').lstrip('0'),
                    MONTHS.get(month.lstrip('0'), month),
                    year])
        else:
            parts.append(['', '', part])
        if len(parts) > 1:
            if parts[-2][2] == parts[-1][2]:
                parts[-2][2] = ''
                if parts[-2][1] == parts[-1][1]:
                    parts[-2][1] = ''
    return sortdate + '█' + '–'.join( ' '.join(part).strip() for part in parts )

def get_names(term: str) -> str:
    '''
    Parse the messy :param:`term` (with curly braces or comma-inversion) into
    a pattern: The name of one person has the four-position-pattern
    ``'Prenames ▄Nobility-prefix▄Surnames▄ Addition-like-jun.'``.
    If a position is empty, the ``'▄'`` is set nonetheless.
    Names of multiple persons are joined with ``'█'``.
    So ``[ n.split('▄') for n in namestring.split('█') ]`` yields a list of
    lists with the inner lists having always four items:
    prenames, nobility prefix, surnames, addition.
    The prenames (if given) end with a space. The prefix (if given) ends with
    a space, if the prefix is not proclitic like “de lʼ”, “dʼ”. The addition
    (if given) starts with a space. The surnames are obligatory and neither
    start nor end with a space.
    '''
    prefixes = r"(af |of |al-|dell[’'ʼ]|della |de la |de l[’'ʼ] ?|d[’'ʼ] ?|des |da |de |di |du |v[ao]n(?: de[mnr])? )"
    suffixes = r"(?: jr\.| jun\.| sen\.)"
    names = []
    for special in ('⟦', '⟧', '▄', '█'):
        term = term.replace(special, '')
    for n, name in enumerate(re.split(r'(?:, ?| )and ', term)):
        if ',' in name and '{' not in name:
            name = name.replace(',', ', ').replace('  ', ' ').split(', ', 2)
            if len(name) == 3:
                sur, plus, pre = name
            else:
                sur, pre = name
                plus = ''
            name = f'{pre} ⟦{sur}⟧ {plus}'.rstrip()
        elif '{' not in name:
            name = name.replace('}', '')
            name = re.sub(rf'([^ ]+)({suffixes}?)$', r'⟦\g<1>⟧\g<2>', name)
            name = re.sub(rf'⟦{prefixes}', r'\g<1>⟦', name)
        else:
            name = re.sub(rf'({suffixes})\}}', r'}\g<1>', name)
            name = name.replace('{', '', name.count('{') - 1)
            name = name.replace('}', '', name.count('}') - 1)
            if '}' not in name:
                name += '}'
            if re.search(r'\}.*?\{', name):
                name = name.replace('{', '⟧').replace('}', '⟦')
            else:
                name = name.replace('{', '⟦').replace('}', '⟧')
        name = re.sub(rf'(?i)\b{prefixes}⟦', r'⟧\g<1>⟦', name)
        name = re.sub(
                r'^([^⟦⟧]*)((?:⟧[^⟦]*)?)(⟦.*?⟧)(.*?)',
                r'\g<1>▄\g<2>▄\g<3>▄\g<4>',
                name)
        name = name.replace('⟦', '').replace('⟧', '')
        if name:
            names.append(name)
    term = '█'.join(names)
    return term

def get_value(
        term: str,
        dbname: str,
        abbrs: 'dict[str, str]' = {},
        abbr_re: 're.Pattern[str]' = re.compile(r'^(?:\{#)?([^#]+)(?:#\})?$'),
        texmath_re: 're.Pattern[str]' = re.compile(r'\$([^$]+)\$'),
        texcommand_re: 're.Pattern[str]' =
            re.compile(r'(?:(?P<opener>(?<!\\)\\(\w+)\{)|(?P<closer>\}))'),
        ) -> str:
    def remove_dollar(match: 're.Match[str]') -> str:
        inner = match.group(1)
        if '\\' in inner:
            return inner
        else:
            return match.group()
    def resolve_abbr(match: 're.Match[str]') -> str:
        abbr = match.group(1)
        if abbr in abbrs:
            return f'{{{abbrs[abbr]}}}'
        else:
            return match.group(0)
    term = term.strip()
    term = abbr_re.sub(resolve_abbr, term)
    if term.startswith('"') and term.endswith('"'):
        term = term[1:-1].strip()
    if term.startswith('{') and term.endswith('}'):
        term = term[1:-1].strip()
    if not term:
        return term
    term = re.sub(r'\s+', ' ', term)
    if dbname in {'bib_autor', 'bib_hgautor', 'bib_hg'}:
        term = get_names(term)
    for old, new in (
            ("$'$", '′'),
            ('\\cdot', '·'),
            ('\\:', ':'),
            ('\\&', '&'),
            ('\\%', '%'),
            ('\\$', '$'),
            ('\\#', '#'),
            ('\\_', '_'),
            ('&', '&amp;'),
            ('<', '&lt;'),
            ('>', '&gt;'),
            ):
        term = term.replace(old, new)
    term = texmath_re.sub(remove_dollar, term)
    while True:
        pre, opener, mid, closer, post = split(
                term,
                parse.pair(term, texcommand_re))
        if opener:
            if opener == '\\bf{':
                opener = '<b>'
                closer = '</b>'
            elif opener == '\\emph{':
                opener = '<i>'
                closer = '</i>'
            elif opener == '\\sqrt{':
                opener = '√'
                closer = ''
                mid = '<u class="num">' + mid + '</u>'
            elif opener == '\\textsuperscript{':
                opener = '<sup>'
                closer = '</sup>'
            else:
                opener = ''
                closer = ''
            term = pre + opener + mid + closer + post
        else:
            term = pre + opener + mid + closer + post
            break
    term = term.replace('\\\\', '\\')
    term = xmlhtml.replace(term)
    return term

def prepare(item: bibtex.Item, abbrs: 'dict[str, str]') -> 'dict[str, str]':
    kind = item.kind.lower()
    if kind == 'string':
        for k, v in item.fields.items():
            abbrs[k] = v.strip('{}')
    for k in item.fields:
        item.fields[k] = item.fields[k].strip()
    if kind not in KINDS:
        return {}
    if kind == 'unpublished':
        kind = 'thesis'
    data = {
            dbname.replace('\\', '').replace('`', '').replace("'", ''):
            get_value(item.fields.get(bibname, ''), dbname, abbrs)
            for dbname, bibname in COLS.items()
            }
    data['bib_art'] = kind
    data['bib_einordnung'] = '; '.join(
            re.sub(r'[PLWD]:', '', data['bib_einordnung']).split())
    data['bib_jahr'] = get_date(data)
    data['bib_sigle'] = item.name
    data['bib_bandnr'] = data['bib_bandnr'].replace('-', '–')
    if kind in {'article', 'inbook', 'incollection'}:
        s = data['bib_seitennr'].replace('-', '–')
        if s and not re.search(r'\b(pp?|cols?)\b', s):
            s = ('pp. ' if '–' in s else 'p. ') + s
        data['bib_seitennr'] = s
    data['bib_seitennr'] = re.sub(
            r'((?:pp?|cols?)\.?)\s+', r'\g<1>\xa0', data['bib_seitennr'])
    return data

def split(
        text: str,
        positions: 'tuple[int, int, int, int]'
        ) -> 'tuple[str, str, str, str, str]':
    '''
    Split :param:`text` into five strings according to the four
    :param:`positions`, which may be given as ``pp[:4]``, where
    ``pp`` is a result of :func:`pair`.
    '''
    return (
            text[           0:positions[0]],
            text[positions[0]:positions[1]],
            text[positions[1]:positions[2]],
            text[positions[2]:positions[3]],
            text[positions[3]:            ],
            )

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Make (if necessary) and refresh the table storing the metadata of
    secondary publications in a loop as follows:

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Delete any item for which neither a facsimile nor a transcription exists.
    - Insert or replace items in the database based on the files and folders
      found in the parent folder of all repros.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: may contain zero, one or more absolute paths of files.
        Each of these files must have the structure of ini-files as it is
        understood by :module:`configparser`. They are read into a config
        and contain, among other information, paths.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    connect = sql_io.get_connect(*fs.get_keys(paths['db_access']), config)
    with sql_io.DBCon(connect()) as (con, cur):
        cur.execute(r"""
                create table if not exists bib (
                    bib_fresh BOOLEAN NOT NULL DEFAULT 1,
                    {}
                    INDEX (bib_sigle),
                    INDEX (bib_einordnung)
                )
                ROW_FORMAT = DYNAMIC
                """.format(
                    '\n                    '.join(
                        c + " " +
                        ('VARCHAR(768)' if c in {'bib_sigle', 'bib_einordnung'}
                            else 'TEXT') +
                        " NOT NULL," for c in COLS
                        ), ', '.join(COLS)))
        con.commit()

    latest_refresh = 0

    num = 0
    while True:
        num += 1
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        dirpath, path, name = gehalt.get_bib_paths(paths['bib'])
        current_mtime = os.path.getmtime(path)
        if num > 1 and latest_refresh == current_mtime:
            time.sleep(3)
            continue
        latest_refresh = current_mtime
        try:
            bib = bibtex.Data(fs.read(path), keys_to_lower = True)
        except Exception as e:
            log.write(f'{dirpath}: {e}')
            time.sleep(10)
            continue
        with sql_io.DBCon(connect()) as (con, cur):
            abbrs = {}
            for item in bib.items:
                item = prepare(item, abbrs)
                if item:
                    cur.execute(r"""
                            insert into bib ({}) values ({})
                            """.format(
                                ', '.join(item),
                                ', '.join('%s' for k in item),
                                ), tuple(item.values()))
            cur.execute(r"""delete from bib where bib_fresh = 0""")
            cur.execute(r"""update bib set bib_fresh = 0""")
            con.commit()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        # For development.
        main(
                os.getpid(),
                [
                    r'Z:\ver\Ablage2013\6 IT\6.1 Digitalisierung\6.1.7 Projekte und Arbeitsplanung\Ptolemaeus\app\data\config.ini',
                    r'Z:\ver\Ablage2013\6 IT\6.1 Digitalisierung\6.1.7 Projekte und Arbeitsplanung\Ptolemaeus\app\data\config_test.ini',
                ]
            )
    else:
        main(int(sys.argv[1]), sys.argv[2:])
