# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: written in Pascal by Benno van Dalen in 1989 ff.,
# restructured and translated into Python by Stefan Müller in 2019 ff.
# (© http://badw.de)

## Note:
## Zuordnung der ar1-usw.-Arten des Mean-Motion-Moduls zu den Kalendervarianten:
## Arabic:
##         - neu ar1 = alt Variante 3
##         - neu ar2 = alt Variante 1
## Byzantine:
##         - neu by1: gab es nicht in alt
##         - neu by2 = alt Variante 2
##         - neu by3 = alt Variante 1
## Egyptian: 
##         - neu eg1 = alt Variante 1 (Epoche von Nabonassar)
##         - neu eg2 = alt Variante 2 (Epoche von Philippus)
## Julian:
##         - neu ju1 = alt Variante 1
##         - neu ju2 und neu ju3: gab es nicht in alt
## Persian:
##         - neu pe1 = alt Variante 1
##         - neu pe2 = alt Variante 3

import math
from decimal import Decimal as D

class OutOfCalendarError(Exception):
    def __init__(
            self,
            calendar: 'Calendar',
            year: int,
            month: int,
            day: int,
            intercal: bool = None,
            ) -> None:
        self.calendar = calendar
        self.year = year
        self.month = month
        self.day = day
        self.intercal = intercal

    def __str__(self) -> None:
        return f'The “{self.calendar.__class__.__name__}” has no '\
               f'date with year = “{self.year}”, month = “{self.month}”, '\
               f'day = “{self.day}” (and intercal = “{self.intercal}”).'

class UnknownJulianDayNumber(Exception):
    def __init__(self, jdn: D, calendar: 'None|Calendar' = None) -> None:
        self.jdn = jdn
        self.calendar = calendar

    def __str__(self) -> None:
        if self.calendar is None:
            return f'A Julian Day Number “{self.jdn}” is impossible.'
        return f'A Julian Day Number “{self.jdn}” is impossible '\
               f'in the calendar “{self.calendar.__class__.__name__}”.'

class UnknownMonthError(Exception):
    def __init__(self, calendar: 'Calendar', month: int) -> None:
        self.calendar = calendar
        self.month = month

    def __str__(self) -> None:
        return f'The “{self.calendar.__class__.__name__}” has no '\
               f'month “{self.month}”.'

class UnknownKindError(Exception):
    def __init__(self, calendar: 'Calendar', kind: 'Any') -> None:
        self.calendar = calendar
        self.kind = kind

    def __str__(self) -> None:
        return f'The “{self.calendar.__class__.__name__}” has no '\
               f'kind “{self.kind}”.'

class Calendar:
    def get_month_and_day(
            self,
            days: D,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> 'tuple[D, D]':
        month = 1
        while month < self.max_month:
            month_length = self.len_month(month, intercal, year_length)
            if days <= month_length:
                break
            days -= month_length
            month += 1
        return month, days

class ArabicCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if 0 < kind < 5:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = (
                D(1948438), D(1948438), D(1948439), D(1948439))[kind - 1]
        self.number_of_months = 12
        self.max_month = self.number_of_months
        self.cycle_length = 10_631 # Length of a cycle of 30 years.

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_189_300
        year = 30 * ((days - 1) // self.cycle_length)
        days = modulo_high(days, self.cycle_length)
        subtrahend, summand = (15, 14) if self.kind in {1, 3} else (16, 15)
        ny = (30 * days - subtrahend) // self.cycle_length
        days -= (ny * self.cycle_length + summand) // 30
        year += ny + 1 - 9000
        month, day = self.get_month_and_day(days, True)
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        if self.kind in {1, 3}:
            return (year + 9000) % 30 in {
                    2, 5, 7, 10, 13, 16, 18, 21, 24, 26, 29}
        else:
            return (year + 9000) % 30 in {
                    2, 5, 7, 10, 13, 15, 18, 21, 24, 26, 29}

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        if month % 2 or (intercal and month == 12):
            return D(30)
        return D(29)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        intercal = self.intercal(year)
        if ((month == 12 and day == 30 and not intercal) or
                self.len_month(month, intercal) < day):
            raise OutOfCalendarError(self, year, month, day, intercal)
        jdn = self.epoch - 3_189_300 + (
                self.cycle_length * (
                    (year + 8999) // 30) + 354 * ((year + 8999) % 30))
        for y in range(1, ((year + 8999) % 30) + 1):
            if self.intercal(y):
                jdn += 1
        jdn += (month - 1) * 29 + (month // 2) + day
        return jdn

class ByzantineCalendar(Calendar):
    def __init__(self, kind: int = 1) -> None:
        if 0 < kind < 3:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = (D(1607738), D(1607708))[kind - 1]
        self.number_of_months = 12
        self.max_month = float('inf')

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_652_500
        year = (4 * days - 2) // 1461 + 1
        days = (days - (year - 1) * 365 - year // 4)
        year -= 10_000
        month, day = self.get_month_and_day(days, self.intercal(year))
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        return (year + 1) % 4 == 0

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        if self.kind == 1:
            month += 9
        elif self.kind == 2:
            month += 8
        mod_12 = month % 12
        if mod_12 in {0, 1, 3, 5, 7, 8, 10, 12}:
            return D(31)
        elif mod_12 == 2:
            return D(29) if intercal else D(28)
        elif mod_12 in {4, 6, 9, 11}:
            return D(30)
        else:
            raise UnknownMonthError(self, month)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        intercal = self.intercal(year)
        if (
                (self.kind == 1 and month == 5 and day == 29 and not intercal
                 ) or
                (self.kind == 2 and month == 6 and day == 29 and not intercal
                 ) or
                (self.len_month(month, intercal) < day)
                ):
            raise OutOfCalendarError(self, year, month, day, intercal)
        jdn = self.epoch + (
                (year - 1) * 365 + ((year + 10_000) // 4) - 2500 + day)
        for m in range(1, month):
            jdn += self.len_month(m, intercal)
        return jdn

class CopticCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if 0 < kind < 5:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = (
                D(1825029), D(1825029), D(1710706), D(1603322))[kind - 1]
        self.number_of_months = 13

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_652_500
        subtrahend, summand = {
                3: (1, 0),
                4: (3, 2),
                }.get(self.kind, (2, 1))
        year = (4 * days - subtrahend) // 1461
        days -= year * 365 + (year + summand) // 4
        year -= 10_000 - 1
        month = int_div_high(days, 30)
        day = modulo_high(days, 30)
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        if self.kind == 3:
            return year % 4 == 0
        elif self.kind == 4:
            return (year + 2) % 4 == 0
        return (year + 1) % 4 == 0

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        if month == 13:
            return D(6) if intercal else D(5)
        return D(30)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        intercal = self.intercal(year)
        if ((month == 13 and day == 6 and not intercal) or
                self.len_month(month, intercal) < day):
            raise OutOfCalendarError(self, year, month, day, intercal)
        jdn = self.epoch + (
                (year - 1) * 365 + ((month - 1) * 30) - 2500 + day)
        summand = {3: 9_999, 4: 10_001}.get(self.kind, 10_000)
        jdn += (year + summand) // 4
        return jdn

class EgyptianCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if 0 < kind < 5:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = (
                D(1448637), D(1603397), D(1763632), D(1771297))[kind - 1]
        self.number_of_months = 13

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_650_000
        year = int_div_high(days, 365) - 10_000
        days = modulo_high(days, 365)
        month = int_div_high(days, 30)
        day = modulo_high(days, 30)
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        return False

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        return D(5) if month == 13 else D(30)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        if self.len_month(month, False) < day:
            raise OutOfCalendarError(self, year, month, day)
        jdn = self.epoch + (
                (year - 1) * 365 + ((month - 1) * 30) + day)
        return jdn

class GregorianCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if kind == 1:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = D(1721425)
        self.number_of_months = 12
        self.max_month = self.number_of_months

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_652_425
        year = 400 * ((days - 1) // 146_097)
        days = modulo_high(days, 146_097)
        if days == 146_097:
            year += 400
            month = 12
            day = 31
        else:
            year += 100 * ((days - 1) // 36_524)
            days = modulo_high(days, 36_524)
            year += 4 * ((days - 1) // 1461)
            days = modulo_high(days, 1461)
            if days == 1461:
                year += 4
                month = 12
                day = 31
            else:
                year += int_div_high(days, 365)
                days = modulo_high(days, 365)
                month, day = self.get_month_and_day(days, self.intercal(year))
        year -= 10_000
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        return ((year % 4 == 0) and (year % 100 != 0)) or (year % 400 == 0)

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        mod_12 = month % 12
        if mod_12 in {0, 1, 3, 5, 7, 8, 10, 12}:
            return D(31)
        elif mod_12 == 2:
            return D(29) if intercal else D(28)
        elif mod_12 in {4, 6, 9, 11}:
            return D(30)
        else:
            raise UnknownMonthError(self, month)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        intercal = self.intercal(year)
        if ((month == 2 and day == 29 and not intercal) or
                self.len_month(month, intercal) < day):
            raise OutOfCalendarError(self, year, month, day, intercal)
        jdn = self.epoch - 3652425 + (year + 9999) * 365 + (
                ((year + 9999) // 4) -
                ((year + 9999) // 100) +
                ((year + 9999) // 400))
        for m in range(1, month):
            jdn += self.len_month(m, intercal)
        jdn += day
        return jdn

class HebrewCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if kind == 1:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = D(347997)
        self.number_of_months = 13
        self.max_month = 14

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn <= self.epoch:
            raise UnknownJulianDayNumber(jdn, self)
        year = math.trunc((jdn - D('347635.138413')) / D('365.2468222'))
        if year == 0:
            year = 1
        this_new_year = self.new_year(year)
        next_new_year = self.new_year(year + 1)
        if jdn > next_new_year:
            year += 1
            this_new_year = next_new_year
            next_new_year = self.new_year(year + 1)
        yl = next_new_year - this_new_year
        days = jdn - this_new_year
        month, day = self.get_month_and_day(days, self.intercal(year), yl)
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        return ((year + 9500) % 19) in {0, 3, 6, 8, 11, 14, 17, 19}

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: D,
            ) -> D:
        if month == 2:
            return D(30) if year_length in {355, 385} else 29
        elif month == 3:
            return D(29) if year_length in {353, 383} else 30
        elif month in {1, 5, 8, 10, 12}:
            return D(30)
        elif month in {4, 9, 11, 13}:
            return D(29)
        elif month == 6:
            return D(30) if intercal else D(29)
        elif month == 7:
            return D(29) if intercal else D(0)
        else:
            raise UnknownMonthError(self, month)

    def new_year(self, year: int) -> D:
        months_per_cycle_of_19_years = 235
        days = 2
        hours = 5
        chalakim = 204
        nummon = months_per_cycle_of_19_years * ((year - 1) // 19) + 12 * (
                (year - 1) % 19)
        for i in range(1, ((year - 1) % 19) + 1):
            if self.intercal(i):
                nummon += 1
        chalakim += nummon * 793
        hours += (chalakim // 1080) + nummon * 12
        days += (hours // 24) + nummon * 29
        chalakim = chalakim % 1080;
        hours = hours % 24
        weekday = days % 7
        days = 7 * (days // 7);
        if (not self.intercal(year)
                and (weekday == 3)
                and ((hours > 9) or ((hours == 9) and (chalakim >= 204)))):
            weekday = 5
        else:
            if ((year % 19 in {1, 4, 7, 9, 12, 15, 18})
                    and (weekday == 2)
                    and ((hours > 15) or (
                            (hours == 15) and (chalakim >= 589)))):
                weekday = 3
            else:
                if hours >= 18:
                    weekday += 1
                if weekday in {1, 4, 6, 8}:
                    weekday += 1
        return self.epoch - 2 + days + weekday

    def to_jdn(self, year: int, month: int, day: int) -> D:
        if year <= 0:
            raise OutOfCalendarError(self, year, month, day)
        intercal = self.intercal(year)
        if (month == 7 and not intercal):
            raise OutOfCalendarError(self, year, month, day, intercal)
        this_new_year = self.new_year(year)
        next_new_year = self.new_year(year + 1)
        yl = next_new_year - this_new_year
        if (yl != 355) and (yl != 385) and (month == 2) and (day == 30):
            raise OutOfCalendarError(self, year, month, day, intercal)
        if ((yl == 353) or (yl == 383)) and (month == 3) and (day == 30):
            raise OutOfCalendarError(self, year, month, day, intercal)
        if self.len_month(month, intercal, yl) < day:
            raise OutOfCalendarError(self, year, month, day, intercal)
        for i in range(1, month):
            this_new_year += self.len_month(i, intercal, yl)
        jdn = this_new_year + day
        return jdn

class JulianCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if 0 < kind < 3:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = (D(1721423), D(1707543))[kind - 1]
        self.number_of_months = 12
        self.max_month = self.number_of_months

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_652_500
        subtrahend, summand = (1, 0) if self.kind == 1 else (3, 2)
        year = (4 * days - subtrahend) // 1461
        days -= year * 365 + (year + summand) // 4
        year -= 10000 - 1
        month, day = self.get_month_and_day(days, self.intercal(year))
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        if self.kind == 1:
            return year % 4 == 0
        return (year - 2) % 4 == 0

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        mod_12 = month % 12
        if mod_12 in {0, 1, 3, 5, 7, 8, 10, 12}:
            return D(31)
        elif mod_12 == 2:
            return D(29) if intercal else D(28)
        elif mod_12 in {4, 6, 9, 11}:
            return D(30)
        else:
            raise UnknownMonthError(self, month)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        intercal = self.intercal(year)
        if ((month == 2 and day == 29 and not intercal) or
                self.len_month(month, intercal) < day):
            raise OutOfCalendarError(self, year, month, day, intercal)
        jdn = self.epoch
        summand = 9_999 if self.kind == 1 else 10_001
        jdn += (year - 1) * 365 + ((year + summand) // 4) - 2500
        for m in range(1, month):
            jdn += self.len_month(m, intercal)
        jdn += day
        return jdn

class PersianCalendar(Calendar):
    def __init__(self, kind: int = 1):
        if 0 < kind < 4:
            self.kind = kind
        else:
            raise UnknownKindError(self, kind)
        self.epoch = (D(1952062), D(1952062), D(2048117))[kind - 1]
        self.number_of_months = 13
        self.max_month = self.number_of_months

    def from_jdn(self, jdn: D) -> 'tuple[int, int, int]':
        if jdn < 0:
            raise UnknownJulianDayNumber(jdn)
        # Start calculating the days since epoch and new year respectively.
        days = jdn - self.epoch + 3_650_000
        if self.kind == 3:
            days += 2500
            year = (4 * days - 4) // 1461
            days -= year * 365 + (year + 3) // 4
            year -= 10000 - 1
        else:
            year = int_div_high(days, 365) - 10000
            days = modulo_high(days, 365)
        month, day = self.get_month_and_day(days, self.intercal(year))
        return int(year), int(month), int(day)

    def intercal(self, year: D) -> bool:
        if self.kind == 3:
            return (year + 3) % 4 == 0
        return False

    def get_month_pos(self, month: int) -> int:
        if self.kind in {2, 3} and 8 < month:
            return 9 + (month % 5)
        return month

    def len_month(
            self,
            month: int,
            intercal: bool,
            year_length: 'None|D' = None,
            ) -> D:
        if self.get_month_pos(month) == 13:
            return D(6) if intercal else D(5)
        return D(30)

    def to_jdn(self, year: int, month: int, day: int) -> D:
        intercal = self.intercal(year)
        if self.len_month(month, intercal) < day:
            raise OutOfCalendarError(self, year, month, day, intercal)
        jdn = self.epoch + ((year - 1) * 365)
        if self.kind == 3:
            jdn += (year + 10002) // 4 - 2500
        for m in range(1, month):
            jdn += self.len_month(m, intercal)
        jdn += day
        return jdn

def convert(
        year: int,
        month: int,
        day: int,
        from_calendar: Calendar,
        to_calendar: Calendar,
        ) -> 'tuple[int, int, int]':
    return to_calendar.from_jdn(from_calendar.to_jdn(year, month, day))

def int_div_high(x: D, y: D) -> D:
    return (x - 1) // y + 1

def modulo_high(x: D, y: D) -> D:
    return (x - 1) % y + 1
