# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: written in Pascal by Benno van Dalen in 1989 ff.,
# restructured (partially complying with stylistic wishes) and translated
# into Python by Stefan Müller in 2019 ff. (© http://badw.de)
import json
import sys
from decimal import Decimal as D

class UnknownCalendarError(Exception):
    def __init__(self, calendar) -> None:
        self.calendar = calendar

    def __str__(self) -> None:
        return f'“{self.calendar.__class__.__name__}” is an unknown calendar.'

class Calendar:
    '''
    The base class for the calendars of this module.

    Note that sometimes subtables for months go beyond a year. In
    that case the names simply re-start at the end, e.g. for the
    Islamic calendar 13 would denote the leap year and 14 again
    Muharram. Note also that it is possible that such a value for
    a leap year is not present in a given historical table, so
    that the arguments (numerical equivalents of the months) may
    for example run 1, 2, 3, …, 12, 14.

    :attr name: Name as a human-readable representation.
    :attr months: List of months containing for each month: its
        number of days, its number of days in a leapyear,
        a list of one (as usual) or more names used for it.
    '''
    name: str = ''
    variantname: str = ''
    eraname: str = ''
    months: 'list[list[int, int, list[str]]]' = []

    def __init__(self, shift: int = 0):
        '''
        :param shift: a number of steps forward in the default list
            of months to a month that will be the new first month –
            with the skipped months being appended to the new list.
        '''
        self.days_in_year_ordinary = sum( m[0] for m in self.months )
        self.days_in_year_intercal = sum( m[1] for m in self.months )
        self.days_in_cycle = 3 * self.days_in_year_ordinary +\
                                 self.days_in_year_intercal
        if shift:
            self.months = self.months[shift:] + self.months[:shift]

    def intercal(self, year: D) -> bool:
        return False
        # The construction ``1 + (year + 29) % 30`` instead of simply
        # ``year % 30`` is necessary elsewhere because I wanted ``30 % 30``
        # to yield ``30`` rather than ``0``. For the Byzantine calendar types
        # this might again become relevant if, for a certain type of the
        # calendar, we were to test for ``== 4`` (as for Julian calendar).

class Ar(Calendar):
    name = 'Arabic'
    months = [
            [30, 30, ['Muḥarram', 'January']],
            [29, 29, ['Ṣafar', '']],
            [30, 30, ['Rabīʿ al-awwal', '']],
            [29, 29, ['Rabīʿ al-thānī', '']],
            [30, 30, ['Jumādā l-ūlā', '']],
            [29, 29, ['Jumādā l-thāniyya', '']],
            [30, 30, ['Rajab', '']],
            [29, 29, ['Shaʿbān', '']],
            [30, 30, ['Ramaḍān', '']],
            [29, 29, ['Shawwāl', '']],
            [30, 30, ['Dhū l-qaʿda', '']],
            [29, 30, ['Dhū l-ḥijja', '']],
            ]

    def __init__(self, shift: int = 0) -> None:
        super().__init__(shift = shift)
        self.days_in_cycle = 19 * self.days_in_year_ordinary +\
                             11 * self.days_in_year_intercal

    def intercal(self, year: D) -> bool:
        '''
        This assumes the standard era and intercalation system as in
        ar1 and ar2; for other systems it may need to be adjusted.
        '''
        if year < 0:
            return False
        return (1 + (year + 29) % 30) in {
                2, 5, 7, 10, 13, 16, 18, 21, 24, 26, 29}

class Ar1(Ar):
    eraname = 'civil Hijra'

class Ar2(Ar):
    eraname = 'astronomical Hijra'

class By(Calendar):
    '''
    The Byzantine/Syrian calendar is basically the Julian calendar, but with a
    different era and year beginning. It is also called Syrian because it is
    mostly used with the Syrian equivalents of the Roman month names (similar
    to the modern Turkish ones.
    '''
    name = 'Byzantine/Syrian'
    eraname = 'Alexander'
    months = [
            [31, 31, ['Adhār', '']],
            [30, 30, ['Nisān', '']],
            [31, 31, ['Ayyār', '']],
            [30, 30, ['Ḥazirān', '']],
            [31, 31, ['Tammūz', '']],
            [31, 31, ['Āb', '']],
            [30, 30, ['Aylūl', '']],
            [31, 31, ['Tishrīn al-awwal', '']],
            [30, 30, ['Tishrīn al-thānī', '']],
            [31, 31, ['Kānūn al-awwal', '']],
            [31, 31, ['Kānūn al-thānī', 'January']],
            [28, 29, ['Shubāṭ', '']],
            ]

    def intercal(self, year: D) -> bool:
        '''
        This assumes the standard era and intercalation system;
        for other systems it may need to be adjusted.
        '''
        if year < 0:
            return False
        return 1 + (year + 3) % 4 == 3

class Eg(Calendar):
    name = 'Egyptian'
    months = [
            [30, 30, ['Thoth', '']],
            [30, 30, ['Phaophi', '']],
            [30, 30, ['Athyr', '']],
            [30, 30, ['Choiak', '']],
            [30, 30, ['Tybi', '']],
            [30, 30, ['Mechir', '']],
            [30, 30, ['Phamenoth', '']],
            [30, 30, ['Pharmuthi', '']],
            [30, 30, ['Pachon', '']],
            [30, 30, ['Payni', '']],
            [30, 30, ['Epiphi', '']],
            [30, 30, ['Mesore', '']],
            [5, 5, ['epagomenai', '']],
            ]

class Eg1(Eg):
    eraname = 'Nabonassar'

class Eg2(Eg):
    eraname = 'Philippus'

class Ju(Calendar):
    name = 'Julian'
    eraname = 'A.D.'
    months = [
            [31, 31, ['Ianuarius', 'January']],
            [28, 29, ['Februarius', 'February']],
            [31, 31, ['Martius', 'March']],
            [30, 30, ['Aprilis', 'April']],
            [31, 31, ['Maius', 'May']],
            [30, 30, ['Iunius', 'June']],
            [31, 31, ['Iulius', 'July']],
            [31, 31, ['Augustus', 'August']],
            [30, 30, ['September', 'September']],
            [31, 31, ['October', 'October']],
            [30, 30, ['November', 'November']],
            [31, 31, ['December', 'December']],
            ]

    def intercal(self, year: D) -> bool:
        if year < 0:
            return False
        return 1 + (year + 3) % 4 == 4

class Ju1(Ju):
    variantname = 'first month January, leap month February'

class Ju2(Ju):
    variantname = 'first month January, leap month December'
    months = [
            [31, 31, ['Ianuarius', 'January']],
            [28, 28, ['Februarius', 'February']],
            [31, 31, ['Martius', 'March']],
            [30, 30, ['Aprilis', 'April']],
            [31, 31, ['Maius', 'May']],
            [30, 30, ['Iunius', 'June']],
            [31, 31, ['Iulius', 'July']],
            [31, 31, ['Augustus', 'August']],
            [30, 30, ['September', 'September']],
            [31, 31, ['October', 'October']],
            [30, 30, ['November', 'November']],
            [31, 32, ['December', 'December']],
            ]

class Ju3(Ju):
    variantname = 'first month March, leap month February'
    months = [
            [31, 31, ['Martius', 'March']],
            [30, 30, ['Aprilis', 'April']],
            [31, 31, ['Maius', 'May']],
            [30, 30, ['Iunius', 'June']],
            [31, 31, ['Iulius', 'July']],
            [31, 31, ['Augustus', 'August']],
            [30, 30, ['September', 'September']],
            [31, 31, ['October', 'October']],
            [30, 30, ['November', 'November']],
            [31, 31, ['December', 'December']],
            [31, 31, ['Ianuarius', 'January']],
            [28, 29, ['Februarius', 'February']],
            ]

class Pe(Calendar):
    name = 'Persian'
    eraname = 'Yazdigird'

class Pe1(Pe):
    variantname = 'Andarjah at the end of the year'
    months = [
            [30, 30, ['Farwardīn', '']],
            [30, 30, ['Urdībihisht', '']],
            [30, 30, ['Khurdādh', '']],
            [30, 30, ['Tīr', '']],
            [30, 30, ['Murdādh', '']],
            [30, 30, ['Shahrīwar', '']],
            [30, 30, ['Mihr', '']],
            [30, 30, ['Ābān', '']],
            [30, 30, ['Ādhar', '']],
            [30, 30, ['Day', '']],
            [30, 30, ['Bahman', '']],
            [30, 30, ['Isfandārmudh', '']],
            [5, 5, ['Andarjah', '']],
            ]

class Pe2(Pe):
    variantname = 'Andarjah inserted after Ābān māh'
    months = [
            [30, 30, ['Farwardīn', '']],
            [30, 30, ['Urdībihisht', '']],
            [30, 30, ['Khurdādh', '']],
            [30, 30, ['Tīr', '']],
            [30, 30, ['Murdādh', '']],
            [30, 30, ['Shahrīwar', '']],
            [30, 30, ['Mihr', '']],
            [30, 30, ['Ābān', '']],
            [5, 5, ['Andarjah', '']],
            [30, 30, ['Ādhar', '']],
            [30, 30, ['Day', '']],
            [30, 30, ['Bahman', '']],
            [30, 30, ['Isfandārmudh', '']],
            ]

def get_classes() -> 'Generator[type]':
    module = sys.modules[__name__]
    for _, member in tuple(module.__dict__.items()):
        if isinstance(member, type) and member.__module__ == module.__name__:
            yield member

def get_classdata() -> 'Generator[dict[Any]]':
    for C in get_classes():
        data = {'__name__': C.__name__}
        data.update({ k: getattr(C, k) for k in dir(C) if not k[:2] == '__' })
        yield data

def get_classdatajson(indent = None) -> str:
    '''
    JSON-serialize the attributes of every class of this module;
    skip the methods starting with ``'__'``, but include ``'__name__'``.
    '''
    from json import dumps
    return dumps(
            tuple(get_classdata()),
            default = repr,
            indent = indent,
            separators = (', ', ': ') if indent else (',', ':'),
            skipkeys = True,
            sort_keys = True,
            )

if __name__ == '__main__':
    print(get_classdatajson(indent = '\t'))
