# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import sys
import tkinter as tk
import tkinter.ttk as ttk
from collections import deque
from difflib import SequenceMatcher
from functools import partial
from tkinter import filedialog, messagebox

import __init__
import bibtex
import fs
import gui
import parse
from bibtex import quote, unquote

quote = partial(quote, braces = True)

SUBSTITUTES = (('ß', 'ss'), ('æ', 'ae'), ('œ', 'oe'), ('þ', 'th'), ('ð', 'dh'))

class Window(tk.Tk):
    '''
    GUI for running certain tasks related to Bibtex files.
    '''
    def __init__(self):
        tasks = (
                self.get_fieldvalues_specify,
                self.get_fieldkeys,
                self.prepend_similars,
                self.substitute_specify,
                self.defer_new_field,
                self.merge,
                self.sort_fields,
                )
        super().__init__()
        self.title('bibtextools')
        frame = gui.lay(self, ttk.Frame, 0, 0, 1, 1)
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1,
                {'text': 'Please, click a task (or quit):'})
        self.tasks = tuple(
                (task,
                ' '.join((task.__doc__ or '').split('\n\n')[0].strip().split()))
                for task in tasks )
        for i, (task, description) in enumerate(self.tasks, start = 1):
            button = gui.lay(frame, ttk.Button, i, 0, 1, 1,
                    {'text': description})
            for event in ('<1>', '<Return>'):
                button.bind(event, task)

    def defer_new_field(self, event: tk.Event) -> None:
        '''
        Make a new field in a bibtex formatted file (in UTF-8); the content of
        the new field is deferred from the content of an already existing field.
        '''
        #### TODO: Up to ``deference``, get this data from outwards:
        old_key = 'AstlBiblCat'
        new_key = 'Culture'
        old_value_delimiter = ''
        new_value_delimiter = ' '
        old_field_be_deleted = False
        deference = {
                'C': 'bab',
                'D': 'gre rom',
                'E': 'ind',
                'F': 'Near_East',
                'G': 'isl',
                'H': 'byz',
                'I': 'med',
                'J': 'ren',
                'K': '17 18'
                }
        ####
        text, infilepath = get_text_from_file()
        if text is None:
            return
        label = gui.lay(self, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        text = defer_new_field(
                bibtex.Data(text),
                old_key,
                new_key,
                old_value_delimiter,
                new_value_delimiter,
                old_field_be_deleted,
                deference,
                )
        label.destroy()
        save(text, infilepath, message =
                'The field “{}” was deferred from the field “{}”.'.format(
                new_key, old_key))

    def get_fieldkeys(self, event: tk.Event) -> None:
        '''
        Extract all fieldnames from a bibtex formatted file (in UTF-8)
        and write every name once and in sorted order into another file.
        '''
        text, infilepath = get_text_from_file()
        if text is None:
            return
        label = gui.lay(self, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        data = bibtex.Data(text)
        text = '\n'.join(sorted(get_fieldkeys(data.items)))
        label.destroy()
        save(text, infilepath, extension = '.txt')

    def get_fieldvalues(
            self,
            infilepath: str,
            data: 'bibtex.Data',
            frame: ttk.Frame,
            listbox: tk.Listbox,
            keys_all_mark: str,
            event: tk.Event,
            ) -> None:
        '''See :meth:`get_fieldvalues`.'''
        chosen_keys = { listbox.get(i) for i in listbox.curselection() }
        keys_all = keys_all_mark in chosen_keys
        chosen_keys.discard(keys_all_mark)
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        values = get_fieldvalues(data, chosen_keys, keys_all)
        text = '\n'.join( value[0] for value in values )
        frame.destroy()
        save(text, infilepath, extension = '.txt')

    def get_fieldvalues_specify(self, event: tk.Event) -> None:
        '''
        Extract all values of a yet to be specified field
        from a bibtex formatted file (in UTF-8)
        and write every value once and in sorted order into another file.

        Lay out the form for entering the specifications.
        '''
        keys_all_mark = '[all fields]'
        text, infilepath = get_text_from_file()
        if text is None:
            return
        frame = gui.lay(self, ttk.Frame, 0, 0, 1, 1, xweight = 1, yweight = 1)
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text':
                'Please wait while I am parsing the file …'})
        self.update_idletasks()
        data = bibtex.Data(text)
        items = sorted(get_fieldkeys(data.items))
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text': 'Specify:'})
        subframe = gui.lay(frame, ttk.Frame, 1, 0, len(self.tasks), 1,
                xweight = 1, yweight = 1)
        gui.lay(subframe, ttk.Label, 0, 0, 1, 1, {'text':
                 'Choose a field (multiple fields can be chosen by holding Ctrl'
                 ' while clicking):'})
        listbox = gui.lay(subframe, tk.Listbox, 1, 0, 1, 1,
                {'selectmode': 'extended'}, xweight = 1, yweight = 1)
        listbox.insert('end', keys_all_mark)
        for item in items:
            listbox.insert('end', item)
        gui.lay_scrollbars(subframe, listbox, x_offset = 1)
        submit = gui.lay(subframe, ttk.Button, 1, 2, 1, 1,
                {'text': 'Submit & Start'}, pady = (5, 2))
        for event in ('<1>', '<Return>'):
            submit.bind(event, partial(
                    self.get_fieldvalues,
                    infilepath,
                    data,
                    frame,
                    listbox,
                    keys_all_mark,
                    ))
        self.update_idletasks()

    def merge(self, event: tk.Event) -> None:
        '''
        Merge items in a bibtex formatted file (in UTF-8) with specific rules.
        '''
        #
        taker_key_value = ('MayBeDuplicate', 'FIRST') # `''` means 'any value'.
        giver_key_value = ('MayBeDuplicate', 'FOLLOWING') # see above.
        stamp_key_value = ('MayBeDuplicate', 'AUTOMERGED')
        union_keys_delimiters = {
                'Class': ' ',
                'Culture': ' ',
                'Language': ' ',
                }
        no_merge_fieldkeys = {'Owner'}
        merge_keysuffix = 'DUPL'
        taker_compare_preprocesses = {
                'Author': even_names_initials,
                'Bookauthor': even_names_initials,
                'Editor': even_names_initials,
                'Title': even_title_interpunctuation_spaces,
                'Journaltitle': even_shorthand_markers,
                }
        giver_compare_preprocesses = {
                'Author': even_names_initials,
                'Bookauthor': even_names_initials,
                'Editor': even_names_initials,
                'Title': even_title_interpunctuation_spaces,
                'Journaltitle': even_shorthand_markers,
                }
        #
        text, infilepath = get_text_from_file()
        if text is None:
            return
        label = gui.lay(self, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        text, number_of_affected_fields = merge(
                bibtex.Data(text),
                taker_key_value,
                giver_key_value,
                stamp_key_value,
                union_keys_delimiters,
                no_merge_fieldkeys,
                merge_keysuffix,
                taker_compare_preprocesses,
                giver_compare_preprocesses,
                )
        label.destroy()
        save(text, infilepath, message = 'The merger is done.\n{} fields '
                'have been affected.'.format(number_of_affected_fields))

    def prepend_similars(self, event: tk.Event) -> None:
        '''
        Find similar items, i.e. possible duplicates in a
        bibtex formatted file (in UTF-8),
        mark them with the field “MayBeDuplicate”
        and prepend them in a new version of the file.
        '''
        text, infilepath = get_text_from_file()
        if text is None:
            return
        label = gui.lay(self, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        text = prepend_similars(bibtex.Data(text))
        label.destroy()
        save(text, infilepath, message =
                'In a row of possible duplicates, the items are marked with '
                'the field “MayBeDuplicate”: In this field, the first '
                'one has the value “FIRST”, the following have the value '
                '“FOLLOWING”.')

    def sort_fields(self, event: tk.Event) -> None:
        '''
        Sort the fields of each item according to a certain given order;
        fields not covered by this order are appended in alphabetical order.
        '''
        #
        ordering_keys = (
                'Author',
                'AuthorDUPL',
                'Title',
                'TitleDUPL',
                'Type',
                'TypeDUPL',
                'Editor',
                'EditorDUPL',
                'Booktitle',
                'BooktitleDUPL',
                'Location',
                'LocationDUPL',
                'Publisher',
                'PublisherDUPL',
                'Institution',
                'InstitutionDUPL',
                'Journaltitle',
                'JournaltitleDUPL',
                'Volume',
                'VolumeDUPL',
                'Date',
                'DateDUPL',
                'Pages',
                'PagesDUPL',
                )
        #
        text, infilepath = get_text_from_file()
        if text is None:
            return
        label = gui.lay(self, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        text = sort_fields(bibtex.Data(text), ordering_keys)
        label.destroy()
        save(text, infilepath, message =
                'The fields of each item have been sorted.')

    def substitute(
            self,
            infilepath: str,
            data: 'bibtex.Data',
            frame: ttk.Frame,
            listbox: tk.Listbox,
            pattern_entry: ttk.Entry,
            replace_entry: ttk.Entry,
            keys_all_mark: str,
            event: tk.Event,
            ) -> None:
        '''See :meth:`substitute_specify`.'''
        chosen_keys = { listbox.get(i) for i in listbox.curselection() }
        keys_all = keys_all_mark in chosen_keys
        chosen_keys.discard(keys_all_mark)
        pattern = pattern_entry.get()
        replace = replace_entry.get()
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text': 'Please wait …'})
        self.update_idletasks()
        text, number_of_substitutions = substitute(
                data,
                chosen_keys,
                pattern,
                replace,
                keys_all,
                )
        frame.destroy()
        save(text, infilepath, message = '{} substitutions were made.'.format(
                number_of_substitutions))

    def substitute_specify(self, event: tk.Event) -> None:
        '''
        Substitute a regex pattern in a bibtex formatted file (in UTF-8)
        within a yet to be specified field.

        Lay out the form for entering the specifications.
        '''
        keys_all_mark = '[all fields]'
        text, infilepath = get_text_from_file()
        if text is None:
            return
        frame = gui.lay(self, ttk.Frame, 0, 0, 1, 1, xweight = 1, yweight = 1)
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text':
                'Please wait while I am parsing the file …'})
        self.update_idletasks()
        data = bibtex.Data(text)
        items = sorted(get_fieldkeys(data.items))
        label = gui.lay(frame, ttk.Label, 0, 0, 1, 1, {'text': 'Specify:'})
        subframe = gui.lay(frame, ttk.Frame, 1, 0, len(self.tasks), 1,
                xweight = 1, yweight = 1)
        gui.lay(subframe, ttk.Label, 0, 0, 1, 1, {'text':
                 'Choose a field (multiple fields can be chosen by holding Ctrl'
                 ' while clicking):'})
        listbox = gui.lay(subframe, tk.Listbox, 1, 0, 1, 1,
                {'selectmode': 'extended'}, xweight = 1, yweight = 1)
        listbox.insert('end', keys_all_mark)
        for item in items:
            listbox.insert('end', item)
        gui.lay_scrollbars(subframe, listbox, x_offset = 1)
        subsubframe = gui.lay(subframe, ttk.Frame, 0, 2, 2, 1, padx = 6)
        gui.lay(subsubframe, ttk.Label, 0, 0, 1, 1, {'text':
                'Enter the Regular Expression (matches will be substituted):'})
        pattern_entry = gui.lay(subsubframe, ttk.Entry, 1, 0, 1, 1)
        gui.lay(subsubframe, ttk.Label, 2, 0, 1, 1, {'text':
                'Enter the substitution string:'})
        replace_entry = gui.lay(subsubframe, ttk.Entry, 3, 0, 1, 1)
        submit = gui.lay(subsubframe, ttk.Button, 4, 0, 1, 1,
                {'text': 'Submit & Start'}, pady = (5, 2))
        for event in ('<1>', '<Return>'):
            submit.bind(event, partial(
                    self.substitute,
                    infilepath,
                    data,
                    frame,
                    listbox,
                    pattern_entry,
                    replace_entry,
                    keys_all_mark,
                    ))
        cancel = gui.lay(subsubframe, ttk.Button, 5, 0, 1, 1,
                {'text': 'Cancel', 'command': frame.destroy})
        self.update_idletasks()

def defer_new_field(
        data: 'bibtex.Data',
        old_key: str,
        new_key: str,
        old_value_delimiter: str,
        new_value_delimiter: str,
        old_field_be_deleted: bool,
        deference: 'dict[str, str]',
        ) -> str:
    data.outside_items = deque([
            '% This file was produced by a script.\n% Encoding: UTF8\n'])
    items = deque()
    while data.items:
        item = data.items.popleft()
        if old_key in item.fields:
            if old_value_delimiter:
                old_values = unquote(item.fields[old_key]
                        ).split(old_value_delimiter)
            else:
                old_values = list(unquote(item.fields[old_key]))
            new_values = [ deference[old_value] for old_value in old_values
                    if old_value in deference ]
            if new_values:
                values = [] if new_key not in item.fields else unquote(
                        item.fields.get(new_key, '')
                        ).split(new_value_delimiter)
                for new_value in new_values:
                    if new_value not in values:
                        values.append(new_value)
                item.fields[new_key] = quote(new_value_delimiter.join(values))
        items.append(item)
    data.items = items
    return data.write()

def even_names_initials(value: str) -> str:
    def abbreviate(name: str) -> str:
        parts = re.split(r'[-\s.]+', name)
        for i in range(len(parts) - 1):
            if parts[i]:
                parts[i] = parts[i][0]
        return ' '.join(parts)
    return ', '.join(abbreviate(name) for name in value.split(' and ')).lower()

def even_title_interpunctuation_spaces(value: str) -> str:
    return re.sub(r'\s*([-_.:,;!?]+)\s*', '\g<1>', value)

def even_shorthand_markers(value: str) -> str:
    return re.sub(r'^#(.*?)#$', '\g<1>', value)

def get_dates(item: 'bibtex.Item') -> 'None|set[int]':
    date = re.search(r'\d\d\d\d', item.fields.get('Date', ''))
    if date:
        date = int(date.group())
        return {date - 1, date, date + 1}
    return None

def get_fieldkeys(items: 'Sequence[bibtex.Item]') -> 'set[str]':
    return { key for item in items for key in item.fields.keys()
            if item.kind.lower() not in ('string', 'preamble') }

def get_fieldvalues(
        data: 'bibtex.Data',
        chosen_keys: 'set[str]',
        keys_all: bool,
        ) -> 'list[tuple[str, str]]':
    values = set()
    for i in range(len(data.items)):
        for key in data.items[i].fields:
            if keys_all or key in chosen_keys:
                values.add((data.items[i].fields[key], key))
    return sorted(values, key = lambda x: (parse.even(x[0]), x[1]))

def get_text_from_file() -> 'tuple[None|str, str]':
    infilepath = filedialog.askopenfilename(
            defaultextension = '.bib',
            filetypes = [('All files', '*'), ('BibTex', '*.bib')],
            initialdir = '.',
            title = 'Choose a UTF-8-BibTex-formatted file (or quit).')
    try:
        with open(infilepath, 'r', encoding = 'utf-8') as file:
            return (file.read(), infilepath)
    except:
        return (None, infilepath)

def get_title(item: 'bibtex.Item') -> str:
    return parse.even(
            ''.join(item.fields.get('Title', '').split()),
            pre = SUBSTITUTES,
            acts = (parse.transletter,),
            )

def merge(
        data: 'bibtex.Data',
        taker_key_value: 'tuple[str, str]',
        giver_key_value: 'tuple[str, str]',
        stamp_key_value: 'tuple[str, str]',
        union_keys_delimiters: 'dict[str, str]',
        no_merge_fieldkeys: 'set[str]',
        merge_keysuffix: str,
        taker_compare_preprocesses: 'dict[str, ty.Callable[[str], str]]',
        giver_compare_preprocesses: 'dict[str, ty.Callable[[str], str]]',
        ) -> 'tuple[str, int]':
    data.outside_items = deque([
            '% This file was produced by a script.\n% Encoding: UTF8\n'])
    items = deque()
    taker_key, taker_value = taker_key_value
    giver_key, giver_value = giver_key_value
    stamp_key, stamp_value = stamp_key_value
    no_merge_fieldkeys.add(stamp_key)
    merge_position = -1
    number_of_affected_fields = 0
    while data.items:
        item = data.items.popleft()
        if unquote(item.fields.get(taker_key, '')) == taker_value:
            merge_position = len(items)
            items.append(item)
        elif unquote(item.fields.get(giver_key, '')) == giver_value and\
                merge_position != -1:
            for key, value in item.fields.items():
                value = unquote(value)
                if key not in no_merge_fieldkeys:
                    number_of_affected_fields += 1
                    items[merge_position].fields[stamp_key] = quote(
                            stamp_value)
                    if key in items[merge_position].fields:
                        merge_value = unquote(
                                items[merge_position].fields[key])
                        if key in union_keys_delimiters:
                            merge_values = merge_value.split(
                                    union_keys_delimiters[key])
                            giver_values = filter(
                                    lambda _: _ not in merge_values,
                                    value.split(union_keys_delimiters[key]))
                            merge_values.extend(giver_values)
                            items[merge_position].fields[key] = quote(
                                    union_keys_delimiters[key]
                                    .join(merge_values))
                        else:
                            taker_compvalue = merge_value
                            giver_compvalue = value
                            if key in taker_compare_preprocesses:
                                taker_compvalue = taker_compare_preprocesses[key
                                        ](taker_compvalue)
                            if key in giver_compare_preprocesses:
                                giver_compvalue = giver_compare_preprocesses[key
                                        ](giver_compvalue)
                            if taker_compvalue != giver_compvalue:
                                key += merge_keysuffix
                                items[merge_position].fields[key] = quote(
                                        value)
                    else:
                        items[merge_position].fields[key] = quote(value)
        else:
            items.append(item)
    data.items = items
    return data.write(), number_of_affected_fields

def prepend_similars(data: 'bibtex.Data') -> str:
    data.outside_items = deque([
            '% This file was produced by a script.\n% Encoding: UTF8\n'])
    similia = deque()
    insimilia = deque()
    while data.items:
        comparata_similia = deque()
        comparata_insimilia = deque()
        comparandum = data.items.popleft()
        dates = get_dates(comparandum)
        title = get_title(comparandum)
        if dates and title:
            title_matcher = SequenceMatcher(None, title)
            while data.items:
                candidatum = data.items.popleft()
                other_dates = get_dates(candidatum)
                other_title = get_title(candidatum)
                title_matcher.set_seq2(other_title)
                if other_dates and (dates & other_dates) and (
                        title == other_title or
                        title_matcher.ratio() > 0.9):
                    comparata_similia.append(candidatum)
                else:
                    comparata_insimilia.append(candidatum)
        if comparata_similia:
            comparandum.fields['MayBeDuplicate'] = '"FIRST"'
            similia.append(comparandum)
            for item in comparata_similia:
                item.fields['MayBeDuplicate'] = '"FOLLOWING"'
                similia.append(item)
        else:
            insimilia.append(comparandum)
        data.items.extend(comparata_insimilia)
    similia.extend(insimilia)
    data.items = similia
    return data.write()

def save(
        text: str,
        infilepath: str,
        extension: str = '.bib',
        message: str = '',
        ) -> None:
    outfilepath = fs.get_new_path(infilepath, ext = extension)
    with open(outfilepath, 'w', encoding = 'utf-8') as file:
        size = file.write(text)
    messagebox.showinfo(
            'Finished',
            'Output:\n\n{}\n\n({} bytes)\n\n'.format(outfilepath, size) +
                message)

def sort_fields(
        data: 'bibtex.Data',
        ordering_keys: 'Iterable[str]',
        ) -> str:
    data.outside_items = deque([
            '% This file was produced by a script.\n% Encoding: UTF8\n'])
    items = deque()
    while data.items:
        item = data.items.popleft()
        sorted_fields = {}
        for key in ordering_keys:
            if key in item.fields:
                sorted_fields[key] = item.fields.pop(key)
        for key, value in sorted(item.fields.items()):
            sorted_fields[key] = value
        item.fields = sorted_fields
        items.append(item)
    data.items = items
    return data.write()

def substitute(
        data: 'bibtex.Data',
        chosen_keys: 'set[str]',
        pattern: str,
        replace: str,
        keys_all: bool,
        ) -> 'tuple[str, int]':
    data.outside_items = deque([
            '% This file was produced by a script.\n% Encoding: UTF8\n'])
    number_of_substitutions = 0
    for i in range(len(data.items)):
        for key in data.items[i].fields:
            if keys_all or key in chosen_keys:
                new, number = re.subn(pattern, replace,
                        unquote(data.items[i].fields[key]))
                data.items[i].fields[key] = quote(new)
                number_of_substitutions += number
    return data.write(), number_of_substitutions

def main() -> None:
    window = Window()
    window.mainloop()

if __name__ == '__main__':
    main()
