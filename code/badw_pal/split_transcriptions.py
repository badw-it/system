# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import xml.etree.ElementTree as ET
from collections import deque

import __init__
import fs
import xmlhtml
from parse import sortnum

def consider_endtag(
        old_tag: str,
        old_attrs: 'dict[str, None|str]',
        new_tag: str,
        new_attrs: 'dict[str, None|str]',
        parser: 'xmlhtml.Replacer',
        ) -> None:
    if new_tag == 'a' and parser.info['data-kept-in-split']:
        parser.info['data-kept-in-split'] = False
    return None

def fragmentize(doc: str) -> str:
    old_items = xmlhtml.itemize(doc)
    new_items = deque()
    open_tags = deque()
    td_th_rank = 0
    while old_items:
        num, start, attrs, end, text, *_ = old_items.popleft()
        if start == end == 'pagina':
            # Add a hyphen if the page break is surrounded by letters:
            if (
                    new_items
                    and new_items[-1][4]
                    and new_items[-1][4][-1].isalpha()
                    and old_items[0][4]
                    and old_items[0][4][0].isalpha()):
                new_items[-1][4] += '-'
            # Close still open tags:
            for s, a in reversed(open_tags):
                new_items.append((num, '', {}, s, '', '', '', '', ''))
            # Add the current element, i.e. the pagina:
            new_items.append((num, start, attrs, end, '', '', '', '', ''))
            # Re-open the tags that were open:
            for s, a in open_tags:
                new_items.append((num, s, a, '', '', '', '', '', ''))
                if s == 'tr' and td_th_rank:
                    for _ in range(td_th_rank - 1):
                        new_items.append(
                                [num, 'td', {}, '', '', '', '', '', ''])
            continue
        elif start and end:
            pass
        elif start:
            if start in {'td', 'th'}:
                td_th_rank += 1
            open_tags.append((start, attrs))
        elif end:
            if end == 'tr':
                td_th_rank = 0
            open_tags.pop()
        new_items.append([num, start, attrs, end, text, '', '', '', ''])
    return xmlhtml.serialize(new_items)

def normalize(doc: str) -> str:
    doc = doc.strip()
    doc = re.sub(r'<p></p>', '', doc)
    doc = re.sub(r'\n\s+', '\n', doc)
    doc = xmlhtml.replace(doc, replace_tag = replace_tag_in_part)
    return doc

def read(path: str) -> str:
    try:
        with open(path, 'r', encoding = 'utf-8') as file:
            text = file.read()
    except UnicodeDecodeError:
        raise Exception('UnicodeDecodeError in: ' + path)
    chapter = os.path.splitext(os.path.basename(path))[0]
    items = deque()
    for start, attrs, end, text, com, pi, decl, undecl, num\
            in xmlhtml.itemize(text):
        if (
                start in {'a', 'span'} and
                'id' in attrs and
                'pagina' in attrs.get('class', '').split()):
            attrs['data-chapter'] = chapter
        items.append((start, attrs, end, text, com, pi, decl, undecl, num))
    return xmlhtml.serialize(items)

def remove_parts_specific_for_fulltext_and_mark_pagebreaks(doc: str) -> str:
    doc = xmlhtml.replace(
            doc,
            replace_element = replace_element,
            replace_tag = replace_tag,
            consider_endtag = consider_endtag,
            info = {'data-kept-in-split': False},
            )
    doc = re.sub(r'(<p[^>]*>)\s+', r'\g<1>', doc)
    doc = re.sub(r'\s+</p>', r'</p>', doc)
    doc = re.sub(r'</p>\s*', r'</p>\n', doc)
    return doc

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    if tag in {'details', 'nav'}:
        return ''
    elif tag == 'img' and not parser.info['data-kept-in-split']:
        return ''
    elif (
            tag in {'a', 'span'} and
            'id' in attrs and
            'pagina' in attrs.get('class', '').split()
            ):
        pagina = attrs['id']
        chapter = attrs['data-chapter']
        return f'<pagina file="{chapter}#{pagina}.xml"/>'
    else:
        return None

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    if (
            tag == 'a' and
            'id' not in attrs and
            'pagina' in attrs.get('class', '').split()
            ):
        tag = 'span'
        attrs = {'class': 'txt'}
    elif (
            tag == 'a' and
            attrs.get('href', '').startswith('intus/')
            ):
        if 'data-kept-in-split' in attrs:
            attrs['href'] = 'text/' + attrs['href']
            parser.info['data-kept-in-split'] = True
        else:
            tag = ''
    elif tag == 'img' and parser.info['data-kept-in-split']:
        if 'src' in attrs:
            attrs['src'] = 'text/' + attrs['src']
    return tag, attrs, startend

def replace_tag_in_part(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    if tag == 'article':
        tag = 'section'
        attrs = {'class': 'txtsrc'}
    return tag, attrs, startend

def split(transcriptiondirpath: str) -> None:
    print(transcriptiondirpath)
    docs = []
    for name in sorted(os.listdir(transcriptiondirpath), key = sortnum):
        if re.search(r'^\d+\.xml$', name):
            docs.append(read(os.path.join(transcriptiondirpath, name)))
    if not docs:
        print(f'No files of the kind “1.xml” found in {transcriptiondirpath}')
        return
    doc = '\n'.join(docs)
    doc = remove_parts_specific_for_fulltext_and_mark_pagebreaks(doc)
    doc = fragmentize(doc)
    parts = re.split(r'(<pagina file="[^"]*"/>)', doc)
    pagesdirpath = os.path.join(transcriptiondirpath, 'pages')
    fs.pave(os.path.join(pagesdirpath, '_'))
    for name in os.listdir(pagesdirpath):
        os.remove(os.path.join(pagesdirpath, name))
    for i in range(1, len(parts), 2):
        filename = re.search(r'file="([^"]*)"', parts[i]).group(1)
        textpart = normalize(parts[i + 1])
        try:
            ET.XML(f'<root>{textpart}</root>')
        except Exception:
            raise Exception(f'Invalid XML in {filename}? Check:\n\n{textpart}')
        path = os.path.join(pagesdirpath, filename)
        fs.write(textpart, path)
        print('\t' + path)

if __name__ == '__main__':
    '''
    Extra pages – say “V₁,74v” in https://ptolemaeus.badw.de/ms/243/351/text/1 –
    must have “class="pagina_extra"” instead of “class="pagina"”.
    '''

    split(
            r"Z:\pal\7. Website\repros\ms\Brussels, BR, 1022-1047 #486\#669\text"
            )
