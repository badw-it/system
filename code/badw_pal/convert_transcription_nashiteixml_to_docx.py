# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2022 ff. (© http://badw.de)
'''
See :func:`main`.
'''
import base64
import io
try: import regex as re
except ImportError: import re
import typing as ty
import xml.etree.ElementTree as ET
from collections import Counter
from html import escape
from os.path import join

from PIL import Image

import __init__
import fs
import xmlhtml

class Form(ty.TypedDict):
    scale: float
    text_align: ty.Literal['left', 'right']
    writing_mode: ty.Literal['lr-tb', 'rl-tb']
    pagination: ty.Literal['edition', 'witness']

def replace_element(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'None|str':
    """
    Process :param:`tag` and :param:`attrs`.
    If the tag is self-closing, :param:`startend` is ``True``.
    A return value of ``None`` does not change the element.
    If a string is returned (possibly the empty string),
    the element is replaced by this string **unescaped**.
    """
    if tag in {'facsimile', 'teiheader'}:
        return ''
    elif tag == 'figure':
        return parser.info.get(attrs.get('facs', '')[1:], '')
    elif tag == 'note' and attrs.get('type', '') == 'koi':
        return ''
    return None

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        ) -> 'tuple[str, dict[str, None|str], bool]':
    """
    Consider the element :param:`tag` with :param:`attrs`; return:

    1. a string: The tag name will be this string. But if the string
       is the empty string, the whole tag will be deleted.
    2. a dictionary: The attributes will be taken from it. And if it
       is empty, no attributes will be set.
    3. a boolean: If ``True``, the element is made self-closing.

    :param startend: ``True``, if the element is self-closing.
    """
    if tag == 'lb':
        if attrs.get('n', '').lstrip('0') == '1':
            tag = ''
        else:
            tag = 'text:line-break'
            attrs = {}
    elif tag == 'note':
        attrs = {}
    elif tag == 'pb':
        attrs = {'page': attrs['n']}
    else:
        tag = ''
    return tag, attrs, startend

def get_fodt(
        doc: str,
        image_dirpath: str = '',
        form: 'dict[str, str|int|float]' = {},
        ) -> str:
    def make_notes(match: 're.Match[str]') -> str:
        nonlocal note_num
        note_num += 1
        text = match.group('text').strip()
        return f'''
<text:note text:id="ftn{note_num}" text:note-class="footnote">
	<text:note-citation>{note_num}</text:note-citation>
	<text:note-body><text:p>{text}</text:p></text:note-body>
</text:note>
'''

    scale = form.get('scale', 1)
    figures = {}
    if image_dirpath:
        xmlns = '{http://www.w3.org/XML/1998/namespace}'
        tei = ET.XML(doc)
        for figure in tei.iterfind('.//figure[@facs]'):
            figures[figure.attrib['facs'][1:]] = b''
        for figure_id in figures:
            graphic = tei.find(f'.//zone[@{xmlns}id="{figure_id}"]/../graphic')
            image_name = graphic.attrib['url']
            zone = tei.find(f'.//zone[@{xmlns}id="{figure_id}"]')
            xys = [
                    tuple(map(int, xystring.split(',')))
                    for xystring in zone.attrib['points'].strip().split()
                    ]
            xmin = min( x for x, y in xys )
            ymin = min( y for x, y in xys )
            xmax = max( x for x, y in xys )
            ymax = max( y for x, y in xys )
            width = round((xmax - xmin) * scale)
            height = round((ymax - ymin) * scale)
            mem = io.BytesIO()
            image_form = 'png'
            Image.open(
                    join(image_dirpath, image_name)
                    ).crop((xmin, ymin, xmax, ymax)
                    ).save(mem, format = image_form)
            imagestring = base64.b64encode(mem.getvalue()).decode()
            figures[figure_id] = f'''</text:p><text:p text:style-name="P1">
<draw:frame
  draw:style-name="fr1"
  draw:name="{escape(figure_id)}"
  text:anchor-type="as-char"
  svg:width="{width}px"
  svg:height="{height}px"
  draw:z-index="0">
 <draw:image draw:mime-type="image/{image_form}">
  <office:binary-data>{imagestring}</office:binary-data>
 </draw:image>
</draw:frame>
</text:p><text:p text:style-name="P1">'''
    info = figures
    replacer = xmlhtml.Replacer(
            replace_element = replace_element,
            replace_tag = replace_tag,
            info = info,
            )
    replacer.parse(doc)
    doc = ''.join(replacer.results)
    doc = doc.strip()
    note_num = 0
    for old, new in (
            (
                r'\[([\da-z]+)\]', # Witnessʼ pagination.
                '\u202A/\\g<1>/\u202C' if form.get('pagination') == 'witness'
                else ''
            ),
            (
                r'<pb page="([^"]*)"/>', # Nashiʼs pagination.
                '<text:line-break/>\u202A/\\g<1>/\u202C'
                    if form.get('pagination') == 'edition'
                else ''
            ),
            (
                r'<text:line-break/>(\s|<text:line-break/>)*',
                ' || <text:line-break/>'
            ),
            (r'^( \|\| <text:line-break/>)+', ''),
            (r'( \|\| <text:line-break/>)+$', ''),
            (
                r'<note>(?P<text>.*?)</note>',
                make_notes
            ),
            (
                r'&lt;(?!\.)((?:(?!&gt;).)+)(?<!\.)&gt;',
                '<text:span text:style-name="T1">\\g<1></text:span>'
            ), # Nashiʼs <> are overlines, here underlines for Word.
            (
                r'&lt;\.((?:(?!&gt;).)+)\.&gt;',
                '&lt;\\g<1>&gt;'
            ),
            (r'\s+', ' '),
            (
                r'\u202a/(L\d*)/\u202c ?\u202a/([^L][^/]*)/\u202c',
                '\u202a/\\g<2>/\u202c\u200F \u200F\u202a/\\g<1>/\u202c'
            ),
            (
                r'[\u202a\u202c\u200f]' if form.get('writing_mode') == 'lr-tb'
                else '',
                ''
            )
            ):
        if old:
            doc = re.sub(old, new, doc)
    doc = f'''<?xml version="1.0" encoding="UTF-8"?>
<office:document xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:rpt="http://openoffice.org/2005/report" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:tableooo="http://openoffice.org/2009/table" xmlns:calcext="urn:org:documentfoundation:names:experimental:calc:xmlns:calcext:1.0" xmlns:drawooo="http://openoffice.org/2010/draw" xmlns:loext="urn:org:documentfoundation:names:experimental:office:xmlns:loext:1.0" xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:formx="urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:grddl="http://www.w3.org/2003/g/data-view#" xmlns:css3t="http://www.w3.org/TR/css3-text/" xmlns:officeooo="http://openoffice.org/2009/office" office:version="1.3" office:mimetype="application/vnd.oasis.opendocument.text">
 <office:font-face-decls>
  <style:font-face style:name="TNR" svg:font-family="&apos;Times New Roman&apos;" style:font-family-generic="roman" style:font-pitch="variable"/>
 </office:font-face-decls>
 <office:styles>
  <style:default-style style:family="paragraph">
   <style:paragraph-properties fo:hyphenation-ladder-count="no-limit" style:text-autospace="ideograph-alpha" style:punctuation-wrap="hanging" style:line-break="strict" style:tab-stop-distance="1cm" style:writing-mode="page"/>
   <style:text-properties style:use-window-font-color="true" loext:opacity="0%" style:font-name="Times New Roman" fo:font-size="12pt" fo:language="ar" fo:country="SA" style:letter-kerning="true" style:font-name-asian="Times New Roman" style:font-size-asian="12pt" style:language-asian="ar" style:country-asian="SA" style:font-name-complex="TNR" style:font-size-complex="12pt" style:language-complex="ar" style:country-complex="SA" fo:hyphenate="false" fo:hyphenation-remain-char-count="2" fo:hyphenation-push-char-count="2" loext:hyphenation-no-caps="false"/>
  </style:default-style>
 </office:styles>
 <office:automatic-styles>
  <style:style style:name="P1" style:family="paragraph" style:parent-style-name="Standard">
   <style:paragraph-properties fo:text-align="{form.get('text_align', 'left')}" style:justify-single-word="false" style:page-number="auto" style:writing-mode="{form.get('writing_mode', 'lr-tb')}"/>
  </style:style>
  <style:style style:name="T1" style:family="text">
   <style:text-properties style:text-underline-style="solid" style:text-underline-width="auto" style:text-underline-color="font-color"/>
  </style:style>
  <style:style style:name="fr1" style:family="graphic" style:parent-style-name="Graphics">
   <style:graphic-properties style:vertical-pos="top" style:vertical-rel="baseline" style:mirror="none" draw:luminance="0%" draw:contrast="0%" draw:red="0%" draw:green="0%" draw:blue="0%" draw:gamma="100%" draw:color-inversion="false" draw:image-opacity="100%" draw:color-mode="standard"/>
  </style:style>
 </office:automatic-styles>
 <office:body>
  <office:text>
   <text:p text:style-name="P1">{doc}</text:p>
  </office:text>
 </office:body>
</office:document>
'''
    return doc

def main(
        path: str,
        soffice_exe_path: str,
        image_dirpath: str = '',
        form: Form = {},
        ) -> 'tuple[str, int]':
    '''
    Convert :param:`path`, a TEI-XML file generated by ALEX (“ALCorpus’ Arabic &
    Latin EXport toolbox”) from the Page XML exported from Nashi.

    Convert it to fodt and docx, return path and size of the latter.

    About Nashi cf.: https://alc.philosophie.uni-wuerzburg.de/nashi

    :param soffice_exe_path: path to an executable soffice.exe file.
    :param image_dirpath: If there are images which have to be included into the
        output files: the path to the folder that contains them.
    :param form: Settings for the handling of the text and images.
    '''
    doc = get_fodt(fs.read(path), image_dirpath, form)
    path, size = fs.write_new(doc, path, ext = '.fodt')
    path, size = fs.convert(path, soffice_exe_path, '.docx')
    return path, size

if __name__ == '__main__':
    print(main(
            r"Z:\pal\4. Personal\6. Stefan\OCR\Avic_Meteor_ar_PageXML.xml",
            r"/Program Files/LibreOffice/program/soffice.exe",
            image_dirpath = r"",
            form = {
                'scale': 0.25,
                'text_align': 'right',
                'writing_mode': 'rl-tb',
                'pagination': 'witness',
                },
            ))
