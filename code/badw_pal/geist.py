# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import json
try: import regex as re
except ImportError: import re
import sys
import time
from unicodedata import normalize

from collections import deque
from urllib.parse import unquote
try:
    from beaker.middleware import SessionMiddleware
except:
    SessionMiddleware = None

import gehalt
import __init__
import bottle
import web_io
from bottle import HTTPError, redirect, request, response, static_file

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        if not db.debug:
            server.quiet = True
        self.kwargs = {
                'app': self if SessionMiddleware is None else
                    SessionMiddleware(self, db.config['session']),
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.hook('before_request')
        def redirect_alternatives():
            if request.urlparts[0] == 'http' and not db.debug:
                redirect(request.url.replace('http', 'https', 1), code = 302)
            if (
                    request.urlparts[1] == 'jordanus.badw.de' and
                    request.urlparts[2] == '/'
                    ):
                redirect(
                        request.url.replace(
                            '://jordanus.badw.de',
                            '://jordanus.badw.de/jordanus', 1),
                        code = 301)
            if '/transcription' in request.urlparts[2]:
                redirect(
                        request.url.replace('/transcriptions', '/texts')\
                                   .replace('/transcription', '/text'),
                        code = 301)

        @self.hook('before_request')
        def status():
            request.user_id, request.user_name, request.roles = db.auth(request)

        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which specify no page:
            Redirect to the default page :attr:`db.page_id`.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/' + db.page_id)

        @self.route('/api/bib/export/<mode>')
        def return_bib_export(mode):
            '''
            Return an export in :param:`mode` of the bibliography
            for download.
            '''
            dirpath, path, name = gehalt.get_bib_paths(db.paths['bib'])
            return static_file(
                    name,
                    dirpath,
                    download = True,
                    )

        @self.route('/api/bib/export/<mode>', method = 'POST')
        def return_bib_export_post(mode, id_re = re.compile(r'id="([^"]*)"')):
            try:
                doc = (
                        '<!DOCTYPE html><html><head><meta charset="utf-8"/><title>Export</title></head><body>\n'
                        + '\n'.join(
                            '<p class="just_ahyph hanging">{}</p>'.format(
                            template(
                                'bib_item.tpl',
                                invert_italics = db.invert_italics,
                                rename = db.rename_bib1,
                                invert_til_authors = 1,
                                **(db.get_bib_item(
                                    unquote(
                                        id_re.search(row['_']).group(1)),
                                    mode))))
                            for row in
                            json.loads(request.body.read().decode())
                            )
                        + '\n</body></html>')
            except: # Sic.
                doc = '<!DOCTYPE html><html><head><meta charset="utf-8"/><title>Export</title></head><body>Sorry, I have not understood the request.</body></html>'
            return doc

        @self.route('/api/bib/<siglum>/<mode>')
        def return_bib_item(siglum, mode):
            '''
            Return the bibliography item :param:`siglum` in a form determined by
            :param:`mode`.
            '''
            item = db.get_bib_item(siglum, mode)
            if not item:
                return ''
            return (
                    '<div class="card" id="row_over"><button onclick="clipcopy(event, 2)" data-done="Copied">Copy</button><p>'
                    + template(
                        'bib_item.tpl',
                        invert_italics = db.invert_italics,
                        rename = db.rename_bib1,
                        invert_til_authors = 1,
                        **item)
                    + '</p></div>')

        @self.route('/api/db/export')
        def return_db_export():
            '''
            Return a partial export of the database for download.
            '''
            response.set_header(
                    'Content-Disposition',
                    'attachment; filename="PAL_Database.json"')
            response.content_type = 'application/json'
            return json.dumps(
                    tuple(db.export_db()), separators = (',', ':')
                    ).replace('\\u2423', '\xa0')

        @self.route('/api/get_num_of_mss')
        def return_num_of_mss():
            return str(db.get_num('ms', lang_name = request.query.lang_name))

        @self.route('/api/get_num_of_prints')
        def return_num_of_prints():
            return str(db.get_num('print', lang_name = request.query.lang_name))

        @self.route('/auth_end')
        def auth_end():
            session = request.environ.get('beaker.session', None)
            if session is not None:
                session.delete()
            redirect(request.query.url or '/' + db.page_id)

        @self.route('/' + db.auth_in_page_id)
        def auth_in():
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_in.tpl'})

        @self.route('/' + db.auth_in_page_id, method = 'POST')
        def auth_in_post():
            user_name = request.forms.user_name
            password = request.forms.password
            user_id = db.auth_in(user_name, password)
            if user_id is None:
                query = request.forms.query
                if query:
                    query = '?' + query
                redirect(request.fullpath + query)
            else:
                session = request.environ['beaker.session']
                session['user_id'] = user_id
                session.save()
                redirect(request.forms.url or '/' + db.page_id)

        @self.route('/astrobibl')
        def redirect_short_url_astrobibl():
            '''
            Redirect requests which specify no page:
            Redirect to the default page :attr:`db.page_id`.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/astrobibl/' + db.page_id)

        @self.route('/astrobibl/section/<siglum>')
        def return_astrobibl(siglum):
            '''
            Return a section of the bibliography of astrology, specified by
            :param:`siglum`.
            '''
            if isinstance(siglum, int) or siglum not in db.bib_astl_sections:
                return ''
            return template(
                    'base_astrobibl.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'astrobibl.tpl',
                        'siglum': siglum,
                        'invert_italics': db.invert_italics,
                        'rename': db.rename_bib2,
                    })

        @self.route('/bib/upload')
        def bib_upload(note = ''):
            if not request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'upload.tpl', 'kind': 'bib', 'note': note})

        @self.route('/bib/upload', method = 'POST')
        def bib_upload_post(note = ''):
            if not request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            elif 'Admin' not in request.roles:
                raise HTTPError(401)
            note = db.bib_upload(request)
            request.method = 'GET'
            return bib_upload(note = note)

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/ersatzindex/<page_id>')
        def return_ersatzindex(page_id):
            '''
            Return an ersatz for the datatable that would otherwise be shown
            on the page :param:`page_id`.
            '''
            table = deque('<table class="index">')
            exists = False
            for row in db.get_index(page_id, request):
                exists = True
                table.append('<tr><td>' + '</td> <td>'.join(
                        val['_'] if isinstance(val, dict) else str(val)
                        for _, val in sorted(row.items())
                        ) + '</td></tr>')
            if not exists:
                raise HTTPError(404, 'The page has not been found.')
            table.append('</table>')
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'text.tpl',
                        'text': ''.join(table),
                        })

        @self.route('/dir/<foldername>')
        def return_internal_download_page(foldername):
            '''
            Return a page showing all the files in a folder :param:`foldername`
            in a parent folder “files” in :attr:`db.content_path`, sorting the
            files by their name and linking them for download.
            '''
            filenames = db.get_dir_filenames(foldername)
            if filenames is None:
                raise HTTPError(404, 'The page has not been found.')
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'dir.tpl',
                        'foldername': foldername,
                        'filenames': filenames,
                        })

        @self.route('/filter')
        def return_filter():
            '''
            Return a page of results filtered according to the filters sent
            with ``request.query.text``.
            '''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'index.tpl',
                        'table_config': {
                            'ajax': f'/filter/json?{request.query_string}',
                            'columns': [
                                {'data': '0', 'render': {'_': '_', 'sort': 's'}, 'type': 'html-num'},
                                {'data': '1', 'render': {'_': '_', 'sort': 's'}},
                                None,
                                ],
                            'order': [[0, 'asc'], [1, 'asc']],
                            'dom': 'fplitp',
                            'language': {'search': 'Filter: '}
                            },
                        'with_filtercard': False,
                        'text': template(
                            'filter.tpl', db = db, request = request,
                            kwargs = {}),
                        })

        @self.route('/filter/json')
        def return_filter_json():
            '''
            Return JSON for a datatable of filtered results.
            '''
            matches, matches_num = db.filter_text(request.query.text)
            response.content_type = 'application/json'
            return json.dumps({
                    'data': matches,
                    'matches_num': matches_num,
                    }, separators = (',', ':')
                    ).replace('\\u2423', '\xa0')

        @self.route('/forum', method = 'POST')
        def create_forum_page():
            if not request.roles:
                raise HTTPError(401)
            page_id = int(request.forms.page_id or 0)
            page_id, post_id = db.create_forum_post(request.user_id, page_id)
            redirect(f'/forum/{page_id}/{post_id}')

        @self.route('/forum/clean_up', method = 'POST')
        def clean_up_forum():
            if not request.roles:
                raise HTTPError(401)
            db.clean_up_forum()
            redirect('/forum')

        @self.route('/forum/redact/<page_id:int>', method = 'POST')
        def redact_page(page_id):
            if 'Admin' not in request.roles:
                raise HTTPError(401)
            db.del_forum_page(page_id)
            redirect('/forum')

        @self.route(
                '/forum/redact/<page_id:int>/<post_id:int>', method = 'POST')
        def redact_post(page_id, post_id):
            if 'Admin' not in request.roles:
                raise HTTPError(401)
            page_deleted = db.del_forum_post(page_id, post_id)
            if page_deleted:
                redirect('/forum')
            else:
                redirect(f'/forum/{page_id}')

        @self.route('/forum/<page_id:int>')
        def return_forum_page(page_id):
            if not request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            posts = db.get_forum_page(page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'forum_page.tpl', 'posts': posts})

        @self.route('/forum/<page_id:int>/<post_id:int>')
        def return_forum_post(page_id, post_id):
            if not request.roles:
                raise HTTPError(401)
            post = db.get_forum_post(post_id)
            if not post:
                raise HTTPError(404, 'The page has not been found.')
            if not (
                    request.user_id == post['user_id'] or
                    'Admin' in request.roles):
                raise HTTPError(401, 'Unauthorized.')
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'forum_post.tpl', 'post': post})

        @self.route('/forum/<page_id:int>/<post_id:int>', method = 'POST')
        def update_forum_post(page_id, post_id):
            if not request.roles:
                raise HTTPError(401)
            try:
                db.update_forum_post(page_id, post_id, request)
            except Exception as e:
                return str(e)
            if request.forms.aim == 'back_to_forum':
                redirect('/forum')
            else:
                redirect(f'/forum/{page_id}#{post_id}')

        @self.route('/glossary/<concept_id>/<expression>')
        def return_glossary_item(concept_id, expression):
            '''
            Return the lemma with the concept :param:`concept_id`
            and the expression :param:`expression`.
            '''
            expression = unquote(expression)
            # must be unquoted because it had to be double-quoted to prevent
            # bottleʼs router from misreading routes with `%2F` (slash) etc.
            lemma = db.get_lemma(
                    concept_id,
                    expression,
                    item_id = None,
                    with_transl_froms = True,
                    with_transl_intos = True,
                    with_synonyms = True,
                    )
            if lemma:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'lemma.tpl',
                            'lemma': lemma,
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/index/<kind>/data')
        def return_index(kind):
            '''
            Return JSON for a datatable of e.g. MSS (or other entities,
            specified by :param:`kind`).
            '''
            response.content_type = 'application/json'
            temp = json.dumps(
                    {'data': tuple(db.get_index(kind, request))},
                    separators = (',', ':')
                    ).replace('\\u2423', '\xa0')
            return temp

        @self.route('/jobs')
        def return_jobs():
            '''
            Return a page describing the current job positions, announcing
            open jobs and providing a link to the jobs archive.
            '''
            general, vacancies = db.get_jobs_information()
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'jobs.tpl',
                        'general': general,
                        'vacancies': vacancies,
                        })

        @self.route('/jobs_archive')
        def return_jobs_archive():
            '''
            Return a page listing current and former vacancies.
            '''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'jobs_archive.tpl',
                        'vacancies': db.get_jobs(),
                        })

        @self.route('/job/<job_id>')
        def return_job(job_id):
            '''Return a page describing a current or a former vacancy.'''
            vacancies = db.get_jobs(job_id = job_id)
            if vacancies:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'job.tpl',
                            'vacancy': vacancies[0],
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/job')
        def redirect_ms(): redirect('/jobs')

        @self.route('/jordanus')
        def redirect_short_url_jordanus():
            '''
            Redirect requests which specify no page:
            Redirect to the default page :attr:`db.page_id`.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/jordanus/' + db.page_id)

        @self.route('/jordanus/index/<kind>/data')
        def return_index_jordanus(kind):
            '''
            Return JSON for a datatable of e.g. authors (or other entities,
            specified by :param:`kind`) of Jordanus.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.get_index_jordanus(kind))},
                    separators = (',', ':')
                    ).replace('\\u2423', '\xa0')

        @self.route('/jordanus/ms/<ms_id>')
        def return_ms_jordanus(ms_id):
            '''
            Return a catalog item of Jordanus.
            '''
            item = db.get_ms_jordanus(ms_id)
            if item:
                return template(
                        'base_jordanus.tpl', db = db, request = request,
                        kwargs = {'tpl': 'ms_jordanus.tpl', 'item': item})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/jordanus/filterform')
        def return_filterform_jordanus():
            '''
            Return a page with a filterform for the catalog of Jordanus.
            '''
            return template(
                    'base_jordanus.tpl', db = db, request = request,
                    kwargs = {'tpl': 'filterform_jordanus.tpl'})

        @self.route('/jordanus/filter')
        def return_filter_jordanus():
            '''
            Return a page of results filtered according to the filters sent
            with ``request.query.text``.
            '''
            return template(
                    'base_jordanus.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'index.tpl',
                        'table_config': {
                            # 'ajax': f'/filter/json?{request.query_string}',
                            'dom': 'fplitp',
                            'language': {'search': 'Filter: '}
                            },
                        'with_filtercard': False,
                        'text': template(
                            'filter_jordanus.tpl', db = db, request = request,
                            kwargs = {}),
                        })

        @self.route('/ms')
        def redirect_ms(): redirect('/manuscripts')

        @self.route('/news')
        def return_news():
            '''Return a page of dates, headers and links of all news items.'''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'news.tpl', 'news': db.get_news()})

        @self.route('/news/<news_id>')
        def return_news_item(news_id):
            '''
            Return a page for one news item specified by :param:`news_id`.
            '''
            news_items = db.get_news(news_id)
            if news_id and news_items:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'news_item.tpl',
                            'news_item': news_items[0],
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/pal/<path:path>')
        def redirect_old_pal(path):
            '''
            Redirect requests for the old site:
            Redirect to the default page :attr:`db.page_id`.

            The redirected request is sent to :func:`.return_page`.
            Further explanation is found in the docstring of that function.
            '''
            redirect('/' + db.page_id)

        @self.route('/process/<table>/<row_id:int>')
        def process_row(table, row_id):
            '''
            Process the row with ID :param:`row_id` in the table :param:`table`.
            '''
            db.process_row(table, int(row_id))
            redirect(request.query.url)

        @self.route('/project')
        @self.route('/start')
        def return_intro():
            '''
            Return an introductory page: the landing page or a project overview.
            '''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'project.tpl', 'texts': db.get_project()})

        @self.route('/subscribe')
        def return_subscription_form():
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'subscription.tpl'})

        @self.route('/subscribe/<cat>', method = ['GET', 'POST'])
        def subscribe(cat = ''):
            if request.method == 'GET':
                note = ''
            else:
                note = db.subscribe(cat, request.forms.email) or \
                        f'You have subscribed to {cat}!'
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'subscription.tpl', 'note': note})

        @self.route('/team')
        def return_team():
            '''Return a page showing the team.'''
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'team.tpl', 'team': db.get_team()})

        @self.route('/team/contribution')
        def return_contribution():
            '''
            Return information about contributions.

            For the time being, you can only get information about contributions
            to the Arabic catalog in Tex form.
            '''
            contributors = db.get_contributors(request)
            if contributors:
                if request.query.form == 'print':
                    response.set_header(
                            'Content-Disposition',
                            f'attachment; filename="Contributors.tex"'
                            )
                    response.content_type = 'text/plain'
                    doc = db.export_contributors(contributors)
                else:
                    doc = ''
                return doc
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/team/<user_id:int>')
        def return_user(user_id):
            '''Return a page describing the user :param:`user_id`.'''
            user = db.get_user(user_id)
            if user and user['user_lastname'] and user['user_lastname'] != '_':
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {'tpl': 'user.tpl', 'user': user})
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/team/<user_id:int>/acta')
        def return_user_acta(user_id):
            '''Return a page listing the links to entries the user has made.'''
            user = db.get_user(user_id)
            if user and user['group_role'] != 'Former Researcher':
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'user_acta.tpl',
                            'user': user,
                            'acta': db.get_catalog_items_per_user(user_id),
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/text')
        @self.route('/text/<text_id>')
        def return_tran_per_text_id(text_id = ''):
            if not text_id:
                redirect('/texts')
            doc_type = 'edition' if text_id[0] == 'E' else\
                       'print' if text_id[0] == 'P' else\
                       'ms'
            try:
                item = next(db.get_tran_data(doc_type, text_id[1:]), {})
                path = f"/{doc_type}/{item['doc_id']}/{item['sub_id']}/text/1"
            except Exception as e:
                raise HTTPError(404, f'The page has not been found.')
            redirect(path)

        @self.route('/work')
        def redirect_work(): redirect('/works')

        @self.route('/<page_id>')
        def return_page(page_id):
            '''
            Return the page :param:`page_id`.

            If the given page is not found, raise the HTTP error 404.
            '''
            if page_id == 'forum':
                if not request.roles:
                    redirect('/auth_in?url=' + db.q(request.fullpath))
            text, tpl = db.get_text_and_template_name('', page_id)
            if text:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'text': text,
                            'ersatzlink': ('/ersatzindex/' + page_id)
                                if tpl == 'index.tpl' else '',
                            'indexinfolink': '/information#indexfilter',
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/<site_id:re:(astrobibl|jordanus)>/<page_id>')
        def return_subsite_page(site_id, page_id):
            '''
            Return the page :param:`page_id` of the subsite :param:`site_id`.

            If the given page is not found, raise the HTTP error 404.
            '''
            text, tpl = db.get_text_and_template_name(site_id, page_id)
            if text:
                return template(
                        f'base_{site_id}.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'base': 'base_{site_id}.tpl',
                            'text': text,
                            'indexinfolink': '/information#indexfilter',
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/<doc_type:re:(ms|print|work)>/<doc_id:int>')
        def return_full_item(doc_type, doc_id):
            '''
            Return one full catalog item.
            '''
            item, tpl = db.get_item_and_template_name(doc_type, doc_id, request)
            if item:
                if request.query.form == 'print':
                    response.set_header(
                            'Content-Disposition',
                            f'attachment; filename="PAL{request.path}.tex"'
                            )
                    response.content_type = 'text/plain'
                    doc = db.export_doc(
                            item, doc_type, doc_id,
                            request.query.form, request.roles)
                else:
                    doc = template(
                            'base.tpl', db = db, request = request,
                            kwargs = {
                                'tpl': tpl,
                                'item': item,
                                'exportable': True,
                                })
                    if 'export' in request.query:
                        response.set_header(
                                'Content-Disposition',
                                f'attachment; filename="PAL{request.path}.html"'
                                )
                        response.content_type = 'text/html'
                        doc = db.export_doc(
                                doc, doc_type, doc_id,
                                request.query.form, request.roles)
                return doc
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route(
                '/<doc_type:re:(edition|ms|print)>/<doc_id:int>/<text_id:int>/text')
        def redirect_tran(doc_type, doc_id, text_id):
            redirect('/texts')

        @self.route(
                '/<doc_type:re:(edition|ms|print)>/<doc_id:int>/<text_id:int>/text/<page_id>')
        def return_tran(doc_type, doc_id, text_id, page_id):
            '''
            Return the page :param:`page_id` of the fulltext transcription of
            the part or work :param:`part_id` of the print or manuscript or
            edition with the ID :param:`doc_id`.

            If the query of the URL orders an export in the form “heureka”,
            return an HTML version of the text fit to be edited heureka-wise.
            '''
            doc = db.get_tran(doc_type, doc_id, text_id, page_id).decode()
            if doc:
                doc = template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'text_marked.tpl',
                            'text': doc,
                            'exportable': True,
                            })
                if 'export' in request.query:
                    response.set_header(
                            'Content-Disposition',
                            f'attachment; filename="PAL{request.path}.html"')
                    response.content_type = 'text/html'
                    return db.export_doc(
                                doc, doc_type, doc_id,
                                request.query.form, request.roles)
                else:
                    return doc
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route(
                '/<doc_type:re:(edition|ms|print)>/<doc_id:int>/<text_id:int>/text/<path:path>')
        def return_static_content_of_tran(doc_type, doc_id, text_id, path):
            '''
            Return a static content file, e.g. an image file,
            that is part of a transcription.

            If the URL contains a query string with a key `export`,
            the static file is served for download.
            '''
            if doc_type == 'edition':
                upward = '..'
                item_id = doc_id
            else:
                upward = '../..'
                item_id = text_id
            item = next(db.get_tran_data(doc_type, item_id), {})
            return static_file(
                    f"{doc_type}/{item.get('tran_path', '')}/{upward}/{path}",
                    db.repros_path,
                    download = ('export' in request.query),
                    )

        @self.route('/<doc_type:re:(ms|print)>/<doc_id:int>/<part_id>/<pagina>')
        def return_repro_item(doc_type, doc_id, part_id, pagina):
            '''
            Return a page with the facsimile or the transcription (or both)
            belonging to the print or manuscript belonging to the given type,
            IDs and pagina.
            '''
            item = db.get_repro_item(doc_type, doc_id, part_id, pagina)
            if item:
                try:
                    img_scale = float(request.query['img_scale'])
                except: # Sic.
                    img_scale = 1
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': 'repro.tpl',
                            'root_class': 'desk_across txt',
                            'item': item,
                            'img_scale': img_scale,
                            'pagina': pagina,
                            })
            else: raise HTTPError(404, 'The page has not been found.')

        @self.route('/repros/<path:path>')
        def return_repro(path):
            '''
            Return a repro (facsimile) located at :param:`path` within
            :attr:`db.repros_path`.
            '''
            return static_file(
                    path,
                    db.repros_path,
                    download = ('export' in request.query),
                    )

        @self.route('/<path:path>')
        def return_static_content(path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/icons/favicon.ico`
            - `/acta/secreta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(2)

def template(*args, **kwargs):
    doc = bottle.template(*args, **kwargs)
    if 'request' in kwargs:
        mark = request.query.mark
        if mark:
            doc = normalize('NFC', web_io.mark(normalize('NFKD', doc), mark))
    return doc

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
