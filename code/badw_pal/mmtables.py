# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: written in Pascal by Benno van Dalen in 1989 ff.,
# restructured (partially complying with stylistic wishes) and translated
# into Python by Stefan Müller in 2019 ff. (© http://badw.de)
from decimal import Decimal as D
import mmcalendars as mmcs

class Subtable:
    '''
    The base class for the tables of this module, which are subtables of
    mean motion tables.
    '''
    name: str = ''

    def __init__(
            self,
            cal: mmcs.Calendar,
            args: 'list[str]' = [],
            entries: 'list[str]' = [],
            complete: bool = True,
            longitude: D = 0,
            ) -> None:
        self.cal = cal
        self.cal_type = self.cal.__class__.__bases__[0]
        self.args = args
        self.entries = entries
        self.complete = complete
        self.longitude = longitude

    def converted_args(self) -> 'list[str]':
        return [ str(self.get_days_in_arg(D(arg or 0)) or '')
                for arg in self.args ]

    def get_days_in_arg(self, arg: D) -> D:
        return arg

    def get_argstring(self, arg: D) -> str:
        '''
        Give the argument expressed as a string
        (so that, for example, month numbers can be replaced by month names).
        '''
        return str(arg)

    def get_squeeze_param(
            self,
            days_in_arg: D,
            total_mean_motion: D,
            preliminary_param: D,
            ) -> D:
        '''
        From :param:`days_in_arg` (calculated with :meth:`get_days_in_arg`)
        compute from any subtable an approximate value for the underlying
        mean motion per day. This could normally simply be done by dividing
        the total mean motion by the number of days corresponding to the
        argument. However, since the total mean motion is taken modulo
        360 degrees, first the correct multiple of 360 should be added to
        the total mean motion. In order to be able to do this, we need a
        preliminary value of the parameter, either the result of a previous
        estimation or a very rough value such as that for 1 day. This we
        multiply by the number of days and compare with the totalmeanmotion.
        We round the difference to a multiple of 360 degrees, add this to
        the total mean motion and then divide it by the number of days.

        .. important::
            Total mean motion will normally be a tabular value from the subtable
            concerned. The last value of any subtable will provide the most
            accurate result. Note that for a subtable of collected years the
            epoch value must already have been subtracted from the total mean
            motion before this function is called. Note that no adjustment is
            needed for subtables for days and months whose first value is zero!
            This is handled by :meth:`get_days_in_arg`. Finally, in order for
            the estimation to work for periods up to 600 years, the preliminary
            parameter should be accurate to 5 seconds of arc.
        '''
        estimated_motion = days_in_arg * preliminary_param
        rotations = round(abs(estimated_motion - total_mean_motion) / 360)
        return (total_mean_motion + rotations * 360) / days_in_arg

class Col(Subtable):
    name = 'collected years'

    def __init__(
            self,
            cal: mmcs.Calendar,
            args: 'list[str]' = [],
            entries: 'list[str]' = [],
            complete: bool = True,
            longitude: D = 0,
            epoch: D = 0,
            ) -> None:
        self.cal = cal
        self.cal_type = self.cal.__class__.__bases__[0]
        self.args = args
        self.entries = entries
        self.complete = complete
        self.longitude = longitude
        self.epoch = epoch

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days for :param:`arg`.

        :param arg: is assumed to be an integer number of years,
            namely the calendar year for whose beginning the mean position
            is to be found. To find the number of years since the epoch,
            the epoch (which is variable of the Col subtable that needs to
            be pre-set from outside) should be subtracted from the argument.
            This implies that the number of years passed since epoch will
            always be zero for the first argument, since this is the epoch
            and will thus be subtracted from itself.

            If the resulting number of years since the epoch is not a multiple
            of 30 (ar) or 4 (by, ju), it is assumed to be reckoned from the
            beginning of the respective cycles of intercalation and to be
            completed years.
        '''
        arg = D(arg)
        arg -= self.epoch
        if self.cal_type == mmcs.Ar:
            if arg % 30:
                num = 0
                for i in range(1, int(arg + 1)):
                    num += (self.cal.days_in_year_intercal
                                if self.cal.intercal(i) else
                            self.cal.days_in_year_ordinary)
                return num
            else:
                return (arg // 30) * self.cal.days_in_cycle
        elif self.cal_type in {mmcs.By, mmcs.Ju}:
            if arg % 4:
                num = 0
                for i in range(1, int(arg + 1)):
                    num += (self.cal.days_in_year_intercal
                                if self.cal.intercal(i) else
                            self.cal.days_in_year_ordinary)
                return num
            else:
                return (arg // 4) * self.cal.days_in_cycle
        elif self.cal_type in {mmcs.Eg, mmcs.Pe}:
            return arg * self.cal.days_in_year_ordinary
        else:
            raise mmcs.UnknownCalendarError(self.cal)

class Day(Subtable):
    name = 'days'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days for :param:`arg`.
        '''
        return D(arg) if self.complete else (D(arg) - 1)

class Ext(Subtable):
    name = 'extended years'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days for :param:`arg`.
        '''
        arg = D(arg)
        if self.cal_type == mmcs.Ar:
            num = 0
            for i in range(1, int(arg + 1)):
                num += (self.cal.days_in_year_intercal
                            if self.cal.intercal(i) else
                        self.cal.days_in_year_ordinary)
            return num
        elif self.cal_type in {mmcs.By, mmcs.Ju}:
            num = (arg // 4) * self.cal.days_in_cycle
            for i in range(1, int(arg % 4 + 1)):
                num += (self.cal.days_in_year_intercal
                            if self.cal.intercal(i) else
                        self.cal.days_in_year_ordinary)
            return num
        elif self.cal_type in {mmcs.Eg, mmcs.Pe}:
            return arg * self.cal.days_in_year_ordinary
        else:
            raise mmcs.UnknownCalendarError(self.cal)

class Fra(Subtable):
    '''
    A special type of subtable found in incidental Islamic tables, which
    gives the mean motion in a half, third, two-thirds, fourth etc. hour.
    '''
    name = 'fractions'

    def get_argstring(self, arg: D) -> str:
        return '1.5' if arg == 1 else str(arg)

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days (possibly a fraction of a day) for :param:`arg`.
        '''
        assert arg > 0
        arg = D(arg)
        return D(2) / 3 if arg == 1 else 1 / arg

class Hou(Subtable):
    name = 'hours'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days (usually a fraction of a day) for :param:`arg`.
        '''
        return D(arg) / 24

class Loa(Subtable):
    '''
    A special type of subtable that tabulates the corrections in mean motions
    to be applied for geographical longitudes different from those for which
    the table was set up. Its arguments are **actual longitudes** (e.g. 70,
    71, 72, …, 100°). The difference for one degree of longitude corresponds
    to the mean motion in 1/360th of a day, i.e. four minutes of time.
    '''
    name = 'longitudes'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days (usually a fraction of a day) for :param:`arg`.
        '''
        return (D(arg) - self.longitude) / 360

class Lod(Subtable):
    '''
    A special type that tabulates the corrections in mean motions
    to be applied for geographical longitudes different from those for
    which the table was set up. Its arguments are **longitude differences**
    from the base meridian of the table (e.g. 15, 14, 13, …, 2, 1° west,
    0°, 1, 2, 3, …, 15° east). The difference for one degree of
    longitude corresponds to the mean motion in 1/360th of a day,
    i.e. four minutes of time.
    '''
    name = 'longitude differences'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days (usually a fraction of a day) for :param:`arg`.
        '''
        return D(arg) / 360

class Mon(Subtable):
    name = 'months'

    def __init__(
            self,
            cal: mmcs.Calendar,
            args: 'list[str]' = [],
            entries: 'list[str]' = [],
            complete: bool = True,
            longitude: D = 0,
            is_leap_year: bool = False,
            first_month: int = 1,
            ) -> None:
        shift = first_month - 1
        if shift:
            cal.months = cal.months[shift:] + cal.months[:shift]
        self.cal = cal
        self.cal_type = self.cal.__class__.__bases__[0]
        self.args = args
        self.entries = entries
        self.complete = complete
        self.longitude = longitude
        self.is_leap_year = is_leap_year

    def get_argstring(self, arg: D) -> str:
        if arg == len(self.cal.months) + 1:
            return 'full year'
        elif arg == len(self.cal.months) + 2:
            return 'leap year'
        assert 0 < arg <= len(self.cal.months)
        return self.cal.months[arg - 1][2][0]

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days for :param:`arg`.
        '''
        if arg == len(self.cal.months) + 1:
            return D(self.cal.days_in_year_ordinary)
        elif arg == len(self.cal.months) + 2:
            return D(self.cal.days_in_year_intercal)
        assert 0 < arg <= len(self.cal.months)
        if not self.complete:
            arg -= 1
        return D(sum( m[self.is_leap_year]
                for m in self.cal.months[0:int(arg)] ))

class Par(Subtable):
    name = 'parts of hours/minutes'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days (usually a fraction of a day) for :param:`arg`.
        '''
        return D(arg) / 1440

class Sin(Subtable):
    name = 'single years'

    def get_days_in_arg(self, arg: D) -> D:
        '''
        Give the number of days for :param:`arg`.

        :param arg: is assumed to be a number of completed years that is a
            multiple of 30 for ar and of 4 for by and ju.
        '''
        arg = D(arg)
        if self.cal_type == mmcs.Ar:
            return (arg // 30) * self.cal.days_in_cycle
        elif self.cal_type in {mmcs.By, mmcs.Ju}:
            return (arg // 4) * self.cal.days_in_cycle
        elif self.cal_type in {mmcs.Eg, mmcs.Pe}:
            return arg * self.cal.days_in_year_ordinary
        else:
            raise mmcs.UnknownCalendarclassError(self.cal)
