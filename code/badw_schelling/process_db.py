# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
import os
import subprocess
import sys
import time
import traceback
from os.path import join

import gehalt
import __init__
import fs
import sql_io
import sys_io

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Refresh or clean up data in an ongoing background process.

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Process the database.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    debug = config['connection'].getboolean('debug')
    db_user, db_password = fs.get_keys(paths['db_access'])
    connect = sql_io.get_connect(db_user, db_password, config)
    exception_old = ''
    counter_to_next_db_dump = 0
    while True:
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        time.sleep(4)
        counter_to_next_db_dump += 1
        if counter_to_next_db_dump >= 10:
            counter_to_next_db_dump = 0
            if not debug:
                os.chdir(join(paths['repros'], '..'))
                subprocess.run([
                        'mariadb-dump',
                        'schelling',
                        '--default-character-set=' + config['db']['charset'],
                        '--user=' + db_user,
                        '--password=' + db_password,
                        '--result-file=db.sql',
                        '--ignore-table=schelling._ft',
                        ])
        try:
            with sql_io.DBCon(connect()) as (con, cur):
                for table in tuple(sql_io.tables(cur, config['db']['name'])):
                    time.sleep(2)
                    if table.startswith('_'):
                        continue
                    col_names = {
                            row['COLUMN_NAME'] for row in con.geteach(r"""
                                COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS
                                where TABLE_NAME = %s
                                """, table)
                            }
                    if 'ID' in col_names and any(
                            col_name[0] == '_' for col_name in col_names ):
                        # Trigger all BEFORE-UPDATE-triggers:
                        cur.execute(rf"""
                                UPDATE `{table}` SET `{table}`.ID = `{table}`.ID
                                """)
                        con.commit()
                    if table == 'dokument':
                        for row in con.getall(r"ID from dokument"):
                            time.sleep(0.1)
                            path = join(
                                    paths['repros'], str(row['ID']), 'text.html'
                                    )
                            cur.execute(r"""
                                    update dokument set _text = %s
                                    where ID = %s
                                    """, (int(os.path.isfile(path)), row['ID']))
                            con.commit()
        except: # Sic.
            e = traceback.format_exc()
            if e != exception_old:
                print()
                print(e)
                exception_old = e
            for _ in range(60):
                time.sleep(2)

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
