# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2018 ff. (© http://badw.de)
import json
import os
try: import regex as re
except ImportError: import re
import shutil
import sys
import time
import traceback
import typing as ty
from collections import defaultdict, deque
from os.path import join
from urllib.parse import urlencode

import __init__
import fs
import sql_io
import sys_io
import web_io
import xmlhtml
from xmlhtml import escape

TEI_TEMPLATE = r'''<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
	<teiHeader>
		<fileDesc>
			<titleStmt><title>Transcription {ID}</title></titleStmt>
			<publicationStmt><p><ref target="https://schelling.badw.de/text/{ID}/">https://schelling.badw.de/text/{ID}/</ref> in TEI-XML.</p></publicationStmt>
			<sourceDesc><p>Cf. <ref target="https://schelling.badw.de/doc/{ID}">https://schelling.badw.de/doc/{ID}</ref>.</p></sourceDesc>
		</fileDesc>
	</teiHeader>
	<text>
		{doc}
	</text>
</TEI>
'''

def correct_docpart(
        docpart: str,
        space_re: 'ty.Pattern[str]' = re.compile(r'[\n\r\t ]+'),
        ) -> str:
    new = deque()
    open_tags = deque() # in reverse order for easy ``new.extend``.
    for num, start, attrs, end, text, *_ in xmlhtml.itemize(
            docpart, balance = False):
        if start:
            start, attrs = replace_tag_docpart(start, attrs, False)
            open_tags.appendleft([num, '', attrs, start, '', '', '', '', ''])
        if end: # not ``elif``, for a self-closing tag would have start and end.
            end, attrs = replace_tag_docpart(end, attrs, True)
            if open_tags and open_tags[0][-1] == num:
                open_tags.popleft()
            else:
                new.appendleft([0, end, {}, '', '', '', '', '', ''])
        new.append([num, start, attrs, end, text, '', '', '', ''])
    new.extend(open_tags)
    docpart = xmlhtml.serialize(new)
    docpart = space_re.sub(' ', docpart.strip())
    return docpart

def get_tei(
        doc: str,
        ID: int,
        presubs: 'list[tuple[re.Pattern[str], str]]' = [
            # Asemantic whitespace:
            (re.compile(r'</s>\s+<ins\b'), r'</s><ins'),
            (re.compile(r'</a>\s+</q>'), r'</a></q>'),
            # Abbreviations with square brackets containing the resolution:
            (
                re.compile(r'(>[^<]*?)\[([^<\]]*)\]'),
                r'\g<1><expan>\g<2></expan>',
            ),
            # Tex code between Euro signs:
            (
                re.compile(r'(>[^<]*?)€([^<€]*)€'),
                r'\g<1><formula notation="TeX">\g<2></formula>',
            ),
            ]
        ) -> str:

    def add(
            num: int,
            tag: str,
            newattrs: 'dict[str, str]',
            new: 'deque[list[int, str, dict[str, str], str, str, str, str, str, str]]',
            newtags: 'defaultdict[int, deque[str]]',
            ) -> None:
        new.append([num, tag, newattrs, '', '', '', '', '', ''])
        newtags[num].appendleft(tag)

    for old_re, new in presubs:
        n = 1
        while n:
            doc, n = old_re.subn(new, doc)
    new = deque()
    newtags: 'defaultdict[int, deque[str]]' = defaultdict(deque) # endtag lookup
    in_head: bool = False
    after_s = False
    ins_after_s: 'set[int]' = set()
    cit_sans_quote_end: 'set[int]' = set()
    for num, start, attrs, end, text, *_ in xmlhtml.itemize(doc):
        if start and not in_head:
            newattrs = {}
            # Universal attributes:
            if (data_as := attrs.get('data-as')):
                if start == 'ins':
                    newattrs['place'] = data_as
                elif start in {'del', 're-ins'}:
                    newattrs['rend'] = data_as
            if (data_by := attrs.get('data-by')) and start not in {
                    'article', 'expan', 'ins'}:
                newattrs['resp'] = data_by
            if (data_from := attrs.get('data-from')):
                if start == 'figure':
                    newattrs['source'] = data_from
                else:
                    newattrs['evidence'] = data_from
            if (ID := attrs.get('id')) and not start == 'a':
                newattrs['xml:id'] = ID
            if (lang := attrs.get('lang')):
                newattrs['xml:lang'] = lang
            # Tags:
            if start == 'a':
                if (href := attrs.get('href')):
                    items = href.split('/')
                    if len(items) == 2 and items[0] == '.':
                        newattrs['facs'] = items[1]
                        add(num, 'pb', newattrs, new, newtags)
                    elif len(items) > 2 and items[0] == '':
                        if items[1] == 'bibel':
                            newattrs['type'] = items[1]
                            newattrs['resp'] = items[2]
                            add(num, 'bibl', newattrs, new, newtags)
                        elif items[1] == 'doc':
                            newattrs['type'] = 'document'
                            newattrs['sameAs'] = items[2]
                            add(num, 'bibl', newattrs, new, newtags)
                        elif items[1] in {'lit', 'system'}:
                            newattrs['type'] = (
                                    'literature' if items[1] == 'lit'
                                    else items[1])
                            newattrs['sameAs'] = items[2]
                            if len(items) > 3:
                                newattrs['n'] = items[3]
                            add(num, 'bibl', newattrs, new, newtags)
                        elif items[1] == 'ort':
                            newattrs['ref'] = items[2]
                            add(num, 'placeName', newattrs, new, newtags)
                        elif items[1] == 'person':
                            newattrs['ref'] = items[2]
                            add(num, 'persName', newattrs, new, newtags)
                        elif items[1] == 'thema':
                            newattrs['n'] = items[2]
                            add(num, 'index', newattrs, new, newtags)
                elif (ID := attrs.get('id')):
                    items = ID.split('/')
                    if 2 == len(items) and items[0] == '.':
                        newattrs['source'] = items[1]
                        add(num, 'pb', newattrs, new, newtags)
                else:
                    add(num, 'reg', newattrs, new, newtags)
            elif start == 'article':
                add(num, 'body', newattrs, new, newtags)
            elif start == 'b':
                newattrs['rend'] = 'b'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'big':
                newattrs['rend'] = 'larger'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'br':
                add(num, 'lb', newattrs, new, newtags)
            elif start == 'c-low':
                newattrs['cert'] = 'low'
                add(num, 'unclear', newattrs, new, newtags)
            elif start == 'c-mid':
                newattrs['cert'] = 'high'
                add(num, 'unclear', newattrs, new, newtags)
            elif start.startswith('col-'):
                newattrs['rend'] = start.split('-')[-1]
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'del':
                add(num, 'del', newattrs, new, newtags)
            elif start == 'drop-cap':
                newattrs['rend'] = 'in'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'e-a':
                newattrs['type'] = 'ea'
                add(num, 'note', newattrs, new, newtags)
            elif start == 'em':
                newattrs['rend'] = 'g'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'expan': # produced by one of the ``presubs``.
                add(num, 'expan', newattrs, new, newtags)
            elif start == 'figure':
                time_place = ''
                for key, value in attrs.items():
                    if key == 'class':
                        newattrs['type'] = value
                    elif key == 'data-from':
                        newattrs['source'] = value
                    elif key == ['data-time']:
                        time_place = f'{value} {time_place}'
                    elif key == ['data-place']:
                        time_place = f'{time_place} {value}'
                time_place = time_place.strip()
                if time_place:
                    newattrs['ana'] = time_place
                add(num, 'figure', newattrs, new, newtags)
            elif start == 'font':
                newattrs['rend'] = 'aq'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'formula': # produced by one of the presubs.
                newattrs['notation'] = 'TeX'
                add(num, 'formula', newattrs, new, newtags)
            elif start in {'h1', 'h2', 'h3', 'h4', 'h5', 'h6'}:
                newattrs['n'] = start[1]
                add(num, 'head', newattrs, new, newtags)
            elif start == 'head':
                in_head = True
            elif start == 'hr':
                newattrs['unit'] = 'section'
                add(num, 'milestone', newattrs, new, newtags)
            elif start == 'i':
                newattrs['rend'] = 'i'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'ins':
                add(
                        num,
                        'supplied' if attrs.get('data-by') == 'hg' else 'add',
                        newattrs, new, newtags)
                if after_s:
                    ins_after_s.add(num)
                    after_s = False
            elif start == 'li':
                add(num, 'item', newattrs, new, newtags)
            elif start == 'meta':
                add(num, 'ptr', newattrs, new, newtags)
            elif start == 'note-b':
                newattrs['place'] = 'bottom'
                add(num, 'note', newattrs, new, newtags)
            elif start == 'note-f':
                newattrs['place'] = 'foot'
                add(num, 'note', newattrs, new, newtags)
            elif start == 'note-l':
                newattrs['place'] = 'left'
                add(num, 'note', newattrs, new, newtags)
            elif start == 'note-r':
                newattrs['place'] = 'right'
                add(num, 'note', newattrs, new, newtags)
            elif start == 'note-t':
                newattrs['place'] = 'top'
                add(num, 'note', newattrs, new, newtags)
            elif start == 'note-x':
                add(num, 'note', newattrs, new, newtags)
            elif start == 'p':
                if attrs.get('class') == 'stanza':
                    add(num, 'lg', newattrs, new, newtags)
                else:
                    add(num, 'p', newattrs, new, newtags)
            elif start == 'paste-in':
                newattrs['rend'] = 'print'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'q':
                add(num, 'cit', newattrs, new, newtags)
                new.append([num, 'quote', {}, '', '', '', '', '', ''])
                cit_sans_quote_end.add(num)
            elif start == 're-ins':
                add(num, 'restore', {}, new, newtags)
                add(num, 'del', newattrs, new, newtags)
            elif start == 's':
                new.append([num, 'subst', {}, '', '', '', '', '', ''])
                add(num, 'del', newattrs, new, newtags)
            elif start == 'section':
                add(num, 'div', newattrs, new, newtags)
            elif start == 'small':
                newattrs['rend'] = 'smaller'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'small-caps':
                newattrs['rend'] = 'k'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'span':
                if attrs.get('class') == 'padding-l':
                    newattrs['dim'] = 'horizontal'
                    add(num, 'space', newattrs, new, newtags)
                elif attrs.get('xml:lang'):
                    add(num, 'foreign', newattrs, new, newtags)
            elif start == 'sub':
                newattrs['rend'] = 'sub'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'sup':
                newattrs['rend'] = 'sup'
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'table':
                add(num, 'table', newattrs, new, newtags)
            elif start == 'time':
                if (datetime := attrs.get('datetime')):
                    if '--' in datetime:
                        (
                        newattrs['from'],
                        newattrs['to']
                        ) = datetime.split('--', 1)
                    else:
                        newattrs['when'] = datetime
                add(num, 'date', newattrs, new, newtags)
            elif start == 'td':
                add(num, 'cell', newattrs, new, newtags)
            elif start == 'tr':
                add(num, 'row', newattrs, new, newtags)
            elif start == 'u':
                try:
                    data_n = int(attrs['data-n'])
                except KeyError:
                    data_n = 1
                newattrs['rend'] = 'u' * data_n
                add(num, 'hi', newattrs, new, newtags)
            elif start == 'ul':
                add(num, 'list', newattrs, new, newtags)
        if end: # Not ``elif``: due to self-closing tags.
            if end == 'head':
                in_head = False
            elif end == 's':
                after_s = True
            elif num in cit_sans_quote_end:
                cit_sans_quote_end.remove(num)
                quote_end = [num, '', {}, 'quote', '', '', '', '', '']
                for i in range(len(new) - 1, -1, -1):
                    if new[i][1] == 'bibl':
                        new.insert(i, quote_end)
                        break
                    elif new[i][1] in {'cit', 'quote'}:
                        new.append(quote_end)
                        break
            for tag in newtags.get(num, []):
                new.append([num, '', {}, tag, '', '', '', '', ''])
                if num in ins_after_s:
                    new.append([num, '', {}, 'subst', '', '', '', '', ''])
                    ins_after_s.remove(num)
        elif text and not in_head:
            new.append([0, '', {}, '', text, '', '', '', ''])
    doc = xmlhtml.serialize(new)
    doc = re.sub(r'\n\s*(?=\n)', '', doc).strip()
    doc = TEI_TEMPLATE.format(ID = ID, doc = doc).strip()
    return doc

def get_thema_matches(
        doc: str,
        ) -> 'ty.Generator[tuple[re.Match, ty.Literal["kapitel", "thema"]]]':
    '''
    Yield matches and their kind for certain milestone sections in :param:`doc`:

    - Either: the milestone is a thema annotation, and its span reaches to a new
      thema annotation or to the end of the text.
    - Or: the milestone is a kapitel annotation, and its span reaches to a thema
      annotation or to a new kapitel annotation or to the end of the text.
    '''
    for m in re.finditer(r'''(?sx)
            <a\ href="/thema/(?P<thema_id>\d+)[^>]*></a>
            (?P<text>(?:(?!<a\ href="/thema/).)*)
            ''',
            doc):
        yield m, 'thema'
    for m in re.finditer(r'''(?sx)
            <a\ href="/kapitel/(?P<thema_id>\d+)[^>]*></a>
            (?P<text>(?:(?!<a\ href="/(?:kapitel|thema)/).)*)
            ''',
            doc):
        yield m, 'kapitel'

def process_doc(
        ID: int,
        doc: str,
        notes: 'list[str]',
        connect: '''ty.Callable[
            [], tuple[sql_io.mysql.Connection, sql_io.mysql.Cursor]]''',
        space_re: 'ty.Pattern[str]' = re.compile(r'[\n\r\t ]+'),
        trailing_space_re: 'ty.Pattern[str]' = re.compile(r'[\n\r\t ]+\n'),
        br_re: 'ty.Pattern[str]' = re.compile(r'<br/>[\n\r\t ]*'),
        ) -> str:
    '''
    - Normalize the HTML-XML document :param:`doc` (whose ID is :param:`ID`).
    - Get the highest ID-integer used in the document;
    - add an each time incremented ID-integer as ID to any p element without ID.
    - read annotations and ingest relations derived from them into the database.
    '''
    replacer = xmlhtml.Replacer(
            replace_comment = lambda comment, parser: comment,
            replace_declaration = lambda decl, parser: decl,
            replace_tag = replace_tag,
            info = {
                'max_num': 0,
                'rels': {
                    'dokument': set(),
                    'literatur': set(),
                    'ort': set(),
                    'person': set(),
                    }
                },
            )
    replacer.parse(doc)
    max_num = replacer.info['max_num']
    doc = ''.join(replacer.results)
    old = xmlhtml.itemize(doc)
    new = deque()
    in_block = []
    blocks = {'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'p', 'td', 'th'}
    while old:
        item = old.popleft()
        # Provide every p element with an id attribute based on ``max_num``:
        if item[1] in blocks:
            in_block.append(item[0])
        if item[3] in blocks: # Not ``elif``: due to self-closing tags.
            in_block.pop()
        elif item[4] and in_block:
            item[4] = space_re.sub(' ', item[4])
        if item[1] == 'p' and not item[2].get('id', ''):
            max_num += 1
            item[2]['id'] = str(max_num)
        new.append(item)
    doc = xmlhtml.serialize(new)
    doc = trailing_space_re.sub(r'\n', doc)
    doc = br_re.sub(r'<br/>\n', doc)
    with sql_io.DBCon(connect()) as (con, cur):
        try:
            # Relations into _system_dokument:
            cur.execute(
                    r"delete from _system_dokument where dokument_ID = %s",
                    (ID,))
            for row in con.getall(r"ID from `system` where ID <> 1"):
                system_ID = row['ID']
                if re.search(
                        r'href="/bibel\b' if system_ID == 2
                        else rf'href="/system/{system_ID}\b',
                        doc):
                    cur.execute(r"""
                            insert into _system_dokument
                            (system_ID, dokument_ID) values
                            (%s, %s)
                            """, (system_ID, ID))
            con.commit()

            # Relations into _thema_dokument:
            cur.execute(
                    r"delete from _thema_dokument where dokument_ID = %s",
                    (ID,))
            for m, art in get_thema_matches(doc):
                docpart = correct_docpart(m.group('text'))
                if docpart:
                    cur.execute(r"""
                            insert ignore -- if thema_ID is non-existent.
                            into _thema_dokument
                            (thema_ID, dokument_ID, art, text) values
                            (%s, %s, %s, %s)
                            """, (
                                m.group('thema_id'),
                                ID,
                                art,
                                docpart,
                                ))
            con.commit()

            # Relations into dokument_dokument:
            cur.execute(r"""
                    update dokument_dokument set _auto = 2
                    where dokument_ID_2 = %s
                    and art = 'erwähnt in'
                    and _auto = 1
                    """, (ID,))
            con.commit()
            for dokument_ID in replacer.info['rels']['dokument']:
                try:
                    cur.execute(r"""
                            insert into dokument_dokument
                            (dokument_ID_1, art, dokument_ID_2, _auto) values
                            (%s, 'erwähnt in', %s, 1)
                            on duplicate key update _auto = 1
                            """, (dokument_ID, ID))
                    con.commit()
                except Exception as e:
                    notes.append(f"""<li>
In <a href="https://schelling.badw.de/ablage/{ID}">{ID}</a>/text.html:
dokument_ID {dokument_ID}: {escape(str(e))}</li>""")

            cur.execute(r"""
                    delete from dokument_dokument
                    where dokument_ID_2 = %s
                    and art = 'erwähnt in'
                    and _auto = 2
                    """, (ID,))
            con.commit()

            # Relations into literatur_dokument:
            cur.execute(r"""
                    update literatur_dokument set _auto = 2
                    where dokument_ID = %s and art = 'erwähnt in' and _auto = 1
                    """, (ID,))
            con.commit()
            for literatur_ID in replacer.info['rels']['literatur']:
                try:
                    cur.execute(r"""
                            insert into literatur_dokument
                            (literatur_ID, art, dokument_ID, _auto) values
                            (%s, 'erwähnt in', %s, 1)
                            on duplicate key update _auto = 1
                            """, (literatur_ID, ID))
                    con.commit()
                except Exception as e:
                    notes.append(f"""<li>
In <a href="https://schelling.badw.de/ablage/{ID}">{ID}</a>/text.html:
literatur_ID {literatur_ID}: {escape(str(e))}</li>""")

            cur.execute(r"""
                    delete from literatur_dokument
                    where dokument_ID = %s and art = 'erwähnt in' and _auto = 2
                    """, (ID,))
            con.commit()

            # Relations into person_ort_dokument:
            cur.execute(r"""
                    update person_ort_dokument set _auto = 2
                    where dokument_ID = %s and art = 'erwähnt in' and _auto = 1
                    """, (ID,))
            con.commit()
            for ort_ID in replacer.info['rels']['ort']:
                try:
                    cur.execute(r"""
                            insert into person_ort_dokument
                            (ort_ID, art, dokument_ID, _auto) values
                            (%s, 'erwähnt in', %s, 1)
                            on duplicate key update _auto = 1
                            """, (ort_ID, ID))
                    con.commit()
                except Exception as e:
                    notes.append(f"""<li>
In <a href="https://schelling.badw.de/ablage/{ID}">{ID}</a>/text.html:
ort_ID {ort_ID}: {escape(str(e))}</li>""")

            for person_ID in replacer.info['rels']['person']:
                try:
                    cur.execute(r"""
                            insert into person_ort_dokument
                            (person_ID, art, dokument_ID, _auto) values
                            (%s, 'erwähnt in', %s, 1)
                            on duplicate key update _auto = 1
                            """, (person_ID, ID))
                    con.commit()
                except Exception as e:
                    notes.append(f"""<li>
In <a href="https://schelling.badw.de/ablage/{ID}">{ID}</a>/text.html:
person_ID {person_ID}: {escape(str(e))}</li>""")

            cur.execute(r"""
                    delete from person_ort_dokument
                    where dokument_ID = %s and art = 'erwähnt in' and _auto = 2
                    """, (ID,))
            con.commit()
        except sql_io.mysql.err.OperationalError as e:
            print(e)
    return doc

def replace_tag(
        tag: str,
        attrs: 'dict[str, None|str]',
        startend: bool,
        parser: 'xmlhtml.Replacer',
        old_url: str = 'http://schelling.badw.de',
        new_url: str = 'https://schelling.badw.de',
        white_re: 're.Pattern[str]' = re.compile(r'\s+'),
        ) -> 'tuple[str, dict[str, str], bool]':
    for key in attrs:
        attrs[key] = white_re.sub(
                r' ', attrs[key] or ''
                ).strip().replace(' /', '/').replace('/ ', '/')
    if tag == 'html' and 'class' in attrs:
        classes = set(attrs['class'].split())
        classes.discard('desk')
        attrs['class'] = ' '.join(sorted(classes))
    ID = attrs.get('id', '')
    if ID:
        ID = ID.strip().lstrip('#')
        if ID:
            # A ``'#'`` may have been l-stripped, thus:
            attrs['id'] = ID
        else:
            # We have no id-attribute, thus:
            attrs.pop('id', '')
        if ID.isdigit():
            num = int(ID)
            if num > parser.info['max_num']:
                parser.info['max_num'] = num
    if tag == 'link' and 'href' in attrs:
        attrs['href'] = attrs['href'].replace(old_url, new_url)
    elif tag == 'script' and 'src' in attrs:
        attrs['src'] = attrs['src'].replace(old_url, new_url)
    elif tag == 'a':
        href = attrs.get('href', '')
        if href.count('/') > 1:
            ID = href.split('/')[2]
            if ID.isdigit():
                ID = int(ID)
                if href.startswith('/doc/'):
                    parser.info['rels']['dokument'].add(ID)
                elif href.startswith('/lit/'):
                    parser.info['rels']['literatur'].add(ID)
                elif href.startswith('/ort/'):
                    parser.info['rels']['ort'].add(ID)
                elif href.startswith('/person/'):
                    parser.info['rels']['person'].add(ID)
    return tag, attrs, tag in xmlhtml.HTML_VOIDS

def replace_tag_docpart(
        tag: str,
        attrs: 'dict[str, None|str]',
        end: bool,
        ) -> 'tuple[str, dict[str, str]]':
    if tag in {'a', 'article', 'body', 'html', 'main', 'section'}:
        tag = ''
    elif tag in {'h1', 'h2', 'h3', 'h4', 'h5', 'h6'}:
        tag = 'p'
    attrs = {}
    return tag, attrs

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Refresh or clean up data in an ongoing background process.

    - Exit, when no process with :param`pid` is running anymore.
      This should be the process ID of the process having started this function.
    - Process all translations found in a folder of repros:

      - Normalize the document.
      - Ensure every paragraph has an ID attribute.
      - Read the annotations and ingest annotated relations into the database.
      - Validate the document and collect any error notes.

    - Write the notes into an HTML-XML fragment and save it at a place specified
      in the configuration. It will be accessible on the projectʼs website.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log`
    instance.

    :param urdata: paths to files containing the configuration.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {'paths_from_config_folder': urdata[0]})
    connect = sql_io.get_connect(*fs.get_keys(paths['db_access']), config)
    exception_old = ''
    main_name = 'text.html'
    xml_main_name = 'text.xml'
    while True:
        print('EIN GESAMTGANG BEGINNT.')
        if not sys_io.pid_exists(pid):
            print(f'GESAMTENDE, DA AUFRUFENDER VORGANG {pid} ENDETE.')
            sys.exit(0)
        time.sleep(10)
        try:
            notes = []
            dtd = fs.read(join(paths['repros'], 'text.dtd'))
            for dirname in os.listdir(paths['repros']):
                time.sleep(1)
                if not dirname.isdigit():
                    continue # ``dirname`` must be a document ID.
                dirpath = join(paths['repros'], dirname)
                if not os.path.isdir(dirpath):
                    continue # It may have been deleted and must not be a file.
                with sql_io.DBCon(connect()) as (con, cur):
                    if not (
                            dirname.isdigit() and
                            con.get("1 from dokument where ID = %s", (dirname,))
                            ):
                        notes.append(f"<li>Name “{ID}”: non-dokument_ID.</li>")
                        continue
                ID = int(dirname)
                for name in os.listdir(dirpath):
                    name_lower = name.lower()
                    if name != name_lower:
                        os.replace(
                                join(dirpath, name),
                                join(dirpath, name_lower))
                names = set(os.listdir(dirpath))
                for name in names:
                    path = join(dirpath, name)
                    ext = os.path.splitext(name)[1]
                    if not os.path.isfile(path):
                        shutil.rmtree(path, ignore_errors = True)
                    elif ext == 'xml':
                        if name != xml_main_name:
                            if xml_main_name in names:
                                os.remove(path)
                            else:
                                os.replace(path, join(dirpath, xml_main_name))
                    elif (
                            ext == 'html'
                            and not name.startswith(main_name)
                            and not main_name in names):
                        os.replace(path, join(dirpath, main_name))
                        names.add(main_name)
                for name in os.listdir(dirpath):
                    path = join(dirpath, name)
                    if name == main_name and os.path.isfile(path):
                        # ``path`` may have been deleted.
                        time.sleep(0.2)
                        try:
                            old_doc = fs.read(path)
                        except Exception as e:
                            notes.append(f"""
<li>In <a href="https://schelling.badw.de/ablage/{ID}">{ID}</a>/text.html:
{escape(str(e))}</li>""")
                            continue
                        new_doc = process_doc(ID, old_doc, notes, connect)
                        if new_doc != old_doc and os.path.isfile(path):
                            # ``path`` may have been deleted.
                            fs.write(new_doc, path)
                        report = json.loads(
                                web_io.read(
                                    'https://dienst.badw.de/validate',
                                    None,
                                    request_kwargs = {
                                        'data': urlencode(
                                            {'doc': new_doc, 'dtd': dtd}
                                            ).encode(),
                                        'method': 'POST',
                                        }))
                        if not report['valid']:
                            note = f"""<li>
In <a href="https://schelling.badw.de/ablage/{dirname}">{dirname}</a>/text.html:
{escape(report['error'])}</li>"""
                            notes.append(note)
                        xml_target_path = join(paths['tei'], f'{dirname}.xml')
                        tei_doc = get_tei(new_doc, ID)
                        fs.write(tei_doc, fs.pave(xml_target_path))
            fs.write(
                    f"""<article class="just sheet wide">
<h1>Bericht des Hintergrundprogrammes über die schriftlichen Reproduktionen</h1>
<p>München,
 <time
  datetime="{time.strftime('%Y-%m-%dT%H%M%S', time.localtime())}"
 >{time.strftime('%d.%m.%Y, %H:%M:%S', time.localtime())}</time>
</p>
<ol>{''.join(notes)}</ol></article>""",
                    paths['reproreport'])
        except: # Sic.
            e = traceback.format_exc()
            if e != exception_old:
                print()
                print(e)
                exception_old = e
            for _ in range(60):
                time.sleep(2)
        print('EIN GESAMTGANG ENDET.')

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
