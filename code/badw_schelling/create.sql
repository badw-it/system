-- Licensed under http://www.apache.org/licenses/LICENSE-2.0
-- Attribution notice: by Stefan Müller in 2023 ff. (© http://badw.de)

-- Any column whose name starts with “_” exists for display purposes only and will be filled automatically.
-- All string columns are written with XML escaping and may contain tags.

CREATE TABLE `dokument` (
	ID           BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`art`        ENUM('', 'Abschrift', 'Amtliches Schreiben', 'Brief', 'Brieffragment', 'Briefkonzept', 'Druck', 'Jahreskalender', 'Nachschrift', 'Poetisches', 'Wissenschaftliche Arbeit') NOT NULL DEFAULT '',
	`datum`      VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`datum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31.
	`_verfasser` VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger` VARCHAR(768) NOT NULL DEFAULT '',
	`_schreiber` VARCHAR(768) NOT NULL DEFAULT '',
	`titel`      TEXT NOT NULL DEFAULT '',
	`standort`   VARCHAR(768) NOT NULL DEFAULT '',
	`signatur`   VARCHAR(768) NOT NULL DEFAULT '',
	`_ordner`    VARCHAR(768) NOT NULL DEFAULT '<button onclick="openDir(event)">&#128194;</button>',
	`stand`      ENUM('i.A.', 'veröff.', 'gesondert') NOT NULL DEFAULT 'i.A.',
	`bemerkung`  TEXT NOT NULL DEFAULT '',
	`anm`        TEXT NOT NULL DEFAULT '',
	`_text`      BOOLEAN NOT NULL DEFAULT 0,
	INDEX (art),
	INDEX (stand),
	PRIMARY KEY (ID)
);
CREATE TABLE `dokument_dokument` (
	ID              BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`dokument_ID_1` BIGINT UNSIGNED DEFAULT NULL,
	`_datum_1`      VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser_1`  VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger_1`  VARCHAR(768) NOT NULL DEFAULT '',
	`_titel_1`      TEXT NOT NULL DEFAULT '',
	`art`           ENUM('', 'Antwort auf', 'Abschrift von', 'Begleitbrief von', 'Beilage zu', 'erschlossen aus', 'erwähnt in', 'notiert auf', 'Konzept von') NOT NULL DEFAULT '',
	`dokument_ID_2` BIGINT UNSIGNED DEFAULT NULL,
	`_datum_2`      VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser_2`  VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger_2`  VARCHAR(768) NOT NULL DEFAULT '',
	`_titel_2`      TEXT NOT NULL DEFAULT '',
	`anm`           TEXT NOT NULL DEFAULT '',
	`_auto`         TINYINT NOT NULL DEFAULT 0,
	INDEX (_auto),
	UNIQUE (art, dokument_ID_1, dokument_ID_2),
	FOREIGN KEY (dokument_ID_1) REFERENCES dokument(ID),
	FOREIGN KEY (dokument_ID_2) REFERENCES dokument(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `literatur` (
	ID                  BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`art`               ENUM('', 'Reihe', 'Buch', 'Teil') NOT NULL DEFAULT '',
	`erscheinungsjahr`  VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`erscheinungsjahr` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31.
	`_verfasser`        VARCHAR(768) NOT NULL DEFAULT '',
	`externverfasser`   VARCHAR(768) NOT NULL DEFAULT '', -- wenn Person nicht in DB, sonst über person_literatur.
	`_herausgeber`      VARCHAR(768) NOT NULL DEFAULT '',
	`externherausgeber` VARCHAR(768) NOT NULL DEFAULT '', -- wenn Person nicht in DB, sonst über person_literatur.
	`_übersetzer`       VARCHAR(768) NOT NULL DEFAULT '',
	`externübersetzer`  VARCHAR(768) NOT NULL DEFAULT '', -- wenn Person nicht in DB, sonst über person_literatur.
	`titel`             TEXT DEFAULT NULL,
	`bandnr.`           VARCHAR(768) NOT NULL DEFAULT '',
	`seiten`            VARCHAR(768) NOT NULL DEFAULT '',
	`verlag`            VARCHAR(768) NOT NULL DEFAULT '',
	`verlagsort`        VARCHAR(768) NOT NULL DEFAULT '', -- Verweis auf ort lohnt sich nicht.
	`link`              VARCHAR(768) NOT NULL DEFAULT '',
	`zitiert_in_AA`     TEXT NOT NULL DEFAULT '',
	`in__literatur_ID`  BIGINT UNSIGNED DEFAULT NULL,
	`anm`               TEXT NOT NULL DEFAULT '',
	INDEX (art),
	FOREIGN KEY (in__literatur_ID) REFERENCES literatur(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `literatur_dokument` (
	ID             BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`literatur_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_datum_1`     VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser_1` VARCHAR(768) NOT NULL DEFAULT '',
	`_titel_1`     TEXT NOT NULL DEFAULT '',
	`art`          ENUM('', 'Ausgabe von', 'Beilage in', 'Druck von', 'erwähnt in', 'zitiert in') NOT NULL DEFAULT '',
	`dokument_ID`  BIGINT UNSIGNED DEFAULT NULL,
	`_datum_2`     VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser_2` VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger_2` VARCHAR(768) NOT NULL DEFAULT '',
	`_titel_2`     TEXT NOT NULL DEFAULT '',
	`anm`          TEXT NOT NULL DEFAULT '',
	`_auto`        TINYINT NOT NULL DEFAULT 0,
	INDEX (_auto),
	UNIQUE (art, literatur_ID, dokument_ID),
	FOREIGN KEY (literatur_ID) REFERENCES literatur(ID),
	FOREIGN KEY (dokument_ID)  REFERENCES dokument(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `ort` (
	ID                 BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name`             VARCHAR(768) NOT NULL DEFAULT '',
	`liegt_in__ort_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_liegt_in__ort`   VARCHAR(768) NOT NULL DEFAULT '',
	`anm`              TEXT NOT NULL DEFAULT '',
	FOREIGN KEY (liegt_in__ort_ID) REFERENCES ort(ID),
	PRIMARY KEY (ID)
); -- auch Einrichtungen (wie die BAdW). Sie können Verfasser oder Empfänger von Dokumenten sein.
CREATE TABLE `person` (
	ID              BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`art`           ENUM('', 'biblisch', 'literarisch', 'mythisch', 'real') NOT NULL DEFAULT 'real',
	`name`          VARCHAR(768) NOT NULL DEFAULT '', -- “Ehename|Mädchenname, Vorname”
	`geburtsdatum`  VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`geburtsdatum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31.
	`todesdatum`    VARCHAR(768) NOT NULL DEFAULT '' COMMENT '(date)' CHECK(`todesdatum` RLIKE '^((ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?( [-/] (ca\\. )?(([?0-3][?0-9]\\.)?[?01][?0-9]\\.)?(\\?|\\d)+( [nv]\\.Chr\\.)?)?)*$'), -- Type DATE could store only dates between 1000-01-01 and 9999-12-31.
	`geburtsort_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_geburtsort`   VARCHAR(768) NOT NULL DEFAULT '',
	`todesort_ID`   BIGINT UNSIGNED DEFAULT NULL,
	`_todesort`     VARCHAR(768) NOT NULL DEFAULT '',
	`leben`         TEXT NOT NULL DEFAULT '',
	`gnd`           VARCHAR(768) NOT NULL DEFAULT '',
	`zitiert_in_AA` TEXT NOT NULL DEFAULT '',
	`anm`           TEXT NOT NULL DEFAULT '',
	INDEX (art),
	FOREIGN KEY (geburtsort_ID) REFERENCES ort(ID),
	FOREIGN KEY (todesort_ID)   REFERENCES ort(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `person_literatur` (
	ID             BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`person_ID`    BIGINT UNSIGNED DEFAULT NULL,
	`_person`      VARCHAR(768) NOT NULL DEFAULT '',
	`art`          ENUM('', 'Herausgeber von', 'Übersetzer von', 'Verfasser von') NOT NULL DEFAULT '',
	`literatur_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_datum`       VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser`   VARCHAR(768) NOT NULL DEFAULT '',
	`_titel`       TEXT NOT NULL DEFAULT '',
	`anm`          TEXT NOT NULL DEFAULT '',
	UNIQUE (art, person_ID, literatur_ID),
	FOREIGN KEY (literatur_ID) REFERENCES literatur(ID),
	FOREIGN KEY (person_ID)    REFERENCES person(ID),
	PRIMARY KEY	(ID)
);
CREATE TABLE `person_ort_dokument` (
	ID            BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`person_ID`   BIGINT UNSIGNED DEFAULT NULL,
	`_person`     VARCHAR(768) NOT NULL DEFAULT '',
	`ort_ID`      BIGINT UNSIGNED DEFAULT NULL,
	`_ort`        VARCHAR(768) NOT NULL DEFAULT '',
	`art`         ENUM('', 'Absendeort von', 'Empfänger von', 'erwähnt in', 'Schreiber von', 'Verfasser von', 'Zielort von') NOT NULL DEFAULT '',
	`dokument_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_datum`      VARCHAR(768) NOT NULL DEFAULT '',
	`_verfasser`  VARCHAR(768) NOT NULL DEFAULT '',
	`_empfänger`  VARCHAR(768) NOT NULL DEFAULT '',
	`_titel`      TEXT NOT NULL DEFAULT '',
	`anm`         TEXT NOT NULL DEFAULT '',
	`_auto`       TINYINT NOT NULL DEFAULT 0,
	INDEX (_auto),
	UNIQUE (art, dokument_ID, ort_ID),
	UNIQUE (art, dokument_ID, person_ID),
	FOREIGN KEY (person_ID)   REFERENCES person(ID),
	FOREIGN KEY (ort_ID)      REFERENCES ort(ID),
	FOREIGN KEY (dokument_ID) REFERENCES dokument(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `person_person` (
	ID            BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`person_ID_1` BIGINT UNSIGNED DEFAULT NULL,
	`_person_1`   VARCHAR(768) NOT NULL DEFAULT '',
	`art`         ENUM('', 'Adoptivsohn von', 'Arzt von', 'Enkel von', 'Erzieher von', 'Geliebte(r) von', 'Geschwister von', 'Hofdame von', 'Hofmeister von', 'Kind von', 'Lehrer von', 'Mitschüler von', 'Neffe / Nichte von', 'Schwiegerkind von', 'Sekretär von', 'Stiefenkel von', 'Stiefgeschwister von', 'Stiefkind von', 'Täufling von', 'Vetter von', 'befreundet mit', 'verheiratet mit', 'verlobt mit', 'verschwägert mit') NOT NULL DEFAULT '',
	`zweifel`     ENUM('', '?') NOT NULL DEFAULT '',
	`person_ID_2` BIGINT UNSIGNED DEFAULT NULL,
	`_person_2`   VARCHAR(768) NOT NULL DEFAULT '',
	`anm`         TEXT NOT NULL DEFAULT '',
	UNIQUE (art, person_ID_1, person_ID_2),
	FOREIGN KEY (person_ID_1) REFERENCES person(ID),
	FOREIGN KEY (person_ID_2) REFERENCES person(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `system` (
	ID                     BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name`                 VARCHAR(768) NOT NULL DEFAULT '',
	`beschreibung`         TEXT NOT NULL DEFAULT '',
	`gehört_zu__system_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_gehört_zu__system`   VARCHAR(768) NOT NULL DEFAULT '',
	`anm`                  TEXT NOT NULL DEFAULT '',
	FOREIGN KEY (gehört_zu__system_ID) REFERENCES system(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `system_person` (
	ID          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`system_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_system`   VARCHAR(768) NOT NULL DEFAULT '',
	`art`       ENUM('') NOT NULL DEFAULT '',
	`person_ID` BIGINT UNSIGNED DEFAULT NULL,
	`_person`   VARCHAR(768) NOT NULL DEFAULT '',
	`anm`       TEXT NOT NULL DEFAULT '',
	FOREIGN KEY (system_ID) REFERENCES system(ID),
	FOREIGN KEY (person_ID) REFERENCES person(ID),
	PRIMARY KEY (ID)
);
CREATE TABLE `thema` (
	ID             BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`name`         VARCHAR(768) NOT NULL DEFAULT '',
	`beschreibung` TEXT NOT NULL DEFAULT '',
	`anm`          TEXT NOT NULL DEFAULT '',
	PRIMARY KEY (ID)
);
CREATE TABLE `_system_dokument` (
	`system_ID`   BIGINT UNSIGNED DEFAULT NULL,
	`dokument_ID` BIGINT UNSIGNED DEFAULT NULL,
	INDEX (dokument_ID, system_ID),
	FOREIGN KEY (system_ID) REFERENCES `system`(ID),
	FOREIGN KEY (dokument_ID)  REFERENCES dokument(ID),
	PRIMARY KEY (system_ID, dokument_ID)
);
CREATE TABLE `_thema_dokument` (
	`thema_ID`    BIGINT UNSIGNED DEFAULT NULL,
	`dokument_ID` BIGINT UNSIGNED DEFAULT NULL,
	`art`         ENUM('kapitel','thema') NOT NULL DEFAULT 'thema',
	`text`        LONGTEXT NOT NULL DEFAULT '',
	INDEX (dokument_ID, thema_ID),
	FOREIGN KEY (thema_ID) REFERENCES thema(ID),
	FOREIGN KEY (dokument_ID)  REFERENCES dokument(ID),
	PRIMARY KEY (thema_ID, dokument_ID)
);

delimiter //

CREATE TRIGGER on_dokument_before_update
BEFORE UPDATE ON dokument
FOR EACH ROW
BEGIN
	SET NEW._verfasser = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM dokument
		JOIN person_ort_dokument ON dokument.ID = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE dokument.ID = NEW.ID
		AND person_ort_dokument.art = 'Verfasser von'
		);
	SET NEW.`_empfänger` = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM dokument
		JOIN person_ort_dokument ON dokument.ID = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE dokument.ID = NEW.ID
		AND person_ort_dokument.art = 'Empfänger von'
		);
	SET NEW.`_schreiber` = IFNULL((
		SELECT GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / ')
		FROM dokument
		JOIN person_ort_dokument ON dokument.ID = person_ort_dokument.dokument_ID
		JOIN person ON person_ort_dokument.person_ID = person.ID
		WHERE dokument.ID = NEW.ID
		AND person_ort_dokument.art = 'Schreiber von'
		), '');
END//

CREATE TRIGGER on_dokument_dokument_before_update
BEFORE UPDATE ON dokument_dokument
FOR EACH ROW
BEGIN
	SET NEW._datum_1 = IFNULL((SELECT dokument.datum FROM dokument WHERE dokument.ID = NEW.dokument_ID_1), '');
	SET NEW._verfasser_1 = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM dokument_dokument
		JOIN person_ort_dokument ON dokument_dokument.dokument_ID_1 = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE dokument_dokument.dokument_ID_1 = NEW.dokument_ID_1
		AND person_ort_dokument.art = 'Verfasser von'
		);
	SET NEW.`_empfänger_1` = IFNULL((
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM dokument_dokument
		JOIN person_ort_dokument ON dokument_dokument.dokument_ID_1 = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE dokument_dokument.dokument_ID_1 = NEW.dokument_ID_1
		AND person_ort_dokument.art = 'Empfänger von'
		), '');
	SET NEW._titel_1 = IFNULL((SELECT dokument.titel FROM dokument WHERE dokument.ID = NEW.dokument_ID_1), '');
	SET NEW._datum_2 = IFNULL((SELECT dokument.datum FROM dokument WHERE dokument.ID = NEW.dokument_ID_2), '');
	SET NEW._verfasser_2 = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM dokument_dokument
		JOIN person_ort_dokument ON dokument_dokument.dokument_ID_2 = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE dokument_dokument.dokument_ID_2 = NEW.dokument_ID_2
		AND person_ort_dokument.art = 'Verfasser von'
		);
	SET NEW.`_empfänger_2` = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM dokument_dokument
		JOIN person_ort_dokument ON dokument_dokument.dokument_ID_2 = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE dokument_dokument.dokument_ID_2 = NEW.dokument_ID_2
		AND person_ort_dokument.art = 'Empfänger von'
		);
	SET NEW._titel_2 = IFNULL((SELECT dokument.titel FROM dokument WHERE dokument.ID = NEW.dokument_ID_2), '');
END//

CREATE TRIGGER on_literatur_before_update
BEFORE UPDATE ON literatur
FOR EACH ROW
BEGIN
	SET NEW._verfasser = IFNULL((
		SELECT GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / ')
		FROM literatur
		JOIN person_literatur ON literatur.ID = person_literatur.literatur_ID
		JOIN person ON person_literatur.person_ID = person.ID
		WHERE literatur.ID = NEW.ID
		AND person_literatur.art = 'Verfasser von'
		), '');
	SET NEW._herausgeber = IFNULL((
		SELECT GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / ')
		FROM literatur
		JOIN person_literatur ON literatur.ID = person_literatur.literatur_ID
		JOIN person ON person_literatur.person_ID = person.ID
		WHERE literatur.ID = NEW.ID
		AND person_literatur.art = 'Herausgeber von'
		), '');
	SET NEW.`_übersetzer` = IFNULL((
		SELECT GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / ')
		FROM literatur
		JOIN person_literatur ON literatur.ID = person_literatur.literatur_ID
		JOIN person ON person_literatur.person_ID = person.ID
		WHERE literatur.ID = NEW.ID
		AND person_literatur.art = 'Übersetzer von'
		), '');
END//

CREATE TRIGGER on_literatur_dokument_before_update
BEFORE UPDATE ON literatur_dokument
FOR EACH ROW
BEGIN
	SET NEW._datum_1 = IFNULL((
		SELECT literatur.erscheinungsjahr FROM literatur
		WHERE literatur.ID = NEW.literatur_ID
		), '');
	SET NEW._verfasser_1 = IFNULL((
		SELECT GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / ')
		FROM literatur_dokument
		JOIN person_literatur ON literatur_dokument.literatur_ID = person_literatur.literatur_ID
		JOIN person ON person_literatur.person_ID = person.ID
		WHERE literatur_dokument.literatur_ID = NEW.literatur_ID
		AND person_literatur.art = 'Verfasser von'
		), '');
	SET NEW._titel_1 = IFNULL((
		SELECT literatur.titel FROM literatur
		WHERE literatur.ID = NEW.literatur_ID
		), '');
	SET NEW._datum_2 = IFNULL((
		SELECT dokument.datum FROM dokument
		WHERE dokument.ID = NEW.dokument_ID
		), '');
	SET NEW._verfasser_2 = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM literatur_dokument
		JOIN person_ort_dokument ON literatur_dokument.dokument_ID = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE literatur_dokument.dokument_ID = NEW.dokument_ID
		AND person_ort_dokument.art = 'Verfasser von'
		);
	SET NEW.`_empfänger_2` = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM literatur_dokument
		JOIN person_ort_dokument ON literatur_dokument.dokument_ID = person_ort_dokument.dokument_ID
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE literatur_dokument.dokument_ID = NEW.dokument_ID
		AND person_ort_dokument.art = 'Empfänger von'
		);
	SET NEW._titel_2 = IFNULL((
		SELECT dokument.titel FROM dokument
		WHERE dokument.ID = NEW.dokument_ID
		), '');
END//

CREATE TRIGGER on_ort_before_update
BEFORE UPDATE ON ort
FOR EACH ROW
BEGIN
	SET NEW._liegt_in__ort = IFNULL((SELECT ort.name FROM ort WHERE ort.ID = NEW.liegt_in__ort_ID), '');
END//

CREATE TRIGGER on_person_before_update
BEFORE UPDATE ON person
FOR EACH ROW
BEGIN
	SET NEW._geburtsort = IFNULL((SELECT ort.name FROM ort where ort.ID = NEW.geburtsort_ID), '');
	SET NEW._todesort = IFNULL((SELECT ort.name FROM ort where ort.ID = NEW.todesort_ID), '');
END//

CREATE TRIGGER on_person_literatur
BEFORE UPDATE ON person_literatur
FOR EACH ROW
BEGIN
	SET NEW._person = IFNULL((SELECT person.name FROM person where person.ID = NEW.person_ID), '');
	SET NEW._datum = IFNULL((
		SELECT literatur.erscheinungsjahr FROM literatur
		WHERE literatur.ID = NEW.literatur_ID
		), '');
	SET NEW._verfasser = IFNULL((
		SELECT GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / ')
		FROM person_literatur
		JOIN person ON person_literatur.person_ID = person.ID
		WHERE person_literatur.literatur_ID = NEW.literatur_ID
		AND person_literatur.art = 'Verfasser von'
		), '');
	SET NEW._titel = IFNULL((
		SELECT literatur.titel FROM literatur
		WHERE literatur.ID = NEW.literatur_ID
		), '');
END//

CREATE TRIGGER on_person_ort_dokument_before_update
BEFORE UPDATE ON person_ort_dokument
FOR EACH ROW
BEGIN
	SET NEW._person = IFNULL((SELECT person.name FROM person where person.ID = NEW.person_ID), '');
	SET NEW._ort = IFNULL((SELECT ort.name FROM ort where ort.ID = NEW.ort_ID), '');
	SET NEW._datum = IFNULL((SELECT dokument.datum FROM dokument WHERE dokument.ID = NEW.dokument_ID), '');
	SET NEW._verfasser = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM person_ort_dokument
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE person_ort_dokument.dokument_ID = NEW.dokument_ID
		AND person_ort_dokument.art = 'Verfasser von'
		);
	SET NEW.`_empfänger` = (
		SELECT CONCAT_WS(' | ',
			GROUP_CONCAT(DISTINCT person.name ORDER BY person.name SEPARATOR ' / '),
			GROUP_CONCAT(DISTINCT ort.name ORDER BY ort.name SEPARATOR ' / ')
			)
		FROM person_ort_dokument
		LEFT JOIN person ON person_ort_dokument.person_ID = person.ID
		LEFT JOIN ort ON person_ort_dokument.ort_ID = ort.ID
		WHERE person_ort_dokument.dokument_ID = NEW.dokument_ID
		AND person_ort_dokument.art = 'Empfänger von'
		);
	SET NEW._titel = IFNULL((
		SELECT dokument.titel FROM dokument
		WHERE dokument.ID = NEW.dokument_ID
		), '');
END//

CREATE TRIGGER on_person_person_before_update
BEFORE UPDATE ON person_person
FOR EACH ROW
BEGIN
	SET NEW._person_1 = IFNULL((SELECT person.name FROM person WHERE person.ID = NEW.person_ID_1), '');
	SET NEW._person_2 = IFNULL((SELECT person.name FROM person WHERE person.ID = NEW.person_ID_2), '');
END//

CREATE TRIGGER on_system_before_update
BEFORE UPDATE ON system
FOR EACH ROW
BEGIN
	SET NEW.`_gehört_zu__system` = IFNULL((SELECT system.`name` FROM system WHERE system.ID = NEW.`gehört_zu__system_ID`), '');
END//

CREATE TRIGGER on_system_person_before_update
BEFORE UPDATE ON system_person
FOR EACH ROW
 BEGIN
	SET NEW._system = IFNULL((SELECT system.`name` FROM system WHERE system.ID = NEW.system_ID), '');
	SET NEW._person = IFNULL((SELECT person.name FROM person WHERE person.ID = NEW.person_ID), '');
END//

delimiter ;
