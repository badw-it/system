# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import json
import os
import stat
import sys
import time
try:
    from beaker.middleware import SessionMiddleware
except:
    SessionMiddleware = None

import gehalt
import __init__
import bottle
from bottle import HTTPError, redirect, request, response, static_file, template
import fs

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        if not db.debug:
            server.quiet = True
        self.kwargs = {
                'app': self if SessionMiddleware is None else
                    SessionMiddleware(self, db.config['session']),
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.hook('before_request')
        def status():
            request.user_id, request.user_name, request.roles = db.auth(request)

        @self.hook('after_request')
        def cors():
            response.set_header('Access-Control-Allow-Origin', '*')

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which do not specify the page to the default page.
            '''
            redirect(f'/{db.page_id}')

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/ablage/<doc_id:int>')
        def ablage(doc_id):
            '''
            Show the folder of repros.
            '''
            if not request.roles:
                redirect(db.page_id)
            textnames, imagenames = db.list_repros(doc_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'ablage.tpl',
                        'note': request.query.note,
                        'doc_id': doc_id,
                        'textnames': textnames,
                        'imagenames': imagenames,
                        })

        @self.route('/ablage/<doc_id:int>', method = 'POST')
        def ablage_post(doc_id):
            '''
            Add to or delete from the folder of repros a file.
            '''
            if not request.roles:
                redirect(db.page_id)
            note = ''
            try:
                if request.forms.redaction == 'delete':
                    name = os.path.basename(request.forms.filename)
                    assert name, 'No filename given.'
                    path = os.path.join(db.repros_path, str(doc_id), name)
                    os.chmod(path, stat.S_IWRITE)
                    os.remove(path)
                else:
                    for file in request.files.getall('file'):
                        assert file, 'No file given.'
                        name = fs.sanitize(file.raw_filename)
                        assert name, 'No filename given.'
                        path = os.path.join(db.repros_path, str(doc_id), name)
                        file.save(path, overwrite = True, max_size = 1024 ** 3)
            except Exception as e:
                note = str(e)
            note = ('?note=' + db.q(note)) if note else ''
            redirect(f'/ablage/{doc_id}{note}')

        @self.route('/ablage/<doc_id:int>/<filename>')
        def return_ablage_static(doc_id, filename):
            '''
            Return a file from the folder of repros for download (if “export” is
            in the request query) or viewing. For viewing, it is not used in the
            case of the HTML text files containing the transcriptions (though it
            could be). In this case, :func:`return_doc` is used.
            '''
            if not request.roles:
                redirect(db.page_id)
            path_tail = f'{doc_id}/{filename}'
            if 'export' in request.query:
                path = os.path.abspath(os.path.join(db.repros_path, path_tail))
                date = f".{time.strftime('%Y-%m-%d_%H·%M·%S')}.html"
                path_dated = f'{path}{date}'
                path_tail_dated = f'{path_tail}{date}'
                if path.startswith(db.repros_path) and path.endswith('.html'):
                    os.replace(path, path_dated)
                    path_tail = path_tail_dated
                    download = filename
            else:
                download = False
            return static_file(path_tail, db.repros_path, download = download)

        @self.route('/api/gnd/person')
        def return_gnd_person_map():
            '''
            Return a machine-readable mapping from GNDs to persons registered in
            the Geschichtsquellen. (Example usage: Corpus Corporum.)
            '''
            response.content_type = 'application/json'
            return json.dumps(db.get_gnd_person_map())

        @self.route('/auth_admin')
        def auth_admin():
            if not request.roles:
                redirect('/' + db.page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_admin.tpl'})

        @self.route('/auth_admin', method = 'POST')
        def auth_admin_post():
            note = db.auth_new(
                    request.forms,
                    request.user_id,
                    request.roles,
                    db.lang_id)
            redirect('/auth_admin?note=' + db.q(note))

        @self.route('/auth_end')
        def auth_end():
            session = request.environ.get('beaker.session', None)
            if session is not None:
                session.delete()
            redirect(request.query.url or '/' + db.page_id)

        @self.route('/' + db.auth_in_page_id)
        def auth_in():
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_in.tpl'})

        @self.route('/' + db.auth_in_page_id, method = 'POST')
        def auth_in_post():
            user_name = request.forms.user_name
            password = request.forms.password
            user_id = db.auth_in(user_name, password)
            if user_id is None:
                query = request.forms.query
                if query:
                    query = '?' + query
                redirect(request.fullpath + query)
            else:
                session = request.environ['beaker.session']
                session['user_id'] = user_id
                session.save()
                redirect(request.forms.url or '/' + db.page_id)

        @self.route('/bibel')
        @self.route('/bibel/<stelle>')
        def redirect_system(stelle = ''):
            redirect(f'/system/2/{db.q(stelle)}')

        @self.route('/db')
        def show_tables():
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'db.tpl',
                        'tables': filter(
                            lambda table: table['TABLE_NAME'][0] != '_',
                            db.get_tables()),
                        })

        @self.route('/db/<table_name>')
        def return_table(table_name):
            '''
            Return a datatable of :param:`table_name`.
            '''
            cols = tuple(db.describe_table(table_name).values())
            kwargs = {
                    'cols': cols,
                    'cols_json': json.dumps(cols, separators=(',', ':')),
                    'idcol':
                        ([ col for col in cols if col['id?'] ] or [None])[0],
                    'table_config': {
                        'ajax': f'{table_name}/json',
                        'columns': [
                            {
                                'data': str(col['pos']),
                                'render': {'_': '_', 'sort': 's'},
                                }
                            for col in cols
                            ],
                        'order': [
                            [col['pos'], col['sort']] for col in cols
                            if col['sort']
                            ] or [[0, 'desc']],
                        },
                    'table_config_in_text': False,
                    'table_name': table_name,
                    'tpl': 'index.tpl',
                    }
            kwargs['text'] = template(
                    'dbtable.tpl', db = db, request = request, kwargs = kwargs
                    ).encode()
            return template(
                    'base.tpl', db = db, request = request, kwargs = kwargs)

        @self.route('/db/<table_name>', method = 'POST')
        def change_table(table_name):
            '''
            Make the change specified by ``request.forms.act`` to the table told
            by ``request.forms.table_name``.

            :param table_name: occurs here, so that the URL of this post request
                can be derived by the javascript from the get request.
            '''
            note = ''
            row = {}
            try:
                if 'editor' not in request.roles:
                    raise HTTPError(401, 'Unauthorized request.')
                act = request.forms.act
                table_name = request.forms.table_name
                if act == 'delete_row':
                    if 'redactor' not in request.roles:
                        raise HTTPError(401, 'Unauthorized request.')
                    row = db.delete_row(table_name, request.forms.row_id)
                elif act == 'insert_row':
                    if 'redactor' not in request.roles:
                        raise HTTPError(401, 'Unauthorized request.')
                    row = db.insert_row(table_name)
                elif act == 'update_cell':
                    row = db.update_cell(
                            table_name,
                            request.forms.col_name,
                            request.forms.row_id,
                            db.normalize(request.forms.val),
                            )
                else:
                    raise HTTPError(405, f'Method “{act}” not allowed.')
            except Exception as e:
                note = f'Error: {e}'
            return {'note': note, 'row': row}

        @self.route('/db/<table_name>/json')
        def return_table_json(table_name):
            '''
            Return JSON for a datatable of :param:`table_name`.
            '''
            response.content_type = 'application/json'
            where = ('_auto', '=', 0) if table_name in {
                    'dokument_dokument',
                    'literatur_dokument',
                    'person_ort_dokument',
                    } else ()
            return json.dumps(
                    {'data': tuple(db.select_rows(table_name, where))},
                    separators = (',', ':'))

        @self.route('/<kind:re:(doc|kapitel|lit|system|thema|ort|person)>/<item_id:int>')
        @self.route('/<kind:re:(doc|kapitel|lit|system|thema)>/<item_id:int>/<stelle>')
        def return_metapage(kind, item_id, stelle = ''):
            '''
            For the item with the ID :param:`item_id`, return the metadata page

            The item may be a document or a person.
            '''
            form = request.query.form
            template_name = kind
            if kind == 'doc':
                item = db.get_doc(item_id, form)
            elif kind == 'lit':
                item = db.get_lit(item_id, form)
            elif kind == 'ort':
                item = db.get_ort(item_id, form)
            elif kind == 'person':
                item = db.get_person(item_id, form)
            elif kind in {'kapitel', 'system', 'thema'}:
                item = db.get_begriff(item_id, form, kind)
                template_name = 'begriff'
            else:
                item = {}
            if item:
                return template(
                        f'{template_name}.tpl' if form == 'tip'
                        else 'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': f'{template_name}.tpl',
                            'lang_id': db.lang_id,
                            'item': item,
                            'form': form,
                            'kind': kind,
                            'stelle': stelle,
                            })
            else:
                raise HTTPError(404, db.glosses['httperror404'][db.lang_id])

        @self.route('/text/<item_id:int>')
        def redirect_noslash(item_id):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(f'/text/{item_id}/')

        @self.route('/text/<item_id_from:int>/<item_id_to:int>')
        def redirect_relative_reference(item_id_from, item_id_to):
            '''
            Redirect URLs resulting from relative references (e.g. href="█#░").
            '''
            redirect(f'/text/{item_id_to}/')

        @self.route('/text/<item_id:int>/')
        @self.route('/text/<item_id:int>/<filename>')
        def return_text(item_id, filename = ''):
            '''
            Return the text of the document with the ID :param:`item_id`, namely
            from the file named :param:`filename`.
            '''
            if not filename:
                filename = 'text.html'
            elif '.' not in filename:
                filename += '.jpg'
            item = db.get_doc(item_id, 'tip')
            if request.roles or item.get('stand') == 'veröff.':
                if filename.endswith('.html'):
                    text = db.get_text(item_id, filename, request.query)
                    if text:
                        return template(
                                'base.tpl', db = db, request = request,
                                kwargs = {
                                    'tpl': 'text.tpl',
                                    'lang_id': db.lang_id,
                                    'item_id': item_id,
                                    'filename': filename,
                                    'text': text,
                                    'root_class': 'desk edition',
                                    })
                else:
                    return static_file(
                            f'{item_id}/{filename}',
                            db.repros_path,
                            download = ('export' in request.query),
                            )
            raise HTTPError(404, db.glosses['httperror404'][db.lang_id])

        @self.route('/<kind>/json')
        def return_index(kind):
            '''
            Return JSON for a datatable of items of the kind :param:`kind`.
            '''
            response.content_type = 'application/json'
            return json.dumps(
                    {'data': tuple(db.get_index(kind, request.roles))},
                    separators = (',', ':'))

        @self.route('/<page_id>')
        def return_page(page_id):
            '''
            Return the page :param:`page_id`.

            If the given page is not found, raise the HTTP error 404.
            '''
            text, tpl = db.get_text_and_template_name('', page_id)
            if text:
                return template(
                        'base.tpl', db = db, request = request,
                        kwargs = {
                            'tpl': tpl,
                            'lang_id': db.lang_id,
                            'root_class': 'desk_across'
                                if b'class="across"' in text.split(b'>', 1)[0]
                                else 'desk',
                            'text': text,
                            'indexinfolink': '/hinweise#index',
                            })
            else:
                raise HTTPError(404, db.glosses['httperror404'][db.lang_id])

        @self.route('/<path:path>')
        def return_static_content(path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/icons/favicon.ico`
            - `/acta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(2)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
