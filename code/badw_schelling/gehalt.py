# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
import os
try: import regex as re
except ImportError: import re
import subprocess
import sys
import typing as ty
from os.path import join

import __init__
import fs
import sql_io
import web_io
import xmlhtml
from parse import sortday, sortxtext

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.
        '''
        super().__init__(urdata, __file__)

        self.repros_path = os.path.abspath(self.paths['repros']) + os.sep
        self.connect = sql_io.get_connect(
                *fs.get_keys(self.paths['db_access']),
                self.config,
                )

        self.letterlikes = (
                'Abschrift',
                'Amtliches Schreiben',
                'Brief',
                'Brieffragment',
                'Briefkonzept',
                )
        self.letterlikes_set = set(self.letterlikes)
        self.letterlikes_str = "'{}'".format("', '".join(self.letterlikes))

        if self.config['modes'].getboolean('refreshing'):
            self.process_db = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../process_db.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])
            self.process_db = subprocess.Popen([
                    sys.executable,
                    fs.join(__file__, '../process_repros.py'),
                    str(os.getpid()),
                    urdata[0],
                    urdata[1],
                    ])

    def get_begriff(
            self,
            item_id: int,
            form: str,
            kind: str,
            ) -> 'dict[str, int|str|None|list[dict[str, int|str|None]]]':
        '''
        From the database, get the information about the record which has the ID
        :param:`item_id` and is of one of the following cases of :param:`kind`:

        - “system”
        - or “thema” or “kapitel” (i.e. ‘sub-thema’).

        The :param:`form` decides for what the information will be used:

        - “tip”: It will be used for a tooltip.
        - otherwise: It will be used for a whole webpage, therefore includes all
          relational information.
        '''
        if kind == 'kapitel':
            kind = 'thema'
        assert kind in {'system', 'thema'}
        with sql_io.DBCon(self.connect()) as (con, cur):
            item = con.get(rf"* from `{kind}` where ID = %s", item_id)
            if form == 'tip' or not item:
                return item
            item['dokument_rels'] = sorted(con.geteach(rf"""
                        DISTINCT
                        _{kind}_dokument.*,
                        dokument.art as dokument_art,
                        dokument.datum as datum,
                        REPLACE(dokument._verfasser, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as _verfasser,
                        REPLACE(dokument.`_empfänger`, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as `_empfänger`,
                        dokument.titel as titel,
                        dokument.stand as stand,
                        dokument._text as _text
                    from _{kind}_dokument
                    join dokument
                        on _{kind}_dokument.dokument_ID = dokument.ID
                        and dokument.stand != 'gesondert'
                    where _{kind}_dokument.{kind}_ID = %s
                    """, item_id), key = lambda row: (
                        row['dokument_art'] not in self.letterlikes_set,
                        row['dokument_art'],
                        sortday(row['datum']),
                        sortxtext(row['_verfasser']),
                        sortxtext(row['_empfänger']),
                        sortxtext(row['titel']),
                        ))
            item['person_rels'] = sorted(con.geteach(r"""
                        person.ID as person_ID,
                        REPLACE(person.name, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as name,
                        person.geburtsdatum as geburtsdatum,
                        person.todesdatum as todesdatum
                    from person
                    join system_person
                        on person.ID = system_person.person_ID
                    where system_person.system_ID = %s
                    """, item_id), key = lambda row: sortxtext(row['name'])
                    ) if kind == 'system' else []
        return item

    def get_doc(
            self,
            item_id: int,
            form: str,
            ) -> 'dict[str, int|str|None|list[dict[str, int|str|None]]]':
        '''
        From the database, get the information about the record which has the ID
        :param:`item_id` and is in the table “dokument”.

        The :param:`form` decides for what the information will be used:

        - “tip”: It will be used for a tooltip.
        - otherwise: It will be used for a whole webpage, therefore includes all
          relational information.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            item = con.get(r"* from dokument where ID = %s", item_id)
            if form == 'tip' or not item:
                return item
            for θ, ρ in ((1, 2), (2, 1)):
                item[f'dokument_rels_{θ}'] = sorted(con.geteach(rf"""
                            dokument_dokument.art as rel_art,
                            dokument.ID as dokument_ID,
                            dokument.art as art,
                            dokument.datum as datum,
                            REPLACE(dokument._verfasser, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as _verfasser,
                            REPLACE(dokument.`_empfänger`, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as `_empfänger`,
                            dokument.titel as titel,
                            dokument.stand as stand,
                            dokument._text as _text
                        from dokument_dokument
                        join dokument
                            on dokument_dokument.dokument_ID_{ρ} = dokument.ID
                            and dokument.stand != 'gesondert'
                        where dokument_dokument.dokument_ID_{θ} = %s
                        """, item_id), key = lambda row: (
                            row['art'] not in self.letterlikes_set,
                            row['art'],
                            row['rel_art'],
                            sortday(row['datum']),
                            sortxtext(row['_verfasser']),
                            sortxtext(row['_empfänger']),
                            sortxtext(row['titel']),
                            ))
            item['literatur_rels'] = sorted(con.geteach(r"""
                        literatur_dokument.art as rel_art,
                        literatur.ID as literatur_ID,
                        literatur.art as art,
                        literatur.erscheinungsjahr as erscheinungsjahr,
                        literatur._verfasser as _verfasser,
                        literatur.titel as titel
                    from literatur_dokument
                    join literatur
                        on literatur_dokument.literatur_ID = literatur.ID
                    where literatur_dokument.dokument_ID = %s
                    """, item_id), key = lambda row: (
                        sortday(row['erscheinungsjahr']),
                        sortxtext(row['_verfasser']),
                        sortxtext(row['titel']),
                        ))
            item['ort_rels'] = sorted(con.geteach(r"""
                        person_ort_dokument.art as rel_art,
                        ort.ID as ort_ID,
                        ort.name as name,
                        ort.liegt_in__ort_ID as uberort_ID,
                        ort._liegt_in__ort as uberort_name
                    from person_ort_dokument
                    join ort
                        on person_ort_dokument.ort_ID = ort.ID
                    where person_ort_dokument.dokument_ID = %s
                    """, item_id), key = lambda row: (
                        row['rel_art'],
                        sortxtext(row['name']),
                        sortxtext(row['uberort_name']),
                        ))
            item['person_rels'] = sorted(con.geteach(r"""
                        person_ort_dokument.art as rel_art,
                        person.ID as person_ID,
                        REPLACE(person.name, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as name,
                        person.geburtsdatum as geburtsdatum,
                        person.todesdatum as todesdatum
                    from person_ort_dokument
                    join person
                        on person_ort_dokument.person_ID = person.ID
                    where person_ort_dokument.dokument_ID = %s
                    """, item_id), key = lambda row: (
                        row['rel_art'],
                        sortxtext(row['name']),
                        ))
        item['absendeort'] = item['zielort'] = {}
        for rel in item['ort_rels']:
            if rel['rel_art'] == 'Absendeort von':
                item['absendeort'] = rel
            elif rel['rel_art'] == 'Zielort von':
                item['zielort'] = rel
        item['verfasser'] = item['schreiber'] = item['empfänger'] = {}
        for rel in item['person_rels']:
            if rel['rel_art'] == 'Verfasser von':
                item['verfasser'] = rel
            elif rel['rel_art'] == 'Schreiber von':
                item['schreiber'] = rel
            elif rel['rel_art'] == 'Empfänger von':
                item['empfänger'] = rel
        return item

    def get_gnd_person_map(self) -> 'dict[str, list[str]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            return {
                    row['gnd']:
                        f"https://schelling.badw.de/person/{row['ID']}"
                    for row in
                        con.geteach("ID, gnd from person where gnd <> ''")
                    }

    def get_index(
            self,
            kind: str,
            roles: 'set[str]',
            ) -> 'ty.Generator[dict[str, int|str|dict[str, str|int]]]':
        '''
        From the database, get all records of :param:`kind` to fill a datatable.

        :param roles: the roles of the user who has triggered the query.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            if kind == 'anderdokument':
                for row in con.geteach(rf"""
                            dokument.*,
                            (select count(1) from person_ort_dokument
                                where dokument.ID = person_ort_dokument.dokument_ID
                                and person_ort_dokument.person_ID is not NULL)
                                    as person_dokument_num,
                            (select count(1) from literatur_dokument
                                where dokument.ID = literatur_dokument.dokument_ID)
                                    as literatur_dokument_num,
                            -- clumsy, but much faster than IN or OR:
                            (select count(1) from dokument_dokument
                                join dokument as dokument2
                                    on dokument_dokument.dokument_ID_2 = dokument2.ID
                                    and dokument2.stand != 'gesondert'
                                where dokument.ID = dokument_dokument.dokument_ID_1)
                            + (select count(1) from dokument_dokument
                                join dokument as dokument3
                                    on dokument_dokument.dokument_ID_1 = dokument3.ID
                                    and dokument3.stand != 'gesondert'
                                where dokument.ID = dokument_dokument.dokument_ID_2)
                                    as dokument_dokument_num
                        from dokument
                        where dokument.art not in ('', {self.letterlikes_str})
                        and dokument.stand != 'gesondert'
                        """):
                    rels = []
                    if row['dokument_dokument_num']:
                        rels.append(f"📃 {row['dokument_dokument_num']} D")
                    if row['literatur_dokument_num']:
                        rels.append(f"📖 {row['literatur_dokument_num']} L")
                    if row['person_dokument_num']:
                        rels.append(f"👤 {row['person_dokument_num']} P")
                    tablerow = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a class="key" href="/doc/{row["ID"]}">{row["ID"]}</a>',
                                's': row["ID"],
                            },
                            row['art'],
                            {
                                '_': row['_verfasser'],
                                's': sortxtext(row['_verfasser']),
                            },
                            {
                                '_': row['_schreiber'],
                                's': sortxtext(row['_schreiber']),
                            },
                            {
                                '_': row['titel'],
                                's': sortxtext(row['titel']),
                            },
                            {
                                '_': row['datum'],
                                's': sortday(row['datum']),
                            },
                            {
                                '_': row['standort'],
                                's': sortxtext(row['standort']),
                            },
                            {
                                '_': row['signatur'],
                                's': sortxtext(row['signatur']),
                            },
                            f'<a class="key" href="/doc/{row["ID"]}">{" ".join(rels)}</a>'
                                if rels else '',
                            f'<a class="key" href="/text/{row["ID"]}/">J</a>'
                                if row['_text'] and (
                                    row['stand'] == 'veröff.'
                                    or 'inspector' in roles
                                    ) else 'N',
                            )) }
                    tablerow['DT_RowId'] = row['ID']
                    yield tablerow
            elif kind == 'brief':
                for row in con.geteach(rf"""
                            dokument.*,
                            IFNULL(person_ort_dokument_her._ort, '') as absendeort,
                            IFNULL(person_ort_dokument_hin._ort, '') as zielort,
                            (select count(1) from person_ort_dokument
                                where dokument.ID = person_ort_dokument.dokument_ID
                                and person_ort_dokument.person_ID is not NULL)
                                    as person_dokument_num,
                            (select count(1) from literatur_dokument
                                where dokument.ID = literatur_dokument.dokument_ID)
                                    as literatur_dokument_num,
                            -- clumsy, but much faster than IN or OR:
                            (select count(1) from dokument_dokument
                                join dokument as dokument2
                                    on dokument_dokument.dokument_ID_2 = dokument2.ID
                                    and dokument2.stand != 'gesondert'
                                where dokument.ID = dokument_dokument.dokument_ID_1)
                            + (select count(1) from dokument_dokument
                                join dokument as dokument3
                                    on dokument_dokument.dokument_ID_1 = dokument3.ID
                                    and dokument3.stand != 'gesondert'
                                where dokument.ID = dokument_dokument.dokument_ID_2)
                                    as dokument_dokument_num
                        from dokument
                        join person_ort_dokument as person_ort_dokument_wer
                            on dokument.ID = person_ort_dokument_wer.dokument_ID
                            and person_ort_dokument_wer.person_ID = 87
                            and person_ort_dokument_wer.art in
                                ('Empfänger von', 'Verfasser von')
                        left join person_ort_dokument as person_ort_dokument_her
                            on dokument.ID = person_ort_dokument_her.dokument_ID
                            and person_ort_dokument_her.art = 'Absendeort von'
                        left join person_ort_dokument as person_ort_dokument_hin
                            on dokument.ID = person_ort_dokument_hin.dokument_ID
                            and person_ort_dokument_hin.art = 'Zielort von'
                        where dokument.art in ({self.letterlikes_str})
                        and dokument.stand != 'gesondert'
                        """):
                    rels = []
                    if row['dokument_dokument_num']:
                        rels.append(f"📃 {row['dokument_dokument_num']}D")
                    if row['literatur_dokument_num']:
                        rels.append(f"📖 {row['literatur_dokument_num']}L")
                    if row['person_dokument_num']:
                        rels.append(f"👤 {row['person_dokument_num']}P")
                    tablerow = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a class="key" href="/doc/{row["ID"]}">{row["ID"]}</a>',
                                's': row["ID"],
                            },
                            row['art'],
                            {
                                '_': row['datum'],
                                's': sortday(row['datum']),
                            },
                            {
                                '_': row['_verfasser'],
                                's': sortxtext(row['_verfasser']),
                            },
                            {
                                '_': row['_empfänger'],
                                's': sortxtext(row['_empfänger']),
                            },
                            {
                                '_': row['_schreiber'],
                                's': sortxtext(row['_schreiber']),
                            },
                            {
                                '_': row['absendeort'],
                                's': sortxtext(row['absendeort']),
                            },
                            {
                                '_': row['zielort'],
                                's': sortxtext(row['zielort']),
                            },
                            {
                                '_': row['titel'],
                                's': sortxtext(row['titel']),
                            },
                            {
                                '_': row['standort'],
                                's': sortxtext(row['standort']),
                            },
                            {
                                '_': row['signatur'],
                                's': sortxtext(row['signatur']),
                            },
                            f'<a class="key" href="/doc/{row["ID"]}">{" ".join(rels)}</a>'
                                if rels else '',
                            f'<a class="key" href="/text/{row["ID"]}/">J</a>'
                                if row['_text'] and (
                                    row['stand'] == 'veröff.'
                                    or 'inspector' in roles
                                    ) else 'N',
                            )) }
                    tablerow['DT_RowId'] = row['ID']
                    yield tablerow
            elif kind == 'ort':
                for row in con.geteach(r"""
                            ort.*,
                            (select count(1) from person_ort_dokument
                                join dokument
                                    on person_ort_dokument.dokument_ID = dokument.ID
                                    and dokument.stand != 'gesondert'
                                where ort.ID = person_ort_dokument.ort_ID
                                and person_ort_dokument.dokument_ID is not NULL)
                                    as ort_dokument_num,
                            -- clumsy, but much faster than IN or OR:
                            (select count(1) from person
                                where ort.ID = person.geburtsort_ID)
                            + (select count(1) from person
                                where ort.ID = person.todesort_ID)
                                    as person_ort_num
                        from ort
                        order by name
                        """):
                    name = row['name']
                    if not name:
                        continue
                    inname = row["_liegt_in__ort"]
                    rels = []
                    if row['ort_dokument_num']:
                        rels.append(f"📃 {row['ort_dokument_num']} D")
                    if row['person_ort_num']:
                        rels.append(f"👤 {row['person_ort_num']} P")
                    tablerow = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'<a href="/ort/{row["ID"]}">{name}</a>',
                                's': sortxtext(name),
                            },
                            {
                                '_': f'{inname} <a class="key" href="/ort#{row["liegt_in__ort_ID"]}" target="_blank">↗</a>'
                                    if inname else '',
                                's': sortxtext(inname) if inname else '',
                            },
                            f'<a class="key" href="/ort/{row["ID"]}">{" ".join(rels)}</a>'
                                if rels else '',
                            )) }
                    tablerow['DT_RowId'] = row['ID']
                    yield tablerow
            elif kind == 'person':
                for row in con.geteach(r"""
                            person.*,
                            (select count(1) from person_ort_dokument
                                join dokument
                                    on person_ort_dokument.dokument_ID = dokument.ID
                                    and dokument.stand != 'gesondert'
                                where person.ID = person_ort_dokument.person_ID
                                and person_ort_dokument.dokument_ID is not NULL)
                                    as person_dokument_num,
                            -- clumsy, but much faster than IN or OR:
                            (select count(1) from person_person
                                where person.ID = person_person.person_ID_1)
                            + (select count(1) from person_person
                                where person.ID = person_person.person_ID_2)
                                    as person_person_num
                        from person
                        order by person.name
                        """):
                    name = row['name']
                    if not name:
                        continue
                    rels = []
                    if row['person_dokument_num']:
                        rels.append(f"📃 {row['person_dokument_num']} D")
                    if row['person_person_num']:
                        rels.append(f"👤 {row['person_person_num']} P")
                    tablerow = { str(rank): item for rank, item in enumerate((
                            row['art'],
                            {
                                '_': f'<a href="/person/{row["ID"]}">{name}</a>',
                                's': sortxtext(row['name']),
                            },
                            {
                                '_': row['geburtsdatum'],
                                's': sortday(row['geburtsdatum']),
                            },
                            {
                                '_': row['todesdatum'],
                                's': sortday(row['todesdatum']),
                            },
                            {
                                '_': row['_geburtsort'],
                                's': sortxtext(row['_geburtsort']),
                            },
                            {
                                '_': row['_todesort'],
                                's': sortxtext(row['_todesort']),
                            },
                            row['leben'],
                            f'<a href="https://d-nb.info/gnd/{row["gnd"]}">{row["gnd"]}</a>'
                                if row['gnd'] else '',
                            row['zitiert_in_AA'],
                            f'<a class="key" href="/person/{row["ID"]}">{" ".join(rels)}</a>'
                                if rels else '',
                            )) }
                    tablerow['DT_RowId'] = row['ID']
                    yield tablerow
            elif kind == 'system':
                for row in con.geteach(r"""
                            system.ID as system_ID,
                            system.name as system_name,
                            system.beschreibung as system_beschreibung,
                            system.gehört_zu__system_ID as obersystem_ID,
                            system._gehört_zu__system as obersystem_name,
                            person.ID as person_ID,
                            person.name as person_name,
                            person.geburtsdatum as geburtsdatum,
                            person.todesdatum as todesdatum
                        from system
                        left join system_person
                            on system.ID = system_person.system_ID
                        left join person
                            on system_person.person_ID = person.ID
                        where system.ID <> 1 -- exclude the non-system.
                        """):
                    person = (
                            f"{row['person_name']} (*{row['geburtsdatum'] or '?'} †{row['todesdatum'] or '?'})"
                            if row['person_ID'] else '')
                    tablerow = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'''<a href="/system/{row["obersystem_ID"]}">{row['obersystem_name']}</a>''',
                                's': sortxtext(row['obersystem_name']),
                            },
                            {
                                '_': f'''<a href="/system/{row["system_ID"]}">{row['system_name']}</a>''',
                                's': sortxtext(row['system_name']),
                            },
                            {
                                '_': f'''<a href="/person/{row["person_ID"]}">{person}</a>''',
                                's': sortxtext(person),
                            } if person else {'_': '', 's': ''},
                            )) }
                    tablerow['DT_RowId'] = row['system_ID']
                    yield tablerow
            elif kind == 'thema':
                for row in con.geteach(r"""
                            thema.ID as thema_ID,
                            thema.name as thema_name,
                            (select count(1) from _thema_dokument
                                where _thema_dokument.thema_ID = thema.ID)
                                    as thema_dokument_num
                        from thema
                        where thema.ID <> 1 -- exclude the non-thema.
                        """):
                    tablerow = { str(rank): item for rank, item in enumerate((
                            {
                                '_': f'''<a href="/thema/{row["thema_ID"]}">{row['thema_name']}</a>''',
                                's': sortxtext(row['thema_name']),
                            },
                            {
                                '_': f"📃 {row['thema_dokument_num']} D",
                                's': row['thema_dokument_num'],
                            },
                            )) }
                    tablerow['DT_RowId'] = row['thema_ID']
                    yield tablerow

    def get_lit(
            self,
            item_id: int,
            form: str,
            ) -> 'dict[str, int|str|None|list[dict[str, int|str|None]]]':
        '''
        From the database, get the information about the record which has the ID
        :param:`item_id` and is in the table “literatur”.

        The :param:`form` decides for what the information will be used:

        - “tip”: It will be used for a tooltip.
        - otherwise: It will be used for a whole webpage.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            item = con.get(r"""
                    * from
                    literatur
                    left join literatur as lit2
                        on literatur.in__literatur_ID = lit2.ID
                    where literatur.ID = %s
                    """, item_id)
        return item

    def get_ort(
            self,
            item_id: int,
            form: str,
            ) -> 'dict[str, int|str|None|list[dict[str, int|str|None]]]':
        '''
        From the database, get the information about the record which has the ID
        :param:`item_id` and is in the table ort.

        The :param:`form` decides for what the information will be used:

        - “tip”: It will be used for a tooltip.
        - otherwise: It will be used for a whole webpage, therefore includes all
          relational information.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            item = con.get(r"* from ort where ID = %s", item_id)
            if form == 'tip' or not item:
                return item
            item['dokument_rels'] = sorted(con.geteach(r"""
                        person_ort_dokument.art as rel_art,
                        dokument.ID as dokument_ID,
                        dokument.art as art,
                        dokument.datum as datum,
                        REPLACE(dokument._verfasser, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as _verfasser,
                        REPLACE(dokument.`_empfänger`, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as `_empfänger`,
                        dokument.titel as titel,
                        dokument.stand as stand,
                        dokument._text as _text
                    from person_ort_dokument
                    join dokument
                        on person_ort_dokument.dokument_ID = dokument.ID
                        and dokument.stand != 'gesondert'
                    where person_ort_dokument.ort_ID = %s
                    """, item_id), key = lambda row: (
                        row['rel_art'],
                        row['art'] not in self.letterlikes_set,
                        row['art'],
                        sortday(row['datum']),
                        sortxtext(row['_verfasser']),
                        sortxtext(row['_empfänger']),
                        sortxtext(row['titel']),
                        ))
            cur.execute(r"""
                    (select
                        'Geburtsort von' as rel_art,
                        person.ID as person_ID,
                        REPLACE(person.name, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as name,
                        person.geburtsdatum as geburtsdatum,
                        person.todesdatum as todesdatum
                    from person where person.geburtsort_ID = %s
                    ) union (select
                        'Todesort von' as rel_art,
                        person.ID as person_ID,
                        REPLACE(person.name, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as name,
                        person.geburtsdatum as geburtsdatum,
                        person.todesdatum as todesdatum
                    from person where person.todesort_ID = %s
                    )
                    """, (item_id, item_id))
            item['person_rels'] = sorted(
                    cur.fetchall(),
                    key = lambda row: (
                        row['rel_art'],
                        sortxtext(row['name']),
                        ))
        return item

    def get_person(
            self,
            item_id: int,
            form: str,
            ) -> 'dict[str, int|str|None|list[dict[str, int|str|None]]]':
        '''
        From the database, get the information about the record which has the ID
        :param:`item_id` and is in the table “person”.

        The :param:`form` decides for what the information will be used:

        - “tip”: It will be used for a tooltip.
        - otherwise: It will be used for a whole webpage, therefore includes all
          relational information.

        Note that the existing person-place relations are the place of birth and
        the place of death; and they are stored in the table “person” itself.
        '''
        with sql_io.DBCon(self.connect()) as (con, cur):
            item = con.get(r"* from person where ID = %s", item_id)
            if form == 'tip' or not item:
                return item
            item['dokument_rels'] = sorted(con.geteach(r"""
                        person_ort_dokument.art as rel_art,
                        dokument.ID as dokument_ID,
                        dokument.art as art,
                        dokument.datum as datum,
                        REPLACE(dokument._verfasser, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as _verfasser,
                        REPLACE(dokument.`_empfänger`, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as `_empfänger`,
                        dokument.titel as titel,
                        dokument.stand as stand,
                        dokument._text as _text
                    from person_ort_dokument
                    join dokument
                        on person_ort_dokument.dokument_ID = dokument.ID
                        and dokument.stand != 'gesondert'
                    where person_ort_dokument.person_ID = %s
                    """, item_id), key = lambda row: (
                        row['art'] not in self.letterlikes_set,
                        row['art'],
                        row['rel_art'],
                        sortday(row['datum']),
                        sortxtext(row['_verfasser']),
                        sortxtext(row['_empfänger']),
                        sortxtext(row['titel']),
                        ))
            item['system_rels'] = sorted(con.geteach(r"""
                        system.ID as system_ID,
                        system.name as system_name
                    from system_person
                    join system
                        on system_person.system_ID = system.ID
                    where system_person.person_ID = %s
                    """, item_id), key = lambda row: (
                        sortxtext(row['system_name']),
                        ))
            for θ, ρ in ((1, 2), (2, 1)):
                item[f'person_rels_{θ}'] = sorted(con.geteach(rf"""
                            person_person.art as rel_art,
                            person.ID as person_ID,
                            REPLACE(person.name, 'Schelling, Friedrich Wilhelm Joseph von', '<small-caps>Schelling</small-caps>') as name,
                            person.geburtsdatum as geburtsdatum,
                            person.todesdatum as todesdatum
                        from person_person
                        join person
                            on person_person.person_ID_{ρ} = person.ID
                        where person_person.person_ID_{θ} = %s
                        """, item_id), key = lambda row: (
                            row['rel_art'],
                            sortxtext(row['name']),
                            ))
        return item

    def get_text(
            self,
            item_id: int,
            filename: str,
            query: 'dict[str, str]',
            text_re: 're.Pattern[str]' =
                re.compile(r'(?s)<article([^>]*>.*</article>)'),
            ) -> str:
        '''
        Get the XML text with the :param:`item_id` and prepare it to be put onto
        a webpage.
        '''
        ref = query.get('ref', '')
        filename = fs.sanitize(filename)
        path = os.path.join(self.repros_path, f'{item_id}/{filename}')
        try:
            text = text_re.search(fs.read(path)).group(1)
        except (FileNotFoundError, AttributeError):
            text = ''
        else:
            prev_next_keys = rf'''
<p class="blink flexrow">
<a aria-label="previous" class="arrowleft key" data-pos="-1" onclick="nav('prev', event, '{ref}')">🔎︎</a>
<a aria-label="next" class="arrowright key" data-pos="-1" onclick="nav('next', event, '{ref}')">🔎︎</a>
</p>''' if ref else ''
            text = rf'''<nav>
<a class="key" href="/doc/{item_id}">Me­ta­da­ten</a>{prev_next_keys}
</nav>
<article class="just sheet wide"{text}'''
        return text

    def list_repros(self, doc_id: int) -> 'tuple[list[str], list[str]]':
        with sql_io.DBCon(self.connect()) as (con, cur):
            doc = con.get(r'ID from dokument where ID = %s', doc_id)
        if doc:
            dirpath = fs.pave_dir(join(self.repros_path, str(doc_id)))
        else:
            return []
        textnames = []
        imagenames = []
        for name in sorted(os.listdir(dirpath), key = sortxtext):
            if name.lower().endswith('.html'):
                textnames.append(name)
            else:
                imagenames.append(name)
        return textnames, imagenames

    @staticmethod
    def normalize(term: str) -> str:
        '''
        Normalize :param:`term` for the database.

        - Prepend and append a blank to the elements br, div, h1–h6, p.
        - Delete the elements head, script, style, xml together with their content.
        - Delete all remaining tags, comments, processing instructions etc., so keep
        only the text, but escaped according to HTML-XML.
        - Delete any leading and trailing whitespace.
        - Turn whitespace characters except non-breaking ones into blanks.
        - Of a sequence of two or more whitespaces, keep only the first.
        '''

        def replace_element(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'None|str':
            if tag in {'head', 'script', 'style', 'xml'}:
                return ''
            return None

        def replace_tag(
                tag: str,
                attrs: 'dict[str, None|str]',
                startend: bool,
                parser: 'xmlhtml.Replacer',
                ) -> 'tuple[str, dict[str, None|str], bool]':
            return '', {}, False

        term = re.sub(r'<(/?)(br|div|h[1-6]|p)\b', r' <\g<1>\g<2>', term)
        term = xmlhtml.replace(
                term,
                replace_element = replace_element,
                replace_tag = replace_tag,
                )
        term = term.strip()
        term = re.sub(r'[^\S\u00a0\u202f]+', ' ', term)
        term = re.sub(r'(\s)\s+', r'\g<1>', term)
        return term
