% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% limit = 16
% if kwargs['form'] == 'tip':
<span>{{!item['art'].title() or '?'}}:
	{{!item['_verfasser'] or '?'}}\\
  % if item['_empfänger']:
	<b>an</b> {{!item['_empfänger']}}\\
  % end
  % if item['titel']:
	<cite>»{{!item['titel']}}«</cite>\\
  % end
	({{!item['datum'] or '?'}})</span>
% else:
<main>
<article class="just sheet wide">
	<h1 id="name">{{!item['art']}}</h1>
	<table class="card mapping">
	  % if item['_text'] and (request.roles or item['stand'] == 'veröff.'):
		<tr><th>Zugriff</th><td><strong><a class="key" href="/text/{{!item['ID']}}/">Text</a></strong></td></tr>
	  % end
	  % if item['datum']:
		<tr><th>Datum</th><td>{{!item['datum']}}</td></tr>
	  % end
	  % if item['absendeort']:
		<tr><th>Absendeort</th><td><a href="/person/{{!item['absendeort']['ort_ID']}}">{{!item['absendeort']['name']}}</a>
		% if item['absendeort']['uberort_ID']:
			(in <a href="/ort/{{!item['absendeort']['uberort_ID']}}"></a>)
		% end
		</td></tr>
	  % end
	  % if item['zielort']:
		<tr><th>Zielort</th><td><a href="/person/{{!item['zielort']['ort_ID']}}">{{!item['zielort']['name']}}</a>
		% if item['zielort']['uberort_ID']:
			(in <a href="/ort/{{!item['zielort']['uberort_ID']}}"></a>)
		% end
		</td></tr>
	  % end
	  % if item['verfasser']:
		<tr><th>Verfasser</th><td><a href="/person/{{!item['verfasser']['person_ID']}}">{{!item['verfasser']['name']}} (*{{!item['verfasser']['geburtsdatum'] or '?'}} †{{!item['verfasser']['todesdatum'] or '?'}})</a></td></tr>
	  % end
	  % if item['schreiber']:
		<tr><th>Schreiber</th><td><a href="/person/{{!item['schreiber']['person_ID']}}">{{!item['schreiber']['name']}} (*{{!item['schreiber']['geburtsdatum'] or '?'}} †{{!item['schreiber']['todesdatum'] or '?'}})</a></td></tr>
	  % end
	  % if item['empfänger']:
		<tr><th>Empfänger</th><td><a href="/person/{{!item['empfänger']['person_ID']}}">{{!item['empfänger']['name']}} (*{{!item['empfänger']['geburtsdatum'] or '?'}} †{{!item['empfänger']['todesdatum'] or '?'}})</a></td></tr>
	  % end
	  % if item['titel']:
		<tr><th>Titel</th><td>{{!item['titel']}}</td></tr>
	  % end
	  % if item['standort']:
		<tr><th>Standort</th><td>{{!item['standort']}}</td></tr>
	  % end
	  % if item['signatur']:
		<tr><th>Signatur</th><td>{{!item['signatur']}}</td></tr>
	  % end
	  % if item['bemerkung']:
		<tr><th>Bemerkung</th><td>{{!item['bemerkung']}}</td></tr>
	  % end
	</table>
	<section>
		<h2>Beziehungen zu anderen Dokumenten</h2>
	  % length = len(item['dokument_rels_1']) + len(item['dokument_rels_2'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['dokument_rels_1']:
			<li>Dieses Dokument ist <b>{{!rel['rel_art'] or '? zu'}}</b>:
				{{!rel['art'].title() or '?'}}
				<a href="/doc/{{!rel['dokument_ID']}}">{{!rel['_verfasser'] or '?'}}\\
				  % if rel['_empfänger']:
					<b>an</b> {{!rel['_empfänger']}}\\
				  % end
				  % if rel['titel']:
					<cite>»{{!rel['titel']}}«</cite>\\
				  % end
					({{!rel['datum'] or '?'}})</a>.
				  % if rel['_text'] and (request.roles or rel['stand'] == 'veröff.'):
					% ref = f"?ref=doc/{item['ID']}" if rel['rel_art'] == 'erwähnt in' else ''
					<strong><a class="key" href="/text/{{!rel['dokument_ID']}}/{{!ref}}">Text</a></strong>
				  % end
			</li>
		  % end
		  % for rel in item['dokument_rels_2']:
			<li>{{!rel['art'].title() or '?'}}
				<a href="/doc/{{!rel['dokument_ID']}}">{{!rel['_verfasser'] or '?'}}\\
				  % if rel['_empfänger']:
					<b>an</b> {{!rel['_empfänger']}}\\
				  % end
				  % if rel['titel']:
					<cite>»{{!rel['titel']}}«</cite>\\
				  % end
					({{!rel['datum'] or '?'}})</a>
				ist <b>{{!rel['rel_art'] or '? zu'}} {{!'dieses' if rel['rel_art'] == 'Antwort auf' else 'diesem'}} Dokument.</b>
			  % if item['_text'] and (request.roles or item['stand'] == 'veröff.') and rel['rel_art'] == 'erwähnt in':
				<strong><a class="key" href="/text/{{!item['ID']}}/?ref=doc/{{!rel['dokument_ID']}}">Text</a></strong>
			  % end
			</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
	<section>
		<h2>Beziehungen zu Literatur</h2>
	  % length = len(item['literatur_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['literatur_rels']:
			<li>
			  % if rel['_verfasser']:
				{{!rel['_verfasser']}}:
			  % end
				»{{!rel['titel'] or '[?]'}}«
			  % if rel['erscheinungsjahr']:
				({{!rel['erscheinungsjahr']}})
			  % end
				ist <b>{{!rel['rel_art'] or '? zu'}}</b> diesem Dokument.
			  % if item['_text'] and (request.roles or item['stand'] == 'veröff.') and rel['rel_art'] == 'erwähnt in':
				<strong><a class="key" href="/text/{{!item['ID']}}/?ref=lit/{{!rel['literatur_ID']}}">Text</a></strong>
			  % end
			</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
	<section>
		<h2>Beziehungen zu Orten</h2>
	  % length = len(item['ort_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['ort_rels']:
			<li>
				<a href="/ort/{{!rel['ort_ID']}}">{{!rel['name']}}</a>
			  % if rel['uberort_ID']:
				(in <a href="/ort/{{!rel['uberort_ID']}}"></a>)
			  % end
				ist <b>{{!rel['rel_art'] or '? zu'}}</b> diesem Dokument.
			  % if item['_text'] and (request.roles or item['stand'] == 'veröff.') and rel['rel_art'] == 'erwähnt in':
				<strong><a class="key" href="/text/{{!item['ID']}}/?ref=ort/{{!rel['ort_ID']}}">Text</a></strong>
			  % end
			</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
	<section>
		<h2>Beziehungen zu Personen</h2>
	  % length = len(item['person_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['person_rels']:
			<li>
				<a href="/person/{{!rel['person_ID']}}">{{!rel['name']}} (*{{!rel['geburtsdatum'] or '?'}} †{{!rel['todesdatum'] or '?'}})</a> ist <b>{{!rel['rel_art'] or '? zu'}}</b> diesem Dokument.
			  % if item['_text'] and (request.roles or item['stand'] == 'veröff.') and rel['rel_art'] == 'erwähnt in':
				<strong><a class="key" href="/text/{{!item['ID']}}/?ref=person/{{!rel['person_ID']}}">Text</a></strong>
			  % end
			</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
</article>
</main>
% end