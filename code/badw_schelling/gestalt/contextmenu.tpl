% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2023 ff. (© http://badw.de)
<dialog class="contextmenu" onclick="if (event.target.className === 'contextmenu') {event.target.close()}">
	<form method="dialog">
		<button>✕ Menü schließen</button>
		<a class="key ref" href="" rel="noopener noreferrer" target="_blank">↗ Verweis-ID folgen</a>
		<button class="delref warn">⤯ Verweis-ID löschen</button>
	  % if 'editor' in request.roles:
		<button class="del warn">🗑 Zeile löschen</button>
	  % end
	</form>
</dialog>
