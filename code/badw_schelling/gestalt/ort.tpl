% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% limit = 16
% if kwargs['form'] == 'tip':
  % region = f" (in {item['_liegt_in__ort']})" if item['_liegt_in__ort'] else ''
<span>{{!item['name']}}{{!region}}</span>
% else:
<main>
<article class="just sheet wide">
	<h1 id="name">{{!item['name']}}
	  % if item['liegt_in__ort_ID']:
		<small>(in <a href="/ort/{{!item['liegt_in__ort_ID']}}">{{!item['_liegt_in__ort']}}</a>)</small>
	  % end
	</h1>
	<section>
		<h2>Beziehungen zu Dokumenten</h2>
	  % length = len(item['dokument_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['dokument_rels']:
			<li>Dieser Ort ist <b>{{!rel['rel_art'] or '? zu'}}</b>:
				{{!rel['art'].title() or '?'}}
				<a href="/doc/{{!rel['dokument_ID']}}">{{!rel['_verfasser'] or '?'}}\\
				  % if rel['_empfänger']:
					<b>an</b> {{!rel['_empfänger']}}\\
				  % end
				  % if rel['titel']:
					<cite>»{{!rel['titel']}}«</cite>\\
				  % end
					({{!rel['datum'] or '?'}})</a>.
				  % if rel['_text'] and (request.roles or rel['stand'] == 'veröff.'):
					% ref = f"?ref=ort/{item['ID']}" if rel['rel_art'] == 'erwähnt in' else ''
					<strong><a class="key" href="/text/{{!rel['dokument_ID']}}/{{!ref}}">Text</a></strong>
				  % end
			</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
	<section>
		<h2>Beziehungen zu Personen</h2>
	  % length = len(item['person_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['person_rels']:
			<li>Dieser Ort ist {{rel['rel_art']}}: <a href="/person/{{!rel['person_ID']}}">{{!rel['name']}} (*{{!rel['geburtsdatum'] or '?'}} †{{!rel['todesdatum'] or '?'}})</a>.</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
</article>
</main>
% end