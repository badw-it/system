% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% doc_id = kwargs['doc_id']
<main>
<article class="just sheet wide">
	<h1>Dateien für das <a href="/doc/{{doc_id}}">Dokument {{doc_id}}</a></h1>
	<h2>Datei hochladen</h2>
	<form enctype="multipart/form-data" method="post">
		<ol class="list">
			<li>Schritt: <input class="key" multiple="" type="file" name="file"/></li>
			<li>Schritt: <button type="submit" formaction="/ablage/{{doc_id}}">Auswahl hochladen.</button></li>
		</ol>
	</form>
	<h2>Derzeit hochgeladene Textdateien</h2>
  % if kwargs['textnames']:
	<ol class="list">
	% for name in kwargs['textnames']:
		<li>
			<div class="flexrow">
				<span>:  {{name}}</span>
			  % if name == 'text.html':
				<a class="key" href="/text/{{doc_id}}/" target="_blank"><strong>Text</strong></a>
			  % else:
				<a class="key" href="/text/{{doc_id}}/{{db.q(name)}}" target="_blank">👁</a>
			  % end
				<a class="key" download="{{name}}" href="/ablage/{{doc_id}}/{{db.q(name)}}?export=1" style="padding: 0"><span onclick="location.reload()" style="display: inline-block; height: 100%; width: 100%"> ⭳ </span></a>
				<form action="/ablage/{{doc_id}}" method="post">
					<input type="hidden" name="filename" value="{{name}}"/>
					<button class="lid" type="button">✕</button><button name="redaction" value="delete">✕</button>
				</form>
			</div>
		</li>
	% end
	</ol>
  % else:
	<p>(keine)</p>
  % end
	<h2>Derzeit hochgeladene Bilddateien</h2>
  % if kwargs['imagenames']:
	<ol class="list">
	% for name in kwargs['imagenames']:
		<li>
			<div class="flexrow">
				<span>:  {{name}}</span>
				<button onclick="window.open('/ablage/{{doc_id}}/{{db.q(name)}}', '_blank', 'popup')" type="button">👁</button>
				<a class="key" download="{{name}}" href="/ablage/{{doc_id}}/{{db.q(name)}}?export=1" style="padding: 0"><span onclick="location.reload()" style="display: inline-block; height: 100%; width: 100%"> ⭳ </span></a>
				<form action="/ablage/{{doc_id}}" method="post">
					<input type="hidden" name="filename" value="{{name}}"/>
					<button class="lid" type="button">✕</button><button name="redaction" value="delete">✕</button>
				</form>
			</div>
		</li>
	% end
	</ol>
  % else:
	<p>(keine)</p>
  % end
</article>
</main>
