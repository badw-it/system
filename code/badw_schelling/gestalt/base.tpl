% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% lang_id = kwargs.setdefault('lang_id', db.lang_id)
<!DOCTYPE html>
<html id="top" class="{{kwargs.get('root_class', 'desk')}}" lang="{{lang_id}}">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="{{request.path}}" rel="canonical"/>
	<link href="/icons/favicon.ico" rel="icon"/>
	<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/cssjs/badw_schelling.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/cssjs/jquery/jquery.min.js"></script>
	<script src="/cssjs/all.js?v={{db.startsecond}}"></script>
	<script src="/cssjs/badw_schelling.js?v={{db.startsecond}}"></script>
	<title>Schelling: {{request.path.lstrip('/').title()}}</title>
</head>
<body>
% if kwargs.get('note', ''):
<input class="flip" id="flip1" type="checkbox"/><label class="key notice top" for="flip1"></label><article class="card">{{kwargs['note']}}</article>
% end
<header>
	<img class="shadow" src="/icons/main.jpg" alt="Schelling"/>
	<h1>Schelling
	<small>Nachlass-Edition</small></h1>
	<input class="flip" id="flip2" type="checkbox"/><label class="key menuswitch" data-checked="Menü einklappen" data-unchecked="Menü ausklappen" for="flip2"></label>
	<div>
		<nav>
			<ul>
				<li>\\
				% if '/start' == request.path:
					<b>Start</b>\\
				% else:
					<a href="{{!'/start'}}">Start</a>\\
				% end
				</li>
				<li>
					<a href="https://schelling-projekt.badw.de">Projekt <i>Schelling in München</i></a>
				</li>
				<li>
					<a href="https://etf.univie.ac.at/forschung/forschungsprojekte/christian-danz/hybridedition-schellings-berliner-philosophie-der-offenbarung/">Projekt <i>Schelling in Berlin</i></a>
				</li>
			</ul>
			<hr/>
			<ul>
				<li>\\
				% if '/brief' == request.path:
					<b>Briefe</b>\\
				% else:
					<a href="{{!'/brief'}}">Briefe</a>\\
				% end
				</li>
				<li>\\
				% if '/anderdokument' == request.path:
					<b>Andere Dokumente</b>\\
				% else:
					<a href="{{!'/anderdokument'}}">Andere Dokumente</a>\\
				% end
				</li>
				<li>\\
				% if '/person' == request.path:
					<b>Personen</b>\\
				% else:
					<a href="{{!'/person'}}">Personen</a>\\
				% end
				</li>
				<li>\\
				% if '/ort' == request.path:
					<b>Orte</b>\\
				% else:
					<a href="{{!'/ort'}}">Orte</a>\\
				% end
				</li>
				<!-- <li>\\
				% if '/system' == request.path:
					<b>Systeme</b>\\
				% else:
					<a href="{{!'/system'}}">Systeme</a>\\
				% end
				</li> -->
				<!-- <li>\\
				% if '/thema' == request.path:
					<b>Themen</b>\\
				% else:
					<a href="{{!'/thema'}}">Themen</a>\\
				% end
				</li> -->
			</ul>
		% if 'editor' in request.roles:
			<hr/>
			<ul>
				<li>\\
				% if '/db' == request.path:
					<b>Datenbank</b>\\
				% else:
					<a href="{{!'/db'}}">Datenbank</a>\\
				% end
				</li>
				<li>\\
				% if '/datenbankhinweise' == request.path:
					<b>Datenbankhinweise</b>\\
				% else:
					<a href="{{!'/datenbankhinweise'}}">Datenbankhinweise</a>\\
				% end
				</li>
			</ul>
		% end
		</nav>
		<hr/>
		<nav class="extra">
			<ul>
				<li><a href="http://badw.de/data/footer-navigation/datenschutz.html">{{!db.glosses['datenschutz'][lang_id]}}</a></li>
				<li><a href="http://badw.de/data/footer-navigation/impressum.html">Impressum</a></li>
			</ul>
		% if request.roles:
			<p><a href="/reproreport">Bericht</a></p>
			<p><strong><a href="/auth_end">{{request.user_name}}: {{db.glosses['logout'][lang_id]}}</a></strong> · <a href="/auth_admin">{{db.glosses['administer'][lang_id]}}</a></p>
		% end
		</nav>
	</div>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
<footer>
	<p>
		<a class="img" href="https://badw.de"><img src="/icons/badw.svg" height="64" alt="BAdW"/></a><a
		   class="img" href="https://uni-freiburg.de"><img src="/icons/freiburg.svg" height="64" alt="Universität Freiburg"/></a><a
		   class="img" href="https://www.univie.ac.at/"><img src="/icons/wien.png" height="64" alt="Universität Wien"/></a> <br/>
		<a class="img" href="https://www.dfg.de"><img src="/icons/dfg.jpg" alt="DFG" height="53"/></a><a
		   class="img" href="https://www.fwf.ac.at/"><img src="/icons/fwf.png" alt="DFG" height="53"/></a>
	</p>
</footer>
% if request.roles:
% include('contextmenu.tpl', db = db, request = request, kwargs = kwargs)
% end
% ####include('matomo.tpl', db = db, request = request, kwargs = kwargs)
</body>
</html>
