% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% if kwargs['form'] == 'tip':
<span>
  % vf = item['_verfasser'] or item['externverfasser'] or '?'
  % hg = item['_herausgeber'] or item['externherausgeber'] or ''
  % übersetzer = item['_übersetzer'] or item['externübersetzer'] or ''
  % stelle = f", {kwargs['stelle']}" if kwargs['stelle'] else ''
  % if item['art'] == 'Teil':
	{{!vf}}:
	<cite>»{{!item['titel']}}«</cite>
  % if item['lit2.titel']:
	In <cite>»{{!item['lit2.titel']}}«</cite> {{!item['bandnr.']}}
  % end
{{stelle}}
  % elif item['art'] == 'Reihe':
	<cite>»{{!item['titel']}}«</cite>{{stelle}}
  % else:
	{{!vf}}:
	<cite>»{{!item['titel']}}«</cite>
	% if übersetzer:
	Übersetzt von {{!übersetzer}}.
	% end
	% if hg:
	Hg. von {{!hg}}.
	% end
	{{!item['verlagsort'] or 'o. O.'}}
	({{!item['erscheinungsjahr'] or '?'}})</span>
  % end
% else:
<main>
<article class="just sheet wide">
	<table class="card mapping">
	  % for key, value in item.items():
		% if key.startswith('lit2.'):
		  % if value and key == 'lit2.ID':
		<tr><th>In der Reihe</th><td><a href="{{value}}">{{value}}</a></td></tr>
		  % end
		% elif value and key not in {'in__literatur_ID', 'anm', 'art', 'ID', 'zitiert_in_AA'}:
		<tr><th>{{key.replace('lit2.', '').replace('_', ' ').strip().title()}}</th><td>{{value}}</td></tr>
		% end
	  % end
	  % if kwargs['stelle']:
		<tr><th>Stelle</th><td>{{kwargs['stelle']}}</td></tr>
	  % end
	</table>
</article>
</main>
% end