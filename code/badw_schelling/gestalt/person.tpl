% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% limit = 16
% if kwargs['form'] == 'tip':
  % datum = f"{item['geburtsdatum']}–{item['todesdatum']}"
<span>{{item['name']}}{{f' ({datum})' if datum else ''}}</span>
% else:
<main>
<article class="just sheet wide">
	<h1 id="name">{{!item['name']}}
	  % if item['art'] != 'real':
		<small>({{!item['art']}})</small>
	  % end
	</h1>
	<table class="card mapping">
	  % if item['geburtsdatum']:
		<tr><th>Geburtsdatum</th><td>{{!item['geburtsdatum']}}</td></tr>
	  % end
	  % if item['todesdatum']:
		<tr><th>Todesdatum</th><td>{{!item['todesdatum']}}</td></tr>
	  % end
	  % if item['_geburtsort']:
		<tr><th>Geburtsort</th><td><a href="/ort/{{!item['geburtsort_ID']}}">{{!item['_geburtsort']}}</a></td></tr>
	  % end
	  % if item['_todesort']:
		<tr><th>Todesort</th><td><a href="/ort/{{!item['todesort_ID']}}">{{!item['_todesort']}}</a></td></tr>
	  % end
	  % if item['leben']:
		<tr><th>Leben</th><td>{{!item['leben'].replace('|', '<br/>')}}</td></tr>
	  % end
	  % if item['gnd']:
		<tr><th>GND und Link zu mehr Angaben</th><td><a href="http://d-nb.info/gnd/{{!item['gnd']}}">{{!item['gnd']}}</a></td></tr>
	  % end
	  % if item['zitiert_in_AA']:
		<tr><th>in Akademieausgabe</th><td>{{!item['zitiert_in_AA'].replace('|', '<br/>')}}</td></tr>
	  % end
	  % if item['system_rels']:
		<tr><th>System</th><td><ul>
		% for row in item['system_rels']:
			<li><a href="/system/{{!row['system_ID']}}">{{!row['system_name']}}</a>
			</li>
		% end
		</ul></td></tr>
	  % end
	</table>
	<section>
		<h2>Beziehungen zu Dokumenten</h2>
	  % length = len(item['dokument_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['dokument_rels']:
			<li>Diese Person ist <b>{{!rel['rel_art'] or '? zu'}}</b>:
				{{!rel['art'].title() or '?'}}
				<a href="/doc/{{!rel['dokument_ID']}}">{{!rel['_verfasser'] or '?'}}\\
				  % if rel['_empfänger']:
					<b>an</b> {{!rel['_empfänger']}}\\
				  % end
				  % if rel['titel']:
					<cite>»{{!rel['titel']}}«</cite>\\
				  % end
					({{!rel['datum'] or '?'}})</a>.
				  % if rel['_text'] and (request.roles or rel['stand'] == 'veröff.'):
					% ref = f"?ref=person/{item['ID']}" if rel['rel_art'] == 'erwähnt in' else ''
					<strong><a class="key" href="/text/{{!rel['dokument_ID']}}/{{!ref}}">Text</a></strong>
				  % end
			</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
	<section>
		<h2>Beziehungen zu anderen Personen</h2>
	  % length = len(item['person_rels_1']) + len(item['person_rels_2'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['person_rels_1']:
			<li>Diese Person ist <b>{{!rel['rel_art'] or '? zu'}}</b>: <a href="/person/{{!rel['person_ID']}}">{{!rel['name']}} (*{{!rel['geburtsdatum'] or '?'}} †{{!rel['todesdatum'] or '?'}})</a>.</li>
		  % end
		  % for rel in item['person_rels_2']:
			<li><a href="/person/{{!rel['person_ID']}}">{{!rel['name']}} (*{{!rel['geburtsdatum'] or '?'}} †{{!rel['todesdatum'] or '?'}})</a> ist <b>{{!rel['rel_art'] or '? zu'}}</b> dieser Person.</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
</article>
</main>
% end