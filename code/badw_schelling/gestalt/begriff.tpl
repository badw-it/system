% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
% item = kwargs['item']
% if kwargs['form'] == 'tip':
<span>{{kwargs['kind'].title()}}:
% if item.get('_gehört_zu__system'):
{{!item['_gehört_zu__system']}} ·
% end
{{!item['name']}}
% if kwargs['stelle']:
· {{!kwargs['stelle']}}\\
% end
</span>
% else:
<main>
<article class="just sheet wide">
	<h1 id="name">
		<small>{{kwargs['kind'].title()}}</small>
		<i>\\
	  % if item.get('_gehört_zu__system'):
		<small>(<a href="/system/{{item['gehört_zu__system_ID']}}">{{!item['_gehört_zu__system']</a>) ·</small>
	  % end
		{{!item['name']}}</i>
	</h1>
  % if item['beschreibung']:
	<section class="card">{{!item['beschreibung']}}</section>
  % end
	<section>
		<h2>Erwähnungen in Dokumenten</h2>
	  % length = len(item['dokument_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		<ul class="list">
		  % for rel in item['dokument_rels']:
			% text_available = rel['_text'] and (request.roles or rel['stand'] == 'veröff.')
			<li>
				<p>
				in: {{!rel['dokument_art'].title() or '?'}}
				<a href="/doc/{{!rel['dokument_ID']}}">{{!rel['_verfasser'] or '?'}}\\
				  % if rel['_empfänger']:
					<b>an</b> {{!rel['_empfänger']}}\\
				  % end
				  % if rel['titel']:
					<cite>»{{!rel['titel']}}«</cite>\\
				  % end
					({{!rel['datum'] or '?'}})</a>.
				  % if text_available:
					% key = 'bibel' if (kwargs['kind'] == 'system' and item['ID'] == 2) else f"kapitel/{item['ID']}" if rel.get('art', '') == 'kapitel' else f"{kwargs['kind']}/{item['ID']}"
					<strong><a class="key" href="/text/{{!rel['dokument_ID']}}/?ref={{!key}}">Text</a></strong>
				  % end
				</p>
			  % if kwargs['kind'] == 'thema' and text_available:
				<div class="card edition petit sheet wide" tabindex="-1"><article>{{!rel['text']}}</article></div>
			  % end
			</li>
		  % end
		</ul>
	  % end
	</section>
  % if kwargs['kind'] == 'system':
	<section>
		<h2>Zugeordnete Personen</h2>
	  % length = len(item['person_rels'])
	  % if length == 0:
		<p>–</p>
	  % else:
		% if length > limit:
		<details class="petit"><summary>(Aus- und Einklappen)</summary>
		% end
		<ul class="list">
		  % for rel in item['person_rels']:
			<li><a href="/person/{{!rel['person_ID']}}">{{!rel['name']}} (*{{!rel['geburtsdatum'] or '?'}} †{{!rel['todesdatum'] or '?'}})</a>.</li>
		  % end
		</ul>
		% if length > limit:
		</details>
		% end
	  % end
	</section>
  % end
</article>
</main>
% end