% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for the startpage with login.
% lang_id = kwargs.get('lang_id', '')
<main>
<article class="cover" style="background: url(/-/icons/cover.jpg); background-size: cover">
	<section class="covertitle just">
		<h1>Kleiner Dienst</h1>
	  % if not request.roles:
		<form action="{{request.path}}" method="post">
			<input type="hidden" name="query" value="{{request.query_string}}"/>
		% url = request.query.url
		% if url:
			<input type="hidden" name="url" value="{{url}}"/>
		% end
			<div class="flextable">
				<label class="card">
					<div style="width: 8rem">{{db.glosses['username'][lang_id]}}:</div>
					<input type="text" name="user_name" aria-label="{{db.glosses['username'][lang_id]}}" autofocus=""/>
				</label>
				<label class="card">
					<div style="width: 8rem">{{db.glosses['password'][lang_id]}}:</div>
					<input type="password" name="password" aria-label="{{db.glosses['password'][lang_id]}}"/>
				</label>
			</div>
			<p><button type="submit">{{db.glosses['login'][lang_id]}}</button></p>
		</form>
		<p>{{db.glosses['data_protection_session_cookie'][lang_id]}}</p>
	  % else:
		<p>Sie sind nun angemeldet, können nach Belieben und Bedarf aus dem im Menü links gelisteten Angebot wählen und sich am Ende wieder abmelden.</p>
	  % end
	</section>
</article>
</main>
