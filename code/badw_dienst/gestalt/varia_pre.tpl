% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2020 ff. (© http://badw.de)
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="/-/icons/favicon.ico" rel="icon"/>
	<title>{{kwargs['url']}}</title>
</head>
<body>
<pre>
{{kwargs['data']}}
</pre>
</body>
</html>