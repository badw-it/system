% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a file-upload form.
% url = kwargs.get('url', '')
<main>
<article class="just sheet">
	<h1>Zwischenablage</h1>
	<p>Hier kann man eine Datei hochladen und einen Link zurückbekommen, mit dem andere die Datei abrufen können – auch ohne Anmeldung. Unbefugte ohne diesen Link können nicht an die Datei gelangen, denn der Link enthält eine Zufallszahl (die das Kennwort ersetzt). Dieser Dienst ist gedacht für die Weitergabe von Dateien, die zu groß sind, um als Anhang versendet zu werden. Dieser Dienst ist aber nicht für Dateien größer als <strong>3 GB</strong> (1024³ Bytes) und nicht als Dauerspeicher gedacht: <strong>Dateien werden nach sieben Tagen gelöscht</strong> (damit unser Rechner nicht überläuft).</p>
	<p class="petit">Möchte man einen ganzen Ordner auf einmal hochladen, ist das mittelbar möglich: Man muss diesen Ordner in eine zip-Datei verwandeln, indem man ihn mit der rechten Maustaste anklickt, “Senden an” und “ZIP-komprimierter Ordner” auswählt. Es entsteht dann neben dem zu sendenden Ordner eine zip-Datei, die eine Kopie des Ordners enthält und wie folgt hochgeladen werden kann.</p>
	<form enctype="multipart/form-data" method="post">
		<ol class="list">
			<li><label>Über den folgenden Knopf eine Datei ansteuern, doppelklicken und warten, bis ihr Name unten mit angezeigt wird:<br/>
				<input class="key" type="file" name="file"/></label></li>
			<li><button type="submit" formaction="/zwischenablage#2">Ausgewählte Datei hochladen.</button> Und zumal bei größeren Dateien und von zuhause aus länger warten, bis die Übertragung geschehen ist – der nächste Schritt hier angezeigt wird und erfolgen kann: …</li>
		  % if url:
			<li id="2">Jetzt mit der <strong>rechten</strong> Maustaste <strong><a href="{{url}}" rel="noopener noreferrer" target="_blank">diesen Link hier</a></strong> anklicken; im sich öffnenden Auswahlfenster “Link-Adresse kopieren” anklicken, dann den Link zum Beispiel in einen Brief einfügen (mit Strg + V) und den Brief an jene senden, denen die Datei übermittelt werden sollte. Die Empfänger müssten dann nur auf den Link klicken und bekommen im Browser das Herunterladen angeboten mit dem dafür üblichen Kästchen – doch nur binnen sieben Tagen, danach ist die hier hochgeladene Datei gelöscht!</li>
			<li>Wenn Sie eine weitere Datei hochladen möchten, <a href="zwischenablage">diesen Link hier</a> klicken und wieder mit dem ersten Schritt beginnen.</li>
		  % end
		</ol>
	</form>
</article>
</main>
