% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a file-upload form.
<main>
<article class="just sheet">
	<h1>Ton-Text</h1>
	<p>Eine Audiodatei hochladen und eine automatische Rohtranskription zurückbekommen. Zur Zeit geht es mit Dateien kleiner als 3 GB (1024³ Bytes) und den Formaten “.m4a”, “mp3”, “wav”.</p>
	<form enctype="multipart/form-data" method="post">
		<ol class="list">
			<li><label>Über den folgenden Knopf eine Datei ansteuern, doppelklicken und warten, bis ihr Name unten mit angezeigt wird:<br/>
				<input class="key" type="file" name="file"/></label></li>
			<li><button type="submit" formaction="/ton-text">Ausgewählte Datei senden.</button> Und ziemlich lange warten – ungefähr halb so lange, wie die Spielzeit der Audiodatei beträgt – bis …</li>
			<li>… eine Textdatei mit der Rohtranskription zurückgesandt und in einem aufspringenden Fensterl zum Herunterladen angeboten wird.</li>
			<li>Für eine weitere Datei jetzt wieder bei Schritt 1 beginnen und dort auf den Knopf drücken.</li>
		</ol>
	</form>
</article>
</main>
