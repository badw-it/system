% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a file-upload form.
<main>
<article class="just sheet">
	<h1>{{kwargs['page_title']}}</h1>
	<p>Die URL einer gewissen Seite angeben und
  % if kwargs['page_title'] == 'Bedienbarkeitsbericht':
	einen Bericht zur Bedienfreundlichkeit für all jene Seiten bekommen,
  % elif kwargs['page_title'] == 'Linkrissfinder':
	eine Auflistung gerissener Links bekommen, gefunden auf all jenen Seiten,
  % end
	die man von der gewissen Seite aus über Links (und ohne Javascript) erreichen kann und die zur selben Oberseite gehören – also zwischen dem “//” und “/” der URL dasselbe stehen haben (“dasselbe” abgesehen von einem etwaigen “www.”, das voranstehen oder fehlen mag).</p>
  % if kwargs['page_title'] == 'Bedienbarkeitsbericht':
	<p>Zur Zeit wird nur Folgendes vermerkt:</p>
	<ul class="list">
		<li>Jedes “img”-Element, das weder “alt”- noch “title”-Attribut hat.</li>
		<li>Jedes “a”-Element, das weder ein “title”-Attribut noch Text, der aus mehr als bloß Leerzeichen besteht, noch ein “img”-Element mit “alt”- oder “title”-Attribut hat.</li>
	</ul>
  % elif kwargs['page_title'] == 'Linkrissfinder':
	<p>Das Ausprobieren von Links kann lange dauern; bei einer großen Menge von Seiten mag es am besten sein, die Prüfung im Browserfenster nebenher laufen zu lassen, vielleicht auch über Nacht. Ungeprüft bleiben – vor allem geschwindigkeitshalber – solche Links, deren Domäne “www.badw.de” oder “badw.de” oder dieselbe ist wie die der Seite, auf der sie gefunden wurden. Die Links selber Domäne werden allerdings später insofern geprüft, als der Linkrissfinder sie aufzurufen versucht, um auf ihnen weitere zu prüfende Links einzusammeln; gelingt der Aufruf nicht, wird das gemeldet, allerdings ohne die Seite oder Seiten noch nennen zu können, darauf der Link gefunden ward.</p>
  % end
	<p>Das Vorgehen:</p>
	<form enctype="application/x-www-form-urlencoded" method="post">
		<ol class="list">
			<li>
				<p>
					<label>Eine URL eintragen: <br/>
					<input type="text" name="url" placeholder="https://example.org/start" value="" autofocus=""/></label>
				</p>
			</li>
			<li>
				<p><button type="submit" formaction="{{request.path}}">Hier klicken und so die eingetragene URL mitteilen.</button></p>
				<p>Sogleich öffnet sich eine Ergebnisseite, auf der die Fälle nach und nach erscheinen, sowie sie beim Gang über die Seiten gefunden werden. Wenn auf einer geprüften Seite keine zu finden sind, wird nur eine laufende Nummer angezeigt. Ist der Gang beendet, erscheint in der letzten Zeile “<strong>Ende</strong>”. – <strong>Der Bericht kann, solange man auf der Ergebnisseite ist, mit Strg-s heruntergeladen werden.</strong> – Man kann vorzeitig oder nach der Ausgabe der Endemeldung auf die Eingabeseite zurückgehen, und zwar mit dem Zurück-Knopf des Browsers, dann mit der Nutzung fortfahren oder sich mit dem Menüpunkt “Abmelden” links oben <strong>abmelden</strong>.</p>
			</li>
		</ol>
	</form>
</article>
</main>
