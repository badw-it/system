% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff.
% # Template for a file-upload form.
<main>
<article class="just sheet">
	<h1>Datentafeln</h1>
	<ul class="list">
	  % for datatablelink, datatablename in kwargs['datatablenames']:
		<li><a href="datatables/{{datatablelink}}">{{datatablename}}</a></li>
	  % end
	</ul>
</article>
</main>
