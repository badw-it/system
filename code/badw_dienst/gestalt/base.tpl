% # Licensed under http://www.apache.org/licenses/LICENSE-2.0
% # Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
% lang_id = kwargs.setdefault('lang_id', db.lang_id)
% note = kwargs.get('note', '')
<!DOCTYPE html>
<html id="top" class="desk txt" lang="{{lang_id}}">
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
  % if db.debug:
	<meta name="robots" content="noindex, nofollow"/>
  % end
	<!-- Licensed under http://www.apache.org/licenses/LICENSE-2.0 - Attribution notice: Development and design by Stefan Müller in 2013 ff. (© http://badw.de) -->
	<link href="{{request.path}}" rel="canonical"/>
	<link href="/-/icons/favicon.ico" rel="icon"/>
	<link href="/cssjs/all.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<link href="/cssjs/badw_dienst.css?v={{db.startsecond}}" media="all" rel="stylesheet"/>
	<script src="/cssjs/jquery/jquery.min.js"></script>
	<title>Dienst: {{kwargs.get('title', request.url.split('//', 1)[-1])}}</title>
</head>
<body>
% if note:
<input class="flip" id="flip1" type="checkbox"/><label class="key notice top" for="flip1"></label><article class="card">{{note}}</article>
% end
<header>
	<a class="img" href="http://badw.de"><img src="/-/icons/badw_marke_name.svg" alt="BAdW"/></a>
	<h1>Kleiner Dienst
	<small title="Homer: Odyssee (Gesang 6 Vers 208, Gesang 14 Vers 58)">δόσις δʼ ὀλίγη τε φίλη τε</small></h1>
	<nav>
		<ul>
		  % if not request.roles:
			<li><strong>Anmeldung</strong></li>
		  % else:
			<li><strong><a href="/auth_end">Abmeldung</a></strong></li>
			<li>{{!'<b>Bedienbarkeitsbericht</b>' if request.path == '/bedienbarkeitsbericht' else '<a href="/bedienbarkeitsbericht">Bedienbarkeitsbericht</a>'}}</li>
			<li>{{!'<b>Linkrissfinder</b>' if request.path == '/linkrissfinder' else '<a href="/linkrissfinder">Linkrissfinder</a>'}}</li>
			<li>{{!'<b>Zwischenablage</b>' if request.path == '/zwischenablage' else '<a href="/zwischenablage">Zwischenablage</a>'}}</li>
			<li>{{!'<b>Datentafeln</b>' if request.path == '/datentafeln' else '<a href="/datentafeln">Datentafeln</a>'}}</li>
		  % end
		</ul>
	</nav>
	<nav class="extra">
		<p>
			<a href="http://badw.de/data/footer-navigation/datenschutz.html">Datenschutz</a> <br/>
			<a href="http://badw.de/data/footer-navigation/impressum.html">Impressum</a>
		</p>
	</nav>
</header>
% include(kwargs['tpl'], db = db, request = request, kwargs = kwargs)
</body>
</html>
