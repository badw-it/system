# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2015 ff. (© http://badw.de)
import os
import shutil
import sys
import time

import __init__
import fs
import sys_io

def main(
        pid: int,
        urdata: 'list[str]',
        ) -> None:
    '''
    Delete temporary files or directories determined by their parent directory –
    along with a timespan after which they are to be deleted, e.g.:

    ``zwischenablage = 604800``

    Read the config (see below about :param:`urdata`). Enter the following loop:

    - Exit, when no process with :param`pid` is running anymore. This PID should
      be the process ID of the process having started this function.
    - Delete the content of the directories specified in ``config['temp']`` (see
      the comment in the config) when they are older than the timespan specified
      along with the directory.
    - Start anew.

    Any occurring errors or issues are written into a :class:`fs.Log` file.

    :param urdata: may contain zero, one or more absolute paths of files.
        Each of these files must have the structure of ini-files as it is
        understood by :module:`configparser`. They are read into a config
        and contain, among other information, paths.
    '''
    log = fs.Log(__file__, '../../../__logs__')
    sys.stderr = log
    sys.stdout = log
    config = fs.get_config(urdata)
    paths = fs.get_abspaths(config, {
            'paths_from_program_folder': __file__,
            'paths_from_config_folder': urdata[0],
            })
    timeout = int(config['temp']['timeout'])
    while True:
        if not sys_io.pid_exists(pid):
            sys.exit(0)
        time.sleep(10)
        for name in os.listdir(paths['temp']):
            path = os.path.join(paths['temp'], name)
            if time.time() - os.path.getmtime(path) > timeout:
                shutil.rmtree(path, ignore_errors = True)

if __name__ == '__main__':
    main(int(sys.argv[1]), sys.argv[2:])
