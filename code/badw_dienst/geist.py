# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
'''
This module is the core and controller of an app built with the framework Geist.
Cf. the documentation here: https://gitlab.lrz.de/badw-it/geist
'''
from gevent import monkey
monkey.patch_all()
import json
import os
import sys
import time
import traceback
from urllib.parse import urlsplit
try:
    from beaker.middleware import SessionMiddleware
except:
    SessionMiddleware = None

import check_doc
import gehalt
import __init__
import bottle
import web_io
from bottle import HTTPError, redirect, request, response, static_file, template

class Geist(bottle.Bottle):
    '''
    The controller object: routes any request from the server to a function,
    reacts on the request in the function, possibly accessing the data base,
    builds a page as an answer to the request and sends the page back.
    '''
    def __init__(self, urdata: 'list[str]'):
        super().__init__()
        db = gehalt.DB(urdata)
        bottle.TEMPLATE_PATH = db.paths['templates']
        bottle.BaseRequest.MEMFILE_MAX = db.config['connection'].get(
                'memfile_max', 10_000_000)
        if db.https:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    certfile = db.paths['ssl_cert'],
                    keyfile  = db.paths['ssl_key'],
                    )
        else:
            server = bottle.server_names[db.server_name](
                    host     = db.host,
                    port     = db.port,
                    )
        server.quiet = False if db.debug else True
        self.kwargs = {
                'app': self if SessionMiddleware is None else
                    SessionMiddleware(self, db.config['session']),
                'host': db.host,
                'port': db.port,
                'debug': db.debug,
                'server': server,
                }
        self.set_routes(db)

    def set_routes(self, db) -> None:
        '''
        Define the request-handling functions and assign them to routes.
        '''
        @self.hook('before_request')
        def status():
            request.user_id, request.user_name, request.roles = db.auth(request)

        @self.hook('before_request')
        def redirect_to_https():
            if request.urlparts[0] == 'http' and not db.debug:
                redirect(request.url.replace('http', 'https', 1), code = 302)

        @self.route('<path:path>/')
        def redirect_endslash(path):
            '''
            Redirect requests whose URL ends with `/`: Ignore the trailing `/`.
            '''
            redirect(path)

        @self.route('/')
        def redirect_short_url():
            '''
            Redirect requests which do not specify the page to the default page.
            '''
            redirect('/' + db.page_id)

        @self.route('/cssjs/<path:path>')
        def return_cssjs(path):
            '''
            Return a CSS or JS file for client-side styling or processing.
            For static content files, see :func:`.return_static_content`.

            Example URLs are:

            - `/cssjs/jquery/jquery.min.js`
            - `/cssjs/all.css`

            The path to the file results from joining ``db.paths['cssjs']`` with
            :param:`path` and must be a real subpath of ``db.paths['cssjs']``.
            '''
            return static_file(
                    path,
                    db.paths['cssjs'],
                    mimetype = 'text/' + (
                        'css' if path.endswith('.css') else
                        'javascript' if path.endswith('.js') else
                        'plain'),
                    )

        @self.route('/auth_end')
        def auth_end():
            session = request.environ.get('beaker.session', None)
            if session is not None:
                session.delete()
            redirect(request.query.url or '/' + db.page_id)

        @self.route('/' + db.auth_in_page_id)
        def auth_in():
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': 'auth_in.tpl', 'title': 'Anmeldung'})

        @self.route('/' + db.auth_in_page_id, method = 'POST')
        def auth_in_post():
            user_name = request.forms.user_name
            password = request.forms.password
            user_id = db.auth_in(user_name, password)
            if user_id is None:
                query = request.forms.query
                if query:
                    query = '?' + query
                redirect(request.fullpath + query)
            else:
                session = request.environ['beaker.session']
                session['user_id'] = user_id
                session.save()
                redirect(request.forms.url or '/' + db.page_id)

        @self.route('/datentafeln')
        def datentafeln(note = ''):
            if 'editor' not in request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'datentafeln.tpl',
                        'datatablenames': db.datentafeln(),
                        'note': note,
                        })

        @self.route('/validate', method = 'POST')
        def validate_post():
            response.content_type = 'application/json'
            return json.dumps(db.validate(request), separators = (',', ':'))

        @self.route('/varia')
        @self.route('/varia/<path:path>')
        def return_varia(path = ''):
            '''
            Serve pages:

            - If :param:`path` is specified, take it as a relative path starting
              from ``db.paths['static']``. Derive the absolute path. Ensure that
              it is still inside ``db.paths['static']``. Serve the file found at
              the absolute path as a static page.
            - Else, take the query value of the key “url” as the URL of the page
              to be served.
            '''
            if path:
                return static_file(
                        path,
                        db.paths['static'],
                        download = ('export' in request.query),
                        )
            else:
                url = request.query.url
                if url and (
                        url.startswith('https://daten.badw.de/') or
                        url.startswith('https://gitlab.lrz.de/badw-data/')):
                    try:
                        data = web_io.read(url, None)
                        if request.query.form == 'pdf':
                            filename = urlsplit(url).path.rstrip('/').rsplit(
                                    '/', 1)[-1] or 'data.pdf'
                            response.set_header(
                                    'Content-Disposition',
                                    f'attachment; filename="{filename}"'
                                        if request.query.export else
                                    f'filename="{filename}"')
                            response.content_type = 'application/pdf'
                            return data
                        else:
                            encoding = request.query.encoding or 'utf-8'
                            data = data.decode(encoding = encoding)
                            tpl = request.query.template
                            if tpl:
                                return template(
                                        tpl, db = db, request = request,
                                        kwargs = {
                                            'url': url,
                                            'data': data,
                                            })
                            else:
                                return data
                    except Exception as e:
                        return traceback.format_exception(e)[-1]
                else: redirect('/')

        @self.route('/zwischenablage')
        def zwischenablage(note = '', url = ''):
            if 'editor' not in request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'zwischenablage.tpl',
                        'note': note,
                        'url': url,
                        })

        @self.route('/zwischenablage', method = 'POST')
        def zwischenablage_post():
            if 'editor' not in request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            note, dirname, filename = db.zwischenablage(request)
            if note:
                return zwischenablage(note = note)
            else:
                dirname = db.q(dirname, safe = '')
                filename = db.q(filename, safe = '')
                return zwischenablage(url =
                        f'/zwischenablage/{dirname}/{filename}')

        @self.route('/zwischenablage/<dirname>/<filename>')
        def return_zwischenablage(dirname, filename):
            return static_file(
                    os.path.join('zwischenablage', dirname, filename),
                    db.paths['temp'],
                    download = True,
                    )

        @self.route('/<page_id:re:(bedienbarkeitsbericht|linkrissfinder)>')
        def check_pages(page_id, note = ''):
            if 'editor' not in request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {
                        'tpl': 'check_pages.tpl',
                        'note': note,
                        'page_title': page_id.title(),
                        })

        @self.route(
                '/<page_id:re:(bedienbarkeitsbericht|linkrissfinder)>',
                method = 'POST')
        def check_pages_post(page_id):
            if 'editor' not in request.roles:
                redirect('/auth_in?url=' + db.q(request.fullpath))
            start_url = request.forms.url
            page_title = page_id.title()
            response.content_type = 'text/html; charset=UTF8'
            yield f'''<!DOCTYPE html>
<head>
	<meta charset="utf-8"/>
	<title>Dienste · {page_title}</title>
</head>
<h1>{page_title}</h1>
'''
            check = {
                    'bedienbarkeitsbericht': check_doc.check_a11y,
                    'linkrissfinder': check_doc.check_links,
                    }[page_id]
            num = 0 # if the following iterator is empty.
            for num, url, term, note in db.check_pages(check, start_url):
                if note:
                    yield f'''
<p>({num}) <a href="{url}">{url}</a>: <b>{note}</b>: <code>{term}</code></p>
'''
                else:
                    yield f'<a href="{url}">{num}</a> '
            yield f'''
<p><strong>Ende</strong> (Anzahl geprüfter Seiten: {num})</p>
'''

        @self.route('/<page_id>/json')
        @self.route('/<site_id>/<page_id>/json')
        def return_data(page_id, site_id = ''):
            '''
            Return data for :param:`site_id` (if given) and :param:`page_id`.
            '''
            return static_file(
                    f'{site_id}/{page_id}.json',
                    db.content_path,
                    mimetype = 'application/json',
                    )

        @self.route('/<page_id>')
        @self.route('/<site_id>/<page_id>')
        def return_page(page_id, site_id = ''):
            '''
            Return the page :param:`page_id`.
            '''
            text, tpl = db.get_text_and_template_name(site_id, page_id)
            return template(
                    'base.tpl', db = db, request = request,
                    kwargs = {'tpl': tpl, 'text': text})

        @self.route('/<_>/<path:path>')
        def return_static_content(_, path):
            '''
            Return a static content file, e.g. an image file.

            Example URLs are:

            - `/de/icons/favicon.ico`
            - `/de/acta/secretissima/1890-03-20.pdf?export`

            If the URL contains a query string with a key `export`,
            the static file is served for download.

            On the server, the path to the file must be a real subpath of
            the content folder and a joint of its path and :param:`path`.
            '''
            return static_file(
                    path,
                    db.content_path,
                    download = ('export' in request.query),
                    )

    def start(self):
        if self.kwargs['debug']:
            bottle.run(**self.kwargs)
        else:
            while True:
                try:
                    bottle.run(**self.kwargs)
                except: # Sic.
                    time.sleep(2)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        urdata = sys.argv[1:]
    else:
        urdata = ['config.ini', 'config_test.ini']
    Geist(urdata).start()
