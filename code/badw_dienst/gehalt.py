# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
import io
import os
try: import regex as re
except ImportError: import re
import secrets
import subprocess
import sys
import typing as ty
from urllib.parse import urlsplit

from lxml import etree

import __init__
import bottle
import fs
import parse
import web_io

class DB(web_io.IO):
    def __init__(self, urdata: 'list[str]') -> None:
        '''
        The docstring of web_io.IO.__init__ applies.
        '''
        super().__init__(urdata, __file__)
        self.process_temp = subprocess.Popen([
                sys.executable,
                fs.join(__file__, '../delete_temp.py'),
                str(os.getpid()),
                urdata[0],
                urdata[1],
                ])

    def check_pages(
            self,
            check: 'ty.Callable[[str], tuple[str, str]]',
            start_url: str,
            skip_url_path_re: 're.Pattern[str]' =
                re.compile(r'(?i)\.(?!html?|xml|xhtml?).{3,5}$'),
            ) -> 'Generator[tuple[int, str, str, str]]':
        '''
        Check webpages and report the results.

        The check is done with the function param:`check` – it takes the content
        of a page, checks if there is an issue and, if yes, yields a description
        of it and the passage which caused it.

        For each issue encountered, yield:

        - the current number of considered pages.
        - the URL of the currently considered page.
        - said passage that caused the issue (if available).
        - said description (which may also be a denotation or an error message).

        These pages are found by a crawl starting at the page :param:`start_url`
        and proceeding iteratively with all linked pages of the same domain (and
        the same subdomains, if given, but insensitive to “www.”) whose URL path
        does not match :param:`skip_url_path_re`.

        The crawl is done with :func:`web_io.iter_pages`.
        '''
        def adapt_even_eval_url(
                scheme: str, netloc: str, path: str, query: str, fragment: str,
                ) -> 'tuple[str, str, bool, bool]':
            if netloc[:4] == 'www.':
                netloc = netloc[4:]
            if netloc != start_netloc:
                return '', '', False, False
            path = path.rstrip('/')
            if skip_url_path_re.search(path):
                return '', '', False, False
            if query:
                query = '?' + query
            return f'{scheme}://{netloc}{path}{query}', path, True, True

        note = ''
        start_netloc = urlsplit(start_url).netloc
        if start_netloc[:4] == 'www.':
            start_netloc = start_netloc[4:]
        for num, (url, doc, note) in enumerate(web_io.iter_pages(
                start_url,
                adapt_even_eval_url,
                request_kwargs = {'headers': {'User-Agent': 'kleiner dienst'}},
                pause = 0.05,
                ), start = 1):
            if note:
                yield num, url, '', note
            else:
                try:
                    yield num, url, '', '' # the following iterator may be empty.
                    for term, note in check(url, doc):
                        yield num, url, term, note
                except Exception as e:
                    yield num, url, '', str(e)

    def datentafeln(self) -> 'list[str]':
        return sorted((
                (self.q(name[:-4]), name[:-4].title())
                for name in os.listdir(self.content_path + 'datatables')
                if name.endswith('.xml')
                ), key = lambda x: parse.sortxtext(x[1]))

    def validate(
            self,
            request: 'bottle.LocalRequest',
            ) -> 'dict[str, None|bool|str]':
        report = {
                'valid': None,
                'error': '',
                }
        try:
            assert 'doc' in request.forms, 'The POST data lack a key “doc”.'
            assert 'dtd' in request.forms, 'The POST data lack a key “dtd”.'
            dtd = etree.DTD(io.StringIO(request.forms.dtd))
            root = etree.XML(request.forms.doc)
            valid = dtd.validate(root)
            if valid:
                report['valid'] = True
            else:
                report['valid'] = False
                report['error'] = str(dtd.error_log.filter_from_errors()[0])
        except Exception as e:
            report['error'] = str(e)
        return report

    def zwischenablage(
            self,
            request: 'bottle.LocalRequest',
            max_size: int = 3 * (1024 ** 3), # 3 gibibytes
            dirname_maxnum: int = 1_000_000_000_000_000_000_000,
            ) -> 'tuple[str, str, str]':
        note = dirname = infilename = ''
        try:
            file = request.files.file
            if not file:
                raise Exception('Es ist keine Datei gegeben.')
            infilename = fs.sanitize(file.raw_filename)
            dirname = str(secrets.randbelow(dirname_maxnum))
            dirpath = fs.pave_dir(fs.get_new_path(
                    os.path.join(self.paths['temp'], 'zwischenablage', dirname),
                    diff_char = '0',
                    ))
            dirname = os.path.basename(dirpath)
            infilepath = os.path.join(dirpath, infilename)
            file.save(infilepath, overwrite = True, max_size = max_size)
        except Exception as e:
            note = str(e)
        return note, dirname, infilename
