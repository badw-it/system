# Licensed under http://www.apache.org/licenses/LICENSE-2.0
# Attribution notice: by Stefan Müller in 2020 ff. (© http://badw.de)
try: import regex as re
except ImportError: import re
import typing as ty

from html import escape, unescape
from urllib.parse import urljoin

import __init__
import web_io
import xmlhtml

def check_a11y(url: str, doc: str) -> 'ty.Generator[tuple[str, str]]':
    '''
    Check :param:`doc` (at :param:`url`):

    - every img-element has an alt- or title-attribute.
    - every a-element contains …

      - non-whitespace text
      - or a title-attribute
      - or an img-element, that has an alt- or title-attribute.

    If there is an issue, yield:

    - the passage that caused the issue (if available).
    - the description (which may also be a denotation or an error message).

    .. important::
        Both strings are HTML strings.
    '''
    a_sans_info: 'tuple[int, str, dict[str, str]]' = (0, '', {})

    for num, start, attrs, end, text, *_ in xmlhtml.itemize(doc):
        note: str = ''
        term: str = ''

        if start == 'img':
            if 'alt' in attrs or 'title' in attrs:
                if a_sans_info[0]:
                    a_sans_info = (0, '', {})
            else:
                note = '<img> has neither @alt nor @title.'
                term = f'{start}{serialize(attrs)}'
        elif start == 'a' and 'title' not in attrs:
            a_sans_info = (num, start, attrs)

        if end == 'a' and num == a_sans_info[0]:
            note = '<a> has neither @title nor non-whitespace text'\
                    ' nor an inner <img> with @alt or @title.'
            term = f'{a_sans_info[1]}{serialize(a_sans_info[2])}'
            a_sans_info = (0, '', {})

        if text.strip() and a_sans_info[0]:
            a_sans_info = (0, '', {})

        if note:
            yield escape(term), escape(note)

def check_links(url: str, doc: str) -> 'ty.Generator[tuple[str, str]]':
    '''
    Check :param:`doc` (at :param:`url`), whether links are broken.

    If there is an issue, yield:

    - the passage that caused the issue (if available).
    - the description (which may also be a denotation or an error message).

    .. important::
        Both strings are HTML strings.
    '''
    current_netloc = web_io.urlsplit_norm(url)[1]
    for m in re.finditer(
            r'''<a\b(?P<attrs>(?:"[^"]*"|'[^']*'|[^>])*)>(?:(?:(?!</a\b).)*)'''
            r'''</a[^>]*>''',
            doc):
        mm = re.search(
                r'''\bhref\s*=\s*(?:"(?P<url>[^"]*)|'(?P<url>[^']*))''',
                m.group('attrs'))
        if not mm:
            continue
        scheme, netloc, path, query, fragment = web_io.urlsplit_norm(
                urljoin(url, mm.group('url').strip()))
        if scheme == 'mailto':
            continue
        if netloc in {current_netloc, 'badw.de', 'www.badw.de'}:
            continue
        if query:
            query = '?' + query
        url_to_check = f'{scheme}://{netloc}{path}{query}'
        try:
            web_io.read(
                    url_to_check,
                    None,
                    request_kwargs = {'headers': web_io.HEADERS},
                    urlopen_kwargs = {'timeout': 10},
                    )
        except Exception as e:
            term = escape(m.group())
            yield (
                    f'{term} → <a href="{url_to_check}">{url_to_check}</a>',
                    escape(str(e)))

def serialize(attrs: 'dict[str, str]') -> str:
    return xmlhtml.serialize_attrs(attrs, sort = False)
