// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2013 ff. (© http://badw.de)
/** Handle click on cell in editable datatable */
function cellAct(event, cols) {
	const send = () => {
		$.post(window.location.href, {
			'act': 'update_cell',
			'table_name': col['table_name'],
			'col_name': col['name'],
			'row_id': tr.id,
			'val': td.innerHTML,
		}).done((data) => {
			if (data['note']) {
				announce(data['note']);
			} else if (!col['id?']) {
				$('#index').DataTable().row('#' + tr.id).data(data['row']).draw('page');
				td.classList.add('blink');
				setTimeout(() => {td.classList.remove('blink')}, 3000);
			};
			td.removeAttribute('contenteditable');
		});
	};
	const deleteRow = () => {
		$.post(window.location.href, {
			'act': 'delete_row',
			'table_name': col['table_name'],
			'row_id': tr.id,
		}).done((data) => {
			if (data['note']) {
				announce(data['note']);
			} else {
				$('#index').DataTable().row('#' + tr.id).remove().draw();
			};
		});
	};
	const deleteValue = () => {
		td.innerHTML = '';
		send();
	};
	let td = event.target;
	while (td !== null && td !== this && td.tagName !== 'TD') {
		td = td.parentNode;
	};
	if (td === null || td.tagName !== 'TD') {return}; // The click did not go into a table cell.
	const tr = td.parentNode;
	if (!tr.id && tr.id !== 0) {return}; // The containing table row has no ID.
	const pos = td.cellIndex;
	const col = cols[pos];
	if (col['name'][0] === '_') {
		return;
	} else if (event.type === 'contextmenu') {
		event.preventDefault();
		let menu = document.querySelector('body > .contextmenu');
		menu.style.left = (event.pageX - 32) + 'px';
		menu.style.top = event.pageY + 'px';
		menu.getElementsByClassName('del')[0].addEventListener('click', deleteRow, {'once': true});
		let refButton = menu.getElementsByClassName('ref')[0];
		let delrefButton = menu.getElementsByClassName('delref')[0];
		if (col['reftable_name']) {
			refButton.style.display = 'inline';
			delrefButton.style.display = 'inline';
			refButton.href = `${col['reftable_name']}#${td.innerHTML}`;
			delrefButton.addEventListener('click', deleteValue, {'once': true});
		} else {
			refButton.style.display = 'none';
			delrefButton.style.display = 'none';
		}
		menu.showModal();
	} else {
		if (col['id?']) {
			localStorage.setItem('refrow_id', `${col['table_name']}\`${col['name']}\`${td.innerHTML}`);
		} else if (col['enum']) {
			let values = [];
			for (let i = 0; i < col['enum'].length; i++) {
				let value = col['enum'][i];
				let attrs = value === td.innerHTML ? ' autofocus="" class="dark"' : '';
				value = value.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				values.push(`<button${attrs}>${value}</button>`);
			};
			let menu = document.createElement('dialog');
			menu.className = 'contextmenu';
			menu.innerHTML = `<form method="dialog">${values.join('<br/>')}</form>`;
			menu.style.left = (event.pageX - 32) + 'px';
			menu.style.top = event.pageY + 'px';
			document.body.appendChild(menu);
			menu.addEventListener('click', function(event) {
				if (event.target.tagName === 'BUTTON' && event.target.className === '') {
					td.innerHTML = event.target.innerHTML;
					menu.remove();
					send();
				} else {menu.remove()};
			}, {'once': true});
			menu.showModal();
		} else if (col['reftable_name']) {
			try {
				var [reftable_name, refcol_name, refrow_id] = localStorage.getItem('refrow_id').split('`');
			} catch {return};
			if (reftable_name !== col['reftable_name'] || refcol_name !== col['refcol_name'] || !refrow_id) {return};
			td.innerHTML = refrow_id;
			send();
		} else {
			if (td.getAttribute('contenteditable') !== null) {return};
			td.setAttribute('contenteditable', '');
			td.focus();
			td.addEventListener('keydown', function(event) {if (event.key === 'Enter') {event.preventDefault(); send();};});
			td.addEventListener('blur', send, {'once': true});
		};
	};
}
function cellsEventListener(act, cols) {
	let element = document.getElementById('index');
	for (let eventname of ['click', 'contextmenu']) {
		if (act === 'add') {
			element.addEventListener(eventname, function(event) {cellAct(event, cols)});
		} else if (act === 'remove') {
			element.removeEventListener(eventname, function(event) {cellAct(event, cols)});
		};
	};
}
/** For the function tablePreset. */
function filterApply(rank, flag, term) {
	if (flag !== null) {
		var caseInsen = flag.indexOf('i') === -1 ? false : true;
		var regex = flag.indexOf('r') === -1 ? false : true;
		var smart = regex ? false : (flag.indexOf('s') === -1 ? false : true);
	};
	if (rank === '_') { // Global search in any column.
		$('#global_term').val(term);
		if (flag !== null) {
			$('#global_regex').prop('checked', regex);
			$('#global_smart').prop('checked', smart);
			$('#global_caseInsen').prop('checked', caseInsen);
		};
		return filterGlobal();
	} else {
		$('#column' + rank + '_term').val(term);
		if (flag !== null) {
			$('#column' + rank + '_regex').prop('checked', regex);
			$('#column' + rank + '_smart').prop('checked', smart);
			$('#column' + rank + '_caseInsen').prop('checked', caseInsen);
		};
		return filterColumn(rank);
	};
}
/** Fulltext search in a column specified by its rank. */
function filterColumn(rank) {
	return $('#index').DataTable().column(rank).search(
		$('#column' + rank + '_term').val(),
		$('#column' + rank + '_regex').prop('checked'),
		$('#column' + rank + '_smart').prop('checked'),
		$('#column' + rank + '_caseInsen').prop('checked')
	);
}
/** Fulltext search in all columns. */
function filterGlobal() {
	return $('#index').DataTable().search(
		$('#global_term').val(),
		$('#global_regex').prop('checked'),
		$('#global_smart').prop('checked'),
		$('#global_caseInsen').prop('checked')
	);
}
/**
* Attribution notice: by Daniel Schwarz in 2019 ff based on http://live.datatables.net/gefemike/2/edit, modified by Stefan Müller in 2019 ff.
* Modify the filter by modifying the input field and the regex checkbox.
* @param {object} table - The datatable to be filtered.
* @param {integer} col - The column of this table.
* @param {string} modus - signals which modification will be applied.
* @param {string} ante - will be prepended to the input string.
* @param {string} post - will be appended to the input string.
*/
function filterModify(table, col, modus, ante, post) {
	let inputField = document.getElementById(`column${col}_term`);
	let currentRawInput = $(inputField).val().split(ante).join('').split(post).join('');
	if (modus === 'neutral') {
		$(inputField).val(currentRawInput);
		$(`#column${col}_regex`).prop('checked', false);
	} else if (modus === 'formstart') {
		$(inputField).val(ante + currentRawInput);
		$(`#column${col}_regex`).prop('checked', true);
		inputField.setSelectionRange(currentRawInput.length + ante.length, currentRawInput.length + ante.length);
	} else if(modus === 'formend') {
		$(inputField).val(currentRawInput + post);
		$(`#column${col}_regex`).prop('checked', true);
		inputField.setSelectionRange(currentRawInput.length, currentRawInput.length);
	};
	$(`#column${col}_term`).focus();
	$(`#column${col}_term`).change();
	table.draw();
}
/**
* Populate any existing select filter that is not yet populated.
* (They get lazily populated on the first use.)
* Extract the values from the column to which the filter belongs:
* Normalize any value and split it up at any semicolon, thus gaining subvalues.
* Add each subvalue to the select filter, if said subvalue is not added yet.
*/
function filterPopulate(table, elem, rank) {
	if (!elem[0].hasAttribute('data-filled')) {
		let data = table.column(parseInt(rank)).data();
		let values = [];
		let reTrim = /\u003c(div|span) (class=['"]trim['"]|hidden=['"]['"])\u003e.*?\u003c\u002f(div|span)\u003e/g;
		let reTag = /\u003c[^\u003e]*\u003e/g;
		let reLt = /\u0026lt;/g;
		let reGt = /\u0026gt;/g;
		let reAmp = /\u0026amp;/g;
		let semicolonSubst = /\u003c\u003e/g;
		let i = data.length;
		while (i--) {
			let value = data.pop();
			if (typeof value === 'object') {value = value._};
			if (value === 0) {
				value = '0';
			} else if (typeof value !== 'string') {
				value = String(value || '');
			};
			value = value.replace(reTrim, "").replace(reTag, "").replace(reLt, "\u0026lt\u003c\u003e").replace(reGt, "\u0026gt\u003c\u003e").replace(reAmp, "\u0026amp\u003c\u003e");
			let subvalues = value.split(';');
			let ii = subvalues.length;
			while (ii--) {
				let value = subvalues[ii].trim().replace(semicolonSubst, ';');
				if (values.indexOf(value) === -1) {
					values.push(value);
				};
			};
		};
		values.sort(function(a, b) {
			var a = a.toLowerCase();
			var b = b.toLowerCase();
			return a < b ? -1 : +(a > b);
		});
		for (let i = 0; i < values.length; i++) {
			elem.append('<option>' + values[i] + '</option>');
		};
		elem[0].setAttribute('data-filled', '');
	};
}
function filterSetOnTextContent(event, colNum, checked) {
	const colName = colNum === '_' ? 'global' : `column${colNum}`;
	const term = event.target.textContent.trim();
	let input = document.getElementById(`${colName}_term`);
	document.getElementById(`${colName}_smart`).checked = checked;
	input.value = term;
	input.classList.add('blink');
	$('#index').DataTable().column(colNum).search(term).draw();
	setTimeout(function() {input.classList.remove('blink')}, 3000);
}
/** For every row element, set attributes (e.g. `id`) provided by a non-XML/HTML data source. */
function rowAddAttrs(row, data, dataIndex) {
	let rowAttrs = data.rowAttrs;
	if (rowAttrs !== null) {
		for (let key in rowAttrs) {
			row.setAttribute(key, rowAttrs[key]);
		}
	}
}
/**
* Attribution notice: by Daniel Schwarz in 2019 based on http://live.datatables.net/gefemike/2/edit, updated by Stefan Müller in 2019 ff.
* Jump to the row whose ID is `id`.
*/
function rowAt(id) {
	// Reset the search filters.
	$("#datatable-reset").click();
	let row = $('#index').DataTable().row(`#${id}`);
	if (row.length > 0) {
		row.show().draw(false);
		window.location.hash = '';
		window.location.hash = `#${encodeURIComponent(id)}`;
		// whereas `document.getElementById(…).scrollIntoView()` does not trigger e.g. Chrome to apply a :target rule of CSS.
	};
}
function rowInsert(event, table_name) {
	$.post(window.location.href, {
		'act': 'insert_row',
		'table_name': table_name,
	}).done((data) => {
		if (data['note']) {
			announce(data['note']);
		} else if (data['row']) {
			$('#index').DataTable().row.add(data['row']).draw().node();
			rowAt(data['row']['DT_RowId']);
		};
	});
}
function tableParamsFromId(id) {
	let params = {};
	id.slice(1).split('~').forEach((segment) => {
		[_, col, flag, term] = segment.match('([^-]*)-([^-]*)-(.*)');
		if (col) {
			params[col] = [flag, term.replaceAll('_1', '~').replaceAll('_0', '_')];
		} else {
			let order = [];
			flag.split('.').forEach((sorter) => {
				[_, sortcol, sortdir] = sorter.match('(\\d+)(a|d)');
				sortdir = (sortdir === 'a') ? 'asc' : 'desc';
				order.push([sortcol, sortdir]);
			});
			params[''] = order;
		};
	});
	return params;
}
function tableParamsFromState(state) {
	if (!state) {return {}};
	let params = {'': state.order};
	if (state) {
		let flag_term = tableParamsFromStateSearch(state.search);
		if (flag_term) {
			params['_'] = flag_term;
		};
		let i = state.columns.length;
		while (i--) {
			let flag_term = tableParamsFromStateSearch(state.columns[i].search);
			if (flag_term) {
				params[i.toString()] = flag_term;
			};
		};
	};
	return params;
}
function tableParamsFromStateSearch(searchobject) {
	let term = searchobject.search;
	if (term) {
		let flag = '';
		if (searchobject.caseInsensitive) {flag += 'i';};
		if (searchobject.regex) {flag += 'r';};
		if (searchobject.smart) {flag += 's';};
		return [flag, term];
	} else {
		return null;
	};
}
function tableParamsToId(params) {
	if ($.isEmptyObject(params)) {return ''};
	let settings = [];
	Object.entries(params).forEach(([col, setting]) => {
		if (!$.isEmptyObject(setting)) {
			if (col) {
				var flag = setting[0];
				var term = setting[1].replaceAll('_', '_0').replaceAll('~', '_1');
			} else {
				var flag = setting.map(function(sorter) {return sorter[0] + sorter[1].slice(0, 1)}).join('.');
				var term = '';
			};
			settings.push(`${col}-${flag}-${term}`);
		};
	});
	return `~${settings.join('~')}`;
}
/** Connect the functions for populating the dropdowns and for searching.
* Set the input filters according to the given search params.
*/
function tablePreset(settings, json, params) {
	if ($.isEmptyObject(params)) {return};
	let datatable = $('#index').DataTable();
	$('#filter_card > table td select').each(
		function() {
			let elem = $(this);
			let rank = elem.parents('tr').attr('data-column_rank');
			// Connect the function for populating the drop-down menu:
			elem.on('focus', function() {filterPopulate(datatable, elem, rank)});
			// Connect the function for filtering the datatable according to the selection:
			elem.on('change', function() {filterApply(rank, null, elem.val()).draw()}); // The ``draw`` is necessary here.
		}
	);
	$('#filter_card > table td input[type="search"]').each(
		function() {
			let elem = $(this);
			let rank = elem.parents('tr').attr('data-column_rank');
			// Connect the function for filtering the datatable according to the selection:
			elem.on('change', function() {filterApply(rank, null, elem.val())}); // A ``draw`` would be superfluous here.
			// Preselect, if any preselection is given by the search params:
			if (rank in params) {
				let flag = params[rank][0];
				let term = params[rank][1];
				this.setAttribute('value', term);
				filterApply(rank, flag, elem.val());
			};
		}
	);
	if ('' in params) {datatable.order(params[''])};
	return datatable;
}
/** Reset sorting and all filters and redraw the table. */
function tableReset(order) {
	history.pushState('', document.title, window.location.pathname + window.location.search);
	document.querySelectorAll('.filterbasismodus').forEach(
		function(filterbasismodus) {filterbasismodus.click()}
	);
	document.querySelectorAll('#filter_card > table td input[type="search"]').forEach(
		function(input) {input.setAttribute('value', '')}
	);
	document.querySelectorAll('#filter_card > table td select').forEach(
		function(select) {select.setAttribute('value', '')}
	);
	$('#index').DataTable().order(order).search('').columns().search('').draw();
}
/**
* Configure the datatable. Preset filter and order of rows according to any internally saved state.
* Use any URI hash that is either starting with “~” or not.
* If it does not, it is assumed to be an ID that should trigger a jump to the very row having that ID.
* If it does, it is assumed to be a string saving a certain state of the table, e.g.:
* ~-0a.1d-_--1808~0-irs-g(%C3%B6|oe)theanisch
* A “~” starts any segment of the state-saving string.
* “-” characters divide any segment into three subsegments.
* In the first subsegment, a “_” means ‘all columns’; any integer n means the nth column, starting with 0; an empty string signals sorting.
* The second subsegment may contain a flag:
* A flag for filtering consists of any of the three letters “i”, “r”, “s” (in any order):
* “i” for ‘insensitive regarding the case’,
* “s” for ‘smart’ (roughly: the term is split up at spaces into single words whose order does not matter for the search),
* “r” for ‘regex search (and no smart search)’.
* A flag for sorting consists of one or more subsubsegments divided with a “.”.
* A subsubsegment consists of a column number and an “a” for ‘ascending’ or a “d” for ‘descending’.
* In the third subsegment, for filtering the search term is specified.
* Important: In this term, “_” must be escaped as “_0” and “~” must be escaped as “_1”.
* Then the whole URI hash, of course, must be escaped as URI component.
*/
function tableSetup(config) {
	const id = decodeURIComponent(window.location.hash.slice(1));
	config.initComplete = function(settings, json) {
		let params = tableParamsFromState(this.api().state.loaded() || datatable.state());
		if (!$.isEmptyObject(params)) {
			tablePreset(settings, json, params).draw();
		};
		if (id) {
			if (id[0] === '~') { // So `id` is not an actual id-value but a filter state.
				tablePreset(settings, json, tableParamsFromId(id)).draw();
			} else {
				rowAt(id);
				if (Number.isInteger(config['click_nth_onclick_element'])) {
					document.getElementById(id).querySelectorAll('[onclick]')[config['click_nth_onclick_element']].click();
				}
			};
		};
	};
	if ('ajax' in config) {config.createdRow = rowAddAttrs;};
	// Create the table here, otherwise a jump to an id-determined row would not work.
	let datatable = $('#index').DataTable(config);
	// Connect a change in filter and sorting with the hash of the URL:
	datatable.on('draw', function() {
		let params = tableParamsFromState(datatable.state());
		if (!$.isEmptyObject(params)) {
			window.location.hash = '#' + encodeURIComponent(tableParamsToId(params));
		};
	});
	// Connect the fulltext search for all columns:
	$('#global_term').on('keyup', function() {filterGlobal().draw();});
	// Connect the checkbox settings for all columns:
	$('#filter_global :checkbox').change(function() {filterGlobal().draw();});
	// Connect the fulltext search for a specific column:
	$('#filter_card input.column').on('keyup', function() {filterColumn(parseInt($(this).parents('tr').attr('data-column_rank'))).draw()});
	// Connect the fulltext search for a specific column:
	$('#filter_card input.column:checkbox').change(function() {filterColumn(parseInt($(this).parents('tr').attr('data-column_rank'))).draw()});
	// Override the style inserted by datatables.
	document.getElementById('index').style.width = '100%';
	// On the event triggered by the pagination buttons, scroll to top.
	$('#index').on('page.dt', function () {window.scrollTo(0, 0);});
	return datatable;
}
