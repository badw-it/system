// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2018 ff.
const charmap = [
[161, 'inverted exclamation mark'],
[162, 'cent sign'],
[163, 'pound sign'],
[164, 'currency sign'],
[165, 'yen sign'],
[166, 'broken bar'],
[167, 'section sign'],
[168, 'diaeresis'],
[169, 'copyright sign'],
[170, 'feminine ordinal indicator'],
[171, 'left-pointing double angle quotation mark'],
[172, 'not sign'],
[173, 'soft hyphen'],
[174, 'registered sign'],
[175, 'macron'],
[176, 'degree sign'],
[177, 'plus-minus sign'],
[178, 'superscript two'],
[179, 'superscript three'],
[180, 'acute accent'],
[181, 'micro sign'],
[182, 'pilcrow sign'],
[183, 'middle dot'],
[184, 'cedilla'],
[185, 'superscript one'],
[186, 'masculine ordinal indicator'],
[187, 'right-pointing double angle quotation mark'],
[188, 'vulgar fraction one quarter'],
[189, 'vulgar fraction one half'],
[190, 'vulgar fraction three quarters'],
[191, 'inverted question mark'],
[192, 'latin capital letter a with grave'],
[193, 'latin capital letter a with acute'],
[194, 'latin capital letter a with circumflex'],
[195, 'latin capital letter a with tilde'],
[196, 'latin capital letter a with diaeresis'],
[197, 'latin capital letter a with ring above'],
[198, 'latin capital letter ae'],
[199, 'latin capital letter c with cedilla'],
[200, 'latin capital letter e with grave'],
[201, 'latin capital letter e with acute'],
[202, 'latin capital letter e with circumflex'],
[203, 'latin capital letter e with diaeresis'],
[204, 'latin capital letter i with grave'],
[205, 'latin capital letter i with acute'],
[206, 'latin capital letter i with circumflex'],
[207, 'latin capital letter i with diaeresis'],
[208, 'latin capital letter eth'],
[209, 'latin capital letter n with tilde'],
[210, 'latin capital letter o with grave'],
[211, 'latin capital letter o with acute'],
[212, 'latin capital letter o with circumflex'],
[213, 'latin capital letter o with tilde'],
[214, 'latin capital letter o with diaeresis'],
[215, 'multiplication sign'],
[216, 'latin capital letter o with stroke'],
[217, 'latin capital letter u with grave'],
[218, 'latin capital letter u with acute'],
[219, 'latin capital letter u with circumflex'],
[220, 'latin capital letter u with diaeresis'],
[221, 'latin capital letter y with acute'],
[222, 'latin capital letter thorn'],
[223, 'latin small letter sharp s'],
[224, 'latin small letter a with grave'],
[225, 'latin small letter a with acute'],
[226, 'latin small letter a with circumflex'],
[227, 'latin small letter a with tilde'],
[228, 'latin small letter a with diaeresis'],
[229, 'latin small letter a with ring above'],
[230, 'latin small letter ae'],
[231, 'latin small letter c with cedilla'],
[232, 'latin small letter e with grave'],
[233, 'latin small letter e with acute'],
[234, 'latin small letter e with circumflex'],
[235, 'latin small letter e with diaeresis'],
[236, 'latin small letter i with grave'],
[237, 'latin small letter i with acute'],
[238, 'latin small letter i with circumflex'],
[239, 'latin small letter i with diaeresis'],
[240, 'latin small letter eth'],
[241, 'latin small letter n with tilde'],
[242, 'latin small letter o with grave'],
[243, 'latin small letter o with acute'],
[244, 'latin small letter o with circumflex'],
[245, 'latin small letter o with tilde'],
[246, 'latin small letter o with diaeresis'],
[247, 'division sign'],
[248, 'latin small letter o with stroke'],
[249, 'latin small letter u with grave'],
[250, 'latin small letter u with acute'],
[251, 'latin small letter u with circumflex'],
[252, 'latin small letter u with diaeresis'],
[253, 'latin small letter y with acute'],
[254, 'latin small letter thorn'],
[255, 'latin small letter y with diaeresis'],
[256, 'latin capital letter a with macron'],
[257, 'latin small letter a with macron'],
[258, 'latin capital letter a with breve'],
[259, 'latin small letter a with breve'],
[260, 'latin capital letter a with ogonek'],
[261, 'latin small letter a with ogonek'],
[262, 'latin capital letter c with acute'],
[263, 'latin small letter c with acute'],
[264, 'latin capital letter c with circumflex'],
[265, 'latin small letter c with circumflex'],
[266, 'latin capital letter c with dot above'],
[267, 'latin small letter c with dot above'],
[268, 'latin capital letter c with caron'],
[269, 'latin small letter c with caron'],
[270, 'latin capital letter d with caron'],
[271, 'latin small letter d with caron'],
[272, 'latin capital letter d with stroke'],
[273, 'latin small letter d with stroke'],
[274, 'latin capital letter e with macron'],
[275, 'latin small letter e with macron'],
[276, 'latin capital letter e with breve'],
[277, 'latin small letter e with breve'],
[278, 'latin capital letter e with dot above'],
[279, 'latin small letter e with dot above'],
[280, 'latin capital letter e with ogonek'],
[281, 'latin small letter e with ogonek'],
[282, 'latin capital letter e with caron'],
[283, 'latin small letter e with caron'],
[284, 'latin capital letter g with circumflex'],
[285, 'latin small letter g with circumflex'],
[286, 'latin capital letter g with breve'],
[287, 'latin small letter g with breve'],
[288, 'latin capital letter g with dot above'],
[289, 'latin small letter g with dot above'],
[290, 'latin capital letter g with cedilla'],
[291, 'latin small letter g with cedilla'],
[292, 'latin capital letter h with circumflex'],
[293, 'latin small letter h with circumflex'],
[294, 'latin capital letter h with stroke'],
[295, 'latin small letter h with stroke'],
[296, 'latin capital letter i with tilde'],
[297, 'latin small letter i with tilde'],
[298, 'latin capital letter i with macron'],
[299, 'latin small letter i with macron'],
[300, 'latin capital letter i with breve'],
[301, 'latin small letter i with breve'],
[302, 'latin capital letter i with ogonek'],
[303, 'latin small letter i with ogonek'],
[304, 'latin capital letter i with dot above'],
[305, 'latin small letter dotless i'],
[306, 'latin capital ligature ij'],
[307, 'latin small ligature ij'],
[308, 'latin capital letter j with circumflex'],
[309, 'latin small letter j with circumflex'],
[310, 'latin capital letter k with cedilla'],
[311, 'latin small letter k with cedilla'],
[312, 'latin small letter kra'],
[313, 'latin capital letter l with acute'],
[314, 'latin small letter l with acute'],
[315, 'latin capital letter l with cedilla'],
[316, 'latin small letter l with cedilla'],
[317, 'latin capital letter l with caron'],
[318, 'latin small letter l with caron'],
[319, 'latin capital letter l with middle dot'],
[320, 'latin small letter l with middle dot'],
[321, 'latin capital letter l with stroke'],
[322, 'latin small letter l with stroke'],
[323, 'latin capital letter n with acute'],
[324, 'latin small letter n with acute'],
[325, 'latin capital letter n with cedilla'],
[326, 'latin small letter n with cedilla'],
[327, 'latin capital letter n with caron'],
[328, 'latin small letter n with caron'],
[329, 'latin small letter n preceded by apostrophe'],
[330, 'latin capital letter eng'],
[331, 'latin small letter eng'],
[332, 'latin capital letter o with macron'],
[333, 'latin small letter o with macron'],
[334, 'latin capital letter o with breve'],
[335, 'latin small letter o with breve'],
[336, 'latin capital letter o with double acute'],
[337, 'latin small letter o with double acute'],
[338, 'latin capital ligature oe'],
[339, 'latin small ligature oe'],
[340, 'latin capital letter r with acute'],
[341, 'latin small letter r with acute'],
[342, 'latin capital letter r with cedilla'],
[343, 'latin small letter r with cedilla'],
[344, 'latin capital letter r with caron'],
[345, 'latin small letter r with caron'],
[346, 'latin capital letter s with acute'],
[347, 'latin small letter s with acute'],
[348, 'latin capital letter s with circumflex'],
[349, 'latin small letter s with circumflex'],
[350, 'latin capital letter s with cedilla'],
[351, 'latin small letter s with cedilla'],
[352, 'latin capital letter s with caron'],
[353, 'latin small letter s with caron'],
[354, 'latin capital letter t with cedilla'],
[355, 'latin small letter t with cedilla'],
[356, 'latin capital letter t with caron'],
[357, 'latin small letter t with caron'],
[358, 'latin capital letter t with stroke'],
[359, 'latin small letter t with stroke'],
[360, 'latin capital letter u with tilde'],
[361, 'latin small letter u with tilde'],
[362, 'latin capital letter u with macron'],
[363, 'latin small letter u with macron'],
[364, 'latin capital letter u with breve'],
[365, 'latin small letter u with breve'],
[366, 'latin capital letter u with ring above'],
[367, 'latin small letter u with ring above'],
[368, 'latin capital letter u with double acute'],
[369, 'latin small letter u with double acute'],
[370, 'latin capital letter u with ogonek'],
[371, 'latin small letter u with ogonek'],
[372, 'latin capital letter w with circumflex'],
[373, 'latin small letter w with circumflex'],
[374, 'latin capital letter y with circumflex'],
[375, 'latin small letter y with circumflex'],
[376, 'latin capital letter y with diaeresis'],
[377, 'latin capital letter z with acute'],
[378, 'latin small letter z with acute'],
[379, 'latin capital letter z with dot above'],
[380, 'latin small letter z with dot above'],
[381, 'latin capital letter z with caron'],
[382, 'latin small letter z with caron'],
[383, 'latin small letter long s'],
[461, 'latin capital letter a with caron'],
[462, 'latin small letter a with caron'],
[463, 'latin capital letter i with caron'],
[464, 'latin small letter i with caron'],
[465, 'latin capital letter o with caron'],
[466, 'latin small letter o with caron'],
[467, 'latin capital letter u with caron'],
[468, 'latin small letter u with caron'],
[469, 'latin capital letter u with diaeresis and macron'],
[470, 'latin small letter u with diaeresis and macron'],
[471, 'latin capital letter u with diaeresis and acute'],
[472, 'latin small letter u with diaeresis and acute'],
[473, 'latin capital letter u with diaeresis and caron'],
[474, 'latin small letter u with diaeresis and caron'],
[475, 'latin capital letter u with diaeresis and grave'],
[476, 'latin small letter u with diaeresis and grave'],
[477, 'latin small letter turned e'],
[478, 'latin capital letter a with diaeresis and macron'],
[479, 'latin small letter a with diaeresis and macron'],
[480, 'latin capital letter a with dot above and macron'],
[481, 'latin small letter a with dot above and macron'],
[482, 'latin capital letter ae with macron'],
[483, 'latin small letter ae with macron'],
[484, 'latin capital letter g with stroke'],
[485, 'latin small letter g with stroke'],
[486, 'latin capital letter g with caron'],
[487, 'latin small letter g with caron'],
[488, 'latin capital letter k with caron'],
[489, 'latin small letter k with caron'],
[490, 'latin capital letter o with ogonek'],
[491, 'latin small letter o with ogonek'],
[492, 'latin capital letter o with ogonek and macron'],
[493, 'latin small letter o with ogonek and macron'],
[494, 'latin capital letter ezh with caron'],
[495, 'latin small letter ezh with caron'],
[496, 'latin small letter j with caron'],
[497, 'latin capital letter dz'],
[498, 'latin capital letter d with small letter z'],
[499, 'latin small letter dz'],
[500, 'latin capital letter g with acute'],
[501, 'latin small letter g with acute'],
[502, 'latin capital letter hwair'],
[503, 'latin capital letter wynn'],
[504, 'latin capital letter n with grave'],
[505, 'latin small letter n with grave'],
[506, 'latin capital letter a with ring above and acute'],
[507, 'latin small letter a with ring above and acute'],
[508, 'latin capital letter ae with acute'],
[509, 'latin small letter ae with acute'],
[510, 'latin capital letter o with stroke and acute'],
[511, 'latin small letter o with stroke and acute'],
[512, 'latin capital letter a with double grave'],
[513, 'latin small letter a with double grave'],
[514, 'latin capital letter a with inverted breve'],
[515, 'latin small letter a with inverted breve'],
[516, 'latin capital letter e with double grave'],
[517, 'latin small letter e with double grave'],
[518, 'latin capital letter e with inverted breve'],
[519, 'latin small letter e with inverted breve'],
[520, 'latin capital letter i with double grave'],
[521, 'latin small letter i with double grave'],
[522, 'latin capital letter i with inverted breve'],
[523, 'latin small letter i with inverted breve'],
[524, 'latin capital letter o with double grave'],
[525, 'latin small letter o with double grave'],
[526, 'latin capital letter o with inverted breve'],
[527, 'latin small letter o with inverted breve'],
[528, 'latin capital letter r with double grave'],
[529, 'latin small letter r with double grave'],
[530, 'latin capital letter r with inverted breve'],
[531, 'latin small letter r with inverted breve'],
[532, 'latin capital letter u with double grave'],
[533, 'latin small letter u with double grave'],
[534, 'latin capital letter u with inverted breve'],
[535, 'latin small letter u with inverted breve'],
[536, 'latin capital letter s with comma below'],
[537, 'latin small letter s with comma below'],
[538, 'latin capital letter t with comma below'],
[539, 'latin small letter t with comma below'],
[540, 'latin capital letter yogh'],
[541, 'latin small letter yogh'],
[542, 'latin capital letter h with caron'],
[543, 'latin small letter h with caron'],
[544, 'latin capital letter n with long right leg'],
[545, 'latin small letter d with curl'],
[546, 'latin capital letter ou'],
[547, 'latin small letter ou'],
[548, 'latin capital letter z with hook'],
[549, 'latin small letter z with hook'],
[550, 'latin capital letter a with dot above'],
[551, 'latin small letter a with dot above'],
[552, 'latin capital letter e with cedilla'],
[553, 'latin small letter e with cedilla'],
[554, 'latin capital letter o with diaeresis and macron'],
[555, 'latin small letter o with diaeresis and macron'],
[556, 'latin capital letter o with tilde and macron'],
[557, 'latin small letter o with tilde and macron'],
[558, 'latin capital letter o with dot above'],
[559, 'latin small letter o with dot above'],
[560, 'latin capital letter o with dot above and macron'],
[561, 'latin small letter o with dot above and macron'],
[562, 'latin capital letter y with macron'],
[563, 'latin small letter y with macron'],
[8211, 'en dash (Gedankenstrich)'],
[8224, 'dagger (crux)'],
]
function addBlock(elem) {
	var oldBlock = elem.parentElement.parentElement.previousElementSibling;
	var newBlock = oldBlock.cloneNode(true);
	newBlock.setAttribute('data-id', '0');
	$(newBlock).find('[data-id]').each(function(i) {
		this.setAttribute('data-id', '0');
	});
	var rank = newBlock.getAttribute('data-rank');
	if (rank) {
		newBlock.setAttribute('data-rank', parseInt(rank) + 1);
	};
	if ($(newBlock).find('> th > div > button').length < 1) {
		$(newBlock).find('> th').append(' <div style="display: inline-flex"><button class="lid" type="button">✕</button><button onclick="deleteSubitem(this)" type="button" value="delete">✕</button></div>');
	};
	$(newBlock).find('td > div.rich').each(function(i) {
		this.innerHTML = '';
		this.removeAttribute('id');
		this.parentElement.previousElementSibling.removeAttribute('style');
	});
	$(oldBlock).after(newBlock);
}
function addShortcuts(editor) {
	editor.shortcuts.add('alt+q', 'Quellcode', 'mceCodeEditor');
	editor.shortcuts.add('alt+o', 'Omega-Tafel (Sonderzeichen)', 'mceShowCharmap');
	editor.shortcuts.add('alt+l', 'Link', 'mceLink');
	editor.shortcuts.add('alt+t', 'Tafelspeicherlink', function() {insertFromLocalStorage(editor)});
	editor.shortcuts.add('alt+a', 'Autograph', function() {insertAnnotation(editor, 'autograph')});
	editor.shortcuts.add('alt+i', 'Incipit', function() {insertAnnotation(editor, 'incipit')});
	editor.shortcuts.add('alt+n', 'Nicht ediert', function() {insertAnnotation(editor, 'unediert')});
	editor.shortcuts.add('alt+u', 'Unikat', function() {insertAnnotation(editor, 'unikat')});
}
function deleteSubitem(button) {
	var block = button.parentElement.parentElement.parentElement;
	var item_id = block.getAttribute('data-id');
	if (item_id) {
		var col = block.getAttribute('data-parent_col') || '';
		var table = col.substring(0, col.indexOf('_'));
		$.post(`/redact/${table}/${item_id}`, {'redaction': 'delete'});
	};
	block.remove();
}
function extractSpaces(str) {
	var prespace = /^\s/.test(str) ? ' ' : '';
	var postspace = /\s$/.test(str) ? ' ' : '';
	str = str.trim();
	return [prespace, str, postspace];
}
function geo(e, info) {
	const keyElem = e.currentTarget;
	const anchorElem = document.getElementById('geo-anchor');
	var mapElem = document.getElementById('geo');
	if (mapElem) {
		mapElem.remove();
		document.getElementById('temp').remove();
		try{document.getElementById('treffer').scrollIntoView();} catch {};
	} else {
		$.ajax({
			type: 'POST',
			url: '/geo',
			data: info,
		}).done(function(data) {
			if (data['note']) {
				announce(data['note']);
			} else {
				const geodata = data['geodata'];
				let i = geodata.length;
				const mapElem = document.createElement('div');
				mapElem.setAttribute('id', 'geo');
				mapElem.setAttribute('style', 'height: calc(100vh - 4em); width: 100%');
				anchorElem.after(mapElem);
				keyElem.innerHTML += '<span id="temp"> ✕</span>';
				var map = L.map('geo').setView(
					(i === 1 ? [geodata[0]['ort_breite'], geodata[0]['ort_länge']] : [50.110556, 8.682222]),
					6,
				);
				L.tileLayer(
					'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
					{
						'maxZoom': 16,
						'attribution': '© <a href="http://www.openstreetmap.org/copyright" title="Geodaten erhoben von Simone Mayer, Roman Deutinger">OSM</a>',
					},
				).addTo(map);
				while (i--) {
					let place = geodata[i];
					let placeName = place['ort_name'];
					if (place['ort_orden']) {
						placeName = `${placeName} (${place['ort_orden']})`;
					};
					let labellength = ((place['persons'].length + place['works'].length).toString().length ** 0.5) + 0.1;
					let color =
						place['ort_gattung'] === 'Bistum' ? 'purple' :
						place['ort_gattung'] === 'Kloster' ? 'darkgreen' :
						place['ort_gattung'] === 'Land' ? '#ffff66' :
						'royalblue';
					let style = `
background: ${color};
border-radius: ${labellength}em;
box-shadow: ${place['ort_gattung'] === 'Land' ? '0 0 15px 5px #ffff66' : 'none'};
color: ${place['ort_gattung'] === 'Land' ? '#000000' : '#ffffff'};
height: ${labellength}em !important;
line-height: ${labellength}em;
opacity: 0.8;
text-align: center;
vertical-align: middle;
width: ${labellength}em !important;
`.trim().replaceAll('\n', ' ');
					let persons = place['persons'].map(row =>
							`<a href="/person/${row['ID']}" target="_blank">${row['name']}</a>`).join('</li><li>');
					let works = place['works'].map(row =>
							`<a href="/werk/${row['ID']}" target="_blank"><cite>${row['name']}</cite>${
								row['person'] ? ' (' + row['person'] + ')' : ''
							}</a>`).join('</li><li>');
					let personlist = persons ? `\n<ul class="list"><li>${persons}</li></ul>` : '';
					let worklist = works ? `\n<ul class="list"><li>${works}</li></ul>` : '';
					L.marker(
						[place['ort_breite'], place['ort_länge']],
						{
							icon: L.divIcon({
								className: 'leaflet-mark',
								html: `<div style="${style}">${(place['persons'].length + place['works'].length) || ''}<div>`,
							}),
						},
					).addTo(map).bindPopup(
						`<a href="/ort/${place['ort_id']}" target="_blank">${placeName}</a>${personlist}${worklist}`,
						{
							closeButton: true,
							maxHeight: mapElem.offsetHeight - 32,
							maxWidth: mapElem.offsetWidth ** 0.9,
							minWidth: 64,
						},
					);
				};
				document.getElementById('geo').scrollIntoView();
			};
		});
	};
}
function initEditor(elem) {
	tinymce.init({
		branding: false,
		charmap: charmap,
		convert_urls: false,
		entity_encoding : 'raw',
		formats: {
			bold: {inline: 'b'},
		},
		init_instance_callback: function(editor) {addShortcuts(editor)},
		inline: true,
		inline_styles: false,
		link_title: false,
		menubar: false,
		paste_as_text: true,
		plugins: 'charmap code link paste save',
		resize: false,
		save_onsavecallback: function(editor) {save_onsavecallback(editor)},
		setup: function(editor) {setupEditor(editor)},
		target: elem,
		target_list: false,
		toolbar: [
			'save | undo | redo | code | charmap | paste | link | insertFromLocalStorage |​ insertAutograph | insertNotEdited | insertUnicum | insertIncipit | bold superscript'
		],
		valid_elements: 'a[href],b,p,sup',
		verify_html: true,
	});
}
function insertAnnotation(editor, txt) {
	var tag = '<a href="/a/' + txt + '">';
	insertTag(editor, txt, tag, '</a>');
}
function insertFromLocalStorage(editor) {
	try {
		var temp = localStorage.getItem('_').split('<>');
		var tag = temp[0];
		var txt = temp[1];
	} catch(err) {return};
	insertTag(editor, txt, tag, '</a>');
}
function insertTag(editor, txt, starttag, endtag) {
	txt = editor.selection.getContent() || txt;
	var prespace, postspace;
	[prespace, txt, postspace] = extractSpaces(txt);
	editor.insertContent(prespace + starttag + txt + endtag + postspace);
}
function s(elem, id) {
	// store in localStorage.
	var table = document.querySelector('[data-dbtable]').getAttribute('data-dbtable');
	var row = elem.parentElement.parentElement;
	var txt = row.querySelector('td:first-of-type').innerHTML;
	var input = row.querySelector('td:nth-last-child(3) input:checked');
	if (input === null) {
		var query = '';
	} else {
		var query = '?modus=' + input.getAttribute('value');
	};
	var tag = `<a href="/${table}/${id}${query}">`;
	localStorage.setItem('_', `${tag}<>${txt}`);
}
function save_onsavecallback(editor) {
	var doc = editor.getContent();
	var elem = editor.getElement();
	var col = elem.getAttribute('data-col');
	var parent = elem.parentElement;
	var item_id = 0;
	while (item_id === 0) {
		parent = parent.parentElement;
		if (parent === null) {break};
		item_id = parent.getAttribute('data-id') || 0;
	};
	if (col === null || parent === null || item_id === 0) {return};
	var kind = parent.getAttribute('data-kind') || '';
	var rank = parent.getAttribute('data-rank') || 0;
	var parent_col = parent.getAttribute('data-parent_col') || '';
	var grandparent_col = parent.getAttribute('data-grandparent_col') || '';
	var parent_id = 0;
	var grandparent_id = 0;
	var ancestor = parent;
	if (parent_col) {
		while (parent_id === 0) {
			ancestor = ancestor.parentElement;
			if (ancestor === null) {break};
			parent_id = ancestor.getAttribute('data-id') || 0;
		};
	};
	if (grandparent_col) {
		while (grandparent_id === 0) {
			ancestor = ancestor.parentElement;
			if (ancestor === null) {break};
			grandparent_id = ancestor.getAttribute('data-id') || 0;
		};
	};
	$.ajax({
		type: 'POST',
		url: '/upsert',
		data: {
			'col': col,
			'doc': doc,
			'item_id': item_id,
			'kind': kind,
			'rank': rank,
			'parent_col': parent_col,
			'parent_id': parent_id,
			'grandparent_col': grandparent_col,
			'grandparent_id': grandparent_id
		},
		async: false, // Block further interaction, until this upsert is done: This is necessary! in order to prevent the error that – predominantly via the save-all-function – parts of a parent block without item_id are saved in parallel and receive different item_ids for their parent block or a part without item_id is saved repeatedly (when the function is triggered in addition to a single save via the editor) and receives itself several item_ids.
	}).done(function(data) {
		if (data['note']) {
			announce(data['note']);
		} else {
			parent.setAttribute('data-id', data['item_id']);
			editor.setContent(data['doc']);
			editor.save();
			elem.parentElement.previousElementSibling.removeAttribute('style');
		};
	});
}
function saveAllEditors(selector) {
	var divs = $(selector);
	var i = divs.length;
	while (i--) {
		let editor = tinymce.get(divs[i].id);
		if (editor) {
			save_onsavecallback(editor);
		};
	};
}
function setupEditor(editor) {
	editor.on('Paste Change input Undo Redo', function(e) {
		if (editor.isDirty()) {
			editor.getElement().parentElement.previousElementSibling.setAttribute('style', 'background-color: #ffff80');
		};
	});
	editor.ui.registry.addButton('insertAutograph', {
		onAction: function() {insertAnnotation(editor, 'autograph')},
		text: 'Autograph',
		title: '‘Autograph’-Link einfügen (Alt+a)',
	});
	editor.ui.registry.addButton('insertFromLocalStorage', {
		onAction: function() {insertFromLocalStorage(editor)},
		text: '📋',
		title: 'Link aus dem Tafelspeicher einfügen oder, wenn Text ausgewählt ist, diesen Text mit dem Link hinterlegen (Alt+t)',
	});
	editor.ui.registry.addButton('insertIncipit', {
		onAction: function() {insertAnnotation(editor, 'incipit')},
		text: 'Incipit',
		title: '‘Incipit’-Link einfügen (Alt+i)',
	});
	editor.ui.registry.addButton('insertNotEdited', {
		onAction: function() {insertAnnotation(editor, 'unediert')},
		text: 'Nicht ediert',
		title: '‘Nicht-ediert’-Link einfügen (Alt+n)',
	});
	editor.ui.registry.addButton('insertUnicum', {
		onAction: function() {insertAnnotation(editor, 'unikat')},
		text: 'Unikat',
		title: '‘Unikat’-Link einfügen (Alt+u)',
	});
}
document.addEventListener("DOMContentLoaded", function(event) {
	let params = new URLSearchParams(document.location.search);
	if (params.get('geo') !== null) {
		document.getElementById('geo-key').click();
	};
});