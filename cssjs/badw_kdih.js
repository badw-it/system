// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2018 ff.
function addBlock(elem) {
	var oldBlock = elem.previousElementSibling;
	var newBlock = oldBlock.cloneNode(true);
	$(newBlock).find('[name]').each(function(i) {
		var parts = this.getAttribute('name').split('_');
		this.setAttribute('name', parts[0] + '_' + String(parseInt(parts[1]) + 1));
	});
	$(newBlock).find('.petit').each(function(i) {
		this.remove();
	});
	$(newBlock).find('[type="search"]').each(function(i) {
		this.value = '';
	});
	$(newBlock).find('[type="checkbox"]').each(function(i) {
		this.checked = false;
	});
	$(oldBlock).after(newBlock);
}
