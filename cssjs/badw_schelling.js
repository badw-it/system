// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2013 ff.
function nav(direction, event, ref) {
	const query = `article [href="/${ref}"], article [href^="/${ref}/"]`;
	const matches = document.querySelectorAll(query);
	const matchesLen = matches.length;
	var pos = parseInt(event.target.dataset.pos);
	if (direction === 'prev') {
		if (pos < 1) {
			pos = matchesLen - 1;
		} else {
			pos -= 1;
		};
		document.querySelector('main > nav .arrowright').dataset.pos = pos;
	} else if (direction === 'next') {
		if (pos >= matchesLen - 1) {
			pos = 0;
		} else {
			pos += 1;
		};
		document.querySelector('main > nav .arrowleft').dataset.pos = pos;
	};
	event.target.dataset.pos = pos;
	document.querySelectorAll('.blink').forEach(element => element.classList.remove('blink'));
	let element = matches[pos];
	$.when(element.scrollIntoView({behavior: "instant", block: "center"})).then(element.classList.add('blink'));
}
function openDir(event) {
	let tr = event.target.parentNode.parentNode;
	window.open(`/ablage/${tr.id}`, '_blank');
}
const urlstart = document.querySelector('html[class~="editor"]') ?
		'https://schelling.badw.de' // for local files: They have to get database content from the server.
		: window.location.origin; // elsewhere, particularly on localhost.
document.addEventListener("DOMContentLoaded", function(event) {
	heureka(urlstart);
	for (let element of document.querySelectorAll('.edition article a, .editor article a')) {
		try {var href = element.attributes.href.value} catch {continue};
		element.setAttribute('data-tipped', '1');
		if (/^\d+#\d+$/.test(href)) { // cross references
			addTip(element, `${urlstart}/doc/${href}`);
		} else if (/^\/(doc|kapitel|lit|ort|person|system|thema)\//.test(href)) { // annotations
			addTip(element, `${urlstart}${href}`);
		} else if (/^\/(bibel)\//.test(href)) { // bible annotations
			href = href.replace('bibel', 'system/2');
			addTip(element, `${urlstart}${href}`);
		}
		else if (/^\.\//.test(href)) { // paginastarts associated with facsimilia
			element.addEventListener('click', function(e) {
				window.open(
						`${e.target.href.split('#')[0]}.jpg`,
						'_blank',
						`left=0,width=${Math.max(100, window.screen.width / 2)}`);
				e.preventDefault();
			});
		};
	};
});
