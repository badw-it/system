// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2013 ff.
function addBlockUser(elem) { // used in `../../code/gestalt/auth_admin.tpl`.
	var oldBlock = elem.parentElement.parentElement.previousElementSibling;
	var newBlock = oldBlock.cloneNode(true);
	$(newBlock).find('input').each(function(i) {
		var name = this.getAttribute('name').split('-')[1];
		this.setAttribute('name', '0-' + name);
		if (name.endsWith('name') || name.endsWith('password') || name.endsWith('password_repeated')) {
			this.setAttribute('value', '');
		};
		if (name.endsWith('delete')) {this.remove();};
		// because a user in spe is not to be deleted already
		// and if oldBlock is the current user, the delete input is not there.
	});
	$(oldBlock).after(newBlock);
}
function addTip(element, url) {
	element.addEventListener('mouseenter', async function(e) {
		const element = document.getElementById('__tip__');
		if (element) {element.remove()};
		const response = await fetch(url.split('#')[0] + '?form=tip');
		let data = await response.text();
		if (data.startsWith('<span>')) {
			document.body.insertAdjacentHTML('afterbegin', data.replace('<span>', '<span id="__tip__">'));
		};
	});
}
function announce(note, elemSelector = "#top") {
	$(elemSelector).prepend(`<input class="flip" id="flip1" type="checkbox"/><label class="key notice top" for="flip1"></label><article class="card">${note}</article>`);
}
function clipcopy(e, self_0_prev_1_next_2) {
	var range = document.createRange();
	var button = $(e.target);
	var sel = window.getSelection();
	var i = sel.rangeCount;
	while (i--) {
		sel.removeRange(sel.getRangeAt(i));
	};
	if (self_0_prev_1_next_2 === 1) {
		range.selectNode(button.prev()[0]);
	} else if (self_0_prev_1_next_2 === 2) {
		range.selectNode(button.next()[0]);
	} else {
		range.selectNode(e.target);
	};
	sel.addRange(range);
	if (document.execCommand('copy')) {
		var text = button.attr('data-done');
		if (text) {
			button.text(text);
		};
	};
}
function escape(term) {
	return term.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;');
}
function heureka(urlstart) {
	// Substitutions in the HTML-XML of any article element under an element with its class being “edition” or “editor”:
	let articles = document.querySelectorAll(':is(.edition, .editor) article');
	if (articles.length) {
		// Substitutions of certain strings:
		for (let article of articles) {
			let htmlOld = article.innerHTML;
			let htmlNew = htmlOld
				.replaceAll(/\[((?:<[^>]*>|[^\]\s])*)\]/g, '<span style="font-style: italic; outline: none">$1</span>')
				.replaceAll('˖', '')
				.replaceAll(/\€([^€]+)\€/g, '<ka-tex>$1</ka-tex>')
				;
			if (htmlNew != htmlOld) {article.innerHTML = htmlNew};
		};
		// Add labels:
		const attrNames = ['data-by', 'data-from', 'data-of', 'lang'];
		for (let element of document.querySelectorAll(`:is(.edition , .editor) :is([${attrNames.join('], [')}])`)) {
			let attrs = [];
			for (let attrName of attrNames) {
				if (element.hasAttribute(attrName)) {
					let attr = escape(element.getAttribute(attrName));
					attrs.push(`<span class="preannotation ${attrName}">${attr}:</span>`);
				};
			};
			if (attrs.length) {
				element.insertAdjacentHTML('beforebegin', attrs.join(' '));
			};
		};
		// Add KaTeX:
		document.head.insertAdjacentHTML(
				'beforeend',
				`<link href="${urlstart}/cssjs/katex/katex.min.css" media="all" rel="stylesheet"/>`);
		const script = document.createElement('script');
		script.src = `${urlstart}/cssjs/katex/katex.min.js`;
		script.onload = () => {
			let elements = document.getElementsByTagName('ka-tex');
			for (let element of elements) {
				katex.render(element.innerHTML, element, {throwOnError: false});
			};
		};
		document.head.appendChild(script);
		// Remove tips on click or scroll:
		document.addEventListener('click', function(e) {
			if (!e.target.closest('#__tip__')) { // only if it is neither e.target nor a parent of e.target.
				const element = document.getElementById('__tip__');
				if (element) {element.remove()};
			};
		});
		document.addEventListener('scroll', function(e) {
			const element = document.getElementById('__tip__');
			if (element) {element.remove()};
		});
	};
};
function insertAtCursor(fieldID, term) {
	// Mainly from https://stackoverflow.com/a/11077016
	var field = document.getElementById(fieldID);
	if (document.selection) {//for IE:
		field.focus();
		sel = document.selection.createRange();
		sel.text = term;
	} else if (field.selectionStart || field.selectionStart == '0') {
		var startPos = field.selectionStart;
		var endPos = field.selectionEnd;
		field.value = field.value.substring(0, startPos)
			+ term
			+ field.value.substring(endPos, field.value.length);
	} else {
		field.value += term;
	}
	field.focus();
}
function unwrapTextNodes() {
	$(this).parent().find('.mark').contents().unwrap();
}
function wrapTextNodes() {
	var id = $(this).attr('data-off');
	var node = this;
	while (true) {
		var node = node.previousSibling;
		if (node === null) {
			return;
		} else if (node.nodeType === 3) {
			$(node).wrap('<span class="mark">');
			// Hereafter, the wrapping is the sibling, thus:
			node = node.parentElement;
		} else if (node.nodeType === 1) {
			if (node.getAttribute('data-on') === id) {return;}
			$(node).wrap('<span class="mark">');
			node = node.parentElement;
		}
	};
}
