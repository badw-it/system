// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2013 ff.

/**
 * Attribution notice: rewritten by Daniel Schwarz in 2019 ff., amended by Stefan Müller in 2020 ff.
 * Creates a div element in a separate row with a close button, two pagination buttons and an image.
 * @param {MouseEvent} e - The click event from the open image button
 * @param {string} path - The path to the image
 * @param {Number} colspan - The column span value for the created table row element
 * @param {Number} distance - The jumping distance for browsing to the left or right.
 *        If undefined, it defaults to 1. If 0, no browsing is provided.
 * @param {Number} height - Optional: A height to be assigned in a style attribute.
 */
function rowImg(e, path, colspan, distance, height) {
	if (colspan === undefined) {colspan = 1};
	if (distance === undefined) {distance = 1};
	const oldButton = document.getElementById('focus');
	if (oldButton) {oldButton.removeAttribute('id')};
	e.target.id = 'focus';
	const id = $(e.target).parent().parent().attr('id');
	const old = document.getElementById('row_img');
	if (old) {old.outerHTML = ''};
	if (path) {
		/* The localized template for the image container with the related function buttons:
		 * 1.) A table row element with one table data element and its column span.
		 * 2.) The close, the backward and the forward button.
		 * 3.) A div container, with the image tag inside.
		 */
		var row_img_template = '<tr id="row_img">';
		row_img_template += 		'<td colspan="' + colspan + '">';
		row_img_template += 			'<button class="close_image">✕ <l- l="de">Schließen</l-><l- l="en">Close</l-></button>';
		if (distance !== 0) {
			row_img_template += 		'<button class="arrow-left">◁ <l- l="de">Zurückblättern (Pfeiltaste links)</l-><l- l="en">Backwards (left arrow key)</l-></button>';
			row_img_template += 		'<button class="arrow-right">▷ <l- l="de">Vorblättern (Pfeiltaste rechts)</l-><l- l="en">Forwards (right arrow key)</l-></button>';
		};
		row_img_template += 			'<div>';
		if (path.endsWith('.pdf')) {
			row_img_template += 			'<object id="image" data="'+path+pdfURLParams+'" type="application/pdf" style="'+ (height ? ('height: ' + height + 'px; ') : '') +'width: 100%"><a href="'+path+'" rel="noopener noreferrer" target="_blank">'+path+'</a></object>';
		} else {
			row_img_template += 			'<img id="image" alt="'+path+'" src="'+path+'"/>';
		};
		row_img_template += 			'</div>';
		row_img_template += 			'<button class="close_image">✕ <l- l="de">Schließen</l-><l- l="en">Close</l-></button>';
		if (distance !== 0) {
			row_img_template += 		'<button class="arrow-left">◁ <l- l="de">Zurückblättern (Pfeiltaste links)</l-><l- l="en">Backwards (left arrow key)</l-></button>';
			row_img_template += 		'<button class="arrow-right">▷ <l- l="de">Vorblättern (Pfeiltaste rechts)</l-><l- l="en">Forwards (right arrow key)</l-></button>';
		};
		row_img_template += 		'</td>';
		row_img_template += 	'</tr>'
		// Append the template after the current table row
		$(e.target).parent().parent().after(row_img_template);
		// Set the event handler for the buttons
		$('.close_image').click(function(){
			$('#row_img').remove();
		});
		if (distance !== 0) {
			$('.arrow-left').click(function() {
				showAdjacentImage('previous', 'image', distance);
			});
			$('.arrow-right').click(function() {
				showAdjacentImage('next', 'image', distance);
			});
			/* Create the key event listener for the pagination event to the document.
			 * If set once, create the state variable KEY_PAGINATION_ACTIVE
			 * and donʼt spawn handlers for the same event anymore.
			 * NOTE: The Internet Explorer uses Left and Right for the event key codes.
			 */
			if ($(document)[0].KEY_PAGINATION_ACTIVE === undefined) {
				$(document).on('keyup', function(e) {
					if ((e.key === 'ArrowLeft' || e.key === 'Left') && $('.arrow-left').length > 0) {
						showAdjacentImage('previous', 'image', distance);
					} else if ((e.key === 'ArrowRight' || e.key === 'Right') && $('.arrow-right').length > 0) {
						showAdjacentImage('next', 'image', distance);
					} else {
						return;
					}
				});
				$(document)[0].KEY_PAGINATION_ACTIVE = true;
			}
		}
	};
	if (id) {window.location.hash = '#' + id};
}

/**
 * Attribution notice: by Daniel Schwarz in 2019 ff., adapted by Stefan Müller in 2020 ff.	
 * Show the previous or the next image
 * @param {string} elementId - The id of the image DOM element
 * @param {string} direction - The direction where the neighbour is found: previous or next
 * @param {Number} distance  - How many pages backward or forward
 */
function showAdjacentImage(direction, elementId, distance) {
	var imageTag = document.getElementById(elementId);
	var srcValue = imageTag.src || (imageTag.data.split('#')[0]);
	// Get the last part of the URL i.e. the filename of the image
	var srcSplitted = srcValue.split('/');
	if (srcSplitted.length < 1) {
		return;
	}
	// From the filename, get the volume ID, the image number and the image type. They must be separated by dots.
	var filenameParts = srcSplitted[srcSplitted.length - 1].split('.');
	var volumeID = filenameParts[0];
	var imageNumber = parseInt(filenameParts[1]);
	var imageType = filenameParts[2];
	if (direction === 'next') {
		imageNumber = imageNumber + distance;
	} else if (direction === 'previous') {
		imageNumber = imageNumber - distance;
	}
	srcSplitted[srcSplitted.length - 1] = volumeID + "." + imageNumber + "." + imageType;
	var srcValueNew = srcSplitted.join('/');
	// Set the source of the image; if it fails to load, reset to the old value.
	$(imageTag)
		.on('error', function() { if (imageTag.src) {imageTag.src = srcValue} else {imageTag.data = srcValue + pdfURLParams} })
		.attr(imageTag.src ? "src" : "data", imageTag.src ? srcValueNew : srcValueNew + pdfURLParams);
}
const rI = rowImg;
const pdfURLParams = '#navpanes=0&statusbar=0&toolbar=0&view=FitH';
const urlstart = document.querySelector('html[class~="editor"]') ?
		'https://publikationen.badw.de' // for local files: They have to get database content from the server.
		: window.location.origin; // elsewhere, particularly on localhost.
document.addEventListener("DOMContentLoaded", function(event) {
	heureka(urlstart);
});
