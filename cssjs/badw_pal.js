// Licensed under http://www.apache.org/licenses/LICENSE-2.0
// Attribution notice: by Stefan Müller in 2013 ff.
function initEditor(selector) {
	tinymce.init({
		branding: false,
		content_css: ['/cssjs/all.css', '/cssjs/badw_pal.css'],
		convert_urls: false,
		custom_elements: "~non-public,~small-caps",
		entity_encoding: 'raw',
		fix_list_elements: true,
		formats: {
			bold: {inline: 'b'},
			italic: {inline: 'i'},
			non_public: {inline: 'non-public'},
            small_caps: {inline: 'small-caps'},
			strikethrough: {inline: 's'},
			underline: {inline: 'u'}
		},
		height: 400,
		inline_styles: false,
		link_title: false,
		menubar: false,
		paste_data_images: true,
		plugins: 'charmap code directionality fullscreen image link lists paste table visualblocks',
		resize: 'both',
		selector: selector,
		toolbar: [
			'undo redo | removeformat | link unlink | bold italic strikethrough underline | subscript superscript | ltr rtl | bullist numlist | table | charmap | code | fullscreen'
		],
		toolbar_mode: 'wrap',
		valid_elements: 'a[href|rel|target],b/strong,bdo[dir],i,img[alt|height|loading|src|width],li,non-public,ol,p[dir],s,small-caps,sub,sup,table,tr,td[style],th[style],u[class],ul',
        visualblocks_default_state: true,
		width: '99%',
        setup: function(editor) {
			editor.ui.registry.addButton('non_public', {
				text: 'non-public',
				title: 'non-public',
				onAction: function() {
					editor.focus();
					editor.formatter.toggle('non_public');
				}
			});
			editor.ui.registry.addButton('small_caps', {
				text: 'Aᴀ',
				title: 'Small caps',
				onAction: function() {
					editor.focus();
					editor.formatter.toggle('small_caps');
				}
			});
        }
	});
}
function listexport(e, linksquery, tablequery, colindex, urlpart, exportListItself) {
	const urlparams = new URLSearchParams(urlpart);
	const form = urlparams.get('form');
	const filename = form === 'print' ? 'Export.tex' : 'Export.html';
	const filetype = form === 'print' ? 'text/plain' : 'text/html';
	const data = linksquery ?
		$(linksquery) :
		$(tablequery).DataTable().rows( {order: 'applied', search: 'applied'} ).data();
	const length = data.length;
	if (exportListItself) {
		let rows = new Array(length);
		for (let i = 0; i < length; i++) {
			rows[i] = data[i][colindex];
		};
		$.ajax({url: urlpart, data: JSON.stringify(rows), datatype: 'html', method: 'POST'}).done(
			function(doc) {promptExport(doc, filename, filetype)});
	} else {
		let urls = [];
		for (let i = 0; i < length; i++) {
			let url = '';
			if (linksquery) {
				url = data[i].href || '';
			} else {
				url = data[i][colindex];
				if (typeof url != 'string') {
					url = url['_'] || '';
				};
				url = (url.match(/href="([^"]*)/) || [])[1] || '';
			};
			if (url && !(urls[urls.length - 1] === url)) { // Export re-occurring MS but not consecutively.
				urls.push(url);
			};
		};
		i = 0;
		let doc = '';
		function listexportCollect() {
			if (i === urls.length) {
				if (doc && form !== 'print') {
					doc += '</main></body></html>';
				};
				promptExport(doc, filename, filetype);
				return;
			};
			$.ajax({url: urls[i] + urlpart}).done(
				function(subdoc) {
					i++;
					if (form !== 'print') {
						if (doc) {
							subdoc = '\n<hr/>\n' + subdoc.match(/<article.*?<\/article>\s*/s)[0];
						} else {
							subdoc = subdoc.replace(/<\/(main|body|html)>\s*/g, '');
						};
					};
					doc += subdoc;
					listexportCollect();
				}
			);
		}
		listexportCollect();
	};
}
function rowOver(e, url) {
	let old = document.getElementById('row_over');
	if (old) {old.outerHTML = ''};
	if (url) {
		$.ajax({url: url, datatype: 'html'}).done(function(data) {
			$(e.target).after($(data).css({'left': (e.clientX - 12) + 'px', 'top': (e.clientY - 24) + 'px', 'overflow': 'visible', 'position': 'fixed', 'z-index': 1}));
		});
		$(e.target).parent().parent().on('mouseleave', function(e) {
			$('#row_over').remove();
		});
	};
}
function promptExport(doc, filename, filetype) {
	filename = filename || 'Export.html';
	const blob = new Blob([doc], {type: filetype || 'text/html'});
	const a = document.createElement('a');
	a.href = window.URL.createObjectURL(blob);
	a.download = filename;
	a.click();
	window.URL.revokeObjectURL(a.href);
}
function zoom(e) {
	$(this).css(
		'height',
		Math.max(
			128,
			parseInt($(this).css('height')) - ((e.deltaY > 0 ? 1 : -1) * 20)));
	e.preventDefault();
}
$(document).on('click scroll', function(e) {
	let element = $('#row_over');
	if (!$(e.target).closest(element).length) {element.remove()};
});
